<?php
/** -----------------------------------------------------------------------------------------------
 * @author : Kevin MAURICE / Vincent BENNER
 * @copyright : PAGE UP / SUPPL'ACTIV 2016
 *
 * @info : General Config
 * @detail :
 */

// ------------------------------------------------------------------------------------
// Define MySQL data
// ------------------------------------------------------------------------------------
define ( 'DB_HOSTNAME', 'localhost' );
define ( 'DB_DATABASE', 'bdd_supplactiv' );
define ( 'DB_USERNAME', 'root' );
define ( 'DB_PASSWORD', 'root' );
#define ( 'DB_DATABASE', 'Qa_WU-SUPPLACTIV-V3' );
#define('DB_USERNAME', 'pageup');
#define('DB_PASSWORD', 'bL-YUx3B');

// ------------------------------------------------------------------------------------
// Define URL_TYPE
// ------------------------------------------------------------------------------------
define ( 'URL_TYPE', "http://" );

// ------------------------------------------------------------------------------------
// Define ADRESSE_BO
// ------------------------------------------------------------------------------------
#define ( 'ADRESSE_BO', URL_TYPE . 'qa.supplactiv.planetb.fr/v3' );
#define ( 'ADRESSE_BO', URL_TYPE . 'qa.supplactiv.planetb.fr/v2' );
define ( 'ADRESSE_BO', URL_TYPE . '127.0.0.1:8888/supplactiv/v3' );

// ------------------------------------------------------------------------------------
// Define PROJECT_NAME
// ------------------------------------------------------------------------------------
define ( 'PROJECT_NAME', 'SUPPLACTIV' );
define( 'PROJECT_NAME_HEAD', 'Suppl\'ACTIV - Marketing Terrain en Pharmacie et Parapharmacie');
define('PROJECT_VERSION', 1);

// ------------------------------------------------------------------------------------
// Create and Define KEY_ID
// ------------------------------------------------------------------------------------
$finalKey = date ( 'dmY' ) + ((date ( 'dmY' ) % 11) % 7) + date ( 'dmY' ) + (date ( 'dmY' ) % 11);
define ( 'KEY_ID', strtoupper ( hash ( 'sha512', $finalKey ) ) );

// ------------------------------------------------------------------------------------
// Define TIME_VALIDITY (in second)
// ------------------------------------------------------------------------------------
define ( 'TIME_VALIDITY', 25000 );

// ------------------------------------------------------------------------------------
// Define ERROR_REFERER (default message)
// ------------------------------------------------------------------------------------
define ( 'ERROR_REFERER', 'ERREUR - Parametres manquants et/ou droits manquants' );

// ------------------------------------------------------------------------------------
// Define DEFAULT_MODULE & DEFAULT_SUBMODULE (dashboard)
// ------------------------------------------------------------------------------------
define ( 'DEFAULT_MODULE', '1.0-Dashboard' );
define ( 'DEFAULT_SUBMODULE', '' );

// ------------------------------------------------------------------------------------
// Define QUALITY_IMG
// Variable en 0 et 100
// ------------------------------------------------------------------------------------
define('QUALITY_IMG', 25);

// ------------------------------------------------------------------------------------
// Define Footer & sidebar Style
// page-sidebar-closed -> Pour afficher uniquement l'image
// page-sidebar-fixed -> ne bouge pas avec le scroll
// page-footer-fixed -> tout le temps visible
// ------------------------------------------------------------------------------------
define('SIDEBAR_STYLE', '');
define('FOOTER_STYLE', '');

define('NAVBAR_STYLE', 'navbar navbar-inverse navbar-fixed-top');

define('MAILJET_KEY_API', '8127b7f50d6e81673e3d353d46b1a335');
define('MAILJET_KEY_SECRET', '5dacb7ae369589c78793ccdf8b24cf1d');