<?php

// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Project Config
// @detail :
// ------------------------------------------------------------------------------------
// Initialisation du tableau de correspondance
$arrayDayCorres = array(
    1 => array('libelleMin' => 'L', 'libelleMax' => 'Lundi', 'boolWD' => true),
    2 => array('libelleMin' => 'M', 'libelleMax' => 'Mardi', 'boolWD' => true),
    3 => array('libelleMin' => 'M', 'libelleMax' => 'Mercredi', 'boolWD' => true),
    4 => array('libelleMin' => 'J', 'libelleMax' => 'Jeudi', 'boolWD' => true),
    5 => array('libelleMin' => 'V', 'libelleMax' => 'Vendredi', 'boolWD' => true),
    6 => array('libelleMin' => 'S', 'libelleMax' => 'Samedi', 'boolWD' => false),
    7 => array('libelleMin' => 'D', 'libelleMax' => 'Dimanche', 'boolWD' => false)
);

$arrayMonthCorres = array(
    1 => array('libelleMin' => 'JAN', 'libelleMax' => 'JANVIER'),
    2 => array('libelleMin' => 'FEV', 'libelleMax' => 'FEVRIER'),
    3 => array('libelleMin' => 'MAR', 'libelleMax' => 'MARS'),
    4 => array('libelleMin' => 'AVR', 'libelleMax' => 'AVRIL'),
    5 => array('libelleMin' => 'MAI', 'libelleMax' => 'MAI'),
    6 => array('libelleMin' => 'JUI', 'libelleMax' => 'JUIN'),
    7 => array('libelleMin' => 'JUI', 'libelleMax' => 'JUILLET'),
    8 => array('libelleMin' => 'AOU', 'libelleMax' => 'AOUT'),
    9 => array('libelleMin' => 'SEP', 'libelleMax' => 'SEPTEMBRE'),
    10 => array('libelleMin' => 'OCT', 'libelleMax' => 'OCTOBRE'),
    11 => array('libelleMin' => 'NOV', 'libelleMax' => 'NOVEMBRE'),
    12 => array('libelleMin' => 'DEC', 'libelleMax' => 'DECEMBRE')
);

$arrayMonthCorres2 = array(
    'JANVIER' => '01',
    'FEVRIER' => '02',
    'MARS' => '03',
    'AVRIL' => '04',
    'MAI' => '05',
    'JUIN' => '06',
    'JUILLET' => '07',
    'AOUT' => '08',
    'SEPTEMBRE' => '09',
    'OCTOBRE' => '10',
    'NOVEMBRE' => '11',
    'DECEMBRE' => '12'
);
