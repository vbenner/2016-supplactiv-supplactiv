<?php

// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Database Connexion
// @detail : No change (check config.general.php)
// ------------------------------------------------------------------------------------
ini_set('display_errors','off');

// ------------------------------------------------------------------------------------
// Require file config (for MySQL parameters)
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/config.general.php';

// ------------------------------------------------------------------------------------
// Require file project
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/config.project.php';

// ------------------------------------------------------------------------------------
// Require general function's
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../includes/functions/func.general.php';

// ------------------------------------------------------------------------------------
// Class Database Connexion
// ------------------------------------------------------------------------------------
class DbConnexion {
	// ------------------------------------------------------------------------------------
	// Define instance
	// ------------------------------------------------------------------------------------
	private static $instance;
	
	// ------------------------------------------------------------------------------------
	// Get or create database instance
	// Return instance of database
	// ------------------------------------------------------------------------------------
	public static function getInstance() {
		if (! isset ( self::$instance )) {
			try {
				self::$instance = new PDO ( "mysql:host=" . DB_HOSTNAME . ";dbname=" . DB_DATABASE, DB_USERNAME, DB_PASSWORD );

                self::$instance->exec('set session sql_mode = "" ');

				self::$instance->query ( "SET NAMES 'utf8'" );
			} catch ( Exception $e ) {
				echo 'ERREUR BDD - ' . $e->getMessage ();
				exit ();
			}
		}
		return self::$instance;
	}
}