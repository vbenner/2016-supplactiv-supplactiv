<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group input-daterange">
                            <label class="control-label col-md-2">Date début :</label>

                            <div class="col-md-4">
                                <input id="dateRechercheD_14_1" class="form-control form-recherche" type="text"
                                       placeholder="" name="start" style="text-align: left" value=""
                                       />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <label class="control-label col-md-2">Date fin :</label>

                            <div class="col-md-4">
                                <input id="dateRechercheF_14_1" class="form-control form-recherche" type="text"
                                       placeholder="" name="end" style="text-align: left" value=""
                                       />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button class="btn btn-primary pull-right btn-valid-recherche-due"><i
                                    class="fa  fa-search "></i> Rechercher
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="well">
            <button class="btn btn-primary btn-sel-all">Tout sélectionner</button>
            <button class="btn btn-primary btn-unsel-all">Tout désélectionner</button>
            <button class="btn bg bg-green pull-right btn-export-due">Exporter les DUE</button>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableDUE_14_1">
    <thead>
    <tr>
        <th>Id Contrat</th>
        <th>Intervenant</th>
        <th>Numéro SS</th>
        <th>Date naissance</th>
        <th>Ville Naissance</th>
        <th>Dep. Naissance</th>
        <th>Date DUE</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>