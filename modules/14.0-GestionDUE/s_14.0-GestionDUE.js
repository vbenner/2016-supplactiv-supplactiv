var tableDUE_14_1, dateRechercheD_14_1, dateRechercheF_14_1;

jQuery(document).ready(function() {
    ClassDUE.init();
});

var ClassDUE = {
    init: function () {

        this.rechercheSessionModule();

        this.initDateRecherche();

        this.initTableDUE();

        this.initButton();

    },


    initButton: function () {
        $('.btn-valid-recherche-due').bind('click', function () {
            if($('#dateRechercheD_14_1').val() != '' && $('#dateRechercheF_14_1').val() != ''){
                ClassDUE.saveSearchToSession();
            }else toastr.error("Les dates de recherche sont incorrectes", "ERREUR - Recherche par date");
        });

        /** Tout selectionner */
        $('.btn-sel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", true);
            });
        });

        /** Tout deselectionner */
        $('.btn-unsel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", false);
            });
        });

        /** Exporter les DUE */
        $('.btn-export-due').bind('click', function (){

            /** On desactive le bouton */
            $(".btn-export-due").hide();

            var listeContrat = new Array();
            $(".chk").each(function(){
                if($(this).is(":checked")){
                    listeContrat.push($(this).attr('id'));
                }
            });


            /** On export uniquement s'il y a des Contrats sélectionnés */
            if(listeContrat.length > 0){

                toastr.info("Veuillez patienter, cr&eacute;ation du fichier en cours..", "Exportation des DUE");
                $.ajax({
                    async:false,
                    url: "includes/functions/web2bdd/func.14.1-exportDUE.php",
                    type: "POST",
                    data: {
                        listeContrat_14_1 : listeContrat
                    },
                    dataType: 'json'
                }).done(function( data ) {
                    document.location.href = 'download/func.14.1-exportDUE.php';
                    tableDUE_14_1.api().ajax.reload();
                    $(".btn-export-due").show();
                    toastr.success("Fichier DUE crée : "+data.totalOK+" contrat(s) sélectionné(s)", "Exportation des DUE");

                });

            }else {
                $(".btn-export-due").show();
                toastr.error("Impossible de cr&eacute;er le fichier d'export, vous devez s&eacute;lectionner au moins 1 contrat !", "ERREUR - Exportation des DUE");
            }

        });
    },

    rechercheSessionModule: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: {
                idModule: '14_1',
                dateRechercheD_14_1: '',
                dateRechercheF_14_1: ''
            },
            dataType: "json"
        }).done(function (data) {
            $('#dateRechercheD_14_1').val(data.sessions.dateRechercheD_14_1);
            $('#dateRechercheF_14_1').val(data.sessions.dateRechercheF_14_1);
        });
    },

    initDateRecherche: function () {
        $('.input-daterange').datepicker();
    },

    saveSearchToSession: function () {

        if (dateRechercheD_14_1 != $('#dateRechercheD_14_1').val() || dateRechercheF_14_1 != $('#dateRechercheF_14_1').val()) {
            dateRechercheD_14_1 = $('#dateRechercheD_14_1').val();
            dateRechercheF_14_1 = $('#dateRechercheF_14_1').val();

            $.ajax({
                async:false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    dateRechercheD_14_1: dateRechercheD_14_1,
                    dateRechercheF_14_1: dateRechercheF_14_1
                },
                dataType: 'json'
            }).done(function (data) {

                tableDUE_14_1.api().ajax.reload();
            });
        }
    },

    initTableDUE : function(){
        tableDUE_14_1 = $('#tableDUE_14_1').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": -1,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.14.1-listeDUE.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableDUE_14_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    }
}