<h1 class="bold"> Evolution du CA</h1>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Campagne :</label>
                                    <select id="lstFiltreCampagne" name="lstFiltreCampagne" class="form-control select2"
                                    >
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
//$year = date('Y');
?>
<table class="table table-striped table-bordered table-hover" id="tableEvolCAY" style="display: none">
    <thead>
    <tr>
        <th></th>
        <th></th>

        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

        <?php
        #for ($i = 4 ; $i >= 0 ; $i--) {
        #    echo '<th>'.($year-$i).'</th>';
        #}
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<table class="table table-striped table-bordered table-hover" id="tableEvolCAS" style="display: none">
    <thead>
    <tr>
        <th></th>
        <th></th>

        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

        <?php
        #for ($i = 4 ; $i >= 0 ; $i--) {
        #    for ($j = 1 ; $j <= 2 ; $j++) {
        #        echo '<th>S'.$j.'/'.($year-$i).'</th>';
        #    }
        #}
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<table class="table table-striped table-bordered table-hover" id="tableEvolCAT" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>

        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

        <?php
        #for ($i = 4 ; $i >= 0 ; $i--) {
        #    for ($j = 1 ; $j <= 4 ; $j++) {
        #        echo '<th>T'.$j.'/'.substr(($year-$i), 2, 2).'</th>';
        #    }
        #}
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<br/>
<table class="table table-striped table-bordered table-hover" id="tableEvolCAM" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>

        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

        <?php
        #for ($i = 1 ; $i >= 0 ; $i--) {
        #    for ($j = 1 ; $j <= 12 ; $j++) {
        #        echo '<th>'.str_pad($j, 2, '0', STR_PAD_LEFT).'/'.substr(($year-$i), 2, 2).'</th>';
        #    }
        #}
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
