/** -----------------------------------------------------------------------------------------------
 * Trois tableaux
 * Semestre / Trimestre / Mois
 */
var tableEvolCAY = null;
var tableEvolCAS = null;
var tableEvolCAT = null;
var tableEvolCAM = null;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    clearTables: function () {
        if (tableEvolCAY != undefined) {
            $('#tableEvolCAY').dataTable().fnClearTable();
            $('#tableEvolCAY').dataTable().fnDestroy();
        }

        if (tableEvolCAS != null) {
            $('#tableEvolCAS').dataTable().fnClearTable();
            $('#tableEvolCAS').dataTable().fnDestroy();
        }

        if (tableEvolCAT != null) {
            $('#tableEvolCAT').dataTable().fnClearTable();
            $('#tableEvolCAT').dataTable().fnDestroy();
        }

        if (tableEvolCAM != null) {
            $('#tableEvolCAM').dataTable().fnClearTable();
            $('#tableEvolCAM').dataTable().fnDestroy();
        }
    },

    bindClear: function () {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function () {
            $("#lstFiltreCampagne").select2("val", "").trigger('change');

            ClassStats.clearTables();
            $('#tableEvolCAY').hide();
            $('#tableEvolCAS').hide();
            $('#tableEvolCAT').hide();
            $('#tableEvolCAM').hide();
        });


    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER
         */
        $('#btnSearch').on('click', function () {


            ClassStats.clearTables();

            /** -----------------------------------------------------------------------------------
             * On récupère 3 listes, QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionCA.php",
                type: "POST",
                data: {
                    campagne: $('#lstFiltreCampagne').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var year = data.YEAR;

                    var M = data.DATAM;
                    var T = data.DATAT;
                    var S = data.DATAS;
                    var Y = data.DATAY;

                    /** ---------------------------------------------------------------------------
                     * GESTION DES ANNEES
                     */
                    var table = 'tableEvolCAY';
                    var tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    var lig1g = '<tr>' + '<td>_INTERVENTIONS</td><td>nb (j)</td>';
                    var lig2g = '<tr>' + '<td>_INTERVENTIONS</td><td>CA (€)</td>';
                    var lig3g = '<tr>' + '<td>_INTERVENTIONS</td><td>Évolution (%) (N -1)</td>';

                    var lig1a = '<tr>' + '<td>ANIMATIONS</td><td>nb (j)</td>';
                    var lig2a = '<tr>' + '<td>ANIMATIONS</td><td>CA (€)</td>';
                    var lig3a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (N -1)</td>';
                    //var lig4a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (y-1)</td>';

                    var lig1f = '<tr>' + '<td>FORMATIONS</td><td>nb (j)</td>';
                    var lig2f = '<tr>' + '<td>FORMATIONS</td><td>CA (€)</td>';
                    var lig3f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (N -1)</td>';
                    //var lig4f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (y-1)</td>';

                    $.each(Y, function (index, item) {

                        /** -----------------------------------------------------------------------
                         * De haut en bas :
                         *
                         * NB
                         * TX (i-1)
                         * TX (y-1)
                         */
                        lig1g += '<td>' + item.nb + '</td>';
                        lig2g += '<td>' + item.ca + '</td>';
                        lig3g += '<td>' + (item.txy != undefined ? item.txy : '-') + '</td>';

                        lig1a += '<td>' + item.nb_a + '</td>';
                        lig2a += '<td>' + item.ca_a + '</td>';
                        lig3a += '<td>' + (item.txy_a != undefined ? item.txy_a : '-') + '</td>';
                        //lig4a += '<td>' + item.txy_a + '</td>';

                        lig1f += '<td>' + item.nb_f + '</td>';
                        lig2f += '<td>' + item.ca_f + '</td>';
                        lig3f += '<td>' + (item.txy_f != undefined ? item.txy_f : '-') + '</td>';
                        //lig4f += '<td>' + item.txy_f + '</td>';
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    lig3g += '</tr>';

                    lig1a += '</tr>';
                    lig2a += '</tr>';
                    lig3a += '</tr>';
                    //lig4a += '</tr>';

                    lig1f += '</tr>';
                    lig2f += '</tr>';
                    lig3f += '</tr>';
                    //lig4f += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    $('#' + table + ' tbody').append(lig3g);

                    $('#' + table + ' tbody').append(lig1a);
                    $('#' + table + ' tbody').append(lig2a);
                    $('#' + table + ' tbody').append(lig3a);
                    //$('#' + table + ' tbody').append(lig4a);

                    $('#' + table + ' tbody').append(lig1f);
                    $('#' + table + ' tbody').append(lig2f);
                    $('#' + table + ' tbody').append(lig3f);
                    //$('#' + table + ' tbody').append(lig4f);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES SEMESTRES
                     */
                    table = 'tableEvolCAS';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>S1/' + i + '</th>';
                        tr += '<th>S2/' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_INTERVENTIONS</td><td>nb (j)</td>';
                    lig2g = '<tr>' + '<td>_INTERVENTIONS</td><td>CA (€)</td>';
                    lig3g = '<tr>' + '<td>_INTERVENTIONS</td><td>Évolution (%) (N -1)</td>';

                    lig1a = '<tr>' + '<td>ANIMATIONS</td><td>nb (j)</td>';
                    lig2a = '<tr>' + '<td>ANIMATIONS</td><td>CA (€)</td>';
                    lig3a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (N -1)</td>';

                    lig1f = '<tr>' + '<td>FORMATIONS</td><td>nb (j)</td>';
                    lig2f = '<tr>' + '<td>FORMATIONS</td><td>CA (€)</td>';
                    lig3f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (N -1)</td>';

                    $.each(S, function (index, item) {

                        var year = item;
                        $.each(year, function (index, subitem) {

                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            lig2g += '<td>' + subitem.ca + '</td>';
                            lig3g += '<td>' + (subitem.txy != undefined ? subitem.txy : '-') + '</td>';

                            lig1a += '<td>' + subitem.nb_a + '</td>';
                            lig2a += '<td>' + subitem.ca_a + '</td>';
                            lig3a += '<td>' + subitem.txy_a + '</td>';

                            lig1f += '<td>' + subitem.nb_f + '</td>';
                            lig2f += '<td>' + subitem.ca_f + '</td>';
                            lig3f += '<td>' + subitem.txy_f + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    lig3g += '</tr>';

                    lig1a += '</tr>';
                    lig2a += '</tr>';
                    lig3a += '</tr>';

                    lig1f += '</tr>';
                    lig2f += '</tr>';
                    lig3f += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    $('#' + table + ' tbody').append(lig3g);

                    $('#' + table + ' tbody').append(lig1a);
                    $('#' + table + ' tbody').append(lig2a);
                    $('#' + table + ' tbody').append(lig3a);
                    //$('#' + table + ' tbody').append(lig4a);

                    $('#' + table + ' tbody').append(lig1f);
                    $('#' + table + ' tbody').append(lig2f);
                    $('#' + table + ' tbody').append(lig3f);
                    //$('#' + table + ' tbody').append(lig4f);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES TRIMESTRES
                     */
                    table = 'tableEvolCAT';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>T1/' + i + '</th>';
                        tr += '<th>T2/' + i + '</th>';
                        tr += '<th>T3/' + i + '</th>';
                        tr += '<th>T4/' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_INTERVENTIONS</td><td>nb (j)</td>';
                    lig2g = '<tr>' + '<td>_INTERVENTIONS</td><td>CA (€)</td>';
                    lig3g = '<tr>' + '<td>_INTERVENTIONS</td><td>Évolution (%) (N -1)</td>';

                    lig1a = '<tr>' + '<td>ANIMATIONS</td><td>nb (j)</td>';
                    lig2a = '<tr>' + '<td>ANIMATIONS</td><td>CA (€)</td>';
                    lig3a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (N -1)</td>';

                    lig1f = '<tr>' + '<td>FORMATIONS</td><td>nb (j)</td>';
                    lig2f = '<tr>' + '<td>FORMATIONS</td><td>CA (€)</td>';
                    lig3f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (N -1)</td>';

                    $.each(T, function (index, item) {

                        var year = item;
                        $.each(year, function (index, subitem) {

                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            lig2g += '<td>' + subitem.ca + '</td>';
                            lig3g += '<td>' + (subitem.txy != undefined ? subitem.txy : '-')  + '</td>';

                            lig1a += '<td>' + subitem.nb_a + '</td>';
                            lig2a += '<td>' + subitem.ca_a + '</td>';
                            lig3a += '<td>' + subitem.txy_a + '</td>';
                            //lig4a += '<td>' + subitem.txy_a + '</td>';

                            lig1f += '<td>' + subitem.nb_f + '</td>';
                            lig2f += '<td>' + subitem.ca_f + '</td>';
                            lig3f += '<td>' + subitem.txy_f + '</td>';
                            //lig4f += '<td>' + subitem.txy_f + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    lig3g += '</tr>';

                    lig1a += '</tr>';
                    lig2a += '</tr>';
                    lig3a += '</tr>';
                    //lig4a += '</tr>';

                    lig1f += '</tr>';
                    lig2f += '</tr>';
                    lig3f += '</tr>';
                    //lig4f += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    $('#' + table + ' tbody').append(lig3g);

                    $('#' + table + ' tbody').append(lig1a);
                    $('#' + table + ' tbody').append(lig2a);
                    $('#' + table + ' tbody').append(lig3a);
                    //$('#' + table + ' tbody').append(lig4a);

                    $('#' + table + ' tbody').append(lig1f);
                    $('#' + table + ' tbody').append(lig2f);
                    $('#' + table + ' tbody').append(lig3f);
                    //$('#' + table + ' tbody').append(lig4f);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES MOIS
                     */
                    table = 'tableEvolCAM';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 1; i <= year; i++) {
                        tr += '<th>01/' + i + '</th>';
                        tr += '<th>02/' + i + '</th>';
                        tr += '<th>03/' + i + '</th>';
                        tr += '<th>04/' + i + '</th>';
                        tr += '<th>05/' + i + '</th>';
                        tr += '<th>06/' + i + '</th>';
                        tr += '<th>07/' + i + '</th>';
                        tr += '<th>08/' + i + '</th>';
                        tr += '<th>09/' + i + '</th>';
                        tr += '<th>10/' + i + '</th>';
                        tr += '<th>11/' + i + '</th>';
                        tr += '<th>12/' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_INTERVENTIONS</td><td>nb (j)</td>';
                    lig2g = '<tr>' + '<td>_INTERVENTIONS</td><td>CA (€)</td>';
                    lig3g = '<tr>' + '<td>_INTERVENTIONS</td><td>Évolution (%) (N -1)</td>';

                    lig1a = '<tr>' + '<td>ANIMATIONS</td><td>nb (j)</td>';
                    lig2a = '<tr>' + '<td>ANIMATIONS</td><td>CA (€)</td>';
                    lig3a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (N -1)</td>';
                    //lig4a = '<tr>' + '<td>ANIMATIONS</td><td>Évolution (%) (y-1)</td>';

                    lig1f = '<tr>' + '<td>FORMATIONS</td><td>nb (j)</td>';
                    lig2f = '<tr>' + '<td>FORMATIONS</td><td>CA (€)</td>';
                    lig3f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (N -1)</td>';
                    //lig4f = '<tr>' + '<td>FORMATIONS</td><td>Évolution (%) (y-1)</td>';

                    $.each(M, function (index, item) {

                        var year = item;
                        $.each(year, function (index, subitem) {

                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            lig2g += '<td>' + subitem.ca + '</td>';
                            lig3g += '<td>' + (subitem.txy != undefined ? subitem.txy : '-')  + '</td>';

                            lig1a += '<td>' + subitem.nb_a + '</td>';
                            lig2a += '<td>' + subitem.ca_a + '</td>';
                            lig3a += '<td>' + subitem.txy_a + '</td>';
                            //lig4a += '<td>' + subitem.txy_a + '</td>';

                            lig1f += '<td>' + subitem.nb_f + '</td>';
                            lig2f += '<td>' + subitem.ca_f + '</td>';
                            lig3f += '<td>' + subitem.txy_f + '</td>';
                            //lig4f += '<td>' + subitem.txy_f + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    lig3g += '</tr>';

                    lig1a += '</tr>';
                    lig2a += '</tr>';
                    lig3a += '</tr>';
                    //lig4a += '</tr>';

                    lig1f += '</tr>';
                    lig2f += '</tr>';
                    lig3f += '</tr>';
                    //lig4f += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    $('#' + table + ' tbody').append(lig3g);

                    $('#' + table + ' tbody').append(lig1a);
                    $('#' + table + ' tbody').append(lig2a);
                    $('#' + table + ' tbody').append(lig3a);
                    //$('#' + table + ' tbody').append(lig4a);

                    $('#' + table + ' tbody').append(lig1f);
                    $('#' + table + ' tbody').append(lig2f);
                    $('#' + table + ' tbody').append(lig3f);
                    //$('#' + table + ' tbody').append(lig4f);


                    /** ---------------------------------------------------------------------------
                     * DATATABLISATION DES TABLES
                     */
//return true;

                    var groupColumn = 0;
                    tableEvolCAY = $('#tableEvolCAY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="7">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolCAS = $('#tableEvolCAS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="11">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolCAT = $('#tableEvolCAT').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="21">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolCAM = $('#tableEvolCAM').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="25">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    $('#tableEvolCAY').show();
                    $('#tableEvolCAS').show();
                    $('#tableEvolCAT').show();
                    $('#tableEvolCAM').show();

                    return true;

                }
            });
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {
        ClassStats.initCampagne();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function () {
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                //var option = $('<option>').attr('value', 0).html('Sélectionner une campagne');
                //$('#lstFiltreCampagne').append(option);
                $('#lstFiltreCampagne').append('<option value=""></option>');
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
        //$('#lstFiltreCampagne').html('<option></option>').trigger('change');
    },

}