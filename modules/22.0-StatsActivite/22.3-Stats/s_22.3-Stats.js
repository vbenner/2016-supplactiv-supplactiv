/** -----------------------------------------------------------------------------------------------
 * Trois tableaux
 * Semestre / Trimestre / Mois
 */
var tableResult = null;
var tableResultS = null;
var tableResultT = null;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    bindClear: function () {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function () {
            $("#lstFiltreMission").select2("val", "").trigger('change');
            $("#lstFiltreCampagne").select2("val", "").trigger('change');
            ClassStats.initMission(0);

            if (tableResult != null) {
                $('#tableResult').dataTable().fnClearTable();
                $('#tableResult').dataTable().fnDestroy();
            }

            if (tableResultT != null) {
                $('#tableResultT').dataTable().fnClearTable();
                $('#tableResultT').dataTable().fnDestroy();
            }

            if (tableResultS != null) {
                $('#tableResultS').dataTable().fnClearTable();
                $('#tableResultS').dataTable().fnDestroy();
            }
        });


    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER
         */
        $('#btnSearch').on('click', function () {

            //var dateDeb = $('#dateDeb').val();
            //var dateFin = $('#dateFin').val();

            /** -----------------------------------------------------------------------------------
             * Minimum SYNDICAL : la MISSION
             */
            if ($('#lstFiltreCampagne').val() == '') {
                toastr.error('Il faut sélectionner au minimum la campagne');
                return false;
            }

            /** -----------------------------------------------------------------------------------
             * On récupère 3 listes, QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.interventionCampagne.php",
                type: "POST",
                data: {
                    campagne: $('#lstFiltreCampagne').val(),
                    mission: $('#lstFiltreMission').val(),
                    //from : $('#dateDeb').val(),
                    //to: $('#dateFin').val(),
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * MOIS
                     *
                     * --> on compare bien avec l'année précédente
                     */
                    var year = data.YEAR;
                    var mois = ['JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN', 'JUIL',
                        'AOUT', 'SEPT', 'OCT', 'NOV', 'DEC'];

                    var dataSet = [];
                    data: [{
                        name: 'Point 1',
                        color: '#00FF00',
                        y: 0
                    }, {
                        name: 'Point 2',
                        color: '#FF00FF',
                        y: 5
                    }]
                    var month = data.DATAM;
                    var totYear = new Array();
                    totYear[0] = 0;
                    totYear[1] = 0;
                    var nb = new Array();
                    nb[0] = '';
                    nb[1] = '';
                    var tx = '';
                    for (i = 1; i <= 12 ; i++) {
                        var m = [];
                        for (j = 0 ; j <= 1 ; j++) {

                            if (month[j][i] != undefined) {
                                nb[j] += '<td>'+month[j][i]+'</td>';
                                m[j] = month[j][i];

                                totYear[j] += (parseInt(month[j][i]));

                            }
                            else {
                                nb[j] += '<td></td>';
                                m[j] = 0;
                            }
                        }
                        dataSet.push({
                            "month": mois[i-1],
                            "y0": parseInt(m[0]),
                            "y1": parseInt(m[1])
                        });
                    }

console.log(dataSet);

                    for (i = 1; i <= 12; i++) {
                        var ratio = (month[1][i] != undefined && month[1][i] != 0 ? 100 * (month[1][i] - month[0][i]) / month[0][i] : '-');
                        if (ratio != '-') {
                            ratio = Math.round(ratio * 100) / 100;
                        }
                        tx += '<td>' + ratio + '</td>';
                    }

                    nb[0] = nb[0] + '<td>' + totYear [0] + '</td>';
                    nb[1] = nb[1] + '<td>' + totYear [1] + '</td>';

                    if (tableResult != null) {
                        $('#tableResult').dataTable().fnClearTable();
                        $('#tableResult').dataTable().fnDestroy();
                    }
                    $('#tableResult tbody').append('<tr><td>Évolution (%) (N -1)</td>' + tx + '<td></td></tr>');
                    $('#tableResult tbody').append('<tr><td>' + year + '</td>' + nb[1] + '</tr>');
                    $('#tableResult tbody').append('<tr><td>'+ (year - 1) + '</td>' + nb[0] + '</tr>');
                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * TRIMESTRES
                     */
                    month = data.DATAT;
                    nb = new Array();
                    nb[0] = '';
                    nb[1] = '';
                    tx = '';
                    for (i = 1; i <= 4; i++) {
                        for (j = 0; j <= 1; j++) {
                            if (month[j][i] != undefined) {
                                nb[j] += '<td>' + month[j][i] + '</td>';
                            }
                            else {
                                nb[j] += '<td></td>';
                            }
                        }
                    }
                    for (i = 1; i <= 4; i++) {
                        var ratio = (month[1][i] != undefined && month[1][i] != 0 ? 100 * (month[1][i] - month[0][i]) / month[0][i] : '-');
                        if (ratio != '-') {
                            ratio = Math.round(ratio * 100) / 100;
                        }
                        tx += '<td>' + ratio + '</td>';
                    }
                    nb[0] = nb[0] + '<td>' + totYear [0] + '</td>';
                    nb[1] = nb[1] + '<td>' + totYear [1] + '</td>';
                    tx = tx + '<td></td>';
                    if (tableResultT != null) {
                        $('#tableResultT').dataTable().fnClearTable();
                        $('#tableResultT').dataTable().fnDestroy();
                    }
                    $('#tableResultT tbody').append('<tr><td>Évolution (%) (N -1)</td>' + tx + '</tr>');
                    $('#tableResultT tbody').append('<tr><td>' + year + '</td>' + nb[1] + '</tr>');
                    $('#tableResultT tbody').append('<tr><td>' + (year - 1)+ '</td>' + nb[0] + '</tr>');
                    tableResultT = $('#tableResultT').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * SEMESTRE
                     */
                    month = data.DATAS;
                    nb = new Array();
                    nb[0] = '';
                    nb[1] = '';
                    tx = '';
                    for (i = 1; i <= 2; i++) {
                        for (j = 0; j <= 1; j++) {
                            if (month[j][i] != undefined) {
                                nb[j] += '<td>' + month[j][i] + '</td>';
                            }
                            else {
                                nb[j] += '<td></td>';
                            }
                        }
                    }

                    for (i = 1; i <= 2; i++) {
                        var ratio = (month[1][i] != undefined && month[1][i] != 0 ? 100 * (month[1][i] - month[0][i]) / month[0][i] : '-');
                        if (ratio != '-') {
                            ratio = Math.round(ratio * 100) / 100;
                        }
                        tx += '<td>' + ratio + '</td>';
                    }
                    tx = tx + '<td></td>';
                    nb[0] = nb[0] + '<td>' + totYear [0] + '</td>';
                    nb[1] = nb[1] + '<td>' + totYear [1] + '</td>';
                    if (tableResultS != null) {
                        $('#tableResultS').dataTable().fnClearTable();
                        $('#tableResultS').dataTable().fnDestroy();
                    }
                    $('#tableResultS tbody').append('<tr><td>Évolution (%) (N -1)</td>' + tx + '</tr>');
                    $('#tableResultS tbody').append('<tr><td>' + year + '</td>' + nb[1] + '</tr>');
                    $('#tableResultS tbody').append('<tr><td>' + (year - 1) + '</td>' + nb[0] + '</tr>');
                    tableResultS = $('#tableResultS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * Affichage du graphique
                     */
                    AmCharts.translations["export"]["en"]["menu.label.save.image"] = "Enregistrer Image...";
                    AmCharts.translations["export"]["en"]["menu.label.save.data"] = "Exporter Données";
                    AmCharts.translations["export"]["en"]["menu.label.draw"] = "Annoter";
                    AmCharts.translations["export"]["en"]["menu.label.print"] = "Imprimer";

                    var chart = AmCharts.makeChart("chartdiv", {
                        "type": "serial",
                        "titles": [
                            {
                                "text": "Nombre d'Interventions",
                                "size": 15
                            }
                        ],
                        "theme": "light",
                        "marginRight": 40,
                        "marginLeft": 40,
                        "autoMarginOffset": 20,
                        "dataDateFormat": "YYYY-MM",
                        "legend": {
                            "horizontalGap": 10,
                            "maxColumns": 1,
                            "position": "right",
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": dataSet,
                        "valueAxes": [{
                            "id": "g1",
                            "axisAlpha": 0,
                            "position": "left",
                            "ignoreAxisWidth":true
                        }],
                        "graphs": [
                            {
                                "id": "g1",
                                "balloon":{
                                    "drop":true,
                                    "adjustBorderColor":false,
                                    "color":"#ffffff"
                                },
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "title": year,
                                "useLineColorForBulletBorder": true,
                                "valueField": "y1",
                                "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
                            },
                            {
                                "id": "g2",
                                "balloon":{
                                    "drop":true,
                                    "adjustBorderColor":false,
                                    "color":"#ffffff"
                                },
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "title": (year-1),
                                "useLineColorForBulletBorder": true,
                                "valueField": "y0",
                                "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
                            },
                        ],

                        /*
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha":1,
                            "cursorColor":"#258cbb",
                            "limitToGraph":"g1",
                            "valueLineAlpha":0.2,
                            "valueZoomable":true
                        },
                        */
                        /*
                        "valueScrollbar":{
                            "oppositeAxis":false,
                            "offset":50,
                            "scrollbarHeight":10
                        },
                        */
                        "categoryField": "month",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            //"position": "left"
                        },
                        "export": {
                            "enabled": true
                        }

                    });
                    chart.addListener("rendered", zoomChart);

                    zoomChart();

                    function zoomChart() {
                        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
                    }

                    return true;

                }
            });
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {
        ClassStats.initCampagne();
        ClassStats.initMission(0);

        /** ---------------------------------------------------------------------------------------
         * Bind Changes
         */
        $('#lstFiltreCampagne').on('change', function () {
            if ($(this).val() == '') {
                ClassStats.initMission(0);
            }
            else {
                ClassStats.initMission($(this).val());
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function () {
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#lstFiltreCampagne').append('<option value=""></option>');
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
                //$('#lstFiltreCampagne').html('<option></option>').trigger('change');
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des missions
     */
    initMission: function (idCampagne) {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeMissions.php",
            type: "POST",
            data: {
                id: idCampagne
            },
            dataType: 'json',
            success: function (data) {
                if (idCampagne != 0) {
                    $('#lstFiltreMission').empty();
                }
                if (idCampagne != 0) {
                    $('#lstFiltreMission').html('<option></option>');
                    $.each(data.items, function (index, item) {
                        var option =
                            $('<option>')
                                .attr('value', item.idMission)
                                .html(item.libelleMission)

                        ;
                        $('#lstFiltreMission').append(option);
                    });
                    $('#lstFiltreMission').trigger('change');
                }
            }
        });
    },

}