<h1 class="bold"> Interventions / Campagne</h1>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="form-body">
                        <div class="alert alert-warning">
                            <strong>Attention</strong>
                            Pour les animations, les données sont exprimées en jours. Pour les formations, on compte en nombre d'interventions.
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Campagne :</label>
                                    <select id="lstFiltreCampagne" name="lstFiltreCampagne" class="form-control select2"
                                    >
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mission :</label>
                                    <select id="lstFiltreMission" name="lstFiltreMission"
                                            class="form-control select2" >
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!--
                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="dateDeb" style="text-align: left"
                                               value="01/01/2018"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="dateFin" style="text-align: left" value="31/12/2018"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->

                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$year = date('Y');
?>
<table class="table table-striped table-bordered table-hover" id="tableResultS" style="width: 100%;">
    <thead>
    <tr>
        <th></th>
        <th>Semestre 1</th>
        <th>Semestre 2</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<table class="table table-striped table-bordered table-hover" id="tableResultT" >
    <thead>
    <tr>
        <th></th>
        <th>Trimestre 1</th>
        <th>Trimestre 2</th>
        <th>Trimestre 3</th>
        <th>Trimestre 4</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<table class="table table-striped table-bordered table-hover" id="tableResult" >
    <thead>
    <tr>
        <th></th>
        <th>JAN</th>
        <th>FEV</th>
        <th>MARS</th>
        <th>AVR</th>
        <th>MAI</th>
        <th>JUIN</th>
        <th>JUIL</th>
        <th>AOUT</th>
        <th>SEP</th>
        <th>OCT</th>
        <th>NOV</th>
        <th>DEC</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<div id="chartdiv" style="width: 100%;height: 500px;"></div>
