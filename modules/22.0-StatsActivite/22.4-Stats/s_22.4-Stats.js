/** -----------------------------------------------------------------------------------------------
 * Trois tableaux
 * Semestre / Trimestre / Mois
 */
var tableResultP = null;
var tableResultM = null;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
        $('.input-daterange').datepicker();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    bindClear: function () {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function () {
            if (tableResultP != null) {
                $('#tableResultP').dataTable().fnClearTable();
                $('#tableResultP').dataTable().fnDestroy();
            }

            if (tableResultM != null) {
                $('#tableResultM').dataTable().fnClearTable();
                $('#tableResultM').dataTable().fnDestroy();
            }
        });


    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER
         */
        $('#btnSearch').on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Minimum SYNDICAL : les dates
             */
            if ($('#dateDeb').val() == '' || $('#dateFin').val() == '') {
                toastr.error('Les dates doivent être indiquées.');
                return false;
            }

            /** -----------------------------------------------------------------------------------
             * On récupère 3 listes, QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.pdvAnime.php",
                type: "POST",
                data: {
                    from: $('#dateDeb').val(),
                    to: $('#dateFin').val(),
                    limit: $('#lstLimit').val()
                },
                dataType: 'json',
                success: function (data) {

                    if (tableResultP != null) {
                        $('#tableResultP').dataTable().fnClearTable();
                        $('#tableResultP').dataTable().fnDestroy();
                        $('#tableResultM').dataTable().fnClearTable();
                        $('#tableResultM').dataTable().fnDestroy();
                        tableResultP = null;
                        tableResultM = null;
                        $('#tableResultP tbody').empty();
                        $('#tableResultM tbody').empty();
                    }

                    var plus = data.PLUS;
                    var moins = data.MOINS;
                    plus.forEach(function(element) {
                        $('#tableResultP tbody').append(
                            '<tr>' +
                            '<td>' + element.NOM + '</td>' +
                            '<td>' + element.ADR + '</td>' +
                            '<td>' + element.QTE + '</td>' +
                            '</tr>'
                        );
                    });
                    moins.forEach(function(element) {
                        $('#tableResultM tbody').append(
                            '<tr>' +
                            '<td>' + element.NOM + '</td>' +
                            '<td>' + element.ADR + '</td>' +
                            '<td>' + element.QTE + '</td>' +
                            '</tr>'
                        );
                    });

                    /** ---------------------------------------------------------------------------
                     * PDV Les PLUS Animés et les MOINS
                     */
                    tableResultM = $('#tableResultM').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "order": [
                            [2, 'asc']
                        ],
                    });
                    tableResultP = $('#tableResultP').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "order": [
                            [2, 'desc']
                        ],
                    });

                    return true;
                }
            });
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {

        /** ---------------------------------------------------------------------------------------
         * Limite
         */
        var limites = [[0, 'ALL'], [5, '5'], [10, '10'], [25, '25'], [50, '50']];
        limites.forEach(function(element) {
            console.log(element[0] + ' -> ' + element[1]);
            var option = $('<option>')
                .attr('value', element[0])
                .html(element[1]);
            $('#lstLimit').append(option);
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function () {
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
        ClassStats.initDates();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                //var option = $('<option>').attr('value', 0).html('Sélectionner une campagne');
                //$('#lstFiltreCampagne').append(option);
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
        $('#lstFiltreCampagne').html('<option></option>').trigger('change');
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des missions
     */
    initMission: function (idCampagne) {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeMissions.php",
            type: "POST",
            data: {
                id: idCampagne
            },
            dataType: 'json',
            success: function (data) {
                if (idCampagne != 0) {
                    $('#lstFiltreMission').empty();
                }
                if (idCampagne != 0) {
                    $('#lstFiltreMission').html('<option></option>');
                    $.each(data.items, function (index, item) {
                        var option =
                            $('<option>')
                                .attr('value', item.idMission)
                                .html(item.libelleMission)

                        ;
                        $('#lstFiltreMission').append(option);
                    });
                    $('#lstFiltreMission').trigger('change');
                }
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des dates du 1er janvier au 31 décemblre
     */
    initDates:function() {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), 0, 1);
        var lastDay = new Date(date.getFullYear(), 11, 31);
        $('#dateDeb').val(firstDay.toLocaleDateString());
        $('#dateFin').val(lastDay.toLocaleDateString());
    }

}