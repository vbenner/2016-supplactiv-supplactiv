<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="start" style="text-align: left"
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="end" style="text-align: left" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Limite :</label>
                                    <select id="lstLimit" name="lstLimit" class="form-control select2">
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right"
                                    style="margin-right: 5px;">Effacer
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$year = date('Y');
?>
<div class="row" style="margin: 0px;">
    <div class="col-md-6">
        <h1>PDV les plus animés</h1>
        <table class="table table-striped table-bordered table-hover" id="tableResultP">
            <thead>
            <tr>
                <th>NOM</th>
                <th>CP/ VILLE</th>
                <th>Interventions</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h1>PDV les moins animés</h1>
        <table class="table table-striped table-bordered table-hover" id="tableResultM">
            <thead>
            <tr>
                <th>NOM</th>
                <th>CP/ VILL E</th>
                <th>Interventions</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
