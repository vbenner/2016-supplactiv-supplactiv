/** -----------------------------------------------------------------------------------------------
 * Trois tableaux
 * Semestre / Trimestre / Mois
 */
var tableEvolCampagneY = undefined;
var tableEvolMissionY = undefined;

var tableEvolInterventionY = undefined;
var tableEvolInterventionS = undefined;
var tableEvolInterventionT = undefined;
var tableEvolInterventionM = undefined;

// ------------------------------------------------------------------------------------------------
// On dissocie les 2 trucs du haut en Animation et Formation
// ------------------------------------------------------------------------------------------------
var tableEvolInterventionAY = undefined;
var tableEvolInterventionAS = undefined;
var tableEvolInterventionAT = undefined;
var tableEvolInterventionAM = undefined;

var tableEvolInterventionFY = undefined;
var tableEvolInterventionFS = undefined;
var tableEvolInterventionFT = undefined;
var tableEvolInterventionFM = undefined;

$().ready(function () {
    ClassStats.init();
    //ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        //ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    bindClear: function () {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function () {
            if (tableEvolCampagneY != undefined) {
                $('#tableEvolCampagneY').dataTable().fnClearTable();
                $('#tableEvolCampagneY').dataTable().fnDestroy();
                $('#tableEvolCampagneY tbody').empty();
                tableEvolCampagneY = undefined;
            }

            if (tableEvolMissionY != undefined) {
                $('#tableEvolMissionY').dataTable().fnClearTable();
                $('#tableEvolMissionY').dataTable().fnDestroy();
                $('#tableEvolMissionY tbody').empty();
                tableEvolMissionY = undefined;
            }

            if (tableEvolInterventionY != undefined) {
                $('#tableEvolInterventionY').dataTable().fnClearTable();
                $('#tableEvolInterventionY').dataTable().fnDestroy();
                $('#tableEvolInterventionY tbody').empty();
                tableEvolInterventionY = undefined;
            }

            if (tableEvolInterventionS != undefined) {
                $('#tableEvolInterventionS').dataTable().fnClearTable();
                $('#tableEvolInterventionS').dataTable().fnDestroy();
                $('#tableEvolInterventionS tbody').empty();
                tableEvolInterventionS = undefined;
            }

            if (tableEvolInterventionT != undefined) {
                $('#tableEvolInterventionT').dataTable().fnClearTable();
                $('#tableEvolInterventionT').dataTable().fnDestroy();
                $('#tableEvolInterventionT tbody').empty();
                tableEvolInterventionT = undefined;
            }

            if (tableEvolInterventionM != undefined) {
                $('#tableEvolInterventionM').dataTable().fnClearTable();
                $('#tableEvolInterventionM').dataTable().fnDestroy();
                $('#tableEvolInterventionM tbody').empty();
                tableEvolInterventionM = undefined;
            }

            if (tableEvolInterventionAY != undefined) {
                $('#tableEvolInterventionAY').dataTable().fnClearTable();
                $('#tableEvolInterventionAY').dataTable().fnDestroy();
                $('#tableEvolInterventionAY tbody').empty();
                tableEvolInterventionAY = undefined;
            }

            if (tableEvolInterventionAS != undefined) {
                $('#tableEvolInterventionAS').dataTable().fnClearTable();
                $('#tableEvolInterventionAS').dataTable().fnDestroy();
                $('#tableEvolInterventionAS tbody').empty();
                tableEvolInterventionAS = undefined;
            }

            if (tableEvolInterventionAT != undefined) {
                $('#tableEvolInterventionAT').dataTable().fnClearTable();
                $('#tableEvolInterventionAT').dataTable().fnDestroy();
                $('#tableEvolInterventionAT tbody').empty();
                tableEvolInterventionAT = undefined;
            }

            if (tableEvolInterventionAM != undefined) {
                $('#tableEvolInterventionAM').dataTable().fnClearTable();
                $('#tableEvolInterventionAM').dataTable().fnDestroy();
                $('#tableEvolInterventionAM tbody').empty();
                tableEvolInterventionAM = undefined;
            }

            if (tableEvolInterventionFY != undefined) {
                $('#tableEvolInterventionFY').dataTable().fnClearTable();
                $('#tableEvolInterventionFY').dataTable().fnDestroy();
                $('#tableEvolInterventionFY tbody').empty();
                tableEvolInterventionFY = undefined;
            }

            if (tableEvolInterventionFS != undefined) {
                $('#tableEvolInterventionFS').dataTable().fnClearTable();
                $('#tableEvolInterventionFS').dataTable().fnDestroy();
                $('#tableEvolInterventionFS tbody').empty();
                tableEvolInterventionFS = undefined;
            }

            if (tableEvolInterventionFT != undefined) {
                $('#tableEvolInterventionFT').dataTable().fnClearTable();
                $('#tableEvolInterventionFT').dataTable().fnDestroy();
                $('#tableEvolInterventionFT tbody').empty();
                tableEvolInterventionFT = undefined;
            }

            if (tableEvolInterventionFM != undefined) {
                $('#tableEvolInterventionFM').dataTable().fnClearTable();
                $('#tableEvolInterventionFM').dataTable().fnDestroy();
                $('#tableEvolInterventionFM tbody').empty();
                tableEvolInterventionFM = undefined;
            }

            $('#tableEvolCampagneY').hide();
            $('#tableEvolMissionY').hide();
            $('#tableEvolInterventionY').hide();
            $('#tableEvolInterventionS').hide();
            $('#tableEvolInterventionT').hide();
            $('#tableEvolInterventionM').hide();
            $('#tableEvolInterventionAY').hide();
            $('#tableEvolInterventionAS').hide();
            $('#tableEvolInterventionAT').hide();
            $('#tableEvolInterventionAM').hide();
            $('#tableEvolInterventionFY').hide();
            $('#tableEvolInterventionFS').hide();
            $('#tableEvolInterventionFT').hide();
            $('#tableEvolInterventionFM').hide();

        });


    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER / genère auto le clear
         */
        $('#btnSearch').on('click', function () {

            $('#btnClear').trigger('click');

            /** -----------------------------------------------------------------------------------
             * To fusionne or not to fusionne
             */
            var dissociate = $('#dissociate').is(':checked') ? 1 : 0;

            /** -----------------------------------------------------------------------------------
             * CAMPAGNES - QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionCampagne.php",
                type: "POST",
                data: {
                    dissociate: dissociate,
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * On récupère la liste des CAMPAGNES
                     */
                    var Y = data.DATAY;
                    $.each(Y, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        //var ligneInfI = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (année -1)</td>';
                        var ligneInfY = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var years = element.years;

                        $.each(years, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        //$.each(years, function( index, tx) {
                        //    ligneInfI += '<td>' + tx.txi + '</td>';
                        //});
                        $.each(years, function( index, tx) {
                            ligneInfY += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                        });

                        ligneSup += '</tr>';
                        //ligneInfI += '</tr>';
                        ligneInfY += '</tr>';

                        $('#tableEvolCampagneY tbody').append(
                            ligneSup
                        );
                        //$('#tableEvolCampagneY tbody').append(
                        //    ligneInfI
                        //);
                        $('#tableEvolCampagneY tbody').append(
                            ligneInfY
                        );
                    });

                    var groupColumn = 0;
                    tableEvolCampagneY = $('#tableEvolCampagneY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                }
            });

            /** -----------------------------------------------------------------------------------
             * MISSIONS - On récupère 1 liste, QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionMission.php",
                type: "POST",
                data: {
                    dissociate:dissociate
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * On récupère la liste des CAMPAGNES
                     */
                    var Y = data.DATAY;
                    $.each(Y, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        //var ligneInfI = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (année -1)</td>';
                        var ligneInfY = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var years = element.years;

                        $.each(years, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        //$.each(years, function( index, tx) {
                        //    ligneInfI += '<td>' + tx.txi + '</td>';
                        //});
                        $.each(years, function( index, tx) {
                            ligneInfY += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                        });

                        ligneSup += '</tr>';
                        //ligneInfI += '</tr>';
                        ligneInfY += '</tr>';

                        $('#tableEvolMissionY tbody').append(
                            ligneSup
                        );
                        //$('#tableEvolCampagneY tbody').append(
                        //    ligneInfI
                        //);
                        $('#tableEvolMissionY tbody').append(
                            ligneInfY
                        );
                    });

                    var groupColumn = 0;
                    tableEvolMissionY = $('#tableEvolMissionY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });

            /** -----------------------------------------------------------------------------------
             * INTERVENTIONS
             * On récupère 4 listes,
             * QTE Y / QTE Y - 1
             * SEMESTRE
             * TRIMESTRE
             * MOIS (sur 2 ans seulement)
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionIntervention.php",
                type: "POST",
                data: {
                    dissociate:dissociate
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * On récupère la liste des CAMPAGNES
                     */
                    var Y = data.DATAY;
                    $.each(Y, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (année -1)</td>';
                        var years = element.years;

                        $.each(years, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(years, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';

                        $('#tableEvolInterventionY tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionY tbody').append(
                            ligneInf
                        );
                    });

                    var S = data.DATAS;
                    $.each(S, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (Semestre -1)</td>';
                        var semestres = element.semestres;

                        $.each(semestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(semestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionS tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionS tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionS tbody').append(
                            ligneInf2
                        );
                    });

                    var T = data.DATAT;
                    $.each(T, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (Trimestre -1)</td>';
                        var trimestres = element.trimestres;

                        $.each(trimestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(trimestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionT tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionT tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionT tbody').append(
                            ligneInf2
                        );
                    });

                    var M = data.DATAM;
                    $.each(M, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (mois -1)</td>';
                        var months = element.months;

                        $.each(months, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(months, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionM tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionM tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionM tbody').append(
                            ligneInf2
                        );
                    });

                    var groupColumn = 0;
                    tableEvolInterventionY = $('#tableEvolInterventionY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionS = $('#tableEvolInterventionS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="11">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionT = $('#tableEvolInterventionT').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="21">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionM = $('#tableEvolInterventionM').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="24">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });

            /** -----------------------------------------------------------------------------------
             * ANIMATIONS
             * On récupère 4 listes,
             * QTE Y / QTE Y - 1
             * SEMESTRE
             * TRIMESTRE
             * MOIS (sur 2 ans seulement)
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionAnimation.php",
                type: "POST",
                data: {
                    dissociate:dissociate
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * On récupère la liste des CAMPAGNES
                     */
                    var Y = data.DATAY;
                    $.each(Y, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var years = element.years;

                        $.each(years, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(years, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy)  + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';

                        $('#tableEvolInterventionAY tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionAY tbody').append(
                            ligneInf
                        );
                    });

                    var S = data.DATAS;
                    $.each(S, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (semestre -1)</td>';
                        var semestres = element.semestres;

                        $.each(semestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(semestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy)  + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi)  + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionAS tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionAS tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionAS tbody').append(
                            ligneInf2
                        );
                    });

                    var T = data.DATAT;
                    $.each(T, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (trimestre -1)</td>';
                        var trimestres = element.trimestres;

                        $.each(trimestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(trimestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy)  + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi)  + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionAT tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionAT tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionAT tbody').append(
                            ligneInf2
                        );
                    });

                    var M = data.DATAM;
                    $.each(M, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (mois -1)</td>';
                        var months = element.months;

                        $.each(months, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(months, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionAM tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionAM tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionAM tbody').append(
                            ligneInf2
                        );
                    });

                    var groupColumn = 0;
                    tableEvolInterventionAY = $('#tableEvolInterventionAY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionAS = $('#tableEvolInterventionAS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="11">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionAT = $('#tableEvolInterventionAT').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="21">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionAM = $('#tableEvolInterventionAM').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="24">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });

            /** -----------------------------------------------------------------------------------
             * FORMATIONS
             * On récupère 4 listes,
             * QTE Y / QTE Y - 1
             * SEMESTRE
             * TRIMESTRE
             * MOIS (sur 2 ans seulement)
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionFormation.php",
                type: "POST",
                data: {
                    dissociate: dissociate
                },
                dataType: 'json',
                success: function (data) {


                    /** ---------------------------------------------------------------------------
                     * On récupère la liste des CAMPAGNES
                     */
                    var Y = data.DATAY;
                    $.each(Y, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (année -1)</td>';
                        var years = element.years;

                        $.each(years, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(years, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';

                        $('#tableEvolInterventionFY tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionFY tbody').append(
                            ligneInf
                        );
                    });

                    var S = data.DATAS;
                    $.each(S, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (semestre -1)</td>';
                        var semestres = element.semestres;

                        $.each(semestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(semestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txy === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionFS tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionFS tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionFS tbody').append(
                            ligneInf2
                        );
                    });

                    var T = data.DATAT;
                    $.each(T, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (trimestre -1)</td>';
                        var trimestres = element.trimestres;

                        $.each(trimestres, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(trimestres, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionFT tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionFT tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionFT tbody').append(
                            ligneInf2
                        );
                    });

                    var M = data.DATAM;
                    $.each(M, function(index, element) {

                        //var ligneSup = '<tr>' + '<td rowspan="2">' + element.nom + '</td><td>nb</td>';
                        //var ligneInf = '<tr><td>tx (i-1)</td>';

                        var ligneSup = '<tr>' + '<td>' + element.nom + '</td><td>nb</td>';
                        var ligneInf = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (N -1)</td>';
                        var ligneInf2 = '<tr>' + '<td>' + element.nom + '</td><td>Évolution (%) (mois -1)</td>';
                        var months = element.months;

                        $.each(months, function( index, nb) {
                            ligneSup += '<td>' + nb.nb + '</td>';
                        });
                        ligneSup += '</tr>';

                        $.each(months, function( index, tx) {
                            ligneInf += '<td>' + (tx.txy === undefined ? '-' : tx.txy) + '</td>';
                            ligneInf2 += '<td>' + (tx.txi === undefined ? '-' : tx.txi) + '</td>';
                        });
                        ligneSup += '</tr>';
                        ligneInf += '</tr>';
                        ligneInf2 += '</tr>';

                        $('#tableEvolInterventionFM tbody').append(
                            ligneSup
                        );
                        $('#tableEvolInterventionFM tbody').append(
                            ligneInf
                        );
                        $('#tableEvolInterventionFM tbody').append(
                            ligneInf2
                        );
                    });

                    var groupColumn = 0;
                    tableEvolInterventionFY = $('#tableEvolInterventionFY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionFS = $('#tableEvolInterventionFS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="11">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionFT = $('#tableEvolInterventionFT').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="21">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });
                    tableEvolInterventionFM = $('#tableEvolInterventionFM').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="24">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });

            $('#tableEvolCampagneY').show();
            $('#tableEvolMissionY').show();
            $('#tableEvolMissionS').show();
            $('#tableEvolInterventionY').show();
            $('#tableEvolInterventionS').show();
            $('#tableEvolInterventionT').show();
            $('#tableEvolInterventionM').show();
            $('#tableEvolInterventionAY').show();
            $('#tableEvolInterventionAS').show();
            $('#tableEvolInterventionAT').show();
            $('#tableEvolInterventionAM').show();
            $('#tableEvolInterventionFY').show();
            $('#tableEvolInterventionFS').show();
            $('#tableEvolInterventionFT').show();
            $('#tableEvolInterventionFM').show();


        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {

        /** ---------------------------------------------------------------------------------------
         * Limite
         */
        var limites = [[0, 'ALL'], [5, '5'], [12, '12'], [36, '36'], [48, '48']];
        limites.forEach(function(element) {
            console.log(element[0] + ' -> ' + element[1]);
            var option = $('<option>')
                .attr('value', element[0])
                .html(element[1]);
            $('#lstLimit').append(option);
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function () {
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
        ClassStats.initDates();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                //var option = $('<option>').attr('value', 0).html('Sélectionner une campagne');
                //$('#lstFiltreCampagne').append(option);
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
        $('#lstFiltreCampagne').html('<option></option>').trigger('change');
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des missions
     */
    initMission: function (idCampagne) {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeMissions.php",
            type: "POST",
            data: {
                id: idCampagne
            },
            dataType: 'json',
            success: function (data) {
                if (idCampagne != 0) {
                    $('#lstFiltreMission').empty();
                }
                if (idCampagne != 0) {
                    $('#lstFiltreMission').html('<option></option>');
                    $.each(data.items, function (index, item) {
                        var option =
                            $('<option>')
                                .attr('value', item.idMission)
                                .html(item.libelleMission)

                        ;
                        $('#lstFiltreMission').append(option);
                    });
                    $('#lstFiltreMission').trigger('change');
                }
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des dates du 1er janvier au 31 décemblre
     */
    initDates:function() {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), 0, 1);
        var lastDay = new Date(date.getFullYear(), 11, 31);
        $('#dateDeb').val(firstDay.toLocaleDateString());
        $('#dateFin').val(lastDay.toLocaleDateString());
    }

}