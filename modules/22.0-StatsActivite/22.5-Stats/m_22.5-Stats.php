<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="alert alert-warning">
                        <strong>Attention</strong> Les formations sont comptées pour 1 dans le tableau des interventions.
                    </div>
                    <div class="form-body">

                        <input type="checkbox" id="dissociate">Dissocier par CP
                        <!--
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Campagne :</label>
                                    <select id="lstFiltreCampagne" name="lstFiltreCampagne" class="form-control select2"
                                    >
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mission :</label>
                                    <select id="lstFiltreMission" name="lstFiltreMission"
                                            class="form-control select2" >
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="start" style="text-align: left"
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="end" style="text-align: left" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        -->
                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$year = date('Y');
?>
<h1><i class="fa fa-dashboard font-green" style="font-size: 24px;"></i><strong> Evolution des Campagnes</strong></h1>
<table class="table table-striped table-bordered table-hover" id="tableEvolCampagneY" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
            for ($i = 4 ; $i >= 0 ; $i--) {
                echo '<th>'.($year-$i).'</th>';
            }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<h1><i class="fa fa-dashboard font-green" style="font-size: 24px;"></i><strong> Evolution des Missions</strong></h1>
<table class="table table-striped table-bordered table-hover" id="tableEvolMissionY" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            echo '<th>'.($year-$i).'</th>';
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<h1><i class="fa fa-dashboard font-green" style="font-size: 24px;"></i><strong> Evolution des Interventions</strong></h1>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionY" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            echo '<th>'.($year-$i).'</th>';
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionS" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 2 ; $j++) {
                echo '<th>S'.$j.'/'.($year-$i).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionT" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 4 ; $j++) {
                echo '<th>T'.$j.'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionM" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 1 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 12 ; $j++) {
                echo '<th>'.str_pad($j, 2, '0', STR_PAD_LEFT).'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<h1><i class="fa fa-dashboard font-green" style="font-size: 24px;"></i><strong> Evolution des Animations</strong></h1>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionAY" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            echo '<th>'.($year-$i).'</th>';
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionAS" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 2 ; $j++) {
                echo '<th>S'.$j.'/'.($year-$i).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionAT" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 4 ; $j++) {
                echo '<th>T'.$j.'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionAM" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 1 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 12 ; $j++) {
                echo '<th>'.str_pad($j, 2, '0', STR_PAD_LEFT).'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<h1><i class="fa fa-dashboard font-green" style="font-size: 24px;"></i><strong> Evolution des Formations</strong></h1>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionFY" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            echo '<th>'.($year-$i).'</th>';
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionFS" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 2 ; $j++) {
                echo '<th>S'.$j.'/'.($year-$i).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionFT" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 4 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 4 ; $j++) {
                echo '<th>T'.$j.'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table table-striped table-bordered table-hover" id="tableEvolInterventionFM" style="display:none;width:100%">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <?php
        for ($i = 1 ; $i >= 0 ; $i--) {
            for ($j = 1 ; $j <= 12 ; $j++) {
                echo '<th>'.str_pad($j, 2, '0', STR_PAD_LEFT).'/'.substr(($year-$i), 2, 2).'</th>';
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>