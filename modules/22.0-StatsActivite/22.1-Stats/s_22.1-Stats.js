/** -----------------------------------------------------------------------------------------------
 *
 */
$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    bindClear : function() {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function() {

            ClassStats.initDates();
            $("#idSalarie_20_1").prop('selectedIndex', 'ALL');
            idSalarie_20_1 = '';
            if (tableAccesWeb_20_1 != null){
                tableAccesWeb_20_1.destroy();
                tableAccesWeb_20_1 = null;
                $('#tableAccesWeb_20_1 tbody').empty();
            }
        });




    },

    bindFilter: function () {
        $('#btnSearch').on('click', function() {

            var campagne = $('#lstFiltreCampagne').val();
            var mission = $('#lstFiltreMission').val();
            var intervenante = $('#lstFiltreIntervenante').val();
            var dateDeb = $('#dateDeb').val();
            var dateFin = $('#dateFin').val();
            var prorata = $('#lstFiltreProrata option:selected').val();

            /** -----------------------------------------------------------------------------------
             * A minima, il faut la campagne
             */
            if (campagne == '' || mission == '' ||campagne == null || mission == null) {
                toastr.error('Il faut au moins séléctionner la mission.');
                return false;
            }

            /** -----------------------------------------------------------------------------------
             * On calcule maintenant la RENTABILITÉ
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stat.calcRentabilite.php",
                type: "POST",
                data: {
                    campagne : campagne,
                    mission : mission,
                    intervenante : intervenante,
                    dateDeb : dateDeb,
                    dateFin : dateFin,
                    prorata : prorata
                },
                dataType: 'json',
                success: function(data) {

                    /** ---------------------------------------------------------------------------
                     * Données sans besoin de calculs savants
                     */
                    if (data.TYPEM == 2) {
                        $('.details_nb').children('.number').html(data.NBF1 + ' / ' + data.NBF2 + ' / ' + data.NBF3);
                    }
                    else {
                        $('.details_nb').children('.number').html(data.NBINTERV);
                    }

                    var prixJ = 0;
                    var pxj = 0;
                    var pxj2 = 0;
                    var pxj3 = 0;
                    var net = 0;
                    if (data.TYPEM == 2) {
                        prixJ = data.PRIXJOUR.F;
                        prixJ2 = data.PRIXJOUR.F2;
                        prixJ3 = data.PRIXJOUR.F3;
                    }
                    else {
                        prixJ = data.PRIXJOUR.A;
                    }
                    if (data.TYPEM == 2) {
                        pxj = parseFloat(prixJ);
                        net = parseFloat(prixJ) * parseFloat(data.NBF1);
                        pxj2 = parseFloat(prixJ2);
                        net += parseFloat(pxj2) * parseFloat(data.NBF2);
                        pxj3 = parseFloat(prixJ3);
                        net += parseFloat(pxj3) * parseFloat(data.NBF3);
                    }
                    else {
                        pxj = parseFloat(prixJ);
                        net = parseFloat(prixJ) * parseFloat(data.NBINTERV);
                    }
                    var AA = net;
                    if (data.TYPEM == 2) {
                        pxj = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(pxj);
                        pxj2 = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(pxj2);
                        pxj3 = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(pxj3);
                        net = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(net);
                        $('.details_frais').children('.number').html(pxj + ' / ' + pxj2 + ' / ' + pxj3);
                        $('.details_net').children('.number').html(net);
                    }
                    else {
                        pxj = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(pxj);
                        net = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(net);
                        $('.details_frais').children('.number').html(pxj);
                        $('.details_net').children('.number').html(net);
                    }

                    $('#tableResult').dataTable().fnDestroy();
                    $('#tableResult tbody').empty();

                    /** ---------------------------------------------------------------------------
                     * Pour chaque INTER, on va mettre le BRUT et le TAUX
                     */
                    var totalVerse = 0;
                    var keys = data.KEYS;
                    var totTaux = 0;
                    var nbTaux = 0;
                    $.each(data.SALAIRES, function (id, item) {

                        var annee = (Object.keys(item)[0]);
                        var tr = '<tr><td>' + keys[id] + '</td>';
                        var detail = item[annee];
                        for (var mois = 1; mois <= 12; mois++) {
                            if (detail[mois] == undefined) {
                                tr = tr + '<td>-</td>' + '<td>-</td>' + '<td>-</td>' + '<td>-</td>';
                            }
                            else {
                                /** ---------------------------------------------------------------
                                 * Brut + taux
                                 */
                                var brut = detail[mois]['brutreference'];
                                var nbinter = detail[mois].count;
                                var cout = detail[mois]['coutemployeur'];
                                brut = Math.round(brut * 100) / 100;
                                cout = Math.round(cout * 100) / 100;
                                nbinter = Math.round(nbinter * 100) / 100;
                                var A = prixJ * nbinter;
                                if (A != 0) {
                                    tx = (A - cout) / A;
                                    totTaux += parseFloat(tx);
                                    nbTaux++;
                                    tx = Math.round(tx * 10000) / 100;
                                }
                                tr = tr + '<td>' + brut + '</td>' + '<td>' + cout + '</td>' + '<td>' + nbinter + '</td>' + '<td>' + tx + '%</td>';
                                totalVerse = (1.0 * totalVerse) + (1.0 * parseFloat(cout));

                            }

                        }
                        $('#tableResult tbody').append(tr);
                    });

                    $('#tableResult').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * On rapporte le cumul
                     */
                    var BB = totalVerse;
                    totalVerse = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(totalVerse);
                    $('.details_totalverse').children('.number').html(totalVerse);

                    var renta = AA-BB;
                    var txrenta = (AA-BB)/AA;
                    //txrenta = totTaux / nbTaux;

                    console.log (AA + ' - ' + BB + ' / ' + AA);
                    txrenta = Math.round(txrenta * 10000) / 100;
                    renta = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(renta);


                    //sommeNet = Math.round(sommeNet * 100) / 100;
                    //var netFinal = data.NBINTERV * data.PRIXJOUR.A - sommeNet;
                    //netFinal = Math.round(netFinal * 100) / 100;

                    $('.details_rentabilite').children('.number').html(renta);
                    $('.details_tx').children('.number').html(txrenta + '%');

                    /** ---------------------------------------------------------------------------
                     * Affichage
                     */
                    //sommeNet = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(sommeNet);
                    //netFinal = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(netFinal);
                    //$('.details_net').children('.number').html(sommeNet);

                }
            });

        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {
        ClassStats.initCampagne();
        ClassStats.initMission(0);

        /** ---------------------------------------------------------------------------------------
         * Bind Changes
         */
        $('#lstFiltreCampagne').on('change', function () {
            if ($(this).val() == '') {
                ClassStats.initMission(0);
                //ClassStats.initIntervenante(0, 0);
            }
            else {
                ClassStats.initMission($(this).val());
                //ClassStats.initIntervenante($(this).val(), 0);
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Bind Changes
         */
        $('#lstFiltreMission').on('change', function () {
            if ($(this).val() == '') {
                ClassStats.initIntervenante($('#lstFiltreCampagne').val(), 0);
            }
            else {
                ClassStats.initIntervenante($('#lstFiltreCampagne').val(), $(this).val());
            }
        });

    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function() {
        $('.input-daterange').datepicker();
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreCampagne').append('<option value=""></option>');
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des missions
     */
    initMission: function (idCampagne) {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeMissions.php",
            type: "POST",
            data: {
                id: idCampagne
            },
            dataType: 'json',
            success: function (data) {
                if (idCampagne != 0) {
                    $('#lstFiltreMission').empty();
                }
                if (idCampagne != 0) {
                    console.log(data);
                    $('#dateDeb').val(data.from);
                    $('#dateFin').val(data.to);

                    $('#lstFiltreMission').html('<option></option>');
                    $.each(data.items, function (index, item) {
                        var option =
                            $('<option>')
                                .attr('value', item.idMission)
                                .html(item.libelleMission)

                        ;
                        $('#lstFiltreMission').append(option);
                    });
                    $('#lstFiltreMission').trigger('change');
                }
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des intervenantes
     */
    initIntervenante: function (idCampagne, idMission) {
        $('#lstFiltreIntervenante').empty();
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {
                idCampagne: idCampagne,
                idMission: idMission,
                bAUTO: 0
            },
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstFiltreIntervenante').append(option);
                });
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des dates du 1er janvier au 31 décemblre
     */
    initDates:function() {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), 0, 1);
        var lastDay = new Date(date.getFullYear(), 11, 31);
        $('#dateDeb').val(firstDay.toLocaleDateString());
        $('#dateFin').val(lastDay.toLocaleDateString());
    }
};