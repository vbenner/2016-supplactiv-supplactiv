<?php
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);
#print 'TEST';
#die();
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Campagne :</label>
                                    <select id="lstFiltreCampagne" name="lstFiltreCampagne"
                                            class="form-control select2">
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mission :</label>
                                    <select id="lstFiltreMission" name="lstFiltreMission"
                                            class="form-control select2">
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Intervenante :</label>
                                    <select id="lstFiltreIntervenante" name="lstFiltreIntervenante"
                                            class="form-control select2">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="dateDeb" style="text-align: left"
                                               value="01/01/2018"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="dateFin" style="text-align: left"
                                               value="31/12/2018"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <label class="control-label">Prorata :</label>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <select id="lstFiltreProrata" name="lstFiltreProrata"
                                            class="form-control select2">
                                        <option value="0" selected>NON</option>
                                        <option value="1" >OUI</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right"
                                    style="margin-right: 5px;">Effacer
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-calendar"></i>
            </div>
            <div class="details details_nb">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">INTERVENTIONS</a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-euro"></i>
            </div>
            <div class="details details_frais">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">PRIX / JOUR</a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-euro"></i>
            </div>
            <div class="details details_net">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">CA CLIENT</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-euro"></i>
            </div>
            <div class="details details_totalverse">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">TOTAL VERSÉ</a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-euro"></i>
            </div>
            <div class="details details_rentabilite">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">RENTABILITÉ</a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="dashboard-stat purple-intense">
            <div class="visual">
                <i class="">&#x25;</i>
            </div>
            <div class="details details_tx">
                <div class="number"></div>
                <div class="desc"></div>
            </div>
            <a class="more">TAUX (≠ MOYENNE DES TAUX)</a>
        </div>
    </div>
</div>


<table class="table table-striped table-bordered table-hover" id="tableResult" style="width: 100%">
    <thead>
    <tr>
        <th rowspan="2">Intervenant</th>
        <?php
        $month = array('Jan.', 'Fév.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août',
            'Sept.', 'Oct.', 'Nov.', 'Dec.');
        for ($i = 0; $i < 12 ; $i++) {
            echo '<th colspan=4>'.$month[$i].'</th>';
        }
        echo '</tr><tr>';
        for ($i = 0; $i < 12 ; $i++) {
            echo '<th>Brut</th><th><small>Total versé</small><th><small>Nb Interv.</small></th><th>Taux</th>';
        }
        echo '</tr>';
        ?>
    </thead>
    <tbody>

    </tbody>
</table>