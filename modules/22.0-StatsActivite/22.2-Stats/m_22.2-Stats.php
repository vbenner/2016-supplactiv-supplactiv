<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Filtre
                </div>
            </div>
            <div class="portlet-body">
                <form>
                    <div class="form-body">
                        <div class="alert alert-warning">
                            <strong>Attention</strong> Si la campagne et / ou la mission sont
                            sélectionnées alors les dates ne seront prises en compte que si elles
                            entrent dans la période ciblée par la campagne.<br/>
                            Les formations sont comptées en nombre d'interventions.
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Campagne :</label>
                                    <select id="lstFiltreCampagne" name="lstFiltreCampagne" class="form-control select2"
                                            >
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mission :</label>
                                    <select id="lstFiltreMission" name="lstFiltreMission"
                                            class="form-control select2" >
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="start" style="text-align: left"
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="end" style="text-align: left" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--
                        <div class="row">
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group" style="padding-left: 0px">
                                    <label class="control-label col-md-6">Date début :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateDeb" readonly
                                               class="form-control form-recherche"
                                               type="text" placeholder="" name="dateDeb" style="text-align: left"
                                               value="01/01/2018"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Date fin :</label>
                                    <div class="col-md-6 input-daterange">
                                        <input id="dateFin" readonly
                                               class="form-control form-recherche"
                                               type="text"
                                               placeholder="" name="dateFin" style="text-align: left" value="31/12/2018"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->

                    </div>

                    <div class="row" style="margin: 0px;">
                        <div class="form-actions right">
                            <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                            <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$year = date('Y');
 ?>
<table class="table table-striped table-bordered table-hover" id="tableResult" style="display: none">
    <thead>
    <tr>
        <th></th>
        <?php
            for ($i = 1; $i <= 12 ; $i++) {
                echo '<th></th>';
            }
            echo '<th></th>';
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<h4>TAUX DE SERVICE MENSUEL THÉORIQUE <span id="quantieme"></span></h4>
<table class="table table-striped table-bordered table-hover" id="tableResultTheo" style="display: none">
    <thead>
    <tr>
        <th></th>
        <?php
        for ($i = 1; $i <= 12 ; $i++) {
            echo '<th></th>';
        }
        ?>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>