/** -----------------------------------------------------------------------------------------------
 * Un seul tableau, celui des résultats
 */
var tableResult = null;
var tableResultTheo = null;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    bindClear : function() {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function() {

            $('#tableResult').hide();
            $('#tableResultTheo').hide();

            ClassStats.initDates();
            $("#lstFiltreMission").select2("val", "");
            $("#lstFiltreCampagne").select2("val", "");
            ClassStats.initMission(0);

            if (tableResult != null){
                tableResult.destroy();
                tableResult = null;
                $('#tableResult tbody').empty();
            }

            if (tableResultTheo != null){
                tableResultTheo.destroy();
                tableResultTheo = null;
                $('#tableResultTheo tbody').empty();
            }
        });




    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER
         */
        $('#btnSearch').on('click', function () {

            /** -----------------------------------------------------------------------------------
             * Minimum SYNDICAL : la MISSION
             */
            //if ($('#lstFiltreMission').val() == '' || $('#lstFiltreMission').val() == null ) {
            //    toastr.error('Il faut sélectionner une mission');
            //    return false;
            //}

            /** -----------------------------------------------------------------------------------
             * DATES
             */
            if ($('#dateDeb').val() == '' || $('#dateFin').val() == '') {
                toastr.error('Il faut indiquer les dates');
                return false;
            }

            /** -----------------------------------------------------------------------------------
             * On récumère 2 listes, QTE / PREVISIONNEL
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.tauxServices.php",
                type: "POST",
                data: {
                    campagne : $('#lstFiltreCampagne').val(),
                    mission : $('#lstFiltreMission').val(),
                    from : $('#dateDeb').val(),
                    to: $('#dateFin').val(),
                },
                dataType: 'json',
                success: function(data) {
                    if (data.RES == 0) {
                        toastr.error(data.INFO);
                        return false;
                    }
                    $('#tableResult').show();
                    $('#tableResultTheo').show();

                    /** ---------------------------------------------------------------------------
                     * On met les entêtes
                     */
                    var year = data.YEAR;
                    var table = 'tableResult';
                    tr = '<tr><th></th>';
                    for (j = 1; j <= 12; j++) {
                        tr += '<th>' + pad(j, 2) + '/' + year + '</th>';

                    }
                    tr += '<th>Cumul</th>';
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    var table = 'tableResultTheo';
                    tr = '<tr><th></th>';
                    for (j = 1; j <= 12; j++) {
                        tr += '<th>' + pad(j, 2) + '/' + year + '</th>';

                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);


                    var month = data.DATA;
                    var tx = data.PERCENT;
                    var nb = '';
                    var cumul = 0;
                    var lastPercent = 0;
                    var percent = '';
                    var quantieme = Math.round(data.REF / 12);
                    for (i = 1; i <= 12 ; i++) {
                        nb += '<td>'+month[i]+'</td>';

                        percent += '<td>'+tx[i]+'</td>';
                        lastPercent = tx[i];
                        cumul = parseInt(cumul) + parseInt(1*month[i]);
                    }

                    percent += '<td>' + lastPercent + '</td>';
                    nb += '<td>' + cumul + '/' + data.REF + '</td>'
                    if (tableResult != null){
                        $('#tableResult').dataTable().fnClearTable();
                        $('#tableResult').dataTable().fnDestroy();
                    }
                    $('#tableResult tbody').append('<tr><td>% cumulé</td>' + percent + '</tr>');
                    $('#tableResult tbody').append('<tr><td>NB interv.</td>' + nb + '</tr>');
                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                        {
                            text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                            extend: 'copy',
                        },
                        {
                            text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                            extend: 'csv',
                        },
                        {
                            text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                            extend: 'excel',
                            exportOptions: {
                                columns: ':visible',
                                format: {
                                    body: function(data, row, column, node) {
                                        data = $('<p>' + data + '</p>').text();
                                        return $.isNumeric(data.replace(',', '.')) ? data.replace(',', '.') : data;
                                    }
                                }
                            }
                        },
                        {
                            text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                            extend: 'pdf',
                        },
                        {
                            text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                            extend: 'print',
                        }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * On refait donc le calcul par rapport au quantième
                     */
                    nb = '';percent = ''; var objectif='';
                    var color = '';
                    for (i = 1; i <= 12 ; i++) {
                        if (month[i] != 0) {
                            if (month[i]>=quantieme) {
                                color = "bg-green";
                            }
                            else {
                                color = "bg-red";

                            }
                            nb += '<td>' + month[i] + '</td>';
                            percent += '<td class="' + color + '">'+Math.round( 100*(month[i]/quantieme)) +'</td>';
                            objectif += '<td>'+quantieme+'</td>';
                        }
                        else {
                            nb += '<td>0</td>';
                            percent += '<td>-</td>';
                            objectif += '<td>'+quantieme+'</td>';
                        }
                    }

                    if (tableResultTheo != null){
                        $('#tableResultTheo').dataTable().fnClearTable();
                        $('#tableResultTheo').dataTable().fnDestroy();
                    }
                    $('#tableResultTheo tbody').append('<tr><td>Taux Mensuel %</td>' + percent + '</tr>');
                    $('#tableResultTheo tbody').append('<tr><td>NB interv.</td>' + nb + '</tr>');
                    $('#tableResultTheo tbody').append('<tr><td>Théorique mensuel</td>' + objectif + '</tr>');
                    tableResultTheo = $('#tableResultTheo').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                }
            });
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {
        ClassStats.initCampagne();
        ClassStats.initMission(0);

        /** ---------------------------------------------------------------------------------------
         * Bind Changes
         */
        $('#lstFiltreCampagne').on('change', function() {
            if ($(this).val() == '') {
                ClassStats.initMission(0);
            }
            else {
                ClassStats.initMission($(this).val());
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function() {
        $('.input-daterange').datepicker();
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
        ClassStats.initDates();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreCampagne').append('<option value=""></option>');
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des missions
     */
    initMission: function (idCampagne) {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeMissions.php",
            type: "POST",
            data: {
                id : idCampagne
            },
            dataType: 'json',
            success: function(data) {
                if (idCampagne != 0) {
                    $('#lstFiltreMission').empty();
                }
                if (idCampagne != 0) {
                    $('#lstFiltreMission').append('<option></option>');
                    $.each(data.items, function (index, item) {
                        var option =
                            $('<option>')
                                .attr('value', item.idMission)
                                .html(item.libelleMission)

                        ;
                        $('#lstFiltreMission').append(option);
                    });
                    $('#lstFiltreMission').trigger('change');
                }
            }
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des dates du 1er janvier au 31 décemblre
     */
    initDates:function() {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), 0, 1);
        var lastDay = new Date(date.getFullYear(), 11, 31);
        $('#dateDeb').val(firstDay.toLocaleDateString());
        $('#dateFin').val(lastDay.toLocaleDateString());
    }
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}