<!-- Style de la page -->
<style type="text/css">
	.flat_area h2 { line-height: 20px !important;font-size: 16px !important; }
</style>

<?php

# ------------------------------------------------------------------------------------
# PREPA REQUETE - Recherche des Articles
# ------------------------------------------------------------------------------------
$sqlRechercheArticle = '
SELECT *
FROM CONTRAT_CIDD_ARTICLE
ORDER BY Position_Article';
$RechercheListeArticleExc = DbConnexion::getInstance()->prepare($sqlRechercheArticle);
	
$sqlRechercheLibelleMission = '
SELECT libelleMission
FROM su_mission
WHERE idMission = :id';
$RechercheLibelleMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheLibelleMission);
	
$sqlRechercheArticleMission = '
SELECT CONTRAT_CIDD_MISSION.Contenu_Article,Titre_Article,Id_Article,Article_Auto
FROM CONTRAT_CIDD_ARTICLE
	INNER JOIN CONTRAT_CIDD_MISSION ON CONTRAT_CIDD_MISSION.FK_Id_Article = CONTRAT_CIDD_ARTICLE.Id_Article
WHERE FK_Id_Mission = :id
ORDER BY Position_Article';
$RechercheArticleMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheArticleMission);
						
$RechercheLibelleMissionExc->bindValue(':id', $_SESSION['ID_Mission_CreationContrat_6_5'], PDO::PARAM_INT);	
$RechercheLibelleMissionExc->execute();
while($Data = $RechercheLibelleMissionExc->fetch(PDO::FETCH_OBJ)){
	$Type = (isset($_GET['modif'])) ? 'Modification' : 'Création';
	echo '<div class="well">'.$Type.' d\'un contrat type pour la mission <strong>'.$Data->libelleMission.'</strong></div>';
}
?>

<form method="post" action="index.php?ap=5.0-Contrat&ss_m=5.3-ConventionForfait">
    <div class="panel-group accordion" id="accordion1">
<?php	
if(!isset($_GET['modif'])){
    $nb = 0;
	$RechercheListeArticleExc->execute();
	while($Data = $RechercheListeArticleExc->fetch(PDO::FETCH_OBJ)){
        $nb++;
		if(!$Data->Article_Auto OR $Data->Id_Article == 5){
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php print $nb ?>">
                            <?php echo $Data->Titre_Article ?></a>
                    </h4>
                </div>
                <div id="collapse_<?php print $nb ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <textarea id="art_<?php echo $Data->Id_Article?>" name="art_<?php echo $Data->Id_Article?>" class="form-control"><?php echo $Data->Contenu_Article;?></textarea>
                    </div>
                </div>
            </div>

			<?php
		}else {
			?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php print $nb ?>">
                            <?php echo $Data->Titre_Article ?> -> AUTO</a>
                    </h4>
                </div>
                <div id="collapse_<?php print $nb ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <textarea style="display:none !important;" id="art_<?php echo $Data->Id_Article?>" name="art_<?php echo $Data->Id_Article?>"  class="form-control"><?php echo $Data->Contenu_Article;?></textarea>
                    </div>
                </div>
            </div>
			<?php
		}
	}
}else {
	$RechercheArticleMissionExc->bindValue(':id', $_SESSION['ID_Mission_CreationContrat_6_5'], PDO::PARAM_INT);	
	$RechercheArticleMissionExc->execute();
	while($Data = $RechercheArticleMissionExc->fetch(PDO::FETCH_OBJ)){
        $nb++;
		if(!$Data->Article_Auto OR $Data->Id_Article == 5){
			?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php print $nb ?>">
                            <?php echo $Data->Titre_Article ?></a>
                    </h4>
                </div>
                <div id="collapse_<?php print $nb ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <textarea id="art_<?php echo $Data->Id_Article?>" name="art_<?php echo $Data->Id_Article?>"  class="form-control"><?php echo str_replace('\\','',$Data->Contenu_Article)?></textarea>
                    </div>
                </div>
            </div>
			<?php
		}else {
			?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php print $nb ?>">
                            <?php echo $Data->Titre_Article ?> -> AUTO</a>
                    </h4>
                </div>
                <div id="collapse_<?php print $nb ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <textarea style="display:none !important;" id="art_<?php echo $Data->Id_Article?>" name="art_<?php echo $Data->Id_Article?>"  class="form-control"><?php echo str_replace('\\','',$Data->Contenu_Article);?></textarea>
                    </div>
                </div>
            </div>
			<?php
		}
	}
}
?></div>

            <button type="submit" class="btn btn-primary btn-lg btn-block" >
            	<span>Valider le contrat type</span>
            </button>

</div>
</form>