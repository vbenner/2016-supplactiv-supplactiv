var tableauCONTRAT; var tableauMISSION;

$(function() {
    //Dialog de suppression d'un contrat
    $( "#dialog_SUPPRESSION_Contrat" ).dialog({
        autoOpen:false,
        resizable: false,
        modal: true
    });

    //Initialisation du datatable
    tableauCONTRAT = $('#tableauCONTRAT').dataTable({
        bJQueryUI: true,
        "aaSorting": [[0,'desc']],
        fnInitComplete: function() {
            $("#dt1 .dataTables_length > label > select").uniform();
            $("#dt1 .dataTables_filter input[type=text]").addClass("text");
            $(".datatable").css("visibility","visible");

        },
        bProcessing: true,
        bDefferRender: true,
        "bRetrieve": true,
        sAjaxSource: "includes/functions/datatable/func.6.3-ListeConvention.php",
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(5)', nRow).children('input').uniform();
            return nRow;
        }
    });

    //Initialisation du datatable
    tableauMISSION = $('#tableauMISSION').dataTable({
        bJQueryUI: true,
        "aaSorting": [[0,'desc']],
        fnInitComplete: function() {
            $("#dt1 .dataTables_length > label > select").uniform();
            $("#dt1 .dataTables_filter input[type=text]").addClass("text");
            $(".datatable").css("visibility","visible");

        },
        bProcessing: true,
        bDefferRender: true,
        "bRetrieve": true,
        sAjaxSource: "includes/functions/datatable/func.6.3-ListeMission.php",
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(5)', nRow).children('input').uniform();
            return nRow;
        }
    });
});

var IdContratSuppression;

//Affichage de la dialog de suppression d'un contrat
function suppressionContrat(IdContrat){
    IdContratSuppression = IdContrat;
    $.post("includes/functions/bdd2web/func.6.3-RechercheInfoConvention.php", {
            'IdContrat' : IdContrat
        },
        function(data) {
            $('#ConteneurInfoSuppression').html(data);
            $( "#modalSuppressionContrat" ).modal("show");
            $('.btn-valid-suppression').unbind().bind('click', function(){
                confirmSuppression(IdContrat);
            })
        }
    );
}

//Suppression d'un contrat et rafraichissement du datatable
function confirmSuppression(IdContrat){
    $.post("includes/functions/web2bdd/func.6.3-SuppressionConvention.php", {
            'IdContrat' : IdContrat
        },
        function(data) {

            tableauCONTRAT.api().ajax.reload();
            $( "#modalSuppressionContrat" ).modal("hide");
        }
    );
}

//Envoi sur la fiche de la convention
function afficheInfoConvention(IDContrat_Recherche_6_5){

    $.ajax({
        async : false,
        type: "POST",
        data: {IDContrat_Recherche_6_5: IDContrat_Recherche_6_5},
        url: "includes/functions/web2session/func.majSession.php",
        dataType: 'json',
        success: function (data) {
            document.location.href = "index.php?ap=5.0-Contrat&ss_m=6.5-FicheConvention";
        }
    });
}

function telechargementConvention(IDContrat_Recherche_6_5){

    $.ajax({
        async : false,
        type: "POST",
        data: {IDContrat_Recherche_6_5: IDContrat_Recherche_6_5},
        url: "includes/functions/web2session/func.majSession.php",
        dataType: 'json',
        success: function (data) {
            window.open('download/func.6-5-exportContratConvention.php');
        }
    });
}

function creationContratMission(ID_Mission_CreationContrat_6_5){
    $.ajax({
        async : false,
        type: "POST",
        data: {ID_Mission_CreationContrat_6_5: ID_Mission_CreationContrat_6_5},
        url: "includes/functions/web2session/func.majSession.php",
        dataType: 'json',
        success: function (data) {
            document.location.href = "index.php?ap=5.0-Contrat&ss_m=6.6-CreationConvention";
        }
    });
}

function modificationContratMission(ID_Mission_CreationContrat_6_5){
    $.ajax({
        async : false,
        type: "POST",
        data: {ID_Mission_CreationContrat_6_5: ID_Mission_CreationContrat_6_5},
        url: "includes/functions/web2session/func.majSession.php",
        dataType: 'json',
        success: function (data) {
            document.location.href = "index.php?ap=5.0-Contrat&ss_m=6.6-CreationConvention&modif=1";
        }
    });
}

function creationContrat(ID_Mission_CreationContrat_6_7){
    $.ajax({
        async : false,
        type: "POST",
        data: {ID_Mission_CreationContrat_6_7: ID_Mission_CreationContrat_6_7},
        url: "includes/functions/web2session/func.majSession.php",
        dataType: 'json',
        success: function (data) {
            document.location.href = "index.php?ap=5.0-Contrat&ss_m=6.7-CreationContratConvention";
        }
    });
}