<?php
if(isset($_POST['art_1'])){

    $sqlInsertionContratType = '
	INSERT INTO CONTRAT_CIDD_TYPE(FK_Id_Mission,Date_Contrat)
	VALUES(:IdMission, :Date)
	ON DUPLICATE KEY UPDATE Date_Contrat = :Date';
    $InsertionContratTypeExc = DbConnexion::getInstance()->prepare($sqlInsertionContratType);

    $sqlInsertionArticleContrat = '
	INSERT INTO CONTRAT_CIDD_MISSION(FK_Id_Mission,FK_Id_Article,Contenu_Article)
	VALUES(:IdMission, :IdArticle, :Contenu)
	ON DUPLICATE KEY UPDATE Contenu_Article = :Contenu';
    $InsertionArticleContratExc = DbConnexion::getInstance()->prepare($sqlInsertionArticleContrat);

    $InsertionContratTypeExc->bindValue(':IdMission', $_SESSION['ID_Mission_CreationContrat_6_5'], PDO::PARAM_INT);
    $InsertionContratTypeExc->bindValue(':Date', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $InsertionContratTypeExc->execute();

    foreach($_POST as $Nom=>$Valeur){
        $Id_Art = str_replace('art_','',trim($Nom));
        $Contenu_Art = $Valeur;
        if($Id_Art != 'mission'){
            $InsertionArticleContratExc->bindValue(':IdMission', $_SESSION['ID_Mission_CreationContrat_6_5'], PDO::PARAM_INT);
            $InsertionArticleContratExc->bindValue(':IdArticle', $Id_Art, PDO::PARAM_INT);
            $InsertionArticleContratExc->bindValue(':Contenu', $Contenu_Art, PDO::PARAM_STR);
            $InsertionArticleContratExc->execute();
        }
    }
}
?>
<h3>Liste des missions enregistr&eacute;es dans la BDD</h3><hr>
<table id="tableauMISSION" class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th>Client</th>
        <th>Libell&eacute; Campagne</th>
        <th>Libell&eacute; Mission</th>
        <th style="width:400px;">Statut</th>
        <th style="width:140px;"></th>
    </tr>
    </thead>
    <tbody></tbody>
</table>



<h3>Liste des Conventions de forfait</h3><hr>
<table id="tableauCONTRAT" class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th style="width:100px">ID Contrat</th>
        <th>Intervenant(e)</th>
        <th>Mission</th>
        <th style="width:90px">Dur&eacute;e</th>
        <th style="width:150px;">Indemnit&eacute; Forfaitaire</th>
        <th style="width:150px;">R&eacute;mun&eacute;ration Brute</th>
        <th style="width:150px;">Date</th>
        <th style="width:90px;"></th>
        <th style="width:90px;"></th>
        <th style="width:90px;"></th>
    </tr>
    </thead>
    <tbody></tbody>
</table>


<div id="modalSuppressionContrat" class="modal fade" tabindex="-1">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'un contrat</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal" id="ConteneurInfoSuppression">


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-suppression">Valider la suppression</button>
            </div>
        </div>
    </div>
</div>
