var tableIntervention_5_2;
var dateRechercheD_5_2, dateRechercheF_5_2, idMission_5_2, idIntervenant_5_2,
    prev_idIntervention_5_2, idIntervention_5_2, boolAuto;

jQuery(document).ready(function () {
    ClassContrat.init();
    ClassContrat.bindEvents();
});

var ClassContrat = {

    bindEvents: function () {
        $('#ztForfaitTel').on('change', function() {
            if ( $('#ztForfaitTel').val() === '') {
                $('#zsTelephone option[value="NON"]').prop('selected', true);
                return true;
            }
            if (! isNaN(parseFloat($('#ztForfaitTel').val()))) {
                $('#zsTelephone option[value="OUI"]').prop('selected', true);
                return true;
            }
        });
    },

    init: function () {

        $('.btn-sel-all, .btn-unsel-all').hide();

        // Etape 1 -> Recherche des sessions pour le module
        this.rechercheSessionModule();

        this.initTableIntervention();

        // Etape 3 -> Initialisation du datepicker
        this.initDateRecherche();

        // Etape 4 -> Initialisation des filtres de recherche
        this.initRechercheIntervenant();
        this.initRechercheMission();

        $(document.body).on("change", "#idMission_5_2", function () {
            ClassContrat.saveSearchToSession();

        });
        $(document.body).on("change", "#idIntervenant_5_2", function () {
            ClassContrat.saveSearchToSession();
        });

        $('.btn-recherche-date').bind('click', function () {
            ClassContrat.saveSearchToSession();
        });

        $('.etape-3-au').hide();
        $('.etape-3-no-au').hide();

        if (idIntervenant_5_2 != '') {
            if (boolAuto == 'OUI') {
                $('.etape-3-au').show();
            } else {
                $('.etape-3-no-au').show();
            }
        }

        $('.btn-validation-contrat').unbind().bind('click', function () {
            // Pb Julie GARNIER --> on sauvegarde la session pour être sûr que
            // tout est bien ?
            ClassContrat.saveSearchToSession();
            ClassContrat.validationContrat(0);
        });

    },

    initTableIntervention: function () {
        tableIntervention_5_2 = $('#tableIntervention_5_2').dataTable({
            "columnDefs": [],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.5.2-listeInterventionSansContrat.php",
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(6)', nRow).children('input').unbind().bind('change', function () {
                    $("#idMission_5_2").select2("val", $(this).attr('data-mission'));
                    $("#idIntervenant_5_2").select2("val", $(this).attr('data-intervenant'));

                    if (!$(this).is(':checked')) {
                        prev_idIntervention_5_2 = $(this).val();
                        idIntervention_5_2 = '';
                    } else {
                        idIntervention_5_2 = $(this).val();
                    }
                    ClassContrat.saveSearchToSession();

                });
            }
        });
        var tableWrapper = $('#tableIntervention_5_2_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    initDateRecherche: function () {
        $('.input-daterange').datepicker();
    },

    initRechercheIntervenant: function () {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.2-rechercheCoupleIntervenantMission.php",
            type: "POST",
            data: {
                typeRecherche: 'intervenant',
                idIntervenant: idIntervenant_5_2,
                idMission: idMission_5_2
            },
            dataType: 'json',
        }).done(function (data) {
            $("#idIntervenant_5_2").select2('destroy').empty().append(
                $('<option>')
                    .attr('value', 'ALL')
                    .html('Tout afficher')
            );

            $.each(data.intervenants, function (key, infoIntervenant) {
                $("#idIntervenant_5_2").append(
                    $('<option>')
                        .attr('value', infoIntervenant.id)
                        .html(infoIntervenant.libelle)
                        .prop('selected', (idIntervenant_5_2 == infoIntervenant.id) ? true : false)
                );
                if (idIntervenant_5_2 == infoIntervenant.id) {
                    boolAuto = infoIntervenant.auto;
                }
            });

            $("#idIntervenant_5_2").select2({
                placeholder: "Cliquez-ici pour rechercher un intervenant",
                allowClear: true
            });
        });
    },

    initRechercheMission: function () {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.2-rechercheCoupleIntervenantMission.php",
            type: "POST",
            data: {
                typeRecherche: 'mission',
                idIntervenant: idIntervenant_5_2,
                idMission: idMission_5_2
            },
            dataType: 'json',
        }).done(function (data) {
            $("#idMission_5_2").select2('destroy').empty().append(
                $('<option>')
                    .attr('value', 'ALL')
                    .html('Tout afficher')
            );

            $.each(data.missions, function (key, infoMission) {
                $("#idMission_5_2").append(
                    $('<option>')
                        .attr('value', infoMission.id)
                        .html(infoMission.libelle)
                        .prop('selected', (idMission_5_2 == infoMission.id) ? true : false)
                );
            });

            $("#idMission_5_2").select2({
                placeholder: "Cliquez-ici pour rechercher une mission",
                allowClear: true
            });
        });
    },

    saveSearchToSession: function () {

        var reponse = false;
        if (idIntervention_5_2 != '') {
        } else {
            /** -----------------------------------------------------------------------------------
             * Correction
             * Si on est ici, c'est qu'on décoche un case. Donc on doit remettre visible le bloc
             * d'étape correspondant en fonction du profil de l'intervenant.
             */
            if (boolAuto == 'OUI') {
                $('.etape-3-au').show();
            } else {
                $('.etape-3-no-au').show();
            }
            //$('.etape-3-au').hide().show();
            //$('.etape-3-no-au').hide().show();
            //$('.btn-sel-all, .btn-unsel-all').hide().show();
        }

        if (idMission_5_2 != $('#idMission_5_2').val() || dateRechercheD_5_2 != $('#dateRechercheD_5_2').val() ||
            dateRechercheF_5_2 != $('#dateRechercheF_5_2').val() || idIntervenant_5_2 != $('#idIntervenant_5_2').val()) {

            dateRechercheD_5_2 = $('#dateRechercheD_5_2').val();
            dateRechercheF_5_2 = $('#dateRechercheF_5_2').val();
            idMission_5_2 = $('#idMission_5_2').val();
            idIntervenant_5_2 = $('#idIntervenant_5_2').val();

            $.ajax({
                async : false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    dateRechercheD_5_2: dateRechercheD_5_2,
                    dateRechercheF_5_2: dateRechercheF_5_2,
                    idMission_5_2: idMission_5_2,
                    idIntervenant_5_2: idIntervenant_5_2,
                    idIntervention_5_2: idIntervention_5_2
                },
                dataType: 'json',
            }).done(function (data) {

                ClassContrat.initRechercheIntervenant();
                ClassContrat.initRechercheMission();
                tableIntervention_5_2.api().ajax.reload();

                ClassContrat.rechercheInfoRemuneration();

                reponse = true;
                return reponse;

            });
        }
    },

    rechercheInfoRemuneration: function () {
        /** ---------------------------------------------------------------------------------------
         * Si l'intervenante est une AUTO-ENTREPRENEUSES, on n'affiche pas les mêmes informations
         */
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.3.2-rechercheInfoIntervenant.php",
            type: "POST",
            data: {idIntervenant: idIntervenant_5_2},
            dataType: 'json'
        }).done(function (data) {

            if (data.intervenant.boolAutoEntrepreneuse == 'OUI') {
                $('.etape-3-au').show();
                $('.etape-3-no-au').hide();
                boolAuto = 'OUI';
            } else {
                $('.etape-3-no-au').show();
                $('.etape-3-au').hide();
                boolAuto = '';
            }

            $('.btn-sel-all, .btn-unsel-all').show();
            $('.btn-sel-all').bind('click', function () {
                $(".chk").each(function () {
                    $(this).prop("checked", true);
                });
            });

            /** Tout deselectionner */
            $('.btn-unsel-all').bind('click', function () {
                $(".chk").each(function () {
                    $(this).prop("checked", false);
                });
            });
            $.ajax({
                async : false,
                url: "includes/functions/bdd2web/func.5.2-rechercheInfoRemuneration.php",
                type: "POST",
                data: {
                    //idIntervention: idIntervention_5_2,
                    idIntervenant: idIntervenant_5_2
                },
                dataType: "json",
            }).done(function (data) {
                if (!$.trim(data)) {
                    return false;
                }
                $('#zsRemuneration').empty().append(
                    $('<option>').html('Choisir une rémunération')
                );
                $.each(data.remunerations, function (key, infoRemuneration) {
                    $('#zsRemuneration').append(
                        $('<option>')
                            .attr('value', infoRemuneration.salaireBrut + '_' + infoRemuneration.tarifKM + '_' + infoRemuneration.fraisRepas + '_' + infoRemuneration.fraisTelephone + '_' +
                                infoRemuneration.autorisationFraisTel + '_' + infoRemuneration.forfaitJournalier + '_' + infoRemuneration.boolVehiculeFonction + '_' +
                                infoRemuneration.boolVehiculePersonnel + '_' + infoRemuneration.forfaitJournalierNet)
                            .html(infoRemuneration.nbIntervention + ' intervention(s) - Taux horaire : ' + infoRemuneration.salaireBrut + ' - Tarif KM : ' + infoRemuneration.tarifKM + ' - Frais repas : ' + infoRemuneration.fraisRepas + ' - Frais Tel : ' + infoRemuneration.fraisTelephone + ' - Véhicule Perso : ' + infoRemuneration.boolVehiculePersonnel + ' - Véhicule fonction : ' + infoRemuneration.boolVehiculeFonction)
                    )
                });

                $('#zsRemuneration').change(function () {

                    if (boolAuto == 'OUI') {
                        var infoTarification = $(this).attr('value');
                        infoTarification = infoTarification.split('_');
                        $('#ztTauxHoraire').val(infoTarification[0]);
                    } else {
                        var infoTarification = $(this).attr('value');
                        infoTarification = infoTarification.split('_');
                        $('#ztTauxHoraire').val(infoTarification[0]);
                        $('#ztForfaitBrut').val(infoTarification[5]);
                        $('#ztForfaitNet').val(infoTarification[8]);
                        $('#ztForfaitRepas').val(infoTarification[2]);
                        $('#ztForfaitTel').val(infoTarification[3]);
                        $('#ztTarifKM').val(infoTarification[1]);

                        $('#zsTelephone option[value="' + infoTarification[4] + '"]').prop('selected', true);
                        $('#zsVehiculePerso option[value="' + infoTarification[7] + '"]').prop('selected', true);
                        $('#zsVehiculeFonction option[value="' + infoTarification[6] + '"]').prop('selected', true);
                    }
                });
            });
        });
    },

    validationContrat: function (boolConfirme) {
        var listeIntervention = ClassContrat.lstInterventionSel();
        if (listeIntervention.length > 0) {

            $.ajax({
                async : false,
                url: "includes/functions/bdd2web/func.5.2-validationContrat.php",
                type: "POST",
                data: {
                    ztTauxHoraire: (boolAuto == 'OUI' ? '0' : $('#ztTauxHoraire').val()),
                    ztForfaitBrut: (boolAuto == 'OUI' ? '0' : $('#ztForfaitBrut').val()),
                    ztForfaitNet: (boolAuto == 'OUI' ? $('#ztForfaitNet').val() : '0'),
                    ztForfaitRepas: (boolAuto == 'OUI' ? '0' : $('#ztForfaitRepas').val()),
                    ztForfaitTel: (boolAuto == 'OUI' ? '0' : $('#ztForfaitTel').val()),
                    ztTarifKM: (boolAuto == 'OUI' ? '0' : $('#ztTarifKM').val()),
                    zsTelephone: (boolAuto == 'OUI' ? '0' : $('#zsTelephone').val()),
                    zsVehiculePerso: (boolAuto == 'OUI' ? 'NON' : $('#zsVehiculePerso').val()),
                    zsVehiculeFonction: (boolAuto == 'OUI' ? 'NON' : $('#zsVehiculeFonction').val()),
                    listeIntervention: listeIntervention,
                    boolConfirme: boolConfirme,
                    boolAuto: (boolAuto == 'OUI' ? 'OUI' : 'NON'),
                    boolFraisInclus: (boolAuto == 'OUI' ? $('#zsFraisInclus').val() : 'NON')
                },
                dataType: "json",
            }).done(function (data) {
                if (boolConfirme == 0) {

                    if (data.mission.libelleMission === undefined) {

                        swal({
                            title: 'Erreur Data mission',
                            html: true,
                            text: 'Liste des interventions : ' + listeIntervention + '<br/>' +
                            'Merci de communiquer une impression d\'écran à PAGE UP.<br/>' +
                            '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#ztTauxHoraire').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#ztForfaitBrut').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? $('#ztForfaitNet').val() : '0') + '<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#ztForfaitRepas').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#ztForfaitTel').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#ztTarifKM').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? '0' : $('#zsTelephone').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? 'NON' : $('#zsVehiculePerso').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? 'NON' : $('#zsVehiculeFonction').val()) + '<br/>' +
                            (boolAuto == 'OUI' ? $('#zsFraisInclus').val() : 'OUI') + '<br/>' +
                            listeIntervention + '<br/>' +
                            boolConfirme + '<br/>' +
                            (boolAuto == 'OUI' ? 'OUI' : 'NON'),
                            type: 'error'
                        });

                        //$('.btn-validation-contrat').trigger('click');
                        return false;
                    }
                    if (data.boolDate == 0) {
                        $('#modalConfirmContrat').modal('show');
                        $('.detail-contrat').empty().append(
                            $('<div>').addClass('alert alert-success').html('Les donn&eacute;es saisies sont correctes. Veuillez v&eacute;rifier les informations suivantes pour valider le contrat')
                        );

                        // -
                        if (data.dureeHeureSupp == 1) {
                            $('.detail-heures').empty().append(
                                $('<div>').addClass('alert alert-danger').html('Attention, l\'ajout de ce contrat provoquera des heures supplémentaires..')
                            );
                            toastr.error('Détection d\heures supplémentaires', 'ATTENTION');
                        }
                        else {
                            $('.detail-heures').empty();
                        }

                        // -
                        if (data.sejour.color != '') {
                            $('.detail-sejour').empty().append(
                                $('<div>').addClass('alert alert-' + data.sejour.color).html(data.sejour.txt)
                            );
                            toastr.error('Vérifier la date du titre de séjour', 'ATTENTION');
                        }
                        else {
                            $('.detail-sejour').empty();
                        }

                        $('.detail-contrat').append($('<h3>').html('Informations principales')).append($('<hr>'));

                        if (boolAuto == 'OUI') {
                            $('.detail-contrat').append(
                                $('<table>').addClass('table table-bordered table-striped')
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Mission :</strong> ' + data.mission.libelleMission))
                                            .append($('<td>').html('<strong>Intervenant :</strong> ' + data.intervenant.nomIntervenant + ' ' + data.intervenant.prenomIntervenant))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').attr('colspan', 2).html('<strong>' + data.infoSaisi.listeIntervention.length + ' intervention(s)</strong> pour une durée total de <strong>' + data.dureeTotalFinal + '</strong>'))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').attr('colspan', 2).html('<strong>Forfait journalier Net</strong> ' + data.infoSaisi.ztForfaitNet + ' €'))
                                    )
                            );
                        } else {
                            $('.detail-contrat').append(
                                $('<table>').addClass('table table-bordered table-striped')
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Mission :</strong> ' + data.mission.libelleMission))
                                            .append($('<td>').html('<strong>Intervenant :</strong> ' + data.intervenant.nomIntervenant + ' ' + data.intervenant.prenomIntervenant))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').attr('colspan', 2).html('<strong>' + data.infoSaisi.listeIntervention.length + ' intervention(s)</strong> pour une durée total de <strong>' + data.dureeTotalFinal + '</strong>'))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Taux horaire :</strong> ' + data.infoSaisi.ztTauxHoraire + ' €'))
                                            .append($('<td>').html('<strong>Forfait journalier Brut</strong> ' + data.infoSaisi.ztForfaitBrut + ' €'))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Frais repas :</strong> ' + data.infoSaisi.ztForfaitRepas + ' €'))
                                            .append($('<td>').html('<strong>Frais téléphone :</strong> ' + data.infoSaisi.ztForfaitTel + ' €'))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Tarif KM :</strong> ' + data.infoSaisi.ztTarifKM + ' €'))
                                            .append($('<td>').html('<strong>Forfait Téléphonique :</strong> ' + data.infoSaisi.zsTelephone))
                                    )
                                    .append(
                                        $('<tr>')
                                            .append($('<td>').html('<strong>Véhicule personnel :</strong> ' + data.infoSaisi.zsVehiculePerso))
                                            .append($('<td>').html('<strong>Véhicule de fonction :</strong> ' + data.infoSaisi.zsVehiculeFonction))
                                    )
                            );
                        }

                        $('.detail-contrat').append($('<h3>').html('Informations sur les interventions')).append($('<hr>'));
                        $('.detail-contrat').append(
                            $('<table>').addClass('table table-bordered table-striped table-inter')
                        );

                        $.each(data.listeIntervention, function (key, infoInter) {
                            $('.table-inter').append(
                                $('<tr>')
                                    .append($('<td>').html(infoInter.libellePdv + ' - ' + infoInter.codePostalPdv + ' ' + infoInter.villePdv))
                                    .append($('<td>').html(infoInter.dteIntervention + ' de ' + infoInter.dteHDIntervention + ' à ' + infoInter.dteHFIntervention))
                            );
                        });

                        $('.btn-valid-final-contrat').unbind().bind('click', function () {
                            ClassContrat.validationContrat(1);
                        });
                    }

                    else {
                        toastr.error('Il semble y avoir des dates sur des mois différents !', 'ERREUR - Création d\'un contrat');
                    }
                }

                else {
                    document.location.href = 'index.php?ap=5.0-Contrat&ss_m=5.4-FicheContrat&s=' + data.idContrat;
                }
            });

        } else {
            toastr.error('Vous devez sélectionner au minimum une intervention', 'ERREUR - Création d\'un contrat');
        }
    },

    rechercheSessionModule: function () {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: {
                idModule: '5_2',
                dateRechercheD_5_2: '',
                dateRechercheF_5_2: '',
                idMission_5_2: 'ALL',
                idIntervenant_5_2: 'ALL',
                idIntervention_5_2: ''
            },
            dataType: "json",
        }).done(function (data) {

            $('#dateRechercheD_5_2').val(data.sessions.dateRechercheD_5_2);
            $('#dateRechercheF_5_2').val(data.sessions.dateRechercheF_5_2);

            idMission_5_2 = data.sessions.idMission_5_2;
            idIntervenant_5_2 = data.sessions.idIntervenant_5_2;
            dateRechercheD_5_2 = data.sessions.dateRechercheD_5_2;
            dateRechercheF_5_2 = data.sessions.dateRechercheF_5_2;
            idIntervention_5_2 = data.sessions.idIntervention_5_2;

            if (idIntervention_5_2 != '') {
                ClassContrat.rechercheInfoRemuneration();
            } else {
                $('.etape-3-au').hide();
                $('.etape-3-no-au').hide();
                $('.btn-sel-all, .btn-unsel-all').hide();
                if (idIntervenant_5_2 != '') {

                }
            }
        });
    },

    lstInterventionSel: function () {
        var listIntervention = new Array();
        tableIntervention_5_2.api().search('').draw();
        var lstCheckbox = tableIntervention_5_2.$(".chk:checked", {"page": "all"});
        lstCheckbox.each(function (index, elem) {
            listIntervention.push($(elem).val());
        });

        //console.log(listIntervention);
        return listIntervention;
    }
}