<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Etape 1 - Recherche des interventions sans contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group input-daterange">
                            <label class="control-label col-md-2">Date début :</label>
                            <div class="col-md-4">
                                <input id="dateRechercheD_5_2" class="form-control form-recherche" type="text" placeholder="" name="start" style="text-align: left" value="" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <label class="control-label col-md-2">Date fin :</label>
                            <div class="col-md-4">
                                <input id="dateRechercheF_5_2" class="form-control form-recherche" type="text" placeholder="" name="end" style="text-align: left" value="" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Mission :</label>
                            <div class="col-md-4">
                                <select id="idMission_5_2" name="idMission_5_2" class="form-control form-recherche">

                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>

                            <label class="control-label col-md-2">Intervenant :</label>
                            <div class="col-md-4">
                                <select id="idIntervenant_5_2" name="idIntervenant_5_2" class="form-control form-recherche">

                                </select>
                                <span class="help-block"> Obligatoire </span>

                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn bg bg-blue btn-sel-all">Tout sélectionner</button>
                        <button class="btn bg bg-blue btn-unsel-all">Tout dé-sélectionner</button>
                        <button class="btn btn-primary pull-right btn-recherche-date"><i class="fa fa-search"></i> Rechercher</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Etape 2 - Choix des interventions</h3><hr>
    </div>
</div>
<table class="table table-striped table-bordered table-hover" id="tableIntervention_5_2" >
    <thead>
    <tr>
        <th>#</th>
        <th>Libellé campagne</th>
        <th>Libellé mission</th>
        <th>Intervante</th>
        <th>Libellé PDV</th>
        <th>Date</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div class="row etape-3-no-au">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Etape 3 - Informations sur le contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">
                        <br/>
                        <div class="alert alert-info" style="padding: 5px;margin-left: 10px;margin-top: -5px;margin-right: 10px;margin-bottom: 0px;">
                            Attention, pour les frais téléphoniques, si vous laissez à NON, le montant, au moment de l'enregistrement sera remise à 0, quelle que soit la valeur que vous avez saisie.<br/>
                            Si vous saisissez un montant, alors frais téléphonique passera automatiquement à OUI.<br/><br/>
                            <i class="fa fa-warning"></i>&nbsp;Seules les rémunération des contrats non archivés sont disponibles.
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <label class="control-label ">Ancienne rémunération :</label>

                                <select class="form-control" id="zsRemuneration">

                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label">Taux horaire :</label>
                                <input id="ztTauxHoraire" class="form-control" type="text" placeholder="" value="0" />
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Forfait Journalier Brut :</label>
                                <input id="ztForfaitBrut" class="form-control" type="text"  value="0" />
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Forfait Repas :</label>

                                <input id="ztForfaitRepas" class="form-control" type="text"  value="0" />
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-4">
                                <label class="control-label">Téléphone :</label>

                                <select class="form-control" id="zsTelephone">
                                    <option value="OUI">OUI</option>
                                    <option value="NON" selected>NON</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Forfait téléphonique :</label>

                                <input id="ztForfaitTel" class="form-control" type="text"  value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label ">Véhicule Personnel :</label>

                                <select class="form-control" id="zsVehiculePerso">
                                    <option value="OUI" selected>OUI</option>
                                    <option value="NON">NON</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Véhicule Fonction :</label>
                                <select class="form-control" id="zsVehiculeFonction">
                                    <option value="OUI">OUI</option>
                                    <option value="NON" selected>NON</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Tarif KM :</label>

                                <input id="ztTarifKM" class="form-control" type="text"  value="0" />
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <button class="btn bg bg-green btn-validation-contrat pull-right">Valider le contrat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row etape-3-au">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Etape 3 - Informations sur le contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">
                        <div class="form-group ">
                            <div class="col-md-4">
                                <label class="control-label">Forfait Journalier Net + Frais Inclus :</label>
                                <input id="ztForfaitNet" class="form-control" type="text"  value="0" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Frais inclus :</label>
                                <div>
                                    <select class="form-control" id="zsFraisInclus" name="zsFraisInclus">
                                        <option value="OUI">OUI</option>
                                        <option value="NON">NON</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn bg bg-green btn-validation-contrat pull-right">Valider le contrat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="modalConfirmContrat" class="modal fade" tabindex="-1">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Validation d'un contrat</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal detail-heures">
                </div>
                <div class="form-horizontal detail-sejour">
                </div>
                <div class="form-horizontal detail-contrat">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-final-contrat">Valider la création du contrat</button>
            </div>
        </div>
    </div>
</div>