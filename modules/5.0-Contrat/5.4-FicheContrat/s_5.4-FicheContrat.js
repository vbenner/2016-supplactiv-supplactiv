var tableContrat_5_1;
var idContrat_5_4;
var isAU;
jQuery(document).ready(function () {
    idContrat_5_4 = $('#zt_idContrat_5_4').val();
    ClassContrat.init();
});

var ClassContrat = {
    init: function () {
        this.rechercheInfoContrat();

        $('.alerte').bind('change', function () {
            $('.btn-export-contrat').removeClass('btn-primary').addClass('btn-danger');
        });

        $('.btn-export-contrat').bind('click', function () {
            $('.btn-export-contrat').removeClass('btn-danger').addClass('btn-primary');
            $.ajax({
                async : false,
                url: "includes/functions/web2bdd/func.5.4-modificationInfoContrat.php",
                type: "POST",
                data: {
                    idContrat: idContrat_5_4,
                    societeA: $((isAU ? '.au ' : '') + '#ztSA').val(),
                    societeB: $((isAU ? '.au ' : '') + '#ztSB').val(),
                    detail: $((isAU ? '.au ' : '') + '#ztDetail').val(),
                    pTxH: $('#ztTauxHoraire').val(),
                    pForfaitJ: $('#ztForfaitJournalier').val(),
                    pForfaitJN: $('#ztForfaitJournalierNet').val(),
                    pForfaitTel: $('#ztForfaitTel').val(),
                    pForfaitRepas: $('#ztForfaitRepas').val(),
                    pCoutKm: $('#ztCoutKM').val(),

                    boolFraisInclus: $((isAU ? '.au ' : '') + '#zsFraisInclus').val(),


                },
                dataType: 'json',
            }).done(function (data) {
                window.open('download/func.5.4-exportContrat.php?idContrat=' + idContrat_5_4);
            });
        });
    },

    rechercheInfoContrat: function () {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.4-rechercheInfoContrat.php",
            type: "POST",
            data: {
                idContrat: idContrat_5_4
            },
            dataType: 'json'
        }).done(function (data) {

            var typePdv = '';
            $.each(data.typePdv, function (libelleTypePdv, nb) {
                typePdv += (typePdv == '') ? nb + ' PDV de type ' + libelleTypePdv : ', ' + nb + ' PDV de type ' + libelleTypePdv;
            });

            ClassContrat.rechercheInterventionDispo(data.contrat.FK_idIntervenant, data.contrat.FK_idMission, data.interventions[0].dateDebut);

            $('#tableIntervention_5_4 tbody').empty();
            $.each(data.interventions, function (i, InfoIntervention) {
                $('#tableIntervention_5_4 tbody').append(
                    $('<tr>')
                        .append($('<td>').html($('<button>').addClass("btn bg bg-blue btn-xs btn-block").html('<i class="fa fa-file"></i> Doc Magasin').unbind().bind('click', function () {
                            ClassContrat.documentationEntreeMagasin(InfoIntervention.idIntervention);
                        })))
                        .append($('<td>').html(InfoIntervention.idIntervention))
                        .append($('<td>').html(InfoIntervention.libellePdv))
                        .append($('<td style="white-space:nowrap;">').append(
                            '<span id="cell_' + InfoIntervention.idIntervention + '">Le ' + InfoIntervention.dteInter + ' de ' + (InfoIntervention.heureD).replace('.', 'H') + ' à ' + (InfoIntervention.heureF).replace('.', 'H') + '</span>&nbsp;')
                            .append($('<button>')
                                .attr('data-id', InfoIntervention.idIntervention)
                                .addClass('btn bg bg-yellow-casablanca btn-xs').html('<i class="fa fa-edit"></i> Modif').on('click', function () {
                                    ClassContrat.modifieDateIntervention(InfoIntervention.idIntervention);
                                })//)
                            ))
                        /**
                         * <a href="#" id="username" data-type="text" data-pk="1" data-url="/post" data-title="Enter username">superuser</a>
                         */
                        .append($('<td>').html(InfoIntervention.commentairePdv_EM))
                        .append($('<td>').html($('<button>').addClass("btn bg bg-red btn-xs btn-block").html('<i class="fa fa-trash-o"></i> Supprimer').unbind().bind('click', function () {
                            ClassContrat.afficheDetailIntervention(InfoIntervention.idIntervention, false)
                        })))
                )
            });

            /** -----------------------------------------------------------------------------------
             * Ajout de la possibilité de modifier les dates
             */
            $('#tableContrat_5_4 tbody').empty().append(
                $('<tr>')
                    .append($('<td>').html(data.contrat.idContrat))
                    .append($('<td>').html(data.contrat.nomIntervenant + ' ' + data.contrat.prenomIntervenant))
                    .append($('<td>').html(data.contrat.libelleCampagne))
                    .append($('<td>').html(data.contrat.libelleTypeMission))
                    .append($('<td>').html(typePdv))
                    .append($('<td>').html(data.duree))
                    .append($('<td>').html(data.contrat.autorisationFraisTel))
                    .append($('<td>').html(data.contrat.boolVehiculePersonnel == 'OUI' ? 'Véhicule Personnel' : 'Véhicule Fonction'))
            );


            $('#ztTauxHoraire').val(data.contrat.salaireBrut);
            $('#ztForfaitJournalier').val(data.contrat.forfaitJournalier);
            $('#ztForfaitJournalierNet').val(data.contrat.forfaitJournalierNet);
            $('#zsFraisInclus').val(data.contrat.boolFraisInclus);
            $('#ztCoutKM').val(data.contrat.tarifKM);
            $('#ztForfaitTel').val(data.contrat.fraisTelephone);
            $('#ztForfaitRepas').val(data.contrat.fraisRepas);

            /** -----------------------------------------------------------------------------------
             *
             */
            if (data.contrat.boolAutoEntrepreneuse == 'OUI') {
                $('.no-au').hide();
                $('.au #ztSA').val(data.contrat.societeA);
                $('.au #ztSB').val(data.contrat.societeB);
                $('.au #ztDetail').val(data.contrat.detailContrat);
                isAU = true;
            } else {
                $('.au').hide();
                $('#ztSA').val(data.contrat.societeA);
                $('#ztSB').val(data.contrat.societeB);
                $('#ztDetail').val(data.contrat.detailContrat);
                isAU = false;
            }
        });
    },

    rechercheInterventionDispo: function (idIntervenant, idMission, dateDebut) {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.4-rechercheInterventionDisponible.php",
            type: "POST",
            data: {
                idIntervenant: idIntervenant,
                idMission: idMission,
                dateDebut: dateDebut
            },
            dataType: 'json'
        }).done(function (data) {
            $('#tableAddIntervention_5_4 tbody').empty();
            if (data !== null) {
                $.each(data.interventions, function (i, InfoIntervention) {
                    $('#tableAddIntervention_5_4 tbody').append(
                        $('<tr>')
                            .append($('<td>').html(InfoIntervention.idIntervention))
                            .append($('<td>').html(InfoIntervention.libelleCampagne))
                            .append($('<td>').html(InfoIntervention.libelleMission))
                            .append($('<td>').html(InfoIntervention.nomIntervenant + ' ' + InfoIntervention.prenomIntervenant))
                            .append($('<td>').html(InfoIntervention.libellePdv))
                            .append($('<td>').html(InfoIntervention.dteInter))
                            .append($('<td>').html($('<button>').addClass("btn bg bg-blue btn-xs btn-block").html('<i class="fa fa-plus"></i> Ajouter').unbind().bind('click', function () {
                                ClassContrat.ajoutInterventionContrat(InfoIntervention.idIntervention);
                            })))
                    )
                });
            }
        });
    },

    ajoutInterventionContrat: function (idIntervention) {
        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.5.4-ajoutInterventionContrat.php",
            type: "POST",
            data: {
                idIntervention: idIntervention,
                idContrat: idContrat_5_4
            },
            dataType: 'json'
        }).done(function (data) {
            ClassContrat.rechercheInfoContrat();
            toastr.success('L\'intervention a bien été ajoutée, pensez à re-télécharger le contrat.', "Ajout d'une intervention");
        });
    },

    documentationEntreeMagasin: function (idIntervention) {
        window.open('download/func.5.4-exportDocMagasin.php?idIntervention=' + idIntervention);
    },

    /** -------------------------------------------------------------------------------------------
     * Pour modifier une date d'intervention, on se contente d'afficher un MODAL
     */
    modifieDateIntervention: function (idIntervention) {

        idInterventionSel = idIntervention;
        $.ajax({
            async : false,
            type: "POST",
            data: {idIntervention: idIntervention},
            url: "includes/functions/bdd2web/func.4.5-rechercheInformationIntervention.php",
            dataType: 'json',
            success: function (data) {

                var oldDate = data.info.dateIntervention;
                var oldHDeb = data.info.heureDebut;
                var oldHFin = data.info.heureFin;
                var idPdv = data.info.idPdv;
                var idIntervenant = data.info.FK_idIntervenant;


                /** -------------------------------------------------------------------------------
                 * Si on clique sur VALIDER, on aura besoin de modifier la date et l'heure de la
                 * cellule dans laquelle on était SANS recharger la page
                 */
                $('.btn-modif-date-intervention').unbind().bind('click', function () {

                    var newDate = $('#ztDateNew').val();
                    var newHDeb = $('#ztHeureDNew').val();
                    var newHFin = $('#ztHeureFNew').val();

                    if (newDate == '' || newHDeb == '' || newHFin == '') {
                        return false;
                    }

                    $.ajax({
                        async : false,
                        type: "POST",
                        data: {
                            idIntervention: idIntervention,
                            dateIntervention: newDate,
                            heureD: newHDeb,
                            heureF: newHFin,
                            idPdv: idPdv,
                            idIntervenant:idIntervenant,
                        },
                        url: "includes/functions/web2bdd/func.4.5-modificationIntervention.php",
                        dataType: 'json',
                        success: function (data) {
                            toastr.options = {
                                timeOut : 10000
                            };
                            $('#modalModifDateIntervention').modal('hide');
                            if (data.heuresupp == 1) {
                                $('#cell_' + idIntervention).html('Le ' + newDate + ' de ' + (newHDeb).replace(':', 'H') + ' à ' + (newHFin).replace(':', 'H'));
                                toastr.error('La  date d l\'intervention <strong>' + idIntervention + '</strong> a bien été modifiée.<br/>Des heures supplémentaires ont été détectées.', "Modification d'une intervention");
                            }
                            else {
                                if (data.error == 1) {
                                    toastr.error('Les dates / heures ne permettent pas la modification <strong>' + idIntervention + '</strong>' + '<br>' + data.info, "Modification d'une intervention");
                                }
                                else {
                                    $('#cell_' + idIntervention).html('Le ' + newDate + ' de ' + (newHDeb).replace(':', 'H') + ' à ' + (newHFin).replace(':', 'H'));
                                    toastr.success('La  date d l\'intervention <strong>' + idIntervention + '</strong> a bien été modifiée', "Modification d'une intervention");
                                }
                            }
                        }
                    });

                });

                $('#ztDateNew').val(oldDate);
                $('#ztHeureDNew').val(oldHDeb);
                $('#ztHeureFNew').val(oldHFin);

                $('#ztDateNew').datepicker({
                    format: 'dd/mm/yyyy',
                    clearBtn: false
                });
                $('#ztHeureDNew, #ztHeureFNew').timepicker({
                    format: 'hh:mm',
                    showMeridian: false,
                    minuteStep: 1
                });

                $('#modalModifDateIntervention').modal('show');
            }
        });

    },

    afficheDetailIntervention: function (idIntervention, boolSup) {

        $('.detail-mod .col-md-12').empty();
        idInterventionSel = idIntervention;
        $.ajax({
            async : false,
            type: "POST",
            data: {idIntervention: idIntervention},
            url: "includes/functions/bdd2web/func.4.5-rechercheInformationIntervention.php",
            dataType: 'json',
            success: function (data) {

                idPdvSel = data.info.idPdv;
                idIntervenantSel = data.info.FK_idIntervenant;

                if (boolSup) {
                    $('.detail-sup .col-md-12').html("<div class='alert alert-success'>Les demandes de suppression ont bien été enregistrées</div>")
                } else $('.detail-sup .col-md-12').html("");

                $('#zrIntervenant').removeAttr('disabled');
                $("#zrIntervenant").select2("destroy");

                $('#zrCampagne').val(data.info.libelleCampagne);
                $('#zrMission').val(data.info.libelleMission);
                $('#zrPdv').val(data.info.libellePdv + ' ' + data.info.codePostalPdv + ' - ' + data.info.villePdv);
                $('#zrContrat').val(data.info.FK_idContrat);

                $('#zrIntervenant').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un intervenant')
                );

                $.each(data.intervenants, function (i, InfoIntervenant) {
                    $('#zrIntervenant').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant + ' - ' + InfoIntervenant.nomIntervenant + ' ' + InfoIntervenant.prenomIntervenant)
                            .prop('selected', (InfoIntervenant.idIntervenant == data.info.FK_idIntervenant) ? true : false)
                    );
                });


                $('#zrDate').val(data.info.dateIntervention);
                $('#zrHeureD').val(data.info.heureDebut);
                $('#zrHeureF').val(data.info.heureFin);

                $('#zrDate').attr('disabled', 'true');
                $('.timepicker-24').attr('disabled', 'true');

                $('#zrIntervenant').attr('disabled', true);
                $('.btn-modif-intervention').hide().unbind();


                /** Bouton de suppression d'une intervention */
                $('.btn-sup-intervention').unbind().bind('click', function () {

                    /** Ouverture d'une modal permettant de demander les suppressions a faire */
                    $('#modalConfirmSuppression').modal('show');
                    $('#titre-sup').css('color', 'black');

                    /** Validation d'une suppression */
                    $('.btn-valid-suppression').unbind().bind('click', function () {

                        var boolContrat = boolDate = boolAll = false;
                        boolContrat = true;
                        boolDate = ($('#chk-del-a').prop('checked')) ? true : false;
                        boolAll = ($('#chk-del-c').prop('checked')) ? true : false;

                        /** On test s'il existe 1 des 3 options */
                        if (boolContrat || boolDate || boolAll) {

                            $.ajax({
                                async : false,
                                type: "POST",
                                data: {
                                    idIntervention: idInterventionSel,
                                    boolContrat: boolContrat,
                                    boolDate: boolDate,
                                    boolAll: boolAll
                                },
                                url: "includes/functions/web2bdd/func.4.5-suppressionIntervention.php",
                                dataType: 'json',
                                success: function (data) {
                                    /** On ferme les modals */
                                    $('#modalConfirmSuppression').modal('hide');


                                    /** On rafraichit le tableau */
                                    ClassContrat.rechercheInfoContrat();

                                    $('#modalDetailIntervention').modal('hide');
                                    toastr.success('L\'intervention <strong>' + idIntervention + '</strong> a bien été supprimé', "Suppression d'une intervention");

                                }
                            });
                        }

                        /** Sinon affichage d'un message d'erreur */
                        else {
                            $('#modalConfirmSuppression').effect('shake');
                            $('#titre-sup').css('color', 'red');
                        }
                    });
                });


                $('#modalDetailIntervention').modal('show');
            }
        });
    }
}