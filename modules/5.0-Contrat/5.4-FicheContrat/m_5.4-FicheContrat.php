<input type="hidden" id="zt_idContrat_5_4"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<table class="table table-striped table-bordered table-hover" id="tableContrat_5_4" >
    <thead>
    <tr>
        <th>ID Contrat</th>
        <th>Intervenant</th>
        <th>Campagne</th>
        <th>Type Mission</th>
        <th>Point de vente</th>
        <th>Durée</th>
        <th>Tel.</th>
        <th>Veh.</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Modification / téléchargement du contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">

                    <!-- Modification 2016-09-14
                     Suppression des readonly sur les zones de tarification du contrat
                     -->
                    <div class="form-body no-au">
                        <div class="form-group ">
                            <div class="col-md-3">
                                <label class="control-label">Taux horaire :</label>
                                <input id="ztTauxHoraire" class="form-control alerte" type="text" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Forfait Journalier :</label>
                                <input id="ztForfaitJournalier" class="form-control alerte" type="text"  />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Forfait Tel :</label>
                                <input id="ztForfaitTel" class="form-control alerte" type="text"  />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Forfait Repas :</label>
                                <input id="ztForfaitRepas" class="form-control alerte" type="text"  />
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-3">
                                <label class="control-label">Coût Kilométrique :</label>
                                <input id="ztCoutKM" class="form-control alerte" type="text"  />
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">Présentation des produits de la société :</label>
                                <input id="ztSA" name="ztSA" class="form-control" type="text"  />
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Pour le compte de la société :</label>
                                <input id="ztSB" name="ztSB" class="form-control" type="text"  />
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <label class="control-label">Détail :</label>
                                <textarea id="ztDetail" class="form-control" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-body au">
                        <div class="form-group ">
                            <div class="col-md-3">
                                <label class="control-label">Forfait Journalier Net :</label>
                                <input id="ztForfaitJournalierNet" class="form-control" type="text" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Frais inclus :</label>
                                <div>
                                    <select class="form-control" id="zsFraisInclus" name="zsFraisInclus">
                                        <option value="NON">NON</option>
                                        <option value="OUI">OUI</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">Présentation des produits de la société :</label>
                                <input id="ztSA" name="ztSA" class="form-control" type="text"  />
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Pour le compte de la société :</label>
                                <input id="ztSB" name="ztSB" class="form-control" type="text"  />
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <label class="control-label">Détail :</label>
                                <textarea id="ztDetail" class="form-control" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn btn-primary pull-right btn-export-contrat"><i class="fa fa-file"></i> Télécharger le contrat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableIntervention_5_4" >
    <thead>
    <tr>
        <th>Document</th>
        <th>#</th>
        <th>Point de vente</th>
        <th>Date intervention</th>
        <th>Commentaire</th>
        <th>Supprimer</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<h3>Ajouter des interventions au contrat</h3><hr>
<table class="table table-striped table-bordered table-hover" id="tableAddIntervention_5_4" >
    <thead>
    <tr>
        <th>#</th>
        <th>Libell&eacute; Campagne</th>
        <th>Libell&eacute; Mission</th>
        <th>Intervenante</th>
        <th>Libell&eacute; PDV</th>
        <th>Date Inter.</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div id="modalModifDateIntervention" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content" style="width: 600px">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modification de la date</h4>
            </div>

            <div class="modal-body" style="padding: 0px !important;">

                <div class="form-horizontal form-bordered form-row-stripped">
                    <div class="row detail-sup">
                        <div class="col-md-12">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Date :</label>
                                <div class="input-icon left">
                                    <input id="ztDateNew" class="form-control" type="text" readonly="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Heure début :</label>
                                <div class="input-icon left">
                                    <input id="ztHeureDNew" class="form-control timepicker timepicker-24" type="text">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Heure Fin :</label>
                                <div class="input-icon left">
                                    <input id="ztHeureFNew" class="form-control timepicker timepicker-24" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn pull-left">Fermer</button>
                <button type="button" class="btn yellow-casablanca btn-modif-date-intervention">Modifier</button>
            </div>
        </div>
    </div>
</div>

<div id="modalDetailIntervention" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Détail d'une intervention</h4>
            </div>
            <div class="modal-body" style="padding: 0px !important;">
                <div class="form-horizontal form-bordered form-row-stripped">
                    <div class="row detail-sup">
                        <div class="col-md-12">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">Campagne :</label>
                                <div class="col-md-10">
                                    <div class="input-icon left">
                                        <input type="text" class="form-control" readonly id="zrCampagne"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">Mission :</label>
                                <div class="col-md-10">
                                    <div class="input-icon left">
                                        <input type="text" class="form-control" readonly id="zrMission"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">Point de vente :</label>
                                <div class="col-md-10">
                                    <div class="input-icon left">
                                        <input type="text" class="form-control" readonly id="zrPdv"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">Contrat :</label>
                                <div class="col-md-4">
                                    <div class="input-icon left">
                                        <input type="text" class="form-control" readonly id="zrContrat"/>
                                    </div>
                                </div>
                                <label class="control-label col-md-2">Intervenant :</label>
                                <div class="col-md-4">
                                    <div class="input-icon left">
                                        <select class="form-control" id="zrIntervenant">

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">

                                <div class="col-md-4">
                                    <label class="control-label">Date :</label>
                                    <div class="input-icon left">
                                        <input type="text" class="form-control" readonly id="zrDate"/>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Heure début :</label>
                                    <div class="input-icon left">
                                        <input type="text" class="form-control timepicker timepicker-24" readonly id="zrHeureD" value="10:00"/>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Heure Fin :</label>
                                    <div class="input-icon left">
                                        <input type="text" class="form-control timepicker timepicker-24" readonly id="zrHeureF" value="18:00"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn pull-left">Fermer</button>
                <button type="button" class="btn red btn-sup-intervention">Supprimer</button>
                <button type="button" class="btn green btn-modif-intervention">Modifier</button>
            </div>
        </div>
    </div>
</div>

<div id="modalConfirmSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'une intervention</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">

                    <h3 id="titre-sup">Veuillez choisir les informations à supprimer</h3>


                    <div class=" well">
                        <input type="checkbox" class="form-control" id="chk-del-a" value="delDate"/> Suppression de la date d'intervention (si planifiée)
                    </div>
                    <div class=" well">
                        <input type="checkbox" class="form-control" id="chk-del-c" value="delAll"/> Suppression complète de l'intervention
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-suppression">Valider la suppression</button>
            </div>
        </div>
    </div>
</div>
