
<?php

$MoisConvention = date('m');
$AnneeConvention = date('Y');
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    1 - Informations sur le contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped" >
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-2">Intervenant :</label>
                            <div class="col-md-10">
                                <select id="Intervenant_Recherche" class="form-control">
                                    <option value="all">Choisir un intervenant</option>
                                    <?php

                                    $sqlRechercheIntervenant = "
                                    SELECT idIntervenant, nomIntervenant, prenomIntervenant
                                    FROM su_intervenant
                                    WHERE idIntervenant <> 1
                                    ORDER BY nomIntervenant";

                                    $RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);
                                    $RechercheIntervenantExc->execute();
                                    while($InfoIntervenant= $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ)):
                                        ?>
                                        <option value="<?php echo $InfoIntervenant->idIntervenant ?>"><?php echo $InfoIntervenant->nomIntervenant.' '.$InfoIntervenant->prenomIntervenant ?></option>
                                    <?php
                                    endwhile;
                                    ?>
                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-4">
                                <label class="control-label">Durée mensuelle :</label>
                                <input type="text" class="form-control" id="DureeMensuelle" value="" />
                                <span class="help-block"> en heure </span>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">R&eacute;mun&eacute;ration brute :</label>
                                <input type="text" class="form-control" id="TauxHoraire" value="" />
                                <span class="help-block"> taux horaire </span>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Indemnit&eacute; forfaitaire :</label>
                                <input type="text" class="form-control" id="IndemniteF" value="" />
                                <span class="help-block"> Laisser vide si pas d'indemnité </span>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label class="control-label">Pour le mois de :</label>
                                <select id="MoisConvention" class="form-control">
                                    <option <?php echo ($MoisConvention == 'JANVIER' || $MoisConvention == '01')? 		'selected="selected"' : '' ?> value="JANVIER">JANVIER</option>
                                    <option <?php echo ($MoisConvention == 'FEVRIER' || $MoisConvention == '02')? 		'selected="selected"' : '' ?> value="FEVRIER">FEVRIER</option>
                                    <option <?php echo ($MoisConvention == 'MARS' || $MoisConvention == '03')? 		'selected="selected"' : '' ?> value="MARS">MARS</option>
                                    <option <?php echo ($MoisConvention == 'AVRIL' || $MoisConvention == '04')? 		'selected="selected"' : '' ?> value="AVRIL">AVRIL</option>
                                    <option <?php echo ($MoisConvention == 'MAI' || $MoisConvention == '05')? 			'selected="selected"' : '' ?> value="MAI">MAI</option>
                                    <option <?php echo ($MoisConvention == 'JUIN' || $MoisConvention == '06')? 		'selected="selected"' : '' ?> value="JUIN">JUIN</option>
                                    <option <?php echo ($MoisConvention == 'JUILLET' || $MoisConvention == '07')? 		'selected="selected"' : '' ?> value="JUILLET">JUILLET</option>
                                    <option <?php echo ($MoisConvention == 'AOUT' || $MoisConvention == '08')? 		'selected="selected"' : '' ?> value="AOUT">AOUT</option>
                                    <option <?php echo ($MoisConvention == 'SEPTEMBRE' || $MoisConvention == '09')? 	'selected="selected"' : '' ?> value="SEPTEMBRE">SEPTEMBRE</option>
                                    <option <?php echo ($MoisConvention == 'OCTOBRE' || $MoisConvention == '10')? 		'selected="selected"' : '' ?> value="OCTOBRE">OCTOBRE</option>
                                    <option <?php echo ($MoisConvention == 'NOVEMBRE' || $MoisConvention == '11')? 	'selected="selected"' : '' ?> value="NOVEMBRE">NOVEMBRE</option>
                                    <option <?php echo ($MoisConvention == 'DECEMBRE' || $MoisConvention == '12')? 	'selected="selected"' : '' ?> value="DECEMBRE">DECEMBRE</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">De l'année :</label>
                                <select id="AnneeConvention" class="form-control">
                                    <option <?php echo ($AnneeConvention == '2011')? 'selected="selected"' : '' ?> value="2011">2011</option>
                                    <option <?php echo ($AnneeConvention == '2012')? 'selected="selected"' : '' ?> value="2012">2012</option>
                                    <option <?php echo ($AnneeConvention == '2013')? 'selected="selected"' : '' ?> value="2013">2013</option>
                                    <option <?php echo ($AnneeConvention == '2014')? 'selected="selected"' : '' ?> value="2014">2014</option>
                                    <option <?php echo ($AnneeConvention == '2015')? 'selected="selected"' : '' ?> value="2015">2015</option>
                                    <option <?php echo ($AnneeConvention == '2016')? 'selected="selected"' : '' ?> value="2016">2016</option>
                                    <option <?php echo ($AnneeConvention == '2017')? 'selected="selected"' : '' ?> value="2017">2017</option>
                                    <option <?php echo ($AnneeConvention == '2018')? 'selected="selected"' : '' ?> value="2018">2018</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <a class="btn btn-primary pull-right" onclick="creationInformationContrat()">Créer le contrat</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    2 - Choix des points de vente
                </div>
            </div>
            <div class="portlet-body">


<!-- Element 2 - Tableau des interventions -->

        <table id="tableauPDV" class="table table-striped table-bordered table-hover" >
            <thead> 
                <tr> 
                    <th style="width:20px"></th> 
                    <th>Libell&eacute; du Point de vente</th>
                    <th>Code Postal - Ville</th>
                </tr> 
            </thead> 
            <tbody></tbody>
        </table>               
</div>
            </div>
        </div>
    </div>