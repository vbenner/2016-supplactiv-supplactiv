
var tableauPDV;

$(function() {



    tableauPDV = $('#tableauPDV').dataTable({
        "columnDefs": [{
            "orderable": true,
            "targets": [1]
        }],
        "order": [
            [1, 'asc']
        ],
        "lengthMenu": [
            [ -1],
            [ "Tout"]
        ],
        "pageLength": -1,
        bProcessing: true,
        bDefferRender: true,
        "bRetrieve": true,
        sAjaxSource: "includes/functions/datatable/func.6.7-ListePdvConventionNv.php",
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        }
    });
    var tableWrapper = $('#tableauPDV_wrapper');
    tableWrapper.find('.dataTables_length select').select2();

	//Select COMBO Pour l'intervenant
    $("#Intervenant_Recherche").select2({
        placeholder: "Cliquez-ici pour rechercher un intervenant",
        allowClear: true
    });
});

//Fonction permettant de rechercher les mails externe puis de la placer dans un input  
function droitAccesBO(Recherche,Taille,Div){
	var BO = '';
	var objListe=document.getElementById(Div);
	var listeDivEnf = objListe.getElementsByTagName('input');
	for(i = 0; i<listeDivEnf.length ; i++){
		if((listeDivEnf[i].id).substr(0,Taille) == Recherche){
			var ID = listeDivEnf[i].id;
			if(document.getElementById(ID).checked){
				BO+= (ID.substr(Taille,ID.length))+"_";	
			}
		}
	}
	return BO;
}

function creationInformationContrat(){
	var IntervenantConvention 	= ($('#Intervenant_Recherche').val()).trim();
	var DureeMensuelle 			= ($('#DureeMensuelle').val()).trim();
	var TauxHoraire				= ($('#TauxHoraire').val()).trim();
	var IndemniteF				= ($('#IndemniteF').val()).trim();
	var MoisConvention			= ($('#MoisConvention').val()).trim();
	var AnneeConvention			= ($('#AnneeConvention').val()).trim();
	var ListePdv = droitAccesBO('PdvConvention_',14,'tableauPDV');
	if(IndemniteF == '') IndemniteF = 0;
	if(IntervenantConvention != 'all'){
		if(DureeMensuelle != '' && TauxHoraire != ''){
			$.post("includes/functions/web2bdd/func.6.7-CreationConvention.php", {
					'IntervenantConvention' : IntervenantConvention,
					'DureeMensuelle' 		: DureeMensuelle,
					'TauxHoraire' 			: TauxHoraire,
					'IndemniteF' 			: IndemniteF,
					'MoisConvention' 		: MoisConvention,
					'AnneeConvention'		: AnneeConvention,
					'ListePdv'				: ListePdv
				},
				function(data) {

                    toastr.error("Le contrat a bien &eacute;t&eacute; cr&eacute;e", "CREATION CONTRAT");
					window.open('download/func.6-5-exportContratConvention.php');
				}
			);

		}else {

            toastr.error("ERREUR - Impossible de cr&eacute;er le contrat, une ou plusieurs sections sont vides !", "CREATION CONTRAT");
		}
	}else {

        toastr.error("ERREUR - Impossible de cr&eacute;er le contrat, Veuillez choisir un intervenant !", "CREATION CONTRAT");
	}
}