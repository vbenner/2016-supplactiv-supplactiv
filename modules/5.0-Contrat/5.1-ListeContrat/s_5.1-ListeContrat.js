var tableContrat_5_1;
var dateRechercheD_5_1, dateRechercheF_5_1, idContrat_5_1, idIntervenant_5_1, boolArchive_5_1, boolAuto_5_1;
jQuery(document).ready(function () {
    ClassContrat.init();
});

var ClassContrat = {
    init: function () {

        this.rechercheSessionModule();
        this.initDateRecherche();
        this.initListeContrat();
        this.initListeIntervenante();

        $(document.body).on("change", "#idContrat_5_1", function () {
            ClassContrat.saveSearchToSession();
        });
        $(document.body).on("change", "#idIntervenant_5_1", function () {
            ClassContrat.saveSearchToSession();
        });

        $(document.body).on("change", "#boolArchive_5_1", function () {
            ClassContrat.saveSearchToSession();
        });

        $(document.body).on("change", "#boolAuto_5_1", function () {
            ClassContrat.saveSearchToSession();
        });

        $(document.body).on('click', '#btnSearch', function() {
            $('#tableContrat_5_1').show();
            if (tableContrat_5_1 == undefined) {
                ClassContrat.initTableContrat();
            }
            else {
                tableContrat_5_1.api().ajax.reload();
            }
        });
    },

    saveSearchToSession: function () {
        if (idContrat_5_1 != $('#idContrat_5_1').val() ||
            dateRechercheD_5_1 != $('#dateRechercheD_5_1').val() ||
            dateRechercheF_5_1 != $('#dateRechercheF_5_1').val() ||
            idIntervenant_5_1 != $('#idIntervenant_5_1').val() ||
            boolArchive_5_1 != $('#boolArchive_5_1').val() ||
            boolAuto_5_1 != $('#boolAuto_5_1').val()
        ) {
            dateRechercheD_5_1 = $('#dateRechercheD_5_1').val();
            dateRechercheF_5_1 = $('#dateRechercheF_5_1').val();
            idContrat_5_1 = $('#idContrat_5_1').val();
            idIntervenant_5_1 = $('#idIntervenant_5_1').val();
            boolArchive_5_1 = $('#boolArchive_5_1').val();
            boolAuto_5_1 = $('#boolAuto_5_1').val();
            $.ajax({
                async : false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    dateRechercheD_5_1: dateRechercheD_5_1,
                    dateRechercheF_5_1: dateRechercheF_5_1,
                    idContrat_5_1: idContrat_5_1,
                    idIntervenant_5_1: idIntervenant_5_1,
                    boolArchive_5_1: boolArchive_5_1,
                    boolAuto_5_1: boolAuto_5_1,
                },
                dataType: 'json'
            }).done(function (data) {
                ClassContrat.initListeContrat();
                ClassContrat.initListeIntervenante();
            });
        }
    },

    rechercheSessionModule: function () {
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: {
                idModule: '5_1',
                dateRechercheD_5_1: '',
                dateRechercheF_5_1: '',
                idContrat_5_1: 'ALL',
                idIntervenant_5_1: 'ALL',
                boolArchive_5_1: 0,
                boolAuto_5_1: 'ALL',
            },
            dataType: "json"
        }).done(function (data) {
            $('#dateRechercheD_5_1').val(data.sessions.dateRechercheD_5_1);
            $('#dateRechercheF_5_1').val(data.sessions.dateRechercheF_5_1);
            idContrat_5_1 = data.sessions.idContrat_5_1;
            idIntervenant_5_1 = data.sessions.idIntervenant_5_1;
            boolArchive_5_1 = data.sessions.boolArchive_5_1;
            boolAuto_5_1 = data.sessions.boolAuto_5_1;
        });
    },

    initTableContrat: function () {

        tableContrat_5_1 = $('#tableContrat_5_1').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 50,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.5.1-listeContrat.php",
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableContrat_5_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    initDateRecherche: function () {
        $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
        $('.input-daterange').datepicker();
    },

    initListeContrat: function () {

        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.1-listeContrat.php",
            type: "POST",
            data: {},
            dataType: "json"
        }).done(function (data) {

            // On vide la zone de selection
            $("#idContrat_5_1").empty();
            $("#idContrat_5_1").append(
                $('<option>')
                    .attr('value', 'ALL')
                    .html('Tout afficher')
            );

            // On parcours les contrats (en fonction des dates selectionnees)
            $.each(data.contrats, function (key, infoContrat) {

                // On ajoute l'option
                $("#idContrat_5_1").append(
                    $('<option>')
                        .attr('value', infoContrat.idContrat)
                        .html(infoContrat.libelleContrat)
                        .attr('selected', (idContrat_5_1 == infoContrat.idContrat) ? true : false)
                );
            });

            // On ajoute le style
            $("#idContrat_5_1").select2({
                placeholder: "Cliquez-ici pour rechercher un contrat",
                allowClear: true
            });
        }).fail(function (jqXHR, textStatus) {

        });

    },

    initListeIntervenante: function () {

        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.5.1-listeIntervenante.php",
            type: "POST",
            data: {},
            dataType: "json"
        }).done(function (data) {

            // On vide la zone de selection
            $("#idIntervenant_5_1").empty();
            $("#idIntervenant_5_1").append(
                $('<option>')
                    .attr('value', 'ALL')
                    .html('Tout afficher')
            );

            // On parcours les contrats (en fonction des dates selectionnees)
            $.each(data.intervenantes, function (key, infoIntervenante) {

                // On ajoute l'option
                $("#idIntervenant_5_1").append(
                    $('<option>')
                        .attr('value', infoIntervenante.idIntervenant)
                        .html(infoIntervenante.libelleIntervenant)
                        .attr('selected', (idIntervenant_5_1 == infoIntervenante.idIntervenant) ? true : false)
                );
            });

            // On ajoute le style
            $("#idIntervenant_5_1").select2({
                placeholder: "Cliquez-ici pour rechercher une intervenante",
                allowClear: true
            });
        }).fail(function (jqXHR, textStatus) {

        });

    },

    retourContratChk: function (idContrat) {

        var boolChk = $('#cnt_' + idContrat).prop('checked') ? 1 : 0;
        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.5.1-retourContrat.php",
            type: "POST",
            data: {idContrat: idContrat, boolChk: boolChk},
            dataType: "json"
        }).done(function (data) {
            toastr.success('Le retour du contrat <strong>' + idContrat + '</strong> a bien &eacute;t&eacute; ' + ((boolChk) ? 'valid&eacute;' : 'supprimé'), 'RETOUR CONTRAT');
        });
    },

    suppressionContrat: function (intervenant, idContrat) {
        $('#sp-contrat').html(idContrat);
        $('#sp-intervenant').html(intervenant);
        $('#modalConfirmSuppression').modal('show');

        $('.btn-valid-suppression').unbind().bind('click', function () {

            var boolDate = ($('#chk-del-a').prop('checked')) ? true : false;
            var boolAll = ($('#chk-del-c').prop('checked')) ? true : false;
            $.ajax({
                async : false,
                url: "includes/functions/web2bdd/func.5.1-suppressionContrat.php",
                type: "POST",
                data: {
                    idContrat: idContrat,
                    boolDate:boolDate,
                    boolAll:boolAll,
                },
                dataType: "json"
            }).done(function (data) {
                tableContrat_5_1.api().ajax.reload();
                $('#modalConfirmSuppression').modal('hide');
                toastr.success('Le contrat <strong>' + idContrat + '</strong> a bien &eacute;t&eacute; supprimé', 'Suppression d\'un contrat');
            });
        });
    },

    archiveContrat: function (idContrat, boolArchive) {
        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.5.1-archiveContrat.php",
            type: "POST",
            data: {idContrat: idContrat, boolArchive: boolArchive},
            dataType: "json"
        }).done(function (data) {
            tableContrat_5_1.api().ajax.reload();
            if (boolArchive == 1) {
                toastr.success('Le contrat <strong>' + idContrat + '</strong> a bien &eacute;t&eacute; archivé', 'Archivage d\'un contrat');
            } else toastr.success('Le contrat <strong>' + idContrat + '</strong> a bien &eacute;t&eacute; désarchivé', 'Désarchivage d\'un contrat');
        });
    }
}
