<?php
    #echo '<pre>MAINTENANCE<br/>';
    #print_r($_SESSION);
    #echo '</pre>';
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des contrats enregistrés dans la Base de Données
                </div>
            </div>
            <div class="portlet-body form">
                <form>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3 input-daterange">
                                <label class="control-label">Date début :</label>
                                <input id="dateRechercheD_5_1" class="form-control form-recherche" type="text"
                                       placeholder="" name="start" style="text-align: left" value=""
                                       onchange="ClassContrat.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-3 input-daterange">
                                <label class="control-label">Date fin :</label>
                                <input id="dateRechercheF_5_1" class="form-control form-recherche" type="text"
                                       placeholder="" name="end" style="text-align: left" value=""
                                       onchange="ClassContrat.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">ID Contrat :</label>
                                <select id="idContrat_5_1" name="idContrat_5_1" class="form-control form-recherche">
                                </select>
                                <span class="help-block"> Facultatif </span>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Intervenante :</label>
                                <select id="idIntervenant_5_1" name="idIntervenant_5_1" class="form-control form-recherche">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">Archive :</label>
                                <select id="boolArchive_5_1" name="boolArchive_5_1" class="form-control form-recherche">
                                    <option value="0" <?php print (isset($_SESSION['boolArchive_5_1']) && $_SESSION['boolArchive_5_1'] == 0) ? 'selected' : '' ?>>NON</option>
                                    <option value="1" <?php print (isset($_SESSION['boolArchive_5_1']) && $_SESSION['boolArchive_5_1'] == 1) ? 'selected' : '' ?>>OUI</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">Auto :</label>
                                <select id="boolAuto_5_1" name="boolAuto_5_1" class="form-control form-recherche">
                                    <?php
                                    $selectedAll = '';
                                    $selected0 = '';
                                    $selected1 = '';
                                    if (!isset($_SESSION['boolAuto_5_1']))  {
                                        $selectedAll = ' selected ';
                                    }
                                    else {
                                        if ($_SESSION['boolAuto_5_1'] == 'ALL') {
                                            $selectedAll = ' selected ';
                                        }
                                        if ($_SESSION['boolAuto_5_1'] == '0') {
                                            $selected0 = ' selected ';
                                        }
                                        if ($_SESSION['boolAuto_5_1'] == '1') {
                                            $selected1 = ' selected ';
                                        }
                                    }
                                    ?>
                                    <option value="ALL" <?php echo $selectedAll; ?> >TOUT</option>
                                    <option value="0" <?php echo $selected0;  ?>>NON</option>
                                    <option value="1" <?php echo $selected1; ?>>OUI</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green">Rechercher</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="alert alert-warning"><strong>Information : </strong> Le déclenchement de la recherche n'est plus automatique. Vous devez cliquer systématiquement sur le bouton RECHERCHER pour afficher les résultats. Vous devez aussi cliquer dessus lorsque vous modifiez les éléments du filtre.</div>
<table class="table table-striped table-bordered table-hover" id="tableContrat_5_1" style="display: none">
    <thead>
    <tr>
        <th>#</th>
        <th>Date</th>
        <th>Intervenante</th>
        <th>Campagne</th>
        <th>Mission</th>
        <th>Date Intervention</th>
        <th>Durée</th>
        <th>Retour</th>
        <!--<th>Action</th>-->
        <th style="width: 100px"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div id="modalConfirmSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'un contrat</h4>
            </div>
            <div class="modal-body" style="padding-bottom: 0px">
                <div class="form-horizontal">
                    <h4 id="titre-sup"><strong>ATTENTION</strong>, confirmez-vous la suppression du contrat
                        <strong><span id="sp-contrat"></span></strong>
                        de l'intervenant <strong><span id="sp-intervenant"></span></strong> ?</h4>
                </div>

                <br/>
                <div class=" well">
                    <div class="row">
                        <div class="col-md-1">
                            <input class="form-control" id="chk-del-a" value="delDate" type="checkbox">
                        </div>
                        <div class="col-md-11">
                            Suppression des dates d'interventions (si planifiées)
                        </div>
                    </div>
                </div>
                <div class=" well">
                    <div class="row">
                        <div class="col-md-1">
                            <input class="form-control" id="chk-del-c" value="delAll" type="checkbox">
                        </div>
                        <div class="col-md-11">
                            Suppression complète des interventions
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-suppression">Oui, je confirme</button>
            </div>
        </div>
    </div>
</div>
