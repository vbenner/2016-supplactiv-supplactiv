
var tableInterlocuteur_12_1;
jQuery(document).ready(function() {
    ClassInterlocuteur.init();
});

var ClassInterlocuteur = {
    init: function(){
        this.initTableInterlocuteur();

        $('#ztTelInterlocuteur').inputmask("99 99 99 99 99");

        $( "#form-admin" ).validate({
            rules: {
                ztTelInterlocuteur: {
                    required: true
                },
                ztNomInterlocuteur: {
                    required: true
                },
                ztPrenomInterlocuteur: {
                    required: true
                },
                ztMailInterlocuteur: {
                    required: true
                }

            },
            success: function (label) {
            },

            submitHandler: function (form) {
                ClassInterlocuteur.ajoutInterlocuteur();
            }
        });
    },

    ajoutInterlocuteur: function() {
        var ztTelInterlocuteur = $("#ztTelInterlocuteur").val();
        var ztNomInterlocuteur = $("#ztNomInterlocuteur").val();
        var ztPrenomInterlocuteur = $("#ztPrenomInterlocuteur").val();
        var ztMailInterlocuteur = $("#ztMailInterlocuteur").val();
        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.12.1-ajoutInterlocuteur.php",
            type: "POST",
            data: {
                ztTelInterlocuteur: ztTelInterlocuteur,
                ztNomInterlocuteur: ztNomInterlocuteur,
                ztPrenomInterlocuteur: ztPrenomInterlocuteur,
                ztMailInterlocuteur: ztMailInterlocuteur
            },
            dataType: 'json'
        }).done(function (data) {
            if (data.result == 1) {
                $("#ztTelInterlocuteur").val('');
                $("#ztNomInterlocuteur").val('');
                $("#ztPrenomInterlocuteur").val('');
                $("#ztMailInterlocuteur").val('');
                toastr.success("L'interlocuteur a été enregistré", "Ajout d'un interlocuteur");
                tableInterlocuteur_12_1.api().ajax.reload();
            }
        });
    },

    initTableInterlocuteur: function(){

        tableInterlocuteur_12_1 = $('#tableInterlocuteur_12_1').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.12.1-listeInterlocuteur.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(2)', nRow).children('input').inputmask("99 99 99 99 99");
                $('td:eq(4)', nRow).children('button').bind('click', function(){

                    $('.btn-modif').unbind().bind('click', function(){
                        ClassInterlocuteur.modificationInterlocuteur($(this).attr('data-id'));
                    });

                });
            }
        });
        var tableWrapper = $('#tableInterlocuteur_12_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    modificationInterlocuteur: function(idUtilisateur){
        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.12.1-modificationInterlocuteur.php",
            type: "POST",
            data: {
                idUtilisateur : idUtilisateur,
                nomUtilisateur : $('#ztNom_'+idUtilisateur).val(),
                prenomUtilisateur : $('#ztPrenom_'+idUtilisateur).val(),
                telUtilisateur : $('#ztTel_'+idUtilisateur).val(),
                mailUtilisateur : $('#ztMail_'+idUtilisateur).val()
            },
            dataType: 'json'
        }).done(function( data ) {
            toastr.success("L'interlocuteur a été modifié", "Modification d'un interlocuteur");
            tableInterlocuteur_12_1.api().ajax.reload();
        });
    }

};