<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajout d'un interlocuteur
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" id="form-admin" method="post" action="#">
                    <div class="form-body">
                        <div class="form-group ">
                            <div class="col-md-3">
                                <label class="control-label">Nom :</label>
                                <input id="ztNomInterlocuteur" name="ztNomInterlocuteur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Prénom :</label>
                                <input id="ztPrenomInterlocuteur" name="ztPrenomInterlocuteur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Tel :</label>
                                <input id="ztTelInterlocuteur" name="ztTelInterlocuteur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Mail :</label>
                                <input id="ztMailInterlocuteur" name="ztMailInterlocuteur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn bg bg-green pull-right btn-add-admin"><i class="fa fa-check"></i> Enregistrer l'utilisateur</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


<h3>Liste des interlocuteurs </h3>
<hr>

<table class="table table-striped table-bordered table-hover" id="tableInterlocuteur_12_1">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Tel</th>
        <th>Mail</th>
        <th width="100">Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>