<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajout d'un administrateur
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" id="form-admin" method="post" action="">
                    <div class="form-body">
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">Nom :</label>
                                <input id="ztNomAdministrateur" name="ztNomAdministrateur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Prénom :</label>
                                <input id="ztPrenomAdministrateur" name="ztPrenomAdministrateur" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">E-mail :</label>
                                <input id="ztEmailAdministrateur" name="ztEmailAdministrateur" class="form-control" type="email" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Rôle :</label>
                                <div class="input-icon left">
                                    <i class="fa fa-warning tooltips tooltips" data-container="body"
                                       data-original-title="Obligatoire"></i>
                                    <select class="form-control" id="zsRole" name="zsRole">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <label class="control-label">Adresse A :</label>
                                <input id="ztAdresseA" name="ztAdresseA" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <label class="control-label">Adresse B :</label>
                                <input id="ztAdresseB" name="ztAdresseB" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">CP :</label>
                                <input id="ztCP" name="ztCP" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Ville:</label>
                                <input id="ztVille" name="ztVille" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn bg bg-green pull-right btn-add-admin"><i class="fa fa-check"></i> Enregistrer l'utilisateur</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<h3>Liste des administrateurs </h3>
<hr>

<table class="table table-striped table-bordered table-hover" id="tableAdministrateur_13_2">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>E-mail</th>
        <th>Profil</th>
        <th width="100">Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="modalConfirmSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'un administrateur</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">

                    <h3 id="titre-sup">Confirmez-vous la suppression de l'administrateur sélectionné ?</h3>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-suppression">Oui, je valide</button>
            </div>
        </div>
    </div>
</div