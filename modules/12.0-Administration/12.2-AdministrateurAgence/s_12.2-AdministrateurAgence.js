

jQuery(document).ready(function() {
    ClassAdministrateur.init();
});

var ClassAdministrateur = {
    init: function(){
        this.initTableAdministrateur();

        $( "#form-admin" ).validate({
            rules: {
                ztEmailAdministrateur: {
                    required: true,
                    email: true
                },
                ztNomAdministrateur: {
                    required: true
                },
                ztPrenomAdministrateur: {
                    required: true
                },
                zsRole: {
                    required: true
                },
                ztAdresseA: {
                    required: true
                },
                ztCP: {
                    required: true
                },
                ztVille: {
                    required: true
                }
            },
            success: function (label) {
            },

            submitHandler: function (form) {
               ClassAdministrateur.testPresenceEmail();
            }
        });
    },

    testPresenceEmail: function(){
        var ztEmailAdministrateur = $("#ztEmailAdministrateur").val();
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.12.2-verifPresenceEmail.php",
            type: "POST",
            data: {
                ztEmailAdministrateur : ztEmailAdministrateur
            },
            dataType: 'json'
        }).done(function( data ) {
            if(data.result == '0'){
                ClassAdministrateur.ajoutAdministrateur();
            }else toastr.error("L'email saisi est déjà utilisé !", "ERREUR - Ajout d'un administrateur");
        });
    },

    ajoutAdministrateur: function(){
        var ztEmailAdministrateur = $("#ztEmailAdministrateur").val();
        var ztNomAdministrateur = $("#ztNomAdministrateur").val();
        var ztPrenomAdministrateur = $("#ztPrenomAdministrateur").val();
        var ztAdresseA = $("#ztAdresseA").val();
        var ztAdresseB = $("#ztAdresseB").val();
        var ztCP = $("#ztCP").val();
        var ztVille = $("#ztVille").val();
        var zsRole = $("#zsRole").val();
        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.12.2-ajoutAdministrateur.php",
            type: "POST",
            data: {
                ztEmailAdministrateur : ztEmailAdministrateur,
                ztNomAdministrateur : ztNomAdministrateur,
                ztPrenomAdministrateur : ztPrenomAdministrateur,
                ztAdresseA : ztAdresseA,
                ztAdresseB : ztAdresseB,
                ztCP : ztCP,
                ztVille : ztVille,
                zsRole : zsRole,
            },
            dataType: 'json'
        }).done(function( data ) {
            if(data.result == 1){
                toastr.success("L'administrateur a été enregistré, un e-mail contenant son mot de passe a été envoyé", "Ajout d'un administrateur");
                ClassAdministrateur.envoiMail(ztEmailAdministrateur);

            }
        });
    },

    envoiMail: function(ztEmailAdministrateur){
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.verifPresenceEmailUtilisateur.php",
            type: "POST",
            data: {
                ztEmailUtilisateur : ztEmailAdministrateur
            },
            dataType: 'json'
        }).done(function( data ) {
        });
    },

    initTableAdministrateur: function(){

        ClassAdministrateur.initListes();

        tableAdministrateur_13_2 = $('#tableAdministrateur_13_2').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.12.2-listeAdministrateur.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(4)', nRow).children('button').bind('click', function(){
                    var idAdmin = $(this).attr('data-id');
                    $('#modalConfirmSuppression').modal('show');
                    $('.btn-valid-suppression').unbind().bind('click', function(){
                        ClassAdministrateur.suppressionAdministrateur(idAdmin);
                    });

                });
            }
        });
        var tableWrapper = $('#tableAdministrateur_13_2_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    suppressionAdministrateur: function(idUtilisateur){
        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.12.2-suppressionAdministrateur.php",
            type: "POST",
            data: {
                idUtilisateur : idUtilisateur
            },
            dataType: 'json'
        }).done(function( data ) {
            $('#modalConfirmSuppression').modal('hide');
            toastr.success("L'administrateur a été supprimé", "Suppression d'un administrateur");
            tableAdministrateur_13_2.api().ajax.reload();
        });
    },

    initListes: function () {

        $.ajax({
            async:false,
            type: "POST",
            url: "includes/functions/bdd2web/func.12.2-listeRoles.php",
            dataType: 'json',
            success: function (data) {

                $('#zsRole').empty();
                $.each(data.roles, function (i, role) {
                    $('#zsRole').append(
                        $('<option>')
                            .attr('value', role.id)
                            .html(role.lib)
                    );
                });
            }
        });
    }

};