<?php

$sqlRechercheInfoMission = '
SELECT *
FROM su_mission
  INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE idMission = :idMission';
$RechercheInfoMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoMission);


if(filter_has_var(INPUT_GET, 's')){

    $RechercheInfoMissionExc->bindValue(':idMission', filter_input(INPUT_GET, 's'));
    $RechercheInfoMissionExc->execute();
    $InfoMission = $RechercheInfoMissionExc->fetch(PDO::FETCH_OBJ);
// Initialisation des dates de la mission
$dateD = new DateTime($InfoMission->dateDebut);
$dateF = new DateTime($InfoMission->dateFin);


// Initialisation du booleen de fin
$boolFin = false;

// Liste jour mission
$arrayDaysMission = array();

while(!$boolFin){
    $dateD->add(new DateInterval('P1D'));

    if($dateD > $dateF){
        $boolFin = true;
    }else {
        if(!isset($arrayDaysMission[$dateD->format('n')])){
            $arrayDaysMission[$dateD->format('n')] = array(
                'LIBELLE' => $dateD->format('m').' - '.$arrayMonthCorres[$dateD->format('n')]['libelleMax'].' '.$dateD->format('Y'),
                'LIBELLEMIN' => $arrayMonthCorres[$dateD->format('n')]['libelleMin'],
                'JOUR' => []
            );
        }

        array_push($arrayDaysMission[$dateD->format('n')]['JOUR'], [
            'J' => $arrayDayCorres[$dateD->format('N')]['libelleMin'],
            'ID' => $dateD->format('d'),
            'D' => $dateD->format('Y-m-d'),
            'W' => $arrayDayCorres[$dateD->format('N')]['boolWD']
        ]);
    }
}
?>
<input type="hidden" id="zt_idMission_15_0" value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>" />

<div class="row">
    <div class="col-md-12">
        <table class="table  table-bordered" id="tableIntervention_15_0">
            <?php

            print '<thead><tr>';
                print '<th colspan="'.(7+sizeof($arrayDaysMission)).'"></th>';
                foreach($arrayDaysMission as $idMois => $lstJour){
                    print '<th colspan="'.sizeof($lstJour['JOUR']).'" class="active">'.$lstJour['LIBELLE'].'</th>';
                }
                print '</tr>';

                print '<tr>';
            print '<td colspan="'.(7+sizeof($arrayDaysMission)).'"></td>';
                foreach($arrayDaysMission as $idMois => $lstJour){
                    foreach($lstJour['JOUR'] as $infoJour){
                        print '<td class="active">'.$infoJour['J'].'</td>';
                    }
                }
                print '</tr>';

                print '<tr>';
                print '<td>ANIMATRICE</td>';
                print '<td>CODE CIP</td>';
                print '<td>PDV</td>';
                print '<td style="width: 300px !important;">ADRESSE</td>';
                print '<td>CP</td>';
                print '<td>VILLE</td>';
                print '<td>TELEPHONE</td>';

                $tdMois = ''; $tdJour = '';
                foreach($arrayDaysMission as $idMois => $lstJour){
                    print '<td class="warning">'.$lstJour['LIBELLEMIN'].'</td>';
                    $tdMois .= '<td class="warning"></td>';
                }

                foreach($arrayDaysMission as $idMois => $lstJour){
                    foreach($lstJour['JOUR'] as $infoJour){
                        print '<td class="'.((!$infoJour['W'])? 'warning' : '').'">'.$infoJour['ID'].'</td>';
                        $tdJour .= '<td class="'.((!$infoJour['W'])? 'warning' : '').'"></td>';
                    }
                }
            print '</tr></thead>';

            ?>
            <tbody></tbody>
        </table>
    </div>
</div>

<?php
}