var idMission;
jQuery(document).ready(function() {
    idMission = $('#zt_idMission_15_0').val();
    ClassMission.init();
});

var ClassMission = {
    init: function(){

        this.rechercheListeIntervention();
    },

    initTableIntervention: function(){

        $('#tableIntervention_15_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true
        });
        var tableWrapper = $('#tableIntervention_15_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    rechercheListeIntervention: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: { idMission : idMission },
            url: "includes/functions/bdd2web/func.15.0-listeMissionIntervention.php",
            dataType: 'json',
            success: function(data) {
                $.each(data.interventions, function (index, InfoIntervention) {
                    $('#tableIntervention_15_0 tbody').append(
                        $('<tr>').append(
                            $('<td>').html(InfoIntervention.intervenant)
                        ).append(
                                $('<td>').html(InfoIntervention.cip)
                            )
                            .append(
                                $('<td>').html(InfoIntervention.libelle)
                            )
                            .append(
                                $('<td>').html(InfoIntervention.adr)
                            )
                            .append(
                                $('<td>').html(InfoIntervention.cp)
                            )
                            .append(
                                $('<td>').html(InfoIntervention.ville)
                            )
                            .append(
                                $('<td>').html(InfoIntervention.tel)
                            )
                            .append(InfoIntervention.mois)
                            .append(InfoIntervention.jour)
                    )
                });

                ClassMission.initTableIntervention();
            }
        });
    }
}
