jQuery(document).ready(function() {
    ClassCategorie.init();
});

var ClassCategorie = {
    init: function(){
        this.initTableCategorie();

        $('.btn-add-categorie').bind('click', function(){
            ClassCategorie.ajoutIntervenant();
        });
    },

    serialize: function(el) {
        var serialized = $(el).serialize();
        if (!serialized) // not a form
            serialized = $(el).
            find('input[name],select[name],textarea[name]').serialize();
        return serialized;
    },

    ajoutIntervenant: function(){
        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.2.40-ajoutCategorie.php",
            type: "POST",
            data: {
                info : ClassCategorie.serialize('#form-categorie'),
            },
            dataType: 'json',
        }).done(function( data ) {
            if(data.result != 0){
                document.location.href = 'index.php?ap=2.0-PointDeVente&ss_m=2.40-CategoriesPDV&s='+data.result;
            }else {
                toastr.error("Veuillez vérifier le libelle !", "ERREUR - Ajout d'une categorie");
            }
        });
    },

    initTableCategorie: function(){

        $('#tableCategorie_2_40').dataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>rB>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                    extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                    extend: 'pdf',
                    exportOptions: {
                        columns: [ 0, 1]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1]
                    },
                },
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                buttons: {
                    copyTitle: 'Ajouté au presse-papier',
                    copySuccess: {
                        _: '%d lignes copiées',
                        1: '1 ligne copiée'
                    }
                }
            },

            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 50,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.2.40-listeCategories.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(2)', nRow).children('button').unbind().bind('click', function(event){

                    if ($(this).hasClass('fiche')) {
                        event.preventDefault();
                        event.stopPropagation();
                        var a = document.createElement('a');
                        a.href = 'index.php?ap=2.0-PointDeVente&ss_m=2.3-fichePDV&s='+$(this).attr('data-id');
                        a.target = '_blank';
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                    }

                    /** ---------------------------------------------------------------------------
                     * Window .open
                     * @type {string}
                     */
                    if ($(this).hasClass('delete')) {
                        ClassPointDeVente.demandeSuppression($(this).attr('data-id'), $(this).attr('data-name'));
                    }
                });
                $('td:eq(3)', nRow).children('button').unbind().bind('click', function(event){

                    if ($(this).hasClass('fiche')) {
                        event.preventDefault();
                        event.stopPropagation();
                        var a = document.createElement('a');
                        a.href = 'index.php?ap=2.0-PointDeVente&ss_m=2.3-fichePDV&s='+$(this).attr('data-id');
                        a.target = '_blank';
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                    }

                    /** ---------------------------------------------------------------------------
                     * Window .open
                     * @type {string}
                     */
                    if ($(this).hasClass('delete')) {
                        ClassPointDeVente.demandeSuppression($(this).attr('data-id'), $(this).attr('data-name'));
                    }
                });
            },

            fnInitComplete: function() {
                //$('#tableIntervenant_4_1 tbody tr').each(function(){
                //    $(this).find('td:eq(1)').attr('nowrap', 'nowrap');
                //});
                $('.dt-buttons').css({
                    'margin-left':'15px',
                    'padding-bottom':'5px'
                });
            }
        });
        var tableWrapper = $('#tablePointDeVente_3_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
};
