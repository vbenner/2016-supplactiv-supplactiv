<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajout d'une catégorie
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" id="form-categorie" method="post" action="#">
                    <div class="form-body">
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">Libelle :</label>
                                <input id="ztLibelle" name="ztLibelle" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn bg bg-green pull-right btn-add-categorie"><i class="fa fa-check"></i> Enregistrer</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<h3>Liste des Catégories </h3>
<hr>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des catégories
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableCategorie_2_40">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Libellé</th>
                        <th>Sous-Catégorie</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
