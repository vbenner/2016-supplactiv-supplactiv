<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajout d'une catégorie / Lot (Fichier Excel)
                </div>
            </div>
            <div class="portlet-body form">
                <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data"
                      action="import/func.importCategories.php" target="uploadFrame" method="post">
                    <div id="uploadInfos">
                        <div id="uploadStatus"></div>
                        <iframe id="uploadFrame" name="uploadFrame"
                                style="width:100%;height:0px;display: none"></iframe>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Fichier Excel :</label>
                            <div class="col-md-10">
                                <fieldset class="label_side">
                                    <div>
                                        <div id=fi" class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input span3"
                                                     data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                            class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">Choisir un fichier </span>
                                            <span class="fileinput-exists">Modifier </span>
                                            <input type="file" id="fichierCAT" name="fichierCAT">
                                        </span>
                                                <a href="#" class="input-group-addon btn red fileinput-exists"
                                                   data-dismiss="fileinput">Supprimer </a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button id="uploadSubmit" type="submit" class="btn btn-primary pull-right"><i
                                    class="fa fa-cogs"></i> Importer le Fichier
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="note note-info">
            <span class="help-block">
            Le Fichier EXCEL doit respecter les colonnes suivantes : CIP ; ID Catégorie ; IDSous-Catégorie.<br/>
            <strong>Exemple</strong> : 123456 ; 2 ; 7 ;<br>
            Pour Parapharmacie (2) ; CARREFOUR (7)<br/>
            Télécharger un fichier modèle <a target="_blank" href="../../../download/_html/FichierCAT.xlsx">ici</a>
            </span>
        </div>
    </div>
</div>