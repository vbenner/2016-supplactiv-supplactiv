var ClassCategorie = {
    resultImport : function(nbLigneOK, nbLigneTotal){
        if(nbLigneOK == 0){
            toastr.error('Veuillez vérifier votre fichier', 'ERREUR - Importation du fichier');
        }else {
            toastr.success('Le fichier a bien été importé', 'Importation du fichier');
        }
    }
}