var idCategorie;
jQuery(document).ready(function () {
    idCategorie = $('#zt_idCategorie_2_41').val();
    ClassFicheCategorie.init();
});

var ClassFicheCategorie = {
    init: function () {
        this.rechercheCategorie();
    },

    rechercheCategorie: function () {
        $.ajax({
            async:false,
            type: "POST",
            data: {idCategorie: idCategorie},
            url: "includes/functions/bdd2web/func.2.41-infoCategorie.php",
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {

                    // Informations generales
                    $('#ztLibelle').val(data.categorie);

                    $('.btn-maj-info').unbind().bind('click', function () {
                        var ztLibelle = $('#ztLibelle').val();

                        if (ztLibelle.trim() != '') {
                            $.ajax({
                                async: false,
                                type: "POST",
                                data: {
                                    idCategorie: idCategorie,
                                    ztLibelle: ztLibelle,
                                    type: 1
                                },
                                url: "includes/functions/web2bdd/func.2.41-majInfoCategorie.php",
                                dataType: 'json',
                                success: function (data) {
                                    toastr.success('Les informations ont bien été enregistrées !', "Informations principales")
                                }
                            });
                        } else toastr.error('Le libellé ne peut être vide !', "ERREUR - Informations principales")
                    });
                }
            }
        });
    },
};