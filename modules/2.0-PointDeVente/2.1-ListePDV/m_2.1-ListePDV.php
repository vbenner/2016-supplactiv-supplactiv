<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des points de vente
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tablePointDeVente_3_1">
                    <thead>
                    <tr>
                        <th>Code CIP</th>
                        <th>Libellé Point de vente</th>
                        <th>Ciblage</th>
                        <th>Catégorie</th>
                        <th>Sous-Catégorie</th>
                        <th>Adresse</th>
                        <th>Code Postal</th>
                        <th>Ville</th>
                        <th>Téléphone</th>
                        <th>Email</th>
                        <th>Titulaire</th>
                        <th>Nb Intervention</th>
                        <th>Actif</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content" style="width: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'un Point de Vente</h4>
            </div>
            <div class="modal-body">
                <p>Confirmez-vous la suppression du point de vente <strong><span id="nom-sup"></span></strong> ?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-confirm-sup">Oui, je confirme</button>
            </div>
        </div>
    </div>
</div>