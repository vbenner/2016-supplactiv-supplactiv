jQuery(document).ready(function() {
    ClassPointDeVente.init();
});

var ClassPointDeVente = {
    init: function(){
        this.initTablePointDeVente();
    },

    demandeSuppression: function(idPDV, nomPDV){
        $('#modalSuppression').modal('show');
        $('#nom-sup').html(nomPDV);
        $('.btn-confirm-sup').unbind().bind('click', function(){
            $.ajax({
                async:false,
                url: "includes/functions/web2bdd/func.2.1-suppressionPDV.php",
                type: "POST",
                data: {
                    idPDV : idPDV
                },
                dataType: 'json'
            }).done(function( data ) {
                $('#modalSuppression').modal('hide');
                toastr.success('Le PDV '+nomPDV+' a bien été supprimé', 'Suppression d\'un PDV');
                tablePointDeVente_3_1.api().ajax.reload();
            });
        });
    },

    initTablePointDeVente: function(){

        $('#tablePointDeVente_3_1').dataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>rB>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                    extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                    extend: 'pdf',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                },
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                buttons: {
                    copyTitle: 'Ajouté au presse-papier',
                    copySuccess: {
                        _: '%d lignes copiées',
                        1: '1 ligne copiée'
                    }
                }
            },

            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.3.1-listePointDeVente.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(13)', nRow).children('button').unbind().bind('click', function(event){

                    if ($(this).hasClass('fiche')) {
                        event.preventDefault();
                        event.stopPropagation();
                        var a = document.createElement('a');
                        a.href = 'index.php?ap=2.0-PointDeVente&ss_m=2.3-fichePDV&s='+$(this).attr('data-id');
                        a.target = '_blank';
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                    }

                    /** ---------------------------------------------------------------------------
                     * Window .open
                     * @type {string}
                     */
                    if ($(this).hasClass('delete')) {
                        ClassPointDeVente.demandeSuppression($(this).attr('data-id'), $(this).attr('data-name'));
                    }
                });
            },

            fnInitComplete: function() {
                //$('#tableIntervenant_4_1 tbody tr').each(function(){
                //    $(this).find('td:eq(1)').attr('nowrap', 'nowrap');
                //});
                $('.dt-buttons').css({
                    'margin-left':'15px',
                    'padding-bottom':'5px'
                });
            }
        });
        var tableWrapper = $('#tablePointDeVente_3_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
};
