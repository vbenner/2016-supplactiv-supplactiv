<input type="hidden" id="zt_idSousCategorie_2_42"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<div class="row">
    <div class="col-md-1">
        <a class="btn btn-success" href="index.php?ap=2.0-PointDeVente&ss_m=2.40-CategoriesPDV">RETOUR</a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajout d'une sous-catégorie
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" id="form-sous-categorie" method="post" action="#">
                    <div class="form-body">
                        <div class="form-group ">
                            <div class="col-md-6">
                                <label class="control-label">Libelle :</label>
                                <input id="ztCategorie" name="ztCategorie" class="form-control" type="hidden"
                                       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>" />
                                <input id="ztLibelle" name="ztLibelle" class="form-control" type="text" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn bg bg-green pull-right btn-add-sous-categorie"><i class="fa fa-check"></i> Enregistrer</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


<h3>Liste des Sous-Catégories </h3>
<hr>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des sous-catégories
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableSousCategorie_2_42">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Libellé</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content" style="width: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'une Sous-Catégorie</h4>
            </div>
            <div class="modal-body">
                <p>Confirmez-vous la suppression de la sous-catégorie <strong><span id="nom-sup"></span></strong> ?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-confirm-sup">Oui, je confirme</button>
            </div>
        </div>
    </div>
</div>