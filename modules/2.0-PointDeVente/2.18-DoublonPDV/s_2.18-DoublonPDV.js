$(function () {

    $('#btnAction').on('click', function () {

        var oldCIP, newCIP  ;
        newCIP = $('#ztKeep').val().trim();
        oldCIP = $('#ztRemove').val().trim();
        if (newCIP == ''|| oldCIP == '') {
            toastr.warning("Les n° doivent être renseignés");
            return false;
        }
        if (newCIP == oldCIP) {
            toastr.warning("Les n° doivent être différents");
            return false;
        }

        $('#confirm-delete #oldCIP').html('<strong>' + oldCIP + '</strong>');
        $('#confirm-delete #newCIP').html('<strong>' + newCIP + '</strong>');
        var modal = $('#confirm-delete');
        $('.btn-ok').on('click', function () {
            $(this).off().removeClass('red').addClass('grey-cascade').html(
                '<i class="fa fa-spin fa-spinner"></i> Patientez...'
            );
            $.ajax({
                async:false,
                url: "includes/functions/web2bdd/func.2.18-cleanDoublons.php",
                type: "POST",
                data: {
                    pOld : oldCIP,
                    pNew : newCIP,
                },
                dataType: "json"
            }).done(function( data ) {
                if (data.result == 0) {
                    toastr.warning(data.raison);
                } else {
                    toastr.success('Remplacement terminé !');
                    $('#ztKeep').val('');
                    $('#ztRemove').val('');
                }
                $('.btn-ok').add('red').removeClass('grey-cascade').html('Valider');
                $(modal).modal('hide');
            });

        });
        $(modal).modal('show');
    })
});