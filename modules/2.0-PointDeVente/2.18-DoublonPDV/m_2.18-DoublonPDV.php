<div>
    <h4>Suppression des Doublons PDV</h4>
    <div class="row">
        <div class="col-md-3">
            <label for="ztKeep">CIP (Merval) à garder</label>
            <div class="input-group">
                <input class="form-control" id="ztKeep">
                <span class="input-group-addon">
                    <i class="fa fa-lock font-green"></i>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <label for="ztKeep">CIP (Merval) à Supprimer</label>
            <div class="input-group">
                <input class="form-control" id="ztRemove">
                <span class="input-group-addon">
                    <i class="fa fa-unlock font-red"></i>
                </span>
            </div>
        </div>
        <div class="col-md-3">
            <label for="btnAction">&nbsp;</label>
            <div class="input-group">
                <button id="btnAction" type="button" class="btn red">Exécuter</button>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-danger">
                <strong>ATTENTION</strong> Cette opération ne peux être annulée a posteriori. Tout remplacement est définitif.
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                ATTENTION
            </div>
            <div class="modal-body">
                Vous être sur le point de remplacer le CIP <span id="oldCIP"></span> par <span id="newCIP"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <a class="btn btn-danger btn-ok">Valider</a>
            </div>
        </div>
    </div>
</div>