var ClassPDV = {
    resultImport : function(nbLigneOK, nbLigneTotal, nbLigneTotalUpdate){
        console.log(nbLigneOK + ', ' + nbLigneTotal + ', ' + nbLigneTotalUpdate);

        if(nbLigneOK == 0 && nbLigneTotalUpdate == 0){
            toastr.error('Aucune donnée n\'a été intégrée dans la base', 'ERREUR - Importation du fichier');
        }else {
            toastr.success('Le fichier a bien été importé<br/>' + nbLigneOK + ' nouvelle(s) lignes<br/>' + nbLigneTotalUpdate + ' modification(s).', 'Importation du fichier');
        }
    }
}