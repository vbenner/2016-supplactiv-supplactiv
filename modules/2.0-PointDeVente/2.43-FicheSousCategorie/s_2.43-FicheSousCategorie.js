var idSousCategorie;
jQuery(document).ready(function () {
    idSousCategorie = $('#zt_idCategorie_2_43').val();
    ClassFicheSousCategorie.init();
});

var ClassFicheSousCategorie = {
    init: function () {
        this.rechercheSousCategorie();
    },

    rechercheSousCategorie: function () {
        $.ajax({
            async:false,
            type: "POST",
            data: {idSousCategorie: idSousCategorie},
            url: "includes/functions/bdd2web/func.2.43-infoSousCategorie.php",
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {

                    // Informations generales
                    $('#ztLibelle').val(data.categorie);

                    $('.btn-maj-info').unbind().bind('click', function () {
                        var ztLibelle = $('#ztLibelle').val();

                        if (ztLibelle.trim() != '') {
                            $.ajax({
                                async: false,
                                type: "POST",
                                data: {
                                    idSousCategorie: idSousCategorie,
                                    ztLibelle: ztLibelle,
                                    type: 1
                                },
                                url: "includes/functions/web2bdd/func.2.43-majInfoSousCategorie.php",
                                dataType: 'json',
                                success: function (data) {
                                    toastr.success('Les informations ont bien été enregistrées !', "Informations principales")
                                }
                            });
                        } else toastr.error('Le libellé ne peut être vide !', "ERREUR - Informations principales")
                    });
                }
            }
        });
    },
};