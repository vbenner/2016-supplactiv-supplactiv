<input type="hidden" id="zt_idCategorie_2_43"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<div class="row">
    <div class="col-md-1">
        <?php
            $param = explode('_', filter_input(INPUT_GET, 's'));
        ?>
        <a class="btn btn-success" href="index.php?ap=2.0-PointDeVente&ss_m=2.42-FicheSousCategorie&s=<?php echo $param[1]; ?>">RETOUR</a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Informations principales
                </div>
                <div class="tools" style="margin-top: 0px;">
                    <button class="btn blue bg-blue-su pull-right btn-maj-info btn-xs" ><i class="icon-check"></i> Mettre à
                        jour les informations principales
                    </button>
                </div>
            </div>
            <div class="portlet-body form">

                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">
                        <label class="control-label col-md-3">Sous-Catégorie :</label>

                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input class="form-control" type="text" placeholder="" id="ztLibelle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
