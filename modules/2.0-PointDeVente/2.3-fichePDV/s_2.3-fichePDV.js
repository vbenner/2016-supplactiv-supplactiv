var idPdv;

function initMap() {

    $.ajax({
        async:false,
        type: "POST",
        data: {idPdv: idPdv},
        url: "includes/functions/bdd2web/func.2.3-infoPdvMap.php",
        dataType: 'json',
        success: function (data) {

            if (data.result == 1) {

                var pdv = {lat: parseFloat(data.coord.lat), lng: parseFloat(data.coord.lng)};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: pdv
                });
                var marker = new google.maps.Marker({
                    position: pdv,
                    map: map
                });

                $.each(data.inter, function (index, item) {
                    var title = parseFloat(item.DIST);
                    var marker = new google.maps.Marker({
                        position: {lat: parseFloat(item.LAT), lng: parseFloat(item.LON)},
                        map: map,
                        title: item.NOM + ' : ' + title.toFixed(2) + ' km',
                        icon: {
                            url: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                            color: '#345678'
                        }
                    });
                });
            }
        }
    });
}


jQuery(document).ready(function () {
    idPdv = $('#zt_idPdv_2_3').val();
    ClassFichePointDeVente.init();
});

var ClassFichePointDeVente = {
    init: function () {
        this.rechercheInfoPointDeVente();
        this.initListeIntervention();

        $('.btn-desactivate-pdv').bind('click', function(){
            ClassFichePointDeVente.activatePDV();
        });

    },

    initListeIntervention: function () {

        $('#tableIntervention_2_3').dataTable({

            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.2.3-listeInterventionPdv.php?idPdv=" + idPdv,
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                /*$('td:eq(7)', nRow).children('button').bind('click', function(){
                    console.log($(this).attr('data-id'));
                    document.location.href='index.php?ap=2.0-PointDeVente&ss_m=2.3-fichePDV&s='+$(this).attr('data-id');
                }) */
            }
        });
        var tableWrapper = $('#tableIntervention_2_3_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    rechercheInfoPointDeVente: function () {

        $.ajax({
            async:false,
            type: "POST",
            data: {idPdv: idPdv},
            url: "includes/functions/bdd2web/func.2.3-infoPdv.php",
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {

                    // Informations generales
                    $('#ztLibelle').val(data.infoPdv.libellePdv);
                    $('#ztAdresseA').val(data.infoPdv.adressePdv_A);
                    $('#ztAdresseB').val(data.infoPdv.adressePdv_B);
                    $('#ztAdresseC').val(data.infoPdv.adressePdv_C);
                    $('#ztCodePostal').val(data.infoPdv.codePostalPdv);
                    $('#ztVille').val(data.infoPdv.villePdv);
                    $('#ztLatitude').val(data.infoPdv.latitude);
                    $('#ztLongitude').val(data.infoPdv.longitude);
                    $('#ztCommentaire').val(data.infoPdv.commentairePdv);
                    $('#ztCommentaireEM').val(data.infoPdv.commentairePdv_EM);
                    $('#ztMerval').val(data.infoPdv.codeMerval);
                    $('#ztOldMerval').val(data.infoPdv.codeMerval_old);

                    // Informations Contact
                    $('#ztNomTitulaireA').val(data.infoPdv.nomTitulairePdv_A);
                    $('#ztPrenomTitulaireA').val(data.infoPdv.prenomTitulairePdv_A);
                    $('#ztNomTitulaireB').val(data.infoPdv.nomTitulairePdv_B);
                    $('#ztPrenomTitulaireB').val(data.infoPdv.prenomTitulairePdv_B);
                    $('#ztTelephoneTitulaire').val(data.infoPdv.telephoneMagasin);
                    $('#ztFaxTitulaire').val(data.infoPdv.faxPdv);
                    $('#ztEmailTitulaire').val(data.infoPdv.emailPdv);

                    // Informations interlocuteur
                    $('#ztNomInterlocuteur').val(data.infoPdv.nomInterlocuteurPdv);
                    $('#ztPrenomInterlocuteur').val(data.infoPdv.prenomInterlocuteurPdv);
                    $('#ztTelInterlocuteur_A').val(data.infoPdv.telephoneInterlocuteurPdv);
                    $('#ztTelInterlocuteur_B').val(data.infoPdv.faxInterlocuteurPdv);
                    $('#ztEmailInterlocuteur').val(data.infoPdv.mailInterlocuteurPdv);

                    // Recherche des categories (selection de la categorie du PDV)
                    ClassFichePointDeVente.rechercheCategorie(data.infoPdv.FK_idCategorie);

                    ClassFichePointDeVente.rechercheCiblage(data.infoPdv.ciblage);

                    // Recherche des sous-categories (selection de la sous-categorie du PDV)
                    ClassFichePointDeVente.rechercheSousCategorie(data.infoPdv.FK_idCategorie, data.infoPdv.FK_idSousCategorie);

                    // Recherche des categories (selection de la categorie du PDV)
                    ClassFichePointDeVente.rechercheFonction(data.infoPdv.FK_idFonctionInterlocuteurPdv);

                    $('.btn-maj-info').unbind().bind('click', function () {
                        var ztLibelle = $('#ztLibelle').val();
                        var ztAdresseA = $('#ztAdresseA').val();
                        var ztAdresseB = $('#ztAdresseB').val();
                        var ztAdresseC = $('#ztAdresseC').val();
                        var ztCodePostal = $('#ztCodePostal').val();
                        var ztVille = $('#ztVille').val();
                        var ztLatitude = $('#ztLatitude').val();
                        var ztLongitude = $('#ztLongitude').val();
                        var ztCommentaire = $('#ztCommentaire').val();
                        var ztCommentaireEM = $('#ztCommentaireEM').val();
                        var zsCategorie = $('#zsCategorie').val();
                        var zsSousCategorie = $('#zsSousCategorie').val();
                        var ztMerval = $('#ztMerval').val();
                        var ztOldMerval = $('#ztOldMerval').val();
                        var zsCiblage = $('#zsCiblage').val();

                        if (ztLibelle.trim() != '') {
                            $.ajax({
                                async: false,
                                type: "POST",
                                data: {
                                    idPdv: idPdv,
                                    ztLibelle: ztLibelle,
                                    ztAdresseA: ztAdresseA,
                                    ztAdresseB: ztAdresseB,
                                    ztAdresseC: ztAdresseC,
                                    ztCodePostal: ztCodePostal,
                                    ztVille: ztVille,
                                    ztLatitude: ztLatitude,
                                    ztLongitude: ztLongitude,
                                    ztCommentaire: ztCommentaire,
                                    ztCommentaireEM: ztCommentaireEM,
                                    zsCategorie: zsCategorie,
                                    zsSousCategorie: zsSousCategorie,
                                    ztMerval: ztMerval,
                                    ztOldMerval: ztOldMerval,
                                    type: 1,
                                    zsCiblage: zsCiblage
                                },
                                url: "includes/functions/web2bdd/func.2.3-majInformationsPdv.php",
                                dataType: 'json',
                                success: function (data) {
                                    if (data.result == 1) {
                                        toastr.success('Les informations ont bien été enregistrées !', "Informations principales")
                                    }
                                    else {
                                        toastr.warning(data.result);
                                    }
                                }
                            });
                        } else toastr.error('Le libellé ne peut être vide !', "ERREUR - Informations principales")
                    });


                    $('.btn-maj-titulaire').unbind().bind('click', function () {

                        ztNomTitulaireA = $('#ztNomTitulaireA').val();
                        ztPrenomTitulaireA = $('#ztPrenomTitulaireA').val();
                        ztNomTitulaireB = $('#ztNomTitulaireB').val();
                        ztPrenomTitulaireB = $('#ztPrenomTitulaireB').val();
                        ztTelephoneTitulaire = $('#ztTelephoneTitulaire').val();
                        ztFaxTitulaire = $('#ztFaxTitulaire').val();
                        ztEmailTitulaire = $('#ztEmailTitulaire').val();

                        $.ajax({
                            async:false,
                            type: "POST",
                            data: {
                                idPdv: idPdv,
                                ztNomTitulaireA: ztNomTitulaireA,
                                ztPrenomTitulaireA: ztPrenomTitulaireA,
                                ztNomTitulaireB: ztNomTitulaireB,
                                ztPrenomTitulaireB: ztPrenomTitulaireB,
                                ztTelephoneTitulaire: ztTelephoneTitulaire,
                                ztFaxTitulaire: ztFaxTitulaire,
                                ztEmailTitulaire: ztEmailTitulaire,
                                type: 2
                            },
                            url: "includes/functions/web2bdd/func.2.3-majInformationsPdv.php",
                            dataType: 'json',
                            success: function (data) {
                                toastr.success('Les informations ont bien été enregistrées !', "Contact point de vente")
                            }
                        });

                    });

                    $('.btn-maj-interlocuteur').unbind().bind('click', function () {


                        ztNomInterlocuteur = $('#ztNomInterlocuteur').val();
                        ztPrenomInterlocuteur = $('#ztPrenomInterlocuteur').val();
                        ztTelInterlocuteur_A = $('#ztTelInterlocuteur_A').val();
                        ztTelInterlocuteur_B = $('#ztTelInterlocuteur_B').val();
                        ztEmailInterlocuteur = $('#ztEmailInterlocuteur').val();
                        zsFonctionInterlocuteur = $('#zsFonctionInterlocuteur').val();

                        $.ajax({
                            async:false,
                            type: "POST",
                            data: {
                                idPdv: idPdv,
                                ztNomInterlocuteur: ztNomInterlocuteur,
                                ztPrenomInterlocuteur: ztPrenomInterlocuteur,
                                ztTelInterlocuteur_A: ztTelInterlocuteur_A,
                                ztTelInterlocuteur_B: ztTelInterlocuteur_B,
                                ztEmailInterlocuteur: ztEmailInterlocuteur,
                                zsFonctionInterlocuteur : zsFonctionInterlocuteur,
                                type: 3
                            },
                            url: "includes/functions/web2bdd/func.2.3-majInformationsPdv.php",
                            dataType: 'json',
                            success: function (data) {
                                toastr.success('Les informations ont bien été enregistrées !', "Interlocuteur point de vente")
                            }
                        });

                    });
                }

            }
        });
    },

    rechercheFonction: function (idFonction) {
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.2.3-rechercheFonction.php",
            dataType: 'json',
            success: function (data) {
                $('#zsFonctionInterlocuteur').empty();
                $.each(data.fonctions, function (i, InfoFonction) {
                    $('#zsFonctionInterlocuteur').append(
                        $('<option>')
                            .attr('value', InfoFonction.idFonctionInterlocuteurPdv)
                            .html(InfoFonction.libelleFonctionInterlocuteurPdv)
                            .attr('selected', (InfoFonction.idFonctionInterlocuteurPdv == idFonction) ? true : false)
                    );
                });
                //$('#zsFonctionInterlocuteur').select2();
            }
        });
    },

    rechercheCiblage: function (idCiblage) {
        $('#zsCiblage option[value=' + idCiblage + ']').attr('selected','selected');
    },

    rechercheCategorie: function (idCategorie) {
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.2.3-rechercheCategorie.php",
            dataType: 'json',
            success: function (data) {
                $('#zsCategorie').empty();
                $.each(data.categories, function (i, InfoCategorie) {
                    $('#zsCategorie').append(
                        $('<option>')
                            .attr('value', InfoCategorie.idCategoriePdv)
                            .html(InfoCategorie.libelleCategoriePdv)
                            .attr('selected', (InfoCategorie.idCategoriePdv == idCategorie) ? true : false)
                    );
                });

                $('#zsCategorie').unbind().bind('change', function () {
                    var idCategorie = $(this).attr('value');
                    ClassFichePointDeVente.rechercheSousCategorie(idCategorie);
                });
            }
        });
    },

    rechercheSousCategorie: function (idCategorie, idSousCategorie) {
        $.ajax({
            async:false,
            type: "POST",
            data: {idCategorie: idCategorie},
            url: "includes/functions/bdd2web/func.2.3-rechercheSousCategorie.php",
            dataType: 'json',
            success: function (data) {
                $('#zsSousCategorie').empty();
                $.each(data.scategories, function (i, InfoSousCategorie) {
                    $('#zsSousCategorie').append(
                        $('<option>')
                            .attr('value', InfoSousCategorie.idSousCategorie)
                            .html(InfoSousCategorie.libelleSousCategorie)
                            .attr('selected', (InfoSousCategorie.idSousCategorie == idSousCategorie) ? true : false)
                    );
                });
            }
        });
    },

    activatePDV: function(){

        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.2.3-modifPDV.php",
            type: "POST",
            data: {
                idPDV : idPdv,
                boolActif : 1
            },
            dataType: 'json'
        }).done(function( data ) {
            if(data.result == 0){
                toastr.warning("Le PDV a été désactivé");
            }else {
                toastr.success("Le PDV a été activé");
            }
        });
    },
}