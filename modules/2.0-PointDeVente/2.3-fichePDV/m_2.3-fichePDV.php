<input type="hidden" id="zt_idPdv_2_3"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Informations principales
                </div>
                <div class="tools" style="margin-top: 0px;">
                    <button class="btn blue bg-blue-su pull-right btn-maj-info btn-xs" ><i class="icon-check"></i> Mettre à
                        jour les informations principales
                    </button>
                </div>
            </div>
            <div class="portlet-body form">

                <div class="form-horizontal form-bordered form-row-stripped">
                    <div class="form-group">
                        <label class="control-label col-md-3">Catégorie :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsCategorie">
                                </select>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Sous-catégorie :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsSousCategorie">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Ciblage :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsCiblage">
                                    <option value=""></option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="HC">HC</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Libellé :</label>

                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input class="form-control" type="text" placeholder="" id="ztLibelle">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse 1 :</label>

                        <div class="col-md-9">
                            <input class="form-control" type="text" placeholder="" id="ztAdresseA">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse 2 :</label>

                        <div class="col-md-3">
                            <input class="form-control" type="text" placeholder="" id="ztAdresseB">
                        </div>
                        <label class="control-label col-md-3">Adresse 3 :</label>

                        <div class="col-md-3">
                            <input class="form-control" type="text" placeholder="" id="ztAdresseC">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Code Postal :</label>

                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input class="form-control" type="text" placeholder="" id="ztCodePostal">
                            </div>
                        </div>
                        <label class="control-label col-md-3">Ville :</label>

                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input class="form-control" type="text" placeholder="" id="ztVille">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Latitude :</label>

                        <div class="col-md-3">
                            <div class="input-icon right">
                                <input class="form-control" type="text" placeholder="" id="ztLatitude">
                            </div>
                        </div>
                        <label class="control-label col-md-3">Longitude :</label>

                        <div class="col-md-3">
                            <div class="input-icon right">
                                <input class="form-control" type="text" placeholder="" id="ztLongitude">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Commentaire :</label>

                        <div class="col-md-4">
                            <textarea class="form-control" rows="2" id="ztCommentaire"></textarea>
                        </div>

                        <label class="control-label col-md-2">Commentaire entrée en magasin :</label>

                        <div class="col-md-4">
                            <textarea class="form-control" rows="2" id="ztCommentaireEM"></textarea>
                        </div>
                    </div>

                    <?php
                        $ro = true;
                        if (
                                $_SESSION['idUtilisateur'.PROJECT_NAME] == 1 ||
                                $_SESSION['idUtilisateur'.PROJECT_NAME] == 1145 ||
                                $_SESSION['idUtilisateur'.PROJECT_NAME] == 1157 ||
                                $_SESSION['idUtilisateur'.PROJECT_NAME] == 1172
                        ) {
                            $ro = false;
                        }
                        echo '
                        <div class="form-group bg-red-pink">
                            <label class="control-label col-md-2">Code CIP :</label>
                            <div class="col-md-4">
                                <input '.($ro ? ' readonly disabled="disabled" ' : 'e').' class="form-control" type="text" placeholder="" id="ztMerval">
                            </div>
                        </div>
                        ';
                    ?>

                    <div class="form-group bg-green-turquoise">
                        <label class="control-label col-md-2">Ancien code CIP :</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" placeholder="" id="ztOldMerval">
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>

<div class="form-actions">
    <button class="btn bg bg-yellow-casablanca btn-desactivate-pdv"><i class="fa fa-archive"></i> Activer /
        Désactiver le PDV
    </button>
</div>
<br/>
<div style="clear: both"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue-chambray">
            <div class="portlet-title">
                <div class="caption">
                    Cartographie
                </div>
                <div class="tools" style="margin-top: 0px;">
                    <a title="" data-original-title="" href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="map" style="height: 800px;width: 100%;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Contact point de vente
                </div>
                <div class="tools" style="margin-top: 0px;">
                    <button class="btn blue bg-blue-su pull-right btn-maj-titulaire btn-xs" ><i class="icon-check"></i>
                        Mettre à jour les contacts
                    </button>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label ">Nom titulaire A :</label>

                            <input class="form-control" type="text" placeholder="" id="ztNomTitulaireA">
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Prénom titulaire A :</label>

                            <input class="form-control" type="text" placeholder="" id="ztPrenomTitulaireA">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label">Nom titulaire B :</label>

                            <input class="form-control" type="text" placeholder="" id="ztNomTitulaireB">
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Prénom titulaire B :</label>

                            <input class="form-control" type="text" placeholder="" id="ztPrenomTitulaireB">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label ">Téléphone :</label>

                            <input class="form-control" type="text" placeholder="" id="ztTelephoneTitulaire">
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Fax :</label>

                            <input class="form-control" type="text" placeholder="" id="ztFaxTitulaire">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12">
                            <label class="control-label ">Email :</label>

                            <input class="form-control" type="text" placeholder="" id="ztEmailTitulaire">
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Interlocuteur point de vente
                </div>
                <div class="tools" style="margin-top: 0px;">
                    <button class="btn blue bg-blue-su pull-right btn-maj-interlocuteur btn-xs" ><i class="icon-check"></i>
                        Mettre à jour l'interlocuteur
                    </button>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label">Nom :</label>

                            <input class="form-control" type="text" placeholder="" id="ztNomInterlocuteur">
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Prénom :</label>

                            <input class="form-control" type="text" placeholder="" id="ztPrenomInterlocuteur">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="control-label">Téléphone 1 :</label>

                            <input class="form-control" type="text" placeholder="" id="ztTelInterlocuteur_A">
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Téléphone 2 :</label>

                            <input class="form-control" type="text" placeholder="" id="ztTelInterlocuteur_B">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-12">
                            <label class="control-label">E-mail :</label>

                            <input class="form-control" type="text" placeholder="" id="ztEmailInterlocuteur">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Fonction :</label>
                            <select class="form-control" id="zsFonctionInterlocuteur">
                            </select>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Historique des interventions sur ce point de vente
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body ">
                <table class="table table-striped table-bordered table-hover" id="tableIntervention_2_3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellé campagne</th>
                        <th>Libellé mission</th>
                        <th>Intervenant</th>
                        <th>Date Intervention</th>
                        <th>ID contrat</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>