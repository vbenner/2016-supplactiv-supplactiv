jQuery(document).ready(function() {

    window.alert = (function() {
        var nativeAlert = window.alert;
        return function(message) {
            window.alert = nativeAlert;
            message.indexOf("DataTables warning") === 0 ?
                console.warn(message) :
                nativeAlert(message);
        }
    })();

    ClassFraisExcel.init();

});

var ClassFraisExcel = {
    init: function(){
        this.initTableFrais();
    },

    initTableFrais: function(){

        $('#tableFrais_7_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.7.0-listeFrais.php?dateFrais="+$('#ztDateIntervention').val(),
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableFrais_7_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
}
