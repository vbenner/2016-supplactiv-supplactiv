<?php

/**
 * Requete sql
 */
require_once 'includes/queries/queries.bdd2web.php';

$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 10) {
    $DT->sub(new DateInterval('P1M'));
}

$ListeTauxKM = array();
$RechercheTauxKmExc->bindValue(':dateFrais', '2014-04-01', PDO::PARAM_STR);
$RechercheTauxKmExc->execute();
while($InfoTaux = $RechercheTauxKmExc->fetch(PDO::FETCH_OBJ)){
    $ListeTauxKM[$InfoTaux->tarifKilometre] = 0;
}
?>

<!--<h4 style="color:red">Attention, maintenance en cours. Ne pas télécharger le fichier.</h4>-->
<input type="hidden" id="ztDateIntervention" value="<?php print $DT->format('Y-m-01') ?>" />

<div class="row">
    <div class="col-md-12" style="margin-bottom: 20px">
        <a class="btn bg bg-blue pull-right btn-lg" target="_blank" href="download/func.7.0-exportFraisExcel.php?dateFrais=<?php print $DT->format('Y-m-01') ?>"><i class="fa fa-file-excel-o"></i> Télécharger le fichier au format excel</a>
    </div>
</div>
<div class="row">


    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des frais d'interventions par intervenante pour le mois de <?php print  $DT->format('F Y') ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableFrais_7_0">
                    <thead>
                    <tr>
                        <th rowspan="2">Intervenant</th>
                        <th colspan="5" class="success">Montant Chargés</th>
                        <th colspan="4" class="active">Montant Non Chargés</th>
                        <th class="warning" colspan="<?php print sizeof($ListeTauxKM) ?>">KM</th>
                    </tr>
                    <tr>
                        <th class="success">P OBJ</th>
                        <th class="success">ANNUL</th>
                        <th class="success">AUTRE</th>
                        <th class="success">RDV</th>
                        <th class="success">FORM</th>
                        <th class="active">F JUST</th>
                        <th class="active">TEL</th>
                        <th class="active">AUT</th>
                        <th class="active">R ACO</th>
                        <?php
                        foreach($ListeTauxKM as $taux=>$km){
                            print '<th class="warning">'.$taux.'</th>';
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>