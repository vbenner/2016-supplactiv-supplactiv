<?php

/** -----------------------------------------------------------------------------------------------
 * Mise à jour
 */
/** Recherche du mois en cours */
$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 10) {
    $DT->sub(new DateInterval('P1M'));
}

$dernierJour = new DateTime($DT->format('Y-m-d') . ' 00:00:00');
$dernierJour->modify('last day of this month');

/**
 * Recherche des mois d'intervention
 */
$sqlRechercheDateIntervention = '
SELECT DISTINCT(DATE_FORMAT(dateDebut, "%Y-%m-01")) AS dateIntervention
FROM su_intervention
WHERE FK_idContrat IS NOT NULL
ORDER BY dateIntervention DESC';
$RechercheDateInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheDateIntervention);

/**
 * Recherche des intervenants travaillant pour la periode donnees
 *
 * @param dateDebut
 * @param dateFin
 *
 * @author Kevin MAURICE - Page UP
 */
$sqlRechercheIntervenant = "
SELECT DISTINCT(FK_idIntervenant), IF(genre = 'R', 1 , 2) AS civilite, nomIntervenant, nomIntervenant_JF, prenomIntervenant,
    numeroSS, adresseIntervenant_A, adresseIntervenant_B,
    adresseIntervenant_C, codePostalIntervenant, villeIntervenant, DATE_FORMAT(dateNaissance, '%d/%m/%Y') AS dateNaissance,
    villeNaissance, paysNaissance, numeroCarteSejour, DATE_FORMAT(dateFinSejour, '%d/%m/%Y') AS dateFinSejour,
    libelleBanque, codeBanque, codeGuichet, numeroCompte, cleSecurite, numeroIBAN, codeBIC,
(
	SELECT DATE_FORMAT(MIN(dateDebut), '%d/%m/%Y')
	FROM su_intervention
	WHERE FK_idContrat IS NOT NULL AND FK_idIntervenant = INTERVENANT.idIntervenant AND dateDebut IS NOT NULL
) AS dateAnciennete
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT DISTINCT(FK_idIntervenant), IF(genre = 'R', 1 , 2) AS civilite, nomIntervenant, nomIntervenant_JF, prenomIntervenant,
    numeroSS, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C,
    codePostalIntervenant, villeIntervenant, DATE_FORMAT(dateNaissance, '%d/%m/%Y') AS dateNaissance,
    villeNaissance, paysNaissance, numeroCarteSejour, DATE_FORMAT(dateFinSejour, '%d/%m/%Y') AS dateFinSejour,
    libelleBanque, codeBanque, codeGuichet, numeroCompte, cleSecurite, numeroIBAN,
    codeBIC,
(
	SELECT DATE_FORMAT(MIN(dateDebut), '%d/%m/%Y')
	FROM su_intervention
	WHERE FK_idContrat IS NOT NULL AND FK_idIntervenant = INTERVENANT.idIntervenant AND dateDebut IS NOT NULL
) AS dateAnciennete
FROM su_contrat_cidd INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE dateContrat = :dateContrat
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
GROUP BY FK_idIntervenant
ORDER BY FK_idIntervenant
";
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

$sqlRechercheDashboardSalarie = "

SELECT CONCAT('fa-user', '_', 'Nombre d\'intervenant CDD','_', COUNT(DISTINCT(FK_idIntervenant))) AS info
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT CONCAT('fa-file-archive-o', '_', 'Nombre d\'intervention','_', COUNT(idIntervention)) AS info
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT CONCAT('fa-user', '_', 'Nombre d\'intervenant CIDD','_', COUNT(DISTINCT(FK_idIntervenant))) AS info
FROM su_contrat_cidd INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE dateContrat = :dateContrat
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
";
$RechercheDashboardSalarieExc = DbConnexion::getInstance()->prepare($sqlRechercheDashboardSalarie);

?>
<input type="hidden" id="ztDateIntervention" value="<?php print $DT->format('Y-m-01') ?>" />
<div class="row">
    <?php
    $RechercheDashboardSalarieExc->bindValue(':dateDebut', $DT->format('Y-m-01') . ' 00:00:00');
    $RechercheDashboardSalarieExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheDashboardSalarieExc->bindValue(':dateContrat', $DT->format('Y-m-01'));

    #echo $DT->format('Y-m-01') . ' 00:00:00'.'<br/>';
    #echo $dernierJour->format('Y-m-d') . ' 23:59:59'.'<br/>';
    #echo $DT->format('Y-m-01').'<br/>';
    #die();
    $RechercheDashboardSalarieExc->execute();
    while($InfoDashboard = $RechercheDashboardSalarieExc->fetch(PDO::FETCH_OBJ)) {
        $Info = preg_split('/_/', $InfoDashboard->info);

        print '
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa '.$Info[0].'"></i>
                </div>
                <div class="details">
                    <div class="number">'.$Info[2].'</div>
                    <div class="desc">'.$Info[1].'</div>
                </div>
            </div>
        </div>';
    }

    /** @var $fp Fichier des salaries */
    $fName = 'download/Fichiers_SAL_GLOBAL'.'_'.date('Ymd_His').'.csv';
    $fp = fopen($fName,"w+");

    /** Recherche des intervenantes */
    $RechercheIntervenantExc->bindValue(':dateDebut', $DT->format('Y-m-01') . ' 00:00:00');
    $RechercheIntervenantExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheIntervenantExc->bindValue(':dateContrat', $DT->format('Y-m-01'));
    $RechercheIntervenantExc->execute();

    /** -------------------------------------------------------------------------------------------
     * Ecriture de la partie HEADER
     */
    $chaine = "Matricule;Civilité;Nom;Prénom;Nom Jeune fille;N° Sécu;Adres 1;Adres 2;";
    $chaine .= "Adres 3;Code postal;Ville;Pays résid;Date naiss;Ville naiss;Pays naiss;";
    $chaine .= "N° carte séjour;Date expir;Emploi;Code insee emploi;";
    #$chaine .= "Type salaire;Horaire collectif;";
    $chaine .= "Nom RIB;";
    $chaine .= "Nom Banque;BIC;non utilisé;";
    $chaine .= "non utilisé;non utilisé;non utilisé;non utilisé;non utilisé;non utilisé;";
    $chaine .= "IBAN;";
    $chaine .= "Date entrée;Date sortie;";
    $chaine .= "DADS-U : Code CNN;Niveau;Echelon;Date d'ancienneté;";
    $chaine .= "Motif sortie;Motif recours CDD;Durée intiale CDD;";
    $chaine .= "Statut catégoriel;Profil;Famille;Nature d'emploi (Code);Département Naissance";

    $chaine .= "\r\n";
    fwrite($fp, $chaine);

    while($InfoIntervenant = $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ)) {


        $Matricule = trim($InfoIntervenant->FK_idIntervenant);
        $DateFS = ($InfoIntervenant->dateFinSejour == '00/00/0000') ? '' : $InfoIntervenant->dateFinSejour;

        $IBAN1 = substr($InfoIntervenant->numeroIBAN, 0, 4);
        $IBAN2 = substr($InfoIntervenant->numeroIBAN, 4, 4);
        $IBAN3 = substr($InfoIntervenant->numeroIBAN, 8, 4);
        $IBAN4 = substr($InfoIntervenant->numeroIBAN, 12, 4);
        $IBAN5 = substr($InfoIntervenant->numeroIBAN, 16, 4);
        $IBAN6 = substr($InfoIntervenant->numeroIBAN, 20, 4);
        $IBAN7 = substr($InfoIntervenant->numeroIBAN, 24, 4);

        $numeroSS = str_replace(' ', '', $InfoIntervenant->numeroSS);

        $chaine = "$Matricule;";
        $chaine .= str_pad($InfoIntervenant->civilite, 2, 0, STR_PAD_LEFT).";";
        $chaine .= "$InfoIntervenant->nomIntervenant;";
        $chaine .= "$InfoIntervenant->prenomIntervenant;";
        $chaine .= "$InfoIntervenant->nomIntervenant_JF;$numeroSS;";
        $chaine .= "$InfoIntervenant->adresseIntervenant_A;$InfoIntervenant->adresseIntervenant_B;";
        $chaine .= "$InfoIntervenant->adresseIntervenant_C;$InfoIntervenant->codePostalIntervenant;";
        $chaine .= "$InfoIntervenant->villeIntervenant;FR;$InfoIntervenant->dateNaissance;";
        $chaine .= "$InfoIntervenant->villeNaissance;$InfoIntervenant->paysNaissance;";
        $chaine .= "$InfoIntervenant->numeroCarteSejour;$DateFS;";
        $chaine .= "Animateur;462d;";
        $chaine .= "$InfoIntervenant->nomIntervenant $InfoIntervenant->prenomIntervenant;";
        $chaine .= "$InfoIntervenant->libelleBanque;$InfoIntervenant->codeBIC;";
        $chaine .= ";;;;;;;";
        $chaine .= "$InfoIntervenant->numeroIBAN;".$DT->format('01/m/Y').";".$dernierJour->format('d/m/Y').";";
        #$chaine .= $DT->format('01/m/Y').";".$dernierJour->format('d/m/Y').";2;";
        $chaine .= "2098;1;120;$InfoIntervenant->dateAnciennete;";
        $chaine .= "008;02;01;06;NC_CDD_HORAIRE;FAMILLE_NC_CDD_HORAIRE;ANIMAT;".substr($numeroSS, 5, 2);

        #$chaine = str_replace("\n", "", $chaine);
        #$chaine = str_replace("\t", "", $chaine);
        #$chaine = str_replace("\r", "", $chaine);
        $chaine = str_replace(array("\r\n", "\n\r", "\n", "\r", "\t", "<br/>"), ',', $chaine);
        $chaine .= "\r\n";
        fwrite($fp, $chaine);
    }
    fclose($fp);
    ?>

    <div class="col-md-12">
        <a class="btn bg bg-green btn-block btn-lg" target="_blank" href="<?php echo $fName; ?>">Télécharger le fichier</a>
    </div>
</div>

