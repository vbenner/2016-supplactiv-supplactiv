<?php

/** Recherche du mois en cours */
$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 10) {
    $DT->sub(new DateInterval('P1M'));
}

$dernierJour = new DateTime($DT->format('Y-m-d') . ' 00:00:00');
$dernierJour->modify('last day of this month');

/**
 * Recherche des mois d'intervention
 */
$sqlRechercheDateIntervention = '
SELECT DISTINCT(DATE_FORMAT(dateDebut, "%Y-%m-01")) AS dateIntervention
FROM su_intervention
WHERE FK_idContrat IS NOT NULL
ORDER BY dateIntervention DESC';
$RechercheDateInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheDateIntervention);

/**
 * Recherche des intervenants travaillant pour la periode donnees
 *
 * @param dateDebut
 * @param dateFin
 *
 * @author Kevin MAURICE - Page UP
 */
$sqlRechercheIntervenant = "
SELECT DISTINCT(FK_idIntervenant), nomIntervenant, nomIntervenant_JF, prenomIntervenant, genre, numeroSS, situationFamiliale, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C, codePostalIntervenant, villeIntervenant, DATE_FORMAT(dateNaissance, '%d/%m/%Y') AS dateNaissance, villeNaissance, paysNaissance, nationalite, numeroCarteSejour, DATE_FORMAT(dateFinSejour, '%d/%m/%Y') AS dateFinSejour, nombreEnfant, libelleBanque, codeBanque, codeGuichet, numeroCompte, cleSecurite, numeroIBAN, codeBIC,
(
	SELECT DATE_FORMAT(MIN(dateDebut), '%d/%m/%Y')
	FROM su_intervention
	WHERE FK_idContrat IS NOT NULL AND FK_idIntervenant = INTERVENANT.idIntervenant AND dateDebut IS NOT NULL
) AS dateAnciennete
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT DISTINCT(FK_idIntervenant), nomIntervenant, nomIntervenant_JF, prenomIntervenant, genre, numeroSS, situationFamiliale, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C, codePostalIntervenant, villeIntervenant, DATE_FORMAT(dateNaissance, '%d/%m/%Y') AS dateNaissance, villeNaissance, paysNaissance, nationalite, numeroCarteSejour, DATE_FORMAT(dateFinSejour, '%d/%m/%Y') AS dateFinSejour, nombreEnfant, libelleBanque, codeBanque, codeGuichet, numeroCompte, cleSecurite, numeroIBAN, codeBIC,
(
	SELECT DATE_FORMAT(MIN(dateDebut), '%d/%m/%Y')
	FROM su_intervention
	WHERE FK_idContrat IS NOT NULL AND FK_idIntervenant = INTERVENANT.idIntervenant AND dateDebut IS NOT NULL
) AS dateAnciennete
FROM su_contrat_cidd INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE dateContrat = :dateContrat
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
GROUP BY FK_idIntervenant
ORDER BY FK_idIntervenant
";
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

$sqlRechercheDashboardSalarie = "

SELECT CONCAT('fa-user', '_', 'Nombre d\'intervenant CDD','_', COUNT(DISTINCT(FK_idIntervenant))) AS info
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT CONCAT('fa-file-archive-o', '_', 'Nombre d\'intervention','_', COUNT(idIntervention)) AS info
FROM su_intervention INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
UNION
SELECT CONCAT('fa-user', '_', 'Nombre d\'intervenant CIDD','_', COUNT(DISTINCT(FK_idIntervenant))) AS info
FROM su_contrat_cidd INTERVENTION
	INNER JOIN su_intervenant INTERVENANT ON INTERVENANT.idIntervenant = INTERVENTION.FK_idIntervenant
WHERE dateContrat = :dateContrat
AND INTERVENANT.boolAutoEntrepreneuse <> 'OUI'
";
$RechercheDashboardSalarieExc = DbConnexion::getInstance()->prepare($sqlRechercheDashboardSalarie);

?>
<input type="hidden" id="ztDateIntervention" value="<?php print $DT->format('Y-m-01') ?>" />
<div class="row">
    <?php
    $RechercheDashboardSalarieExc->bindValue(':dateDebut', $DT->format('Y-m-01') . ' 00:00:00');
    $RechercheDashboardSalarieExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheDashboardSalarieExc->bindValue(':dateContrat', $DT->format('Y-m-01'));
    $RechercheDashboardSalarieExc->execute();
    while($InfoDashboard = $RechercheDashboardSalarieExc->fetch(PDO::FETCH_OBJ)) {
        $Info = preg_split('/_/', $InfoDashboard->info);

        print '
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa '.$Info[0].'"></i>
                </div>
                <div class="details">
                    <div class="number">'.$Info[2].'</div>
                    <div class="desc">'.$Info[1].'</div>
                </div>
            </div>
        </div>';
    }

    /** @var $fp Fichier des salaries */
    $fName = 'download/Fichiers_SAL_GLOBAL'.'_'.date('Ymd_His').'.csv';
    $fp = fopen($fName,"w+");

    /** Recherche des intervenantes */
    $RechercheIntervenantExc->bindValue(':dateDebut', $DT->format('Y-m-01') . ' 00:00:00');
    $RechercheIntervenantExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheIntervenantExc->bindValue(':dateContrat', $DT->format('Y-m-01'));
    $RechercheIntervenantExc->execute();
    while($InfoIntervenant = $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ)) {


        $Matricule = addCaracToString(trim($InfoIntervenant->FK_idIntervenant), 5, '0');
        $DateFS = ($InfoIntervenant->dateFinSejour == '00/00/0000') ? '' : $InfoIntervenant->dateFinSejour;

        $IBAN1 = substr($InfoIntervenant->numeroIBAN, 0, 4);
        $IBAN2 = substr($InfoIntervenant->numeroIBAN, 4, 4);
        $IBAN3 = substr($InfoIntervenant->numeroIBAN, 8, 4);
        $IBAN4 = substr($InfoIntervenant->numeroIBAN, 12, 4);
        $IBAN5 = substr($InfoIntervenant->numeroIBAN, 16, 4);
        $IBAN6 = substr($InfoIntervenant->numeroIBAN, 20, 4);
        $IBAN7 = substr($InfoIntervenant->numeroIBAN, 24, 4);

        $numeroSS = str_replace(' ', '', $InfoIntervenant->numeroSS);

        $chaine = "$Matricule;00001;$InfoIntervenant->nomIntervenant;$InfoIntervenant->prenomIntervenant;$InfoIntervenant->genre;$InfoIntervenant->nomIntervenant_JF;$numeroSS;$InfoIntervenant->situationFamiliale;$InfoIntervenant->adresseIntervenant_A;$InfoIntervenant->adresseIntervenant_B;$InfoIntervenant->adresseIntervenant_C;$InfoIntervenant->codePostalIntervenant;$InfoIntervenant->villeIntervenant;FR;$InfoIntervenant->dateNaissance;$InfoIntervenant->villeNaissance;$InfoIntervenant->paysNaissance;$InfoIntervenant->nationalite;$InfoIntervenant->numeroCarteSejour;$DateFS;$InfoIntervenant->nombreEnfant;NCAD6;2;2;\$E;Animateur;551A;0;0;1;BQUE1;$InfoIntervenant->nomIntervenant $InfoIntervenant->prenomIntervenant;$InfoIntervenant->libelleBanque;$InfoIntervenant->codeBIC;$IBAN1;$IBAN2;$IBAN3;$IBAN4;$IBAN5;$IBAN6;$IBAN7;".$DT->format('01/m/Y').";".$dernierJour->format('d/m/Y').";".$DT->format('01/m/Y').";".$dernierJour->format('d/m/Y').";2;2098;001;008;1;120;$InfoIntervenant->dateAnciennete;";
        #$chaine = str_replace("\n", "", $chaine);
        #$chaine = str_replace("\t", "", $chaine);
        #$chaine = str_replace("\r", "", $chaine);
        $chaine = str_replace(array("\r\n", "\n\r", "\n", "\r", "\t", "<br/>"), ',', $chaine);
        $chaine .= "\r\n";
        fwrite($fp, $chaine);
    }
    fclose($fp);
    ?>

    <div class="col-md-12">
        <a class="btn bg bg-green btn-block btn-lg" target="_blank" href="<?php echo $fName; ?>">Télécharger le fichier</a>
    </div>
</div>