var tableKM_13_0, dateRechercheD_13_0, dateRechercheF_13_0, idIntervenant_13_0, idPointdeVente_13_0, boolPlanification_13_0;
jQuery(document).ready(function() {
    ClassKM.init();
});

var ClassKM = {
    init: function () {

        this.initListeIntervenante();

        this.rechercheSessionModule();

        this.initDateRecherche();

        $(document.body).on("change", "#idIntervenant_13_0", function () {
            ClassKM.saveSearchToSession();
        });

        this.initTableKM();

        this.initButton();

        $("#fi").fileinput();
    },


    initButton: function(){
        $('.btn-valid-recherche-km').bind('click', function(){
           if(($('#dateRechercheD_13_0').val()).trim() != '' && ($('#dateRechercheD_13_0').val()).trim() != ''){
               ClassKM.saveSearchToSession();
           }
        });
    },

    rechercheSessionModule: function(){
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: { idModule : '13_0', dateRechercheD_13_0 : '', dateRechercheF_13_0 : '', idIntervenant_13_0 : 'ALL', idPointdeVente_13_0 : 'ALL', boolPlanification_13_0 : 0},
            dataType: "json"
        }).done(function( data ) {
            $('#dateRechercheD_13_0').val(data.sessions.dateRechercheD_13_0);
            $('#dateRechercheF_13_0').val(data.sessions.dateRechercheF_13_0);

            idIntervenant_13_0 = data.sessions.idIntervenant_13_0;
            idPointdeVente_13_0 = data.sessions.idPointdeVente_13_0;
            boolPlanification_13_0 = data.sessions.boolPlanification_13_0;

            $('#boolPlanification_13_0').prop('checked', (boolPlanification_13_0 == 1) ? true : false);

            $('#idIntervenant_13_0 option[value="'+idIntervenant_13_0+'"]').prop('selected', true);

            $("#idIntervenant_13_0").select2();
            ClassKM.recherchePointDeVenteVisite();
        });
    },

    initDateRecherche: function(){
        $('.input-daterange').datepicker();
    },

    saveSearchToSession: function(){

        boolTMP = ($('#boolPlanification_13_0').is(":checked")) ? 1 : 0;
        if(boolTMP != boolPlanification_13_0 || idIntervenant_13_0 != $('#idIntervenant_13_0').val() || dateRechercheD_13_0 != $('#dateRechercheD_13_0').val() || dateRechercheF_13_0 != $('#dateRechercheF_13_0').val() || idPointdeVente_13_0 != $('#idPointdeVente_13_0').val()){
            dateRechercheD_13_0 = $('#dateRechercheD_13_0').val();
            dateRechercheF_13_0 = $('#dateRechercheF_13_0').val();
            idIntervenant_13_0 = $('#idIntervenant_13_0').val();
            idPointdeVente_13_0 = $('#idPointdeVente_13_0').val();
            boolPlanification_13_0 = ($('#boolPlanification_13_0').is(":checked")) ? 1 : 0;
            $.ajax({
                async:false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    dateRechercheD_13_0 : dateRechercheD_13_0,
                    dateRechercheF_13_0 : dateRechercheF_13_0,
                    idIntervenant_13_0 : idIntervenant_13_0,
                    idPointdeVente_13_0 : idPointdeVente_13_0,
                    boolPlanification_13_0 : boolPlanification_13_0
                },
                dataType: 'json'
            }).done(function( data ) {

                ClassKM.recherchePointDeVenteVisite();
                tableKM_13_0.api().ajax.reload();
            });
        }
    },

    initListeIntervenante: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {  },
            url: "includes/functions/bdd2web/func.6.0-listeIntervenante.php",
            dataType: 'json',
            success: function(data) {
                $('#idIntervenant_13_0').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.intervenants, function (index, InfoIntervenant) {
                    $('#idIntervenant_13_0').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant+' - '+InfoIntervenant.nomIntervenant+' '+InfoIntervenant.prenomIntervenant)
                    )
                });

            }
        });
    },

    recherchePointDeVenteVisite: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {  },
            url: "includes/functions/bdd2web/func.13.0-recherchePdvVisiste.php",
            dataType: 'json',
            success: function(data) {
                $("#idPointdeVente_13_0").select2("destroy");
                $('#idPointdeVente_13_0').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data, function (index, InfoPDV) {
                    $('#idPointdeVente_13_0').append(
                        $('<option>')
                            .attr('value', InfoPDV.idPdv)
                            .html('['+InfoPDV.codeMerval+'] '+InfoPDV.libellePdv+' - '+InfoPDV.adressePdv_A+' '+InfoPDV.codePostalPdv+' '+InfoPDV.villePdv)
                    )
                });
                $('#idPointdeVente_13_0 option[value="'+idPointdeVente_13_0+'"]').prop('selected', true);

                $('#idPointdeVente_13_0').select2();
            }
        });
    },

    initTableKM: function(){

        tableKM_13_0 = $('#tableKM_13_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.13.0-listeKMIntervenant.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableKM_13_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    validModifKm: function(info){
        var infoCouple = info.split('_');
        var nbKm = $('#km_'+info).val();

        $.ajax({
            async:false,
            type: "POST",
            data: {
                idPdv : infoCouple[0],
                idIntervenant : infoCouple[1],
                nbKm : nbKm
            },
            url: "includes/functions/web2bdd/func.13.0-modifKmCouple.php",
            dataType: 'json',
            success: function(data) {
                toastr.success('Les KM ont bien été enregistrés', 'Ajout / Modification des Kilomètres');
            }
        });
    },

    resultImport : function(nbLigneOK, nbLigneTotal){
        if(nbLigneOK == 0){
            toastr.error('Veuillez vérifier votre fichier', 'ERREUR - Importation du fichier');
        }else {
            toastr.success('Le fichier a bien été importé', 'Importation du fichier');
        }
    }
}