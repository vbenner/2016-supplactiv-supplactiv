<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Importation d'un fichier kilomètres
                </div>
            </div>
            <div class="portlet-body form">
                <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importKmPdvIntervenant.php" target="uploadFrame" method="post">
                    <div id="uploadInfos">
                        <div id="uploadStatus"></div>
                        <iframe id="uploadFrame" name="uploadFrame" style="width:100%;height:0px;display: none"></iframe>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Fichier Excel :</label>
                            <div class="col-md-10">
                                <fieldset class="label_side">
                                    <div>

                                        <div id=fi" class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input span3"
                                                     data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                        class="fileinput-filename"></span>
                                                </div>
                                        <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">Choisir un fichier </span>
                                            <span class="fileinput-exists">Modifier </span>
                                            <input type="file" id="fichierPDV" name="fichierPDV">
                                        </span>
                                                <a href="#" class="input-group-addon btn red fileinput-exists"
                                                   data-dismiss="fileinput">Supprimer </a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <span class="help-block">Fichier Excel uniquement. Le détail de ce fichier est disponible dans le fichier de documentation.</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">

                        <button id="uploadSubmit" type="submit" class="btn btn-primary pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group input-daterange">
                            <label class="control-label col-md-2">Date début :</label>

                            <div class="col-md-4">
                                <input id="dateRechercheD_13_0" class="form-control form-recherche" type="text"
                                       placeholder="" name="start" style="text-align: left" value=""
                                       onchange="ClassKM.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <label class="control-label col-md-2">Date fin :</label>

                            <div class="col-md-4">
                                <input id="dateRechercheF_13_0" class="form-control form-recherche" type="text"
                                       placeholder="" name="end" style="text-align: left" value=""
                                       onchange="ClassKM.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Intervenant :</label>

                            <div class="col-md-4">
                                <select id="idIntervenant_13_0" name="idIntervenant_13_0"
                                        class="form-control form-recherche">

                                </select>
                                <span class="help-block"> Facultatif </span>
                            </div>

                            <label class="control-label col-md-2">Non planifiée</label>

                            <div class="col-md-4 ">
                                <input type="checkbox" id="boolPlanification_13_0" value="1" />
                                <span class="help-block"> Facultatif </span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Point de vente :</label>

                            <div class="col-md-10 div-conteneur-pdv">
                                <select id="idPointdeVente_13_0" name="idPointdeVente_13_0"
                                        class="form-control form-recherche">

                                </select>
                                <span class="help-block"> Facultatif </span>

                            </div>
                        </div>
                        <div class="form-actions">
                            <a class="btn btn-primary pull-left" target="_blank" href="download/func.13.0-exportKmIntervenant.php"><i class="fa fa-file"></i> Télécharger les KMS</a>

                            <button class="btn btn-primary pull-right btn-valid-recherche-km"><i
                                    class="fa  fa-search "></i> Rechercher
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<table class="table table-striped table-bordered table-hover" id="tableKM_13_0">
    <thead>
    <tr>
        <th>Code CIP</th>
        <th>Libellé</th>
        <th>Code Postal</th>
        <th>Ville</th>
        <th>Intervenant</th>
        <th>Adresse</th>
        <th>Code Postal</th>
        <th>Ville</th>
        <th>KM Aller</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>