var idIntervenant_10_0, semaine_10_0, tableHeureSup_10_0;

jQuery(document).ready(function() {
    ClassHeureSupp.init();
});

var ClassHeureSupp = {
    init: function(){
        /** Initialisation de la liste intervenante */
        this.initListeIntervenante();

        /** Initialisation des semaines */
        this.initSemaine();

        this.rechercheSessionModule();

        $(document.body).on("change","#zsIntervenante",function(){
            ClassHeureSupp.saveSearchToSession();
        });
        $(document.body).on("change","#zsSemaine",function(){
            ClassHeureSupp.saveSearchToSession();
        });
    },

    rechercheSessionModule: function(){
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: { idModule : '10_0', idIntervenant_10_0 : 'ALL', semaine_10_0 : 'ALL'},
            dataType: "json"
        }).done(function( data ) {
            $('#zsIntervenante option[value="'+data.sessions.idIntervenant_10_0+'"]').prop('selected', true);
            $('#zsSemaine option[value="'+data.sessions.semaine_10_0+'"]').prop('selected', true);

            $('#zsIntervenante').select2();
            $('#zsSemaine').select2();
            /** Initialisation des Heures */
            ClassHeureSupp.initTableHeureSup();
        });
    },

    saveSearchToSession: function(){
        if(idIntervenant_10_0 != $('#zsIntervenante').val() || semaine_10_0 != $('#zsSemaine').val()){
            idIntervenant_10_0 = $('#zsIntervenante').val();
            semaine_10_0 = $('#zsSemaine').val();

            $.ajax({
                async:false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    semaine_10_0 : semaine_10_0,
                    idIntervenant_10_0 : idIntervenant_10_0
                },
                dataType: 'json'
            }).done(function( data ) {
                tableHeureSup_10_0.api().ajax.reload();
            });
        }
    },

    initListeIntervenante: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {  },
            url: "includes/functions/bdd2web/func.6.0-listeIntervenante.php",
            dataType: 'json',
            success: function(data) {
                $('#zsIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.intervenants, function (index, InfoIntervenant) {
                    $('#zsIntervenante').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant+' - '+InfoIntervenant.nomIntervenant+' '+InfoIntervenant.prenomIntervenant)
                    )
                });

            }
        });
    },

    initSemaine : function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {  },
            url: "includes/functions/general/func.getWeekOfYear.php",
            dataType: 'json',
            success: function(data) {
                $('#zsSemaine').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data, function (index, InfoSemaine) {
                    $('#zsSemaine').append(
                        $('<option>')
                            .attr('value', InfoSemaine.debut+'_'+InfoSemaine.fin)
                            .html('['+InfoSemaine.indiceF+'] Du '+InfoSemaine.debutF+' au '+InfoSemaine.finF)
                    )
                });

            }
        });
    },

    initTableHeureSup: function(){

        tableHeureSup_10_0 = $('#tableHeureSup_10_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.10.0-listeHeureSupplementaire.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableHeureSup_10_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    }
}
