
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body" style="padding: 0">
                <div class="form-horizontal">
                    <div class="form-body form-bordered">
                        <div class="form-group">
                            <label class="control-label col-md-3">Choisir une intervenante :</label>

                            <div class="col-md-9">
                                <select id="zsIntervenante" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Choisir une semaine :</label>

                            <div class="col-md-9">
                                <select id="zsSemaine" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableHeureSup_10_0">
            <thead>
            <tr>
                <th>#</th>
                <th>Intervenant</th>
                <th>Semaine</th>
                <th>Total Heure</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
