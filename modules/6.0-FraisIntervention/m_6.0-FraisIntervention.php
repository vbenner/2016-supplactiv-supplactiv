<?php
/** -----------------------------------------------------------------------------------------------
 * 2020-01-06 / La session de paye va jusqu'au 10
 */
$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 10) {
    $DT->sub(new DateInterval('P1M'));
}
?>
<input type="hidden" id="ztDateIntervention" value="<?php print $DT->format('Y-m-01')?>" />
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Saisie des frais d'intervention par intervenante pour le mois de <?php print  $DT->format('F Y') ?>
                </div>
                <div class="actions">
                    <a id="btnImportEXPENSYA" class="btn red-flamingo" href="#">Import EXPENSYA</a>
                </div>
                <div class="actions" style="margin-left: 5px;margin-right: 5px;">
                    <a id="btnImportEXPENSYADetail" class="btn red-flamingo" href="#">Import EXPENSYA (Détail)</a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body form-bordered">
                        <div class="form-group">
                            <label class="control-label col-md-3">Choisir une intervenante :</label>

                            <div class="col-md-9">
                                <select id="zsIntervenante" class="form-control"></select>
                            </div>
                        </div>

                        <div class="form-frais">
                            <h2 class="caption-subject font-blue-sup bold uppercase center-block ">MONTANT CHARG&Eacute;S</h2>

                            <h3 class="form-section fwb">Prime sur objectif (16 + 17)</h3>
                            <div class="row margin-bottom-20">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztPrimeObjectif"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Commentaire" id="ztCommentairePrimeObjectif"/>
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section fwb">Indemnité intervention annulée (28 + 29)</h3>
                            <div class="row margin-bottom-20">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztInterventionAnnulee"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Commentaire" id="ztCommentaireInterventionAnnulee"/>
                                    </div>
                                </div>
                            </div>


                            <h3 class="form-section fwb">Autre Prime (26 + 27)</h3>
                            <div class="row margin-bottom-20">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztAutrePrime"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input class="form-control margin-top-10" type="text" placeholder="Commentaire" id="ztCommentaireAutrePrime"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6">
                                    <h3 class="form-section fwb">Prime RDV</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztPrimeRDV"/>
                                </div>

                                <div class="col-md-6">
                                    <h3 class="form-section fwb">Indemnité formation</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztIndemniteFormation"/>
                                </div>
                            </div>

                            <h2 class="caption-subject font-blue-sup bold uppercase center-block ">MONTANT NON CHARG&Eacute;S</h2>
                            <div class="row ">
                                <div class="col-md-2">
                                    <h3 class="form-section fwb">Frais sur justificatif</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztFraisJustificatif"/>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="form-section fwb">Dont TVA</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztFraisJustificatifTVA"/>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="form-section fwb">Indemnité téléphone</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztIndemniteTelephone"/>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="form-section fwb">Ind RDV Tel</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztAutreFrais"/>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="form-section fwb">Reprise Acompte</h3>
                                    <input class="form-control margin-top-10" type="text" placeholder="Saisir un montant" id="ztRepriseAcompte"/>
                                </div>
                            </div>

                            <h3 class="form-section fwb">Nombre de kilomètres</h3>
                            <div class="row margin-bottom-20 div-km">

                            </div>
                            <div class="form-actions">
                                <button class="btn bg bg-green pull-right btn-valid-frais">Valider les frais</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>