jQuery(document).ready(function() {
    ClassFraisIntervention.init();
});

var ClassFraisIntervention = {
    init: function(){
        $('.form-frais').hide();
        this.initListeUtilisateur();

        /** ---------------------------------------------------------------------------------------
         * Exécution du script d'import
         */
        $(document).on('click', '#btnImportEXPENSYA', function () {
            var btn = $(this);
            btn.prop('disabled', true).removeClass('red-flamingo').addClass('grey');
            $.ajax({
                async:false,
                type: "POST",
                url: "download/sftpGet.php",
                dataType: 'json',
                success: function (data) {
                    if (data.ERR == 0) {
                        toastr.success('Import EXPENSYA terminé', 'Traitement');
                    }
                    btn.prop('disabled', false).addClass('red-flamingo').removeClass('grey');
                }
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Exécution du script d'import
         */
        $(document).on('click', '#btnImportEXPENSYADetail', function () {
            var btn = $(this);
            btn.prop('disabled', true).removeClass('red-flamingo').addClass('grey');
            $.ajax({
                async:false,
                type: "POST",
                url: "download/sftpGetDetail.php",
                dataType: 'json',
                success: function (data) {
                    if (data.ERR == 0) {
                        toastr.success('Import EXPENSYA DETAIL terminé', 'Traitement');
                    }
                    btn.prop('disabled', false).addClass('red-flamingo').removeClass('grey');
                }
            });
        });

    },

    initListeUtilisateur: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {dateFrais : $('#ztDateIntervention').val()},
            url: "includes/functions/bdd2web/func.6.0-listeIntervenante.php",
            dataType: 'json',
            success: function (data) {
                $('#zsIntervenante').empty();
                $('#zsIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir une intervenante')
                );
                $.each( data.intervenants, function( i, InfoIntervenant ) {
                    $('#zsIntervenante').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.nomIntervenant+' '+InfoIntervenant.prenomIntervenant)
                    );
                });

                $('#zsIntervenante').unbind().bind('change', function(){
                    var idIntervenant = $(this).attr('value');
                    ClassFraisIntervention.rechercheFrais(idIntervenant);
                });

                $('#zsIntervenante').select2({
                    placeholder: "Choisir une intervenante",
                    allowClear: true
                });

            }
        });
    },

    rechercheFrais: function(idIntervenant){
        $.ajax({
            async:false,
            type: "POST",
            data: { dateFrais : $('#ztDateIntervention').val(), idIntervenant : idIntervenant},
            url: "includes/functions/bdd2web/func.6.0-fraisIntervention.php",
            dataType: 'json',
            success: function (data) {
                $('.form-frais').hide();
                if(data.result == 1){
                    $('.form-frais').show();
                    // MONTANT CHARGE
                    $('#ztPrimeObjectif').val(data.frais.montantPrimeObjectif);
                    $('#ztInterventionAnnulee').val(data.frais.montantIndemniteAnnulation);
                    $('#ztAutrePrime').val(data.frais.montantAutrePrime);
                    $('#ztPrimeRDV').val(data.frais.montantPrimeRDV);
                    $('#ztIndemniteFormation').val(data.frais.montantIndemniteFormation);
                    $('#ztCommentairePrimeObjectif').val(data.frais.commentairePrimeObjectif);
                    $('#ztCommentaireInterventionAnnulee').val(data.frais.commentaireIndemniteAnnulation);
                    $('#ztCommentaireAutrePrime').val(data.frais.commentaireAutrePrime);

                    // MONTANT NON CHARGE
                    $('#ztFraisJustificatif').val(data.frais.montantFraisJustificatif);
                    $('#ztFraisJustificatifTVA').val(data.frais.montantFraisJustificatifTVA);
                    $('#ztIndemniteTelephone').val(data.frais.montantIndemniteTelephone);
                    $('#ztAutreFrais').val(data.frais.montantAutreFrais);
                    $('#ztRepriseAcompte').val(data.frais.montantRepriseAccompte);

                    $('.div-km').empty();
                    $.each( data.kms, function( i, InfoKm ) {

                        var couleurDiv = (InfoKm.tarifKilometre == '0.00') ? 'bg bg-red text-white' : '';
                        $('.div-km').append(
                            $('<div>').addClass('col-md-2').append(
                                $('<div>').addClass('form-group '+couleurDiv).append(
                                    $('<input>')
                                        .addClass('form-control margin-top-10')
                                        .attr('placeholder', "NB Kilomètre")
                                        .val(InfoKm.totalKilometre)
                                        .attr('data-id', InfoKm.tarifKilometre)
                                        .attr('id', 'km_'+(InfoKm.tarifKilometre).replace('.', '-'))
                                ).append(
                                    $('<span>').addClass('input-group-addon '+couleurDiv).html(InfoKm.tarifKilometre+' &euro;/km')
                                )
                            )
                        )
                    });

                    $('.btn-valid-frais').unbind().bind('click', function(){
                        ClassFraisIntervention.modificationFrais(idIntervenant, data.kms);
                    });

                }else {
                    $('.portlet-body input[type="text"]').val('');
                }
            }
        });
    },

    modificationFrais: function(idIntervenant, listeKm){

        var nbKM = '';
        $.each( listeKm, function( i, InfoKm ) {
            nbKM += InfoKm.tarifKilometre+'_'+($('#km_'+(InfoKm.tarifKilometre).replace('.', '-')).val())+'|';
        });

        $.ajax({
            async:false,
            type: "POST",
            data: {
                dateFrais: $('#ztDateIntervention').val(),
                idIntervenant: idIntervenant,
                ztPrimeObjectif : $('#ztPrimeObjectif').val(),
                ztInterventionAnnulee : $('#ztInterventionAnnulee').val(),
                ztAutrePrime : $('#ztAutrePrime').val(),
                ztPrimeRDV : $('#ztPrimeRDV').val(),
                ztIndemniteFormation : $('#ztIndemniteFormation').val(),
                ztCommentairePrimeObjectif : $('#ztCommentairePrimeObjectif').val(),
                ztCommentaireInterventionAnnulee : $('#ztCommentaireInterventionAnnulee').val(),
                ztCommentaireAutrePrime : $('#ztCommentaireAutrePrime').val(),
                ztFraisJustificatif : $('#ztFraisJustificatif').val(),
                ztFraisJustificatifTVA : $('#ztFraisJustificatifTVA').val(),
                ztIndemniteTelephone : $('#ztIndemniteTelephone').val(),
                ztAutreFrais : $('#ztAutreFrais').val(),
                ztRepriseAcompte : $('#ztRepriseAcompte').val(),
                kms : nbKM

            },
            url: "includes/functions/web2bdd/func.6.0-modificationFraisIntervention.php",
            dataType: 'json',
            success: function (data) {

            }
        });
    }
}
