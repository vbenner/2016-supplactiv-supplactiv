jQuery(document).ready(function() {
    ClassEVP.init();
});

var ClassEVP = {
    init: function(){
        $('#aLink').attr('disabled', true);
        this.initTableEVP();
    },

    initTableEVP: function(){

        $('#tableEVP_8_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 50,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.8.0-listeEvp.php?dateEvp="+$('#ztDateIntervention').val(),
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var api = this.api();
                var json = api.ajax.json();
                $('#aLink').attr('disabled', false).removeClass('bg-red').addClass('bg-green');
                $('#aLink').attr('href', 'download/EVP/' + json.fichier);
            },
            fnInitComplete: function(oSettings, json) {
                //console.log(json);
            }
        });
        var tableWrapper = $('#tableEVP_8_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
}
