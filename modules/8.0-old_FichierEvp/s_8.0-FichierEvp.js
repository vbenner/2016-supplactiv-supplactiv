jQuery(document).ready(function() {
    ClassEVP.init();
});

var ClassEVP = {
    init: function(){
        this.initTableEVP();
    },

    initTableEVP: function(){

        $('#tableEVP_8_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.8.0-listeEvp.php?dateEvp="+$('#ztDateIntervention').val(),
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableEVP_8_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
}
