<?php

/**
 * Requete sql
 */
require_once 'includes/queries/queries.bdd2web.php';

$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 9) {
    $DT->sub(new DateInterval('P1M'));
}

?>
<input type="hidden" id="ztDateIntervention" value="<?php print $DT->format('Y-m-01') ?>" />

<div class="well">
    <h3>
        Fichier EVP pour le mois de <?php print  $DT->format('F Y') ?>
        <a class="btn bg bg-green pull-right" target="_blank" href="download/EVP/<?php print $DT->format('Y-m-01') ?>EVP.csv"><i class="fa fa-file-excel-o"></i> Télécharger le fichier des EVP</a>
    </h3>
</div>

                <table class="table table-striped table-bordered table-hover" id="tableEVP_8_0">
                    <thead>
                    <tr>
                        <th>Intervenant</th>
                        <th>Nb interventions</th>
                        <th>Nb heures</th>
                        <th>Montant brut</th>
                        <th>Nb repas</th>
                        <th>Montant repas</th>
                        <th>Nb km</th>
                        <th>Montant km</th>
                        <th>Acompte</th>
                        <th>Prime OBJ</th>
                        <th>Prime RDV</th>
                        <th>Indemnités F</th>
                        <th>Frais JUSTIF</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
