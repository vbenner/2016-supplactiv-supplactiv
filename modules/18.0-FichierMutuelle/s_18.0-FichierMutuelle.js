var tableMutuelle_17_6;

jQuery(document).ready(function() {
    ClassMutuelle.init();
});

var ClassMutuelle = {
    init: function(){
        this.initDatatableMutuelle();
    },

    initDatatableMutuelle: function(){

        tableMutuelle_17_6 = $('#tableMutuelle_17_6').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 50,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.17.6-listeMutuelle.php?datatable",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableClient_4_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    telechargementPriseAccord: function(idMission, idPdv){


        $.ajax({
            async:false,
            type: "POST",
            data: {idMission_17_3: idMission, idPdv_17_3: idPdv},
            url: "includes/functions/web2session/func.majSession.php",
            dataType: 'json',
            success: function (data) {
                window.location.href = 'download/func.17.3-exportPriseAccord.php';
            }
        });
    }
}
