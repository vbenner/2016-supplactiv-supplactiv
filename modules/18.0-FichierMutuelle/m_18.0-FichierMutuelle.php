<div class="well">
    <a class="btn bg bg-blue pull-right" target="_blank" href="includes/functions/datatable/func.17.6-listeMutuelle.php">Télécharger au format XLSX</a>
    <div style="clear: both"></div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableMutuelle_17_6">
    <thead>
    <tr>
        <th>N° Sécurité sociale</th>
        <th>Intervenant</th>
        <th>Date de naissance</th>
        <th>Date</th>
        <th>Nb heure</th>
        <th>IBAN</th>
        <th>BIC</th>
        <th>Banque</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>