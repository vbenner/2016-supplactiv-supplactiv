<input type="hidden" id="zt_idClient_5_3"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Informations sur le client
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body ">
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a data-toggle="tab" href="#tab_5_1"> Informations sur le client </a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#tab_5_2">Interlocuteur Client </a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#tab_5_3">Signature & Logo</a>
                        </li>
                    </ul>
                    <div class="tab-content form">
                        <div id="tab_5_1" class="tab-pane active">
                            <div class="form-horizontal form-bordered form-row-stripped">

                                <h3 class="form-section fwb" style="padding-left: 5px;">Informations principales</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Type d'activité :</label>

                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <i class="fa fa-warning tooltips tooltips" data-container="body"
                                               data-original-title="Obligatoire"></i>
                                            <select class="form-control" id="zsActivite">

                                            </select>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-3">Filière :</label>

                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <i class="fa fa-warning tooltips tooltips" data-container="body"
                                               data-original-title="Obligatoire"></i>
                                            <select class="form-control" id="zsFiliere">

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">SIRET :</label>

                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztSiret"/>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-3">Raison Sociale :</label>

                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <i class="fa fa-warning tooltips tooltips" data-container="body"
                                               data-original-title="Obligatoire"></i>
                                            <input type="text" class="form-control" id="ztRaisonSociale"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Téléphone :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztTelephone"/>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-3">Fax :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztFax"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Site Web :</label>
                                    <div class="col-md-9">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztSiteWeb"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Compléments :</label>
                                    <div class="col-md-9">
                                        <div class="input-icon left">
                                            <textarea class="form-control" id="ztComplement"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group input-daterange">
                                    <label class="control-label col-md-3">Date création :</label>
                                    <div class="col-md-3">
                                        <input id="dateCreation" class="form-control form-recherche" type="text" placeholder="" name="dateCreation" style="text-align: left" value="" />
                                        <span class="help-block"> Obligatoire </span>
                                    </div>
                                </div>

                                <h3 class="form-section fwb" style="padding-left: 5px;">Adresse du siège</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Adresse :</label>
                                    <div class="col-md-9">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztAdresseSiege"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Code Postal :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztCodePostalSiege"/>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-3">Ville :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztVilleSiege"/>
                                        </div>
                                    </div>
                                </div>

                                <h3 class="form-section fwb" style="padding-left: 5px;">Adresse de facturation</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Destinataire :</label>
                                    <div class="col-md-9">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztDestinataireFacturation"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Adresse :</label>
                                    <div class="col-md-9">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztAdresseFacturation"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Code Postal :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztCodePostalFacturation"/>
                                        </div>
                                    </div>
                                    <label class="control-label col-md-3">Ville :</label>
                                    <div class="col-md-3">
                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztVilleFacturation"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <button class="btn blue bg-blue-su pull-right btn-modif-client"><i class="icon-check"></i> Mettre à jour les informations</button>
                                </div>
                            </div>
                        </div>
                        <div id="tab_5_2" class="tab-pane" style="padding: 10px;">

                                    <table class="table table-striped table-bordered table-hover" id="tableInterlocuteur_4_3" >
                                        <thead>
                                        <tr>
                                            <th>Sexe</th>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Tel</th>
                                            <th>E-mail</th>
                                            <th>Fonction</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                            <div class="form-horizontal form-bordered form-row-stripped">

                                <h3 class="form-section fwb" style="padding-left: 5px;">Ajout d'un interlocuteur client</h3>
                                <div class="form-group">

                                    <div class="col-md-4">
                                        <label class="control-label">Sexe :</label>
                                        <div class="input-icon left">
                                            <select class="form-control" id="zsSexeAdd">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Nom :</label>

                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztNomAdd" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Prénom :</label>

                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztPrenomAdd" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">Tel fixe :</label>

                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztTelFixeAdd" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">Tel mobile :</label>

                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztTelMobileAdd" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">E-mail :</label>

                                        <div class="input-icon left">
                                            <input type="text" class="form-control" id="ztEmailAdd" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">Fonction :</label>
                                        <div class="input-icon left">
                                            <select class="form-control" id="zsFonctionAdd">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button class="btn bg bg-green pull-right btn-add-interlocuteur">Ajouter l'interlocuteur</button>
                                </div>
                            </div>


                        </div>
                        <div id="tab_5_3" class="tab-pane">
                            <div class="alert alert-info" style="margin: 10px">
                                Attention, la tailles des images ne doit pas dépasser 200Pixel de largeur / 130Pixel de hauteur. Dans le cas contraire, l'image sera redimensionné automatiquement.
                            </div>

                            <img id="img-logo" style="margin:0px 0px 10px 10px" src="data:image/gif;base64," />
                            <img id="img-signature" style="margin:0px 0px 10px 10px" src="data:image/gif;base64," />

                            <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importLogo.php?type=logo" target="uploadFrame" method="post">
                                <div id="uploadInfos">
                                    <div id="uploadStatus"></div>
                                    <iframe id="uploadFrame" name="uploadFrame" style="width:100%;height:0px;display: none"></iframe>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Logo :</label>
                                    <div class="col-md-8">
                                        <fieldset class="label_side">
                                            <div>
                                                <div id=fi" class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input span3"
                                                             data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                                class="fileinput-filename"></span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Choisir un logo </span>
                                                            <span class="fileinput-exists">Modifier </span>
                                                            <input type="file" id="fichierLOGO" name="fichierLOGO">
                                                        </span>
                                                        <a href="#" class="input-group-addon btn red fileinput-exists"
                                                           data-dismiss="fileinput">Supprimer </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <span class="help-block">Fichier PNG uniquement..</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="hidden" id="idClient_LOGO" name="idClient_LOGO"
                                               value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
                                        <button id="uploadSubmit" type="submit" class="btn btn-primary pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                                    </div>
                                </div>

                            </form>

                            <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importLogo.php?type=signature" target="uploadFrame2" method="post">
                                <div id="uploadInfos2">
                                    <div id="uploadStatus2"></div>
                                    <iframe id="uploadFrame2" name="uploadFrame2" style="width:100%;height:0px;display: none"></iframe>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Signature :</label>
                                    <div class="col-md-8">
                                        <fieldset class="label_side">
                                            <div>

                                                <div id=fi" class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input span3"
                                                             data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                                class="fileinput-filename"></span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Choisir une signature </span>
                                                            <span class="fileinput-exists">Modifier </span>
                                                            <input type="file" id="fichierSIGNATURE" name="fichierSIGNATURE">
                                                        </span>
                                                        <a href="#" class="input-group-addon btn red fileinput-exists"
                                                           data-dismiss="fileinput">Supprimer </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <span class="help-block">Fichier PNG uniquement..</span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="hidden" id="idClient_SIGNATURE" name="idClient_SIGNATURE"
                                               value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
                                        <button id="uploadSubmit2" type="submit" class="btn btn-primary pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                                    </div>
                                </div>

                            </form>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                   Ajout d'une campagne
                </div>
            </div>
            <div class="portlet-body ">
                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">
                        <label class="control-label col-md-3">Libellé campagne :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">

                                <input type="text" class="form-control" id="ztlibelleCampagne"/>
                                <span class="help-block"> Obligatoire </span>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Interlocuteur client :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">

                                <select class="form-control" id="ztIntClient">

                                </select>
                                <span class="help-block"> Obligatoire </span>

                            </div>
                        </div>
                        <label class="control-label col-md-3">Interlocuteur Agence :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">

                                <select class="form-control" id="ztIntAgence">

                                </select>
                                <span class="help-block"> Obligatoire </span>

                            </div>
                        </div>
                    </div>

                    <div class="form-group input-daterange">
                        <label class="control-label col-md-3">Date début :</label>
                        <div class="col-md-3">
                            <input id="dateDCampagne" class="form-control form-recherche" type="text" placeholder="" name="start" style="text-align: left" value="" />
                            <span class="help-block"> Obligatoire </span>
                        </div>
                        <label class="control-label col-md-3">Date fin :</label>
                        <div class="col-md-3">
                            <input id="dateFCampagne" class="form-control form-recherche" type="text" placeholder="" name="end" style="text-align: left" value="" />
                            <span class="help-block"> Obligatoire </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Tarif Formation € :</label>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztFormation" value="0"/>
                            </div>
                        </div>

                        <label class="control-label col-md-2">Tarif Animation € :</label>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAnimation" value="0"/>
                            </div>
                        </div>

                        <label class="control-label col-md-2">Tarif Merchandising € :</label>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztMerchandising" value="0"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Commentaire :</label>
                        <div class="col-md-9">
                            <div class="input-icon left">
                                <textarea class="form-control" id="ztCommentaireC"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn bg bg-green pull-right btn-ajout-campagne">Ajouter la campagne</button>
                    </div>
                    <div style="clear: both"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Historique des campagnes du client</h3><hr>
    </div>
</div>
<table class="table table-striped table-bordered table-hover" id="tableCampagne_4_3" >
    <thead>
    <tr>
        <th>Libellé campagne</th>
        <th>Date début</th>
        <th>Date fin</th>
        <th>Nb Intervention(s)</th>
        <th>Interlocuteur client</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>