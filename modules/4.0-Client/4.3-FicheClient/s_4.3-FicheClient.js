var idClient, tableCampagne_4_3;
jQuery(document).ready(function() {
    idClient = $('#zt_idClient_5_3').val();
    ClassFicheClient.init();
});

var ClassFicheClient = {
    init: function () {
        $('.input-daterange').datepicker({
            dateFormat: "dd/mm/yy"
        });

        this.rechercheInfoClient();
        this.rechercheCampagneClient();
        //this.rechercheCampagnePrec();
        $('.btn-modif-client').bind('click', function(){
           ClassFicheClient.modifClient();
        });

        $('.btn-add-interlocuteur').bind('click', function(){
            ClassFicheClient.modificationInterlocuteur('NV');
        });

        $('.btn-ajout-campagne').bind('click', function(){
            ClassFicheClient.ajoutCampagne();
        })
    },

    ajoutCampagne: function(){
        ztlibelleCampagne = $('#ztlibelleCampagne').val();
        ztIntClient = $('#ztIntClient').val();
        ztIntAgence = $('#ztIntAgence').val();
        dateDCampagne = $('#dateDCampagne').val();
        dateFCampagne = $('#dateFCampagne').val();
        ztFormation = $('#ztFormation').val();
        ztAnimation = $('#ztAnimation').val();
        ztMerchandising = $('#ztMerchandising').val();
        ztCommentaireC = $('#ztCommentaireC').val();

        if(ztlibelleCampagne.trim() != '' && dateDCampagne.trim() != '' && dateFCampagne.trim() != ''){
            $.ajax({
                async : false,
                type: "POST",
                data: {
                    idClient: idClient,
                    ztlibelleCampagne : ztlibelleCampagne,
                    ztIntClient : ztIntClient,
                    ztIntAgence : ztIntAgence,
                    dateDCampagne : dateDCampagne,
                    dateFCampagne : dateFCampagne,
                    ztFormation : ztFormation,
                    ztAnimation : ztAnimation,
                    ztMerchandising : ztMerchandising,
                    ztCommentaireC : ztCommentaireC
                },
                url: "includes/functions/web2bdd/func.4.3-ajoutCampagne.php",
                dataType: 'json',
                success: function (data) {
                    if(data.result == 1){
                        tableCampagne_4_3.api().ajax.reload();
                        toastr.success('Campagne ajoutée', "Ajout d'une campagne");
                    }else {
                        toastr.error('Les dates sont incorrectes !', "Ajout d'une campagne");
                    }

                }
            });
        }else {
            toastr.error("Veuillez saisir un libellé, une date de début et de fin !", "Ajout d'une campagne");
        }

    },

    modifClient: function(){
        if ($('#dateCreation').val() == '') {
            toastr.error('La date de création du client est obligatoire', 'Modification du client');
            return false;
        }
        $.ajax({
            async : false,
            type: "POST",
            data: {
                idClient: idClient,
                idSiret : $('#ztSiret').val(),
                libelleClient : $('#ztRaisonSociale').val(),
                telClient : $('#ztTelephone').val(),
                faxClient : $('#ztFax').val(),
                emailClient : $('#ztSiteWeb').val(),
                commentaireClient : $('#ztComplement').val(),
                dateCreation : $('#dateCreation').val(),
                adresseClient : $('#ztAdresseSiege').val(),
                codePostalClient : $('#ztCodePostalSiege').val(),
                villeClient : $('#ztVilleSiege').val(),
                destinataireFacturationClient : $('#ztDestinataireFacturation').val(),
                adresseFacturationClient : $('#ztAdresseFacturation').val(),
                codePostalFacturationClient : $('#ztCodePostalFacturation').val(),
                villeFacturationClient : $('#ztVilleFacturation').val(),
                idTypeActivite : $('#zsActivite').val(),
                idFiliere : $('#zsFiliere').val()
            },
            url: "includes/functions/web2bdd/func.4.3-modfificationClient.php",
            dataType: 'json',
            success: function (data) {
                toastr.success('Les informations ont bien été enregistrées', 'Modification du client');
            }
        });

    },

    rechercheInfoClient: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idClient: idClient},
            url: "includes/functions/bdd2web/func.4.3-infoClient.php",
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {

                    // Informations generales
                    $('#ztSiret').val(data.infoClient.idSiret);
                    $('#ztRaisonSociale').val(data.infoClient.libelleClient);
                    $('#ztTelephone').val(data.infoClient.telClient);
                    $('#ztFax').val(data.infoClient.faxClient);
                    $('#ztSiteWeb').val(data.infoClient.emailClient);
                    $('#ztComplement').val(data.infoClient.commentaireClient);

                    if (data.infoClient.dateCreation != '') {
                        var dateFR = new Date(data.infoClient.dateCreation + ' 00:00:00');
                        var dd = dateFR.getDate();
                        var mm = dateFR.getMonth()+1;
                        var yyyy = dateFR.getFullYear();

                        if(dd < 10)
                        {
                            dd='0'+dd;
                        }

                        if(mm<10)
                        {
                            mm='0'+mm;
                        }
                        $('#dateCreation').datepicker('setDate', dd+'/'+mm+'/'+yyyy);
                    }

                    $('#ztAdresseSiege').val(data.infoClient.adresseClient);
                    $('#ztCodePostalSiege').val(data.infoClient.codePostalClient);
                    $('#ztVilleSiege').val(data.infoClient.villeCLient);

                    $('#ztDestinataireFacturation').val(data.infoClient.destinataireFacturationClient);
                    $('#ztAdresseFacturation').val(data.infoClient.adresseFacturationClient);
                    $('#ztCodePostalFacturation').val(data.infoClient.codePostalFacturationClient);
                    $('#ztVilleFacturation').val(data.infoClient.villeFacturationClient);

                    $('#img-logo').attr('src', 'data:image/gif;base64,'+data.infoClient.logoClient);
                    $('#img-signature').attr('src', 'data:image/gif;base64,'+data.infoClient.signatureClient);

                    $('#zsFiliere').empty();
                    $.each( data.filieres, function( i, InfoFiliere ) {
                        $('#zsFiliere').append(
                            $('<option>')
                                .attr('value', InfoFiliere.idFiliere)
                                .html(InfoFiliere.libelleFiliere)
                                .attr('selected', (InfoFiliere.idFiliere == data.infoClient.FK_idFiliere) ? true : false)
                        );
                    });

                    $('#zsActivite').empty();
                    $.each( data.activites, function( i, InfoActivite ) {
                        $('#zsActivite').append(
                            $('<option>')
                                .attr('value', InfoActivite.idTypeActivite)
                                .html(InfoActivite.libelleTypeActivite)
                                .attr('selected', (InfoActivite.idTypeActivite == data.infoClient.FK_idTypeActivite) ? true : false)
                        );
                    });

                    $('#tableInterlocuteur_4_3 tbody').empty();
                    $('#ztIntAgence').empty();
                    $('#ztIntClient').empty();

                    $.each( data.interlocuteursAgence, function( i, InfoInterlocuteur ) {

                        $('#ztIntAgence').append(
                            $('<option>').attr('value', InfoInterlocuteur.idInterlocuteurAgence).html(InfoInterlocuteur.nomInterlocuteurAgence+' '+InfoInterlocuteur.prenomInterlocuteurAgence)
                        );
                    });

                    var titreInterlocuteur = '';
                    $.each( data.titres_interlocuteurs, function( i, InfoTitre ) {
                        titreInterlocuteur += '<option value="'+InfoTitre.idTitreInterlocuteurClient+'" >'+InfoTitre.libelleTitreInterlocuteurClient+'</option>';

                    });
                    $('#zsFonctionAdd').empty().append(titreInterlocuteur);
                    $('#zsSexeAdd').empty()
                        .append($('<option>').attr('value', 'M').html('MASCULIN').attr('selected', true))
                        .append($('<option>').attr('value', 'S').html('FEMININ').attr('selected', false));

                    $.each( data.interlocuteurs, function( i, InfoInterlocuteur ) {






                        $('#ztIntClient').append(
                            $('<option>').attr('value', InfoInterlocuteur.idInterlocuteurClient).html(InfoInterlocuteur.nomInterlocuteurClient+' '+InfoInterlocuteur.prenomInterlocuteurClient)
                        );

                        $('#tableInterlocuteur_4_3 tbody').append(
                            $('<tr>')
                                .append($('<td>').html(
                                    $('<select>').addClass('form-control').attr('id', 'sexInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient)
                                        .append($('<option>').attr('value', 'M').html('MASCULIN').attr('selected', (InfoInterlocuteur.sexeInterlocuteurClient == 'M') ? true : false))
                                        .append($('<option>').attr('value', 'S').html('FEMININ').attr('selected', (InfoInterlocuteur.sexeInterlocuteurClient == 'S') ? true : false))
                                ))
                                .append($('<td>').html($('<input>').attr('type', 'text').attr('id', 'nomInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient).addClass('form-control').val(InfoInterlocuteur.nomInterlocuteurClient)))
                                .append($('<td>').html($('<input>').attr('type', 'text').attr('id', 'prenomInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient).addClass('form-control').val(InfoInterlocuteur.prenomInterlocuteurClient)))

                                .append($('<td>')
                                    .append('Fixe : ').append($('<input>').attr('type', 'text').attr('id', 'telFixeInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient).addClass('form-control').val(InfoInterlocuteur.telInterlocuteurClient_A))

                                    .append('<br />Mobile : ').append($('<input>').attr('type', 'text').attr('id', 'telPortableInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient).addClass('form-control').val(InfoInterlocuteur.telInterlocuteurClient_B)
                                    ))

                                .append($('<td>').html($('<input>').attr('type', 'text').attr('id', 'mailInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient).addClass('form-control').val(InfoInterlocuteur.mailInterlocuteurClient)))
                                .append($('<td>').html(
                                    $('<select>').addClass('form-control').attr('id', 'titreInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient)
                                        .append(titreInterlocuteur)
                                ))

                                .append($('<td>')
                                    .append($('<button>').attr('data-id', InfoInterlocuteur.idInterlocuteurClient).addClass('btn-modif-interlocuteur btn btn-xs bg bg-green btn-block').css('margin-bottom', '10px').html('Modifier').unbind().bind('click', function(){
                                        ClassFicheClient.modificationInterlocuteur($(this).attr('data-id'))
                                    }))
                                    .append((InfoInterlocuteur.nbCampagne == 0) ? $('<button>').attr('data-id', InfoInterlocuteur.idInterlocuteurClient).addClass('btn-sup-interlocuteur btn btn-xs bg bg-red btn-block').html('Supprimer').unbind().bind('click', function(){
                                        ClassFicheClient.suppressionInterlocuteur($(this).attr('data-id'))
                                    }) : '')
                                )
                            );

                        $('#titreInterlocuteurClient_'+InfoInterlocuteur.idInterlocuteurClient+' option[value="'+InfoInterlocuteur.FK_idTitreInterlocuteurClient+'"]').prop('selected', true);
                    });


                }
            }
        });
    },

    suppressionInterlocuteur : function(idInterlocuteur){
        nom = $('#nomInterlocuteurClient_'+idInterlocuteur).val();
        prenom = $('#prenomInterlocuteurClient_'+idInterlocuteur).val();
        $.ajax({
            async : false,
            type: "POST",
            data: {
                idInterlocuteur : idInterlocuteur,
                boolSup : 1
            },
            url: "includes/functions/web2bdd/func.4.3-modificationInterlocuteurClient.php",
            dataType: 'json',
            success: function (data) {
                toastr.success("L'interlocuteur <strong>"+nom+" "+prenom+"</strong> a bien été supprimé", "Suppression d'un interlocuteur");
                ClassFicheClient.rechercheInfoClient();
            }
        });
    },

    modificationInterlocuteur : function(idInterlocuteur){
        var sexe, nom, prenom, tel, mobile, email, statut;

        if(idInterlocuteur != 'NV') {
            sexe = $('#sexInterlocuteurClient_' + idInterlocuteur).val();
            nom = $('#nomInterlocuteurClient_' + idInterlocuteur).val();
            prenom = $('#prenomInterlocuteurClient_' + idInterlocuteur).val();
            tel = $('#telFixeInterlocuteurClient_' + idInterlocuteur).val();
            mobile = $('#telPortableInterlocuteurClient_' + idInterlocuteur).val();
            email = $('#mailInterlocuteurClient_' + idInterlocuteur).val();
            statut = $('#titreInterlocuteurClient_' + idInterlocuteur).val();
        }else {
            sexe = $('#zsSexeAdd').val();
            nom = $('#ztNomAdd').val();
            prenom = $('#ztPrenomAdd').val();
            tel = $('#ztTelFixeAdd').val();
            mobile = $('#ztTelMobileAdd').val();
            email = $('#ztEmailAdd').val();
            statut = $('#zsFonctionAdd').val();
        }

        $.ajax({
            async : false,
            type: "POST",
            data: {
                idInterlocuteur : idInterlocuteur,
                sexe : sexe,
                nom : nom,
                prenom : prenom,
                tel : tel,
                mobile : mobile,
                email : email,
                statut : statut,
                idClient : idClient
            },
            url: "includes/functions/web2bdd/func.4.3-modificationInterlocuteurClient.php",
            dataType: 'json',
            success: function (data) {
                if(idInterlocuteur != 'NV') {
                    toastr.success("L'interlocuteur <strong>" + nom + " " + prenom + "</strong> a bien été modifié", "Modification d'un interlocuteur");
                }else {
                    toastr.success("L'interlocuteur <strong>" + nom + " " + prenom + "</strong> a bien été ajouté", "Ajout d'un interlocuteur");
                }
                ClassFicheClient.rechercheInfoClient();
            }
        });
    },

    rechercheCampagneClient: function(){

        tableCampagne_4_3 = $('#tableCampagne_4_3').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.4.3-listeCampagneClient.php?idClient="+idClient,
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(5)', nRow).children('button').bind('click', function(){
                    document.location.href='index.php?ap=4.0-Client&ss_m=4.4-FicheCampagne&idClient='+idClient+'&s='+$(this).attr('data-id');
                 })
            }
        });
        var tableWrapper = $('#tableCampagne_4_3_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    }
}