var idClient, tableCampagne_4_3;
jQuery(document).ready(function () {
    idClient = 0;
    ClassFicheClient.init();
});

var ClassFicheClient = {
    init: function () {
        this.rechercheInfoClient();

        $('.btn-modif-client').bind('click', function () {
            if(($('#ztRaisonSociale').val()).trim() != "") {
                ClassFicheClient.modifClient();
            }else toastr.error("Vous devez saisir une raison sociale !", "Ajout d'un client");
        });
    },

    modifClient: function () {
        $.ajax({
            async : false,
            type: "POST",
            data: {
                idClient: idClient,
                idSiret: $('#ztSiret').val(),
                libelleClient: $('#ztRaisonSociale').val(),
                telClient: $('#').val(),
                faxClient: $('#ztFax').val(),
                emailClient: $('#ztSiteWeb').val(),
                commentaireClient: $('#ztComplement').val(),
                adresseClient: $('#ztAdresseSiege').val(),
                codePostalClient: $('#ztCodePostalSiege').val(),
                villeCLient: $('#ztVilleSiege').val(),
                destinataireFacturationClient: $('#ztDestinataireFacturation').val(),
                adresseFacturationClient: $('#ztAdresseFacturation').val(),
                codePostalFacturationClient: $('#ztCodePostalFacturation').val(),
                villeFacturationClient: $('#ztVilleFacturation').val(),
                idTypeActivite: $('#zsActivite').val(),
                idFiliere: $('#zsFiliere').val()
            },
            url: "includes/functions/web2bdd/func.4.3-modfificationClient.php",
            dataType: 'json',
            success: function (data) {
                if(data.result != 0){
                    document.location.href = 'index.php?ap=4.0-Client&ss_m=4.3-FicheClient&s='+data.result;
                }
                toastr.success('Les informations ont bien été enregistrées', 'Ajout d\'un client');
            }
        });

    },

    rechercheInfoClient: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idClient: idClient},
            url: "includes/functions/bdd2web/func.4.3-infoClient.php",
            dataType: 'json',
            success: function (data) {

                $('#ztTelephone').inputmask('99-99-99-99-99');
                $('#ztFax').inputmask('99-99-99-99-99');

                $('#zsFiliere').empty();
                $.each(data.filieres, function (i, InfoFiliere) {
                    $('#zsFiliere').append(
                        $('<option>')
                            .attr('value', InfoFiliere.idFiliere)
                            .html(InfoFiliere.libelleFiliere)
                    );
                });

                $('#zsActivite').empty();
                $.each(data.activites, function (i, InfoActivite) {
                    $('#zsActivite').append(
                        $('<option>')
                            .attr('value', InfoActivite.idTypeActivite)
                            .html(InfoActivite.libelleTypeActivite)
                    );
                });
            }
        });
    }
}