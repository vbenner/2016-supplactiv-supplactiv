<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Formulaire d'ajout d'un client
                </div>
            </div>
            <div class="portlet-body form">

                <div class="form-horizontal form-bordered form-row-stripped" style="margin-top: -20px !important;">
                    <h3 class="form-section fwb" style="padding-left: 5px;">Informations principales</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3">Type d'activité :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsActivite">

                                </select>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Filière :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsFiliere">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">SIRET :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztSiret"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Raison Sociale :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztRaisonSociale"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Téléphone :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTelephone"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Fax :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztFax"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Site Web :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztSiteWeb"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Compléments :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <textarea class="form-control" id="ztComplement"></textarea>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section fwb" style="padding-left: 5px;">Adresse du siège</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseSiege"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Code Postal :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztCodePostalSiege"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Ville :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztVilleSiege"/>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section fwb" style="padding-left: 5px;">Adresse de facturation</h3>

                    <div class="form-group">
                        <label class="control-label col-md-3">Destinataire :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztDestinataireFacturation"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseFacturation"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Code Postal :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztCodePostalFacturation"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Ville :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztVilleFacturation"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn blue bg-blue-su pull-right btn-modif-client"><i
                                class="icon-check"></i> Ajouter le client
                        </button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>