var tableClient_4_1;
jQuery(document).ready(function() {
    ClassClient.init();
});

var ClassClient = {
    init: function(){
        this.initTableClient();
    },

    initTableClient: function(){

        tableClient_4_1 = $('#tableClient_4_1').dataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>rB>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                    extend: 'copy',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 5, 6, 7 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 5, 6, 7 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 5, 6, 7 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                    extend: 'pdf',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 5, 6, 7 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                    extend: 'print',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 5, 6, 7 ]
                    },
                },
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                buttons: {
                    copyTitle: 'Ajouté au presse-papier',
                    copySuccess: {
                        _: '%d lignes copiées',
                        1: '1 ligne copiée'
                    }
                }
            },

            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.4.1-listeClient.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0)', nRow).children('button').bind('click', function(){
                    document.location.href='index.php?ap=4.0-Client&ss_m=4.3-FicheClient&s='+$(this).attr('data-id');
                });
            },
            fnInitComplete: function () {
                $('.dt-buttons').css({
                    'margin-left':'15px',
                    'padding-bottom':'5px'
                });

            }
        });
        var tableWrapper = $('#tableClient_4_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    suppressionClient: function(idClient){


        $.ajax({
            async : false,
            type: "POST",
            data: {
                idClient : idClient
            },
            url: "includes/functions/web2bdd/func.4.1-suppressionClient.php",
            dataType: 'json',
            success: function (data) {
                toastr.success('Le client a bien été supprimé', 'Suppression client');
                tableClient_4_1.api().ajax.reload();
            }
        });
    }
}
