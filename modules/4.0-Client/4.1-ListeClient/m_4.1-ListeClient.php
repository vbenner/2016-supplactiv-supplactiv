<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des clients enregistrés dans la Base de Données
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableClient_4_1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ID#</th>
                        <th>Libellé</th>
                        <th>Adresse</th>
                        <th>Code Postal</th>
                        <th>Ville</th>
                        <th>Tél. fixe</th>
                        <th>Site Web / E-mail</th>
                        <th>Interlocuteur</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>