var idCampagne, idClient, tableMission_4_4;
jQuery(document).ready(function() {
    idCampagne = $('#zt_idCampagne_5_4').val();
    idClient = $('#zt_idClient_5_4').val();
    ClassFicheCampagne.init();
});

var ClassFicheCampagne = {
    init: function () {
        this.rechercheInfoCampagne();
        this.rechercheMissionCampagne();
        this.rechercheTypeMission();
        //this.rechercheCampagnePrec();
        this.rechercheChargeMission();
        this.initButton();
    },

    initButton: function(){
      $('.btn-valid-add-mission').bind('click', function(){
          var libelleMission, typeMission, gammeProduit, descriptionProduit, nbInterPrevisionnel, fraisGestion, chargeMission;
          libelleMission = ($('#ztLibelleMissionAdd').val()).trim();
          typeMission = $('#zsTypeMissionAdd').val();
          gammeProduit = ($('#ztGammeProduitAdd').val()).trim();
          descriptionProduit = ($('#ztCommentaireAdd').val()).trim();
          nbInterPrevisionnel = ($('#ztNbInterPrevisionnelAdd').val()).trim();
          fraisGestion = ($('#ztFraisGestionAdd').val()).trim();
          chargeMission = $('#zsChargeMissionAdd').val();
          if (chargeMission == '') {
              toastr.error('Le chargé de mission doit être renseigné.', ' Informations mission');
              return false;
          }
          if(libelleMission != ''){

              $.ajax({
                  async:false,
                  type: "POST",
                  data: {
                      idCampagne : idCampagne,
                      libelleMission: libelleMission,
                      typeMission : typeMission,
                      gammeProduit : gammeProduit,
                      descriptionProduit : descriptionProduit,
                      nbInterPrevisionnel : nbInterPrevisionnel,
                      fraisGestion : fraisGestion,
                      chargeMission : chargeMission
                  },
                  url: "includes/functions/web2bdd/func.4.4-ajoutMission.php",
                  dataType: 'json',
                  success: function (data) {

                      $('#ztLibelleMissionAdd').val('');
                      $('#ztGammeProduitAdd').val('');
                      $('#ztCommentaireAdd').val('');
                      $('#ztNbInterPrevisionnelAdd').val('');
                      $('#ztFraisGestionAdd').val('');
                      tableMission_4_4.api().ajax.reload();
                      toastr.success('La mission a bien été enregistrée', 'Ajout Mission');
                  }
              });

          }else toastr.error("Le libellé de la mission est vide !", "ERREUR - Ajout Mission");
      });

        $('.btn-valid-info-campagne').unbind().bind('click', function(){
            var libelleCampagne = ($('#ztLibelleCampagne').val()).trim();
            var dateFinCampagne = ($('#ztDateFinCampagne').val()).trim();
            var commentaireCampagne = ($('#ztCommentaireCampagne').val()).trim();
            var oldCampagne = $('#zsCampagnePrec').val();

            var tarifA = ($('#ztTarifA').val()).trim();
            var tarifF = ($('#ztTarifF').val()).trim();
            var tarifM = ($('#ztTarifM').val()).trim();
            var tarifF2 = ($('#ztTarifF2').val()).trim();
            var tarifF3 = ($('#ztTarifF3').val()).trim();

            var interlocuteurClient = $('#zsInterlocuteurClient').val();
            var interlocuteurAgence = $('#zsInterlocuteurAgence').val();
            if(libelleCampagne != '' && dateFinCampagne != ''){
                $.ajax({
                    async : false,
                    type: "POST",
                    data: {
                        idCampagne: idCampagne,
                        libelleCampagne: libelleCampagne,
                        dateFinCampagne: dateFinCampagne,
                        commentaireCampagne: commentaireCampagne,
                        interlocuteurClient: interlocuteurClient,
                        interlocuteurAgence : interlocuteurAgence,
                        oldCampagne : oldCampagne,

                        tarifA : tarifA,
                        tarifF : tarifF,
                        tarifM : tarifM,
                        tarifF2 : tarifF2,
                        tarifF3 : tarifF3,

                    },
                    url: "includes/functions/web2bdd/func.4.4-modificationCampagne.php",
                    dataType: 'json',
                    success: function (data) {
                        toastr.success('Les informations ont bien été enregistrées', 'Modificarion de la campagne');
                    }
                });
            }else {
                toastr.error("Le libellé / date de la campagne est vide !", "ERREUR - Modification informations campagne");
            }
        });
    },

    rechercheInfoCampagne: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idCampagne: idCampagne},
            url: "includes/functions/bdd2web/func.4.4-infoCampagne.php",
            dataType: 'json',
            success: function (data) {

                if (data.locked == 1) {
                    $('#ztTarifA').attr('disabled', true);
                    $('#ztTarifM').attr('disabled', true);
                    $('#ztTarifF').attr('disabled', true);
                    $('#ztTarifF2').attr('disabled', true);
                    $('#ztTarifF3').attr('disabled', true);
                }
                $.each( data.tarif, function( i, InfoType ) {
                    var tarif = (InfoType.tarif != 0) ? '//  '+InfoType.tarif+' €' : '';
                    $('.details_'+i+' .number').html(InfoType.nbIntervention+' inter.(s)');
                    $('.details_'+i+' .desc').html(tarif);
                });

                $('.details_frais .number').html(data.frais.fraisSalaire+' €');
                $('.details_frais .desc').html('Frais : '+data.frais.fraisIntervenant+' €');
                $('.details_frais .desc').html('');

                $('#ztLibelleCampagne').val(data.info.libelleCampagne);
                $('#ztDateFinCampagne').val(data.info.dateFin);

                $('#ztTarifA').val(data.info.tarifAnimation);
                $('#ztTarifF').val(data.info.tarifFormation);
                $('#ztTarifF2').val(data.info.tarifFormation2);
                $('#ztTarifF3').val(data.info.tarifFormation3);
                $('#ztTarifM').val(data.info.tarifMarchandising);

                $('#ztCommentaireCampagne').val(data.info.descriptionCampagne);

                $.each( data.interlocuteursClient, function( i, InfoInterlocuteur ) {
                    $('#zsInterlocuteurClient').append(
                        $('<option>')
                            .attr('value', InfoInterlocuteur.idInterlocuteurClient)
                            .html(InfoInterlocuteur.nomInterlocuteurClient+' '+InfoInterlocuteur.prenomInterlocuteurClient)
                            .attr('selected', (InfoInterlocuteur.idInterlocuteurClient == data.info.FK_idInterlocuteurClient) ? true : false)
                    );
                });

                $.each( data.interlocuteursAgence, function( i, InfoInterlocuteur ) {
                    $('#zsInterlocuteurAgence').append(
                        $('<option>')
                            .attr('value', InfoInterlocuteur.idInterlocuteurAgence)
                            .html(InfoInterlocuteur.nomInterlocuteurAgence+' '+InfoInterlocuteur.prenomInterlocuteurAgence)
                            .attr('selected', (InfoInterlocuteur.idInterlocuteurAgence == data.info.FK_idInterlocuteurAgence) ? true : false)
                    );
                });

                ClassFicheCampagne.rechercheCampagnePrec(data.info.FK_idCampagnePrec)
            }
        });
    },

    rechercheMissionCampagne: function(){
        tableMission_4_4 = $('#tableMission_4_4').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.4.4-listeMissionCampagne.php?idCampagne="+idCampagne,
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(5)', nRow).children('button').bind('click', function(){
                    document.location.href='index.php?ap=4.0-Client&ss_m=4.5-FicheMission&idClient='+idClient+'&idCampagne='+idCampagne+'&s='+$(this).attr('data-id');
                })
            }
        });
        var tableWrapper = $('#tableMission_4_4_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    rechercheTypeMission: function(){


        $.ajax({
            async : false,
            type: "POST",
            data: {idCampagne: idCampagne},
            url: "includes/functions/bdd2web/func.4.4-rechercheTypeMission.php",
            dataType: 'json',
            success: function (data) {

                $.each(data, function (i, InfoType) {
                    $('#zsTypeMissionAdd').append(
                        $('<option>')
                            .attr('value', InfoType.idTypeMission)
                            .html(InfoType.libelleTypeMission)
                    );
                });
            }
        });
    },

    rechercheCampagnePrec: function(idOld){

        $.ajax({
            async : false,
            type: "POST",
            data: {idCampagne: idCampagne},
            url: "includes/functions/bdd2web/func.4.4-rechercheCampagnePrec.php",
            dataType: 'json',
            success: function (data) {
                $('#zsCampagnePrec').empty();
                $('#zsCampagnePrec').append('<option value=""></option>');
                $.each(data, function (i, info) {

                    var option = $('<option>')
                        .attr('value', info.idCampagne)
                        .html(info.libelleCampagne);
                    if (idOld == info.idCampagne) {
                        option.prop('selected', 'selected');
                    }
                    $('#zsCampagnePrec').append(
                        option
                    );
                });

                $('#zsCampagnePrec').select2({
                    placeholder:'Campagne',
                    allowClear:true
                });
            }
        });
    },

    rechercheChargeMission: function(){


        $.ajax({
            async : false,
            type: "POST",
            data: {idCampagne: idCampagne},
            url: "includes/functions/bdd2web/func.4.4-rechercheChargeMission.php",
            dataType: 'json',
            success: function (data) {
                $('#zsChargeMissionAdd').append('<option value=""></option>');
                $.each(data, function (i, InfoType) {
                    $('#zsChargeMissionAdd').append(
                        $('<option>')
                            .attr('value', InfoType.idUtilisateur)
                            .html(InfoType.nomUtilisateur)
                    );
                });
            }
        });
    }

}