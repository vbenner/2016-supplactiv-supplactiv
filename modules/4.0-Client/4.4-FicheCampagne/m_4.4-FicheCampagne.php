<input type="hidden" id="zt_idCampagne_5_4"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
<input type="hidden" id="zt_idClient_5_4"
       value="<?php print (filter_has_var(INPUT_GET, 'idClient')) ? filter_input(INPUT_GET, 'idClient') : '' ?>"/>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Informations sur la campagne
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_3">
                                <div class="number">0 Jour(s)</div>
                                <div class="desc">0 €</div>
                            </div>
                            <a class="more">ANIMATION</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat red">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_2">
                                <div class="number">0 Jour(s)</div>
                                <div class="desc">0 €</div>
                            </div>
                            <a class="more">FORMATION</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat grey">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_1">
                                <div class="number">0 Jour(s)</div>
                                <div class="desc">0 €</div>
                            </div>
                            <a class="more">MERCHANDISING</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fa fa-euro"></i>
                            </div>
                            <div class="details details_frais">
                                <div class="number">0 Jour(s)</div>
                                <div class="desc">0 €</div>
                            </div>
                            <a class="more">NB HEURE x TAUX HORAIRE</a>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">
                        <label class="control-label col-md-3">Libellé campagne :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztLibelleCampagne"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Date de fin :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztDateFinCampagne"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Campagne précédente :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <select class="form-control" id="zsCampagnePrec">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Interlocuteur client :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsInterlocuteurClient">

                                </select>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Interlocuteur Agence :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsInterlocuteurAgence">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Commentaire :</label>
                        <div class="col-md-9">
                            <div class="input-icon left">
                                <textarea class="form-control" id="ztCommentaireCampagne"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Tarif Animation € :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTarifA"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Tarif Formation (1F / 2F / 3F):</label>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTarifF"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTarifF2"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTarifF3"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Tarif Merchandising € :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztTarifM"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn blue bg-blue-su pull-right btn-valid-info-campagne"><i class="icon-check"></i> Mettre à jour les
                            informations
                        </button>
                    </div>
                </div>

                <div style="clear: both"></div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Ajouter une mission
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Libellé Mission :</label>
                            <div class="col-md-9">
                                <input id="ztLibelleMissionAdd" class="form-control form-recherche" type="text"
                                       placeholder="" name="start" style="text-align: left" value="" />
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Type Mission :</label>
                            <div class="col-md-3">
                                <select id="zsTypeMissionAdd"
                                        class="form-control form-recherche">
                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <label class="control-label col-md-3">Gamme Produit</label>
                            <div class="col-md-3 ">
                                <input id="ztGammeProduitAdd" class="form-control form-recherche" type="text"
                                       placeholder="" name="start" style="text-align: left" value="" />
                                <span class="help-block"> Facultatif </span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="control-label col-md-3">Description / Commentaire :</label>
                            <div class="col-md-9">
                                <textarea id="ztCommentaireAdd" class="form-control form-recherche" type="text"
                                       placeholder="" style="text-align: left" ></textarea>
                                <span class="help-block"> Facultatif </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Chargé Mission :</label>
                            <div class="col-md-3">
                                <div class="input-icon left">
                                    <select class="form-control" id="zsChargeMissionAdd">
                                    </select>
                                    <span class="help-block"> Obligatoire </span>
                                </div>
                            </div>
                            <label class="control-label col-md-3">Nb Interv. Prévisionnel :</label>
                            <div class="col-md-3">
                                <div class="input-icon left">
                                    <i class="fa fa-warning tooltips tooltips" data-container="body"
                                       data-original-title="Facultatif"></i>
                                    <input type="text" class="form-control" id="ztNbInterPrevisionnelAdd"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Frais de Gestion :</label>
                            <div class="col-md-3">
                                <div class="input-icon left">
                                    <i class="fa fa-warning tooltips tooltips" data-container="body"
                                       data-original-title="Facultatif"></i>
                                    <input type="text" class="form-control" id="ztFraisGestionAdd"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button class="btn btn-primary pull-right btn-valid-add-mission"><i class="fa fa-check"></i> Ajouter la mission

                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <h3>Historique des missions de la campagne</h3><hr>
    </div>
</div>
<table class="table table-striped table-bordered table-hover" id="tableMission_4_4" >
    <thead>
    <tr>
        <th>Libellé Mission</th>
        <th>Type</th>
        <th>Gamme</th>
        <th>Nb Intervention(s)</th>
        <th>Progression</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>