<input type="hidden" id="zt_idCampagne_4_5"
       value="<?php print (filter_has_var(INPUT_GET, 'idCampagne')) ? filter_input(INPUT_GET, 'idCampagne') : '' ?>"/>
<input type="hidden" id="zt_idClient_4_5"
       value="<?php print (filter_has_var(INPUT_GET, 'idClient')) ? filter_input(INPUT_GET, 'idClient') : '' ?>"/>
<input type="hidden" id="zt_idMission_4_5"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>

<style>
.datepicker.dropdown-menu{ z-index: 999888889 !important;}
.select2-drop-active {
        z-index: 999888889 !important;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Informations sur la mission #<strong><?php echo (filter_input(INPUT_GET, 's')); ?></strong>
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"> </a>
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_1">
                                <div class="number">0</div>
                            </div>
                            <a class="more">FINALIS&Eacute;</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat grey">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_2">
                                <div class="number">0</div>
                            </div>
                            <a class="more">SANS CONTRAT</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat orange">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_3">
                                <div class="number">0</div>
                            </div>
                            <a class="more">PARTIELLE</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="dashboard-stat red">
                            <div class="visual">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="details details_4">
                                <div class="number">0</div>
                            </div>
                            <a class="more">INITIALE</a>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal form-bordered form-row-stripped">

                    <div class="form-group">
                        <label class="control-label col-md-3">Libellé mission :</label>
                        <div class="col-md-9">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztLibelleMission"/>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Mission précédente :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <select class="form-control" id="zsMissionPrec">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Gamme Produit :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Facultatif"></i>
                                <input type="text" class="form-control" id="ztGammeProduit"/>
                            </div>
                        </div>

                        <label class="control-label col-md-3">Type Mission :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsTypeMission">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Commentaire :</label>
                        <div class="col-md-9">
                            <div class="input-icon left">
                                <textarea class="form-control" id="ztCommentaireMission"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Chargé Mission :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   ></i>
                                <select class="form-control" id="zsChargeMission">

                                </select>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Nb Interv. Prévisionnel :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Facultatif"></i>
                                <input type="text" class="form-control" id="ztNbInterPrevisionnel"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Frais de gestion :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Facultatif"></i>
                                <input type="text" class="form-control" id="ztFraisGestion"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-actions">
                        <button class="btn blue bg-blue-su pull-right btn-modif-info-mission"><i class="icon-check"></i> Mettre à jour les
                            informations
                        </button>
                    </div>
                </div>

                <div style="clear: both"></div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Formulaire d'ajout d'interventions
                </div>

            </div>
            <div class="portlet-body ">
                <div class="form-horizontal form-bordered form-row-stripped">

            <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2">Département :</label>
                        <div class="col-md-4">
                            <div class="input-icon left">

                                <select class="form-control" id="zsDepartementAdd">

                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <label class="control-label col-md-2">Point de vente :</label>
                        <div class="col-md-4">
                            <div class="input-icon left">

                                <select class="form-control" id="zsPdvAdd">

                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Fréquence :</label>
                        <div class="col-md-4">
                            <div class="input-icon left">

                                <select class="form-control" id="zsFrequenceAdd">

                                </select>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                        <label class="control-label col-md-2">Intervenant :</label>
                        <div class="col-md-4">
                            <div class="input-icon left">

                                <select class="form-control" id="zsIntervenantAdd">

                                </select>
                                <span class="help-block"> Facultatif </span>
                            </div>
                        </div>
                    </div>
            </div>
                    <div class="form-actions">
                        <button class="btn btn-primary pull-right btn-valid-add-intervention"> Ajouter
                        </button>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row" id="portlet-pdv">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    Importation d'un fichier point de vente
                </div>
            </div>
            <div class="portlet-body form">
                <form id="uploadForm-pdv" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importPdvMission.php" target="uploadFrame-pdv" method="post">
                    <div id="uploadInfos-pdv">
                        <div id="uploadStatus-pdv"></div>
                        <iframe id="uploadFrame-pdv" name="uploadFrame-pdv" style="width:100%;height:0px;display: none"></iframe>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Fichier Excel :</label>
                            <div class="col-md-10">
                                <fieldset class="label_side">
                                    <div>

                                        <input type="file" id="fichierPDV-pdv" name="fichierPDV-pdv" >

                                    </div>
                                </fieldset>
                                <span class="help-block">Fichier Excel uniquement. Le détail de ce fichier est disponible dans le fichier de documentation.</span>
                            </div>
                        </div>

                        <input type="hidden" name="idMission_4_5_pdv"
                               value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>


                    </div>
                    <div class="form-actions">
                        <button id="uploadSubmit-pdv" type="submit" class="btn bg bg-green pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row" id="portlet-import-inter">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Importation d'un planning d'intervention
                </div>
            </div>
            <div class="portlet-body form">
                <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importPlanningIntervention.php" target="uploadFrame" method="post">
                    <div id="uploadInfos">
                        <div id="uploadStatus"></div>
                        <iframe id="uploadFrame" name="uploadFrame" style="width:100%;height:0px;display: none"></iframe>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Fichier Excel :</label>
                            <div class="col-md-10">
                                <fieldset class="label_side">
                                    <div>

                                        <div id="fi" class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input span3"
                                                     data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                        class="fileinput-filename"></span>
                                                </div>
                                        <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">Choisir un fichier </span>
                                            <span class="fileinput-exists">Modifier </span>
                                            <input type="file" id="fichierPDV" name="fichierPDV">
                                        </span>
                                                <a href="#" class="input-group-addon btn red fileinput-exists"
                                                   data-dismiss="fileinput">Supprimer </a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <span class="help-block">Fichier Excel uniquement. Le détail de ce fichier est disponible dans le fichier de documentation.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">I1 :</label>
                            <div class="col-md-5">
                                <label class="control-label">Début :</label>
                                <input type="text" class="form-control zti" name="ztI1_D" value="08:00" />
                            </div>
                            <div class="col-md-5">
                                <label class="control-label">Fin :</label>
                                <input type="text" class="form-control zti" name="ztI1_F" value="10:00" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">I2 :</label>
                            <div class="col-md-5">
                                <label class="control-label">Début :</label>
                                <input type="text" class="form-control zti" name="ztI2_D" value="10:00" />
                            </div>
                            <div class="col-md-5">
                                <label class="control-label">Fin :</label>
                                <input type="text" class="form-control zti" name="ztI2_F" value="12:00" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">I3 :</label>
                            <div class="col-md-5">
                                <label class="control-label">Début :</label>
                                <input type="text" class="form-control zti" name="ztI3_D" value="13:00" />
                            </div>
                            <div class="col-md-5">
                                <label class="control-label">Fin :</label>
                                <input type="text" class="form-control zti" name="ztI3_F" value="15:00" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">I4 :</label>
                            <div class="col-md-5">
                                <label class="control-label">Début :</label>
                                <input type="text" class="form-control zti" name="ztI4_D" value="15:00" />
                            </div>
                            <div class="col-md-5">
                                <label class="control-label">Fin :</label>
                                <input type="text" class="form-control zti" name="ztI4_F" value="17:00" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Temps de PAUSE :</label>
                            <div class="col-md-5">
                                <label class="control-label">(format décimal) :</label>
                                <input type="text" class="form-control ztp" name="ztPAUSE" value="1.0" />
                            </div>
                        </div>

                        <input type="hidden" name="idMission_4_5"
                               value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
                        <input type="hidden" id="typeFichier" name="typeFichier" value=""/>


                    </div>
                    <div class="form-actions">
                        <button id="uploadSubmit" type="submit" class="btn btn-primary pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row display-hide" id="portlet-import-formation">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    Importation d'un planning de Formation
                </div>
            </div>
            <div class="portlet-body form">
                <form id="uploadForm" class="form-horizontal form-bordered" enctype="multipart/form-data" action="import/func.importPlanningFormation.php" target="uploadFrame" method="post">
                    <div id="uploadInfos">
                        <div id="uploadStatus"></div>
                        <iframe id="uploadFrame" name="uploadFrame" style="width:100%;height:0px;display: none"></iframe>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Fichier Excel :</label>
                            <div class="col-md-10">
                                <fieldset class="label_side">
                                    <div>

                                        <div id="fi" class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input span3"
                                                     data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                            class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">Choisir un fichier </span>
                                            <span class="fileinput-exists">Modifier </span>
                                            <input type="file" id="fichierPDV" name="fichierPDV">
                                        </span>
                                                <a href="#" class="input-group-addon btn red fileinput-exists"
                                                   data-dismiss="fileinput">Supprimer </a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <span class="help-block">Fichier Excel uniquement. Le détail de ce fichier est disponible dans le fichier de documentation.</span>
                            </div>
                        </div>

                        <input type="hidden" name="idMission_4_5"
                               value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>
                        <input type="hidden" id="typeFichier" name="typeFichier" value=""/>


                    </div>
                    <div class="form-actions">
                        <button id="uploadSubmitFormation" type="submit" class="btn btn-primary pull-right"><i class="fa fa-cogs"></i> Importer le Fichier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Historique des interventions de la mission</h3><hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="well">
            <button class="btn btn-primary btn-sel-all">Tout sélectionner</button>
            <button class="btn btn-primary btn-unsel-all">Tout désélectionner</button>

            <button class="btn btn-danger btn-supp-inter">Suppression Intervention</button>
            <!--<button class="btn btn-danger btn-supp-date">Suppression Date</button>-->

        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableIntervention_4_5" >
    <thead>
    <tr>
        <th>#</th>
        <th>Libellé PDV</th>
        <th>Code Postal</th>
        <th>Ville</th>
        <th>Intervenant</th>
        <th>Date</th>
        <th>Contrat</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div id="modalConfirmSuppDate" class="modal fade" tabindex="-1" style="z-index:100000001">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression des Dates</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">

                    <h3 id="titre-sup">Les dates des interventions que vous avez sélectionnées seront supprimées en une seule fois. Merci de cocher la case ci-dessous avant de valider.</h3>


                    <div class=" well">
                        <input type="checkbox" class="form-control" id="chk-del-e" value="delDate"/> Suppression de la date d'intervention
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red btn-valid-suppression-date">Valider la suppression</button>
            </div>
        </div>
    </div>
</div>

<div id="modalConfirmSuppInter" class="modal fade" tabindex="-1" style="z-index:100000001">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression des Interventions</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">

                    <h3 id="titre-sup">Les interventions que vous avez sélectionnées seront supprimées en une seule fois. Merci de cocher la case ci-dessous avant de valider.</h3>


                    <div class=" well">
                        <input type="checkbox" class="form-control" id="chk-del-f" value="delDate"/> Suppression des interventions
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red btn-valid-suppression-inter">Valider la suppression</button>
            </div>
        </div>
    </div>
</div>

<div id="modalConfirmSuppression" class="modal fade" tabindex="-1" style="z-index:100000001">
    <div class="modal-dialog"  style="width: 800px">
        <div class="modal-content"  style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'une intervention</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">

                        <h3 id="titre-sup">Veuillez choisir les informations à supprimer</h3>


                        <div class=" well">
                           <input type="checkbox" class="form-control" id="chk-del-a" value="delDate"/> Suppression de la date d'intervention (si planifiée)
                        </div>
                        <div class=" well">
                            <input type="checkbox" class="form-control" id="chk-del-b" value="delContrat"/> Suppression de l'affiliation contrat (si présente)
                        </div>
                        <div class=" well">
                            <input type="checkbox" class="form-control" id="chk-del-c" value="delAll"/> Suppression complète de l'intervention
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-valid-suppression">Valider la suppression</button>
            </div>
        </div>
    </div>
</div>
<div id="modalDetailIntervention" class="modal fade" tabindex="-1" style="z-index:100000000">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Détail d'une intervention</h4>
            </div>
            <div class="modal-body" style="padding: 0px !important;">
                <div class="form-horizontal form-bordered form-row-stripped">
                    <div class="row detail-sup">
                        <div class="col-md-12">

                        </div>
                    </div>
                    <div class="row detail-mod">
                        <div class="col-md-12">

                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Campagne :</label>
                            <div class="col-md-10">
                                <div class="input-icon left">
                                    <input type="text" class="form-control" readonly id="zrCampagne"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Mission :</label>
                            <div class="col-md-10">
                                <div class="input-icon left">
                                    <input type="text" class="form-control" readonly id="zrMission"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Point de vente :</label>
                            <div class="col-md-10">
                                <div class="input-icon left">
                                    <input type="text" class="form-control" readonly id="zrPdv"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Contrat :</label>
                            <div class="col-md-4">
                                <div class="input-icon left">
                                    <input type="text" class="form-control" readonly id="zrContrat"/>
                                </div>
                            </div>
                            <label class="control-label col-md-2">Intervenant :</label>
                            <div class="col-md-4">
                                <div class="input-icon left">
                                    <select class="form-control" id="zrIntervenant">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">

                            <div class="col-md-4">
                                <label class="control-label">Date :</label>
                                <div class="input-icon left">
                                    <input type="text" class="form-control" readonly id="zrDate" style="z-index:100000002"/>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Heure début :</label>
                                <div class="input-icon left">
                                    <input type="text" class="form-control timepicker timepicker-24"  id="zrHeureD" value="10:00"/>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Heure Fin :</label>
                                <div class="input-icon left">
                                    <input type="text" class="form-control timepicker timepicker-24"  id="zrHeureF" value="18:00"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn pull-left">Fermer</button>
                <button type="button" class="btn red btn-sup-intervention">Supprimer</button>
                <button type="button" class="btn green btn-modif-intervention">Modifier</button>
            </div>
        </div>
    </div>
</div>




<div id="modalDetailImport" class="modal fade" tabindex="-1" >
    <div class="modal-dialog"  style="width: 1000px">
        <div class="modal-content"  style="width: 1000px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Résultat de l'importation du fichier</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">
                    <h3 class="info-import"></h3>

                    <h5 class="tableImportPdv">Liste des points de ventes en erreur</h5>
                    <table class="table table-striped table-bordered table-hover" id="tableImportPdv" >
                        <thead>
                        <tr>
                            <th width="300">Intervenant / Point de vente</th>
                            <th width="130">Date</th>
                            <th>Informations</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <h5 class="tableImportInter">Liste des intervenants en erreur</h5>
                    <table class="table table-striped table-bordered table-hover" id="tableImportInter" >
                        <thead>
                        <tr>
                            <th width="300">Intervenant / Point de vente</th>
                            <th width="130">Date</th>
                            <th>Informations</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <h5 class="tableImportR">Liste des interventions à résoudre</h5>
                    <table class="table table-striped table-bordered table-hover" id="tableImportR" >
                        <thead>
                        <tr>
                            <th width="300">Intervenant / Point de vente</th>
                            <th width="130">Date</th>
                            <th>Informations</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <table class="table table-striped table-bordered table-hover" id="tableImport" >
                        <thead>
                            <tr>
                                <th width="300">Intervenant / Point de vente</th>
                                <th width="130">Date</th>
                                <th>Informations</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div id="modalDetailImportFormation" class="modal fade" tabindex="-1" >
    <div class="modal-dialog"  style="width: 1000px">
        <div class="modal-content"  style="width: 1000px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Résultat de l'importation du fichier</h4>
            </div>
            <div class="modal-body" >
                <div class="form-horizontal">
                    <h3 class="info-import"></h3>

                    <h5 class="tableImportPdv">Liste des Anomalies détectées</h5>
                    <table class="table table-striped table-bordered table-hover" id="tableImportAnomalie" >
                        <thead>
                        <tr>
                            <th width="100">Ligne</th>
                            <th width="100">Zone</th>
                            <th>Raison</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>