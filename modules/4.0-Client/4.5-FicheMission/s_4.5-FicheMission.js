var idCampagne, idClient, idMission, tableIntervention_4_5, idInterventionSel, idPdvSel, idIntervenantSel;
jQuery(document).ready(function() {
    idCampagne = $('#zt_idCampagne_4_5').val();
    idClient = $('#zt_idClient_4_5').val();
    idMission = $('#zt_idMission_4_5').val();
    ClassFicheMission.init();
    $('.zti').inputmask("99:99");
});

var ClassFicheMission;
ClassFicheMission = {
    init: function () {
        this.rechercheInfoMission();
        this.rechercheDepartement();
        this.rechercheIntervenant();
        this.initButton();
        for (var i = 1; i <= 100; i++) {
            $('#zsFrequenceAdd').append(
                $('<option>').attr('value', i).html(i)
            );
        }
        $('#zsFrequenceAdd').select2();


        $('#zrDate').datepicker({
            format: 'dd/mm/yyyy'
        }).on('changeDate', function(ev) {
            ClassFicheMission.changeDateIntervention($('#zrDate').val());
        });

        $('#zrHeureD').inputmask('99:99').on('change', function(){
            ClassFicheMission.changeDateIntervention($('#zrDate').val());
        });

        $('#zrHeureF').inputmask('99:99').on('change', function(){
            ClassFicheMission.changeDateIntervention($('#zrDate').val());
        });

        this.rechercheIntervention();

        $('#uploadSubmit').bind('click', function(){
            $('.info-import').html('<div class="alert alert-info">Veuillez-patienter, importation du fichier en cours...');
            $('#modalDetailImport').modal('show');
            $('#tableImport tbody').empty();
            $('#tableImportPdv tbody').empty();
            $('#tableImportInter tbody').empty();
            $('#tableImportR tbody').empty();
        });

        $('#uploadSubmitFormation').bind('click', function(){
            $('.info-import').html('<div class="alert alert-info">Veuillez-patienter, importation du fichier en cours...');
            $('#modalDetailImportFormation').modal('show');
            $('#tableImportAnomalie tbody').empty();
            $('#tableImport tbody').empty();
            $('#tableImportPdv tbody').empty();
            $('#tableImportInter tbody').empty();
            $('#tableImportR tbody').empty();
        });
    },


    resultImport: function(resultFichier, typeImport){
        $.ajax({
            async : false,
            type: "POST",
            data: {},
            url: "import/"+resultFichier,
            dataType: 'json',
            success: function (data) {
                if (typeImport != 'F') {
                    $('#tableImport tbody').empty();
                    $('#tableImportPdv tbody').empty();
                    $('#tableImportInter tbody').empty();
                    $('#tableImportR tbody').empty();
                    var nbInterventionDejaPresente = 0; var nbInterventionAjout = 0; var nbInterventionAResoudre = 0;
                    var nbInterventionDel = 0; var nbDateDejaPasse =0; var nbInterventionDejaPresenteAutreMission = 0;
                    var nbIntervenantIntrouvable = 0; var nbInterventionErreur = 0;
                    var idLigne = 0; var nbPdvIntrouvable = 0;
                    $.each(data, function (idIntervenant, InfoIntervenant) {

                        $.each(InfoIntervenant.pdv, function (idPdv, InfoPdv) {
                            $.each(InfoPdv.dates, function (i, InfoDate) {
                                result = ''; var boolErreur = false;
                                if(InfoDate.statut == 0){
                                    result = InfoDate.infos;
                                }else {

                                    if(InfoDate.statut != 11 && InfoDate.statut != 13 && InfoDate.statut != 14)
                                        $.each(InfoDate.infos, function (j, InfoIntervention) {
                                            if (InfoIntervention.boolIntervenant) boolErreur = true;
                                            couleurAlert = (InfoIntervention.boolIntervenant) ? 'danger' : 'warning';
                                            result += '<div class="alert alert-' + couleurAlert + '">ATTENTION - Autre intervention prévue le ' + InfoIntervention.dteDebut + ' de ' + InfoIntervention.hDebut + ' à ' + InfoIntervention.hFin + ', ' + InfoIntervention.libellePdv + ' par ' + InfoIntervention.nomIntervenant + ' ' + InfoIntervention.prenomIntervenant + ' pour la gamme <strong>' + InfoIntervention.libelleGamme + '</strong></div>';

                                        });
                                }
                                if(InfoDate.statut == 1 && (InfoDate.infos).length > 0) {

                                    if(!boolErreur) {
                                        nbInterventionAjout++;
                                    }
                                }else {
                                    if(InfoDate.statut == 0){
                                        nbInterventionDejaPresente++;
                                    }else if(InfoDate.statut == 3){
                                        nbInterventionDel++;
                                    }else if(InfoDate.statut == 10){
                                        $('#tableImportR tbody').append(
                                            $('<tr>')
                                                .append($('<td>').html(InfoIntervenant.libelle + "<br />" + InfoPdv.libelle))
                                                .append($('<td>').html(InfoDate.dateD ))
                                                .append($('<td id="td_' + idLigne + '">').html('<div class="alert alert-danger">ERREUR, l\'intervention est déjà présente dans un contrat</div>'))
                                                .append($('<td>').html('<a href="index.php?ap=5.0-Contrat&ss_m=5.4-FicheContrat&s='+InfoDate.idContrat+'" target="_blank" class="btn btn-primary"><i class="fa fa-search"></i> Afficher le contrat</a>'))
                                        );
                                        nbInterventionAResoudre++;
                                    } else if(InfoDate.statut == 12){
                                        nbDateDejaPasse++;
                                    }else if(InfoDate.statut == 11){
                                        $('#tableImport tbody').append(
                                            $('<tr>')
                                                .append($('<td>').html(InfoIntervenant.libelle + "<br />" + InfoPdv.libelle))
                                                .append($('<td>').html(InfoDate.dateD + '<br />' + InfoDate.dateF))
                                                .append($('<td id="td_' + idLigne + '">').html(InfoDate.infos))
                                                .append($('<td>').html(($('<button>').addClass('btn_' + idLigne + ' btn bg bg-green').attr('data-id', idLigne).html("Voir l'intervention").unbind().bind('click', function () {
                                                    ClassFicheMission.afficheDetailIntervention(InfoDate.idIntervention, false);
                                                }))))
                                        );
                                        nbInterventionDejaPresenteAutreMission++;
                                    }else if(InfoDate.statut == 13){
                                        nbPdvIntrouvable++;
                                        $('#tableImportPdv tbody').append(
                                            $('<tr>')
                                                .append($('<td>').html(InfoIntervenant.libelle + "<br />" + InfoPdv.libelle))
                                                .append($('<td>').html(InfoDate.dateD + '<br />' + InfoDate.dateF))
                                                .append($('<td id="td_' + idLigne + '">').html(InfoDate.infos))
                                                .append($('<td>').html(''))
                                        );
                                    } else if(InfoDate.statut == 14){
                                        nbIntervenantIntrouvable++;
                                        $('#tableImportInter tbody').append(
                                            $('<tr>')
                                                .append($('<td>').html(InfoIntervenant.libelle + "<br />" + InfoPdv.libelle))
                                                .append($('<td>').html(InfoDate.dateD + '<br />' + InfoDate.dateF))
                                                .append($('<td id="td_' + idLigne + '">').html(InfoDate.infos))
                                                .append($('<td>').html(''))
                                        );
                                    }else if(InfoDate.statut == 15) {
                                        nbInterventionErreur++;
                                        $('#tableImport tbody').append(
                                            $('<tr>')
                                                .append($('<td>').html(InfoIntervenant.libelle + "<br />" + InfoPdv.libelle))
                                                .append($('<td>').html(InfoDate.dateD + '<br />' + InfoDate.dateF))
                                                .append($('<td id="td_' + idLigne + '">').html(result))
                                                .append($('<td>').html(($('<button>').addClass('btn_' + idLigne + ' btn bg bg-green').attr('data-id', idLigne).html("Voir l'intervention").unbind().bind('click', function () {
                                                    ClassFicheMission.afficheDetailIntervention(InfoDate.idIntervention, false);
                                                }))))
                                        );

                                    }else {
                                        nbInterventionAjout++;
                                    }
                                }

                                idLigne++;
                            });
                        });

                    });
                    tableIntervention_4_5.api().ajax.reload();
                    $('.info-import').html('');
                    $('.info-import').append('<div class="alert alert-info"><strong>'+nbInterventionAjout+'</strong> intervention(s) ajoutée(s)  <br /><span style="font-size:14px">    - <strong>'+nbDateDejaPasse+'</strong> intervention(s) non traitée(s) car le mois est passé</span><br /><span style="font-size:14px">    - <strong>'+nbInterventionDejaPresente+'</strong> intervention(s) déjà présente(s) dans la mission</span></div>');
                    if(nbInterventionDel > 0) $('.info-import').append('<div class="alert alert-warning"><strong>'+nbInterventionDel+'</strong> intervention(s) supprimée(s)</div>');
                    if(nbInterventionDejaPresenteAutreMission > 0) $('.info-import').append('<div class="alert alert-danger"><strong>'+nbInterventionDejaPresenteAutreMission+'</strong> intervention(s) déjà présente(s) dans une autre mission</div>');
                    if(nbInterventionAResoudre > 0) $('.info-import').append('<div class="alert alert-warning"><strong>'+nbInterventionAResoudre+' intervention(s) en attente de décision</strong></div>');
                    if(nbPdvIntrouvable > 0) $('.info-import').append('<div class="alert alert-danger"><strong>'+nbPdvIntrouvable+'</strong> intervention(s) non traitée(s) car le point de vente est introuvable</div>');
                    if(nbIntervenantIntrouvable > 0) $('.info-import').append('<div class="alert alert-danger"><strong>'+nbIntervenantIntrouvable+'</strong> intervention(s) non traitée(s) car l\'intervenant est introuvable</div>');
                    $('#modalDetailImport').modal('show');

                    if(nbPdvIntrouvable > 0){
                        $('#tableImportPdv, .tableImportPdv').show();
                    }else $('#tableImportPdv, .tableImportPdv').hide();

                    if(nbIntervenantIntrouvable > 0){
                        $('#tableImportInter, .tableImportInter').show();
                    }else $('#tableImportInter, .tableImportInter').hide();

                    if(nbInterventionAResoudre > 0){
                        $('#tableImportR, .tableImportR').show();
                    }else $('#tableImportR, .tableImportR').hide();
                }
                else {

                    if (data != null) {
                        var nb=Object.keys(data).length;
                        $('#modalDetailImportFormation .info-import').html(
                            '<div class="alert alert-danger">Veuillez corriger les anomalies suivantes.'
                        );

                        if (nb !== undefined && nb>0) {
                            $.each(data, function (id, info) {

                                $('#tableImportAnomalie tbody').append(
                                    $('<tr>')
                                        .append($('<td>').html(id-1))
                                        .append($('<td>').html(info.ZONE))
                                        .append($('<td>').html(info.RAISON))
                                );

                            });
                        } else {
                            $('#modalDetailImportFormation .info-import').html(
                                '<div class="alert alert-success">Fichier correctement importé.'
                            );
                            $('#tableImportAnomalie').remove();
                            $('#modalDetailImportFormation .tableImportPdv').remove();
                        }
                    } else {
                        $('#modalDetailImportFormation .info-import').html(
                            '<div class="alert alert-success">Fichier correctement importé.'
                        );
                        $('#tableImportAnomalie').remove();
                        $('#modalDetailImportFormation .tableImportPdv').remove();
                    }
                }
            }
        });
    },

    resultImportPdv: function(nbPdv, nbIntervention){
        if(nbIntervention == 0){
            toastr.error(nbIntervention+' intervention(s) ajoutée(s), veuillez vérifier votre fichier', "ERREUR - Import d'un fichier point de vente");
        }else toastr.success(nbIntervention+' intervention(s) ajoutée(s), '+nbPdv+' PDV ajouté(s)', "Import d'un fichier point de vente");

        tableIntervention_4_5.api().ajax.reload();
    },

    initButton: function(){

        $('.btn-valid-add-intervention').bind('click', function(){
            var idPdv, idFrequence, idIntervenant = null;
            idPdv = $('#zsPdvAdd').val();
            idFrequence = $('#zsFrequenceAdd').val();
            idIntervenant = $('#zsIntervenantAdd').val();
            if(idPdv != null && idPdv != 'ALL'){
                $.ajax({
                    async : false,
                    type: "POST",
                    data: {idPdv: idPdv, idFrequence: idFrequence, idIntervenant: idIntervenant, idMission : idMission},
                    url: "includes/functions/web2bdd/func.4.5-ajoutIntervention.php",
                    dataType: 'json',
                    success: function (data) {
                        tableIntervention_4_5.api().ajax.reload();
                        ClassFicheMission.rechercheInfoMission();
                        toastr.success('Intervention(s) ajoutée(s)', 'Ajout d\'intervention');
                    }
                });
            }
        });

        $('.btn-modif-info-mission').bind('click', function(){
            var libelleMission = gammeProduit = typeMission = commentaireMission = null;
            libelleMission = $('#ztLibelleMission').val();
            gammeProduit = $('#ztGammeProduit').val();
            typeMission = $('#zsTypeMission').val();
            commentaireMission = $('#ztCommentaireMission').val();
            chargeMission = $('#zsChargeMission').val();
            fraisGestion = $('#ztFraisGestion').val();
            nbInterPrevisionnel = $('#ztNbInterPrevisionnel').val();
            missionPrec = $('#zsMissionPrec').val();

            if (chargeMission == '') {
                toastr.error('Le chargé de mission doit être renseigné.', ' Informations mission');
                return false;
            }
            if((libelleMission.trim()).length > 0){
                $.ajax({
                    async : false,
                    type: "POST",
                    data: {
                        idMission: idMission,
                        libelleMission: libelleMission,
                        gammeProduit: gammeProduit,
                        typeMission : typeMission,
                        chargeMission : chargeMission,
                        commentaireMission : commentaireMission,
                        fraisGestion : fraisGestion,
                        nbInterPrevisionnel : nbInterPrevisionnel,
                        missionPrec : missionPrec
                    },
                    url: "includes/functions/web2bdd/func.4.5-modificationInformationsMission.php",
                    dataType: 'json',
                    success: function (data) {
                        toastr.success('Les informations de la mission ont bien été modifiées', ' Informations mission');
                    }
                });
            }else {
                toastr.error("Veuillez saisir le libellé de la mission", "ERREUR - Informations mission");
            }
        });

        /** ---------------------------------------------------------------------------------------
         * Tout selectionner 
         */
        $('.btn-sel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", true);
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Tout deselectionner 
         */
        $('.btn-unsel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", false);
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Tout Supprimer les interventions
         */
        $('.btn-supp-date').bind('click', function () {

            /** On desactive le bouton */
            $('.btn-supp-date').hide();

            var listeInterventions = new Array();
            $(".chk").each(function(){
                if($(this).is(":checked")){
                    listeInterventions.push($(this).attr('id'));
                }
            });

            /** On export uniquement s'il y a des interventions sélectionnées */
            if(listeInterventions.length > 0){

                /** -------------------------------------------------------------------------------
                 * On confirme la suppression
                 */
                $('.btn-valid-suppression-date').unbind().bind('click', function () {
                    if ($('#chk-del-e').is(':checked')) {
                        $.ajax({
                            async : false,
                            url: "includes/functions/web2bdd/func.4.5-suppDateInter.php",
                            type: "POST",
                            data: {
                                listeInterventions_4_5 : listeInterventions
                            },
                            dataType: 'json'
                        }).done(function( data ) {
                            tableIntervention_4_5.api().ajax.reload();
                            $(".btn-supp-date").show();
                            $('#modalConfirmSuppDate').modal('hide');
                            toastr.success(data.totalOK+" date d'Interventions supprimées.", "Information");

                        });
                    } else {
                        toastr.info("Suppression date annulée", "Information");
                        $('#modalConfirmSuppDate').modal('hide');
                    }
                });

                $('#chk-del-e').parent().removeClass('checked');
                $('#modalConfirmSuppDate').modal('show');
                $(".btn-supp-date").show();

            }else {
                $(".btn-supp-date").show();
                toastr.error("Vous devez sélectionner des lignes pour utiliser cette fonctionnalité");
            }

        });

        /** ---------------------------------------------------------------------------------------
         * Tout Supprimer les interventions
         */
        $('.btn-supp-inter').bind('click', function () {
            /** On desactive le bouton */
            $('.btn-supp-inter').hide();

            var listeInterventions = new Array();
            $(".chk").each(function(){
                if($(this).is(":checked")){
                    listeInterventions.push($(this).attr('id'));
                }
            });

            /** On export uniquement s'il y a des interventions sélectionnées */
            if(listeInterventions.length > 0){

                /** -------------------------------------------------------------------------------
                 * On confirme la suppression
                 */
                $('.btn-valid-suppression-inter').unbind().bind('click', function () {
                    if ($('#chk-del-f').is(':checked')) {
                        $('#chk-del-f').prop(':checked', false);
                        $.ajax({
                            async : false,
                            url: "includes/functions/web2bdd/func.4.5-suppInterBatch.php",
                            type: "POST",
                            data: {
                                listeInterventions_4_5 : listeInterventions
                            },
                            dataType: 'json'
                        }).done(function( data ) {
                            tableIntervention_4_5.api().ajax.reload();
                            $(".btn-supp-inter").show();
                            $('#modalConfirmSuppInter').modal('hide');
                            toastr.success(data.totalOK+" interventions supprimées.", "Information");

                        });
                    } else {
                        toastr.info("Suppression intervention annulée", "Information");
                        $('#modalConfirmSuppInter').modal('hide');
                    }
                });

                $('#chk-del-f').parent().removeClass('checked');
                $('#modalConfirmSuppInter').modal('show');
                $(".btn-supp-inter").show();

            }else {
                $(".btn-supp-inter").show();
                toastr.error("Vous devez sélectionner des lignes pour utiliser cette fonctionnalité");
            }
        });

    },

    rechercheInfoMission: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idMission: idMission},
            url: "includes/functions/bdd2web/func.4.5-infoMission.php",
            dataType: 'json',
            success: function (data) {

                /** -------------------------------------------------------------------------------
                 * START VB
                 * Si Formation, on supprime l'importation du fichier PDV
                 */

                /**
                if (data.info.FK_idTypeMission == 2) {
                    $('#portlet-import-inter').addClass('display-hide');
                    $('#portlet-import-formation').removeClass('display-hide');
                    //$('#uploadForm').attr('action', 'import/func.importPlanningFormation.php');
                }
                 **/

                /** -------------------------------------------------------------------------------
                 * END VB
                 */

                $('#ztLibelleMission').val(data.info.libelleMission);
                $('#ztGammeProduit').val(data.info.libelleGamme);
                $('#ztCommentaireMission').val(data.info.descriptionMission);
                $('#ztFraisGestion').val(data.info.fraisGestion);
                $('#ztNbInterPrevisionnel').val(data.info.nbInterPrevisionnel);

                ClassFicheMission.rechercheMissionPrec(data.info.FK_idMissionPrec)
                $.each(data.statuts, function (i, InfoStatut) {
                    $('.details_' + i + ' .number').html(InfoStatut);
                });

                $('#zsTypeMission').empty();
                $.each(data.types, function (i, InfoType) {
                    $('#zsTypeMission').append(
                        $('<option>')
                            .attr('value', InfoType.idTypeMission)
                            .html(InfoType.libelleTypeMission)
                            .attr('selected', (InfoType.idTypeMission == data.info.FK_idTypeMission) ? true : false)
                    );
                });

                $('#zsChargeMission').empty();
                $('#zsChargeMission').append(
                    $('<option>')
                        .attr('value', '')
                        .html('')
                );
                $.each(data.users, function (i, item) {
                    $('#zsChargeMission').append(
                        $('<option>')
                            .attr('value', item.idUtilisateur)
                            .html(item.nomUtilisateur)
                            .attr('selected', (item.idUtilisateur == data.info.decideur_id) ? true : false)
                    );
                });
            }
        });
    },

    rechercheMissionPrec: function(idOld){

        $.ajax({
            async : false,
            type: "POST",
            data: {
                idCampagne: idCampagne,
            },
            url: "includes/functions/bdd2web/func.4.5-rechercheMissionPrec.php",
            dataType: 'json',
            success: function (data) {
                $('#zsMissionPrec').empty();
                $('#zsMissionPrec').append('<option value=""></option>');
                $.each(data, function (i, info) {

                    var option = $('<option>')
                        .attr('value', info.idMission)
                        .html(info.libelleMission);
                    if (idOld == info.idMission) {
                        option.prop('selected', 'selected');
                    }
                    $('#zsMissionPrec').append(
                        option
                    );
                });

                $('#zsMissionPrec').select2({
                    placeholder:'Mission',
                    allowClear:true
                });
            }
        });
    },

    rechercheDepartement: function () {
        $.ajax({
            async : false,
            type: "POST",
            data: {idMission: idMission},
            url: "includes/functions/bdd2web/func.rechercheDepartement.php",
            dataType: 'json',
            success: function (data) {

                $('#zsDepartementAdd').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un département')
                );

                $.each(data.departements, function (i, InfoDepartement) {
                    $('#zsDepartementAdd').append(
                        $('<option>')
                            .attr('value', InfoDepartement.idDepartement)
                            .html(InfoDepartement.idDepartement + ' - ' + InfoDepartement.libelleDepartement)
                    );
                });

                $('#zsDepartementAdd').select2().bind('change', function () {
                    ClassFicheMission.recherchePointDevente($(this).val());
                });
            }
        });
    },

    rechercheIntervenant: function () {
        $.ajax({
            async : false,
            type: "POST",
            data: {idMission: idMission},
            url: "includes/functions/bdd2web/func.rechercheIntervenant.php",
            dataType: 'json',
            success: function (data) {


                $('#zsIntervenantAdd').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un intervenant')
                );

                $.each(data.intervenants, function (i, InfoIntervenant) {
                    $('#zsIntervenantAdd').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant + ' - ' + InfoIntervenant.nomIntervenant + ' ' + InfoIntervenant.prenomIntervenant)
                    );
                });

                $('#zsIntervenantAdd').select2();
            }
        });
    },

    recherchePointDevente: function (idDepartement) {
        $.ajax({
            async : false,
            type: "POST",
            data: {idDepartement: idDepartement},
            url: "includes/functions/bdd2web/func.4.5-recherchePointDeVenteDepartement.php",
            dataType: 'json',
            success: function (data) {
                $("#zsPdvAdd").select2("destroy");
                $('#zsPdvAdd').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un point de vente')
                );

                $.each(data.pdvs, function (i, InfoPDV) {
                    $('#zsPdvAdd').append(
                        $('<option>')
                            .attr('value', InfoPDV.idPdv)
                            .html('[' + InfoPDV.codeMerval + '] ' + InfoPDV.libellePdv + ' - ' + InfoPDV.adressePdv_A + ' ' + InfoPDV.codePostalPdv + ' ' + InfoPDV.villePdv)
                    );
                });

                $('#zsPdvAdd').select2();
            }
        });
    },

    rechercheIntervention: function () {

        tableIntervention_4_5 = $('#tableIntervention_4_5').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 100,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.4.5-listeIntervention.php?idMission=" + idMission,
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0)', nRow).children('button').unbind().bind('click', function () {
                    ClassFicheMission.afficheDetailIntervention($(this).attr('data-id'), false);
                })
            },
            fnDrawCallback: function () {
                //$('.chk').uniform();
            }
        });
        var tableWrapper = $('#tableIntervention_4_5_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    afficheDetailIntervention: function (idIntervention, boolSup) {

        $('.detail-mod .col-md-12').empty();
        idInterventionSel = idIntervention;
        $.ajax({
            async : false,
            type: "POST",
            data: {idIntervention: idIntervention},
            url: "includes/functions/bdd2web/func.4.5-rechercheInformationIntervention.php",
            dataType: 'json',
            success: function (data) {

                idPdvSel = data.info.idPdv;
                idIntervenantSel = data.info.FK_idIntervenant;

                if(boolSup){
                    $('.detail-sup .col-md-12').html("<div class='alert alert-success'>Les demandes de suppression ont bien été enregistrées</div>")
                }else $('.detail-sup .col-md-12').html("");

                $('#zrIntervenant').removeAttr('disabled');
                $("#zrIntervenant").select2("destroy");

                $('#zrCampagne').val(data.info.libelleCampagne);
                $('#zrMission').val(data.info.libelleMission);
                $('#zrPdv').val(data.info.libellePdv + ' ' + data.info.codePostalPdv + ' - ' + data.info.villePdv);
                $('#zrContrat').val(data.info.FK_idContrat);

                $('#zrIntervenant').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un intervenant')
                );

                $.each(data.intervenants, function (i, InfoIntervenant) {
                    $('#zrIntervenant').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant + ' - ' + InfoIntervenant.nomIntervenant + ' ' + InfoIntervenant.prenomIntervenant)
                            .prop('selected', (InfoIntervenant.idIntervenant == data.info.FK_idIntervenant) ? true : false)
                    );
                });


                $('#zrDate').val(data.info.dateIntervention);
                $('#zrHeureD').val(data.info.heureDebut);
                $('#zrHeureF').val(data.info.heureFin);

                if(data.info.dateIntervention == null){
                    $('#zrHeureD').val('10:00');
                    $('#zrHeureF').val('18:00');
                }

                if (data.info.FK_idContrat != null && data.info.dateIntervention != null) {
                    //$('#zrDate').attr('disabled', 'true');
                    $('.timepicker-24').attr('disabled', 'true');

                    $('#zrIntervenant').attr('disabled', true);
                    $('.btn-modif-intervention').hide().unbind().bind('click', function () {
                        ClassFicheMission.modificationIntervention();
                    });
                } else {

                    $('#zrIntervenant').select2();
                    $('#zrDate').removeAttr('disabled');
                    $('.timepicker-24').removeAttr('disabled');

                    $('#zrIntervenant').unbind().bind('click', function(){
                        ClassFicheMission.changeDateIntervention($('#zrDate').val());
                    });

                    $('.btn-modif-intervention').show().unbind().bind('click', function () {
                        ClassFicheMission.modificationIntervention();
                    });
                }

                /** Bouton de suppression d'une intervention */
                $('.btn-sup-intervention').unbind().bind('click', function () {

                    /** Ouverture d'une modal permettant de demander les suppressions a faire */
                    $('#modalConfirmSuppression').modal('show');
                    $('#titre-sup').css('color', 'black');

                    /** Validation d'une suppression */
                    $('.btn-valid-suppression').unbind().bind('click', function () {

                        var boolContrat = boolDate = boolAll = false;
                        boolContrat = ($('#chk-del-b').prop('checked')) ? true : false;
                        boolDate = ($('#chk-del-a').prop('checked')) ? true : false;
                        boolAll = ($('#chk-del-c').prop('checked')) ? true : false;

                        /** On test s'il existe 1 des 3 options */
                        if(boolContrat || boolDate || boolAll){

                            $.ajax({
                                async : false,
                                type: "POST",
                                data: {
                                    idIntervention: idInterventionSel,
                                    boolContrat: boolContrat,
                                    boolDate: boolDate,
                                    boolAll : boolAll
                                },
                                url: "includes/functions/web2bdd/func.4.5-suppressionIntervention.php",
                                dataType: 'json',
                                success: function (data) {
                                    /** On ferme les modals */
                                    $('#modalConfirmSuppression').modal('hide');


                                    /** On rafraichit le tableau */
                                    tableIntervention_4_5.api().ajax.reload();
                                    ClassFicheMission.rechercheInfoMission();

                                    if (boolAll) {
                                        $('#modalDetailIntervention').modal('hide');
                                        toastr.success('L\'intervention <strong>' + idIntervention + '</strong> a bien été supprimé', "Suppression d'une intervention");
                                    } else {
                                        ClassFicheMission.afficheDetailIntervention(idIntervention, true);
                                    }
                                }
                            });
                        }

                        /** Sinon affichage d'un message d'erreur */
                        else {
                            $('#modalConfirmSuppression').effect('shake');
                            $('#titre-sup').css('color', 'red');
                        }
                    });
                });


                $('#modalDetailIntervention').modal('show');
            }
        });
    },

    changeDateIntervention: function(dateIntervention){

        $('.detail-mod .col-md-12').empty();

        var hD = $('#zrHeureD').val();
        var hF = $('#zrHeureF').val();
        hD = hD.replace(':', '');
        hF = hF.replace(':', '');

        if (Math.round(hF) > Math.round(hD)) {
            $.ajax({
                async : false,
                type: "POST",
                data: {
                    idIntervention: idInterventionSel,
                    dateIntervention: dateIntervention,
                    idPdv: idPdvSel,
                    idIntervenant: $('#zrIntervenant').val(),
                    heureDebut: $('#zrHeureD').val(),
                    heureFin: $('#zrHeureF').val()
                },
                url: "includes/functions/bdd2web/func.4.5-rechercheInformationsDateIntervention.php",
                dataType: 'json',
                success: function (data) {
                    var boolErreur = false;
                    $.each(data, function (i, InfoIntervention) {
                        if (InfoIntervention.boolIntervenant) boolErreur = true;
                        couleurAlert = (InfoIntervention.boolIntervenant) ? 'danger' : 'warning';
                        $('.detail-mod .col-md-12').append(
                            '<div class="alert alert-' + couleurAlert + '">ATTENTION - Autre intervention prévue le ' + InfoIntervention.info.dteDebut + ' de ' + InfoIntervention.info.hDebut + ' à ' + InfoIntervention.info.hFin + ', ' + InfoIntervention.info.libellePdv + ' par ' + InfoIntervention.info.nomIntervenant + ' ' + InfoIntervention.info.prenomIntervenant + ' pour la gamme <strong>' + InfoIntervention.info.libelleGamme + '</strong></div>'
                        )
                    });
                    if (boolErreur) {
                        $('.btn-modif-intervention').hide();
                    } else $('.btn-modif-intervention').show();
                }
            });
        } else {
            $('.detail-mod .col-md-12').append(
                '<div class="alert alert-danger">ERREUR - Les heures sont incohérentes...</div>'
            )
        }
    },

    modificationIntervention: function(){

        $.ajax({
            async : false,
            type: "POST",
            data: {
                idIntervention: idInterventionSel,
                idIntervenant: $('#zrIntervenant').val(),
                dateIntervention: $('#zrDate').val(),
                heureD : $('#zrHeureD').val(),
                heureF : $('#zrHeureF').val()
            },
            url: "includes/functions/web2bdd/func.4.5-modificationIntervention.php",
            dataType: 'json',
            success: function (data) {
                $('#modalDetailIntervention').modal('hide');
                tableIntervention_4_5.api().ajax.reload();
                toastr.success('L\'intervention a bien été modifié', 'Modification d\'intervention');
                ClassFicheMission.rechercheInfoMission();
            }
        });
    },

    forceIntervention: function(dateD, dateF, idIntervenant, idPdv, idLigne){
        $.ajax({
            async : false,
            type: "POST",
            data: {
                dateD: dateD,
                dateF: dateF,
                idIntervenant: idIntervenant,
                idPdv : idPdv,
                idMission : idMission
            },
            url: "includes/functions/web2bdd/func.4.5-ajoutInterventionFichier.php",
            dataType: 'json',
            success: function (data) {
                $('#td_'+idLigne).html('<div class="alert alert-success">Intervention ajoutée !</div>');
                $('.btn_'+idLigne).unbind().hide();
                tableIntervention_4_5.api().ajax.reload();
            }
        });
    }
};