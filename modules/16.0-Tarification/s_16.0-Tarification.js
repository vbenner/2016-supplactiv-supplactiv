var tableTarification_16_0, idCampagne, idIntervenant;
jQuery(document).ready(function() {
    ClassTarification.init();
});

var ClassTarification = {
    init: function(){
        this.rechercheSessionModule();
        this.initTableTarification();

        $('.btn-export-tarification').bind('click', function(){
            document.location.href = 'download/func.16.0-exportTarification.php';
        });
    },

    rechercheAffiliation: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.16.0-rechercheAffiliationTarification.php",
            dataType: 'json',
            success: function (data) {

                $('#zsIntervenant').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un intervenant')
                        .prop('selected', ('ALL' == idIntervenant) ? true : false)
                );

                $.each(data.intervenants, function (i, InfoIntervenant) {
                    $('#zsIntervenant').append(
                        $('<option>')
                            .attr('value', i)
                            .html(InfoIntervenant)
                            .prop('selected', (i == idIntervenant) ? true : false)
                    );
                });

                $('#zsIntervenant').select2();
                /**
                $('#zsCampagne').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir une campagne')
                        .prop('selected', ('ALL' == idCampagne) ? true : false)
                );

                $.each(data.campagnes, function (i, InfoCampagne) {
                    $('#zsCampagne').append(
                        $('<option>')
                            .attr('value', i)
                            .html(InfoCampagne)
                            .prop('selected', (i == idCampagne) ? true : false)
                    );
                });
                */
            }
        });
    },

    rechercheAffiliationCampagne : function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.16.0-rechercheAffiliationTarificationCampagne.php",
            dataType: 'json',
            success: function (data) {

                $('#zsCampagne').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir une campagne')
                        .prop('selected', ('ALL' == idCampagne) ? true : false)
                );

                $.each(data.campagnes, function (i, InfoCampagne) {
                    $('#zsCampagne').append(
                        $('<option>')
                            .attr('value', i)
                            .html(InfoCampagne)
                            .prop('selected', (i == idCampagne) ? true : false)
                    );
                });

                $('#zsCampagne').select2().bind('change', function () {
                    ClassTarification.enregistrementRecherche();
                });

                $('#zsIntervenant').select2().bind('change', function () {
                    ClassTarification.enregistrementRecherche();
                });

            }
        });
    },


    enregistrementRecherche : function(){
        idCampagne = $('#zsCampagne').val();
        idIntervenant = $('#zsIntervenant').val();

        $.ajax({
            async:false,
            type: "POST",
            data: {idCampagne_16_0 : idCampagne, idIntervenant_16_0 : idIntervenant},
            url: "includes/functions/web2session/func.majSession.php",
            dataType: 'json',
            success: function (data) {
                tableTarification_16_0.api().ajax.reload();
            }
        });

    },

    rechercheSessionModule: function(){
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: { idModule : '16_0', idCampagne_16_0 : 'ALL', idIntervenant_16_0 : 'ALL'},
            dataType: "json"
        }).done(function( data ) {


            idIntervenant = data.sessions.idIntervenant_16_0;
            idCampagne = data.sessions.idCampagne_16_0;

            ClassTarification.rechercheAffiliation();
            ClassTarification.rechercheAffiliationCampagne();


        });
    },

    initTableTarification: function(){
        tableTarification_16_0 = $('#tableTarification_16_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.16.0-listeTarification.php",
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0)', nRow).children('button').unbind().bind('click', function () {
                    ClassFicheMission.afficheDetailIntervention($(this).attr('data-id'), false);
                })
            }
        });
        var tableWrapper = $('#tableIntervention_4_5_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    }
}