<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                   Outils de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-2">Campagne :</label>
                            <div class="col-md-4">
                                <select id="zsCampagne" name="zsCampagne" class="form-control form-recherche">

                                </select>
                            </div>

                            <label class="control-label col-md-2">Intervenant :</label>
                            <div class="col-md-4">
                                <select id="zsIntervenant" name="zsIntervenant" class="form-control form-recherche">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn btn-primary pull-right btn-export-tarification"><i class="fa fa-file-excel-o"></i> Exporter au format XLSX</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableTarification_16_0" >
    <thead>
    <tr>
        <th>Intervenant</th>
        <th>Adresse</th>
        <th>Code Postal</th>
        <th>Ville</th>
        <th>Tel Mobile</th>
        <th>Tel Fixe</th>
        <th>E-mail</th>
        <th>Campagne</th>
        <th>Taux Horaire</th>
        <th>Forfait repas</th>
        <th>Forfait Tel</th>
        <th>Taux Kms</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>