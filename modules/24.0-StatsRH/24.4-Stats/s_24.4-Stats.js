/** -----------------------------------------------------------------------------------------------
 *
 */
var tableResult;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();
    },

    clearTable: function() {
        $('#tableResult').hide();
        $('#tableResult tbody').empty();
        if (tableResult != undefined) {
            $('#tableResult').dataTable().fnClearTable();
            $('#tableResult').dataTable().fnDestroy();
            $('#tableResult tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getEffectif.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var trA = '<tr><td>AUTO</td>';
                    var trN = '<tr><td>NON AUTO</td>';
                    var trC = '<tr><td>TOTAL</td>';
                    var trP = '<tr><td>Évolution (%) (année -1)</td>';

                    var result = data.DATA;
                    $.each(result, function (index, value) {
                        trA += '<td>' + value.COUNT_AUTO + '</td>';
                        trN += '<td>' + value.COUNT_PASAUTO + '</td>';
                        trC += '<td>' + value.COUNT + '</td>';
                        trP += '<td>' + value.PERCENT + '</td>';
                    });
                    $('#tableResult').append(trA);
                    $('#tableResult').append(trN);
                    $('#tableResult').append(trC);
                    $('#tableResult').append(trP);

                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            //{ "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            /*
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="25">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                            */
                        }

                    });
                    $('#tableResult').show();
                }
            });
        });
    }

}