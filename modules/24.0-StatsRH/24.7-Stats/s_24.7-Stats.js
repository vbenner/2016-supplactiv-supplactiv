/** -----------------------------------------------------------------------------------------------
 *
 */
var tableResult;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

        ClassStats.initIntervenante();
        /** ---------------------------------------------------------------------------------------
         * Pour les auto, on prend OUI / NON / AUTO
         */
        options = '<option value="ALL" selected>TOUT</option>';
        options += '<option value="OUI">OUI</option>';
        options += '<option value="NON">NON</option>';
        $('#lstAuto').html(options).select2();

        /** ---------------------------------------------------------------------------------------
         * Pour les sans intervention, on prend OUI / NON / AUTO
         */
        $('#lstSansInter').html(options).select2();

    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des intervenantes
     */
    initIntervenante: function () {
        $('#lstFiltreIntervenante').empty();
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {
            },
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstFiltreIntervenante').append(option);
                });
                $('#lstFiltreIntervenante').select2({
                        allowClear:true
                    }
                );
            }
        });
    },

    clearTable: function() {
        $('#tableResult').hide();
        $('#tableResult tbody').empty();
        if (tableResult != undefined) {
            $('#tableResult').dataTable().fnClearTable();
            $('#tableResult').dataTable().fnDestroy();
            $('#tableResult tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getInterventionsIntervenant.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                    auto : $('#lstAuto option:selected').val(),
                    inter : $('#lstFiltreIntervenante option:selected').val(),
                    sansinter : $('#lstSansInter option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    $('#tableResult').show();
                    var annee = $('#lstAnnee option:selected').val();
                    var result = data.DATA;
                    var keys = data.KEYS;
                    $.each(keys, function (index, value) {

                        var trC = '<tr><td>' + result[value].NOM + '</td><td>Nb (' + annee + ')</td>';
                        var trC2 = '<tr><td>' + result[value].NOM + '</td><td>Nb (' + (annee-1) + ')</td>';
                        var trP = '<tr><td>' + result[value].NOM + '</td><td style="border-bottom:3px solid #2eb9ce;">Évolution (%) (N-1)</td>';

                        /** -----------------------------------------------------------------------
                         * Animation
                         */
                        var trCA = '<tr><td>' + result[value].NOM + '</td><td>Nb A (' + annee + ')</td>';
                        var trC2A = '<tr><td>' + result[value].NOM + '</td><td>Nb A (' + (annee-1) + ')</td>';
                        var trPA = '<tr><td>' + result[value].NOM + '</td><td style="border-bottom:3px solid #2eb9ce;">Évolution A (%) (N-1)</td>';

                        /** -----------------------------------------------------------------------
                         * Formation
                         */
                        var trCF = '<tr><td>' + result[value].NOM + '</td><td>Nb F (' + annee + ')</td>';
                        var trC2F = '<tr><td>' + result[value].NOM + '</td><td>Nb F (' + (annee-1) + ')</td>';
                        var trPF = '<tr><td>' + result[value].NOM + '</td><td>Évolution F (%) (N-1)</td>';

                        /** -----------------------------------------------------------------------
                         * MOIS / MOIS
                         */
                        var i;
                        for (i = 1; i <= 13; i++) {

                            trC += '<td>' + result[value]['DETAIL'][annee][i]['COUNT'] + '</td>';
                            trC2 += '<td>' + result[value]['DETAIL'][annee-1][i]['COUNT'] + '</td>';
                            trP += '<td style="border-bottom:3px solid #2eb9ce;">' + result[value]['DETAIL'][annee][i]['PERCENT'] + '</td>';

                            trCA += '<td >' + result[value]['DETAIL'][annee][i]['COUNTA'] + '</td>';
                            trC2A += '<td>' + result[value]['DETAIL'][annee-1][i]['COUNTA'] + '</td>';
                            trPA += '<td style="border-bottom:3px solid #2eb9ce;">' + result[value]['DETAIL'][annee][i]['PERCENTA'] + '</td>';

                            trCF += '<td>' + result[value]['DETAIL'][annee][i]['COUNTF'] + '</td>';
                            trC2F += '<td>' + result[value]['DETAIL'][annee-1][i]['COUNTF'] + '</td>';
                            trPF += '<td>' + result[value]['DETAIL'][annee][i]['PERCENTF'] + '</td>';

                        }

                        trC += '</tr>';
                        trC2 += '</tr>';
                        trP += '</tr>';

                        trCA += '</tr>';
                        trC2A += '</tr>';
                        trPA += '</tr>';

                        trCF += '</tr>';
                        trC2F += '</tr>';
                        trPF += '</tr>';

                        //trA += '<td>' + value.COUNT_AUTO + '</td>';
                        //trN += '<td>' + value.COUNT_PASAUTO + '</td>';
                        //trC += '<td>' + value.COUNT + '</td>';
                        //trP += '<td>' + value.PERCENT + '</td>';

                        /** -----------------------------------------------------------------------
                         * Affichage des données
                         */
                        $('#tableResult').append(trC);
                        $('#tableResult').append(trC2);
                        $('#tableResult').append(trP);

                        $('#tableResult').append(trCA);
                        $('#tableResult').append(trC2A);
                        $('#tableResult').append(trPA);

                        $('#tableResult').append(trCF);
                        $('#tableResult').append(trC2F);
                        $('#tableResult').append(trPF);

                    });
                    var groupColumn = 0;
                    tableResult = $('#tableResult').dataTable({
                        lengthMenu : [[18, 36, 72, -1], [18, 36, 72, "All"]],
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group">' +
                                        '<th colspan="15" style="background-color: #8791a1">'+group+'</th>' +
                                        '</tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });
        });
    }
}