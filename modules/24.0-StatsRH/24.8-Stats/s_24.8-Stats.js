/** -----------------------------------------------------------------------------------------------
 *
 */
var tableRemuneration;
var tableRemunerationKM;
var tableRemunerationTEL;
var tableRemunerationREPAS;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

        /** ---------------------------------------------------------------------------------------
         * On récupère la liste des intervenantes
         */
        ClassStats.initListIntervenant();
    },

    initListIntervenant: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function(data) {
                $('#lstIntervenant').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstIntervenant').append(option);
                });
            }
        });
        $('#lstIntervenant').select2();
    },

    clearTable: function() {
        if (tableRemuneration != undefined) {
            $('#tableRemuneration').dataTable().fnClearTable();
            $('#tableRemuneration').dataTable().fnDestroy();
            $('#tableRemuneration tbody').empty();
        }

        if (tableRemunerationKM != undefined) {
            $('#tableRemunerationKM').dataTable().fnClearTable();
            $('#tableRemunerationKM').dataTable().fnDestroy();
            $('#tableRemunerationKM tbody').empty();
        }

        if (tableRemunerationTEL != undefined) {
            $('#tableRemunerationTEL').dataTable().fnClearTable();
            $('#tableRemunerationTEL').dataTable().fnDestroy();
            $('#tableRemunerationTEL tbody').empty();
        }

        if (tableRemunerationREPAS != undefined) {
            $('#tableRemunerationREPAS').dataTable().fnClearTable();
            $('#tableRemunerationREPAS').dataTable().fnDestroy();
            $('#tableRemunerationREPAS tbody').empty();
        }

        $('#tableRemuneration').hide();
        $('#tableRemunerationKM').hide();
        $('#tableRemunerationTEL').hide();
        $('#tableRemunerationREPAS').hide();

    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getRemunerations.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                    intervenant : $('#lstIntervenant option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * Liste des Taux de Rémunération
                     */
                    var keys = data.KEYS;
                    var taux = data.TAUX;
                    $.each(keys, function (index, value) {
                        var mois = taux[value].mois;
                        var tr = '<tr><td>Taux</td><td>' + value + '</td>';
                        var cumul = 0;
                        $.each(mois, function (indexmois, valuemois) {
                            cumul = (cumul * 1) + parseInt(valuemois);
                            tr += '<td>' + valuemois + '</td>';
                        });
                        tr += '<td>' + cumul + '</td></tr>';
                        $('#tableRemuneration tbody').append(tr);
                    });
                    tableRemuneration = $('#tableRemuneration').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * Liste des Taux Kilométriques
                     */
                    keys = data.KEYS_KM;
                    taux = data.TAUX_KM;
                    $.each(keys, function (index, value) {
                        var mois = taux[value].mois;
                        var tr = '<tr><td>Taux KM</td><td>' + value + '</td>';
                        var cumul = 0;
                        $.each(mois, function (indexmois, valuemois) {
                            cumul = (cumul * 1) + parseInt(valuemois);
                            tr += '<td>' + valuemois + '</td>';
                        });
                        tr += '<td>' + cumul + '</td></tr>';
                        $('#tableRemunerationKM tbody').append(tr);
                    });
                    tableRemunerationKM = $('#tableRemunerationKM').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * Liste des Frais Téléphoniques
                     */
                    keys = data.KEYS_TEL;
                    taux = data.TAUX_TEL;
                    $.each(keys, function (index, value) {
                        var mois = taux[value].mois;
                        var tr = '<tr><td>Frais TEL</td><td>' + value + '</td>';
                        var cumul = 0;
                        $.each(mois, function (indexmois, valuemois) {
                            cumul = (cumul * 1) + parseInt(valuemois);
                            tr += '<td>' + valuemois + '</td>';
                        });
                        tr += '<td>' + cumul + '</td></tr>';
                        $('#tableRemunerationTEL tbody').append(tr);
                    });
                    tableRemunerationTEL = $('#tableRemunerationTEL').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    /** ---------------------------------------------------------------------------
                     * Liste des Frais Repas
                     */
                    keys = data.KEYS_REPAS;
                    taux = data.TAUX_REPAS;
                    $.each(keys, function (index, value) {
                        var mois = taux[value].mois;
                        var tr = '<tr><td>Frais REPAS</td><td>' + value + '</td>';
                        var cumul = 0;
                        $.each(mois, function (indexmois, valuemois) {
                            cumul = (cumul * 1) + parseInt(valuemois);
                            tr += '<td>' + valuemois + '</td>';
                        });
                        tr += '<td>' + cumul + '</td></tr>';
                        $('#tableRemunerationREPAS tbody').append(tr);
                    });
                    tableRemunerationREPAS = $('#tableRemunerationREPAS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });


                    $('#tableRemuneration').show();
                    $('#tableRemunerationKM').show();
                    $('#tableRemunerationTEL').show();
                    $('#tableRemunerationREPAS').show();

                }
            });
        });
    }

}