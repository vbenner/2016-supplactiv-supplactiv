<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Nb Intervenants / Tx Rémunération
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Année :</label>
                            <select id="lstAnnee" name="lstAnnee" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Intervenant :</label>
                            <select id="lstIntervenant" name="lstIntervenant" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                        <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableRemuneration" style="display: none">
            <thead>
            <tr>
                <th></th>
                <th></th>
                <th>JAN</th>
                <th>FEV</th>
                <th>MARS</th>
                <th>AVR</th>
                <th>MAI</th>
                <th>JUIN</th>
                <th>JUIL</th>
                <th>AOUT</th>
                <th>SEP</th>
                <th>OCT</th>
                <th>NOV</th>
                <th>DEC</th>
                <th>Cumul</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableRemunerationKM" style="display: none">
            <thead>
            <tr>
                <th></th>
                <th></th>
                <th>JAN</th>
                <th>FEV</th>
                <th>MARS</th>
                <th>AVR</th>
                <th>MAI</th>
                <th>JUIN</th>
                <th>JUIL</th>
                <th>AOUT</th>
                <th>SEP</th>
                <th>OCT</th>
                <th>NOV</th>
                <th>DEC</th>
                <th>Cumul</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableRemunerationTEL" style="display: none">
            <thead>
            <tr>
                <th></th>
                <th></th>
                <th>JAN</th>
                <th>FEV</th>
                <th>MARS</th>
                <th>AVR</th>
                <th>MAI</th>
                <th>JUIN</th>
                <th>JUIL</th>
                <th>AOUT</th>
                <th>SEP</th>
                <th>OCT</th>
                <th>NOV</th>
                <th>DEC</th>
                <th>Cumul</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableRemunerationREPAS" style="display: none">
            <thead>
            <tr>
                <th></th>
                <th></th>
                <th>JAN</th>
                <th>FEV</th>
                <th>MARS</th>
                <th>AVR</th>
                <th>MAI</th>
                <th>JUIN</th>
                <th>JUIL</th>
                <th>AOUT</th>
                <th>SEP</th>
                <th>OCT</th>
                <th>NOV</th>
                <th>DEC</th>
                <th>Cumul</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


