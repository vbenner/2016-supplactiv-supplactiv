<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Nouveaux Entrants
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="alert alert-warning">
                    <strong>Attention</strong>
                    Seules les intervenantes ayant un contrat sont comptabilisées (qu'elles soient actives ou non).
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Année :</label>
                            <select id="lstAnnee" name="lstAnnee" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                        <button id="btnClear" type="button" class="btn yellow-casablanca pull-right"
                                style="margin-right: 5px;">Effacer
                        </button>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</div>


<table class="table table-striped table-bordered table-hover" id="tableEvolAge" style="display: none">
    <thead>
    <tr>
        <?php
        echo '<th></th>';
        for ($i = 1; $i <= 12; $i++) {
            echo '<th></th>';
        }
        echo '<th>Cumul</th>';
        ?>
    </tr>
    </thead>
    <tbody></tbody>
</table>
