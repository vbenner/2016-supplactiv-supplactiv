/** -----------------------------------------------------------------------------------------------
 *
 */
var tableEvolAge;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

    },

    clearTable: function() {
        $('#tableEvolAge').hide();
        $('#tableEvolAge tbody').empty();
        if (tableEvolAge != undefined) {
            $('#tableEvolAge').dataTable().fnClearTable();
            $('#tableEvolAge').dataTable().fnDestroy();
            $('#tableEvolAge tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $('#tableEvolAge').show();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getNouveauxEntrants.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val()
                },
                dataType: 'json',
                success: function (data) {

                    /** ---------------------------------------------------------------------------
                     * Affichage des Header
                     */
                    var year = data.YEAR;
                    table = 'tableEvolAge';
                    var i;
                    tr = '<tr><th></th>';
                    for (i = 1 ; i <= 12 ; i++) {
                        tr += '<th>' + pad(i, 2)+ '</th>';
                    }
                    tr += '<th>CUMUL</th></tr>';
                    $('#' + table + ' thead').html(tr);


                    /** ---------------------------------------------------------------------------
                     * Affichage des MOIS
                     */
                    var mois = data.MOIS[year];
                    var tr = '<tr><td>' + 'Nb (' + year + ')</td>';
                    $.each(mois, function (index, value) {
                        tr += '<td>' + value + '</td>';
                    });
                    tr += '</tr>';

                    mois = data.MOIS[year-1];
                    tr += '<tr><td>' + 'Nb (' + (year-1) + ')</td>';
                    $.each(mois, function (index, value) {
                        tr += '<td>' + value + '</td>';
                    });
                    tr += '</tr>';

                    var taux = data.TXY;
                    tr += '<tr><td>' + 'Évolution (%) (N -1)' + '</td>';
                    $.each(taux, function (index, value) {
                        tr += '<td>' + value + '</td>';
                    });
                    tr += '</tr>';

                    $('#tableEvolAge tbody').append(tr);





                    tableEvolAge = $('#tableEvolAge').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            //{ "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            /*
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="25">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                            */
                        }

                    });



                }
            });
        });
    }

}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}