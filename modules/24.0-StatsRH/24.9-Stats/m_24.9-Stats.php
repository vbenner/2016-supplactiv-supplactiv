<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Interventions / Chef de Projet
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="alert alert-warning">
                    <strong>Attention</strong> Chaque journée de formation compte pour 1.
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Année :</label>
                            <select id="lstAnnee" name="lstAnnee" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                        <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<table class="table table-striped table-bordered table-hover" id="tableResult" style="display: none">
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th>JAN</th>
        <th>FEV</th>
        <th>MARS</th>
        <th>AVR</th>
        <th>MAI</th>
        <th>JUIN</th>
        <th>JUIL</th>
        <th>AOUT</th>
        <th>SEP</th>
        <th>OCT</th>
        <th>NOV</th>
        <th>DEC</th>
        <th>CUMUL</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>