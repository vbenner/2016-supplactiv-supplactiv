/** -----------------------------------------------------------------------------------------------
 *
 */
var tableResult;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

    },

    clearTable: function() {

        $('#tableResult').hide();
        $('#tableResult tbody').empty();
        if (tableResult != undefined) {
            $('#tableResult').dataTable().fnClearTable();
            $('#tableResult').dataTable().fnDestroy();
            $('#tableResult tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getEncadrementCDP.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var annee = $('#lstAnnee option:selected').val();
                    var result = data.DATA;
                    var keys = data.KEYS;
                    $.each(keys, function (index, value) {

                        var trC = '<tr><td>' + result[value].NOM + '</td><td>Nb A+F</td>';
                        var trA = '<tr><td>' + result[value].NOM + '</td><td>Nb A</td>';
                        var trF = '<tr><td>' + result[value].NOM + '</td><td>Nb F</td>';
                        var trP = '<tr><td>' + result[value].NOM + '</td><td>Évolution (%) (N -1)</td>';
                        var trCI = '<tr><td>' + result[value].NOM + '</td><td>Nb Intervenant</td>';
                        var trPI = '<tr><td>' + result[value].NOM + '</td><td>Évolution (%) (N -1)</td>';

                        /** -----------------------------------------------------------------------
                         * MOIS / MOIS
                         */
                        var i;
                        for (i = 1; i <= 13; i++) {

                            //console.log(result[value]['DETAIL'][annee]);
                            //return false;
                            trC += '<td>' + result[value]['DETAIL'][annee][i]['COUNT'] + '</td>';
                            trA += '<td>' + result[value]['DETAIL'][annee][i]['COUNTA'] + '</td>';
                            trF += '<td>' + result[value]['DETAIL'][annee][i]['COUNTF'] + '</td>';
                            trP += '<td>' + result[value]['DETAIL'][annee][i]['PERCENT'] + '</td>';

                            trCI += '<td>' + result[value]['DETAIL'][annee][i]['COUNTI'] + '</td>';
                            trPI += '<td>' + result[value]['DETAIL'][annee][i]['PERCENTI'] + '</td>';

                        }
                        trC += '</tr>';
                        trA += '</tr>';
                        trF += '</tr>';
                        trP += '</tr>';
                        trCI += '</tr>';
                        trPI += '</tr>';
                        //trA += '<td>' + value.COUNT_AUTO + '</td>';
                        //trN += '<td>' + value.COUNT_PASAUTO + '</td>';
                        //trC += '<td>' + value.COUNT + '</td>';
                        //trP += '<td>' + value.PERCENT + '</td>';

                        $('#tableResult').append(trC);
                        $('#tableResult').append(trA);
                        $('#tableResult').append(trF);
                        $('#tableResult').append(trP);
                        $('#tableResult').append(trCI);
                        $('#tableResult').append(trPI);

                        $('#tableResult').show();

                    });
                    var groupColumn = 0;
                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        "lengthMenu": [[12, 24, 48, -1], [12, 24, 48, "All"]],
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<th colspan="15" style="background-color: #8791a1">'+group+'</th>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });

                }
            });
        });
    }
}