/** -----------------------------------------------------------------------------------------------
 *
 */
var tableResult;
var tableMission;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
    $('#lstAnnee').trigger('change');
    ClassStats.initListIntervenant();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

        /** ---------------------------------------------------------------------------------------
         * Pour les auto, on prend OUI / NON / AUTO
         */
        options = '<option value="ALL" selected>TOUT</option>';
        options += '<option value="OUI">OUI</option>';
        options += '<option value="NON">NON</option>';
        $('#lstAuto').html(options).select2();

    },

    initListIntervenant: function (idMission) {
        $('#lstFiltreIntervenante').empty();
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {
                idMission:idMission
            },
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstFiltreIntervenante').append(option);
                });
            }
        });
        $('#lstFiltreIntervenante').select2();
    },

    clearTable: function() {
        $('#tableResult tbody').empty();
        if (tableResult != undefined) {
            $('#tableResult').dataTable().fnClearTable();
            $('#tableResult').dataTable().fnDestroy();
            $('#tableResult tbody').empty();
            tableResult = undefined;
        }

        $('#tableMission tbody').empty();
        console.log('TABLE MISSION = ' + tableMission);
        if (tableMission != undefined) {
            $('#tableMission').dataTable().fnClearTable();
            $('#tableMission').dataTable().fnDestroy();
            $('#tableMission tbody').empty();
            tableMission = undefined;
        }
    },

    bindEvents: function () {

        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstFiltreMission').val(null).trigger('change');
            $('#lstFiltreIntervenante').val(null).trigger('change');
            ClassStats.clearTable();
        });

        $('#lstFiltreMission').unbind().bind('change', function () {
            if ($(this).val() == '') {
                ClassStats.initListIntervenant();
            }
            else {
                ClassStats.initListIntervenant($('#lstFiltreMission option:selected').val());
            }
        });

        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getJourneesIntervenant.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                    mission : $('#lstFiltreMission option:selected').val(),
                    inter : $('#lstFiltreIntervenante option:selected').val(),
                    auto : $('#lstAuto option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var annee = $('#lstAnnee option:selected').val();
                    var result = data.DATA;
                    var keys = data.KEYS;
                    $.each(keys, function (index, value) {

                        var trC = '<tr><td><strong>' + result[value].NOM + '</strong></td><td>Jours Interv. (' + annee + ')</td>';
                        var trCp = '<tr><td><strong>' + result[value].NOM + '</strong></td><td>Jours Interv. (' + (annee-1)+ ')</td>';
                        var trP = '<tr><td><strong>' + result[value].NOM + '</strong></td><td>Évolution (%) (année -1)</td>';

                        /** -----------------------------------------------------------------------
                         * MOIS / MOIS
                         */
                        var i;
                        for (i = 1; i <= 13; i++) {
                            trC += '<td>' + result[value]['DETAIL'][annee][i]['COUNT'] + '</td>';
                            trCp += '<td>' + result[value]['DETAIL'][(annee-1)][i]['COUNT'] + '</td>';
                            trP += '<td>' + result[value]['DETAIL'][annee][i]['PERCENT'] + '</td>';
                        }
                        trC += '</tr>';
                        trCp += '</tr>';

                        trP += '</tr>';
                        $('#tableResult').append(trC);
                        $('#tableResult').append(trCp);
                        $('#tableResult').append(trP);
                    });

                    var groupColumn = 0;
                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        "lengthMenu": [[12, 24, 48, -1], [12, 24, 48, "All"]],
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            { "visible": false, "targets": groupColumn }
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="15">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            } );
                        }

                    });


                    /** ---------------------------------------------------------------------------
                     * AFFICHAGE DES MISSIONS
                     */
                    $('#tableResult').show();

                    /** ---------------------------------------------------------------------------
                     * AFFICHAGE DES MISSIONS
                     */
                    $('#tableMission').hide();
                    if (data.MISSION.length != 0) {
                        $.each(data.MISSION, function (key, value) {
                            var tr0 = '<tr><td><strong>' + key + '</strong></td><td>' + (annee - 1) + '</td>';
                            var tr1 = '<tr><td><strong>' + key + '</strong></td><td>' + annee + '</td>';
                            $.each(value.A, function (key2, datas) {
                                $.each(datas, function (key3, lecount) {
                                    tr0 += '<td>' + lecount + '</td>';
                                });
                            });
                            $.each(value.B, function (key2, datas) {
                                $.each(datas, function (key3, lecount) {
                                    tr1 += '<td>' + lecount + '</td>';
                                });
                            });
                            /** -------------------------------------------------------------------
                             *
                             */
                            $('#tableMission tbody').append(tr1 + '</tr>');
                            $('#tableMission tbody').append(tr0 + '</tr>');
                        });
                        $('#tableMission').show();

                            tableMission = $('#tableMission').dataTable({
                            dom: 'Blfrtip',
                            "lengthMenu": [[12, 24, 48, -1], [12, 24, 48, "All"]],
                            buttons: [
                                {
                                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                    extend: 'copy',
                                },
                                {
                                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                    extend: 'csv',
                                },
                                {
                                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                    extend: 'excel',
                                },
                                {
                                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                    extend: 'pdf',
                                },
                                {
                                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                    extend: 'print',
                                }],
                            language: {
                                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                                buttons: {
                                    copyTitle: 'Ajouté au presse-papier',
                                    copySuccess: {
                                        _: '%d lignes copiées',
                                        1: '1 ligne copiée'
                                    }
                                }
                            },
                            "columnDefs": [
                                { "visible": false, "targets": groupColumn }
                            ],
                            //"order": [[ groupColumn, 'asc' ]],
                            "ordering": false,
                            "drawCallback": function ( settings ) {
                                var api = this.api();
                                var rows = api.rows( {page:'current'} ).nodes();
                                var last=null;

                                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                    if ( last !== group ) {
                                        $(rows).eq( i ).before(
                                            '<tr class="group"><td colspan="15">'+group+'</td></tr>'
                                        );

                                        last = group;
                                    }
                                } );
                            }

                        });

                    }
                }
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Changement de la date pour en déduire les missions
         */
        $('#lstAnnee').on('change', function () {
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/func.getListeMissions.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var result = data.items;
                    $('#lstFiltreMission').empty();
                    $('#lstFiltreMission').append('<option value=""></option>');
                    $.each(result, function (index, value) {
                        $('#lstFiltreMission').append(
                            '<option value="'+value.idMission+'">'+value.libelleMission+'</option>'
                        );
                    });
                    $('#lstFiltreMission').select2({
                        allowClear: true,
                    });
                }
            });
        });
    }
}