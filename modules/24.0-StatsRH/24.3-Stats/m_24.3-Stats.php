<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Effectif Equivalent Temps Plein
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">
                    <!--
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Année :</label>
                            <select id="lstAnnee" name="lstAnnee" class="form-control">
                            </select>
                        </div>
                    </div>
                    -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Intervenant :</label>
                            <select id="lstIntervenant" name="lstIntervenant" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Auto :</label>
                            <select id="lstAuto" name="lstAuto" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Tps :</label>
                            <input id="ztTps" name="ztTps" class="form-control" value="151.67">
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                        <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                    </div>
                </div>
                <br/>

            </div>
        </div>
    </div>
</div>

<?php
/** ---------------------------------------------------------------------------
 * On prend la liste des 4 derniers mois ?
 */
$mois = array('', 'JAN', 'FEV', 'MARS', 'AVR', 'MAI', 'JUIN',
    'JUIL', 'AOUT', 'SEP', 'OCT', 'NOV', 'DEC');
$year = date('Y');
$mois = (int) date('m');
$currMois = $mois;
for ($i = 0 ; $i <= 4 ; $i++){

    #$currMois = $currMois - $i;
    if ($currMois == 0) {
        $currMois = 12;
        $year--;
    }
    $ths = '<th>'.str_pad($currMois, 2, '0', STR_PAD_LEFT).''.
        '/'.$year.'</th>';

    $currMois--;
}
?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" id="tableEETP" style="display: none">
            <thead>
            <tr>
                <th></th>
                <?php echo $ths;?>
                <th>Cumul</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
