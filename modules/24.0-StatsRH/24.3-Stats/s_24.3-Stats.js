/** -----------------------------------------------------------------------------------------------
 *
 */
var tableEETP;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

        /** ---------------------------------------------------------------------------------------
         * Pour les auto, on prend OUI / NON / AUTO
         */
        options = '<option value="ALL" selected>TOUT</option>';
        options += '<option value="OUI">OUI</option>';
        options += '<option value="NON">NON</option>';
        $('#lstAuto').html(options).select2();

        /** ---------------------------------------------------------------------------------------
         * On récupère la liste des intervenantes
         */
        ClassStats.initListIntervenant();
    },

    initListIntervenant: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function(data) {
                $('#lstIntervenant').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstIntervenant').append(option);
                });
            }
        });
        $('#lstIntervenant').select2();
    },

    clearTable: function() {
        $('#tableEETP').hide();
        $('#tableEETP tbody').empty();
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getEETP.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                    intervenant : $('#lstIntervenant option:selected').val(),
                    limite : $('#ztTps').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var autos = data.AUTO;
                    $.each(autos, function (index, value) {
                        var tr = '<tr><td>' + value.nom + ' <span class="label label-danger"> AUTO </span></td>';
                        tr += '<td>' + value.detail[1].cumul + ' h</td>';
                        tr += '<td>' + value.detail[2].cumul + ' h</td>';
                        tr += '<td>' + value.detail[3].cumul + ' h</td>';
                        tr += '<td>' + value.detail[4].cumul + ' h</td>';
                        tr += '<td>' + value.detail[5].cumul + ' h</td>';
                        tr += '<td>' +
                            (value.detail[1].cumul + value.detail[2].cumul +
                             value.detail[3].cumul + value.detail[4].cumul +
                             value.detail[5].cumul) + ' h</td></tr>';
                        $('#tableEETP').append(tr);
                    });

                    var pasautos = data.PASAUTO;
                    $.each(pasautos, function (index, value) {
                        var tr = '<tr><td>' + value.nom + '</td>';
                        tr += '<td>' + value.detail[1].cumul + ' h</td>';
                        tr += '<td>' + value.detail[2].cumul + ' h</td>';
                        tr += '<td>' + value.detail[3].cumul + ' h</td>';
                        tr += '<td>' + value.detail[4].cumul + ' h</td>';
                        tr += '<td>' + value.detail[5].cumul + ' h</td>';
                        tr += '<td>' +
                            (value.detail[1].cumul + value.detail[2].cumul +
                                value.detail[3].cumul + value.detail[4].cumul +
                                value.detail[5].cumul) + ' h</td></tr>';
                        $('#tableEETP').append(tr);
                    });


                    /** ---------------------------------------------------------------------------
                     *
                     */
                    $('#tableEETP').show();
                }
            });
        });
    }

}