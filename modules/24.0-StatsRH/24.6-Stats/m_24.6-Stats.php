<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Missions / Intervenant
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Année :</label>
                            <select id="lstAnnee" name="lstAnnee" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Intervenant :</label>
                            <select id="lstFiltreIntervenante" name="lstFiltreIntervenant"
                                    class="form-control select2" >
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label">Auto :</label>
                            <select id="lstAuto" name="lstAuto" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 0px;">
                    <div class="form-actions right">
                        <button id="btnSearch" type="button" class="btn green pull-right">Filtrer</button>
                        <button id="btnClear" type="button" class="btn yellow-casablanca pull-right" style="margin-right: 5px;">Effacer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<table class="table table-striped table-bordered table-hover" id="tableResult" style="display: none">
    <thead>
    <tr>
        <th>NOM</th>
        <th>NOMBRE MISSIONS</th>
        <th>Évolution (%) (N-1)</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<table class="table table-striped table-bordered table-hover" id="tableResultM" style="display: none">
    <thead>
    <tr>
        <th>NOM</th>
        <th>MISSION</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>