/** -----------------------------------------------------------------------------------------------
 * tableResult = chiffres
 * tableResultM = missions
 */
var tableResult;
var tableResultM;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
    $('#lstAnnee').trigger('change');
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

        ClassStats.initIntervenante();

        /** ---------------------------------------------------------------------------------------
         * Pour les auto, on prend OUI / NON / AUTO
         */
        options = '<option value="ALL" selected>TOUT</option>';
        options += '<option value="OUI">OUI</option>';
        options += '<option value="NON">NON</option>';
        $('#lstAuto').html(options).select2();

    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des intervenantes
     */
    initIntervenante: function () {
        $('#lstFiltreIntervenante').empty();
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeIntervenants.php",
            type: "POST",
            data: {
            },
            dataType: 'json',
            success: function(data) {
                $('#lstFiltreIntervenante').append(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Tout Afficher')
                );
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idIntervenant)
                            .html(item.nomIntervenant+' '+item.prenomIntervenant)

                    ;
                    $('#lstFiltreIntervenante').append(option);
                });
                $('#lstFiltreIntervenante').select2({
                    allowClear:true
                    }
                );
            }
        });
    },


    clearTable: function() {

        if (tableResult != undefined) {
            $('#tableResult').dataTable().fnClearTable();
            $('#tableResult').dataTable().fnDestroy();
            $('#tableResult tbody').empty();
        }

        if (tableResultM != undefined) {
            $('#tableResultM').dataTable().fnClearTable();
            $('#tableResultM').dataTable().fnDestroy();
            $('#tableResultM tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            $('#lstFiltreMission').empty().select2().trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getMissionIntervenant.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                    intervenante : $('#lstFiltreIntervenante option:selected').val(),
                    auto : $('#lstAuto option:selected').val()
                },
                dataType: 'json',
                success: function (data) {

                    var result = data.DATA;
                    var keys = data.KEYS;
                    var tr = '';
                    $.each(keys, function (index, value) {
                        tr = '<tr><td>' + result[value].NOM + '</td>';
                        tr += '<td>' + result[value].COUNT + '</td>';
                        tr += '<td>' + result[value].PERCENT + '</td></tr>';
                        $('#tableResult').append(tr);
                    });
                    tableResult = $('#tableResult').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    var resultm = data.DATAM;
                    var tr = '';
                    $.each(resultm, function (index, value) {
                        tr = '<tr><td>' + value.NOM + '</td>';
                        tr += '<td>' + value.MISSION + '</td></tr>';
                        $('#tableResultM').append(tr);
                    });
                    tableResultM = $('#tableResultM').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                    });

                    tableResult.show();
                    tableResultM.show();
                }
            });
        });

        /** ---------------------------------------------------------------------------------------
         * Changement de la date pour en déduire les missions
         */
        $('#lstAnnee').on('change', function () {
            if ($('#lstFiltreMission option:selected').val() == '') {
                toastr.error('Il faut sélectionner la mission');
                return false;
            }
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/func.getListeMissions.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var result = data.items;
                    $('#lstFiltreMission').empty();
                    $('#lstFiltreMission').append(
                        '<option value="ALL"> - TOUTES - </option>'
                    );
                    $.each(result, function (index, value) {
                        $('#lstFiltreMission').append(
                            '<option value="'+value.idMission+'">'+value.libelleMission+'</option>'
                        );
                    });
                    $('#lstFiltreMission').select2();

                }
            });
        });
    }

}