/** -----------------------------------------------------------------------------------------------
 *
 */
var tableEvolAge;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
    ClassStats.bindEvents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.getAgeMoyen();
    },

    getAgeMoyen: function() {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/stats.getAgeMoyen.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {

                var totCount = parseInt(data.COUNT_HOMME) + parseInt(data.COUNT_FEMME);
                var counthomme = '-';
                var countfemme = '-';
                if (totCount != 0) {
                    counthomme = 100 * data.COUNT_HOMME / totCount;
                    countfemme = 100 * data.COUNT_FEMME / totCount;
                    counthomme = Number(counthomme.toFixed(2));
                    countfemme = Number(countfemme.toFixed(2));
                }

                $('.age-moyen').find('.number').html(data.AGE + ' ans');
                $('.age-moyen').find('.desc').html(data.COUNT + ' intervenant(e)s');
                $('.age-moyen-homme').find('.number').html(data.AGE_HOMME + ' ans');
                $('.age-moyen-homme').find('.desc').html(data.COUNT_HOMME + ' intervenants');
                $('.age-moyen-femme').find('.number').html(data.AGE_FEMME + ' ans');
                $('.age-moyen-femme').find('.desc').html(data.COUNT_FEMME + ' intervenantes');
                $('.percent').find('.number.homme').html('<i class="fa fa-male" style="font-size: 24px;"></i> ' + counthomme + '%');
                $('.percent').find('.number.femme').html('<i class="fa fa-female" style="font-size: 24px;"></i> ' + countfemme + '%');
            }
        });
    },

    initComponents : function() {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, on prend les 5 dernières
         */
        var annee = Number((new Date()).getFullYear());
        var options = ''
        for (i = 0 ; i <= 5 ; i++) {
            options += '<option value="' + (annee - i) + '">' + (annee - i) + '</option>';
        }
        $('#lstAnnee').html(options).select2();

    },

    clearTable: function() {
        $('#tableEvolAge tbody').empty();
        if (tableEvolAge != undefined) {
            $('#tableEvolAge').dataTable().fnClearTable();
            $('#tableEvolAge').dataTable().fnDestroy();
            $('#tableEvolAge tbody').empty();
        }
    },

    bindEvents: function () {
        $('#btnClear').unbind().bind('click', function () {
            var annee = Number((new Date()).getFullYear());
            $('#lstAnnee').val(annee).trigger('change');
            ClassStats.clearTable();
        });
        $('#btnSearch').unbind().bind('click', function () {

            ClassStats.clearTable();
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.getNouveauxEntrants.php",
                type: "POST",
                data: {
                    annee : $('#lstAnnee option:selected').val()
                },
                dataType: 'json',
                success: function (data) {
                    var tr = '<tr>';
                    var mois = data.MOIS;
                    $.each(mois, function (index, value) {
                        tr += '<td>' + value + '</td>';
                    });
                    tr += '</tr>';
                    $('#tableEvolAge tbody').append(tr);
                    tableEvolAge.dataTable();
                }
            });
        });
    }

}