<div class="row" style="margin-right: 15px;">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Statistiques RH
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body ">
                <div class="row">

                    <div class="col-md-3">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-calendar-o"></i>
                            </div>
                            <div class="details age-moyen">
                                <div class="number"></div>
                                <div class="desc"></div>
                            </div>
                            <a class="more"><strong>ÂGE MOYEN</strong></a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fa fa-male"></i>
                            </div>
                            <div class="details age-moyen-homme">
                                <div class="number"></div>
                                <div class="desc"></div>
                            </div>
                            <a class="more"><strong>ÂGE MOYEN / HOMME</strong></a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="dashboard-stat red-pink">
                            <div class="visual">
                                <i class="fa fa-female"></i>
                            </div>
                            <div class="details age-moyen-femme">
                                <div class="number"></div>
                                <div class="desc"></div>
                            </div>
                            <a class="more"><strong>ÂGE MOYEN / FEMME</strong></a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="dashboard-stat blue-steel">
                            <div class="visual">
                                <i class="fa icon-pie-chart"></i>
                            </div>
                            <div class="details percent">
                                <div class="number homme" style="padding-top: 10px !important;"></div>
                                <div class="number femme" style="padding-top: 0px !important;"></div>
                            </div>
                            <a class="more"><strong>Répartition H/F</strong></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



