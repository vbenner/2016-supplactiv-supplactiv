<input type="hidden" id="zt_idIntervenant_3_3"
       value="<?php print (filter_has_var(INPUT_GET, 's')) ? filter_input(INPUT_GET, 's') : '' ?>"/>

<div class="alert alert-info">
    <strong>ATTENTION</strong> - Pour la génération des DUE/DPAE, les informations suivantes sont nécessaires :
    <br/>- Date / Ville et département de Naissance
    <br/>- Numéro de sécurité sociale
</div>
<div class="alert alert-danger">
    <strong>ATTENTION</strong> - Pour une personne née à l'étranger, veuillez choisir le département de naissance 99 -
    ETRANGER (Contrainte URSSAF)
</div>


<div class=" form form-intervenant form-horizontal form-bordered form-row-stripped">
    <div class="row">
        <div class="col-md-8">
            <h2 class="caption-subject font-blue-sup bold uppercase center-block">INFORMATIONS PRINCIPALES</h2>
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Genre :</label>
                    <div class="input-icon left">
                        <i class="fa fa-warning tooltips tooltips" data-container="body"
                           data-original-title="Obligatoire"></i>
                        <select class="form-control" id="zsGenre" name="zsGenre">

                        </select>
                    </div>
                </div>
                <div class="col-md-3"
                    <?php
                    echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                    ?>
                >
                    <label class="control-label">Situation :</label>
                    <div class="input-icon left">
                        <i class="fa fa-warning tooltips tooltips" data-container="body"
                           data-original-title="Obligatoire"></i>
                        <select class="form-control" id="zsSituation" name="zsSituation">

                        </select>
                    </div>
                </div>
                <div class="col-md-3"
                    <?php
                    echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                    ?>
                >
                    <label class="control-label">Nombre d'enfant :</label>
                    <div>

                        <select class="form-control" id="zsNbEnfant" name="zsNbEnfant">
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Auto-entrepreneuse :</label>
                    <div>

                        <select class="form-control" id="zsAutoEntrepreneuse" name="zsAutoEntrepreneuse">

                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-4">
                    <label class="control-label">Nom actuel :</label>
                    <div class="input-icon left">
                        <i class="fa fa-warning tooltips tooltips" data-container="body"
                           data-original-title="Obligatoire"></i>
                        <input type="text" class="form-control" id="ztNom" name="ztNom"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="control-label">Nom de jeune fille :</label>
                    <div>
                        <input type="text" class="form-control" id="ztNomJF" name="ztNomJF"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="control-label">Prénom :</label>
                    <div class="input-icon left">
                        <i class="fa fa-warning tooltips tooltips" data-container="body"
                           data-original-title="Obligatoire"></i>
                        <input type="text" class="form-control" id="ztPrenom" name="ztPrenom"/>
                    </div>
                </div>
            </div>

            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-3">
                    <label class="control-label">Pays naissance :</label>
                    <div>
                        <select class="form-control" id="zsPaysNaissance" name="zsPaysNaissance">

                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Dep. naissance :</label>
                    <div>
                        <select class="form-control" id="zsDepartementNaissance" name="zsDepartementNaissance">

                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Ville naissance :</label>
                    <div class="input-icon left">
                        <i class="fa fa-warning tooltips tooltips" data-container="body"
                           data-original-title="Recommandé"></i>
                        <input type="text" class="form-control" id="ztVilleNaissance" name="ztVilleNaissance"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Nationalité :</label>
                    <div>
                        <select class="form-control" id="zsNationalite" name="zsNationalite">

                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-3">
                    <label class="control-label">Date Naissance :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateNaissance" name="ztDateNaissance"/>
                    </div>
                </div>
                <div class="col-md-3"
                    <?php
                    echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                    ?>
                >
                    <label class="control-label">Carte de séjour :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCarteSejour" name="ztCarteSejour"/>
                    </div>
                </div>
                <div class="col-md-3"
                    <?php
                    echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                    ?>
                >
                    <label class="control-label">Date expiration :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateExpiration" name="ztDateExpiration"/>
                    </div>
                </div>
                <div class="col-md-3"
                    <?php
                    echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                    ?>
                >
                    <label class="control-label">Num. Sécurité sociale :</label>
                    <div>
                        <input type="text" class="form-control" id="ztNumSecuSocial" name="ztNumSecuSocial"/>
                    </div>
                </div>
            </div>

            <!-- DATE DU CONTRAT CADRE -->
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Date Contrat Cadre :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateContratCadre" name="ztDateContratCadre"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <label class="control-label">Date Création Fiche :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateCreation" name="ztDateCreation"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <label class="control-label font-red">Date Modification Fiche :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateModification" name="ztDateModification"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <label class="control-label font-red">Modifié par :</label>
                    <div>
                        <input type="text" class="form-control" id="ztUserModification" name="ztUserModification"/>
                    </div>
                </div>

            </div>


            <!-- DATE URSSAF -->
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label">Date Attestation Urssaf :</label>
                    <div>
                        <input type="text" class="form-control" id="ztDateUrssaf" name="ztDateUrssaf"/>
                    </div>
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-3">
                    <label class="control-label">Niveau :</label>
                    <div>
                        <select class="form-control" id="zsNiveau" name="zsNiveau">

                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <label class="control-label">SIRET :</label>
                    <div>
                        <input type="text" class="form-control" id="ztNumSiret" name="ztNumSiret"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <a id="displayPic" class="iframe" href="#"><img class="lazy" id='picInter' height="80px;"></a>
                    <button type="button" class="form-control" id="btnUploadPic" name="btnUploadPic"
                            style="margin-top: 10px;">Gestion Photo
                    </button>
                </div>

                <div class="col-md-3">
                    <a id="displayCV" class="iframe" href="#"><img class="lazy" id='cvInter' height="80px;"></a>
                    <button type="button" class="form-control" id="btnUploadCV" name="btnUploadCV"
                            style="margin-top: 10px;">Gestion CV
                    </button>
                </div>


            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Commentaire :</label>
                    <div>
                        <textarea class="form-control" id="ztCommentaire" name="ztCommentaire"></textarea>
                    </div>
                </div>
            </div>

            <h2 class="caption-subject font-blue-sup bold uppercase center-block ">ADRESSES</h2>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Adresse :</label>
                    <div>
                        <input type="text" class="form-control" id="ztAdresseA" name="ztAdresseA"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Complément 1 :</label>
                    <div>
                        <input type="text" class="form-control" id="ztAdresseB" name="ztAdresseB"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Complément 2 :</label>
                    <div>
                        <input type="text" class="form-control" id="ztAdresseC" name="ztAdresseC"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label">Code Postal :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCodePostal" name="ztCodePostal"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Ville :</label>
                    <div>
                        <input type="text" class="form-control" id="ztVille" name="ztVille"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label">Latitude :</label>
                    <div>
                        <input type="text" class="form-control" id="ztLatitude" name="ztLatitude"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Longitude :</label>
                    <div>
                        <input type="text" class="form-control" id="ztLongitude" name="ztLongitude"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <h2 class="caption-subject font-blue-sup bold uppercase center-block ">CONTACT</h2>

            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label">Tel. fixe :</label>
                    <div>
                        <input type="text" class="form-control input-tel" id="ztTelFixe" name="ztTelFixe"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Tel. Mobile :</label>
                    <div>
                        <input type="text" class="form-control input-tel" id="ztTelMobile" name="ztTelMobile"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label">Autre tel. :</label>
                    <div>
                        <input type="text" class="form-control input-tel" id="ztAutreTel" name="ztAutreTel"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Fax :</label>
                    <div>
                        <input type="text" class="form-control input-tel" id="ztFax" name="ztFax"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">E-mail :</label>
                    <div>
                        <input type="text" class="form-control" id="ztEmail" name="ztEmail"/>
                    </div>
                </div>
            </div>

            <h2 class="caption-subject font-blue-sup bold uppercase center-block "
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >INFORMATIONS BANCAIRES</h2>

            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-12">
                    <label class="control-label">Numéro IBAN :</label>
                    <div>
                        <input type="text" class="form-control" id="ztIban" name="ztIban"/>
                    </div>
                </div>
            </div>

            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-12">
                    <label class="control-label">Code BIC :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCodeBic" name="ztCodeBic"/>
                    </div>
                </div>
            </div>



            <h2 class="caption-subject font-vert-sup bold uppercase center-block "
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >DOMICILIATION BANCAIRE</h2>

            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-12">
                    <label class="control-label">Adresse Banque :</label>
                    <div>
                        <textarea class="form-control" id="ztAdresseBanque" name="ztAdresseBanque" rows="3"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-6">
                    <label class="control-label">Code Banque :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCodeBanque" name="ztCodeBanque"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Code Guichet :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCodeGuichet" name="ztCodeGuichet"/>
                    </div>
                </div>
            </div>
            <div class="form-group"
                <?php
                echo $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME] > 3 ? ' style="display:none" ' : '';
                ?>
            >
                <div class="col-md-6">
                    <label class="control-label">Numéro Compte :</label>
                    <div>
                        <input type="text" class="form-control" id="ztNumCompte" name="ztNumCompte"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Clé :</label>
                    <div>
                        <input type="text" class="form-control" id="ztCle" name="ztCle"/>
                    </div>
                </div>
            </div>




            <h2 class="caption-subject font-blue-sup bold uppercase center-block ">INFORMATIONS COMPLEMENTAIRES</h2>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Compétences :</label>
                    <div class="">

                        <select class="form-control" id="zsCompetence" name="zsCompetence" multiple>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="control-label">Qualifications :</label>
                    <div class="">

                        <select class="form-control" id="zsQualification" name="zsQualification" multiple>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="control-label">Expérience :</label>
                    <div class="">

                        <select class="form-control" id="zsExperience" name="zsExperience" multiple>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="control-label">Départements d'interventions :</label>
                    <div class="">

                        <select class="form-control" id="zsDepartementIntervention" name="zsDepartementIntervention"
                                multiple>

                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="form-actions">
        <button class="btn bg bg-yellow-casablanca btn-desactivate-intervenant"><i class="fa fa-archive"></i> Activer /
            Désactiver l'intervenant
        </button>
        <button class="btn bg bg-red-sunglo btn-cloture-intervenant"><i class="fa fa-archive"></i> Clôturer /
            Déclôturer l'intervenant
        </button>
        <button class="btn bg bg-green pull-right btn-add-intervenant"><i class="fa fa-save"></i> Modifier l'intervenant
        </button>
    </div>

</div>

<h3 id="h3-histo"></h3>
<hr/>
<table class="table table-striped table-bordered table-hover" id="tableIntervention_3_3">
    <thead>
    <tr>
        <th>Libellé client</th>
        <th>Libellé mission</th>
        <th>Nb intervention</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<div id="modalUploadPhoto" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Gestion de la Photo</h4>
            </div>
            <div class="modal-body" style="padding-bottom: 5px;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">


                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 300px;">
                                    <img src="http://www.placehold.it/200x300/EFEFEF/AAAAAA&amp;text=pas+d'image"
                                         alt=""/></div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 300px;"></div>
                                <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Choisir </span>
                                            <span class="fileinput-exists"> Changer </span>
                                            <input type="file" name="">
                                        </span>
                                    <a href="javascript:;" class="btn default fileinput-exists"
                                       data-dismiss="fileinput"> Supprimer </a>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn yellow btn-erase-photo">Effacer</button>
                <button type="button" class="btn green btn-valid-photo">Valider</button>
            </div>
        </div>
    </div>
</div>

<div id="modalUploadCV" class="modal fade" tabindex="-2">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content" style="width: 800px">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h4 class="modal-title">Intégration Fichier</h4>
            </div>
            <div class="modal-body">


                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <form action="includes/functions/web2bdd/func.3.3-uploadCV.php" class="dropzone" id="fic-dropzone">

                </form>

            </div>
            <div class="modal-footer">
                <button class="btn yellow btn-erase-cv" type="button">Effacer</button>
                <button type="button" class="btn default" data-dismiss="modal">Fermer</button>
            </div>

        </div>
    </div>
</div



