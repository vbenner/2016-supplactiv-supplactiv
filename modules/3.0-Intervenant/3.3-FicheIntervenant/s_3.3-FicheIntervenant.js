var zt_idIntervenant_3_3, tableIntervention_3_3;
var myDropzone;
$(function() {
    zt_idIntervenant_3_3 = $('#zt_idIntervenant_3_3').val();
    ClassAjoutIntervenante.init();
});

var ClassAjoutIntervenante = {

    isValidEmailAddress: function (emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    },

    init: function(){
        $("img.lazy").lazyload();
        ClassAjoutIntervenante.loadPic();
        ClassAjoutIntervenante.loadCV();

        this.rechercheContenuSelect();
        $('#displayPic').fancybox();
        $('#displayCV').fancybox({
            fitToView:false,
            autoSize:false,
            'width':parseInt($(window).width() * 0.7),
            'height':parseInt($(window).height() * 0.8),
            'autoScale':true,
            'type': 'iframe'
        });

        $('.btn-add-intervenant').bind('click', function(){
            ClassAjoutIntervenante.ajoutIntervenant();
        });

        $('.btn-cloture-intervenant').bind('click', function(){
            ClassAjoutIntervenante.clotureIntervenant();
        });

        $('.btn-desactivate-intervenant').bind('click', function(){
            ClassAjoutIntervenante.activateIntervenant();
        });

        this.initTableIntervention();

        $('#btnUploadPic').bind('click', function() {

            $('.btn-erase-photo').unbind().bind('click', function () {
                $.ajax({
                    async : false,
                    url: "includes/functions/web2bdd/func.3.2-modifPhotoIntervenant.php",
                    type: "POST",
                    data: {
                        idIntervenant: zt_idIntervenant_3_3,
                        photo: ''
                    },
                    dataType: 'json',
                }).done(function( data ) {
                    $('#picInter').removeAttr('src');
                    $('#modalUploadPhoto').modal('hide');
                    toastr.error("Photo Supprimée", "INFORMATION");
                });
            });

            $('.btn-valid-photo').unbind().bind('click', function () {
                if ( $('.fileinput-exists').children('img')[0] == undefined) {
                    toastr.warning("Merci de choisir une photographie.", "Information");
                }

                //console.log($('.fileinput-exists').children('img').attr('src'));
                //return;

                $.ajax({
                    async : false,
                    url: "includes/functions/web2bdd/func.3.2-modifPhotoIntervenant.php",
                    type: "POST",
                    data: {
                        idIntervenant: zt_idIntervenant_3_3,
                        photo: $('.fileinput-exists').children('img').attr('src')
                    },
                    dataType: 'json',
                }).done(function( data ) {
                    d = new Date();
                    $('#picInter').attr("data-original", data.photo + '?' + d.getTime());
                    $('#picInter').prop("src", data.photo + '?' + d.getTime());
                    $('#displayPic').attr("href", data.photo);
                    $('#modalUploadPhoto').modal('hide');
                    toastr.success("Photo Ajoutée", "INFORMATION");
                });
            });
            $('#modalUploadPhoto').modal('show');

        });

        $('#btnUploadCV').unbind().bind('click', function() {

            $('.btn-erase-cv').unbind().bind('click', function () {
                $.ajax({
                    async : false,
                    url: "includes/functions/web2bdd/func.3.2-modifCVIntervenant.php",
                    type: "POST",
                    data: {
                        idIntervenant: zt_idIntervenant_3_3,
                        cv: ''
                    },
                    dataType: 'json',
                }).done(function( data ) {
                    $('#displayCV').attr("href", '#');
                    $('#cvInter').attr("src", 'assets/global/img/cvno.png');
                    $('#modalUploadCV').modal('hide');
                    toastr.success("CV Supprimé", "INFORMATION");
                });
            });

            $('#modalUploadCV').modal('show');
            if (! myDropzone) {
                myDropzone = new Dropzone("#fic-dropzone", {
                    paramName: 'cv',
                    maxFilesize: 5,
                    acceptedFiles: '.pdf',
                    init: function () {
                        this.on("complete", function (file) {
                            var fic = JSON.parse(file.xhr.response).res;
                            if (fic != '') {
                                $('#displayCV').attr("href", fic);
                                $('#cvInter').attr("src", 'assets/global/img/cv.png');
                                $('#modalUploadCV').modal('hide');
                                toastr.success("CV ajouté", "INFORMATION");
                            } else {
                                $('#displayCV').attr("href", '');
                                $('#cvInter').attr("src", 'assets/global/img/cvno.png');
                            }
                        });
                    },
                    params: {
                        idInter:  zt_idIntervenant_3_3
                    }
                });
            }
            /*
            $('#fic-dropzone').dropzone({
                paramName : 'livraison',
                maxFilesize : 5,
                acceptedFiles : '.pdf',
                init: function() {
                    this.on("complete", function(file) {
                    });
                }
            });
            */
        });

    },

    loadPic : function () {

        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.3.2-getPhotoIntervenant.php",
            type: "POST",
            data: {
                idIntervenant : zt_idIntervenant_3_3
            },
            dataType: 'json',
        }).done(function( data ) {

            if (data.photo != '') {
                d = new Date();
                $('#picInter').attr("data-original", data.photo + '?' + d.getTime());
                $('#displayPic').attr("href", data.photo);
            }
        });

    },

    loadCV : function () {

        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.3.2-getCVIntervenant.php",
            type: "POST",
            data: {
                idIntervenant : zt_idIntervenant_3_3
            },
            dataType: 'json',
        }).done(function( data ) {

            if (data.cv != '') {
                $('#displayCV').attr("href", data.cv);
                $('#cvInter').attr("src", 'assets/global/img/cv.png');
            } else {
                $('#displayCV').attr("href", '#');
                $('#cvInter').attr("src", 'assets/global/img/cvno.png');
            }
        });

    },

    ajoutIntervenant: function(){

        /** ---------------------------------------------------------------------------------------
         * 2019-10-25 - Test de la partie EMAIL
         */
        if ($('#ztEmail').val() != '') {
            if (! ClassAjoutIntervenante.isValidEmailAddress($('#ztEmail').val())) {
                alert('L\'email est invalide. Merci de le corriger.');
                return false;
            }
        }

        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.3.2-ajoutIntervenant.php",
            type: "POST",
            data: {
                idIntervenant : zt_idIntervenant_3_3,
                info : ClassAjoutIntervenante.serialize('.form-intervenant'),
                lstCompetence : $('#zsCompetence').val(),
                lstQualification : $('#zsQualification').val(),
                lstExperience : $('#zsExperience').val(),
                lstDept : $('#zsDepartementIntervention').val(),
                ztDateCreation : $('#ztDateCreation').val(),
                ztDateUrssaf : $('#ztDateUrssaf').val(),
            },
            dataType: 'json',
        }).done(function( data ) {
            if(data.result != 0){
                $('#ztDateModification').val(data.dteModification);
                $('#ztUserModification').val(data.userModification);
                toastr.success("L'intervenant a bien été modifié", "Modification d'un intervenant");
            }else {
                toastr.error("Veuillez vérifier le nom, prénom, date de naissance, ville de naissance et numéro de sécurité sociale !", "ERREUR - Modification d'un intervenant");
            }
        });
    },

    activateIntervenant: function(){

        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.3.2-modifIntervenant.php",
            type: "POST",
            data: {
                idIntervenant : zt_idIntervenant_3_3,
                boolActif : 1
            },
            dataType: 'json'
        }).done(function( data ) {
            if(data.result == 0){
                toastr.warning("L'intervenant est désactivé");
            }else {
                toastr.success("L'intervenant est activé");
            }
        });
    },

    clotureIntervenant: function(){

        $.ajax({
            async : false,
            url: "includes/functions/web2bdd/func.3.2-modifIntervenant.php",
            type: "POST",
            data: {
                idIntervenant : zt_idIntervenant_3_3,
                boolCLOS : 1
            },
            dataType: 'json'
        }).done(function( data ) {
            if(data.result == 0){
                toastr.success("L'intervenant est déclôturée");
            }else {
                toastr.error("L'intervenant est clôturée");
            }
        });
    },

    serialize: function(el) {
        var serialized = $(el).serialize();
        if (!serialized) // not a form
            serialized = $(el).
                find('input[name],select[name],textarea[name]').serialize();
        return serialized;
    },

    rechercheContenuSelect: function(){
        
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.3.2-rechercheInfoIntervenant.php",
            type: "POST",
            data: { idIntervenant : zt_idIntervenant_3_3 },
            dataType: 'json',
        }).done(function( data ) {

            $('#h3-histo').html('Historique des interventions <span class="badge bg bg-green"> Total heure (4 derniers mois): '+data.dureeTotal+'</span> -- <span class="badge bg bg-green">Heures cumulées du dernier contrat : '+data.nbHeureCumule+'</span>');
            $('.page-title').html("Fiche de l'intervenant(e) <small>"+data.intervenant.idIntervenant+" - "+data.intervenant.nomIntervenant+" "+data.intervenant.prenomIntervenant+"</small>");
            $('#ztNom').val(data.intervenant.nomIntervenant);
            $('#ztNomJF').val(data.intervenant.nomIntervenant_JF);
            $('#ztPrenom').val(data.intervenant.prenomIntervenant);
            $('#ztVilleNaissance').val(data.intervenant.villeNaissance);

            $('#ztDateNaissance').val(data.intervenant.dteNaissance);
            $('#ztCarteSejour').val(data.intervenant.numeroCarteSejour);
            if(data.intervenant.dteFinSejour != '00/00/0000') {
                $('#ztDateExpiration').val(data.intervenant.dteFinSejour);
            }
            $('#ztDateContratCadre').val(data.intervenant.dteContratCadre);

            $('#ztDateCreation').val(data.intervenant.dteCreation);
            if (data.intervenant.dteCreation != '' && data.intervenant.dteCreation != null) {
                $('#ztDateCreation').prop('disabled', true);
            }

            $('#ztDateModification').val(data.intervenant.dteModification);
            $('#ztDateModification').prop('disabled', true);

            $('#ztUserModification').val(data.intervenant.userModification);
            $('#ztUserModification').prop('disabled', true);

            /** -----------------------------------------------------------------------------------
             * Nouveauté DATE ATTESTATION URSSAF
             */
            $('#ztDateUrssaf').val(data.intervenant.dteUrssaf);
            //if (data.intervenant.dteUrssaf != '' && data.intervenant.dteUrssaf != null) {
            //    $('#ztDateUrssaf').prop('disabled', true);
            //}

            if (data.intervenant.boolAutoEntrepreneuse == 'OUI') {
                $('#ztDateUrssaf').closest('.form-group').show();
            }
            else {
                $('#ztDateUrssaf').closest('.form-group').hide();
            }

            $('#ztNumSecuSocial').val(data.intervenant.numeroSS);
            
            $('#ztCommentaire').val(data.intervenant.commentaireIntervenant);

            $('#ztAdresseA').val(data.intervenant.adresseIntervenant_A);
            $('#ztAdresseB').val(data.intervenant.adresseIntervenant_B);
            $('#ztAdresseC').val(data.intervenant.adresseIntervenant_C);
            $('#ztCodePostal').val(data.intervenant.codePostalIntervenant);
            $('#ztVille').val(data.intervenant.villeIntervenant);
            $('#ztLatitude').val(data.intervenant.latitude);
            $('#ztLongitude').val(data.intervenant.longitude);

            $('#ztTelFixe').val(data.intervenant.telephoneIntervenant_A);
            $('#ztTelMobile').val(data.intervenant.mobileIntervenant);
            $('#ztAutreTel').val(data.intervenant.telephoneIntervenant_B);
            $('#ztFax').val(data.intervenant.faxIntervenant);
            $('#ztEmail').val(data.intervenant.mailIntervenant);

            $('#ztIban').val(data.intervenant.numeroIBAN);
            $('#ztCodeBic').val(data.intervenant.codeBIC);
            $('#ztNumSiret').val(data.intervenant.numSIRET);

            $('#ztAdresseBanque').val(data.intervenant.libelleBanque);
            $('#ztCodeBanque').val(data.intervenant.codeBanque);
            $('#ztCodeGuichet').val(data.intervenant.codeGuichet);
            $('#ztNumCompte').val(data.intervenant.numeroCompte);
            $('#ztCle').val(data.intervenant.cleSecurite);

            $.each( data.genres, function( key, info ) {
                $("#zsGenre").append(
                    $('<option>')
                        .attr('value', info.idGenre)
                        .html(info.libelleGenre)
                        .attr('selected', (info.idGenre == data.intervenant.genre) ? true : false)
                );
            });

            $("#zsNiveau").append($('<option>').attr('value', ''));
            $.each( data.niveaux, function( key, info ) {
                $("#zsNiveau").append(
                    $('<option>')
                        .attr('value', info.idNiveau)
                        .html(info.libelleNiveau)
                        .attr('selected', (info.idNiveau == data.intervenant.FK_idNiveau) ? true : false)
                );
            });

            $.each( data.situations, function( key, info ) {
                $("#zsSituation").append(
                    $('<option>')
                        .attr('value', info.idSituation)
                        .html(info.libelleSituation)
                        .attr('selected', (info.idSituation == data.intervenant.situationFamiliale) ? true : false)
                );
            });

            for(var i = 0; i <= 20; i++){
                $("#zsNbEnfant").append(
                    $('<option>')
                        .attr('value', i)
                        .html(i)
                        .attr('selected', (i == data.intervenant.nombreEnfant) ? true : false)
                );
            }

            $('#zsAutoEntrepreneuse').append($('<option>').attr('value', 'NON').html('NON').prop('selected', (data.intervenant.boolAutoEntrepreneuse == 'NON') ? true : false));
            $('#zsAutoEntrepreneuse').append($('<option>').attr('value', 'OUI').html('OUI').prop('selected', (data.intervenant.boolAutoEntrepreneuse == 'OUI') ? true : false));


            $.each( data.pays, function( key, info ) {
                $("#zsPaysNaissance").append(
                    $('<option>')
                        .attr('value', info.codeIso)
                        .html(info.codeIso+' - '+info.libellePays)
                        .attr('selected', (info.codeIso == data.intervenant.paysNaissance) ? true : false)
                );
            });
            $("#zsPaysNaissance").select2();
            $("#zsNiveau").select2({allowClear:true});

            var lstDepartement = new Array();
            if((data.intervenant.lstDepartement) != null)
                lstDepartement = (data.intervenant.lstDepartement).split(',');

            $.each( data.departements, function( key, info ) {
                $("#zsDepartementNaissance").append(
                    $('<option>')
                        .attr('value', info.idDepartement)
                        .html(info.idDepartement+' - '+info.libelleDepartement)
                        .prop('selected', (info.idDepartement == data.intervenant.depNaissanceDUE) ? true : false)
                );

                var selected = false;
                for (index = 0; index < lstDepartement.length; ++index) {
                    selected = (lstDepartement[index] == info.idDepartement) ? true : selected;
                }

                $("#zsDepartementIntervention").append(
                    $('<option>')
                        .attr('value', info.idDepartement)
                        .html(info.idDepartement+' - '+info.libelleDepartement)
                        .prop('selected', selected)
                );


            });
            $("#zsDepartementNaissance").select2();
            $("#zsDepartementIntervention").select2();

            var lstCompetence = lstQualification = lstExperience = new Array();
            if((data.intervenant.lstCompetence) != null)
                lstCompetence = (data.intervenant.lstCompetence).split(',');

            if((data.intervenant.lstQualification) != null)
                lstQualification = (data.intervenant.lstQualification).split(',');

            if((data.intervenant.lstExperience) != null)
                lstExperience = (data.intervenant.lstExperience).split(',');

            $.each( data.competences, function( key, info ) {

                var selected = false;
                for (index = 0; index < lstCompetence.length; ++index) {
                    selected = (lstCompetence[index] == info.idCompetence) ? true : selected;
                }

                $("#zsCompetence").append(
                    $('<option>')
                        .attr('value', info.idCompetence)
                        .html(info.libelleCompetence)
                        .prop('selected', selected)
                );
            });
            $("#zsCompetence").select2();

            $.each( data.qualifications, function( key, info ) {

                var selected = false;
                for (index = 0; index < lstQualification.length; ++index) {
                    selected = (lstQualification[index] == info.idQualification) ? true : selected;
                }

                $("#zsQualification").append(
                    $('<option>')
                        .attr('value', info.idQualification)
                        .html(info.libelleQualification)
                        .prop('selected', selected)
                );
            });
            $("#zsQualification").select2();

            $.each( data.experiences, function( key, info ) {

                var selected = false;
                for (index = 0; index < lstExperience.length; ++index) {
                    selected = (lstExperience[index] == info.idExperience) ? true : selected;
                }

                $("#zsExperience").append(
                    $('<option>')
                        .attr('value', info.idExperience)
                        .html(info.libelleExperience)
                        .prop('selected', selected)
                );
            });
            $("#zsExperience").select2();

            $('#zsNationalite').append($('<option>').attr('value', 'F').html('Française').prop('selected', (data.intervenant.nationalite == 'F') ? true : false));
            $('#zsNationalite').append($('<option>').attr('value', 'E').html('Etrangère').prop('selected', (data.intervenant.nationalite == 'E') ? true : false));

            $("#ztDateNaissance").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateExpiration").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateContratCadre").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateCreation").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateUrssaf").inputmask("d/m/y", {
                "placeholder": "*"
            });

            $('#ztCodePostal').inputmask("99999");
            $('#ztNumSecuSocial').inputmask("9 99 99 9* 999 999 99");
            $('.input-tel').inputmask("99 99 99 99 99");
            $('#ztEmail').inputmask('Regex', { regex: "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}" });

        });
    },

    initTableIntervention: function(){
        tableIntervention_3_3 = $('#tableIntervention_3_3').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.3.3-listeInterventionIntervenant.php?idIntervenant="+zt_idIntervenant_3_3,
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableIntervention_3_3_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    }
}