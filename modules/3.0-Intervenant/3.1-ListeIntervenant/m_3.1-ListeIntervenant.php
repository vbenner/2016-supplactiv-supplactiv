<?php
$sqlRechercheCompetence = '
SELECT *
FROM su_competence
ORDER BY libelleCompetence';
$RechercheCompetenceExc = DbConnexion::getInstance()->prepare($sqlRechercheCompetence);

$sqlRechercheQualification = '
SELECT *
FROM su_qualification
ORDER BY libelleQualification';
$RechercheQualificationExc = DbConnexion::getInstance()->prepare($sqlRechercheQualification);

$sqlRechercheExperience = '
SELECT *
FROM su_experience
ORDER BY libelleExperience';
$RechercheExperienceExc = DbConnexion::getInstance()->prepare($sqlRechercheExperience);

$sqlRechercheNiveau = '
SELECT *
FROM su_niveau
ORDER BY ordreNiveau';
$rechercheNiveauExec = DbConnexion::getInstance()->prepare($sqlRechercheNiveau);

/** -----------------------------------------------------------------------------------------------
 * 2020-09-30 / Récupération de la notion de MOIS
 */
$DT = new DateTimeFrench(date('Y-m-d H:i:s'));

/** -----------------------------------------------------------------------------------------------
 * Liste des mois
 */
$month = ['', 'JANVIER', 'FÉVRIER', 'MARS', 'AVRIL', 'MAI', 'JUI',
    'JUILLET', 'AOÛT', 'SEPTEMBRE', 'OCTOBRE', 'NOVEMBRE', 'DÉCEMBRE'];
$selectMois = '<select id="lst_Mois_3_1" name="lst_Mois_3_1" class="form-control select2">';
for ($i = 1; $i <= 12 ; $i++) {
    $selectMois .= '<option '.
        ($i == date('m') ? 'SELECTED="SELECTED" ' : '')
        .' value="'.$i.'">'.$month[$i].'</option>';
}
$selectMois .= '</select>';

/** -----------------------------------------------------------------------------------------------
 * Liste des années
 */
$year = date('Y');
$selectYear = '<select id="lst_Year_3_1" name="lst_Year_3_1" class="form-control select2">';
for ($i = 0; $i <= 2 ; $i++) {
    $selectYear .= '<option '.
        ($i == 0 ? 'SELECTED="SELECTED" ' : '')
        .' value="'.($year-$i).'">'.($year-$i).'</option>';
}
$selectYear .= '</select>';

#@session_start();
#if ($_SESSION['idUtilisateurSUPPLACTIV'] == 1) {
#    $visible = '';
#} else {
#    $visible = ' style:"display:none" ';
#}
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" action="#">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Compétences :</label>
                                <select id="lstCompetence_3_1" name="lstCompetence_3_1" class="form-control select2"
                                        multiple>
                                    <?php
                                    $RechercheCompetenceExc->execute();
                                    while ($InfoCompetence = $RechercheCompetenceExc->fetch(PDO::FETCH_OBJ)) {
                                        print '<option value="' . $InfoCompetence->idCompetence . '">' . $InfoCompetence->libelleCompetence . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">Qualifications :</label>
                                <select id="lstQualification_3_1" name="lstQualification_3_1"
                                        class="form-control select2" multiple>
                                    <?php
                                    $RechercheQualificationExc->execute();
                                    while ($InfoQualification = $RechercheQualificationExc->fetch(PDO::FETCH_OBJ)) {
                                        print '<option value="' . $InfoQualification->idQualification . '">' . $InfoQualification->libelleQualification . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Expériences :</label>
                                <select id="lstExperience_3_1" name="lstExperience_3_1" class="form-control select2"
                                        multiple>
                                    <?php
                                    $RechercheExperienceExc->execute();
                                    while ($InfoExperience = $RechercheExperienceExc->fetch(PDO::FETCH_OBJ)) {
                                        print '<option value="' . $InfoExperience->idExperience . '">' . $InfoExperience->libelleExperience . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Niveau :</label>
                                <select id="lstNiveau_3_1" name="lstNiveau_3_1" class="form-control select2" multiple>
                                    <?php
                                    $rechercheNiveauExec->execute();
                                    while ($InfoNiveau = $rechercheNiveauExec->fetch(PDO::FETCH_OBJ)) {
                                        print '<option value="' . $InfoNiveau->idNiveau . '">' . $InfoNiveau->libelleNiveau . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Auto :</label>
                                <select id="lstAuto_3_1" name="lstAuto_3_1" class="form-control select2">
                                    <option value="ALL">TOUT</option>
                                    <option value="0">NON</option>
                                    <option value="1">OUI</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">Géocodage :</label>
                                <select id="lstMap_3_1" name="lstMap_3_1" class="form-control select2">
                                    <option value="ALL">TOUT</option>
                                    <option value="0">NON Géocodées</option>
                                    <option value="1">OUI Géocodées</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row" <?php echo $visible; ?> >
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">Export Pôle Emploi
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $selectMois; ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $selectYear; ?>
                    </div>
                    <div class="col-md-2">
                        <button class="btn bg-red-thunderbird no-margin"
                                id="btnExportPE"><i class="fa fa-file-excel-o"></i> Exporter</button>
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
<div class="alert alert-warning"><strong>Information : </strong> La page de l'intervenante s'ouvre désormais dans un
    autre onglet.
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des intervenants enregistrés dans la Base de Données
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableIntervenant_4_1">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nom Prénom</th>
                        <th>Code Postal</th>
                        <th>Ville</th>
                        <th>Mail</th>
                        <th>Tél. mobile</th>
                        <th>Département</th>
                        <th>Nb Heure</th>
                        <th>AUTO E.</th>
                        <th>Actif</th>
                        <th>Clos</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalSuppression" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content" style="width: 500px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Suppression d'un intervenant</h4>
            </div>
            <div class="modal-body">
                <p>Confirmez-vous la suppression de l'intervenant <strong><span id="nom-sup"></span></strong> ?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-confirm-sup">Oui, je confirme</button>
            </div>
        </div>
    </div>
</div>