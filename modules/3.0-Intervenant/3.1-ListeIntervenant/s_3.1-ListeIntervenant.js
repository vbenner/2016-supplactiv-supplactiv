var lstCompetence_3_1, lstQualification_3_1, lstExperience_3_1, lstNiveau_3_1, tableIntervenant_4_1;
var lstAuto_3_1, lstMap_3_1;
var boolNv = true;
jQuery(document).ready(function() {
    ClassIntervenante.saveSearchToSession();
    ClassIntervenante.init();
});

var ClassIntervenante = {
    init: function(){

        /** ---------------------------------------------------------------------------------------
         * On va générer l'Export PE
         */
        $('#lst_Mois_3_1').select2({
            placeholder: "Sélection...",
            allowClear: false
        });

        $('#lst_Year_3_1').select2({
            placeholder: "Sélection...",
            allowClear: false
        });

        $('#btnExportPE').unbind().bind('click', function () {
            $.ajax({
                async: false,
                url: "download/func.3.1-exportPE.php",
                type: "POST",
                data: {
                    lstMonth : $('#lst_Mois_3_1 option:selected').val(),
                    lstYear : $('#lst_Year_3_1 option:selected').val(),
                },
                dataType: 'json'
            }).done(function( data ) {
                var a = document.createElement('a');
                a.target="_blank";
                a.href='http://bo.supplactiv.fr/download/' + data.file;
                a.click();
            });
        });

        $('#lstCompetence_3_1').select2({
            placeholder: "Sélection...",
            allowClear: true
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('#lstQualification_3_1').select2({
            placeholder: "Sélection...",
            allowClear: true
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('#lstExperience_3_1').select2({
            placeholder: "Sélection...",
            allowClear: true
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('#lstNiveau_3_1').select2({
            placeholder: "Sélection...",
            allowClear: true
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('#lstAuto_3_1').select2({
            placeholder: "Sélection...",
            allowClear: false
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('#lstMap_3_1').select2({
            placeholder: "Sélection...",
            allowClear: false
        }).bind('change', function(){
            ClassIntervenante.saveSearchToSession();
        });

        $('a.fancy').fancybox({

            fitToView:false,
            autoSize:false,
            'width':parseInt($(window).width() * 0.7),
            'height':parseInt($(window).height() * 0.8),
            'autoScale':true,
            'type': 'iframe'

            /*
            'frameWidth': 200,
            'frameHeight':600,
            'overlayShow':true,
            'hideOnContentClick':false,
            'type':'iframe'
             */
        });
    },

    initTableIntervenante: function(){

        tableIntervenant_4_1 = $('#tableIntervenant_4_1').DataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>rB>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                    extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                    extend: 'pdf',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10 ]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10 ]
                    },
                },
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                buttons: {
                    copyTitle: 'Ajouté au presse-papier',
                    copySuccess: {
                        _: '%d lignes copiées',
                        1: '1 ligne copiée'
                    }
                }
            },


            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [1, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.4.1-listeIntervenante.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0)', nRow).children('button').unbind().bind('click', function(event){

                    /** ---------------------------------------------------------------------------
                     * Window .open
                     * @type {string}
                     */
                    event.stopPropagation();
                    var a = document.createElement('a');
                    a.href = 'index.php?ap=3.0-Intervenant&ss_m=3.3-FicheIntervenant&s='+$(this).attr('data-id');
                    a.target = '_blank';
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);

                    //document.location.href='index.php?ap=3.0-Intervenant&ss_m=3.3-FicheIntervenant&s='+$(this).attr('data-id');
                });

                $('td:eq(11)', nRow).children('button').bind('click', function(){
                    ClassIntervenante.demandeSuppression($(this).attr('data-id'), $(this).attr('data-name'));
                });
            },
            fnCreatedRow : function( nRow, aData, iDataIndex ) {
                //console.log(nRow + ' - ' + aData);
                //$('td:eq(1)', nRow).attr('nowrap');
            },
            fnInitComplete: function() {
                //$('#tableIntervenant_4_1 tbody tr').each(function(){
                //    $(this).find('td:eq(1)').attr('nowrap', 'nowrap');
                //});
                $('.dt-buttons').css({
                    'margin-left':'15px',
                    'padding-bottom':'5px'
                });
            }
        });
        var tableWrapper = $('#tableIntervenant_4_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    saveSearchToSession: function(){
        lstCompetence_3_1 = $('#lstCompetence_3_1').val();
        lstQualification_3_1 = $('#lstQualification_3_1').val();
        lstExperience_3_1 = $('#lstExperience_3_1').val();
        lstNiveau_3_1 = $('#lstNiveau_3_1').val();
        lstAuto_3_1 = $('#lstAuto_3_1').val();
        lstMap_3_1 = $('#lstMap_3_1').val();

            $.ajax({
                async: false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    lstCompetence_3_1 : lstCompetence_3_1,
                    lstQualification_3_1 : lstQualification_3_1,
                    lstExperience_3_1 : lstExperience_3_1,
                    lstNiveau_3_1 : lstNiveau_3_1,
                    lstAuto_3_1 : lstAuto_3_1,
                    lstMap_3_1 : lstMap_3_1,
                },
                dataType: 'json'
            }).done(function( data ) {
                if(boolNv) {
                    ClassIntervenante.initTableIntervenante();
                    boolNv = false;
                }
                else {
                    tableIntervenant_4_1.ajax.reload();
                }
            });

    },

    demandeSuppression: function(idIntervenant, nomIntervenant){
        $('#modalSuppression').modal('show');
        $('#nom-sup').html(nomIntervenant);
        $('.btn-confirm-sup').unbind().bind('click', function(){
            $.ajax({
                async: false,
                url: "includes/functions/web2bdd/func.3.1-suppressionIntervenant.php",
                type: "POST",
                data: {
                    idIntervenant : idIntervenant
                },
                dataType: 'json'
            }).done(function( data ) {
                $('#modalSuppression').modal('hide');
                toastr.success('L\'intervenant '+nomIntervenant+' a bien été supprimé', 'Suppression d\'un intervenant');
                tableIntervenant_4_1.api().ajax.reload();
            });
        });
    }
}
