jQuery(document).ready(function() {
    ClassAjoutIntervenante.init();
});

var ClassAjoutIntervenante = {

    isValidEmailAddress: function (emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    },

    init: function(){
        this.rechercheContenuSelect();

        $('.btn-add-intervenant').bind('click', function(){

            ClassAjoutIntervenante.ajoutIntervenant();
        });
    },

    ajoutIntervenant: function(){

        /** ---------------------------------------------------------------------------------------
         * Si le fichier est renseigné, il doit être au format .PNG
         */
        if ($("#fichierPHOTO").get(0).files.length !== 0) {
            var nom = $("#fichierPHOTO").prop("files")[0].name;
            nom = nom.toLowerCase();
            if (!nom.match(/\.(png|jpg|jpeg)$/)) {
                alert('Seules les photos au format PNG et JPG sont supportées');
                return false;
            }
        }

        if ($("#fichierCV").get(0).files.length !== 0) {
            var nom = $("#fichierCV").prop("files")[0].name;
            nom = nom.toLowerCase();
            if (!nom.match(/\.(pdf|doc|docx)$/)) {
                alert('Seuls les CV au format PDF et WORD sont supportés');
                return false;
            }
        }

        /** ---------------------------------------------------------------------------------------
         * 2019-10-25 - Test de la partie EMAIL
         */
        if ($('#ztEmail').val() != '') {
            if (! ClassAjoutIntervenante.isValidEmailAddress($('#ztEmail').val())) {
                alert('L\'email est invalide. Merci de le corriger.');
                return false;
            }
        }

        $.ajax({
            async:false,
            url: "includes/functions/web2bdd/func.3.2-ajoutIntervenant.php",
            type: "POST",
            data: {
                info : ClassAjoutIntervenante.serialize('.form-intervenant'),
                lstCompetence : $('#zsCompetence').val(),
                lstQualification : $('#zsQualification').val(),
                lstExperience : $('#zsExperience').val(),
                lstDept : $('#zsDepartementIntervention').val(),
                lstNiveau : $('#zsNiveau').val(),
            },
            dataType: 'json',
        }).done(function( data ) {
            if(data.result != 0){

                if ($("#fichierPHOTO").get(0).files.length !== 0) {

                    var file_data = $("#fichierPHOTO").prop("files")[0]; // Getting the properties of file from file field
                    var form_data = new FormData(); // Creating object of FormData class

                    form_data.append("file", file_data) // Appending parameter named file with properties of file_field to form_data
                    form_data.append("idIntervenant", data.result) // Adding extra parameters to form_data
                    $.ajax({
                        async : false,
                        url: "import/func.importPhoto.php", // Upload Script
                        dataType: 'script',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data, // Setting the data attribute of ajax with file_data
                        type: 'post',
                        success: function(data) {
                            // Do something after Ajax completes
                        }
                    });
                }

                if ($("#fichierCV").get(0).files.length !== 0) {

                    var file_data = $("#fichierCV").prop("files")[0]; // Getting the properties of file from file field
                    var form_data = new FormData(); // Creating object of FormData class

                    form_data.append("file", file_data) // Appending parameter named file with properties of file_field to form_data
                    form_data.append("idIntervenant", data.result) // Adding extra parameters to form_data
                    $.ajax({
                        async : false,
                        url: "import/func.importCV.php", // Upload Script
                        dataType: 'script',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data, // Setting the data attribute of ajax with file_data
                        type: 'post',
                        success: function(data) {
                            // Do something after Ajax completes
                        }
                    });
                }

                document.location.href = 'index.php?ap=3.0-Intervenant&ss_m=3.3-FicheIntervenant&s='+data.result;
            }else {
                toastr.error("Veuillez vérifier le nom, prénom, date de naissance, ville de naissance et numéro de sécurité sociale !", "ERREUR - Ajout d'un intervenant");
            }
        });
    },

    serialize: function(el) {
        var serialized = $(el).serialize();
        if (!serialized) // not a form
            serialized = $(el).
                find('input[name],select[name],textarea[name]').serialize();
        return serialized;
    },

    rechercheContenuSelect: function(){
        $.ajax({
            async : false,
            url: "includes/functions/bdd2web/func.3.2-rechercheInfoIntervenant.php",
            type: "POST",
            data: { },
            dataType: 'json'
        }).done(function( data ) {
            $.each( data.genres, function( key, info ) {
                $("#zsGenre").append(
                    $('<option>')
                        .attr('value', info.idGenre)
                        .html(info.libelleGenre)
                        .attr('selected', (info.idGenre == 'E') ? true : false)
                );
            });

            $.each( data.situations, function( key, info ) {
                $("#zsSituation").append(
                    $('<option>')
                        .attr('value', info.idSituation)
                        .html(info.libelleSituation)
                        .attr('selected', (info.idSituation == 'C') ? true : false)
                );
            });

            for(var i = 0; i <= 20; i++){
                $("#zsNbEnfant").append(
                    $('<option>')
                        .attr('value', i)
                        .html(i)
                        .attr('selected', (i == 0) ? true : false)
                );
            }

            $('#zsAutoEntrepreneuse').append($('<option>').attr('value', 'NON').html('NON').prop('selected', true));
            $('#zsAutoEntrepreneuse').append($('<option>').attr('value', 'OUI').html('OUI').prop('selected', false));


            $.each( data.pays, function( key, info ) {
                $("#zsPaysNaissance").append(
                    $('<option>')
                        .attr('value', info.codeIso)
                        .html(info.codeIso+' - '+info.libellePays)
                        .attr('selected', (info.codeIso == 'FR') ? true : false)
                );
            });
            $("#zsPaysNaissance").select2();

            $.each( data.departements, function( key, info ) {
                $("#zsDepartementNaissance").append(
                    $('<option>')
                        .attr('value', info.idDepartement)
                        .html(info.idDepartement+' - '+info.libelleDepartement)
                );


                $("#zsDepartementIntervention").append(
                    $('<option>')
                        .attr('value', info.idDepartement)
                        .html(info.idDepartement+' - '+info.libelleDepartement)
                );
            });
            $("#zsDepartementNaissance").select2();
            $("#zsDepartementIntervention").select2();


            $.each( data.competences, function( key, info ) {
                $("#zsCompetence").append(
                    $('<option>')
                        .attr('value', info.idCompetence)
                        .html(info.libelleCompetence)
                );
            });
            $("#zsCompetence").select2();

            $.each( data.qualifications, function( key, info ) {
                $("#zsQualification").append(
                    $('<option>')
                        .attr('value', info.idQualification)
                        .html(info.libelleQualification)
                );
            });
            $("#zsQualification").select2();

            $.each( data.experiences, function( key, info ) {
                $("#zsExperience").append(
                    $('<option>')
                        .attr('value', info.idExperience)
                        .html(info.libelleExperience)
                );
            });
            $("#zsExperience").select2();

            $("#zsNiveau").append($('<option value="">'));
            $.each( data.niveaux, function( key, info ) {
                $("#zsNiveau").append(
                    $('<option>')
                        .attr('value', info.idNiveau)
                        .html(info.libelleNiveau)
                );
            });
            $("#zsNiveau").select2({allowClear:true});

            $('#zsNationalite').append($('<option>').attr('value', 'F').html('Française').prop('selected', true));
            $('#zsNationalite').append($('<option>').attr('value', 'E').html('Etrangère').prop('selected', false));

            $("#ztDateNaissance").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateExpiration").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateContratCadre").inputmask("d/m/y", {
                "placeholder": "*"
            });
            $("#ztDateUrssaf").inputmask("d/m/y", {
                "placeholder": "*"
            });


            $('#ztCodePostal').inputmask("99999");
            $('#ztNumSecuSocial').inputmask("9 99 99 9* 999 999 99");
            $('.input-tel').inputmask("99 99 99 99 99");
            $('#ztEmail').inputmask('Regex', { regex: "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}" });

        });
    }
}