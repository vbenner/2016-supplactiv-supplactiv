<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Formulaire de création
                </div>

            </div>
            <div class="portlet-body form">

                <div class="alert alert-info">
                    <strong>ATTENTION</strong> - Pour la génération des DUE/DPAE, les informations suivantes sont
                    nécessaires :
                    <br/>- Date / Ville et département de Naissance
                    <br/>- Numéro de sécurité sociale
                </div>
                <div class="alert alert-danger">
                    <strong>ATTENTION</strong> - Pour une personne née à l'étranger, veuillez choisir le département de
                    naissance 99 - ETRANGER (Contrainte URSSAF)
                </div>

                <div class="form-intervenant form-horizontal form-bordered form-row-stripped">
                    <h2 class="caption-subject font-blue-sup bold uppercase center-block ">INFORMATIONS PRINCIPALES</h2>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label">Genre :</label>
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsGenre" name="zsGenre">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Situation :</label>
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsSituation" name="zsSituation">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Nombre d'enfant :</label>
                            <div>

                                <select class="form-control" id="zsNbEnfant" name="zsNbEnfant">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Auto-entrepreneuse :</label>
                            <div>

                                <select class="form-control" id="zsAutoEntrepreneuse" name="zsAutoEntrepreneuse">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Nom actuel :</label>
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztNom" name="ztNom"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Nom de jeune fille :</label>
                            <div>
                                <input type="text" class="form-control" id="ztNomJF" name="ztNomJF"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Prénom :</label>
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztPrenom" name="ztPrenom"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label">Pays de naissance :</label>
                            <div>
                                <select class="form-control" id="zsPaysNaissance" name="zsPaysNaissance">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Département de naissance :</label>
                            <div>
                                <select class="form-control" id="zsDepartementNaissance" name="zsDepartementNaissance">

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Ville de naissance :</label>
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Recommandé"></i>
                                <input type="text" class="form-control" id="ztVilleNaissance" name="ztVilleNaissance"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Nationalité :</label>
                            <div>
                                <select class="form-control" id="zsNationalite" name="zsNationalite">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-3">
                            <label class="control-label">Date Naissance :</label>
                            <div>
                                <input type="text" class="form-control" id="ztDateNaissance" name="ztDateNaissance"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Carte de séjour :</label>
                            <div>
                                <input type="text" class="form-control" id="ztCarteSejour" name="ztCarteSejour"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Date expiration :</label>
                            <div>
                                <input type="text" class="form-control" id="ztDateExpiration" name="ztDateExpiration"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Num. Sécurité sociale :</label>
                            <div>
                                <input type="text" class="form-control" id="ztNumSecuSocial" name="ztNumSecuSocial"/>
                            </div>
                        </div>
                    </div>

                    <!-- DATE URSSAF -->
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label" style="text-align: left">Date Attestation Urssaf :<br/>
                            <small>Uniquement pour les auto-entrepreneuses</small></label>
                            <div>
                                <input type="text" class="form-control" id="ztDateUrssaf" name="ztDateUrssaf"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">

                        <div class="col-md-3">
                            <label class="control-label">Niveau :</label>
                            <div>
                                <select class="form-control" id="zsNiveau" name="zsNiveau">

                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">SIRET :</label>
                            <div>
                                <input type="text" class="form-control" id="ztNumSiret" name="ztNumSiret"/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Date Contrat Cadre :</label>
                            <div>
                                <input type="text" class="form-control" id="ztDateContratCadre"
                                       name="ztDateContratCadre"/>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Commentaire :</label>
                            <div>
                                <textarea class="form-control" id="ztCommentaire" name="ztCommentaire"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-2" style="text-align:left;">Photo :</label>
                        <div class="col-md-4">
                            <fieldset class="label_side">
                                <div>
                                    <div id=filePhoto" class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input span3"
                                                 data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                        class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Choisir une photo </span>
                                                            <span class="fileinput-exists">Modifier </span>
                                                            <input type="file" id="fichierPHOTO" name="fichierPHOTO">
                                                        </span>
                                            <a href="#" class="input-group-addon btn red fileinput-exists"
                                               data-dismiss="fileinput">Supprimer </a>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <span class="help-block">Fichier JPG / PNG uniquement.</span>
                        </div>

                        <label class="control-label col-md-2" style="text-align:left;">CV :</label>
                        <div class="col-md-4">
                            <fieldset class="label_side">
                                <div>
                                    <div id=fileCV" class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input span3"
                                                 data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp; <span
                                                        class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Choisir un CV </span>
                                                            <span class="fileinput-exists">Modifier </span>
                                                            <input type="file" id="fichierCV" name="fichierCV">
                                                        </span>
                                            <a href="#" class="input-group-addon btn red fileinput-exists"
                                               data-dismiss="fileinput">Supprimer </a>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <span class="help-block">Fichier PDF / WORD uniquement.</span>
                        </div>

                    </div>


                    <h2 class="caption-subject font-blue-sup bold uppercase center-block ">ADRESSES</h2>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Adresse :</label>
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseA" name="ztAdresseA"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Complément 1 :</label>
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseB" name="ztAdresseB"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Complément 2 :</label>
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseC" name="ztAdresseC"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Code Postal :</label>
                            <div>
                                <input type="text" class="form-control" id="ztCodePostal" name="ztCodePostal"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Ville :</label>
                            <div>
                                <input type="text" class="form-control" id="ztVille" name="ztVille"/>
                            </div>
                        </div>
                    </div>

                    <h2 class="caption-subject font-blue-sup bold uppercase center-block ">CONTACT</h2>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Tel. fixe :</label>
                            <div>
                                <input type="text" class="form-control input-tel" id="ztTelFixe" name="ztTelFixe"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Tel. Mobile :</label>
                            <div>
                                <input type="text" class="form-control input-tel" id="ztTelMobile" name="ztTelMobile"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Autre tel. :</label>
                            <div>
                                <input type="text" class="form-control input-tel" id="ztAutreTel" name="ztAutreTel"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Fax :</label>
                            <div>
                                <input type="text" class="form-control input-tel" id="ztFax" name="ztFax"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">E-mail :</label>
                            <div>
                                <input type="text" class="form-control" id="ztEmail" name="ztEmail"/>
                            </div>
                        </div>
                    </div>

                    <h2 class="caption-subject font-blue-sup bold uppercase center-block ">INFORMATIONS BANCAIRES</h2>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Numéro IBAN :</label>
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztIban" name="ztIban"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Code BIC :</label>
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztCodeBic" name="ztCodeBic"/>
                            </div>
                        </div>
                    </div>



                    <h2 class="caption-subject font-vert-sup bold uppercase center-block ">DOMICILIATION BANCAIRE</h2>

                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Adresse Banque :</label>
                            <div>
                                <textarea class="form-control" id="ztAdresseBanque" name="ztAdresseBanque" rows="3"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Code Banque :</label>
                            <div>
                                <input type="text" class="form-control" id="ztCodeBanque" name="ztCodeBanque"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Code Guichet :</label>
                            <div>
                                <input type="text" class="form-control" id="ztCodeGuichet" name="ztCodeGuichet"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label">Numéro Compte :</label>
                            <div>
                                <input type="text" class="form-control" id="ztNumCompte" name="ztNumCompte"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Clé :</label>
                            <div>
                                <input type="text" class="form-control" id="ztCle" name="ztCle"/>
                            </div>
                        </div>
                    </div>

                    <h2 class="caption-subject font-blue-sup bold uppercase center-block ">INFORMATIONS
                        COMPLEMENTAIRES</h2>

                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="control-label">Compétences :</label>
                            <div class="">

                                <select class="form-control" id="zsCompetence" name="zsCompetence" multiple>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Qualifications :</label>
                            <div class="">

                                <select class="form-control" id="zsQualification" name="zsQualification" multiple>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Expérience :</label>
                            <div class="">

                                <select class="form-control" id="zsExperience" name="zsExperience" multiple>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Départements d'interventions :</label>
                            <div class="">

                                <select class="form-control" id="zsDepartementIntervention"
                                        name="zsDepartementIntervention" multiple>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn bg bg-green pull-right btn-add-intervenant"><i class="fa fa-save"></i>
                            Ajouter l'intervenant
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>