/** -----------------------------------------------------------------------------------------------
 * Trois tableaux
 * Semestre / Trimestre / Mois
 */
var tableEvolClientY = null;
var tableEvolClientS = null;
var tableEvolClientT = null;
var tableEvolClientM = null;
var tableListeClient = null;

$().ready(function () {
    ClassStats.init();
    ClassStats.initComponents();
});

/** -----------------------------------------------------------------------------------------------
 * Classe STATS
 */
var table;
var ClassStats = {

    /** -------------------------------------------------------------------------------------------
     * Initialisation globale
     */
    init: function () {
        ClassStats.initList();
        ClassStats.bindEvents();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des boutons
     */
    bindEvents: function () {
        ClassStats.bindClear();
        ClassStats.bindFilter();
    },

    clearTables: function () {
        if (tableEvolClientY != undefined) {
            $('#tableEvolClientY').dataTable().fnClearTable();
            $('#tableEvolClientY').dataTable().fnDestroy();
        }

        if (tableEvolClientS != null) {
            $('#tableEvolClientS').dataTable().fnClearTable();
            $('#tableEvolClientS').dataTable().fnDestroy();
        }

        if (tableEvolClientT != null) {
            $('#tableEvolClientT').dataTable().fnClearTable();
            $('#tableEvolClientT').dataTable().fnDestroy();
        }

        if (tableEvolClientM != null) {
            $('#tableEvolClientM').dataTable().fnClearTable();
            $('#tableEvolClientM').dataTable().fnDestroy();
        }

        if (tableListeClient != null) {
            $('#tableListeClient').dataTable().fnClearTable();
            $('#tableListeClient').dataTable().fnDestroy();
        }
    },

    bindClear: function () {

        /** ---------------------------------------------------------------------------------------
         * On clear les dates et le salarie
         */
        $('#btnClear').on('click', function () {
            $("#lstFiltreCampagne").select2("val", "").trigger('change');
            ClassStats.clearTables();

            $('#tableEvolClientY').hide();
            $('#tableEvolClientS').hide();
            $('#tableEvolClientT').hide();
            $('#tableEvolClientM').hide();
            $('#tableListeClient').hide();
        });
    },

    bindFilter: function () {

        /** ---------------------------------------------------------------------------------------
         * Bouton FILTRER
         */
        $('#btnSearch').on('click', function () {

            ClassStats.clearTables();

            /** -----------------------------------------------------------------------------------
             * On récupère 3 listes, QTE Y / QTE Y - 1
             */
            $.ajax({
                async:false,
                url: "includes/functions/bdd2web/stats.evolutionClient.php",
                type: "POST",
                data: {
                    campagne: $('#lstFiltreCampagne').val(),
                },
                dataType: 'json',
                success: function (data) {

                    var year = data.YEAR;
                    var M = data.DATAM;
                    var T = data.DATAT;
                    var S = data.DATAS;
                    var Y = data.DATAY;
                    var C = data.DATAC;

                    //$('#tableListeClient tbody').empty();

                    /** ---------------------------------------------------------------------------
                     * GESTION DES ANNEES
                     */
                    var table = 'tableEvolClientY';
                    var tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    var lig1g = '<tr>' + '<td>_CLIENTS</td><td>Nb</td>';
                    //var lig2g = ''; //'<tr>' + '<td>_CLIENTS</td><td>tx (i-1)</td>';
                    var lig2g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (N -1)</td>';

                    $.each(Y, function (index, item) {

                        /** -----------------------------------------------------------------------
                         * De haut en bas :
                         *
                         * NB
                         * TX (i-1)
                         * TX (y-1)
                         */
                        lig1g += '<td>' + item.nb + '</td>';
                        lig2g += '<td>' + item.txy + '</td>';
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);

                    /** ---------------------------------------------------------------------------
                     * GESTION DES SEMESTRES
                     */
                    table = 'tableEvolClientS';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>S1/' + i + '</th>';
                        tr += '<th>S2/' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_CLIENTS</td><td>Nb</td>';
                    lig2g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (semestre-1)</td>';
                    //lig3g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (y-1)</td>';

                    $.each(S, function (index, item) {
                        var year = item;
                        $.each(year, function (index, subitem) {
                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            //lig2g += '<td>' + subitem.txi + '</td>';
                            lig2g += '<td>' + subitem.txy + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    //lig3g += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    //$('#' + table + ' tbody').append(lig3g);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES TRIMESTRES
                     */
                    table = 'tableEvolClientT';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 4; i <= year; i++) {
                        tr += '<th>T1/' + i + '</th>';
                        tr += '<th>T2/' + i + '</th>';
                        tr += '<th>T3/' + i + '</th>';
                        tr += '<th>T4/' + i + '</th>';
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_CLIENTS</td><td>Nb</td>';
                    lig2g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (trimestre-1)</td>';
                    //lig3g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (y-1)</td>';

                    $.each(T, function (index, item) {

                        var year = item;
                        $.each(year, function (index, subitem) {

                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            //lig2g += '<td>' + subitem.txi + '</td>';
                            lig2g += '<td>' + subitem.txy + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    //lig3g += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    //$('#' + table + ' tbody').append(lig3g);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES MOIS
                     */
                    table = 'tableEvolClientM';
                    tr = '<tr><th></th><th></th>';
                    for (i = year - 1; i <= year; i++) {
                        for (j = 1; j <= 12; j++) {
                            tr += '<th>' + pad(j, 2) + '/' + i + '</th>';

                        }
                    }
                    tr += '</tr>';
                    $('#' + table + ' thead').html(tr);

                    lig1g = '<tr>' + '<td>_CLIENTS</td><td>Nb</td>';
                    lig2g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (mois-1)</td>';
                    //lig3g = '<tr>' + '<td>_CLIENTS</td><td>Évolution (%) (y-1)</td>';

                    $.each(M, function (index, item) {

                        var year = item;
                        $.each(year, function (index, subitem) {

                            /** -----------------------------------------------------------------------
                             * De haut en bas :
                             *
                             * NB
                             * TX (i-1)
                             * TX (y-1)
                             */
                            lig1g += '<td>' + subitem.nb + '</td>';
                            //lig2g += '<td>' + subitem.txi + '</td>';
                            lig2g += '<td>' + subitem.txi + '</td>';
                        });
                    });

                    lig1g += '</tr>';
                    lig2g += '</tr>';
                    //lig3g += '</tr>';

                    $('#' + table + ' tbody').append(lig1g);
                    $('#' + table + ' tbody').append(lig2g);
                    //$('#' + table + ' tbody').append(lig3g);


                    /** ---------------------------------------------------------------------------
                     * GESTION DES CLIENTS
                     */
                    var table = 'tableListeClient';

                    $.each(C, function (index, item) {

                        /** -----------------------------------------------------------------------
                         * De haut en bas :
                         *
                         * NB
                         * TX (i-1)
                         * TX (y-1)
                         */
                        lig1g = '<tr><td>' + item.LIB + '</td><td>' + item.YEAR + '</td></tr>';
                        $('#' + table + ' tbody').append(lig1g);
                        //lig2g += '<td>' + item.YEAR + '</td>';
                    });

                    //lig1g += '</tr>';
                    //lig2g += '</tr>';

                    //$('#' + table + ' tbody').append(lig1g);
                    //$('#' + table + ' tbody').append(lig2g);


                    /** ---------------------------------------------------------------------------
                     * DATATABLISATION DES TABLES
                     */
//return true;

                    var groupColumn = 0;
                    tableEvolClientY = $('#tableEvolClientY').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="7">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolClientS = $('#tableEvolClientS').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="11">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolClientT = $('#tableEvolClientT').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="21">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    tableEvolClientM = $('#tableEvolClientM').dataTable({
                        "scrollX": true,
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [
                            {"visible": false, "targets": groupColumn}
                        ],
                        //"order": [[ groupColumn, 'asc' ]],
                        "ordering": false,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({page: 'current'}).nodes();
                            var last = null;

                            api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before(
                                        '<tr class="group"><td colspan="25">' + group + '</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                        }

                    });

                    /** ---------------------------------------------------------------------------
                     * Mise à jour de la liste des Clients
                     */
                    tableListeClient = $('#tableListeClient').dataTable({
                        dom: 'Blfrtip',
                        buttons: [
                            {
                                text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                                extend: 'copy',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                                extend: 'csv',
                            },
                            {
                                text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                                extend: 'excel',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                                extend: 'pdf',
                            },
                            {
                                text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                                extend: 'print',
                            }],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                            buttons: {
                                copyTitle: 'Ajouté au presse-papier',
                                copySuccess: {
                                    _: '%d lignes copiées',
                                    1: '1 ligne copiée'
                                }
                            }
                        },
                        "columnDefs": [{
                            "orderable": true,
                            "targets": [0, 1]
                        }],
                        "order": [
                            [0, 'asc']
                        ],
                        "drawCallback": function (settings) {
                            /*
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group"><td colspan="25">'+group+'</td></tr>'
                                    );

                                    last = group;
                                }
                            });
                             */
                        }

                    });


                    $('#tableEvolClientY').show();
                    $('#tableEvolClientS').show();
                    $('#tableEvolClientT').show();
                    $('#tableEvolClientM').show();
                    $('#tableListeClient').show();

                    return true;

                }
            });
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des listes
     */
    initList: function () {
        ClassStats.initCampagne();
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des composants
     */
    initComponents: function () {
        $('.select2').select2({
            placeholder: "Sélection...",
            allowClear: true
        });
    },

    /** -------------------------------------------------------------------------------------------
     * Initialisation des campagnes
     */
    initCampagne: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.getListeCampagnes.php",
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                //var option = $('<option>').attr('value', 0).html('Sélectionner une campagne');
                //$('#lstFiltreCampagne').append(option);
                $.each(data.items, function (index, item) {
                    var option =
                        $('<option>')
                            .attr('value', item.idCampagne)
                            .html(item.libelleCampagne)

                    ;
                    $('#lstFiltreCampagne').append(option);
                });
            }
        });
        $('#lstFiltreCampagne').html('<option></option>').trigger('change');
    },

}

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}