<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * On se place à horizon +nbMois
 * @throws Exception
 */
function expireLeMois($inter, $nbMois, $interval, $inverse = 0, $debug = 0) {

    /** -------------------------------------------------------------------------------------------
     * On prend le premier du mois courant, on ajout nbMois et on retire un mois....
     */
    $date = new DateTime(date('Y-m-01'));
    $date->modify( $nbMois." month");
    $nextMonthEnd = $date->format("Y-m-d");
    $date->modify( $interval." month");
    $nextMonthDeb = $date->format("Y-m-d");

    #if ($debug == 1) {
    #    if ($inverse) {
    #        echo 'ENTRE ' . $nextMonthEnd . ' et ' . $nextMonthDeb . '<br/>';
    #    }
    #    else {
    #        echo 'ENTRE ' . $nextMonthDeb . ' et ' . $nextMonthEnd . '<br/>';
    #    }
    #}
    if ($inverse) {
        return $inter->dateUrssaf >= $nextMonthEnd && $inter->dateUrssaf < $nextMonthDeb;
    }
    else {
        return $inter->dateUrssaf >= $nextMonthDeb && $inter->dateUrssaf < $nextMonthEnd;
    }
}

function buildLigneTR($inter, $color) {

    if ($color != '') {
        $dateFR = DateTime::createFromFormat('Y-m-d', $inter->dateUrssaf);
        $td = '<span class="label label-xs label-'.$color.'">'.$dateFR->format('d/m/Y');
    }
    else {
        $td = 'Non renseigné';
    }
    $tr = '
    <tr>
        <td>' . $inter->nomIntervenant . '</td>
        <td>' . $inter->prenomIntervenant . '</td>
        <td>'.$td.'</span></td>
        <td>
            <a href="index.php?ap=3.0-Intervenant&amp;ss_m=3.3-FicheIntervenant&amp;s=' . $inter->idIntervenant . '" class="btn bg bg-red btn-xs no-margin" data-id="' . $inter->idIntervenant . '"><i class="icon-book-open"></i> Voir fiche</a>
        </td>
    </tr>
    ';

    return $tr;
}

/** -----------------------------------------------------------------------------------------------
 * Liste des Anniversaires
 */
$sqlRechercheAnniversaire = '
SELECT *
FROM su_intervenant
WHERE dateNaissance LIKE :dateNaissance
AND boolActif = 1
';
$RechercheAnniversaireExc = DbConnexion::getInstance()->prepare($sqlRechercheAnniversaire);

/** -----------------------------------------------------------------------------------------------
 * Liste des Heures Supp
 */
$startWeek[ 0 ] = date("Y-m-d", strtotime('last monday', strtotime('next week', time())));
$endWeek[ 0 ] = date("Y-m-d", strtotime('last sunday', strtotime('next week', time())));

#$startWeek[ 0 ] = '2018-07-09';
#$endWeek[ 0 ] = '2018-07-15';

for ($i = 1; $i <= 3; $i++) {
    $startWeek[ $i ] = date("Y-m-d", strtotime($startWeek[ $i - 1 ] . ' + 7 days'));
    $endWeek[ $i ] = date("Y-m-d", strtotime($endWeek[ $i - 1 ] . ' + 7 days'));
}
$sqlRechercheHeuresSupp = '
SELECT SU.idIntervenant, SU.nomIntervenant, SU.prenomIntervenant, SI.dateDebut, SI.dateFin
FROM su_intervention SI
	INNER JOIN su_intervenant SU ON SU.idIntervenant = SI.FK_idIntervenant
WHERE
(
	SI.dateDebut >= :from
	AND
	SI.dateFin <= :to
)
AND SU.idIntervenant <> 1
AND SI.FK_idContrat IS NOT NULL 
ORDER BY SU.nomIntervenant
';
$RechercheHeuresSuppExec = DbConnexion::getInstance()->prepare($sqlRechercheHeuresSupp);

/** -----------------------------------------------------------------------------------------------
 * Liste des Expirations Titre Séjour
 */
$dayNow = date("Y-m-d");
$monthEnd = date("Y-m-d", strtotime($dayNow . ' + 1 month'));
$sqlRechercheTitreSejour = '
SELECT *
FROM su_intervenant
WHERE
	dateFinSejour >= :from
	AND
	dateFinSejour <= :to
AND boolActif = 1
ORDER BY nomIntervenant
';
#echo $sqlRechercheTitreSejour.'<br/>';
#echo 'Du '.$dayNow.'<br/>';
#echo 'Au '.$monthEnd.'<br/>';

$RechercheTitreSejourExec = DbConnexion::getInstance()->prepare($sqlRechercheTitreSejour);


/** -----------------------------------------------------------------------------------------------
 * Liste des Expirations URSSAF
 * Un document URSSAF a une validité de 6 mois.
 * Donc, en avril 2019, les titres expirent si ils ont été créés en Octobre 2018
 */
$dayNow = date("Y-m-01");
$sqlRechercheUrssaf = '
SELECT DISTINCT *
FROM su_intervenant
WHERE
boolAutoEntrepreneuse = \'OUI\'
AND boolActif = 1
ORDER BY nomIntervenant
';
$RechercheUrssafExec = DbConnexion::getInstance()->prepare($sqlRechercheUrssaf);

/** -----------------------------------------------------------------------------------------------
 * Dernier export EXPENSYA
 */
$sqlGetLastExportExpensya = '
SELECT *
FROM du_params
WHERE skey = :key
';
$getLastExportExpensyaExec = DbConnexion::getInstance()->prepare($sqlGetLastExportExpensya);
$getLastExportExpensyaExec->bindValue(':key', 'expensya_export', PDO::PARAM_STR);
$getLastExportExpensyaExec->execute();
$reportExpensya = '';
if ($getLastExportExpensyaExec->rowCount() == 0) {
    $reportExpensya = '
    <div class="alert alert-danger" style="margin-bottom: 0px;">L\'Export Expensya n\'a jamais été réalisé !</div>';
}
else {
    /** -------------------------------------------------------------------------------------------
    /** -------------------------------------------------------------------------------------------
     * On a 2 exports / jour
     * 12:30 et 18:00
     */
    $getLastExportExpensya = $getLastExportExpensyaExec->fetch(PDO::FETCH_OBJ);
    $lastDate = $getLastExportExpensya->svalue;
    $today = date('Y-m-d H:s:i');
    $lastDateFR = dateSqlToFr($lastDate, false, true, true);

    /** -------------------------------------------------------------------------------------------
     * Même jour => OK !
     */
    if (substr($lastDate, 0, 10) == substr($today, 0, 10)) {
        $reportExpensya = '
        <div class="alert alert-success" style="margin-bottom: 0px;">L\'Export Expensya a été réalisé le '.$lastDateFR.'.</div>';
    }
    else {
        /** ---------------------------------------------------------------------------------------
         * On va juste vérifier la différence
         */
        $date1 = strtotime($today);
        $date2 = strtotime($lastDate);
        $diff = abs($date1 - $date2);
        $retour = array();

        $tmp = $diff;
        $retour['second'] = $tmp % 60;

        $tmp = floor( ($tmp - $retour['second']) /60 );
        $retour['minute'] = $tmp % 60;

        $tmp = floor( ($tmp - $retour['minute'])/60 );
        $retour['hour'] = $tmp % 24;

        $tmp = floor( ($tmp - $retour['hour'])  /24 );
        $retour['day'] = $tmp;

        if ($retour['day'] > 0) {
            $reportExpensya = '
                <div class="alert alert-danger" style="margin-bottom: 0px;">L\'Export Expensya a été réalisé le '.$lastDateFR.'.</div>';
        }
        else {
            if ($retour['hour'] >= 12) {
                $reportExpensya = '
                <div class="alert alert-warning" style="margin-bottom: 0px;">L\'Export Expensya a été réalisé le '.$lastDateFR.'.</div>';
            }
            else {
                $reportExpensya = '
                <div class="alert alert-success" style="margin-bottom: 0px;">L\'Export Expensya a été réalisé le '.$lastDateFR.'.</div>';
            }
        }
    }

}

/** -----------------------------------------------------------------------------------------------
 * Info serveur
 */
$total = disk_total_space('/')/1024/1024/1024;
$free = disk_free_space('/')/1024/1024/1024;
$percent = sprintf("%0.2f", $free*100/$total);
$color = '';
if ($percent >= 50) {
    $color = '#03dac6 !important;';
}
if ($percent >= 25 && $percent < 50) {
    $color = '#1e88e5 !important;';
}
if ($percent < 25 && $percent >= 10) {
    $color = '#f44336 !important;';
}
if ($percent < 10) {
    $color = '#b00020 !important;';
}

$infoServeur =  '
<div class="progress m-progress--lg">
    <div class="progress-bar" role="progressbar" 
        style="width: '.$percent.'%;background-color:'.$color.'" aria-valuenow="'.$percent.'" aria-valuemin="0" aria-valuemax="100"
        >
        '.$percent.'%'.' 
    </div>
</div>
';

?>

<div class="row" style="padding: 15px;">
    <h3><i class="icon-clock" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Export Expensya</h3>
    <hr>
    <?php
        echo $reportExpensya;
    ?>
</div>

<div class="row" style="padding: 15px;">
    <h3><i class="icon-clock" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Informations Serveur</h3>
    <hr>
    <h5>Espace disponible :</h5>
    <?php
    echo $infoServeur;
    ?>
</div>

<!-- HEURES SUPP -->
<div class="row" style="padding: 15px;">
    <h3><i class="icon-clock" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Heures Supplémentaires
        à compter du <?php print date('d/m', strtotime($startWeek[ 0 ])) ?></h3>
    <hr>
    <table id="supp" class="table table-striped table-bordered table-hover" style="width: 100%">
        <thead>
        <tr>
            <th style="width=30%">Nom</th>
            <th style="width=30%">Prénom</th>
            <th style="width=10%"><?php echo 'Du ' . dateSqlToFr($startWeek[ 0 ], true, false, false) .
                    '<br/>Au ' . dateSqlToFr($endWeek[ 0 ], true, false, false); ?></th>
            <th style="width=10%"><?php echo 'Du ' . dateSqlToFr($startWeek[ 1 ], true, false, false) .
                    '<br/>Au ' . dateSqlToFr($endWeek[ 1 ], true, false, false); ?></th>
            <th style="width=10%"><?php echo 'Du ' . dateSqlToFr($startWeek[ 2 ], true, false, false) .
                    '<br/>Au ' . dateSqlToFr($endWeek[ 2 ], true, false, false); ?></th>
            <th style="width=10%"><?php echo 'Du ' . dateSqlToFr($startWeek[ 3 ], true, false, false) .
                    '<br/>Au ' . dateSqlToFr($endWeek[ 3 ], true, false, false); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php

        /** ---------------------------------------------------------------------------------------
         * On fait le calcul SEMAINE / SEMAINE
         */
        $tab = array();
        $RechercheHeuresSuppExec->bindValue(':from', $startWeek[ 0 ] . ' 00:00:00');
        $RechercheHeuresSuppExec->bindValue(':to', $endWeek[ 3 ] . ' 23:59:59');
        $RechercheHeuresSuppExec->execute();
        if ($RechercheHeuresSuppExec->rowCount() > 0) {
            while ($inter = $RechercheHeuresSuppExec->fetch(PDO::FETCH_OBJ)) {
                $tab[ $inter->idIntervenant ][ 'nom' ] = $inter->nomIntervenant;
                $tab[ $inter->idIntervenant ][ 'prenom' ] = $inter->prenomIntervenant;
                $tab[ $inter->idIntervenant ][ 'cumul' ][ 0 ] = 0;
                $tab[ $inter->idIntervenant ][ 'cumul' ][ 1 ] = 0;
                $tab[ $inter->idIntervenant ][ 'cumul' ][ 2 ] = 0;
                $tab[ $inter->idIntervenant ][ 'cumul' ][ 3 ] = 0;
            }
        }

        for ($i = 0; $i <= 3; $i++) {

            $RechercheHeuresSuppExec->bindValue(':from', $startWeek[ $i ] . ' 00:00:00');
            $RechercheHeuresSuppExec->bindValue(':to', $endWeek[ $i ] . ' 23:59:59');
            $RechercheHeuresSuppExec->execute();
            if ($RechercheHeuresSuppExec->rowCount() > 0) {

                /** -----------------------------------------------------------------------------------
                 * Liste des Heures Supp
                 * SU.idIntervenant
                 * SU.nomIntervenant
                 * SU.prenomIntervenant
                 * SI.dateDebut
                 * SI.dateFin
                 */
                while ($inter = $RechercheHeuresSuppExec->fetch(PDO::FETCH_OBJ)) {

                    $hourdiff = round((strtotime($inter->dateFin) - strtotime($inter->dateDebut)) / 3600, 2);
                    if ($hourdiff > 7) {
                        $hourdiff -= 1;
                    }
                    $tab[ $inter->idIntervenant ][ 'cumul' ][ $i ] += $hourdiff;
                }
            }
        }

        /** ---------------------------------------------------------------------------------------
         * Et maintenant, on fait le bilan
         */
        #echo '#<pre>';
        #print_r($tab);
        #echo '</pre>';

        $hasLines = false;
        $lines = '';
        foreach ($tab as $item) {
            #die();
            if (
                $item[ 'cumul' ][ 0 ] > 35 ||
                $item[ 'cumul' ][ 1 ] > 35 ||
                $item[ 'cumul' ][ 2 ] > 35 ||
                $item[ 'cumul' ][ 3 ] > 35
            ) {

                print '
                <tr>
                    <td style="width: 20%;">' . $item[ 'nom' ] . '</td>
                    <td style="width: 20%;">' . $item[ 'prenom' ] . '</td>
                    <td style="width: 10%;">' . ($item[ 'cumul' ][ 0 ] > 35 ? $item[ 'cumul' ][ 0 ] : '') . '</td>
                    <td style="width: 10%;">' . ($item[ 'cumul' ][ 1 ] > 35 ? $item[ 'cumul' ][ 1 ] : '') . '</td>
                    <td style="width: 10%;">' . ($item[ 'cumul' ][ 2 ] > 35 ? $item[ 'cumul' ][ 2 ] : '') . '</td>
                    <td style="width: 10%;">' . ($item[ 'cumul' ][ 3 ] > 35 ? $item[ 'cumul' ][ 3 ] : '') . '</td>
                </tr>';
                $hasLines = true;
            }
        }

        if (!$hasLines) {
            print '
                <tr class="nohs">
                    <td colspan="7">
                        <div class="alert alert-info" style="margin-bottom: 0px;">Pas d\'heures supplémentaires pour les 4 prochaines semaines</div>
                    </td>
                </tr>';
        }

        ?>
        </tbody>
    </table>
</div>

<!-- TITRE SEJOUR -->
<div class="row" style="padding: 15px;">
    <h3><i class="icon-calendar" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Expiration Titre
        Séjour à compter du <?php print date('d/m') ?></h3>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date</th>
            <th>Fiche</th>
        </tr>
        </thead>
        <tbody>
        <?php

        /** ---------------------------------------------------------------------------------------
         * On fait le calcul SEMAINE / SEMAINE
         */
        $RechercheTitreSejourExec->bindValue(':from', $dayNow . ' 00:00:00');
        $RechercheTitreSejourExec->bindValue(':to', $monthEnd . ' 23:59:59');
        $RechercheTitreSejourExec->execute();

        if ($RechercheTitreSejourExec->rowCount() > 0) {

            /** -----------------------------------------------------------------------------------
             * Liste des Heures Supp
             * SU.idIntervenant
             * SU.nomIntervenant
             * SU.prenomIntervenant
             * SI.dateDebut
             * SI.dateFin
             */
            while ($inter = $RechercheTitreSejourExec->fetch(PDO::FETCH_OBJ)) {

                $tr = '
                <tr>
                    <td>' . $inter->nomIntervenant . '</td>
                    <td>' . $inter->prenomIntervenant . '</td>
                    <td>' . $inter->dateFinSejour . '</td>';

                if ($inter->dateFinSejour == date('Y-m-d')) {
                    $tr .= '<td><a href="index.php?ap=3.0-Intervenant&amp;ss_m=3.3-FicheIntervenant&amp;s=' . $inter->idIntervenant . '" class="btn bg bg-red btn-xs no-margin" data-id="' . $inter->idIntervenant . '"><i class="icon-book-open"></i> Voir fiche</a></td>';
                }
                else {
                    $tr .= '<td><a href="index.php?ap=3.0-Intervenant&amp;ss_m=3.3-FicheIntervenant&amp;s=' . $inter->idIntervenant . '" class="btn bg bg-green btn-xs no-margin" data-id="1009"><i class="icon-book-open"></i> Voir fiche</a></td>';
                }
                $tr .= '
                </tr>';

                print $tr;
            }
        }
        else {
            print '
                <tr>
                    <td colspan="7">
                        <div class="alert alert-info" style="margin-bottom: 0px;">Pas d\'expiration de Titre de Séjour pour le mois suivant</div>
                    </td>
                </tr>';
        }

        /** ---------------------------------------------------------------------------------------
         * Et maintenant, on fait le bilan
         */
        $hasLines = false;
        foreach ($tab as $item) {
            if (
                $item[ 'cumul' ][ 0 ] > 35 ||
                $item[ 'cumul' ][ 1 ] > 35 ||
                $item[ 'cumul' ][ 2 ] > 35 ||
                $item[ 'cumul' ][ 3 ] > 35
            ) {
                $hasLines = true;
            }
        }

        ?>
        </tbody>
    </table>
</div>

<!-- EXPIRATION URSSAF -->
<div class="row" style="padding: 15px;">
    <h3><i class="icon-calendar" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Expiration Attestation URSSAF
        du <?php print date('d/m') ?></h3>
    <hr>
    <small>Seules les intervenantes ayant travaillé il y a moins de 6 mois figurent dans cette liste.</small><br/>&nbsp;
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date</th>
            <th>Fiche</th>
        </tr>
        </thead>
        <tbody>
        <?php

        /** ---------------------------------------------------------------------------------------
         * V2 de la fonction URSSAF
         * --> on prend TOUT LE MONDE et on traite après
         */
        $RechercheUrssafExec->execute();

        /** ---------------------------------------------------------------------------------------
         * Mois courant
         */
        if ($RechercheUrssafExec->rowCount() > 0) {

            /** -----------------------------------------------------------------------------------
             * SI PAS DE DATE
             */
            while ($inter = $RechercheUrssafExec->fetch(PDO::FETCH_OBJ)) {

                /** -----------------------------------------------------------------------------------
                 * Est-ce que cette intervenante a travaillé dans les 6 derniers mois ?
                 */
                $sqlGetLastIntervention = '
                SELECT COUNT(idIntervention) AS LeCount
                FROM su_intervention
                WHERE NOT ISNULL(FK_idContrat)
                AND FK_idIntervenant = '.$inter->idIntervenant.'
                AND dateDebut >= \''.date("Y-m-d", strtotime("-6 months")).' 00:00:00\'
                LIMIT 0, 1
                ';
                $getLastInterventionExec = DbConnexion::getInstance()->prepare($sqlGetLastIntervention);
                $getLastInterventionExec->execute();
                $getLastIntervention = $getLastInterventionExec->fetchObject();
                if ($getLastIntervention->LeCount != 0) {
                    $tr = '';
                    if ($inter->dateUrssaf == '' || $inter->dateUrssaf == '0000-00-00') {
                        $tr = buildLigneTR($inter, '');
                    }
                    else {
                        /** ---------------------------------------------------------------------------
                         * Expiration le mois en cours
                         */
                        if (expireLeMois($inter, '+1', '-1')) {
                            $tr = buildLigneTR($inter, 'danger');
                        }
                        if (expireLeMois($inter, '+2', '-1')) {
                            $tr = buildLigneTR($inter, 'warning');
                        }
                        /** ---------------------------------------------------------------------------
                         * Pour avoir 6 mois, on enlève 8 mois -> mois complet en cours + -1 pour le
                         * mois d'avant et donc -6 mois
                         */
                        if (expireLeMois($inter, '-17', '+10', 1)) {
                            $tr = buildLigneTR($inter, 'info');
                        }
                    }
                    print $tr;
                }
            }
        }
        else {
            print '
                <tr>
                    <td colspan="7">
                        <div class="alert alert-info" style="margin-bottom: 0px;">Pas d\'expiration d\'attestation URRSAF</div>
                    </td>
                </tr>';
        }

        ?>
        </tbody>
    </table>
</div>



<div class="row" style="padding: 15px;">
    <h3><i class="icon-present" style="font-size: 18px;font-weight: bold;color: #a5c229;"></i>&nbsp;Liste des
        Anniversaires intervenantes du <?php print date('d/m') ?></h3>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Tel fixe</th>
            <th>Tel portable</th>
            <th>Fiche</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $RechercheAnniversaireExc->bindValue(':dateNaissance', '%-' . date('m-d'));
        $RechercheAnniversaireExc->execute();
        if ($RechercheAnniversaireExc->rowCount() > 0) {
            while ($Info = $RechercheAnniversaireExc->fetch(PDO::FETCH_OBJ)) {
                print '
        <tr>
            <td>' . $Info->nomIntervenant . '</td>
            <td>' . $Info->prenomIntervenant . '</td>
            <td>' . $Info->telephoneIntervenant_A . '</td>
            <td>' . $Info->mobileIntervenant . '</td>
            <td><a href="index.php?ap=3.0-Intervenant&ss_m=3.3-FicheIntervenant&s=' . $Info->idIntervenant . '" class="btn bg bg-green btn-xs no-margin" data-id="1009"><i class="icon-book-open"></i> Voir fiche</a></td>
        </tr>';
            }
        }
        else {
            print '
        <tr>
            <td colspan="5"><div class="alert alert-info">Pas d\'anniversaire aujourd\'hui</div> </td>
        </tr>';
        }
        ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Nombre d'interventions / mois
                </div>
            </div>
            <div class="portlet-body">
                <div id="chartdiv" style="width: 100%;height: 500px;"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des Missions disponibles
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tableMission_1_0">
                    <thead>
                    <tr>
                        <th>Client</th>
                        <th>Libellé Campagne</th>
                        <th>Libellé Mission</th>
                        <th>Début</th>
                        <th>Fin</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
