jQuery(document).ready(function() {
   ClassDashboard.init();
   ClassDashboard.initGraph();
});

var ClassDashboard = {
    init: function(){
        this.initTableMission();

        /** ---------------------------------------------------------------------------------------
         * Correction du popup
         */
        if ($('#supp tbody tr').length > 0) {
            var tr = $('#supp tbody tr:first');
            if (tr.hasClass('nohs')) {

            } else {
                toastr.error('Il y a des intervenantes avec des heures supplémentaires', 'ATTENTION - Vérification');
            }
        }
    },

    initGraph: function () {
        AmCharts.translations[ "export" ][ "en" ][ "menu.label.save.image" ] = "Enregistrer Image...";
        AmCharts.translations[ "export" ][ "en" ][ "menu.label.save.data" ] = "Exporter Données";
        AmCharts.translations[ "export" ][ "en" ][ "menu.label.draw" ] = "Annoter";
        AmCharts.translations[ "export" ][ "en" ][ "menu.label.print" ] = "Imprimer";
        var chartContribuant = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "language": "fr",
            "theme": "light",
            "categoryField": "month",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "graphs": [
                {
                    'id':'g1',
                    "title": "Nb",
                    "valueField": "pdv",
                    "bullet": "round",
                    "lineThickness": 2,
                    'lineColor': '#bac3d0'
                }
            ],
            "chartScrollbar": {
                "autoGridCount": true,
                "graph": "g1",
                "scrollbarHeight": 40
            },
            "valueAxes": [
                {
                    "title": "Contrats / mois"
                }
            ],
            "legend": {
                "useGraphSettings": true
            },
            "titles": [
                {
                    "size": 15,
                    "text": "Progression des contrats mensuels"
                }
            ],
            //"dataProvider": JSON.stringify(dataChart),
            "dataLoader": {
                "url": "includes/functions/bdd2web/stats01.php"
            },
            "export": {
                "enabled": true
            }
        });
    },

    initTableMission: function(){

        $('#tableMission_1_0').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [4, 'desc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.1.0-listeMission.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(5)', nRow).children('button').bind('click', function(){
                    console.log($(this).attr('data-id'));
                    document.location.href='index.php?ap=4.0-Client&ss_m=4.5-FicheMission&idClient='+$(this).attr('data-client')+'&idCampagne='+$(this).attr('data-campagne')+'&s='+$(this).attr('data-id');
                })
            }
        });
        var tableWrapper = $('#tableMission_1_0_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    }
}
