var idSalarie;
jQuery(document).ready(function() {
    idSalarie = $('#zt_idSalarie_21_3').val();
    ClassFicheSalarie.init();
});

var ClassFicheSalarie = {
    init: function () {
        this.rechercheInfoSalarie();

        $('.btn-modif-salarie').bind('click', function(){
            ClassFicheSalarie.modifSalarie();
        });
    },

    modifSalarie: function(){
        $.ajax({
            async : false,
            type: "POST",
            data: {
                idSalarie: idSalarie,
                nomSalarie: $('#ztNom').val(),
                prenomSalarie: $('#ztPrenom').val(),
                immatSalarie: $('#ztImmat').val(),
                mailSalarie: $('#ztMail').val(),
                typeSalarie: $('#zsType').val(),
                typeSalarieExpensia: $('#ztType').val(),
                contratSalarie: $('#ztContrat').val(),
                passwordSalarie: $('#ztPwd').val(),
                adresseSalarieA: $('#ztAdresseA').val(),
                adresseSalarieB: $('#ztAdresseB').val(),
                cpSalarie: $('#ztCodePostalSalarie').val(),
                villeSalarie: $('#ztVilleSalarie').val(),
                mailManager: $('#ztMailManager').val(),
                boolActif: $('#zsActif').val(),
            },
            url: "includes/functions/web2bdd/func.21.3-modificationSalarie.php",
            dataType: 'json',
            success: function (data) {
                toastr.success('Les informations ont bien été enregistrées', 'Modification du salarie');
            }
        });

    },

    rechercheInfoSalarie: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idSalarie: idSalarie},
            url: "includes/functions/bdd2web/func.21.3-infoSalarie.php",
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {

                    $('#zsActif').append(
                        $('<option>')
                            .attr('value', 1)
                            .attr('selected', (data.infoSalarie.boolActif == 1) ? true : false)
                            .html('OUI')
                    );
                    $('#zsActif').append(
                        $('<option>')
                            .attr('value', 0)
                            .attr('selected', (data.infoSalarie.boolActif == 0) ? true : false)
                            .html('NON')
                    );


                    $.each(data.types, function (i, infoType) {
                        $('#zsType').append(
                            $('<option>')
                                .attr('value', infoType.idTypeUtilisateur)
                                .attr('selected', (infoType.idTypeUtilisateur == data.infoSalarie.FK_idTypeUtilisateur) ? true : false)
                                .html(infoType.libelleTypeUtilisateur)
                        );
                    });

                    $('#ztNom').val(data.infoSalarie.nomUtilisateur);
                    $('#ztPrenom').val(data.infoSalarie.prenomUtilisateur);
                    $('#ztImmat').val(data.infoSalarie.immatUtilisateur);
                    $('#ztMail').val(data.infoSalarie.mailUtilisateur);

                    $('#ztType').val(data.infoSalarie.typeIntervenantExpensia);
                    $('#ztContrat').val(data.infoSalarie.contratIntervenant);

                    $('#ztAdresseA').val(data.infoSalarie.adresseIntervenant_A);
                    $('#ztAdresseB').val(data.infoSalarie.adresseIntervenant_B);
                    $('#ztCodePostalSalarie').val(data.infoSalarie.codePostalIntervenant);
                    $('#ztVilleSalarie').val(data.infoSalarie.villeIntervenant);
                    $('#ztMailManager').val(data.infoSalarie.mailManager);
                }
            }
        });
    },

}