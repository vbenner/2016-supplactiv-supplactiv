jQuery(document).ready(function() {
    ClassSalarie.init();
});

var ClassSalarie = {
    init: function(){
        this.initSalaries();
    },


    initSalaries: function(){

        $('#tableSalarie_21_1').dataTable({

            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>rB>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            buttons: [
                {
                    text: '<i class="fa fa-clipboard font-green-seagreen" style="font-size: 16px !important"></i> Copier',
                    extend: 'copy',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-red-flamingo" style="font-size: 16px !important"></i> Csv',
                    extend: 'csv',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6]
                    },
                },
                {
                    text: '<i class="fa fa-file-excel-o font-green-seagreen" style="font-size: 16px !important"></i> Excel',
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-red-flamingo" style="font-size: 16px !important"></i> Pdf',
                    extend: 'pdf',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6]
                    },
                },
                {
                    text: '<i class="fa fa-file-pdf-o font-green-seagreen" style="font-size: 16px !important"></i> Imprimer',
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 5, 6]
                    },
                },
            ],
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                buttons: {
                    copyTitle: 'Ajouté au presse-papier',
                    copySuccess: {
                        _: '%d lignes copiées',
                        1: '1 ligne copiée'
                    }
                }
            },

            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.21.1-listeSalaries.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(6)', nRow).children('button').unbind().bind('click', function(event){

                    if ($(this).hasClass('btn')) {
                        event.preventDefault();
                        event.stopPropagation();
                        var a = document.createElement('a');
                        a.href = 'index.php?ap=21.0-GestionSalaries&ss_m=21.3-FicheSalarie&s='+$(this).attr('data-id');
                        //a.target = '_blank';
                        //document.body.appendChild(a);
                        a.click();
                        //document.body.removeChild(a);
                    }

                    /** ---------------------------------------------------------------------------
                     * Window .open
                     * @type {string}
                     */
                    if ($(this).hasClass('delete')) {
                        ClassPointDeVente.demandeSuppression($(this).attr('data-id'), $(this).attr('data-name'));
                    }
                });
            },

            fnInitComplete: function() {
                //$('#tableIntervenant_4_1 tbody tr').each(function(){
                //    $(this).find('td:eq(1)').attr('nowrap', 'nowrap');
                //});
                $('.dt-buttons').css({
                    'margin-left':'15px',
                    'padding-bottom':'5px'
                });
            }
        });
        var tableWrapper = $('#tableSalarie_21_1');
        tableWrapper.find('.dataTables_length select').select2();

    }
};
