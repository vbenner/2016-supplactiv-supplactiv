var idSalarie, tableSalarie_21_1;
jQuery(document).ready(function () {
    idSalarie = 0;
    ClassSalarie.init();
});

var ClassSalarie = {
    init: function () {
        this.rechercheInfoSalarie();

        $('.btn-modif-salarie').bind('click', function () {
            if(($('#ztNom').val()).trim() != "") {
                ClassSalarie.modifSalarie();
            }else toastr.error("Vous devez saisir un nom !", "Ajout d'un salarié");
        });
    },

    modifSalarie: function () {
        $.ajax({
            async : false,
            type: "POST",
            data: {
                idSalarie: 0,
                nomSalarie: $('#ztNom').val(),
                prenomSalarie: $('#ztPrenom').val(),
                immatSalarie: $('#ztImmat').val(),
                mailSalarie: $('#ztMail').val(),
                typeSalarie: $('#zsType').val(),
                typeSalarieExpensia: $('#ztType').val(),
                contratSalarie: $('#ztContrat').val(),
                passwordSalarie: $('#ztPwd').val(),
                adresseSalarieA: $('#ztAdresseA').val(),
                adresseSalarieB: $('#ztAdresseB').val(),
                cpSalarie: $('#ztCodePostalSalarie').val(),
                villeSalarie: $('#ztVilleSalarie').val(),
                mailManager: $('#ztMailManager').val(),
            },
            url: "includes/functions/web2bdd/func.21.3-modificationSalarie.php",
            dataType: 'json',
            success: function (data) {
                if(data.result != 0){
                    document.location.href = 'index.php?ap=21.0-GestionSalaries&ss_m=21.1-ListeSalaries';
                    toastr.success('Les informations ont bien été enregistrées', 'Ajout d\'un Salarié');
                }
                else {
                    toastr.error('Vérifier les données', 'Ajout d\'un Salarié');
                }
            }
        });

    },

    rechercheInfoSalarie: function () {

        $.ajax({
            async : false,
            type: "POST",
            data: {idSalarie: idSalarie},
            url: "includes/functions/bdd2web/func.21.3-infoSalarie.php",
            dataType: 'json',
            success: function (data) {
                //$('#zsType').append($('<option>'));
                $.each(data.types, function (i, infoType) {
                    $('#zsType').append(
                        $('<option>')
                            .attr('value', infoType.idTypeUtilisateur)
                            .html(infoType.libelleTypeUtilisateur)
                    );
                });
                $("#zsType").prepend("<option value='' selected='selected'></option>");

            }
        });
    }
}