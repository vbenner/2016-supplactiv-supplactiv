<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Formulaire d'ajout d'un salarie
                </div>
            </div>
            <div class="portlet-body form">

                <div class="form-horizontal form-bordered form-row-stripped" style="margin-top: -20px !important;">
                    <div class="form-group">
                        <label class="control-label col-md-3">Nom :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztNom"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Prénom :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztPrenom"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Matricule EBP :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztImmat"/>
                            </div>
                        </div>

                        <label class="control-label col-md-3">Mail :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztMail"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Mail manager :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <input type="text" class="form-control" id="ztMailManager"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Type :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="zsType">

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Type Salarie (PayId2 - Expensia) :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="ztType">
                                    <option value="Siege">Siege</option>
                                    <option value="Force de vente terrain">Force de vente terrain</option>
                                </select>
                            </div>
                        </div>

                        <label class="control-label col-md-3">Type Contrat (PayId3 - Expensia) :</label>
                        <div class="col-md-3">
                            <div class="input-icon left">
                                <i class="fa fa-warning tooltips tooltips" data-container="body"
                                   data-original-title="Obligatoire"></i>
                                <select class="form-control" id="ztContrat">
                                    <option value="CDD">CDD</option>
                                    <option value="CDI">CDI</option>
                                    <option value="STAGIAIRE">STAGIAIRE</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Mot de passe:</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztPwd"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse A :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseA"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Adresse B :</label>

                        <div class="col-md-9">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztAdresseB"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Code Postal :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztCodePostalSalarie"/>
                            </div>
                        </div>
                        <label class="control-label col-md-3">Ville :</label>

                        <div class="col-md-3">
                            <div class="input-icon left">
                                <input type="text" class="form-control" id="ztVilleSalarie"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button class="btn blue bg-blue-su pull-right btn-modif-salarie"><i
                                    class="icon-check"></i> Ajouter le Salarie
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>