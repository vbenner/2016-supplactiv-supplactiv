<div class="well">
    <a class="btn bg bg-green pull-left" target="_blank" href="includes/functions/datatable/RegistrePersonnel.csv">Télécharger au format CSV (immédiat)</a>&nbsp;&nbsp;
    <a class="btn bg bg-blue pull-right" target="_blank" href="download/func.17.4-exportRegistrePersonnel.php">Télécharger au format XLSX (+ long)</a>
    <div style="clear: both"></div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableRegistrePersonnel_17_4">
    <thead>
    <tr>
        <th>Intervenant</th>
        <th>Sexe</th>
        <th>Nationalité</th>
        <th>Date naissance</th>
        <th>Date entrée</th>
        <th>Date sortie</th>
        <th>Type de travail</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
