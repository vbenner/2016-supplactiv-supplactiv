<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Période du contrat
                </div>
            </div>
            <div class="portlet-body form">
                <div class=" form-horizontal form-bordered form-row-stripped">
                    <div class="form-body">

                        <div class="form-group input-daterange">
                            <label class="control-label col-md-2">Date début :</label>
                            <div class="col-md-4">
                                <input id="dateRechercheD_17_5" class="form-control form-recherche" type="text" placeholder="" name="start" style="text-align: left" value="" onchange="ClassEtiquette.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                            <label class="control-label col-md-2">Date fin :</label>
                            <div class="col-md-4">
                                <input id="dateRechercheF_17_5" class="form-control form-recherche" type="text" placeholder="" name="end" style="text-align: left" value="" onchange="ClassEtiquette.saveSearchToSession()"/>
                                <span class="help-block"> Obligatoire </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn bg bg-blue btn-sel-all">Tout sélectionner</button>
                        <button class="btn bg bg-blue btn-unsel-all">Tout dé-sélectionner</button>
                        <button class="btn bg bg-green pull-right btn-export">Télécharger les étiquettes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover" id="tableEtiquette_17_5">
    <thead>
    <tr>
        <th>ID</th>
        <th>Nom</th>
        <th>Pr&eacute;nom</th>
        <th>Code Postal</th>
        <th>Ville</th>
        <th>Tel. Fixe</th>
        <th>Tel. Mobile</th>
        <th>D&eacute;partement</th>
        <th>Sel.</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>