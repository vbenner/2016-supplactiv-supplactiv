var tableEtiquette_17_5;
var dateRechercheD_17_5, dateRechercheF_17_5;
jQuery(document).ready(function() {
    ClassEtiquette.init();
});

var ClassEtiquette = {
    init: function(){
        this.rechercheSessionModule();
        this.initTableEtiquette();
        this.initDateRecherche();

        /** Tout selectionner */
        $('.btn-sel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", true);
            });
        });

        /** Tout deselectionner */
        $('.btn-unsel-all').bind('click', function () {
            $(".chk").each(function(){
                $(this).prop("checked", false);
            });
        });

        /** Exporter les DUE */
        $('.btn-export').bind('click', function (){

            /** On desactive le bouton */
            $(".btn-export").hide();

            var listeIntervenant = new Array();
            $(".chk").each(function(){
                if($(this).is(":checked")){
                    listeIntervenant.push($(this).attr('data-id'));
                }
            });


            /** On export uniquement s'il y a des intervenants sélectionnés */
            if(listeIntervenant.length > 0){

                toastr.info("Veuillez patienter, cr&eacute;ation du fichier en cours..", "Exportation des étiquettes");
                $.ajax({
                    async:false,
                    url: "includes/functions/web2session/func.majSession.php",
                    type: "POST",
                    data: {
                        listeIntervenant_17_5 : listeIntervenant
                    },
                    dataType: 'json'
                }).done(function( data ) {
                    document.location.href = 'download/func.17.5-exportEtiquettePersonnel.php';
                    $(".btn-export").show();

                });

            }else {
                $(".btn-export").show();
                toastr.error("Impossible de cr&eacute;er le fichier d'export, vous devez s&eacute;lectionner au moins 1 intervenant !", "ERREUR - Exportation des étiquettes");
            }

        });

    },


    saveSearchToSession: function(){
        if(dateRechercheF_17_5 != $('#dateRechercheF_17_5').val() || dateRechercheD_17_5 != $('#dateRechercheD_17_5').val()){
            dateRechercheF_17_5 = $('#dateRechercheF_17_5').val();
            dateRechercheD_17_5 = $('#dateRechercheD_17_5').val();


            $.ajax({
                async:false,
                url: "includes/functions/web2session/func.majSession.php",
                type: "POST",
                data: {
                    dateRechercheD_17_5 : dateRechercheD_17_5,
                    dateRechercheF_17_5 : dateRechercheF_17_5
                },
                dataType: 'json'
            }).done(function( data ) {
                tableEtiquette_17_5.api().ajax.reload();
            });
        }
    },


    rechercheSessionModule: function(){
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: { idModule : '17_5', dateRechercheD_17_5 : '', dateRechercheF_17_5 : ''},
            dataType: "json"
        }).done(function( data ) {
            $('#dateRechercheD_17_5').val(data.sessions.dateRechercheD_17_5);
            $('#dateRechercheF_17_5').val(data.sessions.dateRechercheF_17_5);

        });
    },

    initTableEtiquette: function(){

        tableEtiquette_17_5 = $('#tableEtiquette_17_5').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [ -1],
                [ "Tout Afficher"]
            ],
            "pageLength": -1,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.17.5-listeEtiquette.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            }
        });
        var tableWrapper = $('#tableEtiquette_17_5_wrapper');
        tableWrapper.find('.dataTables_length select').select2();

    },

    initDateRecherche: function(){
        $('.input-daterange').datepicker();
    }
}
