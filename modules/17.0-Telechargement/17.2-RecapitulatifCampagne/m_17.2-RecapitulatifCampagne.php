<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" action="#">
                    <div class="form-body">
                        <div class="form-group">


                            <div class="col-md-12">
                                <label class="control-label">Campagne :</label>
                                <select id="zsCampagne_17_2" class="form-control select2">

                                </select>
                            </div>

                        </div>
                        <div class="form-actions">
                            <a class="btn bg bg-blue pull-right btn-download-planning" target="_blank" href="download/func.17.2-exportRecapitulatifCampagne.php">Télécharger le récapitulatif campagne</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>