var idCampagne;

jQuery(document).ready(function() {
    ClassRecapitulatif.init();
});

var ClassRecapitulatif = {
    init: function () {

        // Recherche des sessions
        this.rechercheSessionModule();

    },


    enregistrementRecherche: function () {
        idCampagne = $('#zsCampagne_17_2').val();

        $.ajax({
            async:false,
            type: "POST",
            data: {idCampagne_17_2: idCampagne},
            url: "includes/functions/web2session/func.majSession.php",
            dataType: 'json',
            success: function (data) {

            }
        });

    },

    rechercheSessionModule: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: {idModule: '17_2', idCampagne_17_2: 'ALL'},
            dataType: "json"
        }).done(function (data) {

            // Initialisation des variables
            idCampagne = data.sessions.idCampagne_17_2;

            // Recherche des filtres
            ClassRecapitulatif.rechercheFiltre();


        });
    },

    rechercheFiltre: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.17.2-rechercheFiltreRecapitulatif.php",
            dataType: 'json',
            success: function (data) {


                $('#zsCampagne_17_2').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir une campagne')
                        .prop('selected', ('ALL' == idCampagne) ? true : false)
                );

                $.each(data.campagnes, function (i, InfoCampagne) {
                    $('#zsCampagne_17_2').append(
                        $('<option>')
                            .attr('value', InfoCampagne.idCampagne)
                            .html(InfoCampagne.libelleCampagne)
                            .prop('selected', (InfoCampagne.idCampagne == idCampagne) ? true : false)
                    );
                });


                $('#zsCampagne_17_2').select2().bind('change', function () {
                    ClassRecapitulatif.enregistrementRecherche();

                });
            }
        });
    }
}