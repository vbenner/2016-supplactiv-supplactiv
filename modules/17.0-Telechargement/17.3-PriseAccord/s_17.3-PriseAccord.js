var tablePriseAccord_17_3;
jQuery(document).ready(function() {
    ClassPriseAccord.init();
});

var ClassPriseAccord = {
    init: function(){
        this.initTablePriseAccord();
    },

    initTablePriseAccord: function(){

        tablePriseAccord_17_3 = $('#tablePriseAccord_17_3').dataTable({
            "columnDefs": [{
                "orderable": true,
                "targets": [0]
            }],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [50, 100, 500, -1],
                [50, 100, 500, "Tout"]
            ],
            "pageLength": 500,
            bProcessing: true,
            bDefferRender: true,
            "bRetrieve": true,
            sAjaxSource: "includes/functions/datatable/func.17.3-listePriseAccord.php",
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0)', nRow).children('button').bind('click', function(){
                    document.location.href='index.php?ap=4.0-Client&ss_m=4.3-FicheClient&s='+$(this).attr('data-id');
                })
            }
        });
        var tableWrapper = $('#tableClient_4_1_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
    },

    telechargementPriseAccord: function(idMission, idPdv){


        $.ajax({
            async:false,
            type: "POST",
            data: {idMission_17_3: idMission, idPdv_17_3: idPdv},
            url: "includes/functions/web2session/func.majSession.php",
            dataType: 'json',
            success: function (data) {
                window.location.href = 'download/func.17.3-exportPriseAccord.php';
            }
        });
    }
}
