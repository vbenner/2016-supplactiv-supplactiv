<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Liste des prise d'accords disponibles
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tablePriseAccord_17_3">
                    <thead>
                    <tr>
                        <th>Libellé Campagne</th>
                        <th>Libellé Mission</th>
                        <th>Point de vente</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>