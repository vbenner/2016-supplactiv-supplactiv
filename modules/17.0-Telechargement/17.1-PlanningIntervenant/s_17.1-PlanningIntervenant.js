var idCampagne, idMission, anneeRecherche, moisRecherche;
var autoRecherche, listeCampagne;

jQuery(document).ready(function() {
    ClassPlanning.init();
});

var ClassPlanning = {
    init: function () {
        this.rechercheSessionModule();
    },

    enregistrementRecherche: function () {
        idCampagne = $('#zsCampagne_17_1').val();
        idIntervenant = $('#zsIntervenant_17_1').val();
        anneeRecherche = $('#zsAnnee_17_1').val();
        idMission = $('#zsMission_17_1').val();
        moisRecherche = $('#zsMois_17_1').val();
        autoRecherche = $('#lstAuto_17_1').val();

        $.ajax({
            async:false,
            type: "POST",
            data: {idCampagne_17_1: idCampagne, idIntervenant_17_1: idIntervenant,
                idMission_17_1 : idMission, dateRecherche_17_1 : anneeRecherche,
                moisRecherche_17_1 : moisRecherche,
                autoRecherche_17_1 : autoRecherche
            },
            url: "includes/functions/web2session/func.majSession.php",
            dataType: 'json',
            success: function (data) {

            }
        });

    },

    rechercheSessionModule: function () {
        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.rechercheSessionModule.php",
            type: "POST",
            data: {idModule: '17_1', idIntervenant_17_1: 'ALL',
                idCampagne_17_1: 'ALL', idMission_17_1 : 'ALL',
                dateRecherche_17_1 : 'ALL', moisRecherche_17_1 : 'ALL',
                autoRecherche_17_1 : 'ALL'
            },
            dataType: "json"
        }).done(function (data) {

            // Initialisation des variables
            idIntervenant = data.sessions.idIntervenant_17_1;
            idCampagne = data.sessions.idCampagne_17_1;
            idMission = data.sessions.idMission_17_1;
            anneeRecherche = data.sessions.dateRecherche_17_1;
            moisRecherche = data.sessions.moisRecherche_17_1;
            autoRecherche = data.sessions.autoRecherche_17_1;

            // Recherche des filtres
            ClassPlanning.rechercheFiltre();
        });
    },

    rechercheCampagneAnnee: function(){

        $("#zsCampagne_17_1").select2("destroy");
        $('#zsCampagne_17_1').html(
            $('<option>')
                .attr('value', 'ALL')
                .html('Choisir une campagne')
        );
        $.each(listeCampagne, function (i, InfoCampagne) {
            if(anneeRecherche == InfoCampagne.anneeCampagne)
                $('#zsCampagne_17_1').append(
                    $('<option>')
                        .attr('value', InfoCampagne.idCampagne)
                        .html(InfoCampagne.libelleCampagne)
                        .prop('selected', (InfoCampagne.idCampagne == idCampagne) ? true : false)
                );
        });
        $('#zsCampagne_17_1').select2().unbind().bind('change', function () {
            ClassPlanning.enregistrementRecherche();
            if(idCampagne != 'ALL'){
                ClassPlanning.rechercheMission(idCampagne);
            }
        });

    },

    rechercheFiltre: function(){
        $.ajax({
            async:false,
            type: "POST",
            data: {},
            url: "includes/functions/bdd2web/func.17.1-rechercheFiltrePlanning.php",
            dataType: 'json',
            success: function (data) {

                anneeRecherche = (anneeRecherche == null) ? data.dates_d : anneeRecherche;

                $('#zsIntervenant_17_1').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir un intervenant')
                        .prop('selected', ('ALL' == idIntervenant) ? true : false)
                );

                $.each(data.intervenants, function (i, InfoIntervenant) {
                    if(InfoIntervenant.idIntervenant != 1)
                    $('#zsIntervenant_17_1').append(
                        $('<option>')
                            .attr('value', InfoIntervenant.idIntervenant)
                            .html(InfoIntervenant.idIntervenant+' - '+InfoIntervenant.nomIntervenant+' '+InfoIntervenant.prenomIntervenant)
                            .prop('selected', (InfoIntervenant.idIntervenant == idIntervenant) ? true : false)
                    );
                });

                $('#zsCampagne_17_1').html(
                    $('<option>')
                        .attr('value', 'ALL')
                        .html('Choisir une campagne')
                        .prop('selected', ('ALL' == idCampagne) ? true : false)
                );

                listeCampagne = data.campagnes;

                $.each(data.campagnes, function (i, InfoCampagne) {
                    if(anneeRecherche == InfoCampagne.anneeCampagne)
                    $('#zsCampagne_17_1').append(
                        $('<option>')
                            .attr('value', InfoCampagne.idCampagne)
                            .html(InfoCampagne.libelleCampagne)
                            .prop('selected', (InfoCampagne.idCampagne == idCampagne) ? true : false)
                    );
                });


                $('#zsAnnee_17_1').empty();


                $.each(data.dates, function (i, InfoAnnee) {
                    $('#zsAnnee_17_1').append(
                        $('<option>')
                            .attr('value', InfoAnnee)
                            .html(InfoAnnee)
                            .prop('selected', (InfoAnnee == anneeRecherche) ? true : false)
                    );
                });

                $('#zsMois_17_1').empty();
                $('#zsMois_17_1').append($('<option>').attr('value', 'ALL').html('TOUT AFFICHER').prop('selected', ('ALL' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '01').html('01 - JANVIER').prop('selected', ('01' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '02').html('02 - FEVRIER').prop('selected', ('02' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '03').html('03 - MARS').prop('selected', ('03' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '04').html('04 - AVRIL').prop('selected', ('04' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '05').html('05 - MAI').prop('selected', ('05' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '06').html('06 - JUIN').prop('selected', ('06' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '07').html('07 - JUILLET').prop('selected', ('07' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '08').html('08 - AOUT').prop('selected', ('08' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '09').html('09 - SEPTEMBRE').prop('selected', ('09' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '10').html('10 - OCTOBRE').prop('selected', ('10' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '11').html('11 - NOVEMBRE').prop('selected', ('11' == moisRecherche) ? true : false));
                $('#zsMois_17_1').append($('<option>').attr('value', '12').html('12 - DECEMBRE').prop('selected', ('12' == moisRecherche) ? true : false));

                $('#zsMois_17_1').select2().bind('change', function () {
                    ClassPlanning.enregistrementRecherche();
                });

                if(idCampagne != 'ALL'){
                    ClassPlanning.rechercheMission(idCampagne);
                }

                $('#zsIntervenant_17_1').select2().bind('change', function () {
                    ClassPlanning.enregistrementRecherche();
                });

                $('#zsCampagne_17_1').select2().bind('change', function () {
                    ClassPlanning.enregistrementRecherche();
                    if(idCampagne != 'ALL'){
                        ClassPlanning.rechercheMission(idCampagne);
                    }
                });

                $('#zsAnnee_17_1').select2().bind('change', function () {
                    ClassPlanning.enregistrementRecherche();
                    ClassPlanning.rechercheCampagneAnnee();
                });

                $('#lstAuto_17_1').select2().bind('change', function () {
                    ClassPlanning.enregistrementRecherche();
                });

            }
        });
    },

    rechercheMission: function(idCampagne){


        $.ajax({
            async:false,
            url: "includes/functions/bdd2web/func.17.1-listeMissionCampagne.php",
            type: "GET",
            data: {idCampagne : idCampagne},
            dataType: "json"
        }).done(function (data) {

            $("#zsMission_17_1").select2("destroy");
            $('#zsMission_17_1').html(
                $('<option>')
                    .attr('value', 'ALL')
                    .html('Choisir une mission')
            );
            $.each(data.missions, function (i, InfoMission) {
                $('#zsMission_17_1').append(
                    $('<option>')
                        .attr('value', InfoMission.idMission)
                        .html(InfoMission.idMission+' - '+InfoMission.libelleMission)
                        .prop('selected', (InfoMission.idMission == idMission) ? true : false)
                );
            });

            $('#zsMission_17_1').select2().bind('change', function () {
                ClassPlanning.enregistrementRecherche();
            });
        });
    }
};