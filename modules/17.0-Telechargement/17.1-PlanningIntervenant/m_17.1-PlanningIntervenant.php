<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Outil de recherche
                </div>
            </div>
            <div class="portlet-body form">
                <form class=" form-horizontal form-bordered form-row-stripped" action="#">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Année :</label>
                                <select id="zsAnnee_17_1" class="form-control select2">

                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Intervenant :</label>
                                <select id="zsIntervenant_17_1" class="form-control select2">

                                </select>
                            </div>

                            <div class="col-md-3">
                                <label class="control-label">Campagne :</label>
                                <select id="zsCampagne_17_1" class="form-control select2">

                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">Mission :</label>
                                <select id="zsMission_17_1" class="form-control select2">

                                </select>
                            </div>

                            <div class="col-md-2">
                                <label class="control-label">Mois :</label>
                                <select id="zsMois_17_1" class="form-control select2">

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label">Auto :</label>
                                <select id="lstAuto_17_1" name="lstAuto_17_1" class="form-control select2">
                                    <option value="ALL">TOUT</option>
                                    <option value="0">NON</option>
                                    <option value="1">OUI</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <a id="aRef" class="btn bg bg-blue pull-right btn-download-planning" target="_blank"
                               href="download/func.17.1-exportPlanningIntervenant.php">Télécharger le planning</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>