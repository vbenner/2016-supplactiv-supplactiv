

jQuery(document).ready(function() {
    $('#tableBILAN_18_0').dataTable({
        "columnDefs": [{
            "orderable": true,
            "targets": [0]
        }],
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [50, 100, 500, -1],
            [50, 100, 500, "Tout"]
        ]});
});