<?php
ini_set('display_errors','on');

/** Requete sql */
require_once dirname(__FILE__) . '/../../includes/queries/queries.bdd2web.php';
require_once dirname(__FILE__) . '/../../includes/queries/queries.web2bdd.php';

if(filter_has_var(INPUT_GET, 'reload')) {
    $sqlAjoutBilan = '
    INSERT INTO su_bilan(FK_idIntervenant, dateBilan, campagne, nbIntervention, nbHeure, montantBrut, montantRepas, montantKm, montantAutres)
    VALUES(:idIntervenant, :dateBilan, :campagne, :nbIntervention, :nbHeure, :montantBrut, :montantRepas, :montantKm, :montantAutres)
    ON DUPLICATE KEY UPDATE
      campagne = :campagne,
      nbIntervention = :nbIntervention,
      nbHeure = :nbHeure,
      montantBrut = :montantBrut,
      montantRepas = :montantRepas,
      montantKm = :montantKm,
      montantAutres = :montantAutres';
    $AjoutBilanExc = DbConnexion::getInstance()->prepare($sqlAjoutBilan);

    $dateRechercheMois = '2014-12-01';

    $dernierJour = new DateTime($dateRechercheMois . ' 00:00:00');
    $dernierJour->modify('last day of this month');

    $ListeIntervenantContratCDD = array();
    $arOutput = array('aaData' => array(), 'jour' => array());

    /** Recherche des intervenantes */
    $RechercheIntervenantEvpExc->bindValue(':dateDebut', $dateRechercheMois . ' 00:00:00');
    $RechercheIntervenantEvpExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheIntervenantEvpExc->execute();
    while ($InfoEvp = $RechercheIntervenantEvpExc->fetch(PDO::FETCH_OBJ)) {

        /** Initialisation des variables */
        $boolAutorisationHS = false;
        $nbSecondeSup = 0;
        $listeHeureIntervention = array();
        $listeFraisRepas = array();
        $listeFraisTel = array();
        $nbSecondeTotal = 0;
        $nbSecondeHS_25 = 0;
        $nbSecondeHS_50 = 0;
        $arOutput['jour'][$InfoEvp->idIntervenant] = array();
        /**  On parcours les jours du mois */
        for ($idJour = 1; $idJour <= $dernierJour->format('d'); $idJour++) {


            $jourRecherche = ($idJour < 10) ? '0' . $idJour : $idJour;
            /** @var $jourSel Datetime du jour en cours */
            $jourSel = new DateTime($dernierJour->format('Y-m-') . $jourRecherche . ' 23:59:59');

            $boolAffichageHS = false;
            /**
             * Le premier jour du mois n'est pas a Lundi
             * @todo On doit rechercher les heures pour la semaine complete
             */
            if ($idJour == 1 && $jourSel->format('w') > 1) {

                /** On reinitialise les HS */
                $nbSecondeSup = 0;

                /** Recherche des HS */
                $RechercheHSIntervenantExc->bindValue(':dateHS', $dateRechercheMois);
                $RechercheHSIntervenantExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
                $RechercheHSIntervenantExc->execute();
                while ($InfoHS = $RechercheHSIntervenantExc->fetch(PDO::FETCH_OBJ)) {
                    $nbSecondeSup += $InfoHS->nbSeconde;
                    $boolAutorisationHS = true;
                }
            }

            /** Autorisation des heures sup pour la semaine */
            if ($jourSel->format('w') == 1) {
                $boolAutorisationHS = true;
            }

            /** Il s'agit d'un dimanche on arrete de compter les HS */
            if ($jourSel->format('w') == 0 && $boolAutorisationHS) {
                $boolAutorisationHS = false;
                $boolAffichageHS = true;
            }

            /** On recherche les infos sur les interventions */
            $nbSecondeJour = 0;
            $RechercheJourInterventionEvpExc->bindValue(':dateDebut', $jourSel->format('Y-m-d') . ' 00:00:00');
            $RechercheJourInterventionEvpExc->bindValue(':dateFin', $jourSel->format('Y-m-d') . ' 23:59:59');
            $RechercheJourInterventionEvpExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
            $RechercheJourInterventionEvpExc->execute();
            while ($InfoIntervention = $RechercheJourInterventionEvpExc->fetch(PDO::FETCH_OBJ)) {

                array_push($arOutput['jour'][$InfoEvp->idIntervenant], array('jour' => $dernierJour->format('Y-m-') . $jourRecherche . ' 23:59:59', 'frais' => $InfoIntervention->fraisRepas));

                /** On stock le nombre de seconde de l'intervention */
                $nbSecondeJour += $InfoIntervention->nbSeconde;
                if (!isset($listeHeureIntervention[$jourSel->format('Y-m-d')])) {
                    $listeHeureIntervention[$jourSel->format('Y-m-d')] = array();
                }
                if (!isset($listeHeureIntervention[$jourSel->format('Y-m-d')][$InfoIntervention->salaireBrut])) {
                    $listeHeureIntervention[$jourSel->format('Y-m-d')][$InfoIntervention->salaireBrut] = 0;
                }
                $listeHeureIntervention[$jourSel->format('Y-m-d')][$InfoIntervention->salaireBrut] += $InfoIntervention->nbSeconde;
                if (!is_null($InfoIntervention->fraisRepas)) {
                    $listeFraisRepas[$jourSel->format('Y-m-d')] = $InfoIntervention->fraisRepas;
                }
                if (!is_null($InfoIntervention->fraisTelephone)) {
                    $listeFraisTel[$jourSel->format('Y-m-d')] = $InfoIntervention->fraisTelephone;
                }
            }

            /** On limite le nombre de seconde a 25200 */
            if ($nbSecondeJour >= 28800) {
                $nbSecondeTotal += $nbSecondeJour - 3600;
                $nbSecondeJour = $nbSecondeJour - 3600;
            } else $nbSecondeTotal += $nbSecondeJour;

            $nbSecondeSup += ($boolAutorisationHS) ? $nbSecondeJour : 0;

            if ($boolAffichageHS && $nbSecondeSup > 126000) {
                $TotalSupplementaire = $nbSecondeSup - 126000;
                if ($TotalSupplementaire > 28800) {
                    $nbSecondeHS_25 += 28800;
                    $nbSecondeHS_50 += ($TotalSupplementaire - 28800);
                } else $nbSecondeHS_25 += $TotalSupplementaire;
            }

            $nbSecondeSup = ($jourSel->format('w') == 0) ? 0 : $nbSecondeSup;
        }

        /** Montant repas + Commentaire */
        $montantRepas = 0;
        $commentaireRepas = 0;
        $listeTarifRepas = array();
        $nbRepasFinal = 0;
        foreach ($listeFraisRepas as $dateRepas => $montant) {
            if ($montant > 0) {
                $montantRepas += $montant;
                if (!isset($listeTarifRepas[$montant])) $listeTarifRepas[$montant] = 0;
                $listeTarifRepas[$montant]++;
                $nbRepasFinal++;
            }
        }

        /** Montant telephone */
        $montantTel = 0;
        foreach ($listeFraisTel as $dateTel => $montant) {
            $montantTel += $montant;
        }


        /** Recherche des frais d'intervention */
        $RechercheFraisInterventionExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
        $RechercheFraisInterventionExc->bindValue(':dateFrais', $dateRechercheMois);
        $RechercheFraisInterventionExc->execute();
        $infoFraisIntervention = $RechercheFraisInterventionExc->fetch(PDO::FETCH_OBJ);


        /** Recherche des frais KM */
        $listeKmIntervenant = array();
        $nbKmTotal = 0;
        $montantKm = 0;
        $commentaireKm = '';
        $RechercheKmIntervenantExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
        $RechercheKmIntervenantExc->bindValue(':dateKilometre', $dateRechercheMois);
        $RechercheKmIntervenantExc->execute();
        while ($InfoKm = $RechercheKmIntervenantExc->fetch(PDO::FETCH_OBJ)) {
            if (!isset($listeKmIntervenant[$InfoKm->tarifKilometre])) $listeKmIntervenant[$InfoKm->tarifKilometre] = 0;
            $listeKmIntervenant[$InfoKm->tarifKilometre] += $InfoKm->totalKilometre;
            $nbKmTotal += $InfoKm->totalKilometre;
            $montantKm += $InfoKm->tarifKilometre * $InfoKm->totalKilometre;
        }


        /** Recherche des interventions */
        $nbIntervention = 0;
        $commentaireIntervention = '';
        $typeIntervention = array();
        $RechercheListeInterventionMoisExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
        $RechercheListeInterventionMoisExc->bindValue(':dateDebut', $dateRechercheMois . ' 00:00:00');
        $RechercheListeInterventionMoisExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
        $RechercheListeInterventionMoisExc->execute();
        while ($InfoIntervention = $RechercheListeInterventionMoisExc->fetch(PDO::FETCH_OBJ)) {
            $nbIntervention += $InfoIntervention->nbIntervention;
            $typeIntervention[$InfoIntervention->libelleMission] = $InfoIntervention->nbIntervention;
        }

        $Remun_CIDD = 0;
        $Indem_F = 0;
        $Nb_Heure_CIDD = 0;
        $Nb_Inter_CIDD = 0;

        /** Recherche des contrats CIDD */
        $RechercheInterventionCiddExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
        $RechercheInterventionCiddExc->bindValue(':dateContrat', $dateRechercheMois, PDO::PARAM_STR);
        $RechercheInterventionCiddExc->execute();
        while ($InfoContrat = $RechercheInterventionCiddExc->fetch(PDO::FETCH_OBJ)) {

            /** @var $Liste_PDV Liste des points de vente a visiter */
            $listePDV = preg_split('/_/', $InfoContrat->listePDV);
            foreach ($listePDV as $idPdv) {
                if (!empty($idPdv)) {
                    $Nb_Inter_CIDD++;
                    @$typeIntervention[$InfoContrat->libelleMission]++;
                }
            }

            $Remun_CIDD += $InfoContrat->remunerationBrute * $InfoContrat->dureeMensuelle;
            $Nb_Heure_CIDD += $InfoContrat->dureeMensuelle;
            $Indem_F += $InfoContrat->indemniteForfaitaire * ($InfoContrat->dureeMensuelle);
        }

        $nbIntervention += $Nb_Heure_CIDD;
        $commentaireIntervention = '';
        foreach ($typeIntervention as $libelleMission => $nb) {
            $commentaireIntervention .= (empty($commentaireIntervention)) ? $libelleMission . ' : ' . $nb . ' intervention(s)' : ', ' . $libelleMission . ' : ' . $nb . ' intervention(s)';
        }


        /** On calcul la remuneration de l'intervenant */
        $nbHeureTotal = 0;
        $montantTotal = 0;
        $nbHeureTotal2 = 0;
        foreach ($listeHeureIntervention as $jour => $duree) {
            foreach ($duree as $tarif => $nbInter) {
                if ($nbInter >= 28800) {
                    $nbHeureTotal += $nbInter - 3600;
                    $montantTotal += (($nbInter - 3600) * $tarif);
                } else {
                    $nbHeureTotal += $nbInter;
                    $montantTotal += ($nbInter * $tarif);
                }
            }
        }

        /** Formatage du nombre d'heure et du montant */
        $nbHeureTotal = round(($nbHeureTotal / 3600), 2) + $Nb_Heure_CIDD;
        $montantTotal = ($montantTotal / 3600) + $Remun_CIDD;

        /** Conversion des secondes en heures */
        $nbHS_25 = round(($nbSecondeHS_25 / 3600), 2);
        $nbHS_50 = round(($nbSecondeHS_50 / 3600), 2);

        $nbHeureNormal = $nbHeureTotal - $nbHS_25 - $nbHS_50 + $Nb_Heure_CIDD;
        $montantTotalNormal = round((($nbHeureNormal / $nbHeureTotal) * round($montantTotal, 2)), 2) + $Remun_CIDD;


        $arOutput['aaData'][] = array(

            'idIntervenant' => $InfoEvp->idIntervenant . ' - ' . $InfoEvp->nomIntervenant . ' ' . $InfoEvp->prenomIntervenant,
            'campagnes' => $commentaireIntervention,
            'nbInter' => $nbIntervention,
            'nbHeure' => $nbHeureTotal,
            'montantBrut' => $montantTotal,
            'montantRepas' => $montantRepas,
            'montantKM' => $montantKm,
            'montantAutres' => ($infoFraisIntervention->montantRepriseAccompte +
                $infoFraisIntervention->montantPrimeObjectif +
                $infoFraisIntervention->montantPrimeRDV +
                $infoFraisIntervention->montantIndemniteFormation +
                $infoFraisIntervention->montantFraisJustificatif)
        );


        $AjoutBilanExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
        $AjoutBilanExc->bindValue(':dateBilan', $dateRechercheMois);
        $AjoutBilanExc->bindValue(':campagne', json_encode($typeIntervention));
        $AjoutBilanExc->bindValue(':nbIntervention', $nbIntervention);
        $AjoutBilanExc->bindValue(':nbHeure', $nbHeureTotal);
        $AjoutBilanExc->bindValue(':montantBrut', $montantTotal);
        $AjoutBilanExc->bindValue(':montantRepas', $montantRepas);
        $AjoutBilanExc->bindValue(':montantKm', $montantKm);
        $AjoutBilanExc->bindValue(':montantAutres', ($infoFraisIntervention->montantRepriseAccompte +
            $infoFraisIntervention->montantPrimeObjectif +
            $infoFraisIntervention->montantPrimeRDV +
            $infoFraisIntervention->montantIndemniteFormation +
            $infoFraisIntervention->montantFraisJustificatif));
        $AjoutBilanExc->execute();

        $ListeIntervenantContratCDD[$InfoEvp->idIntervenant] = true;

    }
}

$sqlRechercheInfoBilan = '
SELECT *
FROM su_bilan
  LEFT JOIN su_intervenant ON su_intervenant.idIntervenant = su_bilan.FK_idIntervenant
WHERE dateBilan BETWEEN :debut AND :fin';
$RechercheInfoBilanExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoBilan);


$infoIntervenant = array(); $campagneIntervenant = array();


$RechercheInfoBilanExc->bindValue(':debut', '2014-01-01');
$RechercheInfoBilanExc->bindValue(':fin', '2014-12-01');
$RechercheInfoBilanExc->execute();
while($InfoBilan = $RechercheInfoBilanExc->fetch(PDO::FETCH_OBJ)){
    if(!isset($infoIntervenant[$InfoBilan->FK_idIntervenant])){
        $infoIntervenant[$InfoBilan->FK_idIntervenant] = array(
            'nom' => $InfoBilan->nomIntervenant.' '.$InfoBilan->prenomIntervenant,
            'nbInter' => 0,
            'nbHeures' => 0,
            'montantBrut' => 0,
            'montantRepas' => 0,
            'montantKm' => 0,
            'montantAutres' => 0
        );
    }

    $lcampagne = json_decode($InfoBilan->campagne);
    foreach($lcampagne as $campagne => $nb){

        if(!isset($campagneIntervenant[$InfoBilan->idIntervenant])){
            $campagneIntervenant[$InfoBilan->idIntervenant] = array();
        }

        if(!isset($campagneIntervenant[$InfoBilan->idIntervenant][$campagne])){
            $campagneIntervenant[$InfoBilan->idIntervenant][$campagne] = 0;
        }
        $campagneIntervenant[$InfoBilan->idIntervenant][$campagne] += $nb;
    }

    $infoIntervenant[$InfoBilan->FK_idIntervenant]['nbInter'] += $InfoBilan->nbIntervention;
    $infoIntervenant[$InfoBilan->FK_idIntervenant]['nbHeures'] += $InfoBilan->nbHeure;
    $infoIntervenant[$InfoBilan->FK_idIntervenant]['montantBrut'] += $InfoBilan->montantBrut;
    $infoIntervenant[$InfoBilan->FK_idIntervenant]['montantRepas'] += $InfoBilan->montantRepas;
    $infoIntervenant[$InfoBilan->FK_idIntervenant]['montantKm'] += $InfoBilan->montantKm;
    $infoIntervenant[$InfoBilan->FK_idIntervenant]['montantAutres'] += $InfoBilan->montantAutres;
}

?>


<table class="table table-striped table-bordered table-hover" id="tableBILAN_18_0">
    <thead>
    <tr>
        <th>Intervenant</th>
        <th>Campagnes</th>
        <th>Nb interventions</th>
        <th>Nb heures</th>
        <th>Montant brut</th>
        <th>Montant repas</th>
        <th>Montant km</th>
        <th>Total Autres Frais</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($infoIntervenant as $idIntervenant => $info){

        $commentaireCampagne = '';
        if(isset($campagneIntervenant[$idIntervenant])){
            foreach($campagneIntervenant[$idIntervenant] as $libelleCampagne => $qte){
                $commentaireCampagne .= '<strong>'.$libelleCampagne.'</strong> : '.$qte.', ';
            }
        }

        print '
        <tr>
            <td>'.$idIntervenant.' - '.$info['nom'].'</td>
            <td>'.$commentaireCampagne.'</td>
            <td>'.$info['nbInter'].'</td>
            <td>'.$info['nbHeures'].'</td>
            <td>'.$info['montantBrut'].'</td>
            <td>'.$info['montantRepas'].'</td>
            <td>'.$info['montantKm'].'</td>
            <td>'.$info['montantAutres'].'</td>
        </tr>';
    }
    ?>
    </tbody>
</table>
