var Login = function (event) {
    var handleLogin = function() {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "Nom d'utilisateur obligatoire"
                },
                password: {
                    required: "Mot de passe obligatoire"
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                Login.rechercheUtilisateur($('#ztNomUtilisateur').val(), $('#ztPassUtilisateur').val());
            }
        });

        $('#ztPassUtilisateur').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    Login.rechercheUtilisateur($('#ztNomUtilisateur').val(), $('#ztPassUtilisateur').val());
                }
                return false;
            }
        });

        $('#ztNomUtilisateur').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    Login.rechercheUtilisateur($('#ztNomUtilisateur').val(), $('#ztPassUtilisateur').val());
                }
                return false;
            }
        });

    }

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                ztEmailUtilisateur: {
                    required: true,
                    email: true
                },
                ztIdentifiantUtilisateur: {
                    required: true
                }
            },

            messages: {
                ztEmailUtilisateur: {
                    required: "E-mail obligatoire !",
                    email: "Veuillez saisir un e-mail valide !"
                },
                ztIdentifiantUtilisateur: {
                    required: "Identifiant obligatoire !"
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit

            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                Login.reinitMdpUtilisateur();
            }
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    Login.reinitMdpUtilisateur();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();

        },

        rechercheUtilisateur: function(nomUsr, passUsr){
            $.ajax({
                url: "includes/functions/bdd2web/func.verifPresenceUtilisateur.php",
                type: "POST",
                data: {
                    nomUsr : nomUsr,
                    passUsr : passUsr
                },
                dataType: "json"
            }).done(function( data ) {
                if(data.STATUS === 1){
                    document.location.href='index.php';
                }else {
                    $('.alert-danger', $('.login-form')).show().html('Identifiants incorrects');
                }
            }).fail(function( jqXHR, textStatus ) {

            });
        },

        reinitMdpUtilisateur: function(){

            $.ajax({
                url: "includes/functions/bdd2web/func.verifPresenceEmailUtilisateur.php",
                type: "POST",
                data: $('.forget-form').serialize(),
                dataType: "json"
            }).done(function( data ) {
                if(data.STATUS === 1){
                    jQuery('.login-form').show();
                    jQuery('.forget-form').hide();
                    $('#divAlertReinit').show().html('Vos identifiants ont &eacute;t&eacute; envoy&eacute;s par e-mail');
                }else {
                    $('.alert-danger', $('.forget-form')).show().html('Identifiants introuvables');
                }
            }).fail(function( jqXHR, textStatus ) {

            });

            // 	jQuery('.login-form').show();
            //   jQuery('.forget-form').hide();
        }

    };

}();