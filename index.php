<?php
#ini_set('display_errors','on');
#error_reporting(E_ALL);
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Default Page
// @detail : 
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Require Database Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/_config/config.sql.php';

if(checkValiditySession()){
		
	// ------------------------------------------------------------------------------------
	// Require Requests librairies
	// ------------------------------------------------------------------------------------
	require_once dirname ( __FILE__ ) . '/includes/librairies/requests/Requests.php';
	Requests::register_autoloader();
	
	// ------------------------------------------------------------------------------------
	// Include HEADER FILE 
	// ------------------------------------------------------------------------------------
	include_once dirname ( __FILE__ ) . '/includes/components/_header.php';
	
	// ------------------------------------------------------------------------------------
	// Include SIDEBAR FILE
	// ------------------------------------------------------------------------------------
	include_once dirname ( __FILE__ ) . '/includes/components/_sidebar.php';

	// ------------------------------------------------------------------------------------
	// Load module / sub-module name
	// ------------------------------------------------------------------------------------
	$request = Requests::post(
		ADRESSE_BO . '/includes/components/_page-title.php', 
		array('Accept' => 'application/json'), 
		array(
			'ap' => ((filter_has_var(INPUT_GET, 'ap')) ? filter_input(INPUT_GET, 'ap') : DEFAULT_MODULE), 
			'ss_m' => ((filter_has_var(INPUT_GET, 'ss_m')) ? filter_input(INPUT_GET, 'ss_m') : DEFAULT_SUBMODULE)
		)
	);
	
	print '<div class="page-content-wrapper"><div class="page-content">';
	
	// ------------------------------------------------------------------------------------
	// Print Title
	// ------------------------------------------------------------------------------------
	$InfoTitle = json_decode($request->body);
	print $InfoTitle->Html;
	
	$boolModuleDefaut = true;
	$linkJsModule = '';

	// --------------------------------------------------------------------------------------------
	// Check parameter GET (for module and sub-module
	// --------------------------------------------------------------------------------------------
	if(filter_has_var(INPUT_GET, 'ap') || filter_has_var(INPUT_GET, 'ss_m')){
		if(!filter_has_var(INPUT_GET, 'ss_m')){
			if(is_file('modules/' . filter_input(INPUT_GET, 'ap') . '/m_' . filter_input(INPUT_GET, 'ap') . '.php')){
				$boolModuleDefaut = false;
				include_once 'modules/' . filter_input(INPUT_GET, 'ap') . '/m_' . filter_input(INPUT_GET, 'ap') . '.php';
                $linkJsModule = '<script src="modules/' . filter_input(INPUT_GET, 'ap') . '/s_' . filter_input(INPUT_GET, 'ap') . '.js" type="text/javascript"></script>';
			}
		}else {
			if(is_file('modules/' . filter_input(INPUT_GET, 'ap') . '/' . filter_input(INPUT_GET, 'ss_m') . '/m_' . filter_input(INPUT_GET, 'ss_m') . '.php')){
				$boolModuleDefaut = false;
				include_once 'modules/' . filter_input(INPUT_GET, 'ap') . '/' . filter_input(INPUT_GET, 'ss_m') .'/m_' . filter_input(INPUT_GET, 'ss_m') . '.php';
                $linkJsModule = '<script src="modules/' . filter_input(INPUT_GET, 'ap') . '/' . filter_input(INPUT_GET, 'ss_m') .'/s_' . filter_input(INPUT_GET, 'ss_m') . '.js?v='.date('YmdHis').'" type="text/javascript"></script>';
			}
		}
	}
	
	// ------------------------------------------------------------------------------------
	// include default module
	// ------------------------------------------------------------------------------------
	if($boolModuleDefaut){
		if(DEFAULT_SUBMODULE != ''){
			include_once 'modules/' . DEFAULT_MODULE . '/' . DEFAULT_SUBMODULE . '/m_' . DEFAULT_SUBMODULE . '.php';
            $linkJsModule = '<script src="modules/' . DEFAULT_MODULE . '/' . DEFAULT_SUBMODULE . '/s_' . DEFAULT_SUBMODULE . '.js" type="text/javascript"></script>';
		}else{
			include_once 'modules/' . DEFAULT_MODULE . '/m_' . DEFAULT_MODULE . '.php';
            $linkJsModule = '<script src="modules/' . DEFAULT_MODULE . '/s_' . DEFAULT_MODULE . '.js" type="text/javascript"></script>';
		}
	}
	
	print '</div></div>';
	
	// ------------------------------------------------------------------------------------
	// Include FOOTER FILE
	// ------------------------------------------------------------------------------------
	include_once dirname ( __FILE__ ) . '/includes/components/_footer.php';	
}else {
	header('location:login.php');
}