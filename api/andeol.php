<?php
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);
require_once dirname ( __FILE__ ) . '/../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Attention, cela nécessite un ajout de champ dans la table su_intervention
 * andeol_id varchar(256) not null
 */

/** -----------------------------------------------------------------------------------------------
 * METHODE GET - POST - PUT|PATCH - DELETE
 */
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    /** -------------------------------------------------------------------------------------------
     * AUTH
     */
    if (!isset($_GET['_token']) ||
        $_GET['_token'] !== '99c05c1afe6f4b2afadad2f7602409fda5c953cd64dbedf629cf1505673a3fa6cb82580fa71658abea9c27c6c1a38096e0abd9a88fd88db40af5df23629b98b7'
    ) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }
    /** -------------------------------------------------------------------------------------------
     * On ne peut getter que id
     */
    if (!isset($_GET['id'])) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }

    $sql = 'SELECT * FROM su_intervention WHERE idIntervention = :id';
    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':id', (int)$_GET['id'], PDO::PARAM_INT);
    $exec->execute();
    if ($exec->rowCount() == 0) {
        header("HTTP/1.1 405 Method Not Allowed");
        exit;
    }
    $data = $exec->fetch(PDO::FETCH_OBJ);
    header("HTTP/1.1 200 OK");
    print json_encode($data);
    exit;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /** -------------------------------------------------------------------------------------------
     * AUTH
     */
    if (!isset($_POST['_token']) ||
        $_POST['_token'] !== '99c05c1afe6f4b2afadad2f7602409fda5c953cd64dbedf629cf1505673a3fa6cb82580fa71658abea9c27c6c1a38096e0abd9a88fd88db40af5df23629b98b7'
    ) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }
    /** -------------------------------------------------------------------------------------------
     * On ne peut getter que id
     */
    if (!isset($_POST['id'])) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }
    /** -------------------------------------------------------------------------------------------
     * La méthode POST permet de créer
     * id | id_intervenant | id_mission | id_client | date_debut | date_fin
     *
     * Note id_client = Code CIP
     */
    if (!isset($_POST['id']) ||
        !isset($_POST['id_intervenant']) ||
        !isset($_POST['id_mission']) ||
        !isset($_POST['id_client']) ||
        !isset($_POST['date_debut']) ||
        !isset($_POST['date_fin'])
    ) {
        header("HTTP/1.1 412 Precondition Failed");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * Phase #1 - récupération du PDV
     */
    $sql = '
    SELECT *
    FROM su_pdv
    WHERE codeMerval = :FK_idPdv
    ';
    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':FK_idPdv', (int)$_POST['id_client'], PDO::PARAM_INT);
    $exec->execute();
    if ($exec->rowCount() == 0) {
        header("HTTP/1.1 412 Precondition Failed");
        exit;
    }
    if ($exec->rowCount() != 1) {
        header("HTTP/1.1 409 Conflict");
        exit;
    }
    $data = $exec->fetch(PDO::FETCH_OBJ);

    $date_deb = DateTime::createFromFormat('Y/m/d H:i:s', $_POST['date_debut']);
    $date_fin = DateTime::createFromFormat('Y/m/d H:i:s', $_POST['date_fin']);

    /** -------------------------------------------------------------------------------------------
     * Phase #2 - ajout de la ligne
     */
    $sql = '
    INSERT INTO su_intervention (
        FK_idIntervenant, FK_idPdv, FK_idMission, dateDebut, dateFin, FK_idContrat, andeol_id)
    VALUES (:FK_idIntervenant, :FK_idPdv, :FK_idMission, :dateDebut, :dateFin, :FK_idContrat, :andeol_id
    )
    ';

    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':FK_idIntervenant', (int)$_POST['id_intervenant'], PDO::PARAM_INT);
    $exec->bindValue(':FK_idPdv', $data->idPdv, PDO::PARAM_INT);
    $exec->bindValue(':FK_idMission', (int)$_POST['id_mission'], PDO::PARAM_INT);
    $exec->bindValue(':dateDebut', $date_deb->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    $exec->bindValue(':dateFin', $date_fin->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    $exec->bindValue(':FK_idContrat', null, PDO::PARAM_INT);
    $exec->bindValue(':andeol_id', $_POST['id'], PDO::PARAM_STR);
    if (!$exec->execute()) {
        header("HTTP/1.1 405 Method Not Allowed");
        exit;
    }
    $id = DbConnexion::getInstance()->lastInsertId();
    print json_encode($id);
    header("HTTP/1.1 200 OK");
    exit;
}

if ($_SERVER['REQUEST_METHOD'] == 'PUT' ||
    $_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $data = array();
    extractRawData($data, file_get_contents("php://input"));
    $data = (object) $data;
    
    /** -------------------------------------------------------------------------------------------
     * AUTH
     */
    if (!isset($data->_token) ||
        $data->_token !== '99c05c1afe6f4b2afadad2f7602409fda5c953cd64dbedf629cf1505673a3fa6cb82580fa71658abea9c27c6c1a38096e0abd9a88fd88db40af5df23629b98b7'
    ) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * On ne peut getter que id
     */
    if (!isset($data->id)) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * La méthode PUT / PATCH permet de mettre à jour
     * id | id_sirh | date_debut | date_fin
     */
    if (!isset($data->id) ||
        !isset($data->id_sirh) ||
        !isset($data->date_debut) ||
        !isset($data->date_fin)
    ) {
        header("HTTP/1.1 412 Precondition Failed");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * Méthode Pyramide, on pourrait le faire en une... mais on va le faire en 2
     */
    $sql = '
    SELECT * 
    FROM su_intervention 
    WHERE idIntervention = :id 
    AND andeol_id = :andeol_id
    ';
    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':id', (int)$data->id_sirh, PDO::PARAM_INT);
    $exec->bindValue(':andeol_id', (int)$data->id, PDO::PARAM_INT);
    $exec->execute();
    if ($exec->rowCount() == 0) {
        header("HTTP/1.1 405 Method Not Allowed");
        exit;
    }

    $date_deb = DateTime::createFromFormat('Y/m/d H:i:s', $data->date_debut);
    $date_fin = DateTime::createFromFormat('Y/m/d H:i:s', $data->date_fin);


    $sql = '
    UPDATE su_intervention
    SET dateDebut = :dateDebut, dateFin = :dateFin
    WHERE idIntervention = :id 
    AND andeol_id = :andeol_id
    ';

    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':id', $data->id_sirh, PDO::PARAM_INT);
    $exec->bindValue(':andeol_id', $data->id, PDO::PARAM_INT);
    $exec->bindValue(':dateDebut', $date_deb->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    $exec->bindValue(':dateFin', $date_fin->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    $exec->execute();
    header("HTTP/1.1 200 OK");
    print json_encode('done');
    exit;
}

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $data = array();
    extractRawData($data, file_get_contents("php://input"));
    $data = (object) $data;

    /** -------------------------------------------------------------------------------------------
     * AUTH
     */
    if (!isset($data->_token) ||
        $data->_token !== '99c05c1afe6f4b2afadad2f7602409fda5c953cd64dbedf629cf1505673a3fa6cb82580fa71658abea9c27c6c1a38096e0abd9a88fd88db40af5df23629b98b7'
    ) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * On ne peut getter que id
     */
    if (!isset($data->id)) {
        header("HTTP/1.1 401 Unauthorized");
        exit;
    }

    /** -------------------------------------------------------------------------------------------
     * La méthode PUT / PATCH permet de mettre à jour
     * Id | Id_intervenant | Id_mission | Id_client | Date_debut | Date_fin
     */
    if (!isset($data->id) ||
        !isset($data->id_sirh)
    ) {
        header("HTTP/1.1 412 Precondition Failed");
        exit;
    }

    $sql = '
    DELETE 
    FROM su_intervention
    WHERE idIntervention = :id 
    AND andeol_id = :andeol_id
    ';

    $exec = DbConnexion::getInstance()->prepare($sql);
    $exec->bindValue(':id', (int)$data->id_sirh, PDO::PARAM_INT);
    $exec->bindValue(':andeol_id', (int)$data->id, PDO::PARAM_INT);
    if ($exec->execute()) {
        header("HTTP/1.1 200 OK");
        print json_encode('done');
        return;
    }
    else {
        header("HTTP/1.1 405 Method Not Allowed");
        return;
    }
}

/** -----------------------------------------------------------------------------------------------
 * 99c05c1afe6f4b2afadad2f7602409fda5c953cd64dbedf629cf1505673a3fa6cb82580fa71658abea9c27c6c1a38096e0abd9a88fd88db40af5df23629b98b7
 */

/** -----------------------------------------------------------------------------------------------
 * @param array $a_data
 * @return array
 * @author : https://webdevdesigner.com/q/manually-parse-raw-multipart-form-data-data-with-php-31835/
 */
function extractRawData(array &$a_data, $input) {
    preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
    $boundary = $matches[1];
    $a_blocks = preg_split("/-+$boundary/", $input);
    array_pop($a_blocks);
    foreach ($a_blocks as $id => $block)
    {
        if (empty($block))
            continue;
        if (strpos($block, 'application/octet-stream') !== FALSE)
        {
            preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
        }
        else
        {
            preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
        }
        $a_data[$matches[1]] = trim($matches[2]);
    }
    return $a_data;
}