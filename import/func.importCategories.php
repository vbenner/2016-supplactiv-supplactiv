<?php
/** Class PHPExcel  */
require_once dirname(__FILE__) . '/../includes/librairies/phpexcel/PHPExcel/IOFactory.php';

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../includes/queries/queries.web2bdd.php';

/** -----------------------------------------------------------------------------------------------
 * SQL - Recherche de la presence de la catégorie
 */
$sqlRechercheCat = "
SELECT idCategoriePdv
FROM su_pdv_categorie
WHERE libelleCategoriePdv = :libelleCategoriePdv
OR idCategoriePdv :libelleCategoriePdv
";
$rechercheCatExec = DbConnexion::getInstance()->prepare($sqlRechercheCat);

/** -----------------------------------------------------------------------------------------------
 * SQL - Ajout de la catégorie
 */
$sqlInsertionCAT = '
INSERT INTO su_pdv_categorie(libelleCategoriePdv)
    VALUES(:libelleCategoriePdv)';
$insertionCATExec = DbConnexion::getInstance()->prepare($sqlInsertionCAT);


/** -----------------------------------------------------------------------------------------------
 * SQL - Recherche de la presence de la sous-catégorie
 */
$sqlRechercheSousCat = "
SELECT idSousCategorie
FROM su_pdv_sous_categorie
WHERE libelleSousCategorie = :libelleSousCategorie
AND FK_idCategorie = :FK_idCategorie
";
$rechercheSousCatExec = DbConnexion::getInstance()->prepare($sqlRechercheSousCat);

/** -----------------------------------------------------------------------------------------------
 * SQL - MAX sous-CAT
 */
$sqlMaxSousCat = '
SELECT MAX(idSousCategorie) AS LeMax
FROM su_pdv_sous_categorie
WHERE FK_idCategorie = :FK_idCategorie
';
$maxSousCatExec = DbConnexion::getInstance()->prepare($sqlMaxSousCat);

/** -----------------------------------------------------------------------------------------------
 * SQL - Ajout de la catégorie
 */
$sqlInsertionSOUSCAT = '
INSERT INTO su_pdv_categorie(FK_idCategorie, idSousCategorie, libelleSousCategorie)
    VALUES(:FK_idCategorie, :idSousCategorie, :libelleSousCategorie)';
$insertionSOUSCATExec = DbConnexion::getInstance()->prepare($sqlInsertionSOUSCAT);


/** @var $FormatFichier Format de fichier autorise */
$FormatFichier = array(
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
    'application/msword',
    'application/zip'
);

/** @var $CorrespondanceColonne Correspondance des colonnes BDD / Fichier */
$CorrespondanceColonne = array(
    0 => 'libelleCategoriePdv',
    1 => 'libelleSousCategorie',
);

/** Initialisation des variables */
$nbLigneTotal = 0; $nbLigneOK = 0;

/** On test la presence du fichier */
if(isset($_FILES['fichierCAT']) && $_FILES['fichierCAT']['error'] == 0 && $_FILES['fichierCAT']['size'] > 0) {
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);

    /** @var $FormatEnvoi On test le format du fichier */
    $FormatEnvoi = finfo_file($InfoFichier, $_FILES['fichierCAT']['tmp_name']);

    if (in_array($FormatEnvoi, $FormatFichier)) {

        /** Ouverture d'un reader */
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        /** Lecture du fichier */
        $objPHPExcel = $objReader->load($_FILES['fichierCAT']['tmp_name']);

        /** Lecture de la feuille 1 */
        $sheetCAT = $objPHPExcel->getSheet(0);

        /** On pacours les lignes */
        foreach ($sheetCAT->getRowIterator() as $row) {

            $IndiceColonne = 0;
            $InfoLigne = array();
            foreach ($row->getCellIterator() as $cell) {
                @$InfoLigne[$CorrespondanceColonne[$IndiceColonne]] = $cell->getValue();
                $IndiceColonne++;
            }
            /** On test la taille du tableau  */
            if (sizeof($InfoLigne) == 1) {
                $InfoLigne = (object)$InfoLigne;
                if (
                        !empty($InfoLigne->libelleCategoriePdv)
                ) {

                    /** On ajoute dans 2 tables différentes */
                    $rechercheCatExec->bindValue(':libelleCategoriePdv', $InfoLigne->libelleCategoriePdv);
                    $rechercheCatExec->execute();

                    /** On continue si la CAT n'existe pas  */
                    if($rechercheCatExec->rowCount() == 0){
                        $nbLigneTotalCAT++;

                        /** Ajout de la CAT */
                        $insertionCATExec->bindValue(':libelleCategoriePdv', deleteSpecialCarac($InfoLigne->libelleCategoriePdv),PDO::PARAM_STR);
                        if($insertionCATExec->execute()){
                            $idCAT = DbConnexion::getInstance()->lastInsertId();
                            $nbLigneCATOK++;
                        }
                    }
                }
            }
        }

        /** Lecture de la feuille 1 */
        $sheetSOUSCAT = $objPHPExcel->getSheet(1);

        /** On pacours les lignes */
        foreach ($sheetSOUSCAT->getRowIterator() as $row) {

            $IndiceColonne = 0;
            $InfoLigne = array();
            foreach ($row->getCellIterator() as $cell) {
                @$InfoLigne[$CorrespondanceColonne[$IndiceColonne]] = $cell->getValue();
                $IndiceColonne++;
            }
            /** On test la taille du tableau  */
            if (sizeof($InfoLigne) == 2) {
                $InfoLigne = (object)$InfoLigne;
                if(
                    !empty($InfoLigne->libelleCategoriePdv) &&
                    !empty($InfoLigne->libelleSousCategorie) ){

                    /** On ajoute dans 2 tables différentes */
                    $rechercheCatExec->bindValue(':libelleCategoriePdv', $InfoLigne->libelleCategoriePdv);
                    $rechercheCatExec->execute();

                    /** On continu si la CAT existe !!! */
                    if($rechercheCatExec->rowCount() == 1){
                        $rechercheCat = $rechercheCatExec->fetchObject($rechercheCatExec);
                        $idCAT = $rechercheCat->idCategoriePdv;

                        /** On recherche maintenant la sous-categorie */
                        $rechercheSousCatExec->bindValue(':libelleSousCategorie', $InfoLigne->libelleSousCategorie);
                        $rechercheSousCatExec->bindValue(':FK_idCategorie', $idCAT);
                        $rechercheSousCatExec->execute();

                        /** On continu si ça n'existe pas !!! */
                        if($rechercheSousCatExec->rowCount() == 0){

                            $nbLigneTotalSOUS++;

                            $maxSousCatExec->bindValue(':FK_idCategorie', $idCAT);
                            $maxSousCatExec->execute();
                            $maxSousCat = $maxSousCatExec->fetchObject($rechercheCatExec);
                            $max = $maxSousCat->LeMax;
                            $max++;

                            /** Ajout de la SOUS-CAT */
                            $insertionSOUSCATExec->bindValue(':FK_idCategorie', $idCAT, 			PDO::PARAM_INT);
                            $insertionSOUSCATExec->bindValue(':idSousCategorie', 			$max, 			PDO::PARAM_INT);
                            $insertionSOUSCATExec->bindValue(':libelleSousCategorie', 		deleteSpecialCarac($InfoLigne->libelleSousCategorie), 			PDO::PARAM_STR);

                            /** Si l'ajout est OK on insere les contacts PDV */
                            if($insertionSOUSCATExec->execute()){
                                $idSOUSCAT = DbConnexion::getInstance()->lastInsertId();
                                $nbLigneSOUSCATOK++;
                            }
                        }
                    }
                }
            }
        }
    }
}

?>

<script>
    window.top.window.ClassCategorie.resultImport(<?php print $nbLigneTotalCAT ?>, <?php print $nbLigneSOUSCATOK ?>);
</script>