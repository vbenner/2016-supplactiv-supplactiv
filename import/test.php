<?php
ini_set('display_errors', 'on');
$filename = "json.txt";
$handle = fopen($filename, "r");
$contents = fread($handle, filesize($filename));
fclose($handle);

$Liste = json_decode($contents);
$moisPrecendent = null; $indice = 0;
$ListeMois = array();
$ListeIntervention = array();

/** On parcours les lignes du fichier */
foreach($Liste as $idLigne => $Contenu){

    /** S'il s'agit de la deuxieme ligne (0+1) on recupere les indices des mois */
    if($idLigne == 1){
        foreach($Contenu as $idColonne => $mois){
            if(!empty($mois)){
                $ListeMois[$mois] = array(
                    'debut' => $idColonne,
                    'fin' => 0
                );

                if(!is_null($moisPrecendent)){
                    $ListeMois[$moisPrecendent]['fin'] = $idColonne-1;
                }
                $moisPrecendent = $mois;
            }
            $indice++;
        }

        $ListeMois[$moisPrecendent]['fin'] = $indice;
    }

    /** Si la ligne est supérieur a 4 il s'agit d'une ligne couple intervenant / pdv */
    if($idLigne > 3){

        /** @var $Contenu  Contenu de la ligne*/
        $Contenu = (array)$Contenu;

        /** Si l'idIntervenant et le code merval sont present on continu */
        if(isset($Contenu[0]) && isset($Contenu[4])) {

            /** On stock l'idIntervenant et l'idPdv */
            $idIntervenant = $Contenu[0]; $idPdv = $Contenu[4];

            /** Creation du tableau de stockage des dates d'interventions */
            if (!isset($ListeIntervention[$idIntervenant])) {
                $ListeIntervention[$idIntervenant] = array();
            }
            if (!isset($ListeIntervention[$idIntervenant][$idPdv])) {
                $ListeIntervention[$idIntervenant][$idPdv] = array();
            }

            /** On ajoute les infos */
            foreach ($Contenu as $idColonne => $infoLigne) {
                if ($idColonne >= $ListeMois['01-JANVIER']['debut'] && !empty($infoLigne)) {
                     array_push($ListeIntervention[$idIntervenant][$idPdv], array($idColonne => $infoLigne));
                }
            }
        }
    }
}

$listeJourAnnee = array();

foreach($ListeMois as $mois => $indice){
    $infoMois = preg_split('/-/',$mois);
    $idJour = 1;
    $indiceDebut = $indice['debut']+1;
    $indiceFin = ($infoMois[0] == 12) ? $indice['debut']+32: $indice['fin']+1;
    for($i = $indiceDebut; $i < $indiceFin; $i++){
        $idJour = (strlen($idJour) < 2) ? '0'.$idJour : $idJour;
        $listeJourAnnee[$i] = date('Y').'-'.$infoMois[0].'-'.$idJour;
        $idJour++;
    }
}

foreach($ListeIntervention as $idIntervenant => $listePdv){
    print '<h1>'.$idIntervenant.'</h1>';
    foreach($listePdv as $idPdv => $dateIntervention){
        print '<h2> -- '.$idPdv.'</h2>';
        foreach($dateIntervention as $infoDate){
            foreach($infoDate as $indice => $typeIntervention){
                print $indice.' --> '.$listeJourAnnee[$indice].' --> '.$typeIntervention.'<br />';
            }
        }
    }
}