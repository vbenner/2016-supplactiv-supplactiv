<?php

/** Class PHPExcel  */
require_once dirname(__FILE__) . '/../includes/librairies/phpexcel/PHPExcel/IOFactory.php';

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../includes/queries/queries.web2bdd.php';

/**
 * Recherche de la presence du point de vente
 */
$sqlRecherchePdv = "
SELECT idPdv
FROM su_pdv
WHERE codeMerval = :codeMerval LIMIT 1";
$RecherchePdvExc = DbConnexion::getInstance()->prepare($sqlRecherchePdv);

/**
 * Recherche du point de vente
 */
$sqlRecherchePdv = "
SELECT idPdv
FROM su_pdv
WHERE codeMerval = :codeMerval";
$RecherchePdvExc = DbConnexion::getInstance()->prepare($sqlRecherchePdv);

/** @var $FormatFichier Format de fichier autorise */
$FormatFichier = array(
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
    'application/msword',
    'application/zip'
);

/** @var $CorrespondanceColonne Correspondance des colonnes BDD / Fichier */
$CorrespondanceColonne = array(
    0 => 'codeMerval',
    1 => 'libellePdv',
    2 => 'adressePdv_A',
    3 => 'adressePdv_B',
    4 => 'adressePdv_C',
    5 => 'codePostalPdv',
    6 => 'villePdv',
    7 => 'telephoneMagasin',
    8 => 'faxPdv',
    9 => 'nomTitulaire_A',
    10 => 'prenomTitulaire_A',
    11 => 'nomTitulaire_B',
    12 => 'prenomTitulaire_B',
    13 => 'nomInterlocuteurPdv',
    14 => 'prenomInterlocuteurPdv',
    15 => 'telInterlocuteurPdv',
    16 => 'faxInterlocuteurPdv',
    17 => 'mailInterlocuteurPdv',
    18 => 'FK_idCategorie',
    19 => 'FK_idSousCategorie',
    20 => 'nbIntervention',
    21 => 'FK_idIntervenant'
);

/** Initialisation des variables */
$nbLigneTotal = 0; $nbLigneOK = 0;

$nbInterventionAdd = $nbNvPdv = 0;

/** On test la presence du fichier */
if(isset($_FILES['fichierPDV-pdv']) && $_FILES['fichierPDV-pdv']['error'] == 0 && $_FILES['fichierPDV-pdv']['size'] > 0) {
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);

    /** @var $FormatEnvoi On test le format du fichier */
    $FormatEnvoi = finfo_file($InfoFichier, $_FILES['fichierPDV-pdv']['tmp_name']);

    if (in_array($FormatEnvoi, $FormatFichier)) {

        /** Ouverture d'un reader */
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        /** Lecture du fichier */
        $objPHPExcel = $objReader->load($_FILES['fichierPDV-pdv']['tmp_name']);

        /** Lecture de la feuille 1 */
        $sheet = $objPHPExcel->getSheet(0);

        /** On pacours les lignes */
        foreach ($sheet->getRowIterator() as $row) {

            $IndiceColonne = 0;
            $InfoLigne = array();
            foreach ($row->getCellIterator() as $cell) {
                @$InfoLigne[$CorrespondanceColonne[$IndiceColonne]] = $cell->getValue();
                $IndiceColonne++;
            }
            /** On test la taille du tableau  */
            if (sizeof($InfoLigne) == 20 || sizeof($InfoLigne) == 21 || sizeof($InfoLigne) == 22) {
                $InfoLigne = (object)$InfoLigne;

                if(!empty($InfoLigne->codeMerval) && !empty($InfoLigne->libellePdv) && !empty($InfoLigne->FK_idCategorie) && !empty($InfoLigne->FK_idSousCategorie)){

                    /** Recherche du PDV dans la base de donnees */
                    $RecherchePdvExc->bindValue(':codeMerval', $InfoLigne->codeMerval);
                    $RecherchePdvExc->execute();

                    /** @var $idPdv Initialisation de l'identifiant PDV */
                    $idPdv = null;

                    /** On continu si le PDV n'existe pas  */
                    if($RecherchePdvExc->rowCount() == 0){

                        $nbLigneTotal++;

                        /** Ajout du PDV */
                        $InsertionPdvExc->bindValue(':libellePdv', 			deleteSpecialCarac($InfoLigne->libellePdv), 			PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':codeMerval', 			deleteSpecialCarac($InfoLigne->codeMerval), 			PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':adressePdv_A', 		deleteSpecialCarac($InfoLigne->adressePdv_A), 			PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':adressePdv_B', 		deleteSpecialCarac($InfoLigne->adressePdv_B), 			PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':adressePdv_C', 		deleteSpecialCarac($InfoLigne->adressePdv_C), 			PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':codePostalPdv', 		deleteSpecialCarac($InfoLigne->codePostalPdv), 		PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':villePdv', 			deleteSpecialCarac($InfoLigne->villePdv), 				PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':telephoneMagasin', 	deleteSpecialCarac($InfoLigne->telephoneMagasin), 		PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':faxPdv', 				deleteSpecialCarac($InfoLigne->faxPdv), 				PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':FK_idCategorie', 		$InfoLigne->FK_idCategorie, 		PDO::PARAM_INT);
                        $InsertionPdvExc->bindValue(':FK_idSousCategorie', 	$InfoLigne->FK_idSousCategorie, 	PDO::PARAM_INT);
                        $InsertionPdvExc->bindValue(':nomTitulairePdv_A', 	deleteSpecialCarac($InfoLigne->nomTitulaire_A), 	PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':prenomTitulairePdv_A', deleteSpecialCarac($InfoLigne->prenomTitulaire_A), 	PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':nomTitulairePdv_B', deleteSpecialCarac($InfoLigne->nomTitulaire_B), 	PDO::PARAM_STR);
                        $InsertionPdvExc->bindValue(':prenomTitulairePdv_B', deleteSpecialCarac($InfoLigne->prenomTitulaire_B), 	PDO::PARAM_STR);

                        /** Si l'ajout est OK on insere les contacts PDV */
                        if($InsertionPdvExc->execute()){
                            $idPdv = DbConnexion::getInstance()->lastInsertId();

                            $nbLigneOK++;

                            $InsertionIntPdvExc->bindValue(':FK_idPdv', $idPdv, PDO::PARAM_INT);
                            $InsertionIntPdvExc->bindValue(':nomInterlocuteurPdv', deleteSpecialCarac($InfoLigne->nomInterlocuteurPdv), PDO::PARAM_STR);
                            $InsertionIntPdvExc->bindValue(':prenomInterlocuteurPdv', deleteSpecialCarac($InfoLigne->prenomInterlocuteurPdv), PDO::PARAM_STR);
                            $InsertionIntPdvExc->bindValue(':telInterlocuteurPdv', deleteSpecialCarac($InfoLigne->telInterlocuteurPdv), PDO::PARAM_STR);
                            $InsertionIntPdvExc->bindValue(':faxInterlocuteurPdv', deleteSpecialCarac($InfoLigne->faxInterlocuteurPdv), PDO::PARAM_STR);
                            $InsertionIntPdvExc->bindValue(':mailInterlocuteurPdv', deleteSpecialCarac($InfoLigne->mailInterlocuteurPdv), PDO::PARAM_STR);
                            $InsertionIntPdvExc->bindValue(':FK_idTypeInterlocuteurPdv', 1, PDO::PARAM_INT);
                            $InsertionIntPdvExc->execute();

                            $nbNvPdv++;
                        }
                    }
                    else {
                        $infoPdv = $RecherchePdvExc->fetch(PDO::FETCH_OBJ);
                        $idPdv = $infoPdv->idPdv;
                    }

                    $nbIntervention = (!empty($InfoLigne->nbIntervention)) ? $InfoLigne->nbIntervention : 1;
                    $idIntervenant = (!empty($InfoLigne->FK_idIntervenant)) ? $InfoLigne->FK_idIntervenant : 1;


                    for($i = 0; $i < $nbIntervention; $i++){
                        $nbInterventionAdd++;

                        $AjoutInterventionExc->bindValue(':idIntervenant', $idIntervenant, PDO::PARAM_INT);
                        $AjoutInterventionExc->bindValue(':idPdv', $idPdv, PDO::PARAM_INT);
                        $AjoutInterventionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5_pdv'), PDO::PARAM_INT);
                        $AjoutInterventionExc->execute();
                    }
                }
            }
        }
    }
}

?>

<script>
    window.top.window.ClassFicheMission.resultImportPdv(<?php print $nbNvPdv ?>, <?php print $nbInterventionAdd ?>);
</script>