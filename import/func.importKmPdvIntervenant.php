<?php

/** Class PHPExcel  */
require_once dirname(__FILE__) . '/../includes/librairies/phpexcel/PHPExcel/IOFactory.php';

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../includes/queries/queries.web2bdd.php';

/**
 * Recherche de la presence intervenant
 */
$sqlRechercheIntervenant = "
SELECT idIntervenant
FROM su_intervenant
WHERE idIntervenant = :idIntervenant";
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

/**
 * Recherche du point de vente
 */
$sqlRecherchePdv = "
SELECT idPdv
FROM su_pdv
WHERE codeMerval = :codeMerval";
$RecherchePdvExc = DbConnexion::getInstance()->prepare($sqlRecherchePdv);

/** @var $FormatFichier Format de fichier autorise */
$FormatFichier = array(
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
    'application/msword',
    'application/zip'
);

/** @var $CorrespondanceColonne Correspondance des colonnes BDD / Fichier */
$CorrespondanceColonne = array(
    0 => 'codeCIP',
    1 => 'libellePdv',
    2 => 'codePostalPdv',
    3 => 'villePdv',
    4 => 'idIntervenant',
    5 => 'Intervenant',
    6 => 'adresseInter',
    7 => 'codePostalInter',
    8 => 'villeInter',
    9 => 'totalKm'
);

/** Initialisation des variables */
$nbLigneTotal = 0; $nbLigneOK = 0; $file_size = 0;  $FormatEnvoi = '';

/** On test la presence du fichier */
if(isset($_FILES['fichierPDV']) && $_FILES['fichierPDV']['error'] == 0 && $_FILES['fichierPDV']['size'] > 0) {

    /** Ajout du filesize */
    $file_size = $_FILES['fichierPDV']['size'];

    /** @var  $InfoFichier */
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);

    /** @var $FormatEnvoi On test le format du fichier */
    $FormatEnvoi = finfo_file($InfoFichier, $_FILES['fichierPDV']['tmp_name']);


    if (in_array($FormatEnvoi, $FormatFichier)) {

        /** Ouverture d'un reader */
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        /** Lecture du fichier */
        $objPHPExcel = $objReader->load($_FILES['fichierPDV']['tmp_name']);

        /** Lecture de la feuille 1 */
        $sheet = $objPHPExcel->getSheet(0);

        /** On pacourt les lignes */
        foreach ($sheet->getRowIterator() as $row) {

            $IndiceColonne = 0;
            $InfoLigne = array();
            foreach ($row->getCellIterator() as $cell) {
                @$InfoLigne[$CorrespondanceColonne[$IndiceColonne]] = $cell->getValue();
                $IndiceColonne++;
            }


            /** On test la taille du tableau  */
            if (sizeof($InfoLigne) == 10) {
                $InfoLigne = (object)$InfoLigne;

                /** On test la presence des infos */
                if(!empty($InfoLigne->idIntervenant) && !empty($InfoLigne->totalKm) && is_numeric($InfoLigne->totalKm)){
                    $nbLigneTotal++;

                    $idIntervenant = '';
                    $RechercheIntervenantExc->bindValue(':idIntervenant', trim($InfoLigne->idIntervenant), PDO::PARAM_INT);
                    $RechercheIntervenantExc->execute();
                    while($InfoInt = $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ))
                        $idIntervenant = $InfoInt->idIntervenant;

                    $idPdv = '';
                    $RecherchePdvExc->bindValue(':codeMerval', $InfoLigne->codeCIP, PDO::PARAM_STR);
                    $RecherchePdvExc->execute();
                    while($InfoPdv = $RecherchePdvExc->fetch(PDO::FETCH_OBJ))
                        $idPdv = $InfoPdv->idPdv;

                    /** On test la presence des intervenants / pdv et KM */
                    if($InfoLigne->totalKm > 0 && !is_null($idIntervenant) && !is_null($idPdv)){

                        #echo 'on fait '.$idPdv.', '.$idIntervenant.', '.$InfoLigne->totalKm;
                        #die();
                        /** Ajout / Modification DES KM */
                        $InsertionKmPdvIntervenantExc->bindValue(':idPdv', $idPdv, PDO::PARAM_INT);
                        $InsertionKmPdvIntervenantExc->bindValue(':idIntervenant', $idIntervenant, PDO::PARAM_INT);
                        $InsertionKmPdvIntervenantExc->bindValue(':nbKm', $InfoLigne->totalKm, PDO::PARAM_INT);
                        $nbLigneOK += ($InsertionKmPdvIntervenantExc->execute()) ? 1 : 0;

                    }
                }
            }
        }
    }
    else {
        print $FormatEnvoi;
    }
}
?>
<script>
    window.top.window.ClassKM.resultImport(<?php print $nbLigneOK ?>, <?php print $nbLigneTotal ?>);
</script>