<?php

/** -----------------------------------------------------------------------------------------------
 * IMPORT DU PLANNING DE FORMATION
 * @date : 2017-03-02
 * @author : Vincent BENNER
 *
 */
set_time_limit(0);
ini_set('display_errors', 'on');
ini_set('memory_limit', '1020M');

/** Class PHPExcel  */
require_once dirname(__FILE__) . '/../includes/librairies/simplexlsx/simplexlsx.class.php';

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../includes/queries/queries.web2bdd.php';
require_once dirname(__FILE__) . '/../includes/queries/queries.bdd2web.php';

/** -----------------------------------------------------------------------------------------------
 *
 * Recherche de la presence intervenant
 */
$sqlRechercheIntervenant = "
SELECT *
FROM su_intervenant
WHERE idIntervenant = :idIntervenant";
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

$sqlRechercheIntervenant2 = "
SELECT *
FROM su_intervenant
WHERE UPPER(nomIntervenant) = :nomIntervenant AND UPPER(prenomIntervenant) = :prenomIntervenant LIMIT 1";
$RechercheIntervenant2Exc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant2);

/**
 * Recherche du point de vente
 */
$sqlRecherchePdv = "
SELECT *
FROM su_pdv
WHERE codeMerval = :codeMerval
";
$RecherchePdvExc = DbConnexion::getInstance()->prepare($sqlRecherchePdv);

$sqlRechercheAnneeCampagneMission = "
SELECT DATE_FORMAT(dateDebut, '%Y') AS anneeCampagne
FROM su_mission
INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE idMission = :idMission";
$RechercheAnneeCampagneMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheAnneeCampagneMission);

$sqlRechercheInterventionAvecDate = '
SELECT *
FROM su_intervention
WHERE FK_idPdv = :idPdv AND FK_idIntervenant = :idIntervenant AND FK_idMission = :idMission AND dateDebut BETWEEN :dateDebut AND :dateFin';
$RechercheInterventionAvecDateExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionAvecDate);

$sqlInsertTmpXLX = '
INSERT INTO tmp_xls VALUES (
:idm, :idi, :idpdv, :dates, :times, :duree
)
';
$insertTmpXLXExec = DbConnexion::getInstance()->prepare($sqlInsertTmpXLX);

/** -----------------------------------------------------------------------------------------------
 * POUR LA PARTIE FORMATION
 */
$sqlGetVerifOtherIntervenanteSamePDV = '
SELECT COUNT(*) AS LeCount
FROM su_intervention
WHERE FK_idIntervenant != :idinter
AND FK_idMission = :idmission
AND dateDebut >= :dated
AND dateFin <= :datef
AND NOT ISNULL(FK_idContrat)
';
$getVerifOtherIntervenanteSamePDVExec = DbConnexion::getInstance()->prepare($sqlGetVerifOtherIntervenanteSamePDV);

$sqlGetVerifInterventionSameDaySamePDV = '
SELECT COUNT(*) AS LeCount
FROM su_intervention
WHERE FK_idIntervenant = :idinter
AND FK_idMission = :idmission
AND dateDebut >= :dated
AND dateFin <= :datef
AND NOT ISNULL(FK_idContrat)
';
$getVerifInterventionSameDaySamePDVExec = DbConnexion::getInstance()->prepare($sqlGetVerifInterventionSameDaySamePDV);

$sqlAjoutInter = '
INSERT INTO su_intervention VALUES
(null, :idinter, :idpdv, :idmission, :dated, :datef, null)
';
$ajoutInterExec = DbConnexion::getInstance()->prepare($sqlAjoutInter);

/** @var $FormatFichier Format de fichier autorise */
$FormatFichier = array(
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
    'application/msword',
    'application/zip'
);

/** @var $CorrespondanceColonne Correspondance des colonnes BDD / Fichier */
$CorrespondanceColonne = array(
    0 => 'nomIntervenant',
    1 => 'prenomIntervenant',
    2 => 'nomPdv',
    3 => 'adressePdv',
    9 => 'codeCIP',
    12 => 'dateFormation',
    13 => 'horaireFormation',
    14 => 'dureeFormation',

);

#echo '<pre>';
#print_r($_POST);
#echo '</pre>';
#die();

/** Initialisation des variables */
$nbLigneTotal = 0;
$nbLigneOK = 0;

/** On test la presence du fichier */
if (isset($_FILES['fichierPDV']) && $_FILES['fichierPDV']['error'] == 0 && $_FILES['fichierPDV']['size'] > 0) {
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);

    /** @var $FormatEnvoi On test le format du fichier */
    $FormatEnvoi = finfo_file($InfoFichier, $_FILES['fichierPDV']['tmp_name']);

    if (in_array($FormatEnvoi, $FormatFichier)) {
        $xlsx = new SimpleXLSX($_FILES['fichierPDV']['tmp_name']);

        list($num_cols, $num_rows) = $xlsx->dimension();

        $Liste = json_decode(json_encode($xlsx->rows()));

        #print_r($Liste);
        #echo $num_cols.' x '.$num_rows;
        #die();

        /** ---------------------------------------------------------------------------------------
         * On parcourt dans une premier temps les lignes du fichier
         * On en profite pour construire les tableaux et vider la base de données temporaire
         */
        $sqlClearImportXLS = '
        DELETE FROM tmp_xls WHERE mission_id = ' . $_POST['idMission_4_5'] . '
        ';
        #echo $sqlClearImportXLS;
        #die();

        $clearImportXLSExec = DbConnexion::getInstance()->prepare($sqlClearImportXLS);
        $clearImportXLSExec->execute();
        $formatrice = [];
        $anomalies = [];
        $nbLignesEntete = 3;

        #echo '<pre>';
        #echo 'ANOMALIES = ';
        #print_r($anomalies);
        #echo '</pre>';

        #echo 'LIGNE = '.$num_rows;

        foreach ($Liste as $idLigne => $Contenu) {

            /** -----------------------------------------------------------------------------------
             * Pour l'entête, on a juste à passer (3 lignes, La ligne des Colonnes et la ligne
             * des entêtes
             */
            if ($idLigne <= 0) {

            } else {

                #echo '>'.$idLigne."\r\n";
                #if ($idLigne >= 170) {
                #    echo '<pre>';
                #    print_r($Contenu);
                #    echo '</pre>';
                #}

                /** -------------------------------------------------------------------------------
                 * Extraction du nom et du prénom (pour en déduire l'ID)
                 * Du CIP, de la date et de l'heure
                 */
                $idIntervenant = 0;
                $idPdv = 0;
                $dateF = '';
                $timeF = 0;

                $validFormatrice = false;
                $validCIP = false;
                $validDate = false;
                $validTime = false;
                $validDuree = false;

                /** -------------------------------------------------------------------------------
                 * ATTENTION BUG --> Le nombre de lignes n'est pas forcément le bon
                 */
                if (is_array($Contenu)) {

                    $nom = $Contenu[0];
                    $prenom = $Contenu[1];
                    $cip = $Contenu[9];
                    $date = $Contenu[12];
                    $horaire = $Contenu[13];
                    $duree = $Contenu[14];

                    if ($nom != '' && $cip != '' && $date != '' && $horaire != '') {
#echo "LL >> ".$idLigne."\r\n";
                        #echo "   ".$nom."\r\n";
                        #echo "   ".$prenom."\r\n";
                        #echo "   ".$cip."\r\n";
                        #echo "   ".$date."\r\n";
                        #echo "   ".$horaire."\r\n";
                        #echo ' - - - - - - - - - - - - - - -'."\r\n";

                        /** ---------------------------------------------------------------------------
                         * Recherche INTERVENANT ou ANOMALIE
                         */
                        $RechercheIntervenant2Exc->bindValue(':nomIntervenant', trim($nom));
                        $RechercheIntervenant2Exc->bindValue(':prenomIntervenant', trim($prenom));
                        $RechercheIntervenant2Exc->execute();
                        if ($RechercheIntervenant2Exc->rowCount() == 1) {
                            $InfoI = $RechercheIntervenant2Exc->fetch(PDO::FETCH_OBJ);
                            $idIntervenant = $InfoI->idIntervenant;
                            $formatrice[$idIntervenant] = Array(
                                'NOM ' => $nom,
                                'PDV' => []
                            );
                            $validFormatrice = true;
                        } else {
                            $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                'ZONE' => 'INTERVENANT',
                                'RAISON' => trim($nom) . ' ' . trim($prenom) . ' - ' . $RechercheIntervenant2Exc->rowCount() == 0 ? 'INEXISTANT' : 'DOUBLON'
                            );
                        }

                        /** ---------------------------------------------------------------------------
                         * On rajoute donc les PDV a cette formatrice
                         */
                        if ($cip != 0) {
                            $RecherchePdvExc->bindValue(':codeMerval', $cip, PDO::PARAM_INT);
                            $RecherchePdvExc->execute();
                            if ($RecherchePdvExc->rowCount() == 1) {
                                $infoPdv = $RecherchePdvExc->fetch(PDO::FETCH_OBJ);
                                $idPdv = $infoPdv->idPdv;
                                $validCIP = true;
                            } else {

                                $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                    'ZONE' => 'PDV',
                                    'RAISON' => trim($cip) . ' ' . trim($Contenu[2]) . ' - ' . ($RecherchePdvExc->rowCount() == 0 ? ' INEXISTANT' : 'DOUBLON')
                                );
                            }
                        } else {
                            $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                'ZONE' => 'PDV',
                                'RAISON' => ' CODE CIP NON RENSEIGNE / NON CONFORME (NUMERIQUE)'
                            );
                        }

                        /** ---------------------------------------------------------------------------
                         * Le CHAMP DATE doit être valide
                         */
                        if ($date != '') {
                            $dateTmp = explode('/', date('d/m/Y', ($date - 25569) * 24 * 60 * 60));
                            if (!checkdate($dateTmp[1], $dateTmp[0], $dateTmp[2])) {
                                $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                    'ZONE' => 'DATE',
                                    'RAISON' => $date . ' - FORMAT NON RECONNU'
                                );
                            } else {
                                $validDate = true;
                                $dateF = $dateTmp[2] . '-' . $dateTmp[1] . '-' . $dateTmp[0];
                            }
                        } else {
                            $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                'ZONE' => 'DATE',
                                'RAISON' => 'NON RENSEIGNEE'
                            );
                        }

                        if ($horaire == '') {
                            $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                'ZONE' => 'HEURE',
                                'RAISON' => 'NON RENSEIGNEE'
                            );
                        } else {
                            if ($horaire >= 1) {
                                $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                    'ZONE' => 'HEURE',
                                    'RAISON' => $horaire . ' - NON CONFORME'
                                );
                            } else {
                                if ($horaire >= 0 && $horaire < 1) {
                                    $timeF = decimalDayToTime($horaire);
                                    $timeF = date('H:i:s', roundTime(strtotime($timeF), 'i', 15));
                                    $validTime = true;
                                }
                            }
                        }

                        /** ---------------------------------------------------------------------------
                         * La durée doit être vide (=1.5 heures) ou renseignée et valide
                         */
                        if ($duree == '') {
                            $duree = 1.5;
                            $validDuree = true;
                        } else {
                            if (is_float($duree)) {
                                $validDuree = true;
                            } else {
                                $anomalies[$idLigne + $nbLignesEntete - 1] = Array(
                                    'ZONE' => 'DUREE',
                                    'RAISON' => $duree . ' - NON CONFORME'
                                );
                            }
                        }

                        #echo 'MISSION '.filter_input(INPUT_POST, 'idMission_4_5').'<br/>';
                        #echo 'INTER '.$idIntervenant.'<br/>';
                        #echo 'IDPDV '.$idPdv.'<br/>';
                        #echo 'DATE '.$dateF.'<br/>';
                        #echo 'TIME '.$timeF.'<br/>';
                        #die();

                        /** ---------------------------------------------------------------------------
                         * On va mettre ça dans la table temporaire si TOUT EST OK
                         */
                        if ($validFormatrice && $validCIP && $validDate && $validTime && $validDuree) {
                            $insertTmpXLXExec->bindValue(':idm', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
                            $insertTmpXLXExec->bindValue(':idi', $idIntervenant, PDO::PARAM_INT);
                            $insertTmpXLXExec->bindValue(':idpdv', $idPdv, PDO::PARAM_INT);
                            $insertTmpXLXExec->bindValue(':dates', $dateF, PDO::PARAM_STR);
                            $insertTmpXLXExec->bindValue(':times', $timeF, PDO::PARAM_STR);
                            $insertTmpXLXExec->bindValue(':duree', $duree, PDO::PARAM_STR);
                            if (!$insertTmpXLXExec->execute()) {
                            }
                        } else {
                        }
                    }
                }
            }
        }

        /** ---------------------------------------------------------------------------------------
         * On prend maintenant toutes les lignes de cette insertion temporaire et on met tout
         * dans la base, dans la table.
         *
         * Du coup, on va refaire 2 fois le traitement. La première fois c'est pour vérifier
         * qu'on aura pas de problèmes.
         */
        $sqlGetTmpPDV = '
        SELECT *
        FROM tmp_xls
        WHERE mission_id = :idm
        ';
        $getTmpPDVExec = DbConnexion::getInstance()->prepare($sqlGetTmpPDV);
        $getTmpPDVExec->bindValue(':idm', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
        $getTmpPDVExec->execute();

        /** ---------------------------------------------------------------------------------------
         * ON VA ESSAYER DE METTRE CA DANS LES BONNES CASES, POUR CHAQUE LIGNE CONCERNEE
         */
        while ($getTmpPDV = $getTmpPDVExec->fetch(PDO::FETCH_OBJ)) {

            /** -----------------------------------------------------------------------------------
             * On récupère les données de base
             */
            $idinter = $getTmpPDV->inter_id;
            $idmission = $getTmpPDV->mission_id;
            $idpdv = $getTmpPDV->pdv_id;
            $du = $getTmpPDV->date;

           /** -----------------------------------------------------------------------------------
             * Certaines données sont donc recalculées
             */
            $du_time = $getTmpPDV->date.' '.$getTmpPDV->time;
            $tmp = new DateTime($getTmpPDV->date);
            $tmp->add(new DateInterval("PT" . $getTmpPDV->time*60 . "M"));
            $au_time = $tmp->format('Y-m-d H:i:s');

            /** -----------------------------------------------------------------------------------
             * Existe-t'il une INTERVENTION PREVUE, sur le même PDV, à la même DATE mais avec une
             * autre INTER ?
             */
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':idinter', $idinter, PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':idmission', $idmission, PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':dated', $du.' 00:00:00', PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':datef', $du.' 23:59:59', PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->execute();
            $getVerifOtherIntervenanteSamePDV = $getVerifOtherIntervenanteSamePDVExec->fetch(PDO::FETCH_OBJ);
            if ($getVerifOtherIntervenanteSamePDV->LeCount != 0) {
                $anomalies[] = Array(
                    'ZONE' => 'DUPP',
                    'RAISON' => 'MISSION + PDV + INTER'
                );
            }

            /** -----------------------------------------------------------------------------------
             * Existe-t'il une INTERVENTION PREVUE, sur le même PDV, à la même DATE mais avec
             * la même INTER ?
             */
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':idinter', $idinter, PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':idmission', $idmission, PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':dated', $du.' 00:00:00', PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->bindValue(':datef', $du.' 23:59:59', PDO::PARAM_INT);
            $getVerifOtherIntervenanteSamePDVExec->execute();
            $getVerifOtherIntervenanteSamePDV = $getVerifOtherIntervenanteSamePDVExec->fetch(PDO::FETCH_OBJ);
            if ($getVerifOtherIntervenanteSamePDV->LeCount != 0) {
                $anomalies[] = Array(
                    'ZONE' => 'DUPP',
                    'RAISON' => 'MISSION + PDV + INTER <>'
                );
            }

            /** -----------------------------------------------------------------------------------
             * Existe-t'il une INTERVENTION PREVUE, sur le même PDV, à la même DATE et AVEC
             * cette INTER ?
             */
            $getVerifInterventionSameDaySamePDVExec->bindValue(':idinter', $idinter, PDO::PARAM_INT);
            $getVerifInterventionSameDaySamePDVExec->bindValue(':idmission', $idmission, PDO::PARAM_INT);
            $getVerifInterventionSameDaySamePDVExec->bindValue(':dated', $du.' 00:00:00', PDO::PARAM_INT);
            $getVerifInterventionSameDaySamePDVExec->bindValue(':datef', $du.' 23:59:59', PDO::PARAM_INT);
            $getVerifInterventionSameDaySamePDVExec->execute();
            $getVerifInterventionSameDaySamePDV = $getVerifInterventionSameDaySamePDVExec->fetch(PDO::FETCH_OBJ);
            if ($getVerifInterventionSameDaySamePDV->LeCount != 0) {
                $anomalies[] = Array(
                    'ZONE' => 'DUPP',
                    'RAISON' => 'MISSION + PDV + INTER ='
                );
            }
        }

        /** ---------------------------------------------------------------------------------------
         * On arrive au bout !
         */

        /** ---------------------------------------------------------------------------------------
         * Dans un cas on stocke les anomalies, dans l'autre on stocke une chaine vide
         */
        $noerror = true;
        if (count($anomalies) > 0) {
            $noerror = false;
            $date = 'log/' . date('YmdHis') . '_resultTRT.txt';
            $fp = fopen($date, 'a+');
            fwrite($fp, json_encode($anomalies));
            fclose($fp);

        } else {

            $date = 'log/' . date('YmdHis') . '_resultTRT.txt';
            $fp = fopen($date, 'a+');
            fwrite($fp, '');
            fclose($fp);

        }

        if ($noerror) {
            
            $getTmpPDVExec->bindValue(':idm', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
            $getTmpPDVExec->execute();

            /** ---------------------------------------------------------------------------------------
             * ON VA ESSAYER DE METTRE CA DANS LES BONNES CASES, POUR CHAQUE LIGNE CONCERNEE
             */
            while ($getTmpPDV = $getTmpPDVExec->fetch(PDO::FETCH_OBJ)) {

                /** -----------------------------------------------------------------------------------
                 * On récupère les données
                 */
                $idinter = $getTmpPDV->inter_id;
                $idmission = $getTmpPDV->mission_id;
                $idpdv = $getTmpPDV->pdv_id;
                $du = $getTmpPDV->date;

                $du_time = $getTmpPDV->date.' '.$getTmpPDV->time;
                $tmp = new DateTime($du_time);
                $tmp->add(new DateInterval("PT" . $getTmpPDV->duree*60 . "M"));
                $au_time = $tmp->format('Y-m-d H:i:s');

                $ajoutInterExec->bindValue(':idinter',$idinter , PDO::PARAM_INT);
                $ajoutInterExec->bindValue(':idpdv', $idpdv, PDO::PARAM_INT);
                $ajoutInterExec->bindValue(':idmission', $idmission, PDO::PARAM_INT);
                $ajoutInterExec->bindValue(':dated', $du_time , PDO::PARAM_INT);
                $ajoutInterExec->bindValue(':datef', $au_time, PDO::PARAM_INT);
                $ajoutInterExec->execute();
                
            }
        }
        
        ?>
        <script>
            window.top.window.ClassFicheMission.resultImport('<?php print $date ?>', 'F');
        </script>
        <?php

    }
}
function decimalDayToTime($decimal)
{

    /*-----------------------------------
     * Attention, on est en jour, donc il
     * faut multiplier par 24 !
     */
    $day = round($decimal, 3) * 24;
    $hours = floor($day);
    $minutes = floor(($day - $hours) * 60);

    return str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' .
        str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':' .
        '00';
}

function roundTime($time, $entity = 'i', $value = 15)
{

    // prevent big loops
    if (strpos('is', $entity) === false) {
        return $time;
    }

    // up down counters
    $loopsUp = $loopsDown = 0;

    // loop up
    $loop = $time;
    while (date($entity, $loop) % $value != 0) {
        $loopsUp++;
        $loop++;
    }
    $return = $loop;


    // loop down
    $loop = $time;
    while (date($entity, $loop) % $value != 0) {
        $loopsDown++;
        $loop--;
        if ($loopsDown > $loopsUp) {
            $loop = $return;
            break;
        }
    }
    $return = $loop;

    // round seconds down
    if ($entity == 'i' && date('s', $return) != 0) {
        while (intval(date('s', $return)) != 0) {
            $return--;
        }
    }

    return $return;
}