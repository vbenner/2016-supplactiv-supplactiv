<?php

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/**
 * Modification du logo client
 */
$sqlModifPhotoIntervenant = '
UPDATE su_intervenant SET
  b64_photo = :lien
WHERE idIntervenant = :idIntervenant
 ';
$modifPhotoIntervenantExec = DbConnexion::getInstance()->prepare($sqlModifPhotoIntervenant);

# ------------------------------------------------------------------------------
# Initialisation de la variable de retour
# ------------------------------------------------------------------------------
$erreurRetour = '';

# ------------------------------------------------------------------------------
# Initialisation des formats acceptés
# ------------------------------------------------------------------------------
$formatFichier = array('image/png', 'image/jpeg');

# ------------------------------------------------------------------------------
# On recherche si un fichier est envoye
# ------------------------------------------------------------------------------
if (isset($_FILES['file']) && ($_FILES['file']['size'] > 0)) {

    $fichierPHOTO = $_FILES['file'];
    $infoFichier = finfo_open(FILEINFO_MIME_TYPE);
    $formatEnvoi = finfo_file($infoFichier, $fichierPHOTO['tmp_name']);

    if (in_array(strtolower($formatEnvoi), $formatFichier)) {
        @unlink('TEMP.jpg');
        if ($formatEnvoi == 'image/png') {
            $source = imagecreatefrompng($fichierPHOTO['tmp_name']);
        } elseif ($formatEnvoi == 'image/jpeg') {
            $source = imagecreatefromjpeg($fichierPHOTO['tmp_name']);
        }

        $largeur_source = imagesx($source);
        $hauteur_source = imagesy($source);

        $ratio = $largeur_source/$hauteur_source; // width/height
        if( $ratio > 1) {
            $width = 300;
            $height = 300/$ratio;
        }
        else {
            $width = 300*$ratio;
            $height = 300;
        }
        $destination = imagecreatetruecolor($width,$height);

        imagecopyresampled($destination, $source, 0, 0, 0, 0, $width,$height,$largeur_source,$hauteur_source);
        imagejpeg($destination, 'TEMP.jpg');
        rename('./TEMP.jpg', '../download/id.'.filter_input(INPUT_POST, 'idIntervenant').'.jpg');
        $modifPhotoIntervenantExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
        $modifPhotoIntervenantExec->bindValue(':lien', '../../../download/id.'.filter_input(INPUT_POST, 'idIntervenant').'.jpg', PDO::PARAM_STR);
        $modifPhotoIntervenantExec->execute();

        $ErreurRetour = 'La photo a bien &eacute;t&eacute; enregistr&eacute;e';
    } else {
        $ErreurRetour = 'Le format du fichier est incorrect (Format : ' . $FormatEnvoi . ') !';
    }
};

?>
