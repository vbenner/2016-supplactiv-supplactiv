<?php

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

$dossier_pj      = '../download/CV/';
if(isset($_FILES['file']['name'])) {

    /** -------------------------------------------------------------------------------------------
     * On récupère toutes les infos du fichier .XLS / .XLSX
     */
    $fic = basename($_FILES['file']['name']);
    $ext = pathinfo($fic, PATHINFO_EXTENSION);

    if(move_uploaded_file($_FILES['file']['tmp_name'], $dossier_pj . 'cv.'.$_POST['idIntervenant'].'.'.$ext)) {
        $sqlUpdateCV = '
        UPDATE su_intervenant
        SET b64_cv = :cv
        WHERE idIntervenant = :id
        ';
        $updateCVExec = DbConnexion::getInstance()->prepare($sqlUpdateCV);
        $updateCVExec->bindValue(':cv', '/download/CV/'.'cv.'.$_POST['idIntervenant'].'.'.$ext, PDO::PARAM_STR);
        $updateCVExec->bindValue(':id', $_POST['idIntervenant'], PDO::PARAM_STR);
        $updateCVExec->execute();
    }
}