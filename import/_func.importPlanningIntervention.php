<?php
set_time_limit(0);
ini_set('display_errors', 'on');
ini_set('memory_limit', '1020M');

/** Class PHPExcel  */
require_once dirname(__FILE__) . '/../includes/librairies/simplexlsx/simplexlsx.class.php';

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../includes/queries/queries.web2bdd.php';
require_once dirname(__FILE__) . '/../includes/queries/queries.bdd2web.php';

/**
 * Recherche de la presence intervenant
 */
$sqlRechercheIntervenant = "
SELECT *
FROM su_intervenant
WHERE idIntervenant = :idIntervenant";
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

$sqlRechercheIntervenant2 = "
SELECT *
FROM su_intervenant
WHERE UPPER(nomIntervenant) = :nomIntervenant AND UPPER(prenomIntervenant) = :prenomIntervenant LIMIT 1";
$RechercheIntervenant2Exc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant2);
/**
 * Recherche du point de vente
 */
$sqlRecherchePdv = "
SELECT *
FROM su_pdv
WHERE codeMerval = :codeMerval";
$RecherchePdvExc = DbConnexion::getInstance()->prepare($sqlRecherchePdv);

$sqlRechercheAnneeCampagneMission = "
SELECT DATE_FORMAT(dateDebut, '%Y') AS anneeCampagne
FROM su_mission
INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE idMission = :idMission";
$RechercheAnneeCampagneMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheAnneeCampagneMission);

$sqlRechercheInterventionAvecDate = '
SELECT *
FROM su_intervention
WHERE FK_idPdv = :idPdv AND FK_idIntervenant = :idIntervenant AND FK_idMission = :idMission AND dateDebut BETWEEN :dateDebut AND :dateFin';
$RechercheInterventionAvecDateExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionAvecDate);

/** @var $FormatFichier Format de fichier autorise */
$FormatFichier = array(
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
    'application/msword',
    'application/zip'
);

/** @var $CorrespondanceColonne Correspondance des colonnes BDD / Fichier */
$CorrespondanceColonne = array(
    0 => 'nomIntervenant',
    1 => 'prenomIntervenant',
    2 => 'codeClient',
    3 => 'nomCommercial',
    4 => 'libellePdv',
    5 => 'adressePdv',
    6 => 'codePostalPdv',
    7 => 'villePdv',
    8 => 'telephonePdv'
);

/** Initialisation des variables */
$nbLigneTotal = 0; $nbLigneOK = 0;

/** On test la presence du fichier */
if(isset($_FILES['fichierPDV']) && $_FILES['fichierPDV']['error'] == 0 && $_FILES['fichierPDV']['size'] > 0) {
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);

    /** @var $FormatEnvoi On test le format du fichier */
    $FormatEnvoi = finfo_file($InfoFichier, $_FILES['fichierPDV']['tmp_name']);

    if (in_array($FormatEnvoi, $FormatFichier)) {
        $xlsx = new SimpleXLSX($_FILES['fichierPDV']['tmp_name']);

        list($num_cols, $num_rows) = $xlsx->dimension();


        $Liste = json_decode(json_encode($xlsx->rows()));
        $moisPrecendent = null; $indice = 0;
        $ListeMois = array();
        $ListeIntervention = array();
        $boolLigneO = false;
        /** On parcours les lignes du fichier */
        foreach($Liste as $idLigne => $Contenu){

            /** S'il s'agit de la deuxieme ligne (0+1) on recupere les indices des mois */
            if($idLigne == 1 && !$boolLigneO){
                foreach($Contenu as $idColonne => $mois){
                    if(!empty($mois)){
                        $ListeMois[$mois] = array(
                            'debut' => $idColonne,
                            'fin' => 0
                        );

                        if(!is_null($moisPrecendent)){
                            $ListeMois[$moisPrecendent]['fin'] = $idColonne-1;
                        }
                        $moisPrecendent = $mois;
                    }
                    $indice++;
                }

                $ListeMois[$moisPrecendent]['fin'] = $indice;

                $d_ligne = 3;

            }



            if(!isset($ListeMois['01-JANVIER'])){
                $ListeMois = array(); $moisPrecendent = null; $indice = 0;
                if($idLigne == 0){

                    foreach($Contenu as $idColonne => $mois){
                        if(!empty($mois)){
                            $ListeMois[$mois] = array(
                                'debut' => $idColonne,
                                'fin' => 0
                            );

                            if(!is_null($moisPrecendent)){
                                $ListeMois[$moisPrecendent]['fin'] = $idColonne-1;
                            }
                            $moisPrecendent = $mois;
                        }
                        $indice++;
                    }

                    $ListeMois[$moisPrecendent]['fin'] = $indice;
                }

                if(isset($ListeMois['01-JANVIER'])){
                    $boolLigneO = true;

                }
                $d_ligne = 2;
            }

            /** Si la ligne est supérieur a 4 il s'agit d'une ligne couple intervenant / pdv */
            if($idLigne > $d_ligne){

                /** @var $Contenu  Contenu de la ligne*/
                $Contenu = (array)$Contenu;

                /** Si l'idIntervenant et le code merval sont present on continu */
                if(isset($Contenu[0]) && isset($Contenu[9])) {

                    /** On stock l'idIntervenant et l'idPdv */
                    $idIntervenant = 1; $idPdv = $Contenu[9];

                    $RechercheIntervenant2Exc->bindValue(':nomIntervenant', trim($Contenu[0]));
                    $RechercheIntervenant2Exc->bindValue(':prenomIntervenant', trim($Contenu[1]));
                    $RechercheIntervenant2Exc->execute();
                    if($RechercheIntervenant2Exc->rowCount() > 0){
                        $InfoI = $RechercheIntervenant2Exc->fetch(PDO::FETCH_OBJ);
                        $idIntervenant = $InfoI->idIntervenant;
                    }else $idIntervenant = trim($Contenu[0]).' '.trim($Contenu[1]);

                    if(trim($Contenu[0]) == '' && trim($Contenu[0]) == ''){
                        $idIntervenant = 1;
                    }

                    /** Creation du tableau de stockage des dates d'interventions */
                    if (!isset($ListeIntervention[$idIntervenant])) {
                        $ListeIntervention[$idIntervenant] = array();
                    }
                    if (!isset($ListeIntervention[$idIntervenant][$idPdv])) {
                        $ListeIntervention[$idIntervenant][$idPdv] = array();
                    }

                    /** On ajoute les infos */
                    foreach ($Contenu as $idColonne => $infoLigne) {
                        if ($idColonne >= $ListeMois['01-JANVIER']['debut'] && !empty($infoLigne)) {
                            array_push($ListeIntervention[$idIntervenant][$idPdv], array($idColonne => $infoLigne));
                        }
                    }
                }
            }
        }




        /** @var $listeJourAnnee Variable contenant les journees de l'annee */
        $listeJourAnnee = array();

        $RechercheAnneeCampagneMissionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5'));
        $RechercheAnneeCampagneMissionExc->execute();
        $infoCampagne = $RechercheAnneeCampagneMissionExc->fetch(PDO::FETCH_OBJ);

        /**
         * On parcours les indices des mois recherches
         * @var  $mois
         * @var  $indice
         */
        foreach($ListeMois as $mois => $indice){

            /** @var $infoMois On split le libellé du mois 01-JANVIER */
            $infoMois = preg_split('/-/',$mois);

            /** @var $idJour Initialisation de l'idJour */
            $idJour = 1;

            /** Indice de début et de fin */
            $indiceDebut = $indice['debut']+1;
            $indiceFin = ($infoMois[0] == 12) ? $indice['debut']+32: $indice['fin']+1;

            /** On parcours les jours du mois courant */
            for($i = $indiceDebut; $i < $indiceFin; $i++){
                $idJour = (strlen($idJour) < 2) ? '0'.$idJour : $idJour;
                $listeJourAnnee[$i] = $infoCampagne->anneeCampagne.'-'.$infoMois[0].'-'.$idJour;
                $idJour++;
            }
        }


        $resultatTraitement = array();


        /**
         * On parcours les interventions
         * @var  $idIntervenant
         * @var  $listePdv
         */
        foreach($ListeIntervention as $idIntervenant => $listePdv){

            /** Recherche de l'intervenant dans la BDD */
            $id_intervenant = 1; $infoIntervenant = null;
            $RechercheIntervenantExc->bindValue(':idIntervenant', $idIntervenant);
            $RechercheIntervenantExc->execute();
            if($RechercheIntervenantExc->rowCount() > 0){
                $infoIntervenant = $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ);
                $id_intervenant = $infoIntervenant->idIntervenant;
            }else {
                $infoIntervenant = (object)array('idIntervenant' => 99999, 'nomIntervenant' => "$idIntervenant", 'prenomIntervenant' => '');
                $id_intervenant = 99999;
            }

            if(!isset($resultatTraitement[$id_intervenant])){
                $resultatTraitement[$id_intervenant] = array(
                    'libelle' => addCaracToString($infoIntervenant->idIntervenant, 5, '0').' - '.$infoIntervenant->nomIntervenant.' '.$infoIntervenant->prenomIntervenant,
                    'pdv' => array()
                );
            }
            /**
             * On parcours les PDV associés
             * @var  $idPdv
             * @var  $dateIntervention
             */
            foreach($listePdv as $idPdv => $dateIntervention){

                $id_pdv = null; $infoPdv = null;

                $RecherchePdvExc->bindValue(':codeMerval', $idPdv);
                $RecherchePdvExc->execute();
                if($RecherchePdvExc->rowCount() > 0){
                    $infoPdv = $RecherchePdvExc->fetch(PDO::FETCH_OBJ);
                    $id_pdv = $infoPdv->idPdv;
                }

                /** On continu iniquement si le pdv est present en base */
                if(!is_null($id_pdv)) {

                    if(!isset($resultatTraitement[$id_intervenant]['pdv'][$id_pdv])){
                        $resultatTraitement[$id_intervenant]['pdv'][$id_pdv] = array(
                            'libelle' => $infoPdv->libellePdv.' - '.$infoPdv->codePostalPdv.' '.$infoPdv->villePdv,
                            'dates' => array()
                        );
                    }

                    foreach ($dateIntervention as $infoDate) {
                        foreach ($infoDate as $indice => $typeIntervention) {
                            if (isset($listeJourAnnee[$indice])) {
                                //print $indice . ' --> ' . $listeJourAnnee[$indice] . ' --> ' . $typeIntervention . '<br />';

                                $listeIntervention = array();

                                $dateDRecherche = $dateFRecherche = null;

                                /** @var $dateRecherche Formatage de la date */
                                $dateRecherche = new DateTime($listeJourAnnee[$indice] . ' 00:00:00');


                                $heureD = '10:00:00';
                                $heureF = '18:00:00';
                                switch ($typeIntervention) {
                                    case 'I1':
                                        $heureD = filter_input(INPUT_POST, 'ztI1_D') . ':00';
                                        $heureF = filter_input(INPUT_POST, 'ztI1_F') . ':00';
                                        break;
                                    case 'I2':
                                        $heureD = filter_input(INPUT_POST, 'ztI2_D') . ':00';
                                        $heureF = filter_input(INPUT_POST, 'ztI2_F') . ':00';
                                        break;
                                    case 'I3':
                                        $heureD = filter_input(INPUT_POST, 'ztI3_D') . ':00';
                                        $heureF = filter_input(INPUT_POST, 'ztI3_F') . ':00';
                                        break;
                                    case 'I4';
                                        $heureD = filter_input(INPUT_POST, 'ztI4_D') . ':00';
                                        $heureF = filter_input(INPUT_POST, 'ztI4_F') . ':00';
                                        break;
                                }

                                // On format la date de recherche debut et fin
                                $dateDRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . $heureD);
                                $dateFRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . $heureF);

                                // MAJ -> On va comparer la date de la campagne avec celle en cours
                                $boolTRT = true;
                                if ($infoCampagne->anneeCampagne == date('Y')) {
                                    $boolTRT = ($dateDRecherche->format('m') >= date('m')) ? true : false;
                                }

                                //print "TRAITEMENT -> ".$dateRecherche->format('Y-m-d').' : '.$boolTRT.'<br />';

                                if ($boolTRT && $id_intervenant != 99999) {
                                    /** On recherche les dates d'interventions */
                                    $RechercheAutreInterventionExc->bindValue(':dateDebut', $dateRecherche->format('Y-m-d') . ' 00:00:00');
                                    $RechercheAutreInterventionExc->bindValue(':dateFin', $dateRecherche->format('Y-m-d') . ' 23:59:59');
                                    $RechercheAutreInterventionExc->bindValue(':idIntervention', 1);
                                    $RechercheAutreInterventionExc->execute();
                                    while ($InfoIntervention = $RechercheAutreInterventionExc->fetch(PDO::FETCH_OBJ)) {

                                        /** Initialisation des Booleen de camparaison */
                                        $boolDate = $boolIntervenant = $boolPdv = false;
                                        $dateD = $dateF = null;
                                        if (!is_null($InfoIntervention->dateDebut)) {
                                            $dateD = new DateTime($InfoIntervention->dateDebut);
                                            $dateF = new DateTime($InfoIntervention->dateFin);
                                        }

                                        if (!is_null($dateD) && !is_null($dateDRecherche) && (
                                                ($dateDRecherche < $dateD && $dateFRecherche > $dateD) ||
                                                ($dateDRecherche < $dateF && $dateFRecherche > $dateF) ||
                                                ($dateDRecherche > $dateD && $dateFRecherche < $dateF))
                                        ) {
                                            $boolDate = true;
                                        }

                                        if (!is_null($dateD) && !is_null($dateDRecherche) && $dateD == $dateDRecherche && $dateF == $dateFRecherche) {
                                            $boolDate = true;
                                        }

                                        /** Cas 1 -> Le point de vente est le meme que celui de l'intervention
                                         * NON bloquant
                                         */
                                        if ($InfoIntervention->FK_idPdv == $id_pdv && $boolDate) {
                                            $boolPdv = true;
                                        }

                                        /** Cas 2 -> L'intervenant est le meme que celui de l'intervention
                                         * BLOQUANT si horaire similaire
                                         */
                                        if ($InfoIntervention->FK_idIntervenant == $id_intervenant && $InfoIntervention->FK_idIntervenant != 1 && $boolDate) {
                                            $boolIntervenant = true;
                                        }

                                        if ($boolDate && ($boolPdv || $boolIntervenant)) {
                                            $listeIntervention[$InfoIntervention->idIntervention] = array(
                                                'info' => $InfoIntervention,
                                                'boolDate' => $boolDate,
                                                'boolPdv' => $boolPdv,
                                                'boolIntervenant' => $boolIntervenant
                                            );
                                        }
                                    }


                                    $InfoInterventionTrouve = null;
                                    /**  On parcours une premiere fois pour verifier si l'intervention existe */
                                    $boolInterventionTrouve = false;
                                    foreach ($listeIntervention as $idIntervention => $info) {
                                        if ($info['boolIntervenant'] && $info['boolDate'] && $info['boolPdv']) {
                                            $boolInterventionTrouve = true;
                                            $InfoInterventionTrouve = $info['info'];
                                        }
                                    }

                                    /** Si l'intervention n'est pas en BDD on verifie qu'il n'y a pas de probleme avec d'autres dates */
                                    if (!$boolInterventionTrouve) {

                                        if (in_array($typeIntervention, array('1', 'I1', 'I2', 'I3', 'I4'))) {
                                            $boolErreur = false;
                                            $couleurAlert = '';
                                            $lstInterventionMin = array();
                                            foreach ($listeIntervention as $idIntervention => $infos) {

                                                if ($infos['boolIntervenant']) $boolErreur = true;
                                                $info = $infos['info'];

                                                array_push($lstInterventionMin, array(
                                                    'boolIntervenant' => $infos['boolIntervenant'],
                                                    'dteDebut' => $info->dteDebut,
                                                    'hDebut' => $info->hDebut,
                                                    'hFin' => $info->hFin,
                                                    'libellePdv' => $info->libellePdv,
                                                    'nomIntervenant' => $info->nomIntervenant,
                                                    'prenomIntervenant' => $info->prenomIntervenant,
                                                    'libelleGamme' => $info->libelleGamme,
                                                    'idIntervenant' => $info->idIntervenant,
                                                    'idPdv' => $info->idPdv,
                                                    'idIntervention' => $info->idIntervention

                                                ));
                                                //print ervenant . ' pour la gamme <strong>' . $info->libelleGamme . '</strong>';
                                            }

                                            if (sizeof($lstInterventionMin) == 0) {

                                                $AjoutInterventionAvecDateExc->bindValue(':idIntervenant', $id_intervenant, PDO::PARAM_INT);
                                                $AjoutInterventionAvecDateExc->bindValue(':idPdv', $id_pdv, PDO::PARAM_INT);
                                                $AjoutInterventionAvecDateExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
                                                $AjoutInterventionAvecDateExc->bindValue(':dateDebut', $dateDRecherche->format('Y-m-d H:i:s'), PDO::PARAM_STR);
                                                $AjoutInterventionAvecDateExc->bindValue(':dateFin', $dateFRecherche->format('Y-m-d H:i:s'), PDO::PARAM_STR);
                                                if (!$AjoutInterventionAvecDateExc->execute()) {
                                                    print_r($AjoutInterventionAvecDateExc->errorInfo());
                                                }

                                                array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                    'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                                                    'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                                                    'statut' => 1,
                                                    'infos' => ''
                                                ));
                                            } else {

                                                if (!$boolErreur) {
                                                    $AjoutInterventionAvecDateExc->bindValue(':idIntervenant', $id_intervenant, PDO::PARAM_INT);
                                                    $AjoutInterventionAvecDateExc->bindValue(':idPdv', $id_pdv, PDO::PARAM_INT);
                                                    $AjoutInterventionAvecDateExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
                                                    $AjoutInterventionAvecDateExc->bindValue(':dateDebut', $dateDRecherche->format('Y-m-d H:i:s'), PDO::PARAM_STR);
                                                    $AjoutInterventionAvecDateExc->bindValue(':dateFin', $dateFRecherche->format('Y-m-d H:i:s'), PDO::PARAM_STR);
                                                    if (!$AjoutInterventionAvecDateExc->execute()) {
                                                        print_r($AjoutInterventionAvecDateExc->errorInfo());
                                                    }
                                                }
                                                array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                    'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                                                    'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                                                    'statut' => 1,
                                                    'infos' => $lstInterventionMin
                                                ));
                                            }


                                        } else {
                                            #print $id_intervenant.' - '.$id_pdv.' - '.$dateDRecherche->format('Y-m-d');
                                            $RechercheInterventionAvecDateExc->bindValue(':idIntervenant', $id_intervenant, PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':idPdv', $id_pdv, PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':dateDebut', $dateDRecherche->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
                                            $RechercheInterventionAvecDateExc->bindValue(':dateFin', $dateFRecherche->format('Y-m-d') . ' 23:59:59', PDO::PARAM_STR);
                                            $RechercheInterventionAvecDateExc->execute();
                                            if ($RechercheInterventionAvecDateExc->rowCount() > 0) {
                                                while ($InfoInter = $RechercheInterventionAvecDateExc->fetch(PDO::FETCH_OBJ)) {
                                                    if (empty($InfoInter->FK_idContrat)) {
                                                        $SuppressionInterventionExc->bindValue(':idIntervention', $InfoInter->idIntervention);
                                                        $SuppressionInterventionExc->execute();
                                                        array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                            'dateD' => $dateDRecherche->format('d/m/Y'),
                                                            'dateF' => $dateFRecherche->format('d/m/Y'),
                                                            'statut' => 3,
                                                            'infos' => ''
                                                        ));
                                                    }
                                                }
                                            }
                                        }
                                    } else {

                                        /** L'intervention est a supprimé */
                                        if (!in_array($typeIntervention, array('1', 'I1', 'I2', 'I3', 'I4'))) {
                                            //print $id_intervenant . ' - ' . $id_pdv . ' - ' . $dateDRecherche->format('Y-m-d') . '<br />';
                                            $RechercheInterventionAvecDateExc->bindValue(':idIntervenant', $id_intervenant, PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':idPdv', $id_pdv, PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission_4_5'), PDO::PARAM_INT);
                                            $RechercheInterventionAvecDateExc->bindValue(':dateDebut', $dateDRecherche->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
                                            $RechercheInterventionAvecDateExc->bindValue(':dateFin', $dateFRecherche->format('Y-m-d') . ' 23:59:59', PDO::PARAM_STR);
                                            $RechercheInterventionAvecDateExc->execute();
                                            if ($RechercheInterventionAvecDateExc->rowCount() > 0) {
                                                while ($InfoInter = $RechercheInterventionAvecDateExc->fetch(PDO::FETCH_OBJ)) {
                                                    if (empty($InfoInter->FK_idContrat)) {
                                                        $SuppressionInterventionExc->bindValue(':idIntervention', $InfoInter->idIntervention);
                                                        $SuppressionInterventionExc->execute();
                                                        array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                            'dateD' => $dateDRecherche->format('d/m/Y'),
                                                            'dateF' => $dateFRecherche->format('d/m/Y'),
                                                            'statut' => 3,
                                                            'infos' => ''
                                                        ));
                                                    } else {
                                                        array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                            'dateD' => $dateDRecherche->format('d/m/Y'),
                                                            'dateF' => $dateFRecherche->format('d/m/Y'),
                                                            'statut' => 10,
                                                            'infos' => '',
                                                            'idIntervention' => $InfoInter->idIntervention,
                                                            'idContrat' => $InfoInter->FK_idContrat
                                                        ));
                                                    }
                                                }
                                            }
                                        } else {

                                            array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                                                'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                                                'statut' => ($InfoInterventionTrouve->FK_idMission == filter_input(INPUT_POST, 'idMission_4_5')) ? 0 : 11,
                                                'infos' => '<div class="alert alert-danger">L\'intervenant effectue déja une intervention pour une autre mission</div>',
                                                'idIntervention' => $InfoInterventionTrouve->idIntervention
                                            ));
                                        }
                                    }
                                }

                                else {
                                    if($boolTRT) {
                                        if ($id_intervenant == 99999) {
                                            array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                                                'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                                                'statut' => 14,
                                                'infos' => '<div class="alert alert-danger">L\'intervenant <strong>' . $resultatTraitement[$id_intervenant]['libelle'] . '</strong> est introuvable</div>'
                                            ));
                                        } else {
                                            array_push($resultatTraitement[$id_intervenant]['pdv'][$id_pdv]['dates'], array(
                                                'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                                                'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                                                'statut' => 12,
                                                'infos' => ''
                                            ));
                                        }
                                    }
                                }
                            } else print 'JOUR PAS TROUVE';
                        }
                    }


                }

                else {

                    $dateDRecherche = $dateFRecherche = null;

                    /** @var $dateRecherche Formatage de la date */
                    $dateRecherche = new DateTime($listeJourAnnee[$indice] . ' 00:00:00');


                    $heureD = '10:00:00';
                    $heureF = '18:00:00';
                    switch ($typeIntervention) {
                        case 'I1':
                            $heureD = filter_input(INPUT_POST, 'ztI1_D') . ':00';
                            $heureF = filter_input(INPUT_POST, 'ztI1_F') . ':00';
                            break;
                        case 'I2':
                            $heureD = filter_input(INPUT_POST, 'ztI2_D') . ':00';
                            $heureF = filter_input(INPUT_POST, 'ztI2_F') . ':00';
                            break;
                        case 'I3':
                            $heureD = filter_input(INPUT_POST, 'ztI3_D') . ':00';
                            $heureF = filter_input(INPUT_POST, 'ztI3_F') . ':00';
                            break;
                        case 'I4';
                            $heureD = filter_input(INPUT_POST, 'ztI4_D') . ':00';
                            $heureF = filter_input(INPUT_POST, 'ztI4_F') . ':00';
                            break;
                    }

                    // On format la date de recherche debut et fin
                    $dateDRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . $heureD);
                    $dateFRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . $heureF);

                    // MAJ -> On va comparer la date de la campagne avec celle en cours
                    $boolTRT = true;
                    if ($infoCampagne->anneeCampagne == date('Y')) {
                        $boolTRT = ($dateDRecherche->format('m') >= date('m')) ? true : false;
                    }

                    if($idPdv != '' && $boolTRT) {
                        if (!isset($resultatTraitement[$id_intervenant]['pdv'][$idPdv])) {
                            $resultatTraitement[$id_intervenant]['pdv'][$idPdv] = array(
                                'libelle' => $idPdv . ' - INTROUVABLE',
                                'dates' => array()
                            );
                        }

                        array_push($resultatTraitement[$id_intervenant]['pdv'][$idPdv]['dates'], array(
                            'dateD' => $dateDRecherche->format('d/m/Y H:i'),
                            'dateF' => $dateFRecherche->format('d/m/Y H:i'),
                            'statut' => 13,
                            'infos' => '<div class="alert alert-danger">Le point de vente <strong>' . $idPdv . '</strong> est introuvable</div>'
                        ));
                    }
                }
            }
        }

        $date = 'log/'.date('YmdHis').'_resultTRT.txt';
        $fp= fopen($date, 'a+');
        fwrite($fp, json_encode($resultatTraitement));
        fclose($fp);

        ?>
<script>
    window.top.window.ClassFicheMission.resultImport('<?php print $date ?>');
</script>
        <?php
    }
}