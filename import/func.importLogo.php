<?php

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../_config/config.sql.php';

/**
 * Modification du logo client
 */
$sqlModifLogoClient = '
UPDATE su_client SET
  logoClient = :logoClient
  WHERE idClient = :idClient';
$ModifLogoClientExc = DbConnexion::getInstance()->prepare($sqlModifLogoClient);

$sqlModifSignatureClient = '
UPDATE su_client SET
  signatureClient = :signatureClient
WHERE idClient = :idClient';
$ModifSignatureClientExc = DbConnexion::getInstance()->prepare($sqlModifSignatureClient);

# ------------------------------------------------------------------------------
# Initialisation de la variable de retour
# ------------------------------------------------------------------------------
$ErreurRetour = '';

# ------------------------------------------------------------------------------
# Fonction permettant de convertir une image en base64
# ------------------------------------------------------------------------------
function file2base64($source)
{
    $bin_data_source = fread(fopen($source, "r"), filesize($source));
    $b64_data_source = base64_encode($bin_data_source);
    return $b64_data_source;
}

# ------------------------------------------------------------------------------
# Initialisation des formats accept�s
# ------------------------------------------------------------------------------
$FormatFichier = array('image/png');

# ------------------------------------------------------------------------------
# On recherche si un fichier est envoye
# ------------------------------------------------------------------------------
if (isset($_FILES['fichierLOGO']) && ($_FILES['fichierLOGO']['size'] > 0)) {

    $FichierLOGO = $_FILES['fichierLOGO'];
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);
    $FormatEnvoi = strtolower(finfo_file($InfoFichier, $FichierLOGO['tmp_name']));

    if (in_array($FormatEnvoi, $FormatFichier)) {
        @unlink('TEMP.jpg');
        $source = imagecreatefrompng($FichierLOGO['tmp_name']);

        $largeur_source = imagesx($source);
        $hauteur_source = imagesy($source);

        $Hauteur_Calc = 130;
        $destination = imagecreatetruecolor(200, $Hauteur_Calc);
        $largeur_destination = imagesx($destination);
        $hauteur_destination = imagesy($destination);

        imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
        imagejpeg($destination, 'TEMP.jpg');

        $Image64 = file2base64('TEMP.jpg');

        $ModifLogoClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient_LOGO'), PDO::PARAM_INT);
        $ModifLogoClientExc->bindValue(':logoClient', $Image64, PDO::PARAM_STR);
        $ModifLogoClientExc->execute();

        $ErreurRetour = 'Le logo a bien &eacute;t&eacute; enregistr&eacute;';
    } else {
        $ErreurRetour = 'Le format du fichier est incorrect (Format : ' . $FormatEnvoi . ') !';
    }
} else if (isset($_FILES['fichierSIGNATURE']) && ($_FILES['fichierSIGNATURE']['size'] > 0)) {
    $FichierLOGO = $_FILES['fichierSIGNATURE'];
    $InfoFichier = finfo_open(FILEINFO_MIME_TYPE);
    $FormatEnvoi = finfo_file($InfoFichier, $FichierLOGO['tmp_name']);

    if (in_array($FormatEnvoi, $FormatFichier)) {
        @unlink('TEMP.jpg');
        $source = imagecreatefrompng($FichierLOGO['tmp_name']);

        $largeur_source = imagesx($source);
        $hauteur_source = imagesy($source);

        $Hauteur_Calc = 130;
        $destination = imagecreatetruecolor(200, $Hauteur_Calc);
        $largeur_destination = imagesx($destination);
        $hauteur_destination = imagesy($destination);

        imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
        imagejpeg($destination, 'TEMP.jpg');

        $Image64 = file2base64('TEMP.jpg');

        $ModifSignatureClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient_SIGNATURE'), PDO::PARAM_INT);
        $ModifSignatureClientExc->bindValue(':signatureClient', $Image64, PDO::PARAM_STR);
        $ModifSignatureClientExc->execute();

        $ErreurRetour = 'La signature a bien &eacute;t&eacute; enregistr&eacute;e';

    } else $ErreurRetour = 'Le format du fichier est incorrect (Format : ' . $FormatEnvoi . ') !';
} else $ErreurRetour = 'Aucun fichier r&eacute;ceptionn&eacute; !';

?>

<script>
    window.top.window.toastr.info('<?php echo $ErreurRetour ?>', 'Import fichier');
    window.top.window.ClassFicheClient.rechercheInfoClient();
</script>