<?php

include '../_config/config.sql.php';

$Nom_MOIS = array(
    "01"=>"JANVIER",
    "02"=>"FEVRIER",
    "03" => "MARS",
    "04"=> "AVRIL",
    "05" => "MAI" ,
    "06"=>"JUIN",
    "07"=>"JUILLET",
    "08"=>"AOUT",
    "09"=>"SEPTEMBRE",
    "10"=>"OCTOBRE",
    "11"=>"NOVEMBRE",
    "12"=>"DECEMBRE"
);

$sqlRechercheDateCidd = 'SELECT * FROM su_contrat_cidd';
$RechercheDateCiddExc = DbConnexion::getInstance()->prepare($sqlRechercheDateCidd);

$sqlMajDate = 'UPDATE su_contrat_cidd SET dateContrat = :dte WHERE idContrat = :idContrat';
$MajDateExc = DbConnexion::getInstance()->prepare($sqlMajDate);

$RechercheDateCiddExc->execute();
while($InfoDate = $RechercheDateCiddExc->fetch(PDO::FETCH_OBJ)){
    $dte = preg_split('/ /', $InfoDate->dateContrat);

    $MajDateExc->bindValue(':idContrat', $InfoDate->idContrat);
    $MajDateExc->bindValue(':dte', $dte[1].'-'.array_search($dte[0], $Nom_MOIS).'-01');
    #$MajDateExc->execute();
}