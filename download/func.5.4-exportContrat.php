<?php
/** -----------------------------------------------------------------------------------------------
 * Export des contrats
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 *
 * 1.0.1 : on rajoute un répertoire current + nouvelle librairie Html2Pdf
 *
 */
ini_set('display_errors','on');

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Class HTML2PDF */
#require_once __DIR__ . '/../current/includes/librairies/html2pdf_v4.03/html2pdf.class.php';
require __DIR__.'/../current/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

/** Recherche des informations sur le contrat */
$RechercheInfoContratExc->bindValue(':idContrat', filter_input(INPUT_GET, 'idContrat'));
$RechercheInfoContratExc->execute();
$infoContrat = $RechercheInfoContratExc->fetch(PDO::FETCH_OBJ);

#echo 'AVANT';
#echo '<pre>';
#var_dump($infoContrat);
#echo '</pre>';
#return;

ob_start();
if ($infoContrat->AutoE == 'OUI') {
    include(dirname(__FILE__).'/_html/contratApplication.php');
} else {
    include(dirname(__FILE__).'/_html/contratIntervention.php');
}
$content = ob_get_clean();

if(!is_dir('CDD/'.addCaracToString($infoContrat->idIntervenant, 5, '0').'/')){
    mkdir('CDD/'.addCaracToString($infoContrat->idIntervenant, 5, '0'));
}


$html2pdf = new Html2Pdf('P', 'A4', 'fr');
$html2pdf->setDefaultFont('Arial');
$html2pdf->writeHTML($content, isset($_GET['vuehtml']));

$html2pdf->Output('Contrat_'.$infoContrat->nomIntervenant.'_'.$infoContrat->prenomIntervenant.'_'.addCaracToString($infoContrat->idContrat, 6, '0').'_DU_'.date('d-m-Y').'.pdf');
$html2pdf->Output('CDD/'.addCaracToString($infoContrat->idIntervenant, 5, '0').'/'.'Contrat_'.$infoContrat->nomIntervenant.'_'.$infoContrat->prenomIntervenant.'_'.addCaracToString($infoContrat->idContrat, 6, '0').'_DU_'.date('d-m-Y').'.pdf', 'F');
