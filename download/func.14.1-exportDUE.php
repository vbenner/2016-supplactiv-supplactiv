<?php
/**
 * Export des DUE
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** @var $HeaderXML Entete du fichier XML */
$HeaderXML = '<?xml version="1.0" encoding="ISO-8859-1"?>
<FR_DUE_Upload xmlns:cct="urn:oasis:names:tc:ubl:CoreComponentTypes:1.0:0.70" xmlns:rxdt="http://www.repxml.org/DataTypes" xmlns:rxorg="http://www.repxml.org/Organization" xmlns:rxpadr="http://www.repxml.org/PostalAddress" xmlns:rxpers="http://www.repxml.org/Person_Identity" xmlns:rxphadr="http://www.repxml.org/PhoneAddress" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="DUE_Upload.xsd">
	<FR_DUE_Upload.Test.Indicator>120</FR_DUE_Upload.Test.Indicator>
	<FR_DuesGroup>';

/** @var $InfoSociete Informations sur Suppl'ACTIV */
$InfoSociete = '
<FR_Employer>
    <FR_EmployerIdentity>
        <rxorg:FR_Organization.SIRET.Identifier>52814715000023</rxorg:FR_Organization.SIRET.Identifier>
        <rxorg:FR_Organization.Designation.Text>SUPPL ACTIV</rxorg:FR_Organization.Designation.Text>
        <rxorg:FR_Organization.APE.Code>7311Z</rxorg:FR_Organization.APE.Code>
    </FR_EmployerIdentity>
    <FR_Employer.URSSAF.Code>267</FR_Employer.URSSAF.Code>
    <FR_EmployerAddress>
        <rxpadr:FR_PostalAddress.StreetDesignation.Text>7 BOULEVARD REMBRANDT</rxpadr:FR_PostalAddress.StreetDesignation.Text>
        <rxpadr:FR_PostalAddress.Town.Text>DIJON</rxpadr:FR_PostalAddress.Town.Text>
        <rxpadr:FR_PostalAddress.Postal.Code>21000</rxpadr:FR_PostalAddress.Postal.Code>
    </FR_EmployerAddress>
    <FR_EmployerContact>
        <FR_PhoneNumber>
            <rxphadr:FR_PhoneAddress.PhoneNumber.Text>0345427023</rxphadr:FR_PhoneAddress.PhoneNumber.Text>
        </FR_PhoneNumber>
    </FR_EmployerContact>
</FR_Employer>';

/** @var $FooterXML Fin du fichier XML */
$FooterXML = '
	</FR_DuesGroup>
</FR_DUE_Upload>';

/** On test la presence des contrats */
if(isset($_SESSION['listeContrat_14_1']) && !empty($_SESSION['listeContrat_14_1'])){

    $ListeInfoUsr = '';

    /** On parcours les contrats */
    foreach($_SESSION['listeContrat_14_1'] as $idContrat) {

        $listeHeure = array(); $minDate = null; $maxDate = null; $nbIntervention = 1;

        /** Recherche des heures d'intervention */
        $RechercheNbMinContratExc->bindValue(':idContrat', $idContrat, PDO::PARAM_INT);
        $RechercheNbMinContratExc->execute();
        while ($InfoHeure = $RechercheNbMinContratExc->fetch(PDO::FETCH_OBJ)) {
            $dateIntervention = new DateTime($InfoHeure->dateDebut);
            if(!isset($listeHeure[$dateIntervention->format('d/m/Y')])){
                $listeHeure[$dateIntervention->format('d/m/Y')] = 0;
            }
            $listeHeure[$dateIntervention->format('d/m/Y')] += $InfoHeure->nbMin;

            if(is_null($minDate)){
                $minDate = $dateIntervention;
            }

            if($nbIntervention == $RechercheNbMinContratExc->rowCount()){
                $maxDate = $dateIntervention;
            }
            
            $nbIntervention++;
        }

        /** Regle Suppl'ACIV... Si durée supérieur à 8H on passe à 7....... */
        $nbMinFinal = 0;
        foreach ($listeHeure as $date => $nbMin) {
            $nbMinFinal += ($nbMin >= 420) ? 420 : $nbMin;
        }

        /** Recherche des infos sur le contrat */
        $RechercheInfoIntervenantExc->bindValue(':idContrat', $idContrat, PDO::PARAM_INT);
        $RechercheInfoIntervenantExc->execute();
        while ($InfoContrat = $RechercheInfoIntervenantExc->fetch(PDO::FETCH_OBJ)) {
            $NumeroSS = trim(str_replace(' ','',$InfoContrat->numeroSS));
            $Genre  = (in_array($InfoContrat->genre, array('E', 'L'))) ? 2 : 1;
            
            $ListeInfoUsr .=  '
			<FR_EmployeeGroup>
				<FR_Employee>
					<FR_EmployeeIdentity>
						<rxpers:FR_PersonIdentity.Surname.Text>'.substr(deleteSpecialCarac($InfoContrat->nomIntervenant),0,32).'</rxpers:FR_PersonIdentity.Surname.Text>
						<rxpers:FR_PersonIdentity.ChristianName.Text>'.substr(deleteSpecialCarac($InfoContrat->prenomIntervenant),0,16).'</rxpers:FR_PersonIdentity.ChristianName.Text>
						<rxpers:FR_PersonIdentity.Sex.Code>'.$Genre.'</rxpers:FR_PersonIdentity.Sex.Code>
						<rxpers:FR_NNI>
							<rxpers:FR_NNI.NIR.Identifier>'.substr($NumeroSS,0,13).'</rxpers:FR_NNI.NIR.Identifier>
							<rxpers:FR_NNI.NIRKey.Text>'.substr($NumeroSS,-2).'</rxpers:FR_NNI.NIRKey.Text>
						</rxpers:FR_NNI>
						<rxpers:FR_Birth>
							<rxpers:FR_Birth.Date>'.$InfoContrat->dateNaissance.'</rxpers:FR_Birth.Date>
							<rxpers:FR_Birth.Town.Text>'.substr(deleteSpecialCarac($InfoContrat->villeNaissance),0,32).'</rxpers:FR_Birth.Town.Text>
						</rxpers:FR_Birth>
					</FR_EmployeeIdentity>
					<FR_EmployeeComplement>
						<FR_EmployeeComplement.Birth_Department.Code>'.$InfoContrat->depNaissanceDUE.'</FR_EmployeeComplement.Birth_Department.Code>
					</FR_EmployeeComplement>
				</FR_Employee>
				<FR_Contract>
					<FR_Contract.StartContract.Date>'.$minDate->format('Y-m-d').'</FR_Contract.StartContract.Date>
					<FR_Contract.StartContract.Time>'.$minDate->format('H:i:s').'</FR_Contract.StartContract.Time>
					<FR_Contract.EndContract.Date>'.$maxDate->format('Y-m-d').'</FR_Contract.EndContract.Date>
					<FR_Contract.Nature.Code>CDD</FR_Contract.Nature.Code>
					<FR_Contract.HealthService.Text>062</FR_Contract.HealthService.Text>
					<FR_Contract.TrialTime.Text>'.afficheHeureEssai($nbMinFinal).'</FR_Contract.TrialTime.Text>
				</FR_Contract>
			</FR_EmployeeGroup>';
        }
    }

    /** Creation du fichier DUE */
    $fp = fopen("DUE/DUE_".date('Y-m-d').".xml",'w+');
    fwrite($fp, $HeaderXML.$InfoSociete.$ListeInfoUsr.$FooterXML);
    fclose($fp);

    forcerTelechargement("DUE_".date('Y-m-d').".xml", "DUE/DUE_".date('Y-m-d').".xml", 100000);

}