<?php
/**
 * Export des documents magasin
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('display_errors','on');

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Class HTML2PDF */
#require_once __DIR__ . '/../current/includes/librairies/html2pdf_v4.03/html2pdf.class.php';
require __DIR__.'/../current/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

/** Recherche des informations sur le contrat */
$RechercheInfoInterventionExc->bindValue(':idIntervention', filter_input(INPUT_GET, 'idIntervention'));
$RechercheInfoInterventionExc->execute();
$infoIntervention = $RechercheInfoInterventionExc->fetch(PDO::FETCH_OBJ);

ob_start();
include(dirname(__FILE__).'/_html/docMagasin.php');
$content = ob_get_clean();


if(!is_dir('CDD/'.addCaracToString($infoIntervention->FK_idIntervenant, 5, '0').'/')){
    mkdir('CDD/'.addCaracToString($infoIntervention->FK_idIntervenant, 5, '0'));
}


$html2pdf = new Html2Pdf('P', 'A4', 'fr');
$html2pdf->setDefaultFont('Arial');
$html2pdf->writeHTML($content, isset($_GET['vuehtml']));

$html2pdf->Output($infoIntervention->nomIntervenant.'_'.$infoIntervention->dIntervention.'_'.$infoIntervention->libellePdv.'.pdf');
$html2pdf->Output('CDD/'.addCaracToString($infoIntervention->FK_idIntervenant, 5, '0').'/Document_Entre_PDV_'.str_replace('/', '.',$infoIntervention->dateIntervention).'_'.str_replace(' ', '-',$infoIntervention->libellePdv).'.pdf', 'F');