<?php
/** -----------------------------------------------------------------------------------------------
 * Export pour POLE EMPLOI
 */
ini_set('display_errors', 'on');
set_time_limit(0);
ini_set('memory_limit', -1);

/** -----------------------------------------------------------------------------------------------
 * Connexion a la base de donnees
 */
require_once __DIR__ . '/../current/_config/config.sql.php';
#require_once  '../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * MOIS / YEAR
 */
$month = $_POST['lstMonth'];
$year = $_POST['lstYear'];
$filename = 'pe_'.$year.'_'.str_pad($month, 2, '0', STR_PAD_LEFT).'.csv';

/** -----------------------------------------------------------------------------------------------
 * Création du fichier
 */
$ff = fopen($filename, "w") or die("Unable to open file!");

/** -----------------------------------------------------------------------------------------------
 * Requête
 * #1 - on récupère tous ceux qui ont travaillé durant cette période
 */
$sqlExportListeIntervenant = '
SELECT DISTINCT SIV.FK_idIntervenant, SI.nomIntervenant, SI.prenomIntervenant
FROM su_intervention SIV
    INNER JOIN su_intervenant SI ON SIV.FK_idIntervenant = SI.idIntervenant
WHERE NOT ISNULL(SIV.FK_idContrat)
AND YEAR(SIV.dateDebut) = :year
AND MONTH(SIV.dateDebut) = :month
AND SI.boolAutoEntrepreneuse = :auto
GROUP BY SIV.FK_idIntervenant
ORDER BY SIV.FK_idIntervenant

';
$exportListeIntervenantExec = DbConnexion::getInstance()->prepare($sqlExportListeIntervenant);
$exportListeIntervenantExec->bindValue(':year', $year, PDO::PARAM_INT);
$exportListeIntervenantExec->bindValue(':month', $month, PDO::PARAM_INT);
$exportListeIntervenantExec->bindValue(':auto', 'NON', PDO::PARAM_STR);
$exportListeIntervenantExec->execute();
$csv = '';
while($exportListeIntervenant = $exportListeIntervenantExec->fetch(PDO::FETCH_OBJ)) {
    $lig = $exportListeIntervenant->FK_idIntervenant.';'.
        $exportListeIntervenant->nomIntervenant.';'.
        $exportListeIntervenant->prenomIntervenant.';';

    /** -------------------------------------------------------------------------------------------
     * On va donc en profiter pour savoir sur quel PDV ils ont travaillé
     */
    $sqlLastIntervention = '
    SELECT DATE_FORMAT(SIV.dateFin, \'%d/%m/%Y\') AS dateMax, SP.adressePdv_A, 
    SP.codePostalPdv, SP.villePdv
    FROM su_intervention SIV
        INNER JOIN su_pdv SP on SIV.FK_idPdv = SP.idPdv
    WHERE NOT ISNULL(SIV.FK_idContrat)
    AND YEAR(SIV.dateDebut) = :year
    AND MONTH(SIV.dateDebut) = :month
    AND SIV.FK_idIntervenant = :inter
    AND EXISTS (   
        SELECT  1
        FROM    su_intervention   SUB
        WHERE   SIV.idIntervention  = SUB.idIntervention
        GROUP BY SUB.idIntervention
        HAVING  SIV.dateFin = MAX(SUB.dateFin)
    )
    ORDER BY SIV.dateFin DESC
    LIMIT 0, 1
    ';
    $lastInterventionExec = DbConnexion::getInstance()->prepare($sqlLastIntervention);
    $lastInterventionExec->bindValue(':year', $year, PDO::PARAM_INT);
    $lastInterventionExec->bindValue(':month', $month, PDO::PARAM_INT);
    $lastInterventionExec->bindValue(':inter', $exportListeIntervenant->FK_idIntervenant, PDO::PARAM_INT);
    $lastInterventionExec->execute();
    while($lastIntervention = $lastInterventionExec->fetch(PDO::FETCH_OBJ)) {
        $lig .= $lastIntervention->dateMax.';'.$lastIntervention->adressePdv_A.';'.
            $lastIntervention->codePostalPdv.';'.$lastIntervention->villePdv.';';
    }

    $csv .= $lig . "\r\n";
}
fwrite($ff, $csv);
fclose($ff);

print json_encode([
    'file' => $filename
]);