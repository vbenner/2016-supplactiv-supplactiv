<?php
/**
 * Export des etiquettes personnel
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

ini_set('display_errors','on');

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Class HTML2PDF */
#require_once __DIR__ . '/../current/includes/librairies/html2pdf_v4.03/html2pdf.class.php';
require __DIR__.'/../current/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

ob_start();
include(dirname(__FILE__).'/_html/etiquettePersonnel.php');
$content = ob_get_clean();


$html2pdf = new Html2Pdf(   'P', 'A4', 'fr', false, 'ISO-8859-15', array(0, 0, 0, 0));
$html2pdf->setDefaultFont('Arial');
$html2pdf->writeHTML($content, isset($_GET['vuehtml']));

$html2pdf->Output('EtiquettePersonnel.pdf');