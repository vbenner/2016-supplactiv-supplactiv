<?php
/**
 * Export du PLANNING
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
set_time_limit(0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

# ------------------------------------------------------------------------------
# On recupere les recherches
# ------------------------------------------------------------------------------
$AnneeSel 		= $_SESSION['dateRecherche_17_1'];
$MoisSel  		= $_SESSION['moisRecherche_17_1'];
$CampagneSel 	= $_SESSION['idCampagne_17_1'];
$IntervenantSel = $_SESSION['idIntervenant_17_1'] ;
$MissionSel =  $_SESSION['idMission_17_1'];
$AutoSel =  $_SESSION['autoRecherche_17_1'];

#echo '<pre>';
#print_r($_SESSION);
#echo '</pre>';
#die();

if($MoisSel == 'ALL'){
    # ------------------------------------------------------------------------------------
    # On defini le premier et dernier jour du mois
    # ------------------------------------------------------------------------------------
    $PremierJourMois = $AnneeSel.'-01-01 00:00:00';
    $DernierJourMois = $AnneeSel.'-12-31 23:59:59';

} else {
    # ------------------------------------------------------------------------------------
    # On defini le premier et dernier jour du mois
    # ------------------------------------------------------------------------------------
    $PremierJourMois = $AnneeSel.'-'.$MoisSel.'-01 00:00:00';

    $DernierJourMois  = @date(@date("t", @mktime( 0, 0, 0, $MoisSel, 1, $AnneeSel)), @mktime( 0, 0, 0, $MoisSel, 1, $AnneeSel));
    $DernierJourMois = $AnneeSel.'-'.$MoisSel.'-'.$DernierJourMois.' 23:59:59';
}

# ------------------------------------------------------------------------------
# Correspondance des colonnes en fonction du mois
# ------------------------------------------------------------------------------
$LstCorrespondanceColonneMois = array(
    '01'=>'I',
    '02'=>'J',
    '03'=>'K',
    '04'=>'L',
    '05'=>'M',
    '06'=>'N',
    '07'=>'O',
    '08'=>'P',
    '09'=>'Q',
    '10'=>'R',
    '11'=>'S',
    '12'=>'T'
);

# ------------------------------------------------------------------------------
# Curseur d'ecriture en fonction du moi
# ------------------------------------------------------------------------------
$LstCurseurMois = array(
    '01'=>4,
    '02'=>4,
    '03'=>4,
    '04'=>4,
    '05'=>4,
    '06'=>4,
    '07'=>4,
    '08'=>4,
    '09'=>4,
    '10'=>4,
    '11'=>4,
    '12'=>4
);

# ------------------------------------------------------------------------------
# Creation des onglets de mois en fonction de la recherche
# ------------------------------------------------------------------------------
$Lst_Mois = array(
    '01'=>'JANVIER',
    '02'=>'FEVRIER',
    '03'=>'MARS',
    '04'=>'AVRIL',
    '05'=>'MAI',
    '06'=>'JUIN',
    '07'=>'JUILLET',
    '08'=>'AOUT',
    '09'=>'SEPTEMBRE',
    '10'=>'OCTOBRE',
    '11'=>'NOVEMBRE',
    '12'=>'DECEMBRE'
);

# ------------------------------------------------------------------------------
# Nouveau filtre AUTO
# ------------------------------------------------------------------------------
if ($AutoSel == 'ALL') {

}
else {

}
# ------------------------------------------------------------------------------
# Je crée mon fichier Excel
# ------------------------------------------------------------------------------
$objPHPExcel = new Spreadsheet();

$PlaningGlobal = $objPHPExcel->getActiveSheet()->setTitle('PLANNING GLOBAL - '.$AnneeSel);

$ListFeuilleMois = array();
set_time_limit(0);
foreach($Lst_Mois as $ID_Mois=>$Nom_Mois):

    if($MoisSel == 'ALL' || $MoisSel == $ID_Mois):
        # ------------------------------------------------------------------------------
        # Creation de la feuille
        # ------------------------------------------------------------------------------
        $F = $objPHPExcel->createSheet();
        $F->setTitle("$ID_Mois - $Nom_Mois $AnneeSel");

        $F->setCellValue('A4', 'Nom Animatrice');
        $F->setCellValue('B4', 'Campagne');
        $F->setCellValue('C4', 'Mission');
        $F->setCellValue('D4', 'CIP');
        $F->setCellValue('E4', 'Libelle PDV');
        $F->setCellValue('F4', 'Adresse');
        $F->setCellValue('G4', 'Code Postal');
        $F->setCellValue('H4', 'Ville');
        $F->setCellValue('I4', 'Date Intervention');
        $F->setCellValue('J4', 'Contrat');
        $F->setCellValue('K4', 'KM Aller');

        $F->getStyle('A4:K4')->getFont()->setSize(10);
        $F->getStyle('A4:K4')->getFont()->setBold(true);
        $F->getStyle('A4:K4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
        $F->getStyle('A4:K4')->getFill()->setFillType(Fill::FILL_SOLID);
        $F->getStyle('A4:K4')->getFill()->getStartColor()->setARGB('FFD800');

        if($CampagneSel != 'ALL') {
            $F->getColumnDimension('A')->setAutoSize(true);
            $F->getColumnDimension('B')->setAutoSize(true);
            $F->getColumnDimension('C')->setAutoSize(true);
            $F->getColumnDimension('D')->setAutoSize(true);
            $F->getColumnDimension('E')->setAutoSize(true);
            $F->getColumnDimension('F')->setAutoSize(true);
            $F->getColumnDimension('G')->setAutoSize(true);
            $F->getColumnDimension('H')->setAutoSize(true);
            $F->getColumnDimension('I')->setAutoSize(true);
            $F->getColumnDimension('J')->setAutoSize(true);
            $F->getColumnDimension('K')->setAutoSize(true);

            $styleThickBrownBorderOutline = array(
                'borders' => array(
                    'outline' => array(
                        'style' => Border::BORDER_THIN,
                        'color' => array('argb' => '000000'),
                    ),
                ),
            );

            $F->getStyle('A4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('B4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('C4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('D4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('E4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('F4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('G4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('H4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('I4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('J4')->applyFromArray($styleThickBrownBorderOutline);
            $F->getStyle('K4')->applyFromArray($styleThickBrownBorderOutline);
        }

        $ListFeuilleMois[$ID_Mois] = $F;
    endif;
endforeach;


$PlaningGlobal->setCellValue('A4', 'Nom Animatrice');
$PlaningGlobal->setCellValue('B4', 'Campagne');
$PlaningGlobal->setCellValue('C4', 'Mission');
$PlaningGlobal->setCellValue('D4', 'CIP');
$PlaningGlobal->setCellValue('E4', 'Libelle PDV');
$PlaningGlobal->setCellValue('F4', 'Adresse');
$PlaningGlobal->setCellValue('G4', 'Code Postal');
$PlaningGlobal->setCellValue('H4', 'Ville');

$PlaningGlobal->setCellValue('I4', 'Jan');
$PlaningGlobal->setCellValue('J4', 'Fev');
$PlaningGlobal->setCellValue('K4', 'Mar');
$PlaningGlobal->setCellValue('L4', 'Avr');
$PlaningGlobal->setCellValue('M4', 'Mai');
$PlaningGlobal->setCellValue('N4', 'Jui');
$PlaningGlobal->setCellValue('O4', 'Jul');
$PlaningGlobal->setCellValue('P4', 'Aou');
$PlaningGlobal->setCellValue('Q4', 'Sep');
$PlaningGlobal->setCellValue('R4', 'Oct');
$PlaningGlobal->setCellValue('S4', 'Nov');
$PlaningGlobal->setCellValue('T4', 'Dec');

$PlaningGlobal->setCellValue('U4', 'TOTAL');

$PlaningGlobal->getStyle('A4:U4')->getFont()->setSize(10);
$PlaningGlobal->getStyle('A4:U4')->getFont()->setBold(true);
$PlaningGlobal->getStyle('A4:U4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
$PlaningGlobal->getStyle('A4:U4')->getFill()->setFillType(Fill::FILL_SOLID);
$PlaningGlobal->getStyle('A4:U4')->getFill()->getStartColor()->setARGB('FFD800');

if($CampagneSel != 'ALL') {
    $PlaningGlobal->getColumnDimension('A')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('B')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('C')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('D')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('E')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('F')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('G')->setAutoSize(true);
    $PlaningGlobal->getColumnDimension('H')->setAutoSize(true);
}

$PlaningGlobal->getColumnDimension('I')->setWidth(5);
$PlaningGlobal->getColumnDimension('J')->setWidth(5);
$PlaningGlobal->getColumnDimension('K')->setWidth(5);
$PlaningGlobal->getColumnDimension('L')->setWidth(5);
$PlaningGlobal->getColumnDimension('M')->setWidth(5);
$PlaningGlobal->getColumnDimension('N')->setWidth(5);
$PlaningGlobal->getColumnDimension('O')->setWidth(5);
$PlaningGlobal->getColumnDimension('P')->setWidth(5);
$PlaningGlobal->getColumnDimension('Q')->setWidth(5);
$PlaningGlobal->getColumnDimension('R')->setWidth(5);
$PlaningGlobal->getColumnDimension('S')->setWidth(5);
$PlaningGlobal->getColumnDimension('T')->setWidth(5);
$PlaningGlobal->getColumnDimension('U')->setWidth(10);

//Bords noir
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ),
    ),
);


$NumeroLigne = 4;


$sqlRechercheIntervention = "
SELECT idIntervenant, nomIntervenant, prenomIntervenant, idPdv, codeMerval, adressePdv_A, libellePdv, codePostalPdv, villePdv, idCampagne, libelleCampagne, idMission, libelleMission, totalKm
FROM su_intervenant
	INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	LEFT JOIN su_pdv_kilometrage ON su_pdv_kilometrage.FK_idPdv = su_intervention.FK_idPdv AND su_pdv_kilometrage.FK_idIntervenant = su_intervention.FK_idIntervenant
WHERE idIntervenant <> 1 AND su_intervention.dateDebut BETWEEN :dateDebut AND :dateFin
 ";
//$sqlRechercheIntervention .= 'LIMIT 0, 500 ';

if($CampagneSel != 'ALL')
    $sqlRechercheIntervention .= " AND idCampagne = $CampagneSel ";
if($MissionSel != 'ALL' && $MissionSel != '')
    $sqlRechercheIntervention .= " AND idMission = $MissionSel ";
if($IntervenantSel != 'ALL')
    $sqlRechercheIntervention .= " AND idIntervenant = $IntervenantSel ";
if($AutoSel != 'ALL')
    $sqlRechercheIntervention .= " AND boolAutoEntrepreneuse = '".($AutoSel == 1 ? 'OUI' : 'NON')."' ";

$sqlRechercheIntervention .= "
GROUP BY idIntervenant, idPdv, idCampagne, idMission
ORDER BY nomIntervenant, libelleCampagne, libelleMission";

$RechercheInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervention);


$sqlRechercheDteIntervention = "
SELECT DATE_FORMAT(su_intervention.dateDebut, '%Y-%m-%d') AS Dte, FK_idContrat
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE FK_idPdv = :IdPdv AND FK_idIntervenant = :IdIntervenant AND FK_idCampagne = :IdCampagne AND su_intervention.dateDebut BETWEEN '$PremierJourMois' AND '$DernierJourMois' AND idMission = :IdMission
ORDER BY su_intervention.dateDebut
";
$ExcRechercheDteIntervention = DbConnexion::getInstance()->prepare($sqlRechercheDteIntervention);


$RechercheInterventionExc->bindValue(':dateDebut', $PremierJourMois);
$RechercheInterventionExc->bindValue(':dateFin', $DernierJourMois);
$RechercheInterventionExc->execute();
while($InfoIntervention = $RechercheInterventionExc->fetch(PDO::FETCH_OBJ)) {

    $NumeroLigne++;
    $PlaningGlobal->setCellValue('A' . $NumeroLigne, $InfoIntervention->nomIntervenant . ' ' . $InfoIntervention->prenomIntervenant);
    $PlaningGlobal->setCellValue('B' . $NumeroLigne, $InfoIntervention->libelleCampagne);
    $PlaningGlobal->setCellValue('C' . $NumeroLigne, $InfoIntervention->libelleMission);
    $PlaningGlobal->setCellValue('D' . $NumeroLigne, $InfoIntervention->codeMerval);
    $PlaningGlobal->setCellValue('E' . $NumeroLigne, $InfoIntervention->libellePdv);
    $PlaningGlobal->setCellValue('F' . $NumeroLigne, ' '.$InfoIntervention->adressePdv_A.' ');
    $PlaningGlobal->setCellValue('G' . $NumeroLigne, $InfoIntervention->codePostalPdv.' ');
    $PlaningGlobal->setCellValue('H' . $NumeroLigne, $InfoIntervention->villePdv.' ');

    $NbInterMois = array();

    $ExcRechercheDteIntervention->bindValue(':IdPdv', $InfoIntervention->idPdv, PDO::PARAM_INT);
    $ExcRechercheDteIntervention->bindValue(':IdIntervenant',$InfoIntervention->idIntervenant, PDO::PARAM_INT);
    $ExcRechercheDteIntervention->bindValue(':IdCampagne', $InfoIntervention->idCampagne, PDO::PARAM_INT);
    $ExcRechercheDteIntervention->bindValue(':IdMission', $InfoIntervention->idMission, PDO::PARAM_INT);
    $ExcRechercheDteIntervention->execute();

    while($InfoDte = $ExcRechercheDteIntervention->fetch(PDO::FETCH_OBJ)) {

        set_time_limit(0);

        $D = preg_split('/-/', $InfoDte->Dte);
        @$NbInterMois[$D[1]]++;

        $LstCurseurMois[$D[1]]++;

        $ListFeuilleMois[$D[1]]->setCellValue('A' . $LstCurseurMois[$D[1]], $InfoIntervention->nomIntervenant . ' ' . $InfoIntervention->prenomIntervenant);
        $ListFeuilleMois[$D[1]]->setCellValue('B' . $LstCurseurMois[$D[1]], $InfoIntervention->libelleCampagne);
        $ListFeuilleMois[$D[1]]->setCellValue('C' . $LstCurseurMois[$D[1]], $InfoIntervention->libelleMission);
        $ListFeuilleMois[$D[1]]->setCellValue('D' . $LstCurseurMois[$D[1]], $InfoIntervention->codeMerval);
        $ListFeuilleMois[$D[1]]->setCellValue('E' . $LstCurseurMois[$D[1]], $InfoIntervention->libellePdv);
        $ListFeuilleMois[$D[1]]->setCellValue('F' . $LstCurseurMois[$D[1]], ' '.$InfoIntervention->adressePdv_A);
        $ListFeuilleMois[$D[1]]->setCellValue('G' . $LstCurseurMois[$D[1]], $InfoIntervention->codePostalPdv);
        $ListFeuilleMois[$D[1]]->setCellValue('H' . $LstCurseurMois[$D[1]], $InfoIntervention->villePdv);
        $ListFeuilleMois[$D[1]]->setCellValue('I' . $LstCurseurMois[$D[1]], $InfoDte->Dte);
        $ListFeuilleMois[$D[1]]->setCellValue('J' . $LstCurseurMois[$D[1]], $InfoDte->FK_idContrat);
        $ListFeuilleMois[$D[1]]->setCellValue('K' . $LstCurseurMois[$D[1]], $InfoIntervention->totalKm);

        if($CampagneSel != 'ALL') {
            $ListFeuilleMois[$D[1]]->getStyle('A' . $LstCurseurMois[$D[1]] . ':O' . $LstCurseurMois[$D[1]])->getFont()->setSize(10);
        }

    }

    # ------------------------------------------------------------------------------
    # Style de la ligne
    # ------------------------------------------------------------------------------
    if($CampagneSel != 'ALL') {
        $PlaningGlobal->getStyle('A' . $NumeroLigne . ':U' . $NumeroLigne)->getFont()->setSize(10);
        $PlaningGlobal->getStyle('I' . $NumeroLigne . ':U' . $NumeroLigne)->getFill()->setFillType(Fill::FILL_SOLID);
        $PlaningGlobal->getStyle('I' . $NumeroLigne . ':U' . $NumeroLigne)->getFill()->getStartColor()->setARGB('DEDEDE');

    }

    $TotalInter = 0;

    foreach ($NbInterMois as $IDM => $NbInter) {
        $PlaningGlobal->setCellValue($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne, $NbInter);

        if($CampagneSel != 'ALL') {
            $PlaningGlobal->getStyle($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne)->getFill()->getStartColor()->setARGB('006D00');
            $PlaningGlobal->getStyle($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
        }
        $TotalInter += $NbInter;
    }
    $PlaningGlobal->setCellValue('U' . $NumeroLigne, $TotalInter);
    $PlaningGlobal->getStyle('U' . $NumeroLigne)->getFont()->setBold(true);

}
if($CampagneSel != 'ALL') {
    $objPHPExcel->getActiveSheet()->getStyle('A4:U'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Planning-Intervenant-'.date('Ymd-His').'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;
