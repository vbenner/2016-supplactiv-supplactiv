<?php

/** -----------------------------------------------------------------------------------------------
 * CRON Quotidien
 *
 * Import des fichiers de NDF vers le serveur EXPENSYA pour la fonctionnalité des notes
 * de frais
 *
 * - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - =
 * @version : 1.0.0
 * @dev : Vincent BENNER © PAGE UP 2017
 * @date : 2017-08-09
 * @history :
 *
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once __DIR__ . '/../current/_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Paramètres d'accès
 */
$serveur = 'sftp.expensya.com';
$user='supplactivprod';
$pass='@Uy!s52par?d66d';

/** -----------------------------------------------------------------------------------------------
 * Nouveaux paramètres
 */
$serveur = 'sftp.expensya.com';
$user='supplactivprod';
$pass='@Uy!s52par?d66d';


/** -----------------------------------------------------------------------------------------------
 * Vérification de base
 */
if (!function_exists("ssh2_connect")) die("La bibliotheque ssh2 n'est pas disponible sur le serveur");

/** -----------------------------------------------------------------------------------------------
 * Fonction DEBUG
 *
 * @param $msg
 * @param string $var
 */
function debug($msg, $var = 'debug')
{
    if (isset($_GET[$var]))
    {
        echo  trim($msg), "\n";
        ob_flush();
        flush();
    }
}


function importNDF($fic, $year, $month) {

    $sqlUpdateLineFrais = '
    INSERT INTO su_intervenant_frais (
      FK_idIntervenant, dateFrais, montantFraisJustificatif, montantFraisJustificatifTVA,
      montantIndemniteTelephone, montantAutreFrais)
    VALUES(:id, :datef, :montantf, :montanttva, :montanttel, :montantautre)
    ON DUPLICATE KEY UPDATE    
      montantFraisJustificatif=:montantf, 
      montantFraisJustificatifTVA=:montanttva,
      montantIndemniteTelephone=:montanttel,
      montantAutreFrais=:montantautre
    ';

    $sqlUpdateLineKM = '
    INSERT INTO su_intervenant_km (
      FK_idIntervenant, dateKilometre, tarifKilometre, totalKilometre)
    VALUES(:id, :datef, :tx, :km)
    ON DUPLICATE KEY UPDATE    
      tarifKilometre=:tx, 
      totalKilometre=:km
    ';
    $updateLineFraisExec = DbConnexion::getInstance()->prepare($sqlUpdateLineFrais);
    $updateLineKMExec = DbConnexion::getInstance()->prepare($sqlUpdateLineKM);
    $content = file_get_contents($fic);
    $lines = explode("\r\n", $content);
    $i=1;
    foreach ($lines as $line) {
        if ($line != '' || $i != 1) {

            /** -----------------------------------------------------------------------------------
             * Partie la plus SIMPLE : les frais
             */
            $data = explode(';', $line);
            $id = $data[0];
            $datef = $year.'-'.$month.'-01';
            $montantf = str_replace(',', '.', $data[2]);
            $montanttva = str_replace(',', '.', $data[14]);
            $montanttel = str_replace(',', '.', $data[11]);
            $montantautre = str_replace(',', '.', $data[12]);

            #if ($id == 1382 || $id == 1351 || $id == 1846 || $id == 1637 || $id == 1611) {
                $updateLineFraisExec->bindValue(':id', $id, PDO::PARAM_INT);
                $updateLineFraisExec->bindValue(':datef', $datef, PDO::PARAM_STR);
                $updateLineFraisExec->bindValue(':montantf', $montantf, PDO::PARAM_STR);
                $updateLineFraisExec->bindValue(':montanttva', $montanttva, PDO::PARAM_STR);
                $updateLineFraisExec->bindValue(':montanttel', $montanttel, PDO::PARAM_STR);
                $updateLineFraisExec->bindValue(':montantautre', $montantautre, PDO::PARAM_STR);
                $updateLineFraisExec->execute();
            #}

            /** -----------------------------------------------------------------------------------
             * Partie la plus COMPLIQUEE : les TAUX
             */
            $km1 = str_replace(',', '.', $data[3]);
            $tx1 = str_replace(',', '.', $data[4]);
            $km2 = str_replace(',', '.', $data[5]);
            $tx2 = str_replace(',', '.', $data[6]);
            $km3 = str_replace(',', '.', $data[7]);
            $tx3 = str_replace(',', '.', $data[8]);
            $km4 = str_replace(',', '.', $data[9]);
            $tx4 = str_replace(',', '.', $data[10]);

            #if ($id == 1382 || $id == 1351 || $id == 1846 || $id == 1637 || $id == 1611) {
                if ($km1 != 0 && $tx1 != 0) {
                    $updateLineKMExec->bindValue(':id', $id, PDO::PARAM_INT);
                    $updateLineKMExec->bindValue(':datef', $datef, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':tx', $tx1, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':km', $km1, PDO::PARAM_INT);
                    $updateLineKMExec->execute();
                }
                if ($km2 != 0 && $tx2 != 0) {
                    $updateLineKMExec->bindValue(':id', $id, PDO::PARAM_INT);
                    $updateLineKMExec->bindValue(':datef', $datef, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':tx', $tx2, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':km', $km2, PDO::PARAM_INT);
                    $updateLineKMExec->execute();
                }
                if ($km3 != 0 && $tx3 != 0) {
                    $updateLineKMExec->bindValue(':id', $id, PDO::PARAM_INT);
                    $updateLineKMExec->bindValue(':datef', $datef, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':tx', $tx3, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':km', $km3, PDO::PARAM_INT);
                    $updateLineKMExec->execute();
                }
                if ($km4 != 0 && $tx4 != 0) {
                    $updateLineKMExec->bindValue(':id', $id, PDO::PARAM_INT);
                    $updateLineKMExec->bindValue(':datef', $datef, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':tx', $tx4, PDO::PARAM_STR);
                    $updateLineKMExec->bindValue(':km', $km4, PDO::PARAM_INT);
                    $updateLineKMExec->execute();
                }
            #}
        }
        $i++;
    }

}

/** -----------------------------------------------------------------------------------------------
 * Class sftp
 */
class sftp
{
    private $id = false, $server = false, $url = false;

    /** @brief Constructeur
     *
     * Tente d'etablir une connexion SFTP à un serveur distant.
     * @param	$server : ip/host du server
     * @param 	$user : Identifiant de la connexion
     * @param	$pass: Mot de passe
     * @throw : Une exception remontera en cas d'erreur
     */
    public function __construct ($server, $user, $pass)
    {
        $this->id = ssh2_connect($server, 22);
        if(!$this->id || empty($this->id))
            throw new Exception ('Echec de connexion - ' . $server . '<br/>');
        if (!ssh2_auth_password($this->id, $user, $pass))
            throw new Exception ("Echec d'identification - " . $server . '<br/>');
        $this->url = ssh2_sftp($this->id);
        if (!$this->url)
            throw new Exception ("Echec d'ouverture du SFTP sur le serveur " . $server . '<br/>');
        $this->server = $server;
    }

    /** @brief Destructeur
     *
     * Ferme une connection sftp
     */
    public function __destruct()
    {
        debug("Fermeture de la connexion SSH" . '<br/>');
        ssh2_exec($this->id, "exit");
    }

    /** @brief Retourne la taille d'un fichier sur le serveur distant
     * @param	$file
     *	chemin absolu du fichier
     * @return
     *	taille du fichier en octet
     */
    private function getfilesize($file)
    {
        return filesize("ssh2.sftp://" . $this->url . $file);
    }

    /** @brief Recupere un fichier sur le serveur distant
     *
     * Tente de recuperer un fichier sur le serveur distant,
     * user friendly , $name  n'a pas besoin d'ÃƒÂªtre le chemin absolu vers le fichier
     * par exemple get("foo/zomg/lol.csv") va recuperer le fichier /../foo/zomg/lol.csv sur le serveur
     * dans le fichier lol.csv du dossier courant local
     * @param	$name
     *	nom du fichier ÃƒÂ  rÃƒÂ©cuperer sur le serveur
     * @param 	$local_name
     *	facultatif, nom du fichier en local aprÃƒÂ¨s transfert
     * @return
     *	Renvois le chemin du fichier recupÃƒÂ©rÃƒÂ© en local
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function get($name, $local_name = false)
    {
        try {$name = $this->fpath($name);}
        catch (Exception $e) {throw $e;}
        if (!$local_name)
            $local_name = '.' . (strpos(name, '/') === false?strrchr($name, '/'):('/' . $name));
        debug("Tentative de transfert du fichier '" .$local_name ."' depuis le serveur '" .$name . "'");
        $handle = @fopen("ssh2.sftp://" . $this->url . $name, 'r');
        if (!$handle)
            throw new Exception ("Impossible d'ouvrir le fichier " . $name . " sur le serveur distant " . $this->server);
        $size = $this->getfilesize($name);
        $contents = '';
        $transfered = 0;$i = 0;
        while ($transfered < $size && ($buffer = fread($handle, $size - $transfered)))
        {
            debug($transfered, " transfere sur ", $size);
            $transfered += strlen($buffer);
            $contents .= $buffer;
            if ($i++ > 1000)
            {
                fclose($handle);
                throw new Exception ('Erreur pendant le transfert du fichier ' . $name . 'depuis le serveur' . $this->server . ' : timeout');
            }
        }
        file_put_contents ($local_name, $contents);
        fclose($handle);
        debug("\nTransfert du fichier '" . $name . "' depuis le serveur " . $this->server . " reussi");
        return ($local_name);
    }

    /** @brief DÃƒÂ©pose un fichier sur le serveur distant
     *
     * Tente de dÃƒÂ©poser un fichier sur le serveur distant,
     * user friendly , $name n'a pas besoin d'ÃƒÂªtre le chemin absolu vers le fichier
     * par exemple put("foo/zomg/lol.csv") va deposer le fichier lol.csv du dossier courant dans le dossier /../foo/zomg/lol.csv du serveur
     * @param	$name
     *	nom du fichier ÃƒÂ  dÃƒÂ©poser sur le serveur
     * @param 	$local_name
     *	facultatif, nom du fichier
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function put($name, $remote_name = false)
    {
        try
        {
            if (!$remote_name)
            {
                $remote_name = $this->fpath($name);
                $name = '.' . (strpos($name, '/') === false ?('/' . $name):strrchr($name, '/'));
            }
            else
                $remote_name = $this->path(substr($remote_name, 0 , strrpos($remote_name, '/'))) . strrchr($remote_name, '/');
        }
        catch (Exception $e) {throw $e;}
        debug("Tentative de transfert du fichier '" . $name . "' sur le serveur " . $this->server);
        $handle = fopen("ssh2.sftp://" . $this->url . $remote_name, 'w');
        if (!$handle)
            throw new Exception('Impossible d\'ouvrir le fichier ' . $remote_name . ' en ecriture sur le serveur ' . $this->server);
        $data = file_get_contents(strpos($name, "/")?'.' . strrchr($name, "/"):$name);
        if ($data === false)
            throw new Exception("Impossible d'ouvrir le fichier local " . $name);
        $transfered = fwrite($handle, $data);
        if ($transfered === false)
            throw new Exception("Impossible de transferer le fichier " . $name);
        $size = strlen($data);
        if ($transfered != $size)
        {
            while ($transfered < $size && ($transfered += fwrite($handle, mb_substr($data, $transfered, $size - $transfered))))
                debug($transfered . " transfere sur " . $size);
        }
        debug("Fichier " . $remote_name . " correctement depose");
        fclose($handle);
    }


    /** @brief Deplace un fichier sur le serveur distant
     *
     * Tente de deplacer un fichier sur le serveur distant
     * user friendly , les chemins n'ont pas besoin d'ÃƒÂªtre absolu
     * par exemple mv("foo/zomg/lol.csv", "omfg/lolz.txt") va deplacer /../foo/zomg/lol.csv vers /../omfg/lolz.txt
     * @param	$old
     *	nom du fichier ÃƒÂ  dÃƒÂ©placer
     * @param 	$new
     *	nouveau nom & chemin
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function mv($old, $new)
    {
        try
        {
            $old = $this->fpath($old);
            $new = $this->fpath($new);
        }
        catch (Exception $e)
        {
            throw $e;
        }
        debug('Tentative de deplacement du fichier \'' . $old . '\' vers \'' . $new . "'");
        $old_handle = fopen("ssh2.sftp://" . $this->url .$old, 'r');
        if (!$old_handle)
            throw new Exception ("Impossible d'ouvrir en lecture le fichier  '" . $old . "' sur le serveur distant");
        $new_handle = fopen("ssh2.sftp://" . $this->url .$new, 'w');
        if (!$new_handle)
            throw new Exception ("Impossible d'ouvrir en ecriture le fichier  '" . $new . "' sur le serveur distant");
        $size = $this->getfilesize($old);
        $read = 0;
        $wrote = 0;
        while ($read < $size && ($buffer = fread($old_handle, $size - $read)))
        {
            $tmp_size = strlen($buffer);
            $read += $tmp_size;
            $tmp_wrote = 0;
            while ($tmp_wrote <$tmp_size)
            {
                $tmp = fwrite($new_handle, substr($buffer, $tmp_wrote));
                if (!$tmp)
                    break;
                $tmp_wrote += $tmp;
            }
            $wrote += $tmp_size;
        }
        debug("taille du fichier = " . $size . " , " . $read . " octets lu, " . $wrote . " octets ecrit");
        fclose($new_handle);
        fclose($old_handle);
        if ($size == $read && $read == $wrote)
            unlink("ssh2.sftp://" . $this->url . $old);
        else
            throw new Exception ("Erreur pendant le deplacement du fichier " . $old . " en " . $old . "\ntaille du fichier = " . $size . " , " . $read . " octets lu, " . $wrote . " octets ecrit\n");
    }

    /** @brief Récupère le chemin absolu d'un dossier
     *
     * @param	$dir
     *	nom du dossier
     * @return
     *	chemin absolu du dossier $dir sur le serveur distant
     */
    private function path($dir)
    {
        $rlpath = @ssh2_sftp_realpath($this->url, $dir);
        if (!$rlpath)
            throw new Exception ("Dossier introuvable sur le serveur distant : "  . $dir);
        return $rlpath;
    }

    /** @brief RÃƒÂ©cupere le chemin absolu d'un fichier sur le serveur
     *
     * @param	$name
     *	nom du fichier
     * @return
     *	chemin absolu du fichier sur le serveur
     */
    private function fpath($name)
    {
        try
        {
            if (strpos($name, '/') === false)
                $name = $this->path('.') . '/' . $name;
            else
                $name = $this->path(substr($name, 0 , strrpos($name, '/'))) . strrchr($name, '/');
        }
        catch (Exception $e)
        {
            throw $e;
        }
        return $name;
    }

    /** @brief Liste le contenu d'un dossier
     *
     * Liste le contenu d'un dossier passÃƒÂ© en parametre ou le dossier courant si aucun dossiÃƒÂ© donnÃƒÂ©.
     * Ne renvois pas les dossiers
     * Offre la possibilitÃƒÂ©e de filtrer les resultats possedant l'extension "$ext" et la chaÃƒÂ®ne de caractere $like dans leur noms
     * @param	$directory
     *	nom du dossier
     * @param	$like
     *	pattern ÃƒÂ  respeceter
     * @param	$ext
     *	extension des fichiers ÃƒÂ  respecter
     * @return
     *	renvois la liste des fichiers sous la forme d'un tableau non associatif
     * @throw
     *	une exception remontera en cas d'erreur
     */
    public function flist($directory = './' , $like = false, $ext = false)
    {
        $dir = $this->path($directory);
        $handle = opendir("ssh2.sftp://" . $this->url . $dir);
        if (!$handle)
            throw new Exception ("Impossible d'ouvrir le dossier " .  $directory . " sur le serveur " . $this->server);
        $files = array();
        while (false !== ($file = readdir($handle)))
        {
            if (substr($file, 0, 1) != "." && !is_dir($file))
            {
                if ($like && $ext)
                {
                    if (stripos($file, $like) !== false && strrchr($file , ".") == (strpos($ext, ".") !== false?$ext:(".".$ext)))
                        $files[] = $dir . '/' . $file;
                }
                else
                    $files[] =  $dir . '/' . $file;
            }
        }
        sort($files);
        return $files;
    }

    /** @brief Recupere une liste de fichier
     *
     * Recupere une liste de fichier et les deplace (sur le serveur distant) ds le dossier $dir_if_success si spÃƒÂ©cifiÃƒÂ©
     * @param	$files_ar
     *	tableau de fichiers
     * @param	$like
     *	pattern ÃƒÂ  respeceter
     * @param	$ext
     *	extension des fichiers ÃƒÂ  respecter
     * @return
     *	renvois la liste des fichiers sous la forme d'un tableau non associatif
     * @throw
     *	une exception remontera en cas d'erreur
     */
    public function get_files($files_ar, $dir_if_success = false)
    {
        $error = count($files_ar);
        $files = array();
        foreach ($files_ar as $k =>$v)
        {
            debug("Transfert de $v.");
            try
            {
                $files[$k] = $this->get($v);
                if ($dir_if_success)
                    $this->mv($v, $dir_if_success . strrchr($files[$k], "/"));
                debug("Transfert reussi");
            }
            catch (Exception $e)
            {
                debug("Echec dans le transfer du fichier $v :\n" . $e->getMessage());;
                $error--;
            }
        }
        if (!$error)
            throw new Exception (
                "Impossible de récupérer les fichiers spécifiés, vérifiez les droits, 
                le type de transfert ou type de connexion.");
        if (empty($files))
            return false;
        sort($files);
        return $files;
    }
}

/** -----------------------------------------------------------------------------------------------
 * TRANSFERT
 */
try
{

    /** -------------------------------------------------------------------------------------------
     * Liste les fichiers
     */
    $sftp = new sftp($serveur, $user, $pass);
    $list = $sftp->flist("export", 'Supplactiv', 'csv');
    foreach ($list as $unFic) {

        /** ---------------------------------------------------------------------------------------
         * On rélécharge le fichier en LOCAL si il n'a pas été traité
         */
        if (strpos($unFic, 'traite') === false && strpos($unFic, '_detail') === false) {

            $locFile = $sftp->get($unFic);

            /** -----------------------------------------------------------------------------------
             * Avec le nom on a le mois et l'année
             * Supplactiv_09-2017_12102017T0941
             */
            $tmp = explode('_', $unFic);
            $dateFull = $tmp[1];
            $month = substr($dateFull, 4, 2);
            $year = substr($dateFull, 0, 4);

            #die($month . ' - ' . $year);

            /** -----------------------------------------------------------------------------------
             * On traite le fichier local
             */
            importNDF($locFile, $year, $month);

            $newLocFile = str_replace('.csv', '_traite.csv', $locFile);
            @rename($locFile, $newLocFile);

        }
        else {


        }

    }


    print json_encode(
        array('ERR' => 0)
    );

    die();

    $sftp->put($filenameU, "import/".$filenameU);
    $sftp->put($filenameP, "import/".$filenameP);
    $sftp->put($filenameUP, "import/".$filenameUP);
    unlink($filenameU);
    unlink($filenameP);
    unlink($filenameUP);
    $sftp = null;
}
catch (Exception $e)
{
    die ($e->getMessage()."\n");
}
die('Fin du traitement');
