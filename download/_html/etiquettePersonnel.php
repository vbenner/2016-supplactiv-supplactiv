<?php
/**
 * Export des etiquettes personnels
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

function infoIntervenante($IdIntervenant , $P){
    $Padding = 12+$P;
    $sqlRechercheInfoIntervenant = "
	SELECT nomIntervenant, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C, codePostalIntervenant, villeIntervenant
	FROM su_intervenant
	WHERE idIntervenant = :idIntervenant";
    $RechercheInfoIntervenant = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);

    $RechercheInfoIntervenant->bindValue(':idIntervenant', $IdIntervenant);
    $RechercheInfoIntervenant->execute();
    while($InfoInter = $RechercheInfoIntervenant->fetch(PDO::FETCH_OBJ)):
        echo '
    	<div style="padding-top:'.$Padding.'px;"><a style="text-decoration:none;color:Black;font-size:12px;font-weight:bold;">'.$InfoInter->nomIntervenant.' '.$InfoInter->prenomIntervenant.'</a><br/>
	    	<a style="text-decoration:none;color:Black;font-size:10px;">'.$InfoInter->adresseIntervenant_A.'</a><br/>
	    	<a style="text-decoration:none;color:Black;font-size:10px;">'.$InfoInter->adresseIntervenant_B.'</a><br/>
	    	<a style="text-decoration:none;color:Black;font-size:10px;">'.$InfoInter->adresseIntervenant_C.'</a><br/>
	    	<a style="text-decoration:none;color:Black;font-size:10px;">'.$InfoInter->codePostalIntervenant.' '.$InfoInter->villeIntervenant.'</a>
	    </div>';
    endwhile;
}


$Num = 1;
$ListeIntervenante = array();

foreach($_SESSION['listeIntervenant_17_5'] as $i => $idIntervenant) {
    if (!empty($idIntervenant)) {
        @$ListeIntervenante[$Num] = $idIntervenant;
        $Num++;
    }
}

$NbPage = ((sizeof($_SESSION['listeIntervenant_17_5'])-(sizeof($_SESSION['listeIntervenant_17_5'])%21))/21);
if(sizeof($_SESSION['listeIntervenant_17_5'])%21 > 1) $NbPage++;
$NbPage = ($NbPage == 0) ? 1 : $NbPage;

$Num = 1;
for($i = 1; $i <= $NbPage; $i++){
    ?>
    <style type="text/css">
        .div_Etq{
            width:258px;margin:0px;padding-left:5px;height:148px;padding-top:10px;border:1px solid White;
        }
    </style>
    <page style="font-size: 12px;">
        <table style="width:100%;margin:0px;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],0) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],0) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],0) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],1) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],1) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],1) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],2) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],2) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],2) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],3) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],3) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],3) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],4) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],4) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],4) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],5) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],5) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],5) : '';$Num++; ?></div></td>
            </tr>
            <tr>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],6) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],6) : '';$Num++; ?></div></td>
                <td style="padding:0px;"><div class="div_Etq"><?php echo (isset($ListeIntervenante[$Num]))? infoIntervenante($ListeIntervenante[$Num],6) : '';$Num++; ?></div></td>
            </tr>
        </table>
    </page>
<?php
}