<?php
# ------------------------------------------------------------------------------
# Retourne un Format 
# ------------------------------------------------------------------------------
function afficheHeureRemun($Minutes){
	if($Minutes%60 == 0){
		$HeureTotal = $Minutes/60;
		$HeureTotal = (strlen($HeureTotal) == 1) ? '0'.$HeureTotal : $HeureTotal;
		return $HeureTotal.'H';
	}else {
		$MinTotal = $Minutes%60;
		$HeureTotal = ($Minutes-$MinTotal)/60;
		$MinTotal = (strlen($MinTotal) == 1) ? '0'.$MinTotal : $MinTotal;
		$HeureTotal = (strlen($HeureTotal) == 1) ? '0'.$HeureTotal : $HeureTotal;
		return $HeureTotal.'H'.$MinTotal;
	}
}

$sqlRecherheFraisTel = '
SELECT idIntervention, fraisTelephone, DATE_FORMAT(dateDebut, "%Y-%m-%d") AS Dte
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_IdPdv
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE FK_idContrat = :IdContrat
ORDER BY dateDebut';
$RecherheFraisTelExc = DbConnexion::getInstance()->prepare($sqlRecherheFraisTel);

# ------------------------------------------------------------------------------
# Recherche des informations concernant le contrat
# ------------------------------------------------------------------------------
$sqlRechercheInfoIntervenant = '
SELECT idIntervenant, DATE_FORMAT(dateCreationContrat, "%d/%m/%Y") AS dteContrat,  nomIntervenant, prenomIntervenant, DATE_FORMAT(dateNaissance, "%d/%m/%Y") AS dateNaissance,
	villeNaissance, numeroSS, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C, codePostalIntervenant, villeIntervenant, nationalite, descriptionMission, 
	FK_idTypeMission, numSIRET, forfaitJournalierNet, DATE_FORMAT(dateContratCadre, "%d/%m/%Y") AS dteContratCadre,
	dateCreationContrat, dateCreationContrat
FROM su_intervenant
    INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE FK_idContrat = :IdContrat
GROUP BY idIntervenant';
$ResultContrat = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);

$sqlRechercheMontantRepas = "
SELECT fraisRepas, libelleGamme, descriptionMission
FROM su_intervention_frais
	INNER JOIN su_intervention ON su_intervention.idIntervention = su_intervention_frais.FK_idIntervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE FK_idContrat = :IdContrat";
$ResultMontantRepas = DbConnexion::getInstance()->prepare($sqlRechercheMontantRepas);

$sqlRechercheIntervention = '
SELECT DATE_FORMAT(dateDebut, "%d/%m/%Y") AS Date, DATE_FORMAT(dateDebut, "%HH%i") AS HeureD, DATE_FORMAT(dateDebut, "%H") AS HeureDC, 
	   DATE_FORMAT(dateDebut, "%i") AS MinDC, DATE_FORMAT(dateFin, "%H") AS HeureFC, DATE_FORMAT(dateFin, "%i") AS MinFC, DATE_FORMAT(dateFin, "%HH%i") AS HeureF,
	   libellePdv, adressePdv_A, codePostalPdv, villePdv
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE FK_idContrat = :IdContrat
ORDER BY dateDebut';
$ResultIntervention = DbConnexion::getInstance()->prepare($sqlRechercheIntervention);

$sqlRechercheContrat = "
SELECT *
FROM su_contrat
WHERE idContrat = :IdContrat";
$ResultInfoContrat = DbConnexion::getInstance()->prepare($sqlRechercheContrat);

$IdTypeMission = ''; $DescriptionMission = ''; $dteContrat = '';$dteContratCadre = '';
$ResultContrat->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
$ResultContrat->execute();
$newAdresse = false;
while($Contrat = $ResultContrat->fetch(PDO::FETCH_OBJ)):

    if ($Contrat->dateCreationContrat >= '2017-06-19') {
        $newAdresse = true;
        #die("Maintenance -> ".$Contrat->dateCreationContrat );
    }
	$NomIntervenant     = $Contrat->nomIntervenant;
	$PrenomIntervenant  = $Contrat->prenomIntervenant;
	$DateNaissance  	= $Contrat->dateNaissance;
	$VilleNaissance 	= $Contrat->villeNaissance;
	$NumeroSS 			= $Contrat->numeroSS;
	$NumeroSIRET        = $Contrat->numSIRET;
	$AdresseIntervenant = $Contrat->adresseIntervenant_A;
	$AdresseIntervenant.= ($Contrat->adresseIntervenant_B != '') ? ', '.$Contrat->adresseIntervenant_B : '';
	if($Contrat->adresseIntervenant_C != ''):
		$AdresseIntervenant.= ', '.$Contrat->adresseIntervenant_C;
	endif;
	$AdresseIntervenant.= ', '.$Contrat->codePostalIntervenant.' '.$Contrat->villeIntervenant;
	$Nationalite = ($Contrat->nationalite == 'F')? 'Francaise' : 'Etrang&egrave;re';
	
	$IdTypeMission = $Contrat->FK_idTypeMission;
	$DescriptionMission = $Contrat->descriptionMission;
    $dteContrat = $Contrat->dteContrat;
    $dteContratCadre = $Contrat->dteContratCadre;
endwhile;


$ResultInfoContrat->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
$ResultInfoContrat->execute();
while($InfoContrat = $ResultInfoContrat->fetch(PDO::FETCH_OBJ)):
	$SalaireBrut = $InfoContrat->salaireBrut;
	$ForfaitNet = $InfoContrat->forfaitJournalierNet;
    $fraisInclus = $InfoContrat->boolFraisInclus;
	$AutorisationFraisTel = $InfoContrat->autorisationFraisTel;
	$VehiculePersonnel = $InfoContrat->boolVehiculePersonnel;
	$VehiculeFonction = $InfoContrat->boolVehiculeFonction;
	$TarifKM = $InfoContrat->tarifKM;
	$SocieteA = $InfoContrat->societeA;
	$DetailContrat = $InfoContrat->detailContrat;
endwhile;

$MontantFraisTel = 0;
if($AutorisationFraisTel == 'OUI')
{
	$LstFrais = array();
	$RecherheFraisTelExc->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
	$RecherheFraisTelExc->execute();
	while($InfoFraisTel = $RecherheFraisTelExc->fetch(PDO::FETCH_OBJ))
	{
		@$LstFrais[$InfoFraisTel->Dte] = $InfoFraisTel->fraisTelephone;
	}

	$NbTotalJour = 0;
	foreach($LstFrais as $Dte=> $Val)
	{
		$NbTotalJour++;
		$MontantFraisTel += $Val;
	}

	$MontantFraisTel = round($MontantFraisTel/$NbTotalJour, 2);
}

$MontantRepas = 0; $DetailMission = '';
$ResultMontantRepas->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
$ResultMontantRepas->execute();
while($InfoMontant = $ResultMontantRepas->fetch(PDO::FETCH_OBJ)) {
    $MontantRepas = ($InfoMontant->fraisRepas != 0.00) ? $InfoMontant->fraisRepas : $MontantRepas;
    $GammeProduit = $InfoMontant->libelleGamme;
    $DetailMission = $InfoMontant->descriptionMission;
}

?>
<style type="text/css">
	h4{
		text-align:center;
		text-decoration:underline;
		margin:5px 0px;
		font-size:13px;
	}
	
	p{
		text-align: justify;
		margin: 5px 0px;
	}
</style>
<page style="font-size:11px">
	<div style="padding-bottom:10px;text-align:left"><img src="_html/logo_Entete.png" /></div>

	<p style="text-align: center">ANNEXE 1 -  CONTRAT D’APPLICATION</p>
	<div style="border:1px solid Black">
        <h3 style="text-align:center;margin:0px;padding:4px 0px;">
            CONTRAT D'APPLICATION
        </h3>
    </div>

    <?php
    ?>
	<p style="text-align:justify;margin-top:10px;">
        <i>Entre les soussign&eacute;s :</i><br/><br/>
        <?php
        if ($newAdresse) {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        } else {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 24, Rue de la Redoute 21850 St Apollinaire, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        }
        ?>
        <br/><br/>
		Ci-apr&egrave;s le &laquo;CLIENT&raquo;<br/>
        <i>D'une part</i><br/>
        <i>Et,</i>
    </p>
    
    <p style="margin-top:10px;">	
        <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant ?></font>, n&eacute;(e) le 
        <font style="color:#3B5998"><?php echo $DateNaissance ?></font> &agrave; 
        <font style="color:#3B5998"><?php echo $VilleNaissance ?></font>, de nationalit&eacute; 
        <font style="color:#3B5998"><?php echo $Nationalite ?></font>,<br/> demeurant au : 
        <font style="color:#3B5998"><?php echo $AdresseIntervenant ?></font>.<br/>
        Num&eacute;ro de SIRET
        <font style="color:#3B5998"><?php echo $NumeroSIRET ?></font>,<br/>
		<br/>Ci-apr&egrave;s le &laquo;PRESTATAIRE&raquo;<br/>
        <i>D'autre part</i><br/><br/>
        En ex&eacute;cution du contrat cadre conclu le <b><font style="color:#3B5998"><?php echo $dteContratCadre?></font></b> entre les parties, il a été convenu ce qui suit.
    </p>
    
    <!-- ARTICLE 1 -->
    <p>
		Le PRESTATAIRE s’engage &agrave; r&eacute;aliser les Prestations de la marque &laquo;<b><font style="color:#3B5998"> <?php echo $SocieteA; ?></font></b> &raquo; dans les
		points de vente suivants, aux dates suivantes, conform&eacute;ment au planning ci-dessous :<br/>
	</p>

	<ul>
    <!-- ON LISTE LES INTERVENTIONS -->
    <?php
    //Initialisation du nombre d'heure 
    $LstHeureJour = array();
    $ResultIntervention->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
	$ResultIntervention->execute();
	while($InfoInter = $ResultIntervention->fetch(PDO::FETCH_OBJ)):

        $nbTotalH = ((($InfoInter->HeureFC*60)+$InfoInter->MinFC)-(($InfoInter->HeureDC*60)+$InfoInter->MinDC));
		echo "
		<p style='color:#3B5998;margin:0px;padding:0px;'>
			<li>&nbsp;Le $InfoInter->Date ($InfoInter->libellePdv, $InfoInter->adressePdv_A, $InfoInter->codePostalPdv $InfoInter->villePdv) ".(($nbTotalH >= 480) ? "" : "")."
		</li></p>";
		/*
        echo "
		<p style='color:#3B5998;margin:0px;padding:0px;'>
			<li>&nbsp;Le $InfoInter->Date de $InfoInter->HeureD &agrave; $InfoInter->HeureF ($InfoInter->libellePdv, $InfoInter->adressePdv_A, $InfoInter->codePostalPdv $InfoInter->villePdv) ".(($nbTotalH >= 480) ? "" : "")."
		</li></p>";
		*/
	endwhile;
	
	//On calcul le nombre d'heure total remunere
	$NbMinFinal = 0;
	foreach($LstHeureJour as $D=>$Total):
		$NbMinFinal += ($Total >= 480) ? $Total-60 : $Total;
	endforeach;
	
    ?>

	</ul>
	<!-- NOMBRE TOTAL D'HEURE -->
    <p>
		Pour chaque journ&eacute;e d’intervention, le PRESTATAIRE percevra une r&eacute;mun&eacute;ration d’un montant global de <b><font style="color:#3B5998"><?php echo $ForfaitNet; ?></font></b>&euro; HT (frais<?php echo ($fraisInclus == 'NON' ? ' <strong>non</strong> ' : ' '); ?>compris)
	</p>

    <p>
        <?php
        if ($newAdresse) {
            echo 'Fait &agrave; Dijon<br />';
        } else {
            echo 'Fait &agrave; St Apollinaire<br />';
        }
        ?>
		Le <?php echo $dteContrat; ?><br />
		en deux exemplaires.<br />
		Signatures pr&eacute;c&eacute;d&eacute;es de la mention manuscrite "Lu et approuv&eacute;"
	</p>
    <br /><br />
    <p style="width:50%;float:left;">
    	Pour le PRESTATAIRE<br/>
		<?php echo $PrenomIntervenant.' '.$NomIntervenant; ?>
    </p>
    
    <p style="float:left;text-align:right;width:40%">
    	Pour la Soci&eacute;t&eacute; SUPPL'ACTIV :<br />
    	<img src="_html/signature2.png"/>
	</p>
</page>    