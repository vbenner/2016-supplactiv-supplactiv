<?php
# ------------------------------------------------------------------------------
# Retourne un Format 
# ------------------------------------------------------------------------------
function afficheHeureRemun($Minutes){
	if($Minutes%60 == 0){
		$HeureTotal = $Minutes/60;
		$HeureTotal = (strlen($HeureTotal) == 1) ? '0'.$HeureTotal : $HeureTotal;
		return $HeureTotal.'H';
	}else {
		$MinTotal = $Minutes%60;
		$HeureTotal = ($Minutes-$MinTotal)/60;
		$MinTotal = (strlen($MinTotal) == 1) ? '0'.$MinTotal : $MinTotal;
		$HeureTotal = (strlen($HeureTotal) == 1) ? '0'.$HeureTotal : $HeureTotal;
		return $HeureTotal.'H'.$MinTotal;
	}
}

$sqlRecherheFraisTel = '
SELECT idIntervention, fraisTelephone, DATE_FORMAT(dateDebut, "%Y-%m-%d") AS Dte
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_IdPdv
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE FK_idContrat = :IdContrat
ORDER BY dateDebut';
$RecherheFraisTelExc = DbConnexion::getInstance()->prepare($sqlRecherheFraisTel);

# ------------------------------------------------------------------------------
# Recherche des informations concernant le contrat
# ------------------------------------------------------------------------------
$sqlRechercheInfoIntervenant = '
SELECT idIntervenant, DATE_FORMAT(dateCreationContrat, "%d/%m/%Y") AS dteContrat,  
  nomIntervenant, prenomIntervenant, DATE_FORMAT(dateNaissance, "%d/%m/%Y") AS dateNaissance,
  villeNaissance, numeroSS, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C,
  codePostalIntervenant, villeIntervenant, nationalite, descriptionMission, FK_idTypeMission,
  dateCreationContrat
FROM su_intervenant
    INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE FK_idContrat = :IdContrat
GROUP BY idIntervenant';
$ResultContrat = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);

$sqlRechercheMontantRepas = "
SELECT fraisRepas, libelleGamme, descriptionMission
FROM su_intervention_frais
	INNER JOIN su_intervention ON su_intervention.idIntervention = su_intervention_frais.FK_idIntervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE FK_idContrat = :IdContrat";
$ResultMontantRepas = DbConnexion::getInstance()->prepare($sqlRechercheMontantRepas);

$sqlRechercheIntervention = '
SELECT DATE_FORMAT(dateDebut, "%d/%m/%Y") AS Date, DATE_FORMAT(dateDebut, "%HH%i") AS HeureD, 
  DATE_FORMAT(dateDebut, "%H") AS HeureDC,  DATE_FORMAT(dateDebut, "%i") AS MinDC, 
  DATE_FORMAT(dateFin, "%H") AS HeureFC, DATE_FORMAT(dateFin, "%i") AS MinFC, 
  DATE_FORMAT(dateFin, "%HH%i") AS HeureF, libellePdv, adressePdv_A, codePostalPdv, villePdv
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE FK_idContrat = :IdContrat
ORDER BY dateDebut';
$ResultIntervention = DbConnexion::getInstance()->prepare($sqlRechercheIntervention);

$sqlRechercheContrat = "
SELECT *
FROM su_contrat
WHERE idContrat = :IdContrat";
$ResultInfoContrat = DbConnexion::getInstance()->prepare($sqlRechercheContrat);

$IdTypeMission = ''; $DescriptionMission = ''; $dteContrat = '';
$ResultContrat->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
$ResultContrat->execute();
$newAdresse = false;

while($Contrat = $ResultContrat->fetch(PDO::FETCH_OBJ)):
    if ($Contrat->dateCreationContrat >= '2017-06-19') {
        $newAdresse = true;
    }
	$NomIntervenant     = $Contrat->nomIntervenant;
	$PrenomIntervenant  = $Contrat->prenomIntervenant;
	$DateNaissance  	= $Contrat->dateNaissance;
	$VilleNaissance 	= $Contrat->villeNaissance;
	$NumeroSS 			= $Contrat->numeroSS;
	
	$AdresseIntervenant = $Contrat->adresseIntervenant_A;
	$AdresseIntervenant.= ($Contrat->adresseIntervenant_B != '') ? ', '.$Contrat->adresseIntervenant_B : '';
	if($Contrat->adresseIntervenant_C != ''):
		$AdresseIntervenant.= ', '.$Contrat->adresseIntervenant_C;
	endif;
	$AdresseIntervenant.= ', '.$Contrat->codePostalIntervenant.' '.$Contrat->villeIntervenant;
	$Nationalite = ($Contrat->nationalite == 'F')? 'Francaise' : 'Etrang&egrave;re';
	
	$IdTypeMission = $Contrat->FK_idTypeMission;
	$DescriptionMission = $Contrat->descriptionMission;
    $dteContrat = $Contrat->dteContrat;
endwhile;

$idContrat = filter_input(INPUT_GET, 'idContrat');
$ResultInfoContrat->bindValue(':IdContrat', $idContrat, PDO::PARAM_INT);
$ResultInfoContrat->execute();
while($InfoContrat = $ResultInfoContrat->fetch(PDO::FETCH_OBJ)):
	$SalaireBrut = $InfoContrat->salaireBrut;
	$AutorisationFraisTel = $InfoContrat->autorisationFraisTel;
	$VehiculePersonnel = $InfoContrat->boolVehiculePersonnel;
	$VehiculeFonction = $InfoContrat->boolVehiculeFonction;
	$TarifKM = $InfoContrat->tarifKM;
	$SocieteA = $InfoContrat->societeA;
	$DetailContrat = $InfoContrat->detailContrat;
endwhile;

$MontantFraisTel = 0;
if($AutorisationFraisTel == 'OUI')
{
	$LstFrais = array();
	$RecherheFraisTelExc->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
	$RecherheFraisTelExc->execute();
	while($InfoFraisTel = $RecherheFraisTelExc->fetch(PDO::FETCH_OBJ))
	{
		@$LstFrais[$InfoFraisTel->Dte] = $InfoFraisTel->fraisTelephone;
	}

	$NbTotalJour = 0;
	foreach($LstFrais as $Dte=> $Val)
	{
		$NbTotalJour++;
		$MontantFraisTel += $Val;
	}

	$MontantFraisTel = round($MontantFraisTel/$NbTotalJour, 2);
}

$MontantRepas = 0; $DetailMission = '';
$ResultMontantRepas->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
$ResultMontantRepas->execute();
while($InfoMontant = $ResultMontantRepas->fetch(PDO::FETCH_OBJ)) {
    $MontantRepas = ($InfoMontant->fraisRepas != 0.00) ? $InfoMontant->fraisRepas : $MontantRepas;
    $GammeProduit = $InfoMontant->libelleGamme;
    $DetailMission = $InfoMontant->descriptionMission;
}

?>
<style type="text/css">
	h4{
		text-align:center;
		text-decoration:underline;
		margin:5px 0px;
		font-size:13px;
	}
	
	p{
		text-align: justify;
		margin: 5px 0px;
	}
</style>
<?php
/** -------------------------------------------------------------------------------------------
 * DONNEES COFFREO
 * $intervenant
 */
$sqlIntervenant = '
SELECT DISTINCT SUI.*, DATE_FORMAT(dateNaissance, \'%d/%m/%Y\') AS dateFR
FROM su_intervenant SUI
INNER JOIN su_intervention SI ON SI.FK_idIntervenant = SUI.idIntervenant
INNER JOIN su_contrat SC ON SC.idContrat = SI.FK_idContrat
WHERE SC.idContrat = :idContrat
';

$intervenantExec = DbConnexion::getInstance()->prepare($sqlIntervenant);
$intervenantExec->bindValue(':idContrat', $idContrat, PDO::PARAM_INT);
$intervenantExec->execute();
$intervenant = $intervenantExec->fetch(PDO::FETCH_OBJ);

$coffreo['resource']['resourceId'] = $intervenant->idIntervenant;
$coffreo['resource']['title'] = ($intervenant->genre == 'E' || $intervenant->genre == 'L' ? 'Mme' : 'M') ;
$coffreo['resource']['firstname'] = $intervenant->prenomIntervenant;
$coffreo['resource']['lastname'] = $intervenant->nomIntervenant;
$coffreo['resource']['birthdate'] = $intervenant->dateFR;
$coffreo['resource']['address1'] = $intervenant->adresseIntervenant_A;
$coffreo['resource']['address2'] = $intervenant->adresseIntervenant_B;
$coffreo['resource']['address3'] = $intervenant->adresseIntervenant_C;
$coffreo['resource']['zip'] = $intervenant->codePostalIntervenant;
$coffreo['resource']['city'] = $intervenant->villeIntervenant;
$coffreo['resource']['country'] = 'FR';
$coffreo['resource']['email'] = $intervenant->mailIntervenant;
$coffreo['resource']['cellPhone'] = $intervenant->mobileIntervenant;

$sqlIntervention = '
SELECT DATE_FORMAT(MIN(dateDebut), \'%d/%m/%Y\') AS DATEDEB, 
       DATE_FORMAT(MAX(dateFin), \'%d/%m/%Y\') AS DATEFIN,
       DATE_FORMAT(MIN(dateDebut), \'%H:%i:%s\') AS HDEB, 
       DATE_FORMAT(MAX(dateFin), \'%H:%i:%s\') AS HFIN
FROM su_intervention
WHERE FK_idContrat = :idContrat
';
$interventionExec = DbConnexion::getInstance()->prepare($sqlIntervention);
$interventionExec->bindValue(':idContrat', $idContrat, PDO::PARAM_INT);
$interventionExec->execute();
$intervention = $interventionExec->fetch(PDO::FETCH_OBJ);

$coffreo['md']['startDate'] = $intervention->DATEDEB;
$coffreo['md']['startTime'] = $intervention->HDEB;
$coffreo['md']['endDate'] = $intervention->DATEFIN;

?>

<page>
    <p>
    [issuer}<br/>
    #providerId{pme}<br/>
    #customerId{supplactiv}<br/>
    #entityId{supplactiv}<br/>
    #siret{528 147 150 00023}<br/>
    #s.name{SUPPL'ACTIV}<br/>
    #s.address1{7-9, Boulevard Rembrandt}<br/>
    #s.address2{Bât Apogée C}<br/>
    #s.address3{}<br/>
    #s.zip{21000}<br/>
    #s.city{DIJON}<br/>
    #s.country{FR}<br/>
    {issuer]
    </p>
    <p>
    [signatory}<br/>
    #title{M}<br/>
    #firstname{Alain}<br/>
    #lastname{BERTHAUD}<br/>
    #email{aberthaud@supplactiv.fr}<br/>
    {signatory]
    </p>
    <p>
    [document}<br/>
    #date:fr{<?php echo date('d/m/Y'); ?>}<br/>
    #version{v4.3}<br/>
    #type{FR_CTU}<br/>
    #oid{<?php echo $idContrat; ?>}<br/>
    {document]
    </p>
    <p>
    [resource}<br/>
    #resourceId{<?php print $coffreo['resource']['resourceId']; ?>}<br/>
    #title{<?php echo $coffreo['resource']['title']; ?>}<br/>
    #firstname{<?php echo $coffreo['resource']['firstname']; ?>}<br/>
    #lastname{<?php echo $coffreo['resource']['lastname']; ?>}<br/>
    #birthdate:fr{<?php echo $coffreo['resource']['birthdate']; ?>}<br/>
    #address1{<?php echo $coffreo['resource']['address1']; ?>}<br/>
    #address2{<?php echo $coffreo['resource']['address2']; ?>}<br/>
    #address3{<?php echo $coffreo['resource']['address3']; ?>}<br/>
    #zip{<?php echo $coffreo['resource']['zip']; ?>}<br/>
    #city{<?php echo $coffreo['resource']['city']; ?>}<br/>
    #country{<?php echo $coffreo['resource']['country']; ?>}<br/>
    #email{<?php echo $coffreo['resource']['email']; ?>}<br/>
    #cellPhone{<?php echo $coffreo['resource']['cellPhone']; ?>}<br/>
    {resource]
    </p>
    <p>
    [staffingCustomer}<br/>
    #ws:name{}<br/>
    #ws:workSiteCode{}<br/>
    #ws:address1{}<br/>
    #ws:address2{}<br/>
    #ws:address3{}<br/>
    #ws:zip{}<br/>
    #wscity{}<br/>
    #ws:country{}<br/>
    {staffingCustomer]
    </p>
    <p>
    [positionCharacteristics}<br/>
    #positionStatus{}<br/>
    #positionTitle{}<br/>
    {positionCharacteristics]
    </p>
    <p>
    [md}<br/>
    #startDate:fr{<?php echo $coffreo['md']['startDate']; ?>}<br/>
    #startTime{<?php echo $coffreo['md']['startTime']; ?>}<br/>
    #endDate:fr{<?php echo $coffreo['md']['endDate']; ?>}<br/>
    #currency{EUR}<br/>
    #paidAmount{}<br/>
    {md]
    </p>
</page>
<page style="font-size:11px">
	<div style="padding-bottom:10px;text-align:left"><img src="_html/logo_Entete.png" /></div>
        
	<div style="border:1px solid Black">
        <h3 style="text-align:center;margin:0px;padding:4px 0px;">
            CONTRAT D'INTERVENTION A DUREE DETERMINEE
        </h3>
    </div>

	<p style="text-align:justify;margin-top:10px;">
        <i>Entre les soussign&eacute;s :</i><br/><br/>
        <?php
        if ($newAdresse) {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        } else {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 24, Rue de la Redoute 21850 St Apollinaire, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        }
        ?>
        <br/><br/>
        <i>D'une part</i><br/>
        <i>Et,</i>
    </p>
    
    <p style="margin-top:10px;">	
        <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant ?></font>, n&eacute;(e) le 
        <font style="color:#3B5998"><?php echo $DateNaissance ?></font> &agrave; 
        <font style="color:#3B5998"><?php echo $VilleNaissance ?></font>, de nationalit&eacute; 
        <font style="color:#3B5998"><?php echo $Nationalite ?></font>,<br/> demeurant au : 
        <font style="color:#3B5998"><?php echo $AdresseIntervenant ?></font>.<br/>
        Num&eacute;ro de s&eacute;curit&eacute; sociale 
        <font style="color:#3B5998"><?php echo $NumeroSS ?></font>, ci apr&egrave;s d&eacute;nomm&eacute; l'intervenant,<br/>
        <i>D'autre part</i><br/>
        <b>Il a &eacute;t&eacute; convenu ce qui suit :</b>
    </p>
    
    <!-- ARTICLE 1 -->
    <h4>ARTICLE 1 - Engagement</h4>
    <p>
    	 L'intervenant atteste avoir r&eacute;alis&eacute; une visite m&eacute;dicale du travail au cours des 24 derniers mois et &ecirc;tre apte &agrave; r&eacute;aliser les missions pour lesquelles il sera missionn&eacute;. A ce titre, il pr&eacute;sentera &agrave; la soci&eacute;t&eacute; SUPPL'ACTIV, son dernier avis d'aptitude. <br/>
         La soci&eacute;t&eacute; SUPPL'ACTIV engage l'intervenant en qualit&eacute; <?php echo ($IdTypeMission == 1) ? "de charg&eacute; d'optimisation lin&eacute;aire" : (($IdTypeMission == 6) ? "de charg&eacute; de relev&eacute;s d'implantation" : "d'animateur - formateur commercial") ?>, niveau 1, coefficient 120 de la Convention Collective nationale des Prestataires de services.<br/>
         La d&eacute;claration nominative pr&eacute;alable &agrave; l'embauche a &eacute;t&eacute; remise &agrave; l'URSSAF de Dijon.<br/>
         Conform&eacute;ment &agrave; la loi du 6 janvier 1978, l'intervenant a un droit d'acc&egrave;s et de rectification aux informations port&eacute;es sur ce document.
    </p>
    
    <!-- ARTICLE 2 -->
    <h4>ARTICLE 2 - Objet, dur&eacute;e et lieu d'ex&eacute;cution du contrat</h4>
    <p>
    	Ce contrat est conclu dans le cadre d'un Contrat d'Intervention &agrave; Dur&eacute;e D&eacute;termin&eacute;e, conform&eacute;ment &agrave; la Convention collective des Prestataires de services. Ce contrat est conclu pour les dates et horaires suivants :
    </p>
    
    <!-- ON LISTE LES INTERVENTIONS -->
    <?php
    //Initialisation du nombre d'heure 
    $LstHeureJour = array();
    $ResultIntervention->bindValue(':IdContrat', filter_input(INPUT_GET, 'idContrat'), PDO::PARAM_INT);
	$ResultIntervention->execute();
	while($InfoInter = $ResultIntervention->fetch(PDO::FETCH_OBJ)):

        $nbTotalH = ((($InfoInter->HeureFC*60)+$InfoInter->MinFC)-(($InfoInter->HeureDC*60)+$InfoInter->MinDC));
		echo "
		<p style='color:#3B5998;margin:0px;padding:0px;'>
			Le $InfoInter->Date de $InfoInter->HeureD &agrave; $InfoInter->HeureF ($InfoInter->libellePdv, $InfoInter->adressePdv_A, $InfoInter->codePostalPdv $InfoInter->villePdv) ".(($nbTotalH >= 480) ? "dont 1 heure de pause" : "")."
		</p>";
		
		@$LstHeureJour[$InfoInter->Date]+= $nbTotalH;
	endwhile;
	
	//On calcul le nombre d'heure total remunere
	$NbMinFinal = 0;
	foreach($LstHeureJour as $D=>$Total):
		$NbMinFinal += ($Total >= 480) ? $Total-60 : $Total;
	endforeach;
	
    ?>
    <!-- NOMBRE TOTAL D'HEURE --> 
    <p>
    	Pour ce contrat, le nombre d'heures r&eacute;mun&eacute;r&eacute;es sera de <font style="color:#3B5998"><?php echo afficheHeureRemun($NbMinFinal) ?></font>.
    </p>
    
    <!-- ARTICLE 3 -->
    <h4>ARTICLE 3 - P&eacute;riode d'essai</h4>
    <p>
    	Ce contrat est conclu avec une p&eacute;riode d'essai de <font style="color:#3B5998"><?php echo afficheHeureEssaiContrat($NbMinFinal) ?></font>
    </p>
    
    <!-- ARTICLE 4 -->
    <h4>ARTICLE 4 - R&eacute;mun&eacute;ration, frais et indemnit&eacute;s</h4>
    <p>
        En contrepartie de son intervention, l'intervenant percevra une r&eacute;mun&eacute;ration horaire brute de 
        <font style="color:#3B5998"><?php echo $SalaireBrut; ?> &euro;</font>
        
        <?php
        //Frais Tel 
        echo ($AutorisationFraisTel == 'OUI') ? ' et une participation aux frais t&eacute;l&eacute;phoniques de <font style="color:#3B5998">'.$MontantFraisTel.' &euro;</font> par journ&eacute;e travaill&eacute;e' : '';
        
        //Frais Repas
        echo ($MontantRepas != 0) ? ', ainsi qu\'une indemnit&eacute; repas d\'un montant de <font style="color:#3B5998">'.$MontantRepas.' &euro;.</font>' : '.';
        ?>
    </p>
    <?php
    if($VehiculePersonnel == 'OUI'):
		echo '
		<p>
			En sus, pour ses d&eacute;placements, l\'intervenant percevra une indemnit&eacute; de <font style="color:#3B5998">'.$TarifKM.' &euro;</font> par km parcouru. Les frais de p&eacute;age et de parking seront rembours&eacute;s sur pr&eacute;sentation de justificatifs.
		</p>';
	endif;
	if($VehiculeFonction == 'OUI'):
		echo '
		<p>
			En sus, les frais de p&eacute;age, de parking et d\'essence, sur justificatif.	
		</p>';
	endif;
    ?>	
    <p>
        Il pourra &eacute;galement &ecirc;tre demand&eacute; &agrave; l'intervenant d'effectuer des heures compl&eacute;mentaires qui lui seront r&eacute;mun&eacute;r&eacute;es conform&eacute;ment aux dispositions l&eacute;gales et conventionnelles en vigueur.<br/>
        Tout autre frais ne pourra &ecirc;tre engag&eacute; pour le compte de la soci&eacute;t&eacute; SUPPL'ACTIV que sur autorisation &eacute;crite de celle-ci et sera rembours&eacute; sur la base de justificatifs.
    </p>

    <br /><br />
    <?php
    /** ---------------------------------------------------------------------------------------
     * Modification 2021-05-28 - Suppression du bloc final / Demande Marine
     */
    if ($newAdresse) {
        echo 'Fait &agrave; Dijon ';
    } else {
        echo 'Fait &agrave; St Apollinaire ';
    }
    echo 'le '.$dteContrat.'<br/>';
    #echo 'en deux exemplaires.<br />';
    #echo 'Signatures pr&eacute;c&eacute;d&eacute;es de la mention manuscrite "Lu et approuv&eacute;"';
    ?>
    <p style="width:50%;float:left;">
        Pour la soci&eacute;t&eacute; Suppl'ACTIV :<br />
        <!-- 2021-05-28 - Suppression de l'image -->
        <!--
    	<img src="_html/signature2.png"/>
    	-->
    </p>

    <p style="float:left;text-align:right;width:40%">
        L'intervenant :
    </p>

</page>
<!-- SAUT DE PAGE GÉNÉRÉ LE - 2021-05-28 / Mail Marine !-->
<page style="font-size:11px">

<!-- ARTICLE 5 -->
    <h4>ARTICLE 5 - Fonctions</h4>
    
    <p>
        L'intervenant exercera les fonctions suivantes pour le compte de la soci&eacute;t&eacute; : <font style="color:#3B5998"><?php echo $SocieteA; ?></font><br/>
    </p>
    <ul style="margin-left:20px;">
        <li><?php echo ($IdTypeMission == 1) ? "Optimisation de lin&eacute;aires, mise en valeur des produits " : (($IdTypeMission == 6) ? "Recensement - relev&eacute;s d'implantation - m&eacute;trage" : "Animation - Formation sur les produits ") ?> <font style="color:#3B5998"><?php echo $GammeProduit; ?></font></li>
        <?php echo ($DetailContrat != '') ? '<li>'.$DetailContrat.'</li>' : ''; ?>
        <?php echo ($DetailMission != '') ? '<li>'.$DetailMission.'</li>' : ''; ?>
    </ul> 
    <?php if ($IdTypeMission != 1): ?> 
    <p>
        Dans le cadre de cet emploi, l'intervenant s'engage , sur le temps d’intervention convenu par le présent contrat de travail à élaborer, lors de chaque mission, un rapport d’activité destiné à restituer l’information auprès de la Société SUPPL’ACTIV sous la forme qui lui aura été précisée. L’intervenant s’engage également à transmettre ce rapport ainsi que les résultats de l’intervention à la Société SUPPLACTIV  dans les 8 jours suivants l’intervention.
    </p> 
    <?php endif; ?>
    <!-- ARTICLE 6 -->
    <h4>ARTICLE 6 - Lien de subordination</h4>
    <p>
        Il est express&eacute;ment rappel&eacute; &agrave; l'intervenant qu'il demeure sous la subordination de la soci&eacute;t&eacute; SUPPL'ACTIV pendant toute la dur&eacute;e de son contrat.<br/>
        Il ne recevra donc ses instructions que de la soci&eacute;t&eacute; SUPPL'ACTIV et ne pourra modifier la teneur de sa mission sans accord pr&eacute;alable de la soci&eacute;t&eacute; SUPPL'ACTIV.	
    </p>
    
    <!-- ARTICLE 7 -->
    <h4>ARTICLE 7 - V&eacute;hicule</h4>
    <p>
        Il est express&eacute;ment convenu que l'intervenant dispose de son permis de conduire, et d'une assurance indispensable &agrave; l'ex&eacute;cution de sa mission. L'intervenant s'engage &agrave; informer la soci&eacute;t&eacute; SUPPL'ACTIV sans d&eacute;lai en cas de suspension ou de retrait de son permis de conduire.<br/>
        L'intervenant s'engage &agrave; utiliser le v&eacute;hicule dans le respect du Code de la route.			
    </p>
    <?php
    if($VehiculeFonction == 'OUI'):
	    ?>
	    <p>
	        La soci&eacute;t&eacute; SUPPL'ACTIV met &agrave; la disposition de l'intervenant un v&eacute;hicule pour se rendre sur son lieu de travail. Il est express&eacute;ment convenu qu'il s'agit d'un v&eacute;hicule de fonction r&eacute;serv&eacute; exclusivement &agrave; un usage professionnel.<br/><br/>
	        
	        D&egrave;s lors qu'il n'utilise pas son v&eacute;hicule personnel, aucune indemnit&eacute; kilom&eacute;trique ne sera vers&eacute;e &agrave; l'intervenant.	                
	    </p>	
	<?php
	endif;
	?>
	<?php if ($IdTypeMission != 1): ?> 
	<!-- ARTICLE 8 -->
	<h4>ARTICLE 8 - Indemnisation en cas d'annulation</h4>
	<p>
        En cas d'annulation de l'intervention par la soci&eacute;t&eacute; SUPPL'ACTIV, l'intervenant percevra une indemnit&eacute;e dans les conditions suivantes :					
    </p>
    <ul style="margin-left:20px">
        <li>Aucune indemnisation en cas d'annulation au moins 7 jours avant la date du d&eacute;but de la mission.</li>
        <li>50 % du salaire brut qu'il aurait per&ccedil;u en cas d'annulation entre 7 et 3 jours avant le d&eacute;but de la mission.</li>
        <li>100 % de son salaire brut en cas d'annulation dans les 72 heures avant le d&eacute;but de la mission.</li>
    </ul>
    <?php endif; ?>
    <!-- ARTICLE 9 -->
    <h4>ARTICLE <?php echo ($IdTypeMission == 1)? 8 : 9 ?> - Hygi&egrave;ne et s&eacute;curit&eacute;</h4>
	<p>
		Bien que salari&eacute; de la soci&eacute;t&eacute; SUPPL'ACTIV, l'intervenant s'engage &agrave; respecter les consignes qui peuvent lui &ecirc;tre donn&eacute;es par la direction de l'&eacute;tablissement dans les locaux duquel il effectue sa prestation en ce qui concerne l'hygi&egrave;ne et la s&eacute;curit&eacute;.<br/>
		L'intervenant s'engage &eacute;galement &agrave; en respecter le r&egrave;glement int&eacute;rieur.				
	</p>
		
	<!-- ARTICLE 10 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 9 : 10 ?> - Cong&eacute;s pay&eacute;s et Pr&eacute;carit&eacute;</h4>
	<p>
		L'intervenant b&eacute;n&eacute;ficiera d'une indemnit&eacute; compensatrice de cong&eacute;s pay&eacute;s, ainsi que d'une indemnit&eacute; de pr&eacute;carit&eacute; selon les dispositions l&eacute;gales et conventionnelles en vigueur.			
	</p>
	
	<!-- ARTICLE 11 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 10 : 11 ?> - Confidentialit&eacute;</h4>
	<p>
		L'intervenant s'engage &agrave; observer la discr&eacute;tion la plus stricte sur les informations se rapportant aux activit&eacute;s de la soci&eacute;t&eacute; auxquelles il aura acc&egrave;s &agrave; l'occasion et dans le cadre de ses fonctions.		
	</p>
	
	<!-- ARTICLE 12 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 11 : 12 ?> - Respect de la dur&eacute;e du temps de travail</h4>
	<p>
		L'intervenant s'engage &agrave; respecter les dur&eacute;es maximales l&eacute;gales du travail fix&eacute;es actuellement &agrave; 48 heures hebdomadaires, 44 heures hebdomadaires sur 12 semaines cons&eacute;cutives et &agrave; 10 heures par jour.
	</p>
	
	<!-- ARTICLE 13 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 12 : 13 ?> - Priorit&eacute; d'embauche</h4>
	<p>
		Si un poste sous contrat &agrave; dur&eacute;e ind&eacute;termin&eacute;e venait &agrave; &ecirc;tre cr&eacute;&eacute; pendant la dur&eacute;e d'ex&eacute;cution du pr&eacute;sent contrat, celui-ci sera propos&eacute; en priorit&eacute; aux salari&eacute;s de la soci&eacute;t&eacute; SUPPL'ACTIV, dont l'intervenant, sans toutefois que cette priorit&eacute; ne lui assure l'embauche.	
	</p>
	
	<!-- ARTICLE 14 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 13 : 14 ?> - Comportement et tenue</h4>
	<p>
        Compte tenu du contact régulier avec la clientèle et du lieu des interventions (officines pharmaceutiques), l’intervenant devra se présenter dans une tenue correcte et soignée assurant le respect de l’hygiène et de sa sécurité ainsi que celle des clients (port d’une blouse).
    </p>
	
	<!-- ARTICLE 15 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 14 : 15 ?> - Mat&eacute;riel</h4>
	<p>
		Les biens de toute nature qui seront remis par la Soci&eacute;t&eacute; SUPPL'ACTIV ou par une soci&eacute;t&eacute; cliente de SUPPL'ACTIV &agrave; l'intervenant(e) <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant; ?></font> pour l'ex&eacute;cution de ses fonctions, ne sont d&eacute;tenus par lui/elle qu'&agrave; titre pr&eacute;caire.<br/>
		L'intervenant(e) <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant; ?></font> s'engage express&eacute;ment &agrave; les restituer &agrave; premi&egrave;re demande.<br />
		L'intervenant(e) <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant; ?></font> est garant(e) de leur maintien en parfait &eacute;tat et ne peut ni les pr&ecirc;ter, ni les louer, ni les c&eacute;der &agrave; des tiers.
		En cas de rupture du pr&eacute;sent contrat, et en tout &eacute;tat de cause d&egrave;s la fin de la mission, l'intervenant(e) <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant; ?></font> sera tenu de restituer &agrave; la soci&eacute;t&eacute;, sans d&eacute;lai, l'ensemble des biens, documents, tarifs et documents commerciaux qui lui auraient &eacute;t&eacute; remis.
	</p>
	
	<!-- ARTICLE 16 -->
	<h4>ARTICLE <?php echo ($IdTypeMission == 1)? 15 : 16 ?> - Retraite et pr&eacute;voyance</h4>
    <p>
		L'intervenant b&eacute;n&eacute;ficiera des r&eacute;gimes de retraite et de pr&eacute;voyance souscrits par la soci&eacute;t&eacute; SUPPL'ACTIV : KLESIA et MALAKOFF HUMANIS.
	</p>
    
    <!-- ARTICLE 17 -->
    <h4>ARTICLE <?php echo ($IdTypeMission == 1)? 16 : 17 ?> - Dispositions diverses</h4> 
    <p>
        L'intervenant reconna&icirc;t avoir &eacute;t&eacute; inform&eacute; que la convention collective des Prestataires de services est applicable au pr&eacute;sent contrat.<br/>
        L'intervenant d&eacute;clare &ecirc;tre libre de tout engagement &agrave; la date de son embauche et n'&ecirc;tre tenue par aucune clause de non concurrence qui lui interdirait de conclure le pr&eacute;sent contrat.
    </p>
</page>