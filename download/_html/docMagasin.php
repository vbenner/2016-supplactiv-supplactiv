<?php

$ListeMoisFR = array(	
	'01'=>'Janvier',
	'02'=>'F&eacute;vrier',
	'03'=>'Mars',
	'04'=>'Avril',
	'05'=>'Mai',
	'06'=>'Juin',
	'07'=>'Juillet',
	'08'=>'Aout',
	'09'=>'Septembre',
	'10'=>'Octobre',
	'11'=>'Novembre',
	'12'=>'D&eacute;cembre'
);
					
					
$sqlRechercheInfoIntervention = "
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, dateNaissance, codePostalIntervenant, 
	   villeIntervenant, DATE_FORMAT(dateDebut, '%d/%m/%Y') AS Dte, libellePdv, codePostalPdv, villePdv, telephoneMagasin, 
	   faxPdv, emailPdv, DATE_FORMAT(dateDebut, '%HH%i') AS HeureD, DATE_FORMAT(dateFin, '%HH%i') AS HeureF, adressePdv_A,
       dateDebut,
(
	SELECT MIN(dateDebut)
	FROM su_intervention
	WHERE FK_idIntervenant = INTER.idIntervenant
) AS dateMin
FROM su_intervenant INTER
    INNER JOIN su_intervention DTE ON DTE.FK_idIntervenant = INTER.idIntervenant
    INNER JOIN su_pdv PDV ON PDV.idPdv = DTE.FK_idPdv
WHERE idIntervention = :idIntervention";
$RechercheInfoInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervention);

$sqlRechercheMontantRepas = "
SELECT fraisRepas, libelleGamme
FROM su_intervention_frais
	INNER JOIN su_intervention ON su_intervention.idIntervention = su_intervention_frais.FK_idIntervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE idIntervention = :IdIntervention";
$ResultMontantRepas = DbConnexion::getInstance()->prepare($sqlRechercheMontantRepas);

$sqlRechercheIntAgence = "
SELECT nomInterlocuteurAgence, prenomInterlocuteurAgence, libelleTypeMission, FK_idCategorie, FK_idSousCategorie, idClient, logoClient, signatureClient
FROM su_agence_interlocuteur
	INNER JOIN su_campagne ON su_agence_interlocuteur.idInterlocuteurAgence = su_campagne.FK_idInterlocuteurAgence
	INNER JOIN su_mission ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_mission_type ON su_mission_type.idTypeMission = su_mission.FK_idTypeMission
	INNER JOIN su_intervention ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
	INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient
WHERE idIntervention =  :IdIntervention";
$RechercheIntAgence = DbConnexion::getInstance()->prepare($sqlRechercheIntAgence);


$RechercheInfoInterventionExc->bindValue(':idIntervention', filter_input(INPUT_GET, 'idIntervention'), PDO::PARAM_INT);
$RechercheInfoInterventionExc->execute();
$newAdresse = false;
while($InfoContrat = $RechercheInfoInterventionExc->fetch(PDO::FETCH_OBJ)):

    if ($InfoContrat->dateDebut >= '2017-06-19') {
        $newAdresse = true;
    }
    #echo 'MAINTENANCE';
    #echo '<pre>';
    #print_r($InfoContrat);
    #echo '</pre>';
	$LibellePdv = $InfoContrat->libellePdv;
	$CodePostal_Ville = $InfoContrat->codePostalPdv.' '.$InfoContrat->villePdv;
	$AdressePdv = $InfoContrat->adressePdv_A;
	$TelMagasin = $InfoContrat->telephoneMagasin;
	$FaxPdv = $InfoContrat->faxPdv;
	$EmailPdv = $InfoContrat->emailPdv;
	$NomIntervenant = $InfoContrat->nomIntervenant;
	$PrenomIntervenant = $InfoContrat->prenomIntervenant;
	$DateIntervention = substr($InfoContrat->Dte,0,2).' '.$ListeMoisFR[substr($InfoContrat->Dte,3,2)].' '.substr($InfoContrat->Dte,-4);
	$HeureIntervention = $InfoContrat->HeureD.' &agrave; '.$InfoContrat->HeureF;
	$HeureInterventionD = $InfoContrat->HeureD;

	$AdrInter = $InfoContrat->adresseIntervenant_A.', '.$InfoContrat->codePostalIntervenant.' '.$InfoContrat->villeIntervenant;
	$DteNaissance = $InfoContrat->dateNaissance;
	$DteMin = substr($InfoContrat->dateMin, 0, 10);
endwhile;

$RechercheInfoContratExc->bindValue(':idContrat', $infoIntervention->FK_idContrat, PDO::PARAM_INT);
$RechercheInfoContratExc->execute();
while($InfoContrat = $RechercheInfoContratExc->fetch(PDO::FETCH_OBJ)):
	$SocieteA = $InfoContrat->societeA;
	$DetailContrat = $InfoContrat->detailContrat;
endwhile;


$GammeProduit = '';
$ResultMontantRepas->bindValue(':IdIntervention', filter_input(INPUT_GET, 'idIntervention'), PDO::PARAM_INT);
$ResultMontantRepas->execute();
while($InfoMontant = $ResultMontantRepas->fetch(PDO::FETCH_OBJ)):
	$GammeProduit = $InfoMontant->libelleGamme;
endwhile;



$RechercheIntAgence->bindValue(':IdIntervention', filter_input(INPUT_GET, 'idIntervention'), PDO::PARAM_INT);
$RechercheIntAgence->execute();
while($InfoIntervention = $RechercheIntAgence->fetch(PDO::FETCH_OBJ)):
	$IntAgence = $InfoIntervention->nomInterlocuteurAgence.' '.$InfoIntervention->prenomInterlocuteurAgence;
	$TypePrestation = $InfoIntervention->libelleTypeMission;
	$CategoriePDV = $InfoIntervention->FK_idCategorie;
	$SSCategoriePDV = $InfoIntervention->FK_idSousCategorie;
	$LogoClient = $InfoIntervention->logoClient;
	$SignatureClient = $InfoIntervention->signatureClient;
	$IdClient = $InfoIntervention->idClient;
endwhile;

?>

<!-- PAGE D'ENTETE -->
<page style="font-size: 14pt">
	<div style="padding-bottom:10px;text-align:left;border-bottom:1px double Black;"><img src="_html/logo_Entete.png" /></div>
	<p style="text-align:center;margin-bottom:70px;font-size:12pt;">
        <?php
        if ($newAdresse) {
            echo '7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon  TEL : 03 45 42 70 23';
        } else {
            echo '24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE  TEL : 03 45 42 70 23';
        }
		?>
	</p>
	<h3 style="font-size:30pt">
		Telecopie
	</h3>
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>A : <?php echo $LibellePdv.' à '.$CodePostal_Ville ?></b>
	</div>
	
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>De : Agence SUPPL'ACTIV</b>
	</div>
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>T&eacute;l&eacute;phone : <?php echo $TelMagasin ?></b>
	</div>
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>T&eacute;l&eacute;copie : <?php echo $FaxPdv ?></b>
	</div>
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>Email : <?php echo $EmailPdv ?></b>
	</div>
	<div style="border-bottom:1px solid black;font-size:12pt;margin-bottom:15px;">
		<b>Nombres de pages : 3</b>
	</div>
	
	<p style="font-size:14pt;">
		A l'attention du PC S&eacute;curit&eacute;
		<br/>
		<br/>
		Veuillez trouver ci-joint les documents relatifs à l'animation/formation qui sera effectu&eacute;e dans votre point de vente le
		<?php echo $DateIntervention ?>, par notre animatrice/formatrice <?php echo $NomIntervenant.' '.$PrenomIntervenant ?>, sur la gamme <?php echo $GammeProduit ?>.
		<br/><br/>
		Bien Cordialement
		<br/><br/>
		Alain BERTHAUD
		<br/>
		Pr&eacute;sident.
	</p>
	<img  width="200" height="118" src="_html/signature.png"/>
</page>


<!-- ORDRE DE MISSION -->
<page style="font-size: 14pt">
	<div style="padding-bottom:10px;text-align:left;border-bottom:1px double Black;"><img src="_html/logo_Entete.png" /></div>
	<p style="text-align:center;margin-bottom:70px;font-size:12pt;">
        <?php
        if ($newAdresse) {
            echo '7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon  TEL : 03 45 42 70 23';
        } else {
            echo '24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE  TEL : 03 45 42 70 23';
        }
        ?>
	</p>
	<h3 style="font-size:30pt;text-align:center;margin-bottom:50px;">
		ORDRE DE MISSION
	</h3>
	
	<p style="font-size:12pt">
		<b>A l'attention du responsable PDV :</b>
	</p>
	<p style="font-size:14pt;">
		<br/>
        En accord avec les termes de vos conditions générales d’accès au magasin, nous
            vous rappelons que  l'animatrice/formatrice
		<?php echo $NomIntervenant.' '.$PrenomIntervenant ?>	
		interviendra dans lespace parapharmacie pour le compte de notre client 		
		<?php echo $SocieteA ?> sur la gamme <?php echo $GammeProduit ?> 
		aux dates suivantes :<br/><br/>
		Le <?php echo $DateIntervention.' de '.$HeureIntervention ?>
		<br/><br/><br/>
		Nous vous remercions par avance de lui r&eacute;server le meilleur accueil.
		<br/><br/><br/>
		La S&eacute;curit&eacute; de votre magasin a &eacute;t&eacute; pr&eacute;venue par fax de cette intervention.
		<br/><br/><br/>
		Nous nous tenons à votre disposition pour vous apporter toute pr&eacute;cision sur cette animation/formation, et vous prions de croire en lexpression de nos salutations distingu&eacute;es.
		<br/><br/><br/>
        Le Chef de Projet Trade Marketing
		<br/><br/>
		<?php echo $IntAgence ?>
	</p>
</page>


<!-- PC SECURITE -->
<page style="font-size: 14pt">
	<div style="padding-bottom:10px;text-align:left;border-bottom:1px double Black;"><img src="_html/logo_Entete.png" /></div>
	<p style="text-align:center;margin-bottom:20px;font-size:12pt;">
        <?php
        if ($newAdresse) {
            echo '7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon  TEL : 03 45 42 70 23';
        } else {
            echo '24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE  TEL : 03 45 42 70 23';
        }
        ?>
	</p>
	<h3 style="font-size:30pt;text-align:center;margin-bottom:20px;">
		A L'ATTENTION DE LA S&Eacute;CURIT&Eacute;
	</h3>
	
	<p style="font-size:12pt">
		<b>Magasin : <?php echo $LibellePdv ?></b>
	</p>
	<p style="font-size:12pt">
		<b>Fax : <?php echo $FaxPdv ?></b>
	</p>
	
	<p style="font-size:14pt;">
		<br/>
		Je soussign&eacute;, Alain BERTHAUD, en ma qualit&eacute; de pr&eacute;sident de la soci&eacute;t&eacute; SUPPL'ACTIV, certifie que la prestation de <?php echo $TypePrestation ?> :
		<br/><br/>
		
		Pour le compte de  : <?php echo $SocieteA ?>
		<br/><br/>
		Sur la gamme : <?php echo $GammeProduit ?>
		<br/><br/>
		Date : Le <?php echo $DateIntervention.' de '.$HeureIntervention ?>
		<br/><br/>
		sera r&eacute;alis&eacute;e par l'animatrice/formatrice <?php echo $NomIntervenant.' '.$PrenomIntervenant ?> employ&eacute;e r&eacute;guli&egrave;rement au regard des articles L143-3, L143-5 et L620-2 du Code du Travail.
		<br/><br/><br/>
		SUPPL'ACTIV respecte les dispositions l&eacute;gales en vigueur et lensemble de la r&eacute;glementation sociale, et est à jour dans le versement des cotisations aux divers organismes sociaux.
		<br/><br/>
		Restant à votre disposition, je vous prie de croire en lexpression de mes salutations distingu&eacute;es.
		<br/><br/>
		Alain BERTHAUD
		<br/>
		Pr&eacute;sident.
	</p>
	<img width="240" height="142" src="_html/signature.png"/>
</page>

<!-- KBIS -->
<page style="font-size: 14pt">
	<img src="_html/kbis.png"/>
</page>

	<page style="font-size: 14pt">
		<img src="_html/kbis2.png"/>
	</page>
<!-- URSSAF -->
<page style="font-size: 14pt">
	<img src="_html/urssaf.png"/>
</page>

<!-- DOC SPECIAL CARREFOUR -->
<?php if($CategoriePDV == 2 && in_array($SSCategoriePDV, array(7,2))): ?>
<page style="font-size: 14pt">
	<div style="text-align:left;padding-bottom:10px;">
		<?php if($LogoClient != ''): ?>
              <img  src="data:image/gif;base64,<?php echo $LogoClient ?>" />
        <?php endif; ?>
	</div>	
	
	<p style="font-size:14pt;">
		CARREFOUR<br/>
		<?php echo $LibellePdv; ?><br/>
		<?php echo $AdressePdv ?><br/>
		<?php echo $CodePostal_Ville ?><br/>
		<?php echo 'Fax : '.$FaxPdv ?><br/><br/>
		NANTERRE, Le <?php echo date('d/m/Y')?>
	</p>
	<p style="font-size:14pt;text-align: right">
		A l'Attention de Monsieur le Directeur<br/><br/><br/>
		<!--Copie : Service S&eacute;curit&eacute;<br/><br/><br/>Monsieur,<br/><br/>-->
	</p>
	<p style="font-size:14pt;">
		Monsieur,<br/><br/>
		En accord avec les termes de vos conditions g&eacute;n&eacute;rales d'acc&egrave;s au magasin, 
		nous vous informons que nous avons mandat&eacute; la Soci&eacute;t&eacute; SUPPL'ACTIV afin de
		r&eacute;aliser la prestation d'animation/formation commerciale qui aura lieu le : <?php echo $DateIntervention ?> &agrave; <?php echo $HeureInterventionD ?>,
		sur les produits suivants : <?php echo $GammeProduit ?>.
		<br/><br/>
		Animation/formation pr&eacute;vue le : <?php echo $DateIntervention ?>
		<br/><br/><br/>
	
		Vous souhaitant bonne r&eacute;ception de la pr&eacute;sente.
		<br/><br/><br/>
		Je vous prie d'agr&eacute;er, Monsieur le Directeur, l'expression de mes sentiments distingu&eacute;s.<br/><br/>
		<?php
			switch ($IdClient) {
				case 9:
					echo 'Alexandre Stagnara<br/>';
					echo 'Responsable Force de vente.<br/>';
					break;
				case 22:
					echo 'Yvick MOSNAY DE BOISHERAUD<br/>';
					echo 'Responsable Grands Comptes<br/>';
					echo '01 55 35 17 75<br/>';
					break;
				case 28:
					echo 'Leslie Prudon<br/>';
					echo 'Responsable réseau animatrices formatrices Natescience.<br/>';
					break;
			}
		?>
	</p>
	<?php if($SignatureClient != ''): ?>
              <img  src="data:image/gif;base64,<?php echo $SignatureClient ?>" />
        <?php endif; ?>
</page>
<?php endif; ?>

<!-- DOC SPECIAL AUCHAN -->
<?php if($CategoriePDV == 2 && $SSCategoriePDV == 6): ?>
<page style="font-size: 14pt">
	<div style="text-align:center;padding-bottom:10px;text-align:center;border-bottom:1px double Black;"><img src="_html/logo_Entete.png" /></div>	
	<p style="text-align:center;margin-bottom:70px;font-size:12pt;">
        <?php
        if ($newAdresse) {
            echo '7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon  TEL : 03 45 42 70 23';
        } else {
            echo '24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE  TEL : 03 45 42 70 23';
        }
        ?>
	</p>
	<h3 style="font-size:30pt;text-align:center;margin-bottom:20px;">
		ATTESTATION D'EMPLOI
	</h3>
	<p style="font-size:14pt;">
		Je soussign&eacute; : BERTHAUD Alain (Président)
	</p>
	<p style="font-size:14pt;">
		Soci&eacute;t&eacute; Suppl'ACTIV <br />
        <?php
        if ($newAdresse) {
            echo 'Adresses : 7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon';
        } else {
            echo 'Adresses : 24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE';
        }
        ?>
        <br />
        <?php
        if ($newAdresse) {
            echo 'N&deg; SIRET : 528 147 150 000 23';
        } else {
            echo 'N&deg; SIRET : 528 147 150 000 15';
        }
        ?>
	</p>
	<p style="font-size:14pt;">
		Certifie que le porteur de la pr&eacute;sente : <?php echo $NomIntervenant.' '.$PrenomIntervenant ?> 
		n&eacute;(e) le  <?php echo $DteNaissance ?><br />
		Adresse : <?php echo $AdrInter ?>
	</p>
	<p style="font-size:14pt">
	est inscrit &agrave; l'effectif depuis le <?php echo $DteMin ?>, sous contrat &agrave; dur&eacute;e d&eacute;termin&eacute;e en qualit&eacute;e d'animatrice/formatrice et que conform&eacute;ment aux dispositions de la loi du 31 décembre 1991 relative au travail clandestin, il est fait application des dispositions des articles L 324-9 et suivants concernant le travail dissimul&eacute;, des articles L L 3243-1, L3243-2, L L1221-10, L1221-13, L1221-15  du code du travail relatifs au bulletin de paie et au Registre unique du Personnel, et des articles L 5221-1 et suivants relatifs au travail des &eacute;trangers.
<br /><br />
De plus, ne satisfait pas en fait des conditions pr&eacute;vues par les articles L7311-3, L7313-1, L7313-2, L7313-18 et suivants du livre III du Code du travail et cependant est appel&eacute; &agrave; exercer des op&eacute;rations de ventes en dehors des locaux de l'entreprise ci-dessus d&eacute;sign&eacute;e.
<br /><br />
Dans le cadre de son contrat de travail, il est appel&eacute; &agrave; intervenir dans les hypermarch&eacute;s et supermarch&eacute;s.
<br /><br />
		Attestation &eacute;tablie pour valoir ce que de droit.
<br /><br />
A Dijon, le <?php echo date('d/m/Y') ?>
		
	</p>
	<img width="240" height="142" src="_html/signature.png"/>
</page>
<?php endif; ?>