<?php

ini_set('display_errors', 'on');
$ObjPdv = null; $ObjInterlocuteur = null; $ObjEnseigne = null; $ObjInterlocuteurPdv = null;

$sqlRechercheInfoPdv = "
SELECT *
FROM su_pdv
WHERE idPdv = :IdPdv";
$RechercheInfoPdvExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoPdv);

$sqlRechercheResponsableAgence = "
SELECT nomInterlocuteurAgence, prenomInterlocuteurAgence, telInterlocuteurAgence, libelleTypeMission, logoClient
FROM su_agence_interlocuteur
	INNER JOIN su_campagne ON su_agence_interlocuteur.idInterlocuteurAgence = su_campagne.FK_idInterlocuteurAgence
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
	INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient
	INNER JOIN su_mission ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_mission_type ON su_mission_type.idTypeMission = su_mission.FK_idTypeMission
WHERE idMission = :IdMission";
$RechercheResponsableAgencePdvExc = DbConnexion::getInstance()->prepare($sqlRechercheResponsableAgence);

$sqlRechercheEnseignePdv = "
SELECT libelleSousCategorie
FROM su_pdv_sous_categorie
WHERE FK_idCategorie = :Cat AND idSousCategorie = :SSCat";
$RechercheEnseignePdv = DbConnexion::getInstance()->prepare($sqlRechercheEnseignePdv);

$sqlRechercheInterlocuteurPdv = "
SELECT nomInterlocuteurPdv, prenomInterlocuteurPdv
FROM su_pdv_interlocuteur
WHERE FK_idPdv = :IdPdv";
$RechercheInterlocuteurPdv = DbConnexion::getInstance()->prepare($sqlRechercheInterlocuteurPdv);

$sqlRecherchePA = "
SELECT nomIntervenant, prenomIntervenant, DATE_FORMAT(dateDebut, '%d/%m/%Y') AS DteInter, DATE_FORMAT(dateDebut, '%HH%i') AS HeureD, DATE_FORMAT(dateFin, '%HH%i') AS HeureF
FROM su_intervenant
    INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut > :dateDebut AND FK_IdPdv = :idPdv AND FK_IdMission = :idMission
ORDER BY DteInter";
$RecherchePriseAccordExc = DbConnexion::getInstance()->prepare($sqlRecherchePA);

$RechercheInterlocuteurPdv->bindValue(':IdPdv', $_SESSION['idPdv_17_3'], PDO::PARAM_INT);
$RechercheInterlocuteurPdv->execute();
while($InfoInterlocuteurPdv = $RechercheInterlocuteurPdv->fetch(PDO::FETCH_OBJ)):
    $ObjInterlocuteurPdv = (object)$InfoInterlocuteurPdv;
endwhile;

$RechercheInfoPdvExc->bindValue(':IdPdv', $_SESSION['idPdv_17_3'], PDO::PARAM_INT);
$RechercheInfoPdvExc->execute();
while($InfoPdv = $RechercheInfoPdvExc->fetch(PDO::FETCH_OBJ)):
    $ObjPdv = (object)$InfoPdv;
endwhile;

$RechercheResponsableAgencePdvExc->bindValue(':IdMission', $_SESSION['idMission_17_3'], PDO::PARAM_INT);
$RechercheResponsableAgencePdvExc->execute();
while($InfoInterlocuteur = $RechercheResponsableAgencePdvExc->fetch(PDO::FETCH_OBJ)):
    $ObjInterlocuteur = (object)$InfoInterlocuteur;
endwhile;

$RechercheEnseignePdv->bindValue(':Cat', 	$ObjPdv->FK_idCategorie, 		PDO::PARAM_INT);
$RechercheEnseignePdv->bindValue(':SSCat', 	$ObjPdv->FK_idSousCategorie, 	PDO::PARAM_INT);
$RechercheEnseignePdv->execute();
while($InfoEnseigne = $RechercheEnseignePdv->fetch(PDO::FETCH_OBJ)):
    $ObjEnseigne = (object)$InfoEnseigne;
endwhile;
?>
<page>
    <div style="text-align:center;padding-bottom:10px;text-align:center;border-bottom:1px double Black;"><img src="_html/logo_Entete.png" /></div>
    <p style="text-align:center;margin-bottom:10px;font-size:12pt;">
        24 RUE DE LA REDOUTE  21850 ST APOLLINAIRE  TEL : 03 45 42 70 23
    </p>

    <?php if($ObjInterlocuteur->logoClient != ''): ?>
        <div style="text-align:center">
            <img  src="data:image/gif;base64,<?php echo $ObjInterlocuteur->logoClient ?>" />
        </div>
    <?php endif; ?>
    <h3 style="font-size:24pt;text-align:center;margin-bottom:10px;">
        PRISE D'ACCORD DE RDV - <?php echo strtoupper($ObjInterlocuteur->libelleTypeMission) ?>
    </h3>


    <div style="border:1px solid Black;padding:10px;margin-bottom:10px;">
        <p style="font-weight:bold;font-size:10pt;margin:0px;">Coordonn&eacute;es du point de vente</p>

        <table>
            <tr>
                <td><p>Nom du Point de vente :</p></td>
                <td><p><?php echo $ObjPdv->libellePdv ?></p></td>
            </tr>
        </table>

        <table>
            <tr>
                <td><p>Enseigne :</p></td>
                <td><p><?php echo $ObjEnseigne->libelleSousCategorie ?></p></td>
            </tr>
        </table>

        <table>
            <tr>
                <td><p>Nom du Titulaire :</p></td>
                <td>
                    <p>
                        <?php
                        echo $ObjPdv->nomTitulairePdv_A.' '.$ObjPdv->prenomTitulairePdv_A;
                        if($ObjPdv->nomTitulairePdv_B != ''):
                            echo ' | '.$ObjPdv->nomTitulairePdv_B.' '.$ObjPdv->prenomTitulairePdv_B;
                        endif;
                        ?>
                    </p>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td><p>Personne r&eacute;f&eacute;rente pour l'op&eacute;ration :</p></td>
                <td><p><?php echo (isset($ObjInterlocuteurPdv->nomInterlocuteurPdv)) ? $ObjInterlocuteurPdv->nomInterlocuteurPdv.' '.$ObjInterlocuteurPdv->prenomInterlocuteurPdv : '' ?></p></td>
            </tr>
        </table>

        <table>
            <tr>
                <td><p style="">Adresse :</p></td>
                <td style="width:300px;"><p><?php echo $ObjPdv->adressePdv_A ?></p></td>
                <td><p style="margin-left:20px;">CP / Ville :</p></td>
                <td><p><?php echo $ObjPdv->codePostalPdv.' '.$ObjPdv->villePdv ?></p></td>
            </tr>
            <tr>
                <td><p>T&eacute;l :</p></td>
                <td style="width:300px;"><p><?php echo $ObjPdv->telephoneMagasin ?></p></td>
                <td><p style="margin-left:20px;">Fax :</p></td>
                <td><p><?php echo $ObjPdv->faxPdv ?></p></td>
            </tr>
        </table>
    </div>

    <div style="border:1px solid Black;padding:10px;margin-bottom:10px;height:180px;">
        <p style="font-weight:bold;font-size:10pt;margin:0px;text-align:center;">Date(s) et Horaire(s) d'intervention(s)</p>
        <ul style="margin-top:10px;width:500px;">
            <?php

            $RecherchePriseAccordExc->bindValue(':idPdv', $_SESSION['idPdv_17_3']);
            $RecherchePriseAccordExc->bindValue(':idMission', $_SESSION['idMission_17_3']);
            $RecherchePriseAccordExc->bindValue(':dateDebut', date('Y-m-d').' 00:00:00');
            $RecherchePriseAccordExc->execute();
            while($InfoIntervention = $RecherchePriseAccordExc->fetch(PDO::FETCH_OBJ)):
                ?>
                <li>Le <?php echo $InfoIntervention->DteInter.' de '.$InfoIntervention->HeureD.' &agrave; '.$InfoIntervention->HeureF.' ( '.$InfoIntervention->nomIntervenant.' '.$InfoIntervention->prenomIntervenant.' ) ' ?></li>
            <?php
            endwhile;
            ?>
        </ul>
    </div>

    <div style="border:1px solid Black;padding:10px;height:100px;margin-bottom:10px;">
        <p style="font-weight:bold;font-size:10pt;margin:0px;text-align:center">Tampon du PDV et signature du titulaire</p>

    </div>

    <div style="border:1px solid Black;padding:10px;margin-bottom:10px;">
        <p style="font-weight:bold;font-size:10pt;margin:0px;text-align:center">Prise d'accord &agrave; transmettre par retour<br/>Fax  03 45 42 70 25</p>

    </div>

    <div style="border:1px solid Black;padding:10px;margin-bottom:10px;">
        <p style="font-weight:bold;font-size:10pt;margin:0px;text-align:center">Pour toute demande particuli&egrave;re<br />merci de contacter <?php echo $ObjInterlocuteur->nomInterlocuteurAgence.' '.$ObjInterlocuteur->prenomInterlocuteurAgence.' au '.$ObjInterlocuteur->telInterlocuteurAgence ?></p>

    </div>

</page>