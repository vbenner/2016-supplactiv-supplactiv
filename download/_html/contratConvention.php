<?php


# ------------------------------------------------------------------------------
# Recherche des informations concernant le contrat
# ------------------------------------------------------------------------------
$sqlRechercheInfoIntervenant = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, DATE_FORMAT(dateNaissance, "%d/%m/%Y") AS dateNaissance,
 villeNaissance, numeroSS, adresseIntervenant_A, adresseIntervenant_B, adresseIntervenant_C, 
 codePostalIntervenant, villeIntervenant, nationalite, dureeMensuelle, dateContratCadre, 
 indemniteForfaitaire, remunerationBrute, listePDV, 
FROM su_contrat_cidd
    INNER JOIN su_intervenant ON su_contrat_cidd.FK_idIntervenant = su_intervenant.idIntervenant
WHERE idContrat = :IdContrat
';
$ResultContrat = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);


$sqlRechercheInfoContrat = '
SELECT CONTRAT_CIDD_MISSION.Contenu_Article,Titre_Article,Position_Article,Article_Auto,Id_Article
FROM CONTRAT_CIDD_ARTICLE
	INNER JOIN CONTRAT_CIDD_MISSION ON CONTRAT_CIDD_MISSION.FK_Id_Article = CONTRAT_CIDD_ARTICLE.Id_Article
	INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idMission = CONTRAT_CIDD_MISSION.FK_Id_Mission
WHERE idContrat = :Contrat
ORDER BY Position_Article';
$RechercheInfoContrat = DbConnexion::getInstance()->prepare($sqlRechercheInfoContrat);

$sqlRechercheInfoPdv = "
SELECT libellePdv,codePostalPdv,villePdv, adressePdv_A
FROM su_pdv
WHERE idPdv = :IdPdv";
$RechercheInfoPdv = DbConnexion::getInstance()->prepare($sqlRechercheInfoPdv);
		
$ResultContrat->bindValue(':IdContrat', $_SESSION['IDContrat_Recherche_6_5'], PDO::PARAM_INT);
$ResultContrat->execute();
while($Contrat = $ResultContrat->fetch(PDO::FETCH_OBJ)):
	$NomIntervenant     = $Contrat->nomIntervenant;
	$PrenomIntervenant  = $Contrat->prenomIntervenant;
	$DateNaissance  	= $Contrat->dateNaissance;
	$VilleNaissance 	= $Contrat->villeNaissance;
	$NumeroSS 			= $Contrat->numeroSS;
	
	$AdresseIntervenant = $Contrat->adresseIntervenant_A;
	$AdresseIntervenant.= ($Contrat->adresseIntervenant_B != '') ? ', '.$Contrat->adresseIntervenant_B : '';
	if($Contrat->adresseIntervenant_C != ''):
		$AdresseIntervenant.= ', '.$Contrat->adresseIntervenant_C;
	endif;
	$AdresseIntervenant.= ', '.$Contrat->codePostalIntervenant.' '.$Contrat->villeIntervenant;
	$Nationalite = ($Contrat->nationalite == 'F')? 'Francaise' : 'Etrang&egrave;re';
	
	$DureeMensuelle = $Contrat->dureeMensuelle;

    $dateC = preg_split('/-/', $Contrat->dateContrat);
    $moisS = '';
    foreach($arrayMonthCorres as $id => $Info){
        $idMois = (strlen($id) == 1) ? '0'.$id : $id;
        if($idMois == $dateC[1]){
            $moisS = $Info['libelleMax'];
        }
    }
	$MoisSel = $moisS.' '.$dateC[0];
	
	$IndemniteForfaitaire = $Contrat->indemniteForfaitaire;
	$RemunerationHoraire = $Contrat->remunerationBrute;
	$Lst_PDV = preg_split('/_/',$Contrat->listePDV);
endwhile;


?>
<style type="text/css">
	h4{
		text-align:center;
		text-decoration:underline;
		margin:5px 0px;
		font-size:13px;
	}
	
	p{
		text-align: justify;
		margin: 5px 0px;
	}
</style>
<page style="font-size:11px">
	<div style="padding-bottom:10px;text-align:left"><img src="_html/logo_Entete.png" /></div>
        
	<div style="border:1px solid Black">
        <h3 style="text-align:center;margin:0px;padding:4px 0px;">
            CONTRAT D'INTERVENTION A DUREE DETERMINEE
        </h3>
    </div>

	<p style="text-align:justify;margin-top:10px;">
        <i>Entre les soussign&eacute;s :</i><br/><br/>
        <?php
        if ($Contrat->dateCreationContrat >= '2017-06-19') {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 7-9, Boulevard Rembrandt - Bât Apogée C - 21000 Dijon, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        } else {
            echo 'La SAS SUPPLACTIV, inscrite au RCS de Dijon sous le num&eacute;ro 528 147 150 au capital de 40 000&euro;, dont le si&egrave;ge social est situ&eacute; 24, Rue de la Redoute 21850 St Apollinaire, code APE : 7311Z, dont les cotisations de s&eacute;curit&eacute; sociale sont vers&eacute;es &agrave; l\'URSSAF de la C&ocirc;te d\'Or, agissant par son pr&eacute;sident en exercice,';
        }
        ?>
        <br/><br/>
        <i>D'une part</i><br/>
        <i>Et,</i>
    </p>
    
    <p style="margin-top:10px;">	
        <font style="color:#3B5998"><?php echo $NomIntervenant.' '.$PrenomIntervenant ?></font>, n&eacute;(e) le 
        <font style="color:#3B5998"><?php echo $DateNaissance ?></font> &agrave; 
        <font style="color:#3B5998"><?php echo $VilleNaissance ?></font>, de nationalit&eacute; 
        <font style="color:#3B5998"><?php echo $Nationalite ?></font>,<br/> demeurant au : 
        <font style="color:#3B5998"><?php echo $AdresseIntervenant ?></font>.<br/>
        Num&eacute;ro de s&eacute;curit&eacute; sociale 
        <font style="color:#3B5998"><?php echo $NumeroSS ?></font>, ci apr&egrave;s d&eacute;nomm&eacute; l'intervenant,<br/>
        <i>D'autre part</i><br/>
        <b>Il a &eacute;t&eacute; convenu ce qui suit :</b>
    </p>
    
    <?php	
	$RechercheInfoContrat->bindValue(':Contrat', $_SESSION['IDContrat_Recherche_6_5'], PDO::PARAM_INT);
	$RechercheInfoContrat->execute();
	while($Data = $RechercheInfoContrat->fetch(PDO::FETCH_OBJ)):
			?>
			<h4><?php echo $Data->Titre_Article ?></h4>
			<p>
				<?php 
				if($Data->Article_Auto && $Data->Id_Article != 5)
				{
					switch($Data->Id_Article):
						case 2 :
						?>
						La dur&eacute;e mensuelle de travail de <?php echo $NomIntervenant.' '.$PrenomIntervenant; ?> pour le mois de <?php echo $MoisSel; ?> est fix&eacute;e &agrave; <?php echo $DureeMensuelle; ?> heures. Compte tenu de l'activit&eacute; sp&eacute;cifique de <?php echo $NomIntervenant.' '.$PrenomIntervenant; ?>, il est express&eacute;ment convenu que l'organisation du temps de travail est laiss&eacute; &agrave; la discr&eacute;tion de <?php echo $NomIntervenant.' '.$PrenomIntervenant; ?>
						<?php
						break;
						
						case 3:
						?>
						Ce contrat  est conclu avec une p&eacute;riode d'essai de <?php echo round(($DureeMensuelle/5),0) ?> Heure(s).
						<?php
						break;
						
						case 4:
						?>
						En contrepartie de son activit&eacute; l'intervenant percevra une r&eacute;mun&eacute;ration horaire brute de <?php echo $RemunerationHoraire; ?> &euro; <?php if($IndemniteForfaitaire != 0) { ?>, et une indemnit&eacute; forfaitaire de <?php echo $IndemniteForfaitaire; ?>&euro; par heure travaill&eacute;e pour frais de d&eacute;placement. <?php } ?><br/>
Il pourra &eacute;galement &ecirc;tre demand&eacute; &agrave; l'intervenant d'effectuer des heures compl&eacute;mentaires qui lui seront r&eacute;mun&eacute;r&eacute;es conform&eacute;ment aux dispositions l&eacute;gales et conventionnelles en vigueur.<br/>
Tout autre frais ne pourra &ecirc;tre engag&eacute; pour le compte de la soci&eacute;t&eacute; SUPPL'ACTIV que sur autorisation &eacute;crite de celle-ci et sera rembours&eacute; sur la base de justificatifs.

						<?php
						break;
					endswitch;
				}else {
					echo stripslashes($Data->Contenu_Article) ;
					if($Data->Id_Article == 5):
						?>
						<br/>
						Sur la dur&eacute;e de travail d&eacute;finie &agrave; l'article 2, <?php echo $NomIntervenant.' '.$PrenomIntervenant; ?> s'engage &agrave; visiter les points de vente suivants :<br/><br/>
						<ul style="margin-left:20px">
						<?php
						foreach($Lst_PDV as $PDV):
							if($PDV != ''):
								$RechercheInfoPdv->bindValue(':IdPdv', $PDV, PDO::PARAM_INT);
								$RechercheInfoPdv->execute();
								while($DataPDV = $RechercheInfoPdv->fetch(PDO::FETCH_OBJ)):
									echo '<li>'.$DataPDV->libellePdv.' - '.$DataPDV->adressePdv_A.' '.$DataPDV->codePostalPdv.' '.$DataPDV->villePdv.'</li>';
								endwhile;
							endif;
						endforeach;						
						?>
						</ul>
						<br/><br/>Une visite ne sera consid&eacute;r&eacute;e comme effectu&eacute;e qu'apr&egrave;s l'envoi &agrave; la soci&eacute;t&eacute; SUPPL'ACTIV, dans les 8 jours de l'intervention, du rapport de visite vis&eacute; par le responsable du point de vente.

						<?php
					endif;
				}
				?>
			</p>
		<?php
	endwhile;
	?>
	<p>
    	Fait &agrave; Dijon<br />
		Le <?php echo date('d/m/Y') ?><br />
		en deux exemplaires.<br />
		Signatures pr&eacute;c&eacute;d&eacute;es de la mention manuscrite "Lu et approuv&eacute;"
	</p>
    <br /><br />
    <p style="width:50%;float:left;">
    	L'intervenant :
    </p>
    
    <p style="text-align:right;width:40%">	
    	Pour la soci&eacute;t&eacute; Suppl'ACTIV :<br />
    	<img src="_html/signature.png"/>
	</p>

</page>