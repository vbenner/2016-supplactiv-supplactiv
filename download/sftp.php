<?php

/** -----------------------------------------------------------------------------------------------
 * CRON Quotidien
 *
 * Export des fichiers de référence vers le serveur EXPENSYA pour la fonctionnalité des notes
 * de frais
 *
 * - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - =
 * @version : 1.0.0
 * @dev : Vincent BENNER © PAGE UP 2017
 * @date : 2017-08-09
 * @history :
 *
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once __DIR__ . '/../current/_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Paramètres d'accès
 */
$serveur = 'sftp.expensya.com';
$user='supplactivprod';
$pass='@Uy!s52par?d66d';

/** -----------------------------------------------------------------------------------------------
 * Nouveaux paramètres
 */
$serveur = 'sftp.expensya.com';
$user='supplactivprod';
$pass='@Uy!s52par?d66d';

/** -----------------------------------------------------------------------------------------------
 * Vérification de base
 */
if (!function_exists("ssh2_connect")) die("La bibliotheque ssh2 n'est pas disponible sur le serveur");

/** -----------------------------------------------------------------------------------------------
 * Fonction DEBUG
 *
 * @param $msg
 * @param string $var
 */
function debug($msg, $var = 'debug')
{
    if (isset($_GET[$var]))
    {
        echo  trim($msg), "\n";
        ob_flush();
        flush();
    }
}

/** -----------------------------------------------------------------------------------------------
 * @param $type
 * @param $table
 * @param $fields
 * @param $sort
 * @param $file
 */
function generateExport($type, $table, $fields, $inners, $wheres, $groupbys, $sort, $file)
{

    /** -------------------------------------------------------------------------------------------
     * On crée l'entête de la requête
     */
    $select = '';
    $header = '';
    foreach ($fields as $key => $field) {
        $header .= $field[2].';';
        if ($field[1] == 1) {
            /** -----------------------------------------------------------------------------------
             * Attention, on va rajouter une fonction supplémentaire... Format Date
             */
            $select .= ($field[4] != '' ? ' DATE_FORMAT('.$field[0].', "'.$field[4].'")'.' AS '.$field[2].', ' : $field[0].' AS '.$field[2].', ');
        } else {
            $select .= '\''.$field[3].'\' AS '.$field[2].', ';
        }
    }
    $select = substr($select, 0, strlen($select)-2);

    $inner = '';
    foreach ($inners as $key => $value) {
        $inner .= ($value[3] != '' ? $value[3] : ' INNER ').' JOIN '.$value[0].' ON '.$value[1].'='.$value[2].' ';
    }

    /** -------------------------------------------------------------------------------------------
     * [0] -> condition
     * [1] -> signe
     * [2] -> value
     */
    $where = '';
    $hasWhere = false;
    foreach ($wheres as $key => $value) {
        $hasWhere = true;
        $where .= ' '.$value[0].$value[1].$value[2].' AND ';
    }
    if ($hasWhere) {
        $where = substr($where, 0, strlen($where) - 5);
        $where = ' WHERE ('.$where.') ';
    }

    $groupby = '';
    foreach ($groupbys as $key => $value) {
        $groupby .= ' GROUP BY '.$value[0].'.'.$value[1].' ';
    }

    $sqlGetReferentiel = '
    SELECT '.$select.'
    FROM '.$table.' '.
    $inner.
    $where.
    $groupby.
    '
    ORDER BY '.$sort.'
    ';

    $getReferentielExec = DbConnexion::getInstance()->prepare($sqlGetReferentiel);

    /** -------------------------------------------------------------------------------------------
     * Sauvegarde ENTETE
     * On rajoute Manuellement PayId2 et PayId3
     */
    $header .= 'PayId2;PayId3;';
    file_put_contents($file, $header."\r\n");

    /** -------------------------------------------------------------------------------------------
     * Sauvegarde DATA
     */
    $getReferentielExec->execute();
    while($getReferentiel = $getReferentielExec->fetch(PDO::FETCH_OBJ)) {

        $str = '';
        foreach ($getReferentiel as $key => $val) {

            /** -----------------------------------------------------------------------------------
             * Quelques réajustements
             */
            if ($val == 'OUI') {
                $val = 1;
            }
            if ($val == 'NON') {
                $val = 0;
            }
            if ($val == '') {
                /** -------------------------------------------------------------------------------
                 * USER
                 */
                if ($key == 'ManagerMail' && ($type == 'U')) {
                    $val = 'dlaurent@supplactiv.fr';
                }
                /** -------------------------------------------------------------------------------
                 * Au niveau du PROJET, on va récupérer le mail du CONTRAT
                 */
                if ($key == 'ManagerMail' && ($type == 'P')) {

                    /** ---------------------------------------------------------------------------
                     * On récupère la clé de la mission pour en déduire l'interloculeur du contrat
                     */
                    $idMission = $getReferentiel->ProjectExternalId;
                    $sqlGetAgent = '
                    SELECT mailInterlocuteurAgence
                    FROM su_agence_interlocuteur SA
	                    INNER JOIN su_campagne SC ON SC.FK_idInterlocuteurAgence = SA.idInterlocuteurAgence
	                    INNER JOIN su_mission SM ON SM.FK_idCampagne = SC.idCampagne 
                    WHERE SM.idMission = '.$idMission.'
                    ';

                    #die($idMission.' - '.$sqlGetAgent);

                    $getAgentExec = DbConnexion::getInstance()->prepare($sqlGetAgent);
                    $getAgentExec->execute();
                    #print_r($getAgentExec);
                    #die('Count = '.$getAgentExec->rowCount() );
                    if ($getAgentExec->rowCount() > 0) {
                        $getAgent = $getAgentExec->fetch();
                        $val = $getAgent['mailInterlocuteurAgence'];
                    }
                    else {
                        $val = 'dlaurent@supplactiv.fr';
                    }
                    //die('ID -> '.;
                }
            }
            $str .= str_replace(array("\r\n", "\n\r", "\n", "\r", "\t", "<br/>"), ',', $val) . ';';
        }
        file_put_contents($file, $str."\r\n", FILE_APPEND);
    }

    /** -------------------------------------------------------------------------------------------
     * Pour les utilisateurs, on fait une jointure sur du_utilisateur
     */
    if ($type == 'U') {
        /** ---------------------------------------------------------------------------------------
         * LastName;FirstName;Language;CountryCode;PayId;Mail;ManagerMail;IsAdmin;IsValid;Contract;
         * ABICHOU;DONIYA;FR;FR;FR;delta91170@hotmail.fr;dlaurent@supplactiv.fr;0;1;0;
         */
        $sqlUnionUtiilisateurs = '
        SELECT *
        FROM du_utilisateur
        WHERE idUtilisateur <> 1
        AND idUtilisateur <> 1170
        AND boolActif = 1
        ORDER BY nomUtilisateur
        ';

        $unionUtilisateursExec = DbConnexion::getInstance()->prepare($sqlUnionUtiilisateurs);
        $unionUtilisateursExec->execute();
        while ($unionUtilisateurs = $unionUtilisateursExec->fetch(PDO::FETCH_OBJ)) {

            $str = $unionUtilisateurs->nomUtilisateur.';'.$unionUtilisateurs->prenomUtilisateur.';';
            $str .= 'FR'.';'.'FR'.';'.$unionUtilisateurs->immatUtilisateur.';';
            $str .= $unionUtilisateurs->mailUtilisateur.';';
            $str .= $unionUtilisateurs->mailManager.';';
            $str .= ($unionUtilisateurs->idUtilisateur == 1145 ? '1' : '0').';'.$unionUtilisateurs->boolActif.';'.'0'.';';
            $str .= 'FR'.';';
            $str .= $unionUtilisateurs->villeIntervenant.';';
            $str .= $unionUtilisateurs->codePostalIntervenant.';';
            $str .= $unionUtilisateurs->adresseIntervenant_A    .';';
            $str .= $unionUtilisateurs->adresseIntervenant_B    .';';
            $str .= $unionUtilisateurs->typeIntervenantExpensia    .';';
            $str .= $unionUtilisateurs->contratIntervenant    .';';
            file_put_contents($file, $str."\r\n", FILE_APPEND);
        }
    }
}


/** -----------------------------------------------------------------------------------------------
 * Class sftp
 */
class sftp
{
    private $id = false, $server = false, $url = false;

    /** @brief Constructeur
     *
     * Tente d'etablir une connexion SFTP à un serveur distant.
     * @param	$server : ip/host du server
     * @param 	$user : Identifiant de la connexion
     * @param	$pass: Mot de passe
     * @throw : Une exception remontera en cas d'erreur
     */
    public function __construct ($server, $user, $pass)
    {
        $this->id = ssh2_connect($server, 22);
        if(!$this->id || empty($this->id))
            throw new Exception ('Echec de connexion - ' . $server . '<br/>');
        if (!ssh2_auth_password($this->id, $user, $pass))
            throw new Exception ("Echec d'identification - " . $server . '<br/>');
        $this->url = ssh2_sftp($this->id);
        if (!$this->url)
            throw new Exception ("Echec d'ouverture du SFTP sur le serveur " . $server . '<br/>');
        $this->server = $server;
    }

    /** @brief Destructeur
     *
     * Ferme une connection sftp
     */
    public function __destruct()
    {
        debug("Fermeture de la connexion SSH" . '<br/>');
        ssh2_exec($this->id, "exit");
    }

    /** @brief Retourne la taille d'un fichier sur le serveur distant
     * @param	$file
     *	chemin absolu du fichier
     * @return
     *	taille du fichier en octet
     */
    private function getfilesize($file)
    {
        return filesize("ssh2.sftp://" . $this->url . $file);
    }

    /** @brief Recupere un fichier sur le serveur distant
     *
     * Tente de recuperer un fichier sur le serveur distant,
     * user friendly , $name  n'a pas besoin d'ÃƒÂªtre le chemin absolu vers le fichier
     * par exemple get("foo/zomg/lol.csv") va recuperer le fichier /../foo/zomg/lol.csv sur le serveur
     * dans le fichier lol.csv du dossier courant local
     * @param	$name
     *	nom du fichier ÃƒÂ  rÃƒÂ©cuperer sur le serveur
     * @param 	$local_name
     *	facultatif, nom du fichier en local aprÃƒÂ¨s transfert
     * @return
     *	Renvois le chemin du fichier recupÃƒÂ©rÃƒÂ© en local
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function get($name, $local_name = false)
    {
        try {$name = $this->fpath($name);}
        catch (Exception $e) {throw $e;}
        if (!$local_name)
            $local_name = '.' . (strpos(name, '/') === false?strrchr($name, '/'):('/' . $name));
        debug("Tentative de transfert du fichier '" .$local_name ."' depuis le serveur '" .$name . "'");
        $handle = @fopen("ssh2.sftp://" . $this->url . $name, 'r');
        if (!$handle)
            throw new Exception ("Impossible d'ouvrir le fichier " . $name . " sur le serveur distant " . $this->server);
        $size = $this->getfilesize($name);
        $contents = '';
        $transfered = 0;$i = 0;
        while ($transfered < $size && ($buffer = fread($handle, $size - $transfered)))
        {
            debug($transfered, " transfere sur ", $size);
            $transfered += strlen($buffer);
            $contents .= $buffer;
            if ($i++ > 1000)
            {
                fclose($handle);
                throw new Exception ('Erreur pendant le transfert du fichier ' . $name . 'depuis le serveur' . $this->server . ' : timeout');
            }
        }
        file_put_contents ($local_name, $contents);
        fclose($handle);
        debug("\nTransfert du fichier '" . $name . "' depuis le serveur " . $this->server . " reussi");
        return ($local_name);
    }

    /** @brief Dépose un fichier sur le serveur distant
     *
     * Tente de déposer un fichier sur le serveur distant,
     * user friendly , $name n'a pas besoin d'être le chemin absolu vers le fichier
     * par exemple put("foo/zomg/lol.csv") va deposer le fichier lol.csv du dossier
     * courant dans le dossier /../foo/zomg/lol.csv du serveur
     * @param	$name
     *	nom du fichier à déposer sur le serveur
     * @param 	$local_name
     *	facultatif, nom du fichier
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function put($name, $remote_name = false)
    {
        try
        {
            if (!$remote_name)
            {
                $remote_name = $this->fpath($name);
                $name = '.' . (strpos($name, '/') === false ?('/' . $name):strrchr($name, '/'));
            }
            else
                $remote_name = $this->path(substr($remote_name, 0 , strrpos($remote_name, '/'))) . strrchr($remote_name, '/');
        }
        catch (Exception $e) {throw $e;}
        debug("Tentative de transfert du fichier '" . $name . "' sur le serveur " . $this->server);
        $handle = fopen("ssh2.sftp://" . $this->url . $remote_name, 'w');
        if (!$handle)
            throw new Exception('Impossible d\'ouvrir le fichier ' . $remote_name . ' en ecriture sur le serveur ' . $this->server);
        $data = file_get_contents(strpos($name, "/")?'.' . strrchr($name, "/"):$name);
        if ($data === false)
            throw new Exception("Impossible d'ouvrir le fichier local " . $name);
        $transfered = fwrite($handle, $data);
        if ($transfered === false)
            throw new Exception("Impossible de transferer le fichier " . $name);
        $size = strlen($data);
        if ($transfered != $size)
        {
            while ($transfered < $size && ($transfered += fwrite($handle, mb_substr($data, $transfered, $size - $transfered))))
                debug($transfered . " transfere sur " . $size);
        }
        debug("Fichier " . $remote_name . " correctement depose");
        fclose($handle);
    }


    /** @brief Deplace un fichier sur le serveur distant
     *
     * Tente de deplacer un fichier sur le serveur distant
     * user friendly , les chemins n'ont pas besoin d'ÃƒÂªtre absolu
     * par exemple mv("foo/zomg/lol.csv", "omfg/lolz.txt") va deplacer /../foo/zomg/lol.csv vers /../omfg/lolz.txt
     * @param	$old
     *	nom du fichier ÃƒÂ  dÃƒÂ©placer
     * @param 	$new
     *	nouveau nom & chemin
     * @throw
     *	Une exception remontera en cas d'erreur
     */
    public function mv($old, $new)
    {
        try
        {
            $old = $this->fpath($old);
            $new = $this->fpath($new);
        }
        catch (Exception $e)
        {
            throw $e;
        }
        debug('Tentative de deplacement du fichier \'' . $old . '\' vers \'' . $new . "'");
        $old_handle = fopen("ssh2.sftp://" . $this->url .$old, 'r');
        if (!$old_handle)
            throw new Exception ("Impossible d'ouvrir en lecture le fichier  '" . $old . "' sur le serveur distant");
        $new_handle = fopen("ssh2.sftp://" . $this->url .$new, 'w');
        if (!$new_handle)
            throw new Exception ("Impossible d'ouvrir en ecriture le fichier  '" . $new . "' sur le serveur distant");
        $size = $this->getfilesize($old);
        $read = 0;
        $wrote = 0;
        while ($read < $size && ($buffer = fread($old_handle, $size - $read)))
        {
            $tmp_size = strlen($buffer);
            $read += $tmp_size;
            $tmp_wrote = 0;
            while ($tmp_wrote <$tmp_size)
            {
                $tmp = fwrite($new_handle, substr($buffer, $tmp_wrote));
                if (!$tmp)
                    break;
                $tmp_wrote += $tmp;
            }
            $wrote += $tmp_size;
        }
        debug("taille du fichier = " . $size . " , " . $read . " octets lu, " . $wrote . " octets ecrit");
        fclose($new_handle);
        fclose($old_handle);
        if ($size == $read && $read == $wrote)
            unlink("ssh2.sftp://" . $this->url . $old);
        else
            throw new Exception ("Erreur pendant le deplacement du fichier " . $old . " en " . $old . "\ntaille du fichier = " . $size . " , " . $read . " octets lu, " . $wrote . " octets ecrit\n");
    }

    /** @brief Récupère le chemin absolu d'un dossier
     *
     * @param	$dir
     *	nom du dossier
     * @return
     *	chemin absolu du dossier $dir sur le serveur distant
     */
    private function path($dir)
    {
        $rlpath = @ssh2_sftp_realpath($this->url, $dir);
        if (!$rlpath)
            throw new Exception ("Dossier introuvable sur le serveur distant : "  . $dir);
        return $rlpath;
    }

    /** @brief RÃƒÂ©cupere le chemin absolu d'un fichier sur le serveur
     *
     * @param	$name
     *	nom du fichier
     * @return
     *	chemin absolu du fichier sur le serveur
     */
    private function fpath($name)
    {
        try
        {
            if (strpos($name, '/') === false)
                $name = $this->path('.') . '/' . $name;
            else
                $name = $this->path(substr($name, 0 , strrpos($name, '/'))) . strrchr($name, '/');
        }
        catch (Exception $e)
        {
            throw $e;
        }
        return $name;
    }

    /** @brief Liste le contenu d'un dossier
     *
     * Liste le contenu d'un dossier passÃƒÂ© en parametre ou le dossier courant si aucun dossiÃƒÂ© donnÃƒÂ©.
     * Ne renvois pas les dossiers
     * Offre la possibilitÃƒÂ©e de filtrer les resultats possedant l'extension "$ext" et la chaÃƒÂ®ne de caractere $like dans leur noms
     * @param	$directory
     *	nom du dossier
     * @param	$like
     *	pattern ÃƒÂ  respeceter
     * @param	$ext
     *	extension des fichiers ÃƒÂ  respecter
     * @return
     *	renvois la liste des fichiers sous la forme d'un tableau non associatif
     * @throw
     *	une exception remontera en cas d'erreur
     */
    public function flist($directory = './' , $like = false, $ext = false)
    {
        $dir = $this->path($directory);
        $handle = opendir("ssh2.sftp://" . $this->url . $dir);
        if (!$handle)
            throw new Exception ("Impossible d'ouvrir le dossier " .  $directory . " sur le serveur " . $this->server);
        $files = array();
        while (false !== ($file = readdir($handle)))
        {
            if (substr($file, 0, 1) != "." && !is_dir($file))
            {
                if ($like && $ext)
                {
                    if (stripos($file, $like) !== false && strrchr($file , ".") == (strpos($ext, ".") !== false?$ext:(".".$ext)))
                        $files[] = $dir . '/' . $file;
                }
                else
                    $files[] =  $dir . '/' . $file;
            }
        }
        sort($files);
        return $files;
    }

    /** @brief Recupere une liste de fichier
     *
     * Recupere une liste de fichier et les deplace (sur le serveur distant) ds le dossier $dir_if_success si spÃƒÂ©cifiÃƒÂ©
     * @param	$files_ar
     *	tableau de fichiers
     * @param	$like
     *	pattern ÃƒÂ  respeceter
     * @param	$ext
     *	extension des fichiers ÃƒÂ  respecter
     * @return
     *	renvois la liste des fichiers sous la forme d'un tableau non associatif
     * @throw
     *	une exception remontera en cas d'erreur
     */
    public function get_files($files_ar, $dir_if_success = false)
    {
        $error = count($files_ar);
        $files = array();
        foreach ($files_ar as $k =>$v)
        {
            debug("Transfert de $v.");
            try
            {
                $files[$k] = $this->get($v);
                if ($dir_if_success)
                    $this->mv($v, $dir_if_success . strrchr($files[$k], "/"));
                debug("Transfert reussi");
            }
            catch (Exception $e)
            {
                debug("Echec dans le transfer du fichier $v :\n" . $e->getMessage());;
                $error--;
            }
        }
        if (!$error)
            throw new Exception (
                "Impossible de récupérer les fichiers spécifiés, vérifiez les droits, 
                le type de transfert ou type de connexion.");
        if (empty($files))
            return false;
        sort($files);
        return $files;
    }
}

/** -----------------------------------------------------------------------------------------------
 * Création du FICHIER d'export INTERVENANT
 */
$filenameU = 'users_' . date('Ymd_His') . '.csv';
$tab=array(
    0=> array('DISTINCT nomIntervenant', 1, 'LastName'),
    1=> array('prenomIntervenant', 1, 'FirstName'),
    2=> array('Language', 0, 'Language', 'FR'),
    3=> array('CountryCode', 0, 'CountryCode', 'FR'),
    4=> array('idIntervenant', 1, 'idIntervenant'),
    5=> array('mailIntervenant', 1, 'Mail'),
    6=> array('mailUtilisateur', 1, 'ManagerMail'),
    7=> array('IsAdmin', 0, 'IsAdmin', '0'),
    8=> array('su_intervenant.boolActif', 1, 'IsValid'),
    9=> array('boolAutoEntrepreneuse', 1, 'Contract'),

    10=> array('Country', 0, 'Country', 'FR'),
    11=> array('su_intervenant.villeIntervenant', 1, 'City'),
    12=> array('su_intervenant.codePostalIntervenant', 1, 'ZipCode'),
    13=> array('su_intervenant.adresseIntervenant_A', 1, 'Address'),
    14=> array('su_intervenant.adresseIntervenant_B', 1, 'Address2'),
);
$inner = array(
    0 => array ('su_intervention', 'idIntervenant', 'su_intervention.FK_idIntervenant'),
    1 => array ('su_mission', 'idMission', 'su_intervention.FK_idMission'),
    2 => array ('du_utilisateur', 'idUtilisateur', 'su_mission.decideur_id', 'LEFT'),
);
$where  = array (
    0 => array('su_intervenant.idIntervenant', '<>', '1')
);
$groupby = array(
    0 => array ('su_intervenant', 'idIntervenant'),
);

generateExport('U', 'su_intervenant', $tab, $inner, $where, $groupby,'nomIntervenant', $filenameU);

/** -----------------------------------------------------------------------------------------------
 * Création du FICHIER d'export PROJETS
 */
$filenameP = 'projects_' . date('Ymd_His') . '.csv';
$tab=array(
    0=> array('DISTINCT idMission', 1, 'ProjectExternalId'),
    1=> array('ProjectExternalId', 0, 'ProjectReference', ''),
    2=> array('libelleMission', 1, 'ProjectName'),
    3=> array('libelleClient', 1, 'ClientName', 'FR'),
    4=> array('su_campagne.dateDebut', 1, 'ProjectStartDate', '', '%Y-%m-%d'),
    5=> array('su_campagne.dateFin', 1, 'ProjectEndDate', '', '%Y-%m-%d'),
    6=> array('mailUtilisateur', 1, 'ManagerMail'),
    7=> array('ValidatorPayId', 0, 'ValidatorPayId', ''),
    8=> array('ProjectHasBillable', 0, 'ProjectHasBillable', '1'),
    9=> array('ProjectIsActive', 0, 'ProjectIsActive', '1'),

    );
$inner  = array(
    0 => array ('su_campagne', 'idCampagne', 'su_mission.FK_idCampagne'),
    1 => array ('du_utilisateur', 'idUtilisateur', 'su_mission.decideur_id', 'LEFT'),
    2 => array ('su_client_interlocuteur', 'idInterlocuteurClient', 'su_campagne.FK_idInterlocuteurClient'),
    3 => array ('su_client', 'idClient', 'su_client_interlocuteur.FK_idClient'),
    4 => array ('su_intervention', 'idMission', 'su_intervention.FK_idMission'),
);
$where  = array(
    0 => array(' YEAR(su_campagne.dateDebut) ', ' = ', date('Y')),
);
$groupby = array();
generateExport('P', 'su_mission', $tab, $inner, $where, $groupby,'idMission', $filenameP);

/** -----------------------------------------------------------------------------------------------
 * Création du FICHIER d'export USER PROJETS
 */
$filenameUP = 'userProjects_' . date('Ymd_His') . '.csv';
$tab=array(
    0=> array('idIntervention', 1, 'UserProjectExternalId'),
    1=> array('FK_idMission', 1, 'ProjectExternalId'),
    2=> array('tarifKM', 1, 'IK'),
    3=> array('mailIntervenant', 1, 'UserMail'),
    4=> array('dateDebut', 1, 'StartDate', '', '%Y-%m-%d'),
    5=> array('dateFin', 1, 'EndDate', '', '%Y-%m-%d'),

    6=> array('Country', 0, 'Country', 'FR'),
    7=> array('villePdv', 1, 'City'),
    8=> array('codePostalPdv', 1, 'ZipCode'),
    9=> array('adressePdv_A', 1, 'Address'),
    10=> array('adressePdv_B', 1, 'Address2'),

);
$inner  = array(
    0 => array ('su_contrat', 'FK_idContrat', 'su_contrat.idContrat'),
    1 => array ('su_intervenant', 'FK_idIntervenant', 'su_intervenant.idIntervenant'),
    2 => array ('su_pdv', 'idPdv', 'su_intervention.FK_idPdv'),
);
$where  = array(
    0 => array(' YEAR(su_intervention.dateDebut) ', ' = ', date('Y')),
);
$groupby = array();
generateExport('UP', 'su_intervention', $tab, $inner, $where, $groupby,'idIntervention', $filenameUP);

/** -----------------------------------------------------------------------------------------------
 * TRANSFERT
 */
try
{
    #die('Blocage temporaire');
    $sftp = new sftp($serveur, $user, $pass);
    $sftp->put($filenameU, "import/".$filenameU);
    $sftp->put($filenameP, "import/".$filenameP);
    $sftp->put($filenameUP, "import/".$filenameUP);
    unlink($filenameU);
    unlink($filenameP);
    unlink($filenameUP);
    $sftp = null;

    /** -------------------------------------------------------------------------------------------
     * En fin de traitement, on met à jour à la date
     */
    $sqlStoreAction = '
    INSERT INTO du_params (skey, svalue)
    VALUES (:key, :value)
    ON DUPLICATE KEY UPDATE
        svalue = :value
    ';
    $storeActionExec = DbConnexion::getInstance()->prepare($sqlStoreAction);
    $storeActionExec->bindValue(':key', 'expensya_export', PDO::PARAM_STR);
    $storeActionExec->bindValue(':value', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $storeActionExec->execute();

}
catch (Exception $e)
{
    die ($e->getMessage()."\n");
}
die('Fin du traitement');
