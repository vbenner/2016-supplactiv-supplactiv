<?php
ini_set('display_errors','on');
/**
 * Export du RECAPITULATIF CAMPAGNE
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

# ------------------------------------------------------------------------------
# On recupere la campagne
# ------------------------------------------------------------------------------
$CampagneSel = $_SESSION['idCampagne_17_2'];

# ------------------------------------------------------------------------------
# Correspondance des colonnes en fonction du mois
# ------------------------------------------------------------------------------
$LstCorrespondanceColonneMois = array(
    '01'=>'F',
    '02'=>'G',
    '03'=>'H',
    '04'=>'I',
    '05'=>'J',
    '06'=>'K',
    '07'=>'L',
    '08'=>'M',
    '09'=>'N',
    '10'=>'O',
    '11'=>'P',
    '12'=>'Q'
);


# ------------------------------------------------------------------------------
# Je cr�e mon fichier Excel
# ------------------------------------------------------------------------------
$objPHPExcel = new Spreadsheet();

$PlaningGlobal = $objPHPExcel->getActiveSheet()->setTitle('RECAPITULATIF DES CAMPAGNES');


$PlaningGlobal->setCellValue('A4', 'Libelle Client');
$PlaningGlobal->setCellValue('B4', 'Libelle Campagne');
$PlaningGlobal->setCellValue('C4', 'Libelle Mission');
$PlaningGlobal->setCellValue('D4', 'Libelle Intervenant');
$PlaningGlobal->setCellValue('E4', 'Libelle PDV');

$PlaningGlobal->setCellValue('F4', 'Jan');
$PlaningGlobal->setCellValue('G4', 'Fev');
$PlaningGlobal->setCellValue('H4', 'Mar');
$PlaningGlobal->setCellValue('I4', 'Avr');
$PlaningGlobal->setCellValue('J4', 'Mai');
$PlaningGlobal->setCellValue('K4', 'Jui');
$PlaningGlobal->setCellValue('L4', 'Jul');
$PlaningGlobal->setCellValue('M4', 'Aou');
$PlaningGlobal->setCellValue('N4', 'Sep');
$PlaningGlobal->setCellValue('O4', 'Oct');
$PlaningGlobal->setCellValue('P4', 'Nov');
$PlaningGlobal->setCellValue('Q4', 'Dec');

$PlaningGlobal->getStyle('A4:Q4')->getFont()->setSize(10);
$PlaningGlobal->getStyle('A4:Q4')->getFont()->setBold(true);
$PlaningGlobal->getStyle('A4:Q4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
$PlaningGlobal->getStyle('A4:Q4')->getFill()->setFillType(Fill::FILL_SOLID);
$PlaningGlobal->getStyle('A4:Q4')->getFill()->getStartColor()->setARGB('FFD800');

$PlaningGlobal->getColumnDimension('A')->setAutoSize(true);
$PlaningGlobal->getColumnDimension('B')->setAutoSize(true);
$PlaningGlobal->getColumnDimension('C')->setAutoSize(true);
$PlaningGlobal->getColumnDimension('D')->setAutoSize(true);
$PlaningGlobal->getColumnDimension('E')->setAutoSize(true);

$PlaningGlobal->getColumnDimension('F')->setWidth(5);
$PlaningGlobal->getColumnDimension('G')->setWidth(5);
$PlaningGlobal->getColumnDimension('H')->setWidth(5);
$PlaningGlobal->getColumnDimension('I')->setWidth(5);
$PlaningGlobal->getColumnDimension('J')->setWidth(5);
$PlaningGlobal->getColumnDimension('K')->setWidth(5);
$PlaningGlobal->getColumnDimension('L')->setWidth(5);
$PlaningGlobal->getColumnDimension('M')->setWidth(5);
$PlaningGlobal->getColumnDimension('N')->setWidth(5);
$PlaningGlobal->getColumnDimension('O')->setWidth(5);
$PlaningGlobal->getColumnDimension('P')->setWidth(5);
$PlaningGlobal->getColumnDimension('Q')->setWidth(5);


//Bords noir
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ),
    ),
);

$PlaningGlobal->getStyle('A4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('B4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('C4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('D4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('E4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('F4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('G4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('H4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('I4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('J4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('K4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('L4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('M4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('N4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('O4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('P4')->applyFromArray($styleThickBrownBorderOutline);
$PlaningGlobal->getStyle('Q4')->applyFromArray($styleThickBrownBorderOutline);


$NumeroLigne = 4;


$sqlRechercheIntervention = "
SELECT idIntervenant, nomIntervenant, prenomIntervenant, idPdv, libellePdv, idCampagne, libelleCampagne, idMission, libelleMission, libelleClient, idClient
FROM su_intervenant
	INNER JOIN su_intervention ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
	INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient";

if($CampagneSel != 'ALL') {
    $sqlRechercheIntervention .= " WHERE idCampagne = $CampagneSel ";
}

$sqlRechercheIntervention .= "
GROUP BY idIntervenant, idPdv, idCampagne, idMission, idClient
ORDER BY libelleCampagne, libelleMission, libelleClient, nomIntervenant";
$RechercheInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervention);

$sqlRechercheDteIntervention = "
SELECT DATE_FORMAT(su_intervention.dateDebut, '%Y-%m-%d') AS Dte
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
WHERE FK_idPdv = :IdPdv AND FK_idIntervenant = :IdIntervenant AND FK_idCampagne = :IdCampagne AND FK_idClient = :IdClient AND idMission = :IdMission";
$RechercheDteInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheDteIntervention);

$RechercheInterventionExc->execute();
while($InfoIntervention = $RechercheInterventionExc->fetch(PDO::FETCH_OBJ)) {
    $NbInterMois = array();
    $RechercheDteInterventionExc->bindValue(':IdPdv', $InfoIntervention->idPdv, PDO::PARAM_INT);
    $RechercheDteInterventionExc->bindValue(':IdIntervenant', $InfoIntervention->idIntervenant, PDO::PARAM_INT);
    $RechercheDteInterventionExc->bindValue(':IdCampagne', $InfoIntervention->idCampagne, PDO::PARAM_INT);
    $RechercheDteInterventionExc->bindValue(':IdClient', $InfoIntervention->idClient, PDO::PARAM_INT);
    $RechercheDteInterventionExc->bindValue(':IdMission', $InfoIntervention->idMission, PDO::PARAM_INT);
    $RechercheDteInterventionExc->execute();
    while ($InfoDte = $RechercheDteInterventionExc->fetch(PDO::FETCH_OBJ)) {
        if ($InfoDte->Dte != '') {
            $D = explode('-', $InfoDte->Dte);
            //$D = preg_split('/-/', $InfoDte->Dte);
            @$NbInterMois[$D[1]]++;
        }
    }

    if ($RechercheDteInterventionExc->rowCount() > 0) {
        $NumeroLigne++;
        $PlaningGlobal->setCellValue('A' . $NumeroLigne, $InfoIntervention->libelleClient);
        $PlaningGlobal->setCellValue('B' . $NumeroLigne, $InfoIntervention->libelleCampagne);
        $PlaningGlobal->setCellValue('C' . $NumeroLigne, $InfoIntervention->libelleMission);
        $PlaningGlobal->setCellValue('D' . $NumeroLigne, $InfoIntervention->nomIntervenant . ' ' . $InfoIntervention->prenomIntervenant);
        $PlaningGlobal->setCellValue('E' . $NumeroLigne, $InfoIntervention->libellePdv);


        # ------------------------------------------------------------------------------
        # Style de la ligne
        # ------------------------------------------------------------------------------
        $PlaningGlobal->getStyle('A' . $NumeroLigne . ':Q' . $NumeroLigne)->getFont()->setSize(10);
        $PlaningGlobal->getStyle('F' . $NumeroLigne . ':Q' . $NumeroLigne)->getFill()->setFillType(Fill::FILL_SOLID);
        $PlaningGlobal->getStyle('F' . $NumeroLigne . ':Q' . $NumeroLigne)->getFill()->getStartColor()->setARGB('DEDEDE');

      /*  $PlaningGlobal->getStyle('A' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('B' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('C' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('D' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('E' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('F' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);

        $PlaningGlobal->getStyle('G' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('H' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('I' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('J' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('K' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('L' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('M' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('N' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('O' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('P' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        $PlaningGlobal->getStyle('Q' . $NumeroLigne)->applyFromArray($styleThickBrownBorderOutline); */

        foreach ($NbInterMois as $IDM => $NbInter) {
            $PlaningGlobal->setCellValue($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne, $NbInter);
            $PlaningGlobal->getStyle($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne)->getFill()->getStartColor()->setARGB('006D00');
            $PlaningGlobal->getStyle($LstCorrespondanceColonneMois[$IDM] . $NumeroLigne)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
        }

    }
}

ob_clean();
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Campagne-Recapitulatif.xlsx"');
header('Cache-Control: max-age=0');

#header("Content-type: application/octet-stream");
#header('Content-Disposition: attachment;filename="Campagne-Recapitulatif.xlsx"');
#header("Pragma: no-cache");
#header ("Expires: 0");

$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;

?>