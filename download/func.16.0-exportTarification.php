<?php
/**
 * Export du RESEAU
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => Fill::FILL_SOLID,
            'startcolor' => array('rgb' => $color)
        ));

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFont()
        ->applyFromArray(array(
            'color' => array('rgb' => 'FF00FF00')
        ));
}

#Spreadsheet()
$objPHPExcel = new Spreadsheet();

/** Initialisation du document */
$objPHPExcel->getProperties()->setCreator("Suppl'ACTIV")
    ->setLastModifiedBy("Suppl'ACTIV")
    ->setTitle("RESEAU")
    ->setSubject("RESEAU")
    ->setKeywords("RESEAU")
    ->setCategory("RESEAU");

/** Creation des colonnes */
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Nom')
    ->setCellValue('B1', 'Prénom')
    ->setCellValue('C1', 'Adresse 1')
    ->setCellValue('D1', 'Adresse 2')
    ->setCellValue('E1', 'Code postal')
    ->setCellValue('F1', 'Ville')
    ->setCellValue('G1', 'Tel Fixe')
    ->setCellValue('H1', 'Tel Mobile')
    ->setCellValue('I1', 'Mail')
    ->setCellValue('J1', 'Commentaires')
    ->setCellValue('K1', 'Campagne')
    ->setCellValue('L1', 'Taux Horaire')
    ->setCellValue('M1', 'Forfait repas')
    ->setCellValue('N1', 'Forfait tel')
    ->setCellValue('O1', 'Taux kms')
    ->setCellValue('P1', 'Nb Intervention');

/** @var $styleThickBrownBorderOutline Bordure */
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        ),
    ),
);

/** Style de la ligne 1 */
for ($col = 'A'; $col != 'Q'; $col++){
    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle($col.'1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($col.'1')->applyFromArray($styleThickBrownBorderOutline);
}

cellColor('A1:P1', '406caf');

$numeroLigne = 2;

$RechercheTarificationCampagneExc->execute();
while($InfoCampagne = $RechercheTarificationCampagneExc->fetch(PDO::FETCH_OBJ)){

    $boolAffiche = true;
    $boolAffiche = (isset($_SESSION['idIntervenant_16_0']) && in_array($_SESSION['idIntervenant_16_0'], array($InfoCampagne->idIntervenant, 'ALL'))) ? $boolAffiche : false;
    $boolAffiche = (isset($_SESSION['idCampagne_16_0']) && in_array($_SESSION['idCampagne_16_0'], array($InfoCampagne->idCampagne, 'ALL'))) ? $boolAffiche : false;

    if($boolAffiche) {


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$numeroLigne, $InfoCampagne->nomIntervenant)
            ->setCellValue('B'.$numeroLigne, $InfoCampagne->prenomIntervenant)
            ->setCellValue('C'.$numeroLigne, $InfoCampagne->adresseIntervenant_A)
            ->setCellValue('D'.$numeroLigne, $InfoCampagne->adresseIntervenant_B)
            ->setCellValue('E'.$numeroLigne, $InfoCampagne->codePostalIntervenant)
            ->setCellValue('F'.$numeroLigne, $InfoCampagne->villeIntervenant)
            ->setCellValue('G'.$numeroLigne, $InfoCampagne->telephoneIntervenant_A)
            ->setCellValue('H'.$numeroLigne, $InfoCampagne->mobileIntervenant)
            ->setCellValue('I'.$numeroLigne, $InfoCampagne->mailIntervenant)
            ->setCellValue('J'.$numeroLigne, $InfoCampagne->commentaireIntervenant)
            ->setCellValue('K'.$numeroLigne, $InfoCampagne->libelleCampagne)

            ->setCellValue('L'.$numeroLigne, $InfoCampagne->salaireBrut)
            ->setCellValue('M'.$numeroLigne, $InfoCampagne->fraisRepas)
            ->setCellValue('N'.$numeroLigne, $InfoCampagne->fraisTelephone)
            ->setCellValue('O'.$numeroLigne, $InfoCampagne->tarifKM)

            ->setCellValue('P'.$numeroLigne, $InfoCampagne->nbIntervention);

        for ($col = 'A'; $col != 'Q'; $col++){
            $objPHPExcel->getActiveSheet()->getStyle($col.$numeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        }
        $numeroLigne++;
    }
}
$objPHPExcel->getActiveSheet()->setAutoFilter('A1:P'.$numeroLigne);
/** On renomme la feuille */
$objPHPExcel->getActiveSheet()->setTitle('RESEAU');

/** On active la feuille par defaut */
$objPHPExcel->setActiveSheetIndex(0);

/** On force le telechargement du fichier */
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="SupplACTIV_RESEAU.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;