<?php
/**
 * Export du RESEAU
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => Fill::FILL_SOLID,
            'startcolor' => array('rgb' => $color)
        ));

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFont()
        ->applyFromArray(array(
            'color' => array('rgb' => 'FF00FF00')
        ));
}


/** @var $objPHPExcel Objet PHPExcel */
$objPHPExcel = new Spreadsheet();

/** Initialisation du document */
$objPHPExcel->getProperties()->setCreator("Suppl'ACTIV")
    ->setLastModifiedBy("Suppl'ACTIV")
    ->setTitle("FRAIS DES INTERVENANT")
    ->setSubject("FRAIS DES INTERVENANT")
    ->setKeywords("FRAIS DES INTERVENANT")
    ->setCategory("FRAIS DES INTERVENANT");

/** Creation des colonnes */
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'NOM INTERVENANT')
    ->setCellValue('B1', 'PRIME SUR OBJECTIF')
    ->setCellValue('C1', 'COMMENTAIRE PRIME SUR OBJECTIF')
    ->setCellValue('D1', 'INDEMNITE INTERVENTION ANNULE')
    ->setCellValue('E1', 'COMMENTAIRE INDEMNITE')
    ->setCellValue('F1', 'AUTRE PRIME')
    ->setCellValue('G1', 'COMMENTAIRE AUTRE PRIME')
    ->setCellValue('H1', 'PRIME RDV')
    ->setCellValue('I1', 'INDEMNITE FORMATION')
    ->setCellValue('J1', 'FRAIS SUR JUSTIFICATIF')
    ->setCellValue('K1', 'FRAIS KM')
    ->setCellValue('L1', 'INDEMNITE TELEPHONE')
    ->setCellValue('M1', 'AUTRE FRAIS')
    ->setCellValue('N1', 'REPRISE ACOMPTE');

/** @var $styleThickBrownBorderOutline Bordure */
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        ),
    ),
);


/** Style de la ligne 1 */
for ($col = 'A'; $col != 'O'; $col++){
    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle($col.'1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($col.'1')->applyFromArray($styleThickBrownBorderOutline);
}

cellColor('A1:N1', '406caf');

$numeroLigne = 2;

$ListeTauxKM = array();
$RechercheTauxKmExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
$RechercheTauxKmExc->execute();
while ($InfoTaux = $RechercheTauxKmExc->fetch(PDO::FETCH_OBJ)) {
    $ListeTauxKM[$InfoTaux->tarifKilometre] = 0;
}

$RechercheIntervenantMoisExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
$RechercheIntervenantMoisExc->execute();
while ($InfoFrais = $RechercheIntervenantMoisExc->fetch(PDO::FETCH_OBJ)) {

    $hasFrais = true;
    $hasKm = false;
    #print_r($InfoFrais);
    #return;

    foreach ($ListeTauxKM as $tarif => $km) {
        $ListeTauxKM[$tarif] = 0;
    }

    /** Recherche des differents KM possible */
    $RechercheKmInterventionExc->bindValue(':idIntervenant', $InfoFrais->idIntervenant, PDO::PARAM_INT);
    $RechercheKmInterventionExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
    $RechercheKmInterventionExc->execute();

    if (
        $InfoFrais->montantPrimeRDV == 0 &&
        $InfoFrais->montantIndemniteFormation == 0 &&
        $InfoFrais->montantIndemniteTelephone == 0 &&
        $InfoFrais->montantAutreFrais == 0 &&
        $InfoFrais->montantFraisJustificatif == 0 &&
        $InfoFrais->montantPrimeObjectif == 0 &&
        $InfoFrais->montantIndemniteAnnulation == 0 &&
        $InfoFrais->montantAutrePrime == 0 &&
        $InfoFrais->montantRepriseAccompte == 0
    ) {
        $hasFrais = false;
    }

    $listeKM = '';
    while ($InfoKm = $RechercheKmInterventionExc->fetch(PDO::FETCH_OBJ)) {
        $listeKM .= (!empty($listeKM)) ? ' | ' . $InfoKm->tarifKilometre . ' &euro; : ' . $InfoKm->totalKilometre : $InfoKm->tarifKilometre . ' &euro; : ' . $InfoKm->totalKilometre;
        $ListeTauxKM[$InfoKm->tarifKilometre] = $InfoKm->totalKilometre;
    }

    $LstKM = '';
    foreach ($ListeTauxKM as $tarif => $km) {
        if ($km > 0) {
            $LstKM .= ($LstKM == '') ? 'NB KM A ' . $tarif . ' EURO : ' . $km : ' | ' . 'NB KM A ' . $tarif . ' EURO : ' . $km;
            $hasKm = true;
        }

    }

    if ($hasKm || $hasFrais) {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $numeroLigne, $InfoFrais->nomIntervenant . ' ' . $InfoFrais->prenomIntervenant)
            ->setCellValue('B' . $numeroLigne, $InfoFrais->montantPrimeObjectif)
            ->setCellValue('C' . $numeroLigne, $InfoFrais->commentairePrimeObjectif)
            ->setCellValue('D' . $numeroLigne, $InfoFrais->montantIndemniteAnnulation)
            ->setCellValue('E' . $numeroLigne, $InfoFrais->commentaireIndemniteAnnulation)
            ->setCellValue('F' . $numeroLigne, $InfoFrais->montantAutrePrime)
            ->setCellValue('G' . $numeroLigne, $InfoFrais->commentaireAutrePrime)
            ->setCellValue('H' . $numeroLigne, $InfoFrais->montantPrimeRDV)
            ->setCellValue('I' . $numeroLigne, $InfoFrais->montantIndemniteFormation)
            ->setCellValue('J' . $numeroLigne, $InfoFrais->montantFraisJustificatif)
            ->setCellValue('K' . $numeroLigne, $LstKM)
            ->setCellValue('L' . $numeroLigne, $InfoFrais->montantIndemniteTelephone)
            ->setCellValue('M' . $numeroLigne, $InfoFrais->montantAutreFrais)
            ->setCellValue('N' . $numeroLigne, $InfoFrais->montantRepriseAccompte);

        for ($col = 'A'; $col != 'O'; $col++) {
            $objPHPExcel->getActiveSheet()->getStyle($col . $numeroLigne)->applyFromArray($styleThickBrownBorderOutline);
        }

        cellColor('B' . $numeroLigne . ':I' . $numeroLigne, 'ffbb00');
        cellColor('J' . $numeroLigne . ':N' . $numeroLigne, '67bb00');
        $numeroLigne++;
    }
}

$objPHPExcel->getActiveSheet()->setAutoFilter('A1:P'.$numeroLigne);
/** On renomme la feuille */
$objPHPExcel->getActiveSheet()->setTitle('RESEAU');

/** On active la feuille par defaut */
$objPHPExcel->setActiveSheetIndex(0);

/** On force le telechargement du fichier */
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="SupplACTIV_FRAIS.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;
