<?php
/**
 * Export du registre du personnel
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('display_errors', 'on');
set_time_limit(0);
ini_set('memory_limit', -1);

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

$objPHPExcel = new Spreadsheet();

$sheet = $objPHPExcel->getActiveSheet()->setTitle('REGISTRE DU PERSONNEL');

$objPHPExcel->getActiveSheet()->setCellValue('A4', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B4', 'INTERVENANT');
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'SEXE');
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'NATIONALITE');
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'DATE NAISSANCE');
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'DATE ENTREE');
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'DATE SORTIE');
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'TYPE TRAVAIL');


$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->setFillType(Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A4:H4')->getFill()->getStartColor()->setARGB('FFD800');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//Bords noir
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ),
    ),
);

$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray($styleThickBrownBorderOutline);

$NumeroLigne = 4;

$Nb_Intervention_Mois = array(); $Liste_Intervention = array();
$ListeIntervenantTOTAL = array(); $ListeInterventionFinal = array();

/** Recherche des intervenants */
$tabIntervenant = array();
$RechercheIntervenantNonAutoExec->execute();
while($DataIntervenant = $RechercheIntervenantNonAutoExec->fetch(PDO::FETCH_OBJ)) {
    $tabIntervenant[] = $DataIntervenant->idIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['NOM'] = $DataIntervenant->nomIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['PRENOM'] = $DataIntervenant->prenomIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['NAISSANCE'] = $DataIntervenant->dateNaissance;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['DEP'] = $DataIntervenant->departementNaissance;
}

/** Recherche des interventions avec contrat */

foreach ($tabIntervenant as $idInter) {
    $RechercheInterventionAvecContratNoAutoExec->bindValue(':idinter', $idInter);
    $RechercheInterventionAvecContratNoAutoExec->execute();
    while($DataContrat = $RechercheInterventionAvecContratNoAutoExec->fetch(PDO::FETCH_OBJ)) {
        @$Liste_Intervention[$DataContrat->FK_idIntervenant][$DataContrat->Dte] = true;
    }
}

#$RechercheInterventionAvecContratExc->execute();
#while($DataContrat = $RechercheInterventionAvecContratExc->fetch(PDO::FETCH_OBJ)) {
#    @$Liste_Intervention[$DataContrat->FK_idIntervenant][$DataContrat->Dte] = true;
#}

/** Parcours des interventions */
foreach($Liste_Intervention as $IdIntervenant=>$Valeur) {

    /** Initialisation des variables de debut et de fin */
    $DateDebut = ''; $DateFin = '';
    foreach ($Valeur as $DateIntervention => $Total) {
        if ($DateIntervention != '') {

            $DateDebut = (empty($DateDebut)) ? $DateIntervention : $DateDebut;
            $DateFin = (empty($DateFin)) ? $DateIntervention : $DateFin;

            if (isset($Liste_Intervention[$IdIntervenant][$DateIntervention + 1])) {
                $DateFin = $DateIntervention + 1;
            } else {


                $NumeroLigne++;


                $objPHPExcel->getActiveSheet()->setCellValue('A'.$NumeroLigne, $IdIntervenant);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$NumeroLigne, $ListeIntervenantTOTAL[$IdIntervenant]['NOM'].' '.@$ListeIntervenantTOTAL[$IdIntervenant]['PRENOM']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$NumeroLigne, "F");
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$NumeroLigne, $ListeIntervenantTOTAL[$IdIntervenant]['DEP']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$NumeroLigne, $ListeIntervenantTOTAL[$IdIntervenant]['NAISSANCE']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$NumeroLigne, substr($DateDebut,0,4).' - '.substr($DateDebut,4,2).' - '.substr($DateDebut,-2));
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$NumeroLigne, substr($DateFin,0,4).' - '.substr($DateFin,4,2).' - '.substr($DateFin,-2));
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$NumeroLigne, 'Animateur / Formateur');

                //Style
            /*    $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne.':H'.$NumeroLigne)->getFont()->setSize(10);

                $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne.':H'.$NumeroLigne)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

                $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline); */

                $DateDebut = ''; $DateFin = '';
            }
        }
    }
}

$RechercheCIDDNoAutoExec->execute();
while($InfoCidd = $RechercheCIDDNoAutoExec->fetch(PDO::FETCH_OBJ)) {
    $Dte = preg_split('/-/', $InfoCidd->dateContrat);
    $DernierJourMois = date(date("t", mktime(0, 0, 0, $Dte[1], 1, $Dte[0])), mktime(0, 0, 0, $Dte[1], 1, $Dte[0]));

    $NumeroLigne++;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$NumeroLigne, $InfoCidd->idIntervenant);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$NumeroLigne, $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NOM'].' '.@$ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['PRENOM']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$NumeroLigne, "F");
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$NumeroLigne, $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['DEP']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$NumeroLigne, $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NAISSANCE']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$NumeroLigne, $Dte[0].' - '.$Dte[1].' - 01');
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$NumeroLigne, $Dte[0].' - '.$Dte[1].' - '.$DernierJourMois);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$NumeroLigne, 'Convention de Forfait');

    //Style
 /*   $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne.':H'.$NumeroLigne)->getFont()->setSize(10);

    $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne.':H'.$NumeroLigne)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne.':H'.$NumeroLigne)->getFill()->getStartColor()->setARGB('FFD980');

    $objPHPExcel->getActiveSheet()->getStyle('A'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('C'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('D'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('E'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('F'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('G'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    $objPHPExcel->getActiveSheet()->getStyle('H'.$NumeroLigne)->applyFromArray($styleThickBrownBorderOutline); */
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="RegistrePersonnel.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;