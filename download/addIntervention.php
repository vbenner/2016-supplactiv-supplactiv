<?php

/** -----------------------------------------------------------------------------------------------
 * Le mot de passe est généré en 'dur'
 */
if (!isset($_POST['pwd'])) {
    header('HTTP/1.1 500 Error Code POST 1.0');
    return;
}
if (!isset($_POST['json'])) {
    header('HTTP/1.1 500 Error Code POST 1.1');
    return;
}

/** -----------------------------------------------------------------------------------------------
 * Il doit être sécurisé
 */
$password = 'wYh7]jU]%#,]qSjP';
if ($_POST['pwd'] != $password) {
    header('HTTP/1.1 500 Error Code POST 2.0');
    return;
}

/** -----------------------------------------------------------------------------------------------
 * On décode le JSON
 */
$json = json_decode($_POST['json']);
foreach ($json as $item) {

    /** -------------------------------------------------------------------------------------------
     * idPDV (ou Code CIP)
     * idIntervenant
     * idMission (aka idCampagne)
     * dateIntervention Y-m-d
     * hDeb + hFin H:i:s
     * type (A = Animation / F = Formation)
     * action (A = Ajout / D = Delete / U = Update
     *
     * idIntervention : facultatif
     */
    $idPDV = $item->idPDV;
    $idIntervenant = $item->idIntervenant;
    $idMission = $item->idMission;
    $dateIntervention = $item->dateIntervention;
    $hDeb = $item->hDeb;
    $hFin = $item->hFin;
    $type = $item->type;
    $action = $item->action;

    $idIntervention = $item->idIntervention;

    /** -------------------------------------------------------------------------------------------
     * SI A => INSERT
     * et renvoyer ID de l'intervention dans le code 200
     */

    /** -------------------------------------------------------------------------------------------
     * SI U => UPDATE
     */

    /** -------------------------------------------------------------------------------------------
     * SI D => DELETE
     */

}