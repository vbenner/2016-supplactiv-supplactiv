<?php
/**
 * Export des contrats de convention
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('display_errors','on');

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Class HTML2PDF */
#require_once __DIR__ . '/../current/includes/librairies/html2pdf_v4.03/html2pdf.class.php';
require __DIR__.'/../current/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

# ------------------------------------------------------------------------------------
# Recherche des infos de l'intervenant
# ------------------------------------------------------------------------------------
$sqlRechercheInfo = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant
FROM su_intervenant
    INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idIntervenant = su_intervenant.idIntervenant
WHERE idContrat = :Contrat
GROUP BY IdIntervenant, NomIntervenant, PrenomIntervenant';
$RechercheInfoExc = DbConnexion::getInstance()->prepare($sqlRechercheInfo);

ob_start();
include(dirname(__FILE__).'/_html/contratConvention.php');
$content = ob_get_clean();


try
{
    # ------------------------------------------------------------------------------------
    # On defini les parametres du PDF
    # ------------------------------------------------------------------------------------
    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));

    # ------------------------------------------------------------------------------------
    # On recupere l'ID du contrat
    # ------------------------------------------------------------------------------------
    $IdContrat = $_SESSION['IDContrat_Recherche_6_5'];


    $RechercheInfoExc->bindValue(':Contrat', $IdContrat, PDO::PARAM_INT);
    $RechercheInfoExc->execute();
    $InfoContrat = $RechercheInfoExc->fetch(PDO::FETCH_OBJ);

    $html2pdf->Output('Contrat_'.$InfoContrat->nomIntervenant.'_'.$InfoContrat->idIntervenant.'_'.$IdContrat.'_DU_'.date('d-m-Y').'.pdf');
    if(!is_dir('CIDD/')): mkdir('CIDD'); endif;
    if(!is_dir('CIDD/'.$InfoContrat->idIntervenant.'/')):
        mkdir('CIDD/'.$InfoContrat->idIntervenant);
    endif;

    $html2pdf->Output('CIDD/'.$InfoContrat->idIntervenant.'/Contrat_'.$IdContrat.'.pdf', 'F');
}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}
