<?php
/**
 * Export du KILOMETRAGE
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('memory_limit', -1);
set_time_limit(0);

/** Connexion a la base de donnees */
require_once __DIR__ . '/../current/_config/config.sql.php';

/** Requête sql */
require_once __DIR__ . '/../current/includes/queries/queries.bdd2web.php';

/** Librairie PHP Excel */
require '../current/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

function cellColor($cells, $color)
{
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => Fill::FILL_SOLID,
            'startcolor' => array('rgb' => $color)
        ));

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFont()
        ->applyFromArray(array(
            'color' => array('rgb' => 'FF00FF00')
        ));
}


/** @var $objPHPExcel Objet PHPExcel */
$objPHPExcel = new Spreadsheet();

/** Initialisation du document */
$objPHPExcel->getProperties()->setCreator("Suppl'ACTIV")
    ->setLastModifiedBy("Suppl'ACTIV")
    ->setTitle("RECAPITULATIF DES KILOMETRAGES")
    ->setSubject("RECAPITULATIF DES KILOMETRAGES")
    ->setKeywords("RECAPITULATIF DES KILOMETRAGES")
    ->setCategory("RECAPITULATIF DES KILOMETRAGES");

/** Creation des colonnes */
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Code CIP')
    ->setCellValue('B1', 'Libellé PDV')
    ->setCellValue('C1', 'Code Postal PDV')
    ->setCellValue('D1', 'Ville PDV')
    ->setCellValue('E1', 'Id Intervenant')
    ->setCellValue('F1', 'Intervenant (Nom + Prénom)')
    ->setCellValue('G1', 'Adresse Intervenant')
    ->setCellValue('H1', 'Code Postal Intervenant')
    ->setCellValue('I1', 'Ville Intervenant')
    ->setCellValue('J1', 'KM Aller');

/** @var $styleThickBrownBorderOutline Bordure */
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        ),
    ),
);


/** Style de la ligne 1 */
for ($col = 'A'; $col != 'K'; $col++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle($col . '1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($col . '1')->applyFromArray($styleThickBrownBorderOutline);
}

cellColor('A1:J1', '406caf');

$numeroLigne = 2;


/**
 * Recherche des couple point de vente / intervenant avec leurs KM
 *
 * @param idIntervenant
 * @param idPdv
 *
 * @author Kevin MAURICE - Page UP
 * @var $RecherchePdvVisiteKmExc
 */
$sqlRecherchePdvVisiteKm = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, codePostalIntervenant, villeIntervenant, idPdv, codePostalPdv, villePdv, libellePdv, codeMerval, totalKm
FROM su_intervention
	INNER JOIN su_intervenant  ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv  ON su_pdv.idPdv = su_intervention.FK_idPdv
	LEFT JOIN su_pdv_kilometrage ON su_pdv_kilometrage.FK_idPdv = su_intervention.FK_idPdv AND su_pdv_kilometrage.FK_idIntervenant = su_intervenant.idIntervenant
WHERE su_intervention.FK_idIntervenant <> 1';

/** Filtre sur le point de vente */
if (isset($_SESSION['idPointdeVente_13_0']) && $_SESSION['idPointdeVente_13_0'] != 'ALL') {
    $sqlRecherchePdvVisiteKm .= ' AND su_intervention.FK_idPdv = :idPdv';
}

/** Filtre sur l'intervenante */
if (isset($_SESSION['idIntervenant_13_0']) && $_SESSION['idIntervenant_13_0'] != 'ALL') {
    $sqlRecherchePdvVisiteKm .= ' AND su_intervention.FK_idIntervenant = :idIntervenant';
}

/** Filtre sur la date de debut et de fin */
$sqlRecherchePdvVisiteKm .= (isset($_SESSION['boolPlanification_13_0']) && $_SESSION['boolPlanification_13_0'] == 1) ? ' AND ((dateDebut BETWEEN :dateDebut AND :dateFin) OR dateDebut IS NULL )' : ' AND dateDebut BETWEEN :dateDebut AND :dateFin';
$sqlRecherchePdvVisiteKm .= ' GROUP BY idIntervenant, idPdv';

$RecherchePdvVisiteKmExc = DbConnexion::getInstance()->prepare($sqlRecherchePdvVisiteKm);

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_13_0']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_13_0']);

/** Recherche des couples PDV / intervenant */
$RecherchePdvVisiteKmExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
$RecherchePdvVisiteKmExc->bindValue(':dateFin', $dateFin->format('Y-m-d') . ' 23:59:00', PDO::PARAM_STR);
if (isset($_SESSION['idPointdeVente_13_0']) && $_SESSION['idPointdeVente_13_0'] != 'ALL') {
    $RecherchePdvVisiteKmExc->bindValue(':idPdv', $_SESSION['idPointdeVente_13_0'], PDO::PARAM_INT);
}
if (isset($_SESSION['idIntervenant_13_0']) && $_SESSION['idIntervenant_13_0'] != 'ALL') {
    $RecherchePdvVisiteKmExc->bindValue(':idIntervenant', $_SESSION['idIntervenant_13_0'], PDO::PARAM_INT);
}
$RecherchePdvVisiteKmExc->execute();
while ($InfoKm = $RecherchePdvVisiteKmExc->fetch(PDO::FETCH_OBJ)) {

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $numeroLigne, $InfoKm->codeMerval)
        ->setCellValue('B' . $numeroLigne, $InfoKm->libellePdv)
        ->setCellValue('C' . $numeroLigne, $InfoKm->codePostalPdv)
        ->setCellValue('D' . $numeroLigne, $InfoKm->villePdv)
        ->setCellValue('E' . $numeroLigne, $InfoKm->idIntervenant)
        ->setCellValue('F' . $numeroLigne, $InfoKm->nomIntervenant . ' ' . $InfoKm->prenomIntervenant)
        ->setCellValue('G' . $numeroLigne, $InfoKm->adresseIntervenant_A)
        ->setCellValue('H' . $numeroLigne, $InfoKm->codePostalIntervenant)
        ->setCellValue('I' . $numeroLigne, $InfoKm->villeIntervenant)
        ->setCellValue('J' . $numeroLigne, $InfoKm->totalKm);

    for ($col = 'A'; $col != 'K'; $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col . $numeroLigne)->applyFromArray($styleThickBrownBorderOutline);
    }
    $numeroLigne++;
}


$objPHPExcel->getActiveSheet()->setAutoFilter('A1:K' . $numeroLigne);
/** On renomme la feuille */
$objPHPExcel->getActiveSheet()->setTitle('RECAPITULATIF DES KILOMETRAGES');

/** On active la feuille par defaut */
$objPHPExcel->setActiveSheetIndex(0);

/** On force le telechargement du fichier */
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Kilometrage-Intervenant.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = new Xlsx($objPHPExcel);
$objWriter->save('php://output');
exit;
