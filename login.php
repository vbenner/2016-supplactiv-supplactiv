<?php
// ------------------------------------------------------------------------------------
// Require Database Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/_config/config.sql.php';

session_unset();
session_start();
// ------------------------------------------------------------------------------------
// PREPA REQUETE - Maj des identifiants
// ------------------------------------------------------------------------------------
$sqlMajMdpUtilisateur = '
UPDATE du_utilisateur SET
  passUtilisateur = passUtilisateurReinit,
  boolDdeReinit = 0,
  tokenReinitMdp = ""
WHERE idUtilisateur = :idUtilisateur';
$MajMdpUtilisateurExc = DbConnexion::getInstance()->prepare($sqlMajMdpUtilisateur);

// ------------------------------------------------------------------------------------
// PREPA REQUETE - Recherche du token
// ------------------------------------------------------------------------------------
$sqlRechercheToken = '
SELECT idUtilisateur
FROM du_utilisateur
WHERE tokenReinitMdp = :token AND boolDdeReinit = 1';
$RechercheTokenExc = DbConnexion::getInstance()->prepare($sqlRechercheToken);

$boolDdeReinit = false;
if(filter_has_var(INPUT_GET, 'token') && filter_input(INPUT_GET, 'token') != ''){
    $RechercheTokenExc->bindValue(':token', filter_input(INPUT_GET, 'token'), PDO::PARAM_STR);
    $RechercheTokenExc->execute();
    if($RechercheTokenExc->rowCount() > 0){
        $InfoUtilisateur = $RechercheTokenExc->fetch(PDO::FETCH_OBJ);
        $MajMdpUtilisateurExc->bindValue(':idUtilisateur', $InfoUtilisateur->idUtilisateur, PDO::PARAM_INT);
        $boolDdeReinit = ($MajMdpUtilisateurExc->execute()) ? true : false;
    }
}
?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo PROJECT_NAME_HEAD ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="Kevin MAURICE - Page UP" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <link href="assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
    <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="login" style="background-color: #c0c0c0;">
<!-- BEGIN LOGO -->
<div class="logo">

</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content" style="border: 2px #2A598C solid">
<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="#" method="post">
    <div style="clear: left; float: left; position: relative; left: 10%;">
    <h3 class="form-title"><img src="assets/global/img/logo-sidebar.png" style="width: 80%"/></h3>
    </div>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
			<span>
				 Identifiants incorrects
			</span>
    </div>
    <div id="divAlertReinit" class="alert alert-info display-hide">
        <button class="close" data-close="alert"></button>
        <span></span>
    </div>
    <?php
    if(filter_has_var(INPUT_GET, 'token')){
        if($boolDdeReinit){
            print '<div class="alert alert-success"><button class="close" data-close="alert"></button><span>Vos identifiants ont &eacute;t&eacute; r&eacute;initialis&eacute;s</span></div>';
        }else {
            print '<div class="alert alert-danger"><button class="close" data-close="alert"></button><span>ERREUR - Cl&eacute; de r&eacute;initialisation incorrecte !</span></div>';
        }
    }
    ?>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">E-mail</label>
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input id="ztNomUtilisateur" class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="E-mail" name="username"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Mot de passe</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input id="ztPassUtilisateur" class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Mot de passe" name="password"/>
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn pull-right bg bg-green">
            Connexion <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>
    <div class="forget-password">
        <h4>Mot de passe oubli&eacute; ?</h4>
        <p>
            <a href="javascript:;" id="forget-password">Cliquez-ici pour r&eacute;initialiser votre mot de passe</a>
        </p>
    </div>
</form>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="index.html" method="post">
    <h3 class="form-title"><img src="assets/global/img/logo-sidebar.png" style="width: 100%"/></h3>

    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
			<span>
				 Identifiants incorrects
			</span>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input id="ztEmailUtilisateur" class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="E-mail" name="ztEmailUtilisateur"/>
        </div>
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn">
            <i class="m-icon-swapleft"></i> Retour </button>
        <button type="submit" class="btn pull-right bg bg-green">
            Valider <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2014 &copy; Page UP
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/select2/dist/js/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>