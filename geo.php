<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(-1);

/** -----------------------------------------------------------------------------------------------
 * On récupère les infos du PDV (éventuellement on géocode)
 */

require_once dirname(__FILE__) . '/_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * On recherche le PDV
 */
$sqlGetAllIntervenantes = '
SELECT *
FROM su_intervenant
WHERE boolActif = 1
AND ISNULL(latitude)
ORDER BY idIntervenant
';
$getAllIntervenantesExec = DbConnexion::getInstance()->prepare($sqlGetAllIntervenantes);
$getAllIntervenantesExec->execute();
$i = 0;
$j= 0;
while ($inter = $getAllIntervenantesExec->fetch(PDO::FETCH_OBJ)) {

    /** -------------------------------------------------------------------------------------------
     * On géocode rapidement
     */
    $j++;
    $url = ('https://maps.googleapis.com/maps/api/geocode/json?address=' . $inter->adresseIntervenant_A . ',' . $inter->adresseIntervenant_B . ',' . $inter->codePostalIntervenant . ',' . $inter->villeIntervenant . '&region=FR&key=AIzaSyCYinfTYbQZNLJYFIYqgikVtjj5RITpm5k');

    echo $url.'<br/>';

    $url = str_replace(" ", "+", $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response, TRUE);

    /** -------------------------------------------------------------------------------------------
     * Si tout est OK
     */
    if ($result[ 'status' ] == 'OK') {

        $i++;
        $latlon = array(
            'lat' => $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lat' ],
            'lng' => $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lng' ],
        );

        $sqlUpdateInter = '
        UPDATE su_intervenant
        SET latitude = :lat, longitude = :lng
        WHERE idIntervenant = :inter
        ';
        $updateInterExec = DbConnexion::getInstance()->prepare($sqlUpdateInter);
        $updateInterExec->bindValue(':inter', $inter->idIntervenant, PDO::PARAM_INT);
        $updateInterExec->bindValue(':lat', $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lat' ], PDO::PARAM_INT);
        $updateInterExec->bindValue(':lng', $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lng' ], PDO::PARAM_INT);
        $updateInterExec->execute();

    } else {
        echo $inter->nomIntervenant.'<br/>';
    }

}

echo $i.'/'.$j;
