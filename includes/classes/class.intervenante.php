<?php

/**
 * @name Model_Base_Intervenant
 * @version 24/09/2014 (dd/mm/yyyy)
 * @author WebProjectHelper (http://www.elfangels.fr/webprojecthelper/)
 */
class Intervenant
{
    // Nom de la table
    const TABLENAME = 'su_intervenant';

    // Nom des champs
    const FIELDNAME_IDINTERVENANT = 'idIntervenant';
    const FIELDNAME_NOMINTERVENANT = 'nomIntervenant';
    const FIELDNAME_NOMINTERVENANT_JF = 'nomIntervenant_JF';
    const FIELDNAME_PRENOMINTERVENANT = 'prenomIntervenant';
    const FIELDNAME_ADRESSEINTERVENANT_A = 'adresseIntervenant_A';
    const FIELDNAME_ADRESSEINTERVENANT_B = 'adresseIntervenant_B';
    const FIELDNAME_ADRESSEINTERVENANT_C = 'adresseIntervenant_C';
    const FIELDNAME_TELEPHONEINTERVENANT_A = 'telephoneIntervenant_A';
    const FIELDNAME_TELEPHONEINTERVENANT_B = 'telephoneIntervenant_B';
    const FIELDNAME_FAXINTERVENANT = 'faxIntervenant';
    const FIELDNAME_MOBILEINTERVENANT = 'mobileIntervenant';
    const FIELDNAME_MAILINTERVENANT = 'mailIntervenant';
    const FIELDNAME_NUMEROSS = 'numeroSS';
    const FIELDNAME_DATENAISSANCE = 'dateNaissance';
    const FIELDNAME_VILLENAISSANCE = 'villeNaissance';
    const FIELDNAME_DEPARTEMENTNAISSANCE = 'departementNaissance';
    const FIELDNAME_PAYSNAISSANCE = 'paysNaissance';
    const FIELDNAME_NATIONALITE = 'Nationalite';
    const FIELDNAME_COMMENTAIREINTERVENANT = 'commentaireIntervenant';
    const FIELDNAME_CODEPOSTALINTERVENANT = 'codePostalIntervenant';
    const FIELDNAME_VILLEINTERVENANT = 'villeIntervenant';
    const FIELDNAME_BOOLACTIF = 'boolActif';
    const FIELDNAME_LIBELLEBANQUE = 'libelleBanque';
    const FIELDNAME_CODEBANQUE = 'codeBanque';
    const FIELDNAME_CODEGUICHET = 'codeGuichet';
    const FIELDNAME_NUMEROCOMPTE = 'numeroCompte';
    const FIELDNAME_CLESECURITE = 'cleSecurite';
    const FIELDNAME_GENRE = 'genre';
    const FIELDNAME_SITUATIONFAMILIALE = 'situationFamiliale';
    const FIELDNAME_NUMEROCARTESEJOUR = 'numeroCarteSejour';
    const FIELDNAME_DATEFINSEJOUR = 'dateFinSejour';
    const FIELDNAME_NOMBREENFANT = 'nombreEnfant';
    const FIELDNAME_BOOLAUTOENTREPRENEUSE = 'boolAutoEntrepreneuse';
    const FIELDNAME_DEPNAISSANCEDUE = 'depNaissanceDUE';
    const FIELDNAME_NUMEROIBAN = 'numeroIBAN';
    const FIELDNAME_CODEBIC = 'codeBIC';

    /** @var PDO  */
    protected $_pdo;

    /** @var array tableau pour le chargement fainéant */
    protected static $_lazyload;

    /** @var int  */
    protected $_idIntervenant;

    /** @var string  */
    protected $_nomIntervenant;

    /** @var string  */
    protected $_nomIntervenant_JF;

    /** @var string  */
    protected $_prenomIntervenant;

    /** @var string  */
    protected $_adresseIntervenant_A;

    /** @var string  */
    protected $_adresseIntervenant_B;

    /** @var string  */
    protected $_adresseIntervenant_C;

    /** @var string  */
    protected $_telephoneIntervenant_A;

    /** @var string  */
    protected $_telephoneIntervenant_B;

    /** @var string  */
    protected $_faxIntervenant;

    /** @var string  */
    protected $_mobileIntervenant;

    /** @var string  */
    protected $_mailIntervenant;

    /** @var string  */
    protected $_numeroSS;

    /** @var int  */
    protected $_dateNaissance;

    /** @var string  */
    protected $_villeNaissance;

    /** @var string  */
    protected $_departementNaissance;

    /** @var string  */
    protected $_paysNaissance;

    /** @var string  */
    protected $_nationalite;

    /** @var string  */
    protected $_commentaireIntervenant;

    /** @var string  */
    protected $_codePostalIntervenant;

    /** @var string  */
    protected $_villeIntervenant;

    /** @var bool  */
    protected $_boolActif;

    /** @var string  */
    protected $_libelleBanque;

    /** @var string  */
    protected $_codeBanque;

    /** @var string  */
    protected $_codeGuichet;

    /** @var string  */
    protected $_numeroCompte;

    /** @var string  */
    protected $_cleSecurite;

    /** @var string  */
    protected $_genre;

    /** @var string  */
    protected $_situationFamiliale;

    /** @var string  */
    protected $_numeroCarteSejour;

    /** @var int  */
    protected $_dateFinSejour;

    /** @var int  */
    protected $_nombreEnfant;

    /** @var bool  */
    protected $_boolAutoEntrepreneuse;

    /** @var string  */
    protected $_depNaissanceDUE;

    /** @var string  */
    protected $_numeroIBAN;

    /** @var string  */
    protected $_codeBIC;

    /**
     * Construire un(e) intervenant
     * @param $pdo PDO
     * @param $idIntervenant int
     * @param $nomIntervenant string
     * @param $nomIntervenant_JF string
     * @param $prenomIntervenant string
     * @param $adresseIntervenant_A string
     * @param $adresseIntervenant_B string
     * @param $adresseIntervenant_C string
     * @param $telephoneIntervenant_A string
     * @param $telephoneIntervenant_B string
     * @param $faxIntervenant string
     * @param $mobileIntervenant string
     * @param $mailIntervenant string
     * @param $numeroSS string
     * @param $dateNaissance int
     * @param $villeNaissance string
     * @param $departementNaissance string
     * @param $paysNaissance string
     * @param $nationalite string
     * @param $commentaireIntervenant string
     * @param $codePostalIntervenant string
     * @param $villeIntervenant string
     * @param $boolActif bool
     * @param $libelleBanque string
     * @param $codeBanque string
     * @param $codeGuichet string
     * @param $numeroCompte string
     * @param $cleSecurite string
     * @param $genre string
     * @param $situationFamiliale string
     * @param $numeroCarteSejour string
     * @param $dateFinSejour int
     * @param $nombreEnfant int
     * @param $boolAutoEntrepreneuse bool
     * @param $depNaissanceDUE string
     * @param $numeroIBAN string
     * @param $codeBIC string
     * @param $lazyload bool Activer le chargement fainéant ?
     */
    protected function __construct(PDO $pdo,$idIntervenant,$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,$dateNaissance,$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,$dateFinSejour,$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC,$lazyload=true)
    {
        // Sauvegarder pdo
        $this->_pdo = $pdo;

        // Sauvegarder les attributs
        $this->_idIntervenant = $idIntervenant;
        $this->_nomIntervenant = $nomIntervenant;
        $this->_nomIntervenant_JF = $nomIntervenant_JF;
        $this->_prenomIntervenant = $prenomIntervenant;
        $this->_adresseIntervenant_A = $adresseIntervenant_A;
        $this->_adresseIntervenant_B = $adresseIntervenant_B;
        $this->_adresseIntervenant_C = $adresseIntervenant_C;
        $this->_telephoneIntervenant_A = $telephoneIntervenant_A;
        $this->_telephoneIntervenant_B = $telephoneIntervenant_B;
        $this->_faxIntervenant = $faxIntervenant;
        $this->_mobileIntervenant = $mobileIntervenant;
        $this->_mailIntervenant = $mailIntervenant;
        $this->_numeroSS = $numeroSS;
        $this->_dateNaissance = $dateNaissance;
        $this->_villeNaissance = $villeNaissance;
        $this->_departementNaissance = $departementNaissance;
        $this->_paysNaissance = $paysNaissance;
        $this->_nationalite = $nationalite;
        $this->_commentaireIntervenant = $commentaireIntervenant;
        $this->_codePostalIntervenant = $codePostalIntervenant;
        $this->_villeIntervenant = $villeIntervenant;
        $this->_boolActif = $boolActif;
        $this->_libelleBanque = $libelleBanque;
        $this->_codeBanque = $codeBanque;
        $this->_codeGuichet = $codeGuichet;
        $this->_numeroCompte = $numeroCompte;
        $this->_cleSecurite = $cleSecurite;
        $this->_genre = $genre;
        $this->_situationFamiliale = $situationFamiliale;
        $this->_numeroCarteSejour = $numeroCarteSejour;
        $this->_dateFinSejour = $dateFinSejour;
        $this->_nombreEnfant = $nombreEnfant;
        $this->_boolAutoEntrepreneuse = $boolAutoEntrepreneuse;
        $this->_depNaissanceDUE = $depNaissanceDUE;
        $this->_numeroIBAN = $numeroIBAN;
        $this->_codeBIC = $codeBIC;

        // Sauvegarder pour le chargement fainéant
        if ($lazyload) {
            self::$_lazyload[$idIntervenant] = $this;
        }
    }

    /**
     * Créer un(e) intervenant
     * @param $pdo PDO
     * @param $nomIntervenant string
     * @param $nomIntervenant_JF string
     * @param $prenomIntervenant string
     * @param $adresseIntervenant_A string
     * @param $adresseIntervenant_B string
     * @param $adresseIntervenant_C string
     * @param $telephoneIntervenant_A string
     * @param $telephoneIntervenant_B string
     * @param $faxIntervenant string
     * @param $mobileIntervenant string
     * @param $mailIntervenant string
     * @param $numeroSS string
     * @param $dateNaissance int
     * @param $villeNaissance string
     * @param $departementNaissance string
     * @param $paysNaissance string
     * @param $nationalite string
     * @param $commentaireIntervenant string
     * @param $codePostalIntervenant string
     * @param $villeIntervenant string
     * @param $boolActif bool
     * @param $libelleBanque string
     * @param $codeBanque string
     * @param $codeGuichet string
     * @param $numeroCompte string
     * @param $cleSecurite string
     * @param $genre string
     * @param $situationFamiliale string
     * @param $numeroCarteSejour string
     * @param $dateFinSejour int
     * @param $nombreEnfant int
     * @param $boolAutoEntrepreneuse bool
     * @param $depNaissanceDUE string
     * @param $numeroIBAN string
     * @param $codeBIC string
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant
     */
    public static function create(PDO $pdo,$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,$dateNaissance,$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,$dateFinSejour,$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC,$lazyload=true)
    {
        // Ajouter le/la intervenant dans la base de données
        $pdoStatement = $pdo->prepare('INSERT INTO '.Intervenant::TABLENAME.' ('.Intervenant::FIELDNAME_NOMINTERVENANT.','.Intervenant::FIELDNAME_NOMINTERVENANT_JF.','.Intervenant::FIELDNAME_PRENOMINTERVENANT.','.Intervenant::FIELDNAME_ADRESSEINTERVENANT_A.','.Intervenant::FIELDNAME_ADRESSEINTERVENANT_B.','.Intervenant::FIELDNAME_ADRESSEINTERVENANT_C.','.Intervenant::FIELDNAME_TELEPHONEINTERVENANT_A.','.Intervenant::FIELDNAME_TELEPHONEINTERVENANT_B.','.Intervenant::FIELDNAME_FAXINTERVENANT.','.Intervenant::FIELDNAME_MOBILEINTERVENANT.','.Intervenant::FIELDNAME_MAILINTERVENANT.','.Intervenant::FIELDNAME_NUMEROSS.','.Intervenant::FIELDNAME_DATENAISSANCE.','.Intervenant::FIELDNAME_VILLENAISSANCE.','.Intervenant::FIELDNAME_DEPARTEMENTNAISSANCE.','.Intervenant::FIELDNAME_PAYSNAISSANCE.','.Intervenant::FIELDNAME_NATIONALITE.','.Intervenant::FIELDNAME_COMMENTAIREINTERVENANT.','.Intervenant::FIELDNAME_CODEPOSTALINTERVENANT.','.Intervenant::FIELDNAME_VILLEINTERVENANT.','.Intervenant::FIELDNAME_BOOLACTIF.','.Intervenant::FIELDNAME_LIBELLEBANQUE.','.Intervenant::FIELDNAME_CODEBANQUE.','.Intervenant::FIELDNAME_CODEGUICHET.','.Intervenant::FIELDNAME_NUMEROCOMPTE.','.Intervenant::FIELDNAME_CLESECURITE.','.Intervenant::FIELDNAME_GENRE.','.Intervenant::FIELDNAME_SITUATIONFAMILIALE.','.Intervenant::FIELDNAME_NUMEROCARTESEJOUR.','.Intervenant::FIELDNAME_DATEFINSEJOUR.','.Intervenant::FIELDNAME_NOMBREENFANT.','.Intervenant::FIELDNAME_BOOLAUTOENTREPRENEUSE.','.Intervenant::FIELDNAME_DEPNAISSANCEDUE.','.Intervenant::FIELDNAME_NUMEROIBAN.','.Intervenant::FIELDNAME_CODEBIC.') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        if (!$pdoStatement->execute(array($nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,date('Y-m-d',$dateNaissance),$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,date('Y-m-d',$dateFinSejour),$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC))) {
            throw new Exception('Erreur durant l\'insertion d\'un(e) intervenant dans la base de données');
        }

        // Construire le/la intervenant
        return new Intervenant($pdo,intval($pdo->lastInsertId()),$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,$dateNaissance,$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,$dateFinSejour,$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC,$lazyload);
    }

    /**
     * Compter les intervenants
     * @param $pdo PDO
     * @return int Nombre de intervenants
     */
    public static function count(PDO $pdo)
    {
        if (!($pdoStatement = $pdo->query('SELECT COUNT('.Intervenant::FIELDNAME_IDINTERVENANT.') FROM '.Intervenant::TABLENAME))) {
            throw new Exception('Erreur lors du comptage des intervenants dans la base de données');
        }
        return $pdoStatement->fetchColumn();
    }

    /**
     * Requête de sélection
     * @param $pdo PDO
     * @param $where string|array
     * @param $orderby string|array
     * @param $limit string|array
     * @param $from string|array
     * @return PDOStatement
     */
    protected static function _select(PDO $pdo,$where=null,$orderby=null,$limit=null,$from=null)
    {
        return $pdo->prepare('SELECT DISTINCT '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_IDINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NOMINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NOMINTERVENANT_JF.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_PRENOMINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_ADRESSEINTERVENANT_A.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_ADRESSEINTERVENANT_B.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_ADRESSEINTERVENANT_C.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_TELEPHONEINTERVENANT_A.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_TELEPHONEINTERVENANT_B.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_FAXINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_MOBILEINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_MAILINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NUMEROSS.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_DATENAISSANCE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_VILLENAISSANCE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_DEPARTEMENTNAISSANCE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_PAYSNAISSANCE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NATIONALITE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_COMMENTAIREINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_CODEPOSTALINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_VILLEINTERVENANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_BOOLACTIF.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_LIBELLEBANQUE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_CODEBANQUE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_CODEGUICHET.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NUMEROCOMPTE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_CLESECURITE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_GENRE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_SITUATIONFAMILIALE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NUMEROCARTESEJOUR.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_DATEFINSEJOUR.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NOMBREENFANT.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_BOOLAUTOENTREPRENEUSE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_DEPNAISSANCEDUE.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_NUMEROIBAN.', '.Intervenant::TABLENAME.'.'.Intervenant::FIELDNAME_CODEBIC.' '.
            'FROM '.Intervenant::TABLENAME.($from != null ? ', '.(is_array($from) ? implode(', ',$from) : $from) : '').
            ($where != null ? ' WHERE '.(is_array($where) ? implode(' AND ',$where) : $where) : '').
            ($orderby != null ? ' ORDER BY '.(is_array($orderby) ? implode(', ',$orderby) : $orderby) : '').
            ($limit != null ? ' LIMIT '.(is_array($limit) ? implode(', ', $limit) : $limit) : ''));
    }

    /**
     * Charger un(e) intervenant
     * @param $pdo PDO
     * @param $idIntervenant int
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant
     */
    public static function load(PDO $pdo,$idIntervenant,$lazyload=true)
    {
        // Déjà chargé(e) ?
        if ($lazyload && isset(self::$_lazyload[$idIntervenant])) {
            return self::$_lazyload[$idIntervenant];
        }

        // Charger le/la intervenant
        $pdoStatement = self::_select($pdo,Intervenant::FIELDNAME_IDINTERVENANT.' = ?');
        if (!$pdoStatement->execute(array($idIntervenant))) {
            throw new Exception('Erreur lors du chargement d\'un(e) intervenant depuis la base de données');
        }

        // Récupérer le/la intervenant depuis le jeu de résultats
        return self::fetch($pdo,$pdoStatement,$lazyload);
    }

    /**
     * Recharger les données depuis la base de données
     */
    public function reload()
    {
        // Recharger les données
        $pdoStatement = self::_select($this->_pdo,Intervenant::FIELDNAME_IDINTERVENANT.' = ?');
        if (!$pdoStatement->execute(array($this->_idIntervenant))) {
            throw new Exception('Erreur durant le rechargement des données d\'un(e) intervenant depuis la base de données');
        }

        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idIntervenant,$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,$dateNaissance,$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,$dateFinSejour,$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC) = $values;

        // Sauvegarder les valeurs
        $this->_nomIntervenant = $nomIntervenant;
        $this->_nomIntervenant_JF = $nomIntervenant_JF;
        $this->_prenomIntervenant = $prenomIntervenant;
        $this->_adresseIntervenant_A = $adresseIntervenant_A;
        $this->_adresseIntervenant_B = $adresseIntervenant_B;
        $this->_adresseIntervenant_C = $adresseIntervenant_C;
        $this->_telephoneIntervenant_A = $telephoneIntervenant_A;
        $this->_telephoneIntervenant_B = $telephoneIntervenant_B;
        $this->_faxIntervenant = $faxIntervenant;
        $this->_mobileIntervenant = $mobileIntervenant;
        $this->_mailIntervenant = $mailIntervenant;
        $this->_numeroSS = $numeroSS;
        $this->_dateNaissance = $dateNaissance === null ? null : strtotime($dateNaissance);
        $this->_villeNaissance = $villeNaissance;
        $this->_departementNaissance = $departementNaissance;
        $this->_paysNaissance = $paysNaissance;
        $this->_nationalite = $nationalite;
        $this->_commentaireIntervenant = $commentaireIntervenant;
        $this->_codePostalIntervenant = $codePostalIntervenant;
        $this->_villeIntervenant = $villeIntervenant;
        $this->_boolActif = $boolActif;
        $this->_libelleBanque = $libelleBanque;
        $this->_codeBanque = $codeBanque;
        $this->_codeGuichet = $codeGuichet;
        $this->_numeroCompte = $numeroCompte;
        $this->_cleSecurite = $cleSecurite;
        $this->_genre = $genre;
        $this->_situationFamiliale = $situationFamiliale;
        $this->_numeroCarteSejour = $numeroCarteSejour;
        $this->_dateFinSejour = $dateFinSejour === null ? null : strtotime($dateFinSejour);
        $this->_nombreEnfant = $nombreEnfant;
        $this->_boolAutoEntrepreneuse = $boolAutoEntrepreneuse;
        $this->_depNaissanceDUE = $depNaissanceDUE;
        $this->_numeroIBAN = $numeroIBAN;
        $this->_codeBIC = $codeBIC;
    }

    /**
     * Charger tous/toutes les intervenants
     * @param $pdo PDO
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant[] Tableau de intervenants
     */
    public static function loadAll(PDO $pdo,$lazyload=true)
    {
        // Sélectionner tous/toutes les intervenants
        $pdoStatement = self::selectAll($pdo);

        // Récupèrer tous/toutes les intervenants
        $intervenants = self::fetchAll($pdo,$pdoStatement,$lazyload);

        // Retourner le tableau
        return $intervenants;
    }

    /**
     * Sélectionner tous/toutes les intervenants
     * @param $pdo PDO
     * @return PDOStatement
     */
    public static function selectAll(PDO $pdo, $order=null)
    {
        $pdoStatement = self::_select($pdo, null, $order);
        if (!$pdoStatement->execute()) {
            throw new Exception('Erreur lors du chargement de tous/toutes les intervenants depuis la base de données');
        }
        return $pdoStatement;
    }

    /**
     * Récupèrer le/la intervenant suivant(e) d'un jeu de résultats
     * @param $pdo PDO
     * @param $pdoStatement PDOStatement
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant
     */
    public static function fetch(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idIntervenant,$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,$dateNaissance,$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,$dateFinSejour,$nombreEnfant,$boolAutoEntrepreneuse,$depNaissanceDUE,$numeroIBAN,$codeBIC) = $values;

        // Construire le/la intervenant
        return $lazyload && isset(self::$_lazyload[intval($idIntervenant)]) ? self::$_lazyload[intval($idIntervenant)] :
            new Intervenant($pdo,intval($idIntervenant),$nomIntervenant,$nomIntervenant_JF,$prenomIntervenant,$adresseIntervenant_A,$adresseIntervenant_B,$adresseIntervenant_C,$telephoneIntervenant_A,$telephoneIntervenant_B,$faxIntervenant,$mobileIntervenant,$mailIntervenant,$numeroSS,strtotime($dateNaissance),$villeNaissance,$departementNaissance,$paysNaissance,$nationalite,$commentaireIntervenant,$codePostalIntervenant,$villeIntervenant,$boolActif ? true : false,$libelleBanque,$codeBanque,$codeGuichet,$numeroCompte,$cleSecurite,$genre,$situationFamiliale,$numeroCarteSejour,strtotime($dateFinSejour),intval($nombreEnfant),$boolAutoEntrepreneuse ? true : false,$depNaissanceDUE,$numeroIBAN,$codeBIC,$lazyload);
    }

    /**
     * Récupèrer tous/toutes les intervenants d'un jeu de résultats
     * @param $pdo PDO
     * @param $pdoStatement PDOStatement
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant[] Tableau de intervenants
     */
    public static function fetchAll(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        $intervenants = array();
        while ($intervenant = self::fetch($pdo,$pdoStatement,$lazyload)) {
            $intervenants[] = $intervenant;
        }
        return $intervenants;
    }

    /**
     * Test d'égalité
     * @param $intervenant Intervenant
     * @return bool Les objets sont-ils égaux ?
     */
    public function equals($intervenant)
    {
        // Test si null
        if ($intervenant == null) { return false; }

        // Tester la classe
        if (!($intervenant instanceof Intervenant)) { return false; }

        // Tester les ids
        return $this->_idIntervenant == $intervenant->_idIntervenant;
    }

    /**
     * Vérifier que le/la intervenant existe en base de données
     * @return bool Le/La intervenant existe en base de données ?
     */
    public function exists()
    {
        $pdoStatement = $this->_pdo->prepare('SELECT COUNT('.Intervenant::FIELDNAME_IDINTERVENANT.') FROM '.Intervenant::TABLENAME.' WHERE '.Intervenant::FIELDNAME_IDINTERVENANT.' = ?');
        if (!$pdoStatement->execute(array($this->getIdIntervenant()))) {
            throw new Exception('Erreur lors de la vérification qu\'un(e) intervenant existe dans la base de données');
        }
        return $pdoStatement->fetchColumn() == 1;
    }

    /**
     * Supprimer le/la intervenant
     * @return bool Opération réussie ?
     */
    public function delete()
    {
        // Supprimer le/la intervenant
        $pdoStatement = $this->_pdo->prepare('DELETE FROM '.Intervenant::TABLENAME.' WHERE '.Intervenant::FIELDNAME_IDINTERVENANT.' = ?');
        if (!$pdoStatement->execute(array($this->getIdIntervenant()))) {
            throw new Exception('Erreur lors de la suppression d\'un(e) intervenant dans la base de données');
        }

        // Supprimer du tableau pour le chargement fainéant
        if (isset(self::$_lazyload[$this->_idIntervenant])) {
            unset(self::$_lazyload[$this->_idIntervenant]);
        }

        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }

    /**
     * Mettre à jour un champ dans la base de données
     * @param $fields array
     * @param $values array
     * @return bool Opération réussie ?
     */
    protected function _set($fields,$values)
    {
        // Préparer la mise à jour
        $updates = array();
        foreach ($fields as $field) {
            $updates[] = $field.' = ?';
        }

        // Mettre à jour le champ
        $pdoStatement = $this->_pdo->prepare('UPDATE '.Intervenant::TABLENAME.' SET '.implode(', ', $updates).' WHERE '.Intervenant::FIELDNAME_IDINTERVENANT.' = ?');
        if (!$pdoStatement->execute(array_merge($values,array($this->getIdIntervenant())))) {
            throw new Exception('Erreur lors de la mise à jour d\'un champ d\'un(e) intervenant dans la base de données');
        }

        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }

    /**
     * Mettre à jour tous les champs dans la base de données
     * @return bool Opération réussie ?
     */
    public function update()
    {
        return $this->_set(array(Intervenant::FIELDNAME_NOMINTERVENANT,Intervenant::FIELDNAME_NOMINTERVENANT_JF,Intervenant::FIELDNAME_PRENOMINTERVENANT,Intervenant::FIELDNAME_ADRESSEINTERVENANT_A,Intervenant::FIELDNAME_ADRESSEINTERVENANT_B,Intervenant::FIELDNAME_ADRESSEINTERVENANT_C,Intervenant::FIELDNAME_TELEPHONEINTERVENANT_A,Intervenant::FIELDNAME_TELEPHONEINTERVENANT_B,Intervenant::FIELDNAME_FAXINTERVENANT,Intervenant::FIELDNAME_MOBILEINTERVENANT,Intervenant::FIELDNAME_MAILINTERVENANT,Intervenant::FIELDNAME_NUMEROSS,Intervenant::FIELDNAME_DATENAISSANCE,Intervenant::FIELDNAME_VILLENAISSANCE,Intervenant::FIELDNAME_DEPARTEMENTNAISSANCE,Intervenant::FIELDNAME_PAYSNAISSANCE,Intervenant::FIELDNAME_NATIONALITE,Intervenant::FIELDNAME_COMMENTAIREINTERVENANT,Intervenant::FIELDNAME_CODEPOSTALINTERVENANT,Intervenant::FIELDNAME_VILLEINTERVENANT,Intervenant::FIELDNAME_BOOLACTIF,Intervenant::FIELDNAME_LIBELLEBANQUE,Intervenant::FIELDNAME_CODEBANQUE,Intervenant::FIELDNAME_CODEGUICHET,Intervenant::FIELDNAME_NUMEROCOMPTE,Intervenant::FIELDNAME_CLESECURITE,Intervenant::FIELDNAME_GENRE,Intervenant::FIELDNAME_SITUATIONFAMILIALE,Intervenant::FIELDNAME_NUMEROCARTESEJOUR,Intervenant::FIELDNAME_DATEFINSEJOUR,Intervenant::FIELDNAME_NOMBREENFANT,Intervenant::FIELDNAME_BOOLAUTOENTREPRENEUSE,Intervenant::FIELDNAME_DEPNAISSANCEDUE,Intervenant::FIELDNAME_NUMEROIBAN,Intervenant::FIELDNAME_CODEBIC),array($this->_nomIntervenant,$this->_nomIntervenant_JF,$this->_prenomIntervenant,$this->_adresseIntervenant_A,$this->_adresseIntervenant_B,$this->_adresseIntervenant_C,$this->_telephoneIntervenant_A,$this->_telephoneIntervenant_B,$this->_faxIntervenant,$this->_mobileIntervenant,$this->_mailIntervenant,$this->_numeroSS,date('Y-m-d',$this->_dateNaissance),$this->_villeNaissance,$this->_departementNaissance,$this->_paysNaissance,$this->_nationalite,$this->_commentaireIntervenant,$this->_codePostalIntervenant,$this->_villeIntervenant,$this->_boolActif,$this->_libelleBanque,$this->_codeBanque,$this->_codeGuichet,$this->_numeroCompte,$this->_cleSecurite,$this->_genre,$this->_situationFamiliale,$this->_numeroCarteSejour,date('Y-m-d',$this->_dateFinSejour),$this->_nombreEnfant,$this->_boolAutoEntrepreneuse,$this->_depNaissanceDUE,$this->_numeroIBAN,$this->_codeBIC));
    }

    /**
     * Récupérer le/la idIntervenant
     * @return int
     */
    public function getIdIntervenant()
    {
        return addCaracToString($this->_idIntervenant, 7, '0');
    }

    /**
     * Récupérer le/la nomIntervenant
     * @return string
     */
    public function getNomIntervenant()
    {
        return $this->_nomIntervenant;
    }

    /**
     * Définir le/la nomIntervenant
     * @param $nomIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNomIntervenant($nomIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_nomIntervenant = $nomIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NOMINTERVENANT),array($nomIntervenant)) : true;
    }

    /**
     * Récupérer le/la nomIntervenant_JF
     * @return string
     */
    public function getNomIntervenant_JF()
    {
        return $this->_nomIntervenant_JF;
    }

    /**
     * Définir le/la nomIntervenant_JF
     * @param $nomIntervenant_JF string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNomIntervenant_JF($nomIntervenant_JF,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_nomIntervenant_JF = $nomIntervenant_JF;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NOMINTERVENANT_JF),array($nomIntervenant_JF)) : true;
    }

    /**
     * Récupérer le/la prenomIntervenant
     * @return string
     */
    public function getPrenomIntervenant()
    {
        return $this->_prenomIntervenant;
    }

    /**
     * Définir le/la prenomIntervenant
     * @param $prenomIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setPrenomIntervenant($prenomIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_prenomIntervenant = $prenomIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_PRENOMINTERVENANT),array($prenomIntervenant)) : true;
    }

    /**
     * Récupérer le/la adresseIntervenant_A
     * @return string
     */
    public function getAdresseIntervenant_A()
    {
        return $this->_adresseIntervenant_A;
    }

    /**
     * Définir le/la adresseIntervenant_A
     * @param $adresseIntervenant_A string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setAdresseIntervenant_A($adresseIntervenant_A,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_adresseIntervenant_A = $adresseIntervenant_A;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_ADRESSEINTERVENANT_A),array($adresseIntervenant_A)) : true;
    }

    /**
     * Récupérer le/la adresseIntervenant_B
     * @return string
     */
    public function getAdresseIntervenant_B()
    {
        return $this->_adresseIntervenant_B;
    }

    /**
     * Définir le/la adresseIntervenant_B
     * @param $adresseIntervenant_B string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setAdresseIntervenant_B($adresseIntervenant_B,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_adresseIntervenant_B = $adresseIntervenant_B;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_ADRESSEINTERVENANT_B),array($adresseIntervenant_B)) : true;
    }

    /**
     * Récupérer le/la adresseIntervenant_C
     * @return string
     */
    public function getAdresseIntervenant_C()
    {
        return $this->_adresseIntervenant_C;
    }

    /**
     * Définir le/la adresseIntervenant_C
     * @param $adresseIntervenant_C string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setAdresseIntervenant_C($adresseIntervenant_C,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_adresseIntervenant_C = $adresseIntervenant_C;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_ADRESSEINTERVENANT_C),array($adresseIntervenant_C)) : true;
    }

    /**
     * Récupérer le/la telephoneIntervenant_A
     * @return string
     */
    public function getTelephoneIntervenant_A()
    {
        return $this->_telephoneIntervenant_A;
    }

    /**
     * Définir le/la telephoneIntervenant_A
     * @param $telephoneIntervenant_A string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setTelephoneIntervenant_A($telephoneIntervenant_A,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_telephoneIntervenant_A = $telephoneIntervenant_A;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_TELEPHONEINTERVENANT_A),array($telephoneIntervenant_A)) : true;
    }

    /**
     * Récupérer le/la telephoneIntervenant_B
     * @return string
     */
    public function getTelephoneIntervenant_B()
    {
        return $this->_telephoneIntervenant_B;
    }

    /**
     * Définir le/la telephoneIntervenant_B
     * @param $telephoneIntervenant_B string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setTelephoneIntervenant_B($telephoneIntervenant_B,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_telephoneIntervenant_B = $telephoneIntervenant_B;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_TELEPHONEINTERVENANT_B),array($telephoneIntervenant_B)) : true;
    }

    /**
     * Récupérer le/la faxIntervenant
     * @return string
     */
    public function getFaxIntervenant()
    {
        return $this->_faxIntervenant;
    }

    /**
     * Définir le/la faxIntervenant
     * @param $faxIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setFaxIntervenant($faxIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_faxIntervenant = $faxIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_FAXINTERVENANT),array($faxIntervenant)) : true;
    }

    /**
     * Récupérer le/la mobileIntervenant
     * @return string
     */
    public function getMobileIntervenant()
    {
        return $this->_mobileIntervenant;
    }

    /**
     * Définir le/la mobileIntervenant
     * @param $mobileIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setMobileIntervenant($mobileIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_mobileIntervenant = $mobileIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_MOBILEINTERVENANT),array($mobileIntervenant)) : true;
    }

    /**
     * Récupérer le/la mailIntervenant
     * @return string
     */
    public function getMailIntervenant()
    {
        return $this->_mailIntervenant;
    }

    /**
     * Définir le/la mailIntervenant
     * @param $mailIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setMailIntervenant($mailIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_mailIntervenant = $mailIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_MAILINTERVENANT),array($mailIntervenant)) : true;
    }

    /**
     * Récupérer le/la numeroSS
     * @return string
     */
    public function getNumeroSS()
    {
        return $this->_numeroSS;
    }

    /**
     * Définir le/la numeroSS
     * @param $numeroSS string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNumeroSS($numeroSS,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_numeroSS = $numeroSS;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NUMEROSS),array($numeroSS)) : true;
    }

    /**
     * Récupérer le/la dateNaissance
     * @return int
     */
    public function getDateNaissance()
    {
        return $this->_dateNaissance;
    }

    /**
     * Définir le/la dateNaissance
     * @param $dateNaissance int
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setDateNaissance($dateNaissance,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_dateNaissance = $dateNaissance;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_DATENAISSANCE),array(date('Y-m-d',$dateNaissance))) : true;
    }

    /**
     * Récupérer le/la villeNaissance
     * @return string
     */
    public function getVilleNaissance()
    {
        return $this->_villeNaissance;
    }

    /**
     * Définir le/la villeNaissance
     * @param $villeNaissance string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setVilleNaissance($villeNaissance,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_villeNaissance = $villeNaissance;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_VILLENAISSANCE),array($villeNaissance)) : true;
    }

    /**
     * Récupérer le/la departementNaissance
     * @return string
     */
    public function getDepartementNaissance()
    {
        return $this->_departementNaissance;
    }

    /**
     * Définir le/la departementNaissance
     * @param $departementNaissance string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setDepartementNaissance($departementNaissance,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_departementNaissance = $departementNaissance;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_DEPARTEMENTNAISSANCE),array($departementNaissance)) : true;
    }

    /**
     * Récupérer le/la paysNaissance
     * @return string
     */
    public function getPaysNaissance()
    {
        return $this->_paysNaissance;
    }

    /**
     * Définir le/la paysNaissance
     * @param $paysNaissance string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setPaysNaissance($paysNaissance,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_paysNaissance = $paysNaissance;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_PAYSNAISSANCE),array($paysNaissance)) : true;
    }

    /**
     * Récupérer le/la nationalite
     * @return string
     */
    public function getNationalite()
    {
        return $this->_nationalite;
    }

    /**
     * Définir le/la nationalite
     * @param $nationalite string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNationalite($nationalite,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_nationalite = $nationalite;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NATIONALITE),array($nationalite)) : true;
    }

    /**
     * Récupérer le/la commentaireIntervenant
     * @return string
     */
    public function getCommentaireIntervenant()
    {
        return $this->_commentaireIntervenant;
    }

    /**
     * Définir le/la commentaireIntervenant
     * @param $commentaireIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCommentaireIntervenant($commentaireIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_commentaireIntervenant = $commentaireIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_COMMENTAIREINTERVENANT),array($commentaireIntervenant)) : true;
    }

    /**
     * Récupérer le/la codePostalIntervenant
     * @return string
     */
    public function getCodePostalIntervenant()
    {
        return $this->_codePostalIntervenant;
    }

    /**
     * Définir le/la codePostalIntervenant
     * @param $codePostalIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCodePostalIntervenant($codePostalIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_codePostalIntervenant = $codePostalIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_CODEPOSTALINTERVENANT),array($codePostalIntervenant)) : true;
    }

    /**
     * Récupérer le/la villeIntervenant
     * @return string
     */
    public function getVilleIntervenant()
    {
        return $this->_villeIntervenant;
    }

    /**
     * Définir le/la villeIntervenant
     * @param $villeIntervenant string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setVilleIntervenant($villeIntervenant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_villeIntervenant = $villeIntervenant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_VILLEINTERVENANT),array($villeIntervenant)) : true;
    }

    /**
     * Récupérer le/la boolActif
     * @return bool
     */
    public function getBoolActif()
    {
        return $this->_boolActif;
    }

    /**
     * Définir le/la boolActif
     * @param $boolActif bool
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolActif($boolActif,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolActif = $boolActif;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_BOOLACTIF),array($boolActif)) : true;
    }

    /**
     * Récupérer le/la libelleBanque
     * @return string
     */
    public function getLibelleBanque()
    {
        return $this->_libelleBanque;
    }

    /**
     * Définir le/la libelleBanque
     * @param $libelleBanque string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setLibelleBanque($libelleBanque,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_libelleBanque = $libelleBanque;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_LIBELLEBANQUE),array($libelleBanque)) : true;
    }

    /**
     * Récupérer le/la codeBanque
     * @return string
     */
    public function getCodeBanque()
    {
        return $this->_codeBanque;
    }

    /**
     * Définir le/la codeBanque
     * @param $codeBanque string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCodeBanque($codeBanque,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_codeBanque = $codeBanque;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_CODEBANQUE),array($codeBanque)) : true;
    }

    /**
     * Récupérer le/la codeGuichet
     * @return string
     */
    public function getCodeGuichet()
    {
        return $this->_codeGuichet;
    }

    /**
     * Définir le/la codeGuichet
     * @param $codeGuichet string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCodeGuichet($codeGuichet,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_codeGuichet = $codeGuichet;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_CODEGUICHET),array($codeGuichet)) : true;
    }

    /**
     * Récupérer le/la numeroCompte
     * @return string
     */
    public function getNumeroCompte()
    {
        return $this->_numeroCompte;
    }

    /**
     * Définir le/la numeroCompte
     * @param $numeroCompte string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNumeroCompte($numeroCompte,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_numeroCompte = $numeroCompte;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NUMEROCOMPTE),array($numeroCompte)) : true;
    }

    /**
     * Récupérer le/la cleSecurite
     * @return string
     */
    public function getCleSecurite()
    {
        return $this->_cleSecurite;
    }

    /**
     * Définir le/la cleSecurite
     * @param $cleSecurite string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCleSecurite($cleSecurite,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_cleSecurite = $cleSecurite;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_CLESECURITE),array($cleSecurite)) : true;
    }

    /**
     * Récupérer le/la genre
     * @return string
     */
    public function getGenre()
    {
        return $this->_genre;
    }

    /**
     * Définir le/la genre
     * @param $genre string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setGenre($genre,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_genre = $genre;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_GENRE),array($genre)) : true;
    }

    /**
     * Récupérer le/la situationFamiliale
     * @return string
     */
    public function getSituationFamiliale()
    {
        return $this->_situationFamiliale;
    }

    /**
     * Définir le/la situationFamiliale
     * @param $situationFamiliale string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setSituationFamiliale($situationFamiliale,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_situationFamiliale = $situationFamiliale;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_SITUATIONFAMILIALE),array($situationFamiliale)) : true;
    }

    /**
     * Récupérer le/la numeroCarteSejour
     * @return string
     */
    public function getNumeroCarteSejour()
    {
        return $this->_numeroCarteSejour;
    }

    /**
     * Définir le/la numeroCarteSejour
     * @param $numeroCarteSejour string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNumeroCarteSejour($numeroCarteSejour,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_numeroCarteSejour = $numeroCarteSejour;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NUMEROCARTESEJOUR),array($numeroCarteSejour)) : true;
    }

    /**
     * Récupérer le/la dateFinSejour
     * @return int
     */
    public function getDateFinSejour()
    {
        return $this->_dateFinSejour;
    }

    /**
     * Définir le/la dateFinSejour
     * @param $dateFinSejour int
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setDateFinSejour($dateFinSejour,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_dateFinSejour = $dateFinSejour;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_DATEFINSEJOUR),array(date('Y-m-d',$dateFinSejour))) : true;
    }

    /**
     * Récupérer le/la nombreEnfant
     * @return int
     */
    public function getNombreEnfant()
    {
        return $this->_nombreEnfant;
    }

    /**
     * Définir le/la nombreEnfant
     * @param $nombreEnfant int
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNombreEnfant($nombreEnfant,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_nombreEnfant = $nombreEnfant;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NOMBREENFANT),array($nombreEnfant)) : true;
    }

    /**
     * Récupérer le/la boolAutoEntrepreneuse
     * @return bool
     */
    public function getBoolAutoEntrepreneuse()
    {
        return $this->_boolAutoEntrepreneuse;
    }

    /**
     * Définir le/la boolAutoEntrepreneuse
     * @param $boolAutoEntrepreneuse bool
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolAutoEntrepreneuse($boolAutoEntrepreneuse,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolAutoEntrepreneuse = $boolAutoEntrepreneuse;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_BOOLAUTOENTREPRENEUSE),array($boolAutoEntrepreneuse)) : true;
    }

    /**
     * Récupérer le/la depNaissanceDUE
     * @return string
     */
    public function getDepNaissanceDUE()
    {
        return $this->_depNaissanceDUE;
    }

    /**
     * Définir le/la depNaissanceDUE
     * @param $depNaissanceDUE string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setDepNaissanceDUE($depNaissanceDUE,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_depNaissanceDUE = $depNaissanceDUE;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_DEPNAISSANCEDUE),array($depNaissanceDUE)) : true;
    }

    /**
     * Récupérer le/la numeroIBAN
     * @return string
     */
    public function getNumeroIBAN()
    {
        return $this->_numeroIBAN;
    }

    /**
     * Définir le/la numeroIBAN
     * @param $numeroIBAN string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setNumeroIBAN($numeroIBAN,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_numeroIBAN = $numeroIBAN;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_NUMEROIBAN),array($numeroIBAN)) : true;
    }

    /**
     * Récupérer le/la codeBIC
     * @return string
     */
    public function getCodeBIC()
    {
        return $this->_codeBIC;
    }

    /**
     * Définir le/la codeBIC
     * @param $codeBIC string
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCodeBIC($codeBIC,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_codeBIC = $codeBIC;

        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Intervenant::_set(array(Intervenant::FIELDNAME_CODEBIC),array($codeBIC)) : true;
    }

    /**
     * ToString
     * @return string Représentation de intervenant sous la forme d'un string
     */
    public function __toString()
    {
        return '[Intervenant idIntervenant="'.$this->_idIntervenant.'" nomIntervenant="'.$this->_nomIntervenant.'" nomIntervenant_JF="'.$this->_nomIntervenant_JF.'" prenomIntervenant="'.$this->_prenomIntervenant.'" adresseIntervenant_A="'.$this->_adresseIntervenant_A.'" adresseIntervenant_B="'.$this->_adresseIntervenant_B.'" adresseIntervenant_C="'.$this->_adresseIntervenant_C.'" telephoneIntervenant_A="'.$this->_telephoneIntervenant_A.'" telephoneIntervenant_B="'.$this->_telephoneIntervenant_B.'" faxIntervenant="'.$this->_faxIntervenant.'" mobileIntervenant="'.$this->_mobileIntervenant.'" mailIntervenant="'.$this->_mailIntervenant.'" numeroSS="'.$this->_numeroSS.'" dateNaissance="'.date('d/m/Y',$this->_dateNaissance).'" villeNaissance="'.$this->_villeNaissance.'" departementNaissance="'.$this->_departementNaissance.'" paysNaissance="'.$this->_paysNaissance.'" nationalite="'.$this->_nationalite.'" commentaireIntervenant="'.$this->_commentaireIntervenant.'" codePostalIntervenant="'.$this->_codePostalIntervenant.'" villeIntervenant="'.$this->_villeIntervenant.'" boolActif="'.($this->_boolActif?'true':'false').'" libelleBanque="'.$this->_libelleBanque.'" codeBanque="'.$this->_codeBanque.'" codeGuichet="'.$this->_codeGuichet.'" numeroCompte="'.$this->_numeroCompte.'" cleSecurite="'.$this->_cleSecurite.'" genre="'.$this->_genre.'" situationFamiliale="'.$this->_situationFamiliale.'" numeroCarteSejour="'.$this->_numeroCarteSejour.'" dateFinSejour="'.date('d/m/Y',$this->_dateFinSejour).'" nombreEnfant="'.$this->_nombreEnfant.'" boolAutoEntrepreneuse="'.($this->_boolAutoEntrepreneuse?'true':'false').'" depNaissanceDUE="'.$this->_depNaissanceDUE.'" numeroIBAN="'.$this->_numeroIBAN.'" codeBIC="'.$this->_codeBIC.'"]';
    }
    /**
     * Sérialiser
     * @param $serialize bool Activer la sérialisation ?
     * @return string Sérialisation du/de la intervenant
     */
    public function serialize($serialize=true)
    {
        // Sérialiser le/la intervenant
        $array = array('idintervenant' => $this->_idIntervenant,'nomintervenant' => $this->_nomIntervenant,'nomintervenant_jf' => $this->_nomIntervenant_JF,'prenomintervenant' => $this->_prenomIntervenant,'adresseintervenant_a' => $this->_adresseIntervenant_A,'adresseintervenant_b' => $this->_adresseIntervenant_B,'adresseintervenant_c' => $this->_adresseIntervenant_C,'telephoneintervenant_a' => $this->_telephoneIntervenant_A,'telephoneintervenant_b' => $this->_telephoneIntervenant_B,'faxintervenant' => $this->_faxIntervenant,'mobileintervenant' => $this->_mobileIntervenant,'mailintervenant' => $this->_mailIntervenant,'numeross' => $this->_numeroSS,'datenaissance' => $this->_dateNaissance,'villenaissance' => $this->_villeNaissance,'departementnaissance' => $this->_departementNaissance,'paysnaissance' => $this->_paysNaissance,'nationalite' => $this->_nationalite,'commentaireintervenant' => $this->_commentaireIntervenant,'codepostalintervenant' => $this->_codePostalIntervenant,'villeintervenant' => $this->_villeIntervenant,'boolactif' => $this->_boolActif,'libellebanque' => $this->_libelleBanque,'codebanque' => $this->_codeBanque,'codeguichet' => $this->_codeGuichet,'numerocompte' => $this->_numeroCompte,'clesecurite' => $this->_cleSecurite,'genre' => $this->_genre,'situationfamiliale' => $this->_situationFamiliale,'numerocartesejour' => $this->_numeroCarteSejour,'datefinsejour' => $this->_dateFinSejour,'nombreenfant' => $this->_nombreEnfant,'boolautoentrepreneuse' => $this->_boolAutoEntrepreneuse,'depnaissancedue' => $this->_depNaissanceDUE,'numeroiban' => $this->_numeroIBAN,'codebic' => $this->_codeBIC);

        // Retourner la sérialisation (ou pas) du/de la intervenant
        return $array;
    }

    /**
     * Désérialiser
     * @param $pdo PDO
     * @param $string string Sérialisation du/de la intervenant
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Intervenant
     */
    public static function unserialize(PDO $pdo,$string,$lazyload=true)
    {
        // Désérialiser la chaine de caractères
        $array = unserialize($string);

        // Construire le/la intervenant
        return $lazyload && isset(self::$_lazyload[$array['idintervenant']]) ? self::$_lazyload[$array['idintervenant']] :
            new Intervenant($pdo,$array['idintervenant'],$array['nomintervenant'],$array['nomintervenant_jf'],$array['prenomintervenant'],$array['adresseintervenant_a'],$array['adresseintervenant_b'],$array['adresseintervenant_c'],$array['telephoneintervenant_a'],$array['telephoneintervenant_b'],$array['faxintervenant'],$array['mobileintervenant'],$array['mailintervenant'],$array['numeross'],$array['datenaissance'],$array['villenaissance'],$array['departementnaissance'],$array['paysnaissance'],$array['nationalite'],$array['commentaireintervenant'],$array['codepostalintervenant'],$array['villeintervenant'],$array['boolactif'],$array['libellebanque'],$array['codebanque'],$array['codeguichet'],$array['numerocompte'],$array['clesecurite'],$array['genre'],$array['situationfamiliale'],$array['numerocartesejour'],$array['datefinsejour'],$array['nombreenfant'],$array['boolautoentrepreneuse'],$array['depnaissancedue'],$array['numeroiban'],$array['codebic'],$lazyload);
    }

}