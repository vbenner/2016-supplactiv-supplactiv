<?php
/** -----------------------------------------------------------------------------------------------
 * @name Sous_Module
 * @version 03/01/2014 (dd/mm/yyyy)
 * @author Kevin MAURICE - Page Up (http://www.pageup.fr)
 *
 * © Modification Vincent BENNER / Création d'une notion d'autorisation ou non suivant le
 * profil utilisateur
 *
 */
class Sous_Module
{
    // Nom de la table
    const TABLENAME = 'du_sous_module';
    
    // Nom des champs
    const FIELDNAME_IDSOUS_MODULE = 'idSousModule';
    const FIELDNAME_LIBELLESOUSMODULE = 'libelleSousModule';
    const FIELDNAME_ORDRESOUSMODULE = 'ordreSousModule';
    const FIELDNAME_CHEMINSOUSMODULE = 'cheminSousModule';
    const FIELDNAME_TYPENOTIFICATION = 'typeNotification';
    const FIELDNAME_FONCTIONNOTIFICATION = 'fonctionNotification';
    const FIELDNAME_BOOLACTIF = 'boolActif';
    const FIELDNAME_MODULE_IDMODULE = 'FK_idModule';
    
    /** @var PDO  */
    protected $_pdo;
    
    /** @var array tableau pour le chargement fainéant */
    protected static $_lazyload;
    
    /** @var int  */
    protected $_idSous_module;
    
    /** @var string  */
    protected $_libellesousmodule;
    
    /** @var int  */
    protected $_ordreSousmodule;
    
    /** @var string  */
    protected $_cheminsousmodule;
    
    /** @var string  */
    protected $_typeNotification;
    
    /** @var string  */
    protected $_fonctionnotification;
    
    /** @var int  */
    protected $_boolActif;
    
    /** @var int id de module */
    protected $_module;
    
    /**
     * Construire un(e) sous_module
     * @param $pdo PDO 
     * @param $idSous_module int 
     * @param $libellesousmodule string 
     * @param $cheminsousmodule string 
     * @param $typeNotification string 
     * @param $fonctionnotification string 
     * @param $ordreSousmodule int 
     * @param $boolActif int 
     * @param $module int Id de module
     * @param $lazyload bool Activer le chargement fainéant ?
     */
    protected function __construct(PDO $pdo,$idSous_module,$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule=0,$boolActif=0,$module=null,$lazyload=true)
    {
        // Sauvegarder pdo
        $this->_pdo = $pdo;
        
        // Sauvegarder les attributs
        $this->_idSous_module = $idSous_module;
        $this->_libellesousmodule = $libellesousmodule;
        $this->_cheminsousmodule = $cheminsousmodule;
        $this->_typeNotification = $typeNotification;
        $this->_fonctionnotification = $fonctionnotification;
        $this->_ordreSousmodule = $ordreSousmodule;
        $this->_boolActif = $boolActif;
        $this->_module = $module;
        
        // Sauvegarder pour le chargement fainéant
        if ($lazyload) {
            self::$_lazyload[$idSous_module] = $this;
        }
    }
    
    /**
     * Créer un(e) sous_module
     * @param $pdo PDO 
     * @param $libellesousmodule string 
     * @param $cheminsousmodule string 
     * @param $typeNotification string 
     * @param $fonctionnotification string 
     * @param $ordreSousmodule int 
     * @param $boolActif int 
     * @param $module Module 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module 
     */
    public static function create(PDO $pdo,$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule=0,$boolActif=0,$module=null,$lazyload=true)
    {
        // Ajouter le/la sous_module dans la base de données
        $pdoStatement = $pdo->prepare('INSERT INTO '.Sous_Module::TABLENAME.' ('.Sous_Module::FIELDNAME_LIBELLESOUSMODULE.','.Sous_Module::FIELDNAME_CHEMINSOUSMODULE.','.Sous_Module::FIELDNAME_TYPENOTIFICATION.','.Sous_Module::FIELDNAME_FONCTIONNOTIFICATION.','.Sous_Module::FIELDNAME_ORDRESOUSMODULE.','.Sous_Module::FIELDNAME_BOOLACTIF.','.Sous_Module::FIELDNAME_MODULE_IDMODULE.') VALUES (?,?,?,?,?,?,?)');
        if (!$pdoStatement->execute(array($libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule,$boolActif,$module == null ? null : $module->getIdModule()))) {
            throw new Exception('Erreur durant l\'insertion d\'un(e) sous_module dans la base de données');
        }
        
        // Construire le/la sous_module
        return new Sous_Module($pdo,intval($pdo->lastInsertId()),$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule,$boolActif,$module == null ? null : $module->getIdModule(),$lazyload);
    }
    
    /**
     * Compter les sous_modules
     * @param $pdo PDO 
     * @return int Nombre de sous_modules
     */
    public static function count(PDO $pdo)
    {
        if (!($pdoStatement = $pdo->query('SELECT COUNT('.Sous_Module::FIELDNAME_IDSOUS_MODULE.') FROM '.Sous_Module::TABLENAME))) {
            throw new Exception('Erreur lors du comptage des sous_modules dans la base de données');
        }
        return $pdoStatement->fetchColumn();
    }
    
    /**
     * Requte de sélection
     * @param $pdo PDO 
     * @param $where string|array 
     * @param $orderby string|array 
     * @param $limit string|array 
     * @param $from string|array 
     * @return PDOStatement 
     */
    protected static function _select(PDO $pdo,$where=null,$orderby=null,$limit=null,$from=null)
    {
        return $pdo->prepare('SELECT DISTINCT '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_IDSOUS_MODULE.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_LIBELLESOUSMODULE.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_CHEMINSOUSMODULE.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_TYPENOTIFICATION.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_FONCTIONNOTIFICATION.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_ORDRESOUSMODULE.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_BOOLACTIF.', '.Sous_Module::TABLENAME.'.'.Sous_Module::FIELDNAME_MODULE_IDMODULE.' '.
                             'FROM '.Sous_Module::TABLENAME.($from != null ? ', '.(is_array($from) ? implode(', ',$from) : $from) : '').
                             ($where != null ? ' WHERE '.(is_array($where) ? implode(' AND ',$where) : $where) : '').
                             ($orderby != null ? ' ORDER BY '.(is_array($orderby) ? implode(', ',$orderby) : $orderby) : '').
                             ($limit != null ? ' LIMIT '.(is_array($limit) ? implode(', ', $limit) : $limit) : ''));
    }
    
    /**
     * Charger un(e) sous_module
     * @param $pdo PDO 
     * @param $idSous_module int 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module 
     */
    public static function load(PDO $pdo,$idSous_module,$lazyload=true)
    {
        // Déjà chargé(e) ?
        if ($lazyload && isset(self::$_lazyload[$idSous_module])) {
            return self::$_lazyload[$idSous_module];
        }
        
        // Charger le/la sous_module
        $pdoStatement = self::_select($pdo,Sous_Module::FIELDNAME_IDSOUS_MODULE.' = ?');
        if (!$pdoStatement->execute(array($idSous_module))) {
            throw new Exception('Erreur lors du chargement d\'un(e) sous_module depuis la base de données');
        }
        
        // Récupérer le/la sous_module depuis le jeu de résultats
        return self::fetch($pdo,$pdoStatement,$lazyload);
    }
    
    /**
     * Charger un(e) sous_module par le chemin
     * @param $pdo PDO
     * @param $cheminSous_module string
     * @return Sous_Module
     */
    public static function loadByLink(PDO $pdo,$cheminSous_module)
    {
    	
    	// Charger le/la sous_module
    	$pdoStatement = self::_select($pdo,Sous_Module::FIELDNAME_CHEMINSOUSMODULE.' = ?');
    	if (!$pdoStatement->execute(array($cheminSous_module))) {
    		throw new Exception('Erreur lors du chargement d\'un(e) sous_module depuis la base de données');
    	}
    
    	// Récupérer le/la sous_module depuis le jeu de résultats
    	return self::fetch($pdo,$pdoStatement,true);
    }
    
    /**
     * Recharger les données depuis la base de données
     */
    public function reload()
    {
        // Recharger les données
        $pdoStatement = self::_select($this->_pdo,Sous_Module::FIELDNAME_IDSOUS_MODULE.' = ?');
        if (!$pdoStatement->execute(array($this->_idSous_module))) {
            throw new Exception('Erreur durant le rechargement des données d\'un(e) sous_module depuis la base de données');
        }
        
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idSous_module,$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule,$boolActif,$module) = $values;
        
        // Sauvegarder les valeurs
        $this->_libellesousmodule = $libellesousmodule;
        $this->_cheminsousmodule = $cheminsousmodule;
        $this->_typeNotification = $typeNotification;
        $this->_fonctionnotification = $fonctionnotification;
        $this->_ordreSousmodule = $ordreSousmodule;
        $this->_boolActif = $boolActif;
        $this->_module = $module;
    }
    
    /**
     * Charger tous/toutes les sous_modules
     * @param $pdo PDO 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module[] Tableau de sous_modules
     */
    public static function loadAll(PDO $pdo,$lazyload=true)
    {
        // Sélectionner tous/toutes les sous_modules
        $pdoStatement = self::selectAll($pdo);
        
        // Récupérer tous/toutes les sous_modules
        $sous_modules = self::fetchAll($pdo,$pdoStatement,$lazyload);
        
        // Retourner le tableau
        return $sous_modules;
    }
    
    /**
     * Sélectionner tous/toutes les sous_modules
     * @param $pdo PDO 
     * @return PDOStatement 
     */
    public static function selectAll(PDO $pdo)
    {
        $pdoStatement = self::_select($pdo);
        if (!$pdoStatement->execute()) {
            throw new Exception('Erreur lors du chargement de tous/toutes les sous_modules depuis la base de données');
        }
        return $pdoStatement;
    }
    
    /**
     * Récupérer le/la sous_module suivant(e) d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module 
     */
    public static function fetch(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idSous_module,$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,$ordreSousmodule,$boolActif,$module) = $values;
        
        // Construire le/la sous_module
        return $lazyload && isset(self::$_lazyload[intval($idSous_module)]) ? self::$_lazyload[intval($idSous_module)] :
               new Sous_Module($pdo,intval($idSous_module),$libellesousmodule,$cheminsousmodule,$typeNotification,$fonctionnotification,intval($ordreSousmodule),intval($boolActif),$module,$lazyload);
    }
    
    /**
     * Récupérer tous/toutes les sous_modules d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module[] Tableau de sous_modules
     */
    public static function fetchAll(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        $sous_modules = array();
        while ($sous_module = self::fetch($pdo,$pdoStatement,$lazyload)) {
            $sous_modules[] = $sous_module;
        }
        return $sous_modules;
    }
    
    /**
     * Test d'égalité
     * @param $sous_module Sous_Module 
     * @return bool Les objets sont-ils égaux ?
     */
    public function equals($sous_module)
    {
        // Test si null
        if ($sous_module == null) { return false; }
        
        // Tester la classe
        if (!($sous_module instanceof Sous_Module)) { return false; }
        
        // Tester les ids
        return $this->_idSous_module == $sous_module->_idSous_module;
    }
    
    /**
     * Vérifier que le/la sous_module existe en base de données
     * @return bool Le/La sous_module existe en base de données ?
     */
    public function exists()
    {
        $pdoStatement = $this->_pdo->prepare('SELECT COUNT('.Sous_Module::FIELDNAME_IDSOUS_MODULE.') FROM '.Sous_Module::TABLENAME.' WHERE '.Sous_Module::FIELDNAME_IDSOUS_MODULE.' = ?');
        if (!$pdoStatement->execute(array($this->getIdSous_module()))) {
            throw new Exception('Erreur lors de la vérification qu\'un(e) sous_module existe dans la base de données');
        }
        return $pdoStatement->fetchColumn() == 1;
    }
    
    /**
     * Supprimer le/la sous_module
     * @return bool Opération réussie ?
     */
    public function delete()
    {
        // Supprimer le/la sous_module
        $pdoStatement = $this->_pdo->prepare('DELETE FROM '.Sous_Module::TABLENAME.' WHERE '.Sous_Module::FIELDNAME_IDSOUS_MODULE.' = ?');
        if (!$pdoStatement->execute(array($this->getIdSous_module()))) {
            throw new Exception('Erreur lors de la suppression d\'un(e) sous_module dans la base de données');
        }
        
        // Supprimer du tableau pour le chargement fainéant
        if (isset(self::$_lazyload[$this->_idSous_module])) {
            unset(self::$_lazyload[$this->_idSous_module]);
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre à jour un champ dans la base de données
     * @param $fields array 
     * @param $values array 
     * @return bool Opération réussie ?
     */
    protected function _set($fields,$values)
    {
        // Préparer la mise à jour
        $updates = array();
        foreach ($fields as $field) {
            $updates[] = $field.' = ?';
        }
        
        // Mettre  jour le champ
        $pdoStatement = $this->_pdo->prepare('UPDATE '.Sous_Module::TABLENAME.' SET '.implode(', ', $updates).' WHERE '.Sous_Module::FIELDNAME_IDSOUS_MODULE.' = ?');
        if (!$pdoStatement->execute(array_merge($values,array($this->getIdSous_module())))) {
            throw new Exception('Erreur lors de la mise  jour d\'un champ d\'un(e) sous_module dans la base de données');
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre à jour tous les champs dans la base de données
     * @return bool Opération réussie ?
     */
    public function update()
    {
        return $this->_set(array(Sous_Module::FIELDNAME_LIBELLESOUSMODULE,Sous_Module::FIELDNAME_ORDRESOUSMODULE,Sous_Module::FIELDNAME_CHEMINSOUSMODULE,Sous_Module::FIELDNAME_TYPENOTIFICATION,Sous_Module::FIELDNAME_FONCTIONNOTIFICATION,Sous_Module::FIELDNAME_BOOLACTIF,Sous_Module::FIELDNAME_MODULE_IDMODULE),array($this->_libellesousmodule,$this->_ordreSousmodule,$this->_cheminsousmodule,$this->_typeNotification,$this->_fonctionnotification,$this->_boolActif,$this->_module));
    }
    
    /**
     * Récupérer le/la idSous_module
     * @return int 
     */
    public function getIdSous_module()
    {
        return $this->_idSous_module;
    }
    
    /**
     * Récupérer le/la libellesousmodule
     * @return string 
     */
    public function getLibellesousmodule()
    {
        return $this->_libellesousmodule;
    }
    
    /**
     * Définir le/la libellesousmodule
     * @param $libellesousmodule string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setLibellesousmodule($libellesousmodule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_libellesousmodule = $libellesousmodule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_LIBELLESOUSMODULE),array($libellesousmodule)) : true;
    }
    
    /**
     * Récupérer le/la ordreSousmodule
     * @return int 
     */
    public function getOrdreSousmodule()
    {
        return $this->_ordreSousmodule;
    }
    
    /**
     * Définir le/la ordreSousmodule
     * @param $ordreSousmodule int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setOrdreSousmodule($ordreSousmodule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_ordreSousmodule = $ordreSousmodule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_ORDRESOUSMODULE),array($ordreSousmodule)) : true;
    }
    
    /**
     * Récupérer le/la cheminsousmodule
     * @return string 
     */
    public function getCheminsousmodule()
    {
        return $this->_cheminsousmodule;
    }
    
    /**
     * Définir le/la cheminsousmodule
     * @param $cheminsousmodule string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCheminsousmodule($cheminsousmodule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_cheminsousmodule = $cheminsousmodule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_CHEMINSOUSMODULE),array($cheminsousmodule)) : true;
    }
    
    /**
     * Récupérer le/la typeNotification
     * @return string 
     */
    public function getTypeNotification()
    {
        return $this->_typeNotification;
    }
    
    /**
     * Définir le/la typeNotification
     * @param $typeNotification string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setTypeNotification($typeNotification,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_typeNotification = $typeNotification;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_TYPENOTIFICATION),array($typeNotification)) : true;
    }
    
    /**
     * Récupérer le/la fonctionnotification
     * @return string 
     */
    public function getFonctionnotification()
    {
        return $this->_fonctionnotification;
    }
    
    /**
     * Définir le/la fonctionnotification
     * @param $fonctionnotification string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setFonctionnotification($fonctionnotification,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_fonctionnotification = $fonctionnotification;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_FONCTIONNOTIFICATION),array($fonctionnotification)) : true;
    }
    
    /**
     * Récupérer le/la boolActif
     * @return int 
     */
    public function getBoolActif()
    {
        return $this->_boolActif;
    }
    
    /**
     * Définir le/la boolActif
     * @param $boolActif int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolActif($boolActif,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolActif = $boolActif;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_BOOLACTIF),array($boolActif)) : true;
    }
    
    /**
     * Récupérer le/la module
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module 
     */
    public function getModule($lazyload=true)
    {
        // Retourner null si nécessaire
        if ($this->_module === null) { return null; }
        
        // Charger et retourner module
        return Module::load($this->_pdo,$this->_module,$lazyload);
    }
    
    /**
     * Récupérer le/les id(s) du/de la module
     * @return int Id de module
     */
    public function getModuleId()
    {
        return $this->_module;
    }
    
    /**
     * Définir le/la module
     * @param $module Module 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setModule($module=null,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_module = $module == null ? null : $module->getIdModule();
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_MODULE_IDMODULE),array($module == null ? null : $module->getIdModule())) : true;
    }
    
    /**
     * Définir le/la module d'aprs son/ses id(s)
     * @param $idModule int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setModuleById($idModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_module = $idModule === null ? null : $idModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Sous_Module::_set(array(Sous_Module::FIELDNAME_MODULE_IDMODULE),array($idModule)) : true;
    }
    
    /**
     * Sélectionner les sous_modules par module
     * @param $pdo PDO 
     * @param $module Module 
     * @param $order
     * @return PDOStatement 
     */
    public static function selectByModule(PDO $pdo,Module $module, $order)
    {
        $pdoStatement = self::_select($pdo,Sous_Module::FIELDNAME_MODULE_IDMODULE.' = ?', $order);
        if (!$pdoStatement->execute(array($module->getIdModule()))) {
            throw new Exception('Erreur lors du chargement de tous/toutes les sous_modules d\'un(e) module depuis la base de données');
        }
        return $pdoStatement;
    }
    
    /**
     * ToString
     * @return string Représentation de sous_module sous la forme d'un string
     */
    public function __toString()
    {
        return '[Sous_Module idSous_module="'.$this->_idSous_module.'" libellesousmodule="'.$this->_libellesousmodule.'" ordreSousmodule="'.$this->_ordreSousmodule.'" cheminsousmodule="'.$this->_cheminsousmodule.'" typeNotification="'.$this->_typeNotification.'" fonctionnotification="'.$this->_fonctionnotification.'" boolActif="'.$this->_boolActif.'" module="'.$this->_module.'"]';
    }
    /**
     * Sérialiser
     * @param $serialize bool Activer la sérialisation ?
     * @return string Sérialisation du/de la sous_module
     */
    public function serialize($serialize=true)
    {
        // Sérialiser le/la sous_module
        $array = array('idsous_module' => $this->_idSous_module,'libellesousmodule' => $this->_libellesousmodule,'cheminsousmodule' => $this->_cheminsousmodule,'typenotification' => $this->_typeNotification,'fonctionnotification' => $this->_fonctionnotification,'ordresousmodule' => $this->_ordreSousmodule,'boolactif' => $this->_boolActif,'module' => $this->_module);
        
        // Retourner la sérialisation (ou pas) du/de la sous_module
        return $serialize ? serialize($array) : $array;
    }
    
    /**
     * Désérialiser
     * @param $pdo PDO 
     * @param $string string Sérialisation du/de la sous_module
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Sous_Module 
     */
    public static function unserialize(PDO $pdo,$string,$lazyload=true)
    {
        // Désérialiser la chaine de caractres
        $array = unserialize($string);
        
        // Construire le/la sous_module
        return $lazyload && isset(self::$_lazyload[$array['idsous_module']]) ? self::$_lazyload[$array['idsous_module']] :
               new Sous_Module($pdo,$array['idsous_module'],$array['libellesousmodule'],$array['cheminsousmodule'],$array['typenotification'],$array['fonctionnotification'],$array['ordresousmodule'],$array['boolactif'],$array['module'],$lazyload);
    }
    
}

/**
 * @name Module
 * @version 03/01/2014 (dd/mm/yyyy)
 * @author Kevin MAURICE - Page UP (http://www.pageup.fr/)
 */
class Module
{
    // Nom de la table
    const TABLENAME = 'du_module';
    
    // Nom des champs
    const FIELDNAME_IDMODULE = 'idModule';
    const FIELDNAME_LIBELLEMODULE = 'libelleModule';
    const FIELDNAME_LOGOMODULE = 'logoModule';
    const FIELDNAME_ORDREMODULE = 'ordreModule';
    const FIELDNAME_CHEMINMODULE = 'cheminModule';
    const FIELDNAME_BOOLACTIF = 'boolActif';
    const FIELDNAME_BOOLSOUSMODULE = 'boolSousModule';
    const FIELDNAME_BOOLACCORDEON = 'boolAccordeon';
    const FIELDNAME_LEVEL_MAX = 'levelMax';
    const FIELDNAME_BLOC_IDBLOC = 'FK_idBloc';

    /** @var PDO  */
    protected $_pdo;
    
    /** @var array tableau pour le chargement fainéant */
    protected static $_lazyload;
    
    /** @var int  */
    protected $_idModule;
    
    /** @var string  */
    protected $_libelleModule;
    
    /** @var string  */
    protected $_logoModule;
    
    /** @var int  */
    protected $_ordreModule;
    
    /** @var string  */
    protected $_cheminModule;
    
    /** @var int  */
    protected $_boolActif;
    
    /** @var int  */
    protected $_boolSousModule;
    
    /** @var int  */
    protected $_boolAccordeon;
    
    /** @var int  */
    protected $_levelMax;

    /** @var int id de bloc */
    protected $_bloc;
    
    /**
     * Construire un(e) module
     * @param $pdo PDO 
     * @param $idModule int 
     * @param $libelleModule string 
     * @param $logoModule string 
     * @param $cheminModule string 
     * @param $ordreModule int 
     * @param $boolActif int 
     * @param $boolSousModule int 
     * @param $boolAccordeon int 
     * @param $levelMax int
     * @param $bloc int Id de bloc
     * @param $lazyload bool Activer le chargement fainéant ?
     */
    protected function __construct(PDO $pdo,$idModule,$libelleModule,$logoModule,$cheminModule,$ordreModule=0,$boolActif=0,$boolSousModule=0,$boolAccordeon=0,$levelMax=0,$bloc=null,$lazyload=true)
    {
        // Sauvegarder pdo
        $this->_pdo = $pdo;
        
        // Sauvegarder les attributs
        $this->_idModule = $idModule;
        $this->_libelleModule = $libelleModule;
        $this->_logoModule = $logoModule;
        $this->_cheminModule = $cheminModule;
        $this->_ordreModule = $ordreModule;
        $this->_boolActif = $boolActif;
        $this->_boolSousModule = $boolSousModule;
        $this->_boolAccordeon = $boolAccordeon;
        $this->_levelMax = $levelMax;
        $this->_bloc = $bloc;
        
        // Sauvegarder pour le chargement fainéant
        if ($lazyload) {
            self::$_lazyload[$idModule] = $this;
        }
    }
    
    /**
     * Créer un(e) module
     * @param $pdo PDO 
     * @param $libelleModule string 
     * @param $logoModule string 
     * @param $cheminModule string 
     * @param $ordreModule int 
     * @param $boolActif int 
     * @param $boolSousModule int 
     * @param $boolAccordeon int 
     * @param $levelMax int
     * @param $bloc Bloc
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module 
     */
    public static function create(PDO $pdo,$libelleModule,$logoModule,$cheminModule,$ordreModule=0,$boolActif=0,$boolSousModule=0,$boolAccordeon=0,$levelMax=0,$bloc=null,$lazyload=true)
    {
        // Ajouter le/la module dans la base de données
        $pdoStatement = $pdo->prepare('INSERT INTO '.Module::TABLENAME.' ('.Module::FIELDNAME_LIBELLEMODULE.','.Module::FIELDNAME_LOGOMODULE.','.Module::FIELDNAME_CHEMINMODULE.','.Module::FIELDNAME_ORDREMODULE.','.Module::FIELDNAME_BOOLACTIF.','.Module::FIELDNAME_BOOLSOUSMODULE.','.Module::FIELDNAME_BOOLACCORDEON.','.Module::FIELDNAME_LEVEL_MAX.','.Module::FIELDNAME_BLOC_IDBLOC.') VALUES (?,?,?,?,?,?,?,?,?)');
        if (!$pdoStatement->execute(
            array($libelleModule,$logoModule,$cheminModule,$ordreModule,$boolActif,
                $boolSousModule,$boolAccordeon,$levelMax,
                $bloc == null ? null : $bloc->getIdBloc()))) {
            throw new Exception('Erreur durant l\'insertion d\'un(e) module dans la base de données');
        }
        
        // Construire le/la module
        return new Module($pdo,intval($pdo->lastInsertId()),$libelleModule,$logoModule,$cheminModule,
            $ordreModule,$boolActif,$boolSousModule,$boolAccordeon,
            $levelMax,$bloc == null ? null : $bloc->getIdBloc(),$lazyload);
    }
    
    /**
     * Compter les modules
     * @param $pdo PDO 
     * @return int Nombre de modules
     */
    public static function count(PDO $pdo)
    {
        if (!($pdoStatement = $pdo->query('SELECT COUNT('.Module::FIELDNAME_IDMODULE.') FROM '.Module::TABLENAME))) {
            throw new Exception('Erreur lors du comptage des modules dans la base de données');
        }
        return $pdoStatement->fetchColumn();
    }
    
    /**
     * Requte de sélection
     * @param $pdo PDO 
     * @param $where string|array 
     * @param $orderby string|array 
     * @param $limit string|array 
     * @param $from string|array 
     * @return PDOStatement 
     */
    protected static function _select(PDO $pdo,$where=null,$orderby=null,$limit=null,$from=null)
    {
        return $pdo->prepare(
            'SELECT DISTINCT '.Module::TABLENAME.'.'.Module::FIELDNAME_IDMODULE.', '.
                Module::TABLENAME.'.'.Module::FIELDNAME_LIBELLEMODULE.', '.
                Module::TABLENAME.'.'.Module::FIELDNAME_LOGOMODULE.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_CHEMINMODULE.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_ORDREMODULE.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_BOOLACTIF.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_BOOLSOUSMODULE.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_BOOLACCORDEON.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_LEVEL_MAX.', '.
            Module::TABLENAME.'.'.Module::FIELDNAME_BLOC_IDBLOC.' '.
                             'FROM '.Module::TABLENAME.($from != null ? ', '.(is_array($from) ? implode(', ',$from) : $from) : '').
                             ($where != null ? ' WHERE '.(is_array($where) ? implode(' AND ',$where) : $where) : '').
                             ($orderby != null ? ' ORDER BY '.(is_array($orderby) ? implode(', ',$orderby) : $orderby) : '').
                             ($limit != null ? ' LIMIT '.(is_array($limit) ? implode(', ', $limit) : $limit) : ''));
    }
    
    /**
     * Charger un(e) module
     * @param $pdo PDO 
     * @param $idModule int 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module 
     */
    public static function load(PDO $pdo,$idModule,$lazyload=true)
    {
        // Déjà chargé(e) ?
        if ($lazyload && isset(self::$_lazyload[$idModule])) {
            return self::$_lazyload[$idModule];
        }
        
        // Charger le/la module
        $pdoStatement = self::_select($pdo,Module::FIELDNAME_IDMODULE.' = ?');
        if (!$pdoStatement->execute(array($idModule))) {
            throw new Exception('Erreur lors du chargement d\'un(e) module depuis la base de données');
        }
        
        // Récupérer le/la module depuis le jeu de résultats
        return self::fetch($pdo,$pdoStatement,$lazyload);
    }
    
    /**
     * Charger un(e) module par le chemin
     * @param $pdo PDO
     * @param $cheminModule int
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module
     */
    public static function loadByLink(PDO $pdo,$cheminModule)
    {
    	
    
    	// Charger le/la module
    	$pdoStatement = self::_select($pdo,Module::FIELDNAME_CHEMINMODULE.' = ?');
    	if (!$pdoStatement->execute(array($cheminModule))) {
    		throw new Exception('Erreur lors du chargement d\'un(e) module depuis la base de données');
    	}
    
    	// Récupérer le/la module depuis le jeu de résultats
    	return self::fetch($pdo,$pdoStatement, true);
    }
    
    /**
     * Recharger les données depuis la base de données
     */
    public function reload()
    {
        // Recharger les données
        $pdoStatement = self::_select($this->_pdo,Module::FIELDNAME_IDMODULE.' = ?');
        if (!$pdoStatement->execute(array($this->_idModule))) {
            throw new Exception('Erreur durant le rechargement des données d\'un(e) module depuis la base de données');
        }
        
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idModule,$libelleModule,$logoModule,$cheminModule,$ordreModule,$boolActif,$boolSousModule,$boolAccordeon,$levelMax,$bloc) = $values;
        
        // Sauvegarder les valeurs
        $this->_libelleModule = $libelleModule;
        $this->_logoModule = $logoModule;
        $this->_cheminModule = $cheminModule;
        $this->_ordreModule = $ordreModule;
        $this->_boolActif = $boolActif;
        $this->_boolSousModule = $boolSousModule;
        $this->_boolAccordeon = $boolAccordeon;
        $this->_levelMax = $levelMax;
        $this->_bloc = $bloc;
    }
    
    /**
     * Charger tous/toutes les modules
     * @param $pdo PDO 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module[] Tableau de modules
     */
    public static function loadAll(PDO $pdo,$lazyload=true)
    {
        // Sélectionner tous/toutes les modules
        $pdoStatement = self::selectAll($pdo);
        
        // Récupérer tous/toutes les modules
        $modules = self::fetchAll($pdo,$pdoStatement,$lazyload);
        
        // Retourner le tableau
        return $modules;
    }
    
    /**
     * Sélectionner tous/toutes les modules
     * @param $pdo PDO 
     * @return PDOStatement 
     */
    public static function selectAll(PDO $pdo)
    {
        $pdoStatement = self::_select($pdo);
        if (!$pdoStatement->execute()) {
            throw new Exception('Erreur lors du chargement de tous/toutes les modules depuis la base de données');
        }
        return $pdoStatement;
    }
    
    /**
     * Récupérer le/la module suivant(e) d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module 
     */
    public static function fetch(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idModule,$libelleModule,$logoModule,$cheminModule,$ordreModule,$boolActif,$boolSousModule,$boolAccordeon,$levelMax,$bloc) = $values;
        
        // Construire le/la module
        return $lazyload && isset(self::$_lazyload[intval($idModule)]) ? self::$_lazyload[intval($idModule)] :
               new Module($pdo,intval($idModule),$libelleModule,$logoModule,
                   $cheminModule,intval($ordreModule),intval($boolActif),
                   intval($boolSousModule),intval($boolAccordeon),
                   intval($levelMax),$bloc,$lazyload);
    }
    
    /**
     * Récupérer tous/toutes les modules d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module[] Tableau de modules
     */
    public static function fetchAll(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        $modules = array();
        while ($module = self::fetch($pdo,$pdoStatement,$lazyload)) {
            $modules[] = $module;
        }
        return $modules;
    }
    
    /**
     * Test d'égalité
     * @param $module Module
     * @return bool Les objets sont-ils égaux ?
     */
    public function equals($module)
    {
        // Test si null
        if ($module == null) { return false; }
        
        // Tester la classe
        if (!($module instanceof Module)) { return false; }
        
        // Tester les ids
        return $this->_idModule == $module->_idModule;
    }
    
    /**
     * Vérifier que le/la module existe en base de données
     * @return bool Le/La module existe en base de données ?
     */
    public function exists()
    {
        $pdoStatement = $this->_pdo->prepare('SELECT COUNT('.Module::FIELDNAME_IDMODULE.') FROM '.Module::TABLENAME.' WHERE '.Module::FIELDNAME_IDMODULE.' = ?');
        if (!$pdoStatement->execute(array($this->getIdModule()))) {
            throw new Exception('Erreur lors de la vérification qu\'un(e) module existe dans la base de données');
        }
        return $pdoStatement->fetchColumn() == 1;
    }
    
    /**
     * Supprimer le/la module
     * @return bool Opération réussie ?
     */
    public function delete()
    {
        // Supprimer les sous_modules associé(e)s
        $select = $this->selectSous_modules();
        while ($sous_module = Sous_Module::fetch($this->_pdo,$select)) {
            $sous_module->setModule(null);
        }
        
        // Supprimer le/la module
        $pdoStatement = $this->_pdo->prepare('DELETE FROM '.Module::TABLENAME.' WHERE '.Module::FIELDNAME_IDMODULE.' = ?');
        if (!$pdoStatement->execute(array($this->getIdModule()))) {
            throw new Exception('Erreur lors de la suppression d\'un(e) module dans la base de données');
        }
        
        // Supprimer du tableau pour le chargement fainéant
        if (isset(self::$_lazyload[$this->_idModule])) {
            unset(self::$_lazyload[$this->_idModule]);
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre  jour un champ dans la base de données
     * @param $fields array 
     * @param $values array 
     * @return bool Opération réussie ?
     */
    protected function _set($fields,$values)
    {
        // Préparer la mise à jour
        $updates = array();
        foreach ($fields as $field) {
            $updates[] = $field.' = ?';
        }
        
        // Mettre  jour le champ
        $pdoStatement = $this->_pdo->prepare('UPDATE '.Module::TABLENAME.' SET '.implode(', ', $updates).' WHERE '.Module::FIELDNAME_IDMODULE.' = ?');
        if (!$pdoStatement->execute(array_merge($values,array($this->getIdModule())))) {
            throw new Exception('Erreur lors de la mise  jour d\'un champ d\'un(e) module dans la base de données');
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre  jour tous les champs dans la base de données
     * @return bool Opération réussie ?
     */
    public function update()
    {
        return $this->_set(array(Module::FIELDNAME_LIBELLEMODULE,Module::FIELDNAME_LOGOMODULE,
            Module::FIELDNAME_ORDREMODULE,Module::FIELDNAME_CHEMINMODULE,Module::FIELDNAME_BOOLACTIF,
            Module::FIELDNAME_BOOLSOUSMODULE,Module::FIELDNAME_BOOLACCORDEON,
            Module::FIELDNAME_LEVEL_MAX,Module::FIELDNAME_BLOC_IDBLOC),
            array(
                $this->_libelleModule,$this->_logoModule,$this->_ordreModule,
                $this->_cheminModule,$this->_boolActif,$this->_boolSousModule,
                $this->_boolAccordeon,$this->_levelMax,$this->_bloc));
    }
    
    /**
     * Récupérer le/la idModule
     * @return int 
     */
    public function getIdModule()
    {
        return $this->_idModule;
    }
    
    /**
     * Récupérer le/la libelleModule
     * @return string 
     */
    public function getLibelleModule()
    {
        return $this->_libelleModule;
    }
    
    /**
     * Définir le/la libelleModule
     * @param $libelleModule string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setLibelleModule($libelleModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_libelleModule = $libelleModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_LIBELLEMODULE),array($libelleModule)) : true;
    }
    
    /**
     * Récupérer le/la logoModule
     * @return string 
     */
    public function getLogoModule()
    {
        return $this->_logoModule;
    }
    
    /**
     * Définir le/la logoModule
     * @param $logoModule string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setLogoModule($logoModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_logoModule = $logoModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_LOGOMODULE),array($logoModule)) : true;
    }
    
    /**
     * Récupérer le/la ordreModule
     * @return int 
     */
    public function getOrdreModule()
    {
        return $this->_ordreModule;
    }
    
    /**
     * Définir le/la ordreModule
     * @param $ordreModule int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setOrdreModule($ordreModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_ordreModule = $ordreModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_ORDREMODULE),array($ordreModule)) : true;
    }
    
    /**
     * Récupérer le/la cheminModule
     * @return string 
     */
    public function getCheminModule()
    {
        return $this->_cheminModule;
    }
    
    /**
     * Définir le/la cheminModule
     * @param $cheminModule string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setCheminModule($cheminModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_cheminModule = $cheminModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_CHEMINMODULE),array($cheminModule)) : true;
    }

    /**
     * Récupérer le/la levelMex
     * @return int
     */
    public function getLevelMax()
    {
        return $this->_levelMax;
    }


    /**
     * Récupérer le/la boolActif
     * @return int 
     */
    public function getBoolActif()
    {
        return $this->_boolActif;
    }
    
    /**
     * Définir le/la boolActif
     * @param $boolActif int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolActif($boolActif,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolActif = $boolActif;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_BOOLACTIF),array($boolActif)) : true;
    }
    
    /**
     * Récupérer le/la boolSousModule
     * @return int 
     */
    public function getBoolSousModule()
    {
        return $this->_boolSousModule;
    }
    
    /**
     * Définir le/la boolSousModule
     * @param $boolSousModule int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolSousModule($boolSousModule,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolSousModule = $boolSousModule;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_BOOLSOUSMODULE),array($boolSousModule)) : true;
    }
    
    /**
     * Récupérer le/la boolAccordeon
     * @return int 
     */
    public function getBoolAccordeon()
    {
        return $this->_boolAccordeon;
    }
    
    /**
     * Définir le/la boolAccordeon
     * @param $boolAccordeon int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBoolAccordeon($boolAccordeon,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_boolAccordeon = $boolAccordeon;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_BOOLACCORDEON),array($boolAccordeon)) : true;
    }
    
    /**
     * Sélectionner les sous_modules
     * @return PDOStatement 
     */
    public function selectSous_modules()
    {
        return Sous_Module::selectByModule($this->_pdo,$this);
    }
    
    /**
     * Récupérer le/la bloc
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc 
     */
    public function getBloc($lazyload=true)
    {
        // Retourner null si nécessaire
        if ($this->_bloc === null) { return null; }
        
        // Charger et retourner bloc
        return Bloc::load($this->_pdo,$this->_bloc,$lazyload);
    }
    
    /**
     * Récupérer le/les id(s) du/de la bloc
     * @return int Id de bloc
     */
    public function getBlocId()
    {
        return $this->_bloc;
    }
    
    /**
     * Définir le/la bloc
     * @param $bloc Bloc 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBloc($bloc=null,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_bloc = $bloc == null ? null : $bloc->getIdBloc();
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_BLOC_IDBLOC),array($bloc == null ? null : $bloc->getIdBloc())) : true;
    }
    
    /**
     * Définir le/la bloc d'après son/ses id(s)
     * @param $idBloc int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setBlocById($idBloc,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_bloc = $idBloc === null ? null : $idBloc;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Module::_set(array(Module::FIELDNAME_BLOC_IDBLOC),array($idBloc)) : true;
    }
    
    /**
     * Sélectionner les modules par bloc
     * @param $pdo PDO 
     * @param $bloc Bloc 
     * @param $order colonne
     * @return PDOStatement 
     */
    public static function selectByBloc(PDO $pdo,Bloc $bloc, $order)
    {
        $pdoStatement = self::_select($pdo,Module::FIELDNAME_BLOC_IDBLOC.' = ?', $order);
        if (!$pdoStatement->execute(array($bloc->getIdBloc()))) {
            throw new Exception('Erreur lors du chargement de tous/toutes les modules d\'un(e) bloc depuis la base de données');
        }
        return $pdoStatement;
    }
    
    /**
     * ToString
     * @return string Représentation de module sous la forme d'un string
     */
    public function __toString()
    {
        return '[Module idModule="'.$this->_idModule.'" libelleModule="'.$this->_libelleModule.'" logoModule="'.$this->_logoModule.'" ordreModule="'.$this->_ordreModule.'" cheminModule="'.$this->_cheminModule.'" boolActif="'.$this->_boolActif.'" boolSousModule="'.$this->_boolSousModule.'" boolAccordeon="'.$this->_boolAccordeon.'" levelMax="'.$this->_levelMax.'" bloc="'.$this->_bloc.'"]';
    }
    /**
     * Sérialiser
     * @param $serialize bool Activer la sérialisation ?
     * @return string Sérialisation du/de la module
     */
    public function serialize($serialize=true)
    {
        // Sérialiser le/la module
        $array = array(
            'idmodule' => $this->_idModule,'libellemodule' => $this->_libelleModule,
            'logomodule' => $this->_logoModule,'cheminmodule' => $this->_cheminModule,
            'ordremodule' => $this->_ordreModule,'boolactif' => $this->_boolActif,
            'boolsousmodule' => $this->_boolSousModule,
            'boolaccordeon' => $this->_boolAccordeon,
            'levelMax' => $this->_levelMax,
            'bloc' => $this->_bloc);
        
        // Retourner la sérialisation (ou pas) du/de la module
        return $serialize ? serialize($array) : $array;
    }
    
    /**
     * Désérialiser
     * @param $pdo PDO 
     * @param $string string Sérialisation du/de la module
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Module 
     */
    public static function unserialize(PDO $pdo,$string,$lazyload=true)
    {
        // Désérialiser la chaine de caractres
        $array = unserialize($string);
        
        // Construire le/la module
        return $lazyload && isset(self::$_lazyload[$array['idmodule']]) ? self::$_lazyload[$array['idmodule']] :
               new Module($pdo,$array['idmodule'],$array['libellemodule'],
                   $array['logomodule'],$array['cheminmodule'],$array['ordremodule'],
                   $array['boolactif'],$array['boolsousmodule'],$array['boolaccordeon'],
                   $array['levelMax'],$array['bloc'],$lazyload);
    }
    
}

/**
 * @name Bloc
 * @version 03/01/2014 (dd/mm/yyyy)
 * @author Kevin MAURICE - Page UP (http://www.pageup.fr/)
 */
class Bloc
{
    // Nom de la table
    const TABLENAME = 'du_bloc';
    
    // Nom des champs
    const FIELDNAME_IDBLOC = 'idBloc';
    const FIELDNAME_LIBELLEBLOC = 'libelleBloc';
    const FIELDNAME_ORDREBLOC = 'ordreBloc';
    
    /** @var PDO  */
    protected $_pdo;
    
    /** @var array tableau pour le chargement fainéant */
    protected static $_lazyload;
    
    /** @var int  */
    protected $_idBloc;
    
    /** @var string  */
    protected $_libelleBloc;
    
    /** @var int  */
    protected $_ordreBloc;
    
    /**
     * Construire un(e) bloc
     * @param $pdo PDO 
     * @param $idBloc int 
     * @param $libelleBloc string 
     * @param $ordreBloc int 
     * @param $lazyload bool Activer le chargement fainéant ?
     */
    protected function __construct(PDO $pdo,$idBloc,$libelleBloc,$ordreBloc=0,$lazyload=true)
    {
        // Sauvegarder pdo
        $this->_pdo = $pdo;
        
        // Sauvegarder les attributs
        $this->_idBloc = $idBloc;
        $this->_libelleBloc = $libelleBloc;
        $this->_ordreBloc = $ordreBloc;
        
        // Sauvegarder pour le chargement fainéant
        if ($lazyload) {
            self::$_lazyload[$idBloc] = $this;
        }
    }
    
    /**
     * Créer un(e) bloc
     * @param $pdo PDO 
     * @param $libelleBloc string 
     * @param $ordreBloc int 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc 
     */
    public static function create(PDO $pdo,$libelleBloc,$ordreBloc=0,$lazyload=true)
    {
        // Ajouter le/la bloc dans la base de données
        $pdoStatement = $pdo->prepare('INSERT INTO '.Bloc::TABLENAME.' ('.Bloc::FIELDNAME_LIBELLEBLOC.','.Bloc::FIELDNAME_ORDREBLOC.') VALUES (?,?)');
        if (!$pdoStatement->execute(array($libelleBloc,$ordreBloc))) {
            throw new Exception('Erreur durant l\'insertion d\'un(e) bloc dans la base de données');
        }
        
        // Construire le/la bloc
        return new Bloc($pdo,intval($pdo->lastInsertId()),$libelleBloc,$ordreBloc,$lazyload);
    }
    
    /**
     * Compter les blocs
     * @param $pdo PDO 
     * @return int Nombre de blocs
     */
    public static function count(PDO $pdo)
    {
        if (!($pdoStatement = $pdo->query('SELECT COUNT('.Bloc::FIELDNAME_IDBLOC.') FROM '.Bloc::TABLENAME))) {
            throw new Exception('Erreur lors du comptage des blocs dans la base de données');
        }
        return $pdoStatement->fetchColumn();
    }
    
    /**
     * Requte de sélection
     * @param $pdo PDO 
     * @param $where string|array 
     * @param $orderby string|array 
     * @param $limit string|array 
     * @param $from string|array 
     * @return PDOStatement 
     */
    protected static function _select(PDO $pdo,$where=null,$orderby=null,$limit=null,$from=null)
    {
        return $pdo->prepare('SELECT DISTINCT '.Bloc::TABLENAME.'.'.Bloc::FIELDNAME_IDBLOC.', '.Bloc::TABLENAME.'.'.Bloc::FIELDNAME_LIBELLEBLOC.', '.Bloc::TABLENAME.'.'.Bloc::FIELDNAME_ORDREBLOC.' '.
                             'FROM '.Bloc::TABLENAME.($from != null ? ', '.(is_array($from) ? implode(', ',$from) : $from) : '').
                             ($where != null ? ' WHERE '.(is_array($where) ? implode(' AND ',$where) : $where) : '').
                             ($orderby != null ? ' ORDER BY '.(is_array($orderby) ? implode(', ',$orderby) : $orderby) : '').
                             ($limit != null ? ' LIMIT '.(is_array($limit) ? implode(', ', $limit) : $limit) : ''));
    }
    
    /**
     * Charger un(e) bloc
     * @param $pdo PDO 
     * @param $idBloc int 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc 
     */
    public static function load(PDO $pdo,$idBloc,$lazyload=true)
    {
        // Déjà chargé(e) ?
        if ($lazyload && isset(self::$_lazyload[$idBloc])) {
            return self::$_lazyload[$idBloc];
        }
        
        // Charger le/la bloc
        $pdoStatement = self::_select($pdo,Bloc::FIELDNAME_IDBLOC.' = ?');
        if (!$pdoStatement->execute(array($idBloc))) {
            throw new Exception('Erreur lors du chargement d\'un(e) bloc depuis la base de données');
        }
        
        // Récupérer le/la bloc depuis le jeu de résultats
        return self::fetch($pdo,$pdoStatement,$lazyload);
    }
    
    /**
     * Recharger les données depuis la base de données
     */
    public function reload()
    {
        // Recharger les données
        $pdoStatement = self::_select($this->_pdo,Bloc::FIELDNAME_IDBLOC.' = ?');
        if (!$pdoStatement->execute(array($this->_idBloc))) {
            throw new Exception('Erreur durant le rechargement des données d\'un(e) bloc depuis la base de données');
        }
        
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idBloc,$libelleBloc,$ordreBloc) = $values;
        
        // Sauvegarder les valeurs
        $this->_libelleBloc = $libelleBloc;
        $this->_ordreBloc = $ordreBloc;
    }
    
    /**
     * Charger tous/toutes les blocs
     * @param $pdo PDO 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc[] Tableau de blocs
     */
    public static function loadAll(PDO $pdo,$lazyload=true)
    {
        // Sélectionner tous/toutes les blocs
        $pdoStatement = self::selectAll($pdo);
        
        // Récupérer tous/toutes les blocs
        $blocs = self::fetchAll($pdo,$pdoStatement,$lazyload);
        
        // Retourner le tableau
        return $blocs;
    }
    
    /**
     * Sélectionner tous/toutes les blocs
     * @param $pdo PDO 
     * @param $orderby Colonne
     * @return PDOStatement 
     */
    public static function selectAll(PDO $pdo, $orderby=null)
    {
        $pdoStatement = self::_select($pdo, null, $orderby);
        if (!$pdoStatement->execute()) {
            throw new Exception('Erreur lors du chargement de tous/toutes les blocs depuis la base de données');
        }
        return $pdoStatement;
    }
    
    /**
     * Récupérer le/la bloc suivant(e) d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc 
     */
    public static function fetch(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        // Extraire les valeurs
        $values = $pdoStatement->fetch(PDO::FETCH_NUM);
        if (!$values) { return null; }
        list($idBloc,$libelleBloc,$ordreBloc) = $values;
        
        // Construire le/la bloc
        return $lazyload && isset(self::$_lazyload[intval($idBloc)]) ? self::$_lazyload[intval($idBloc)] :
               new Bloc($pdo,intval($idBloc),$libelleBloc,intval($ordreBloc),$lazyload);
    }
    
    /**
     * Récupérer tous/toutes les blocs d'un jeu de résultats
     * @param $pdo PDO 
     * @param $pdoStatement PDOStatement 
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc[] Tableau de blocs
     */
    public static function fetchAll(PDO $pdo,PDOStatement $pdoStatement,$lazyload=true)
    {
        $blocs = array();
        while ($bloc = self::fetch($pdo,$pdoStatement,$lazyload)) {
            $blocs[] = $bloc;
        }
        return $blocs;
    }
    
    /**
     * Test d'égalité
     * @param $bloc Bloc 
     * @return bool Les objets sont-ils égaux ?
     */
    public function equals($bloc)
    {
        // Test si null
        if ($bloc == null) { return false; }
        
        // Tester la classe
        if (!($bloc instanceof Bloc)) { return false; }
        
        // Tester les ids
        return $this->_idBloc == $bloc->_idBloc;
    }
    
    /**
     * Vérifier que le/la bloc existe en base de données
     * @return bool Le/La bloc existe en base de données ?
     */
    public function exists()
    {
        $pdoStatement = $this->_pdo->prepare('SELECT COUNT('.Bloc::FIELDNAME_IDBLOC.') FROM '.Bloc::TABLENAME.' WHERE '.Bloc::FIELDNAME_IDBLOC.' = ?');
        if (!$pdoStatement->execute(array($this->getIdBloc()))) {
            throw new Exception('Erreur lors de la vérification qu\'un(e) bloc existe dans la base de données');
        }
        return $pdoStatement->fetchColumn() == 1;
    }
    
    /**
     * Supprimer le/la bloc
     * @return bool Opération réussie ?
     */
    public function delete()
    {
        // Supprimer les modules associé(e)s
        $select = $this->selectModules();
        while ($module = Module::fetch($this->_pdo,$select)) {
            $module->setBloc(null);
        }
        
        // Supprimer le/la bloc
        $pdoStatement = $this->_pdo->prepare('DELETE FROM '.Bloc::TABLENAME.' WHERE '.Bloc::FIELDNAME_IDBLOC.' = ?');
        if (!$pdoStatement->execute(array($this->getIdBloc()))) {
            throw new Exception('Erreur lors de la suppression d\'un(e) bloc dans la base de données');
        }
        
        // Supprimer du tableau pour le chargement fainéant
        if (isset(self::$_lazyload[$this->_idBloc])) {
            unset(self::$_lazyload[$this->_idBloc]);
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre  jour un champ dans la base de données
     * @param $fields array 
     * @param $values array 
     * @return bool Opération réussie ?
     */
    protected function _set($fields,$values)
    {
        // Préparer la mise à jour
        $updates = array();
        foreach ($fields as $field) {
            $updates[] = $field.' = ?';
        }
        
        // Mettre  jour le champ
        $pdoStatement = $this->_pdo->prepare('UPDATE '.Bloc::TABLENAME.' SET '.implode(', ', $updates).' WHERE '.Bloc::FIELDNAME_IDBLOC.' = ?');
        if (!$pdoStatement->execute(array_merge($values,array($this->getIdBloc())))) {
            throw new Exception('Erreur lors de la mise  jour d\'un champ d\'un(e) bloc dans la base de données');
        }
        
        // Opération réussie ?
        return $pdoStatement->rowCount() == 1;
    }
    
    /**
     * Mettre  jour tous les champs dans la base de données
     * @return bool Opération réussie ?
     */
    public function update()
    {
        return $this->_set(array(Bloc::FIELDNAME_LIBELLEBLOC,Bloc::FIELDNAME_ORDREBLOC),array($this->_libelleBloc,$this->_ordreBloc));
    }
    
    /**
     * Récupérer le/la idBloc
     * @return int 
     */
    public function getIdBloc()
    {
        return $this->_idBloc;
    }
    
    /**
     * Récupérer le/la libelleBloc
     * @return string 
     */
    public function getLibelleBloc()
    {
        return $this->_libelleBloc;
    }

    /**
     * Définir le/la libelleBloc
     * @param $libelleBloc string 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setLibelleBloc($libelleBloc,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_libelleBloc = $libelleBloc;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Bloc::_set(array(Bloc::FIELDNAME_LIBELLEBLOC),array($libelleBloc)) : true;
    }
    
    /**
     * Récupérer le/la ordreBloc
     * @return int 
     */
    public function getOrdreBloc()
    {
        return $this->_ordreBloc;
    }
    
    /**
     * Définir le/la ordreBloc
     * @param $ordreBloc int 
     * @param $execute bool Exécuter la requête update ?
     * @return bool Opération réussie ?
     */
    public function setOrdreBloc($ordreBloc,$execute=true)
    {
        // Sauvegarder dans l'objet
        $this->_ordreBloc = $ordreBloc;
        
        // Sauvegarder dans la base de données (ou pas)
        return $execute ? Bloc::_set(array(Bloc::FIELDNAME_ORDREBLOC),array($ordreBloc)) : true;
    }
    
    /**
     * Sélectionner les modules
     * @return PDOStatement 
     */
    public function selectModules()
    {
        return Module::selectByBloc($this->_pdo,$this);
    }
    
    /**
     * ToString
     * @return string Représentation de bloc sous la forme d'un string
     */
    public function __toString()
    {
        return '[Bloc idBloc="'.$this->_idBloc.'" libelleBloc="'.$this->_libelleBloc.'" ordreBloc="'.$this->_ordreBloc.'"]';
    }
    /**
     * Sérialiser
     * @param $serialize bool Activer la sérialisation ?
     * @return string Sérialisation du/de la bloc
     */
    public function serialize($serialize=true)
    {
        // Sérialiser le/la bloc
        $array = array('idbloc' => $this->_idBloc,'libellebloc' => $this->_libelleBloc,'ordrebloc' => $this->_ordreBloc);
        
        // Retourner la sérialisation (ou pas) du/de la bloc
        return $serialize ? serialize($array) : $array;
    }
    
    /**
     * Désérialiser
     * @param $pdo PDO 
     * @param $string string Sérialisation du/de la bloc
     * @param $lazyload bool Activer le chargement fainéant ?
     * @return Bloc 
     */
    public static function unserialize(PDO $pdo,$string,$lazyload=true)
    {
        // Désérialiser la chaine de caractères
        $array = unserialize($string);
        
        // Construire le/la bloc
        return $lazyload && isset(self::$_lazyload[$array['idbloc']]) ? self::$_lazyload[$array['idbloc']] :
               new Bloc($pdo,$array['idbloc'],$array['libellebloc'],$array['ordrebloc'],$lazyload);
    } 
}
