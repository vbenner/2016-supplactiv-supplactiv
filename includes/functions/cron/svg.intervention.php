<?php

/** -----------------------------------------------------------------------------------------------
 * Connection à la base de données
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
set_time_limit(0);
error_reporting(E_ALL);


require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** ---------------------------------------------------------------------------------------
 * On récupère la liste de toutes les tables
 * (ici uniquement su_intervention)
 */
$table = 'su_intervention';
$return = '';
$time = time();

/** -----------------------------------------------------------------------------------------------
 * Et on sauvegarde table / table
 */

    /** -------------------------------------------------------------------------------------------
     * Extraction de TOUTES les lignes
     */
    $resultExec = DbConnexion::getInstance()->prepare('SELECT * FROM ' . $table);
    $resultExec->execute();

    /** -------------------------------------------------------------------------------------------
     * On analyse les champs
     */
    $column = array();
    $type = array();
    $num_fields = $resultExec->columnCount();
    for ($counter = 0; $counter < $num_fields; $counter++) {
        $meta = $resultExec->getColumnMeta($counter);
        $column[] = $meta[ 'name' ];
        $type[] = $meta[ 'native_type' ];
    }

    /** -------------------------------------------------------------------------------------------
     * Entête du DUMP
     */
    $return .= 'DROP TABLE ' . $table . ';';

    /** -------------------------------------------------------------------------------------------
     * Entête du DUMP
     */
    $resultExec2 = DbConnexion::getInstance()->prepare('SHOW CREATE TABLE ' . $table);
    $resultExec2->execute();

    $row2 = $resultExec2->fetch(PDO::FETCH_ASSOC);
    $return .= "\n\n" . $row2[ 'Create Table' ] . ";\n\n";

    #for ($i = 0; $i < $num_fields; $i++)
    #{
    $resultExec->execute();
    while ($row = $resultExec->fetch(PDO::FETCH_OBJ)) {

        $return .= 'INSERT INTO ' . $table . ' VALUES(';
        for ($j = 0; $j < $num_fields; $j++) {

            $row->{$column[$j]} = addslashes($row->{$column[$j]});
            $row->{$column[$j]} = preg_replace("/\n/", "\\n", $row->{$column[$j]});
            if ($type[$j] == 'DATETIME') {
                $return .= '"' . $row->{$column[$j]} . '"';
            }
            else {
                $return .= $row->{$column[$j]};
            }
            if ($j < ($num_fields - 1)) {
                $return .= ',';
            }
        }
        $return .= ");\n";
    }
    $return .= "\n\n\n";


/** ---------------------------------------------------------------------------------------
 * Pour toutes les tables...
 */
$file = 'db-backup-' . $time . '-' . $table . '.sql';
$handle = fopen($file, 'w+');
fwrite($handle, $return);
fclose($handle);

if (gzCompressFile($file)) {
    unlink($file);
    die ('Fin Traitement');
}
else {
    die('Zip error');
}

function gzCompressFile($source, $level = 9){
    $dest = $source . '.gz';
    $mode = 'wb' . $level;
    $error = false;
    if ($fp_out = gzopen($dest, $mode)) {
        if ($fp_in = fopen($source,'rb')) {
            while (!feof($fp_in))
                gzwrite($fp_out, fread($fp_in, 1024 * 512));
            fclose($fp_in);
        } else {
            $error = true;
        }
        gzclose($fp_out);
    } else {
        $error = true;
    }
    if ($error)
        return false;
    else
        return $dest;
}
