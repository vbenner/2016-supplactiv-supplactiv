<?php

/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER / PAGE UP
 * @detail Suppression d'un PDV
 */
ini_set('display_errors', 'on');

/** -----------------------------------------------------------------------------------------------
 * Connexion a la base de donnees
 */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Fichier SQL
 */
$sqlSupprSSCat = '
DELETE FROM su_pdv_sous_categorie 
WHERE FK_idCategorie = :FK_idCategorie
AND idSousCategorie = :idSousCategorie
';
$supprSSCatExec = DbConnexion::getInstance()->prepare($sqlSupprSSCat);

/** -----------------------------------------------------------------------------------------------
 * On test la presence du POST
 */
if(
    filter_has_var(INPUT_POST, 'idSSCAT') &&
    filter_has_var(INPUT_POST, 'idCAT')
) {
echo $sqlSupprSSCat;
    $supprSSCatExec->bindValue(':FK_idCategorie', filter_input(INPUT_POST, 'idCAT'));
    $supprSSCatExec->bindValue(':idSousCategorie', filter_input(INPUT_POST, 'idSSCAT'));
    $supprSSCatExec->execute();
    print json_encode(array(
        'result' => ($supprSSCatExec->execute()) ? 1 : 0
    ));
}