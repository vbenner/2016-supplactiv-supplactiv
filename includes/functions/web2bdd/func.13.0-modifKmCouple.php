<?php
/**
 * Ajout / Modification de couple PDV / intervenant
 * @detail Ajout du Kilometrage entre un PDV et un intervenant
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idPdv') && filter_has_var(INPUT_POST, 'idIntervenant')) {

    /** Ajout / Modification DES KM */
    $InsertionKmPdvIntervenantExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'), PDO::PARAM_INT);
    $InsertionKmPdvIntervenantExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
    $InsertionKmPdvIntervenantExc->bindValue(':nbKm', filter_input(INPUT_POST, 'nbKm'), PDO::PARAM_INT);

    /** Afffichage du resultats */
    print json_encode(array('result' => ($InsertionKmPdvIntervenantExc->execute()) ? 1 : 0));
}