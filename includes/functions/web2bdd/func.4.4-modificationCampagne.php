<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Modification des informations sur la campagne
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idCampagne' )) {
    $sqlModificationInformationCampagne = '
    UPDATE su_campagne SET
      libelleCampagne = :libelleCampagne,
      descriptionCampagne = :descriptionCampagne,
      dateFin = :dateFin,
      FK_idInterlocuteurClient = :interlocuteurClient,
      FK_idInterlocuteurAgence = :interlocuteurAgence,
      tarifAnimation = :tarifa,
      tarifMarchandising = :tarifm,
      tarifFormation = :tariff,
      tarifFormation2 = :tariff2,
      tarifFormation3 = :tariff3,
      FK_idCampagnePrec = :oldCampagne
      WHERE idCampagne = :idCampagne';
    $ModificationInformationCampagneExc = DbConnexion::getInstance()->prepare($sqlModificationInformationCampagne);

    $oldCampagne = filter_input(INPUT_POST, 'oldCampagne') != '' ?
        filter_input(INPUT_POST, 'oldCampagne') : 0;
    $ModificationInformationCampagneExc->bindValue(':libelleCampagne', filter_input(INPUT_POST, 'libelleCampagne'));
    $ModificationInformationCampagneExc->bindValue(':descriptionCampagne', filter_input(INPUT_POST, 'commentaireCampagne'));
    $ModificationInformationCampagneExc->bindValue(':dateFin', filter_input(INPUT_POST, 'dateFinCampagne'));
    $ModificationInformationCampagneExc->bindValue(':interlocuteurClient', filter_input(INPUT_POST, 'interlocuteurClient'));
    $ModificationInformationCampagneExc->bindValue(':interlocuteurAgence', filter_input(INPUT_POST, 'interlocuteurAgence'));
    $ModificationInformationCampagneExc->bindValue(':idCampagne', filter_input(INPUT_POST, 'idCampagne'));
    $ModificationInformationCampagneExc->bindValue(':oldCampagne', $oldCampagne);

    $ModificationInformationCampagneExc->bindValue(':tarifa', filter_input(INPUT_POST, 'tarifA'));
    $ModificationInformationCampagneExc->bindValue(':tarifm', filter_input(INPUT_POST, 'tarifM'));
    $ModificationInformationCampagneExc->bindValue(':tariff', filter_input(INPUT_POST, 'tarifF'));

    $ModificationInformationCampagneExc->bindValue(':tariff2', filter_input(INPUT_POST, 'tarifF2'));
    $ModificationInformationCampagneExc->bindValue(':tariff3', filter_input(INPUT_POST, 'tarifF3'));
    print json_encode(array('result' => ($ModificationInformationCampagneExc->execute()) ? 1 : 0));
}