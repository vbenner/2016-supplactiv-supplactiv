<?php

/** ---------------------------------------------------------------------------------------------
 * Modification (uniquement) du flag ACTIF
 *
 * @author Vincent BENNER - Page UP
 * @copyright Page UP
 */

/** ---------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** ---------------------------------------------------------------------------------------------
 * Requete sql
 */
$sqlModificationFlagActif = '
UPDATE su_intervenant SET
  boolActif = NOT (boolActif)  
WHERE idIntervenant = :idIntervenant
';

$sqlGetNewFlagValue = '
SELECT boolActif
FROM su_intervenant  
WHERE idIntervenant = :idIntervenant
';
$modificationFlagActifExec = DbConnexion::getInstance()->prepare($sqlModificationFlagActif);
$getNewFlagValueExec = DbConnexion::getInstance()->prepare($sqlGetNewFlagValue);

/** -----------------------------------------------------------------------------------------------
 * Clôture
 */
$sqlModificationFlagClos = '
UPDATE su_intervenant SET
  boolCLOS = NOT (boolCLOS)  
WHERE idIntervenant = :idIntervenant
';

$sqlGetNewFlagClosValue = '
SELECT boolCLOS
FROM su_intervenant  
WHERE idIntervenant = :idIntervenant
';
$modificationFlagClosExec = DbConnexion::getInstance()->prepare($sqlModificationFlagClos);
$getNewFlagClosValueExec = DbConnexion::getInstance()->prepare($sqlGetNewFlagClosValue);

if(filter_has_var(INPUT_POST, 'boolActif')){
    $modificationFlagActifExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $modificationFlagActifExec->execute();

    $getNewFlagValueExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $getNewFlagValueExec->execute();
    $getNewFlagValue = $getNewFlagValueExec->fetch(PDO::FETCH_OBJ);


    print json_encode(array(
        'result' => $getNewFlagValue->boolActif
    ));
}

if(filter_has_var(INPUT_POST, 'boolCLOS')){
    $modificationFlagClosExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $modificationFlagClosExec->execute();

    $getNewFlagClosValueExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $getNewFlagClosValueExec->execute();
    $getNewFlagClosValue = $getNewFlagClosValueExec->fetch(PDO::FETCH_OBJ);


    print json_encode(array(
        'result' => $getNewFlagClosValue->boolCLOS
    ));
}