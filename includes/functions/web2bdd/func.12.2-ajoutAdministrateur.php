<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'un administrateur
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

if(filter_has_var(INPUT_POST, 'ztEmailAdministrateur')){
    $passUtilisateur = generePass(8);

    $AjoutAdministrateurAgenceExc->bindValue(':emailUtilisateur', filter_input(INPUT_POST, 'ztEmailAdministrateur'));
    $AjoutAdministrateurAgenceExc->bindValue(':nomUtilisateur', filter_input(INPUT_POST, 'ztNomAdministrateur'));
    $AjoutAdministrateurAgenceExc->bindValue(':prenomUtilisateur', filter_input(INPUT_POST, 'ztPrenomAdministrateur'));
    $AjoutAdministrateurAgenceExc->bindValue(':passUtilisateur', md5($passUtilisateur));
    $AjoutAdministrateurAgenceExc->bindValue(':roleUtilisateur', filter_input(INPUT_POST, 'zsRole'));

    $AjoutAdministrateurAgenceExc->bindValue(':ville', filter_input(INPUT_POST, 'ztVille'));
    $AjoutAdministrateurAgenceExc->bindValue(':cp', filter_input(INPUT_POST, 'ztCP'));
    $AjoutAdministrateurAgenceExc->bindValue(':adresseA', filter_input(INPUT_POST, 'ztAdresseA'));
    $AjoutAdministrateurAgenceExc->bindValue(':adresseB', filter_input(INPUT_POST, 'ztAdresseB'));

    print json_encode(array(
        'result' => ($AjoutAdministrateurAgenceExc->execute()) ? 1 : $AjoutAdministrateurAgenceExc->errorInfo()
    ));
}

