<?php
/**
 * Modification d' un client
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';


/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idClient')) {

    // Si l'idClient est different de 0 il s'agit d'une modification

    if(filter_input(INPUT_POST, 'idClient') != 0) {

        $dateCreation = filter_input(INPUT_POST, 'dateCreation');
        $tmp = explode('/', $dateCreation);
        $ModificationClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient'));
        $ModificationClientExc->bindValue(':idSiret', filter_input(INPUT_POST, 'idSiret'));
        $ModificationClientExc->bindValue(':libelleClient', filter_input(INPUT_POST, 'libelleClient'));
        $ModificationClientExc->bindValue(':telClient', filter_input(INPUT_POST, 'telClient'));
        $ModificationClientExc->bindValue(':faxClient', filter_input(INPUT_POST, 'faxClient'));
        $ModificationClientExc->bindValue(':emailClient', filter_input(INPUT_POST, 'emailClient'));
        $ModificationClientExc->bindValue(':commentaireClient', filter_input(INPUT_POST, 'commentaireClient'));
        $ModificationClientExc->bindValue(':adresseClient', filter_input(INPUT_POST, 'adresseClient'));
        $ModificationClientExc->bindValue(':codePostalClient', filter_input(INPUT_POST, 'codePostalClient'));
        $ModificationClientExc->bindValue(':villeClient', filter_input(INPUT_POST, 'villeClient'));
        $ModificationClientExc->bindValue(':destinataireFacturationClient', filter_input(INPUT_POST, 'destinataireFacturationClient'));
        $ModificationClientExc->bindValue(':adresseFacturationClient', filter_input(INPUT_POST, 'adresseFacturationClient'));
        $ModificationClientExc->bindValue(':codePostalFacturationClient', filter_input(INPUT_POST, 'codePostalFacturationClient'));
        $ModificationClientExc->bindValue(':villeFacturationClient', filter_input(INPUT_POST, 'villeFacturationClient'));
        $ModificationClientExc->bindValue(':FK_idTypeActivite', filter_input(INPUT_POST, 'idTypeActivite'));
        $ModificationClientExc->bindValue(':FK_idFiliere', filter_input(INPUT_POST, 'idFiliere'));
        $ModificationClientExc->bindValue(':dateCreation', $tmp[2].'-'.$tmp[1].'-'.$tmp[0]);

        if (!$ModificationClientExc->execute()) {
            print_r($ModificationClientExc->errorInfo());
        }
        print json_encode(array(
            'result' => ($ModificationClientExc->execute()) ? 1 : $ModificationClientExc->errorInfo()
        ));
    }else {
        $AjoutClientExc->bindValue(':idSiret', filter_input(INPUT_POST, 'idSiret'));
        $AjoutClientExc->bindValue(':libelleClient', filter_input(INPUT_POST, 'libelleClient'));
        $AjoutClientExc->bindValue(':telClient', filter_input(INPUT_POST, 'telClient'));
        $AjoutClientExc->bindValue(':faxClient', filter_input(INPUT_POST, 'faxClient'));
        $AjoutClientExc->bindValue(':emailClient', filter_input(INPUT_POST, 'emailClient'));
        $AjoutClientExc->bindValue(':commentaireClient', filter_input(INPUT_POST, 'commentaireClient'));
        $AjoutClientExc->bindValue(':adresseClient', filter_input(INPUT_POST, 'adresseClient'));
        $AjoutClientExc->bindValue(':codePostalClient', filter_input(INPUT_POST, 'codePostalClient'));
        $AjoutClientExc->bindValue(':villeClient', filter_input(INPUT_POST, 'villeClient'));
        $AjoutClientExc->bindValue(':destinataireFacturationClient', filter_input(INPUT_POST, 'destinataireFacturationClient'));
        $AjoutClientExc->bindValue(':adresseFacturationClient', filter_input(INPUT_POST, 'adresseFacturationClient'));
        $AjoutClientExc->bindValue(':codePostalFacturationClient', filter_input(INPUT_POST, 'codePostalFacturationClient'));
        $AjoutClientExc->bindValue(':villeFacturationClient', filter_input(INPUT_POST, 'villeFacturationClient'));
        $AjoutClientExc->bindValue(':FK_idTypeActivite', filter_input(INPUT_POST, 'idTypeActivite'));
        $AjoutClientExc->bindValue(':FK_idFiliere', filter_input(INPUT_POST, 'idFiliere'));

        $AjoutClientExc->bindValue(':dateCreation', date('Y-m-d'), PDO::PARAM_STR);

        print json_encode(array(
            'result' => ($AjoutClientExc->execute()) ? DbConnexion::getInstance()->lastInsertId() : 0
        ));
    }

}

