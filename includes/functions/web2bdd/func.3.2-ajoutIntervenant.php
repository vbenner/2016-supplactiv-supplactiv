<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

#echo '<pre>';
#print_r($_FILES);
#echo '</pre>';
#echo '- - - - - - - - - - - - -'.'<br/>';
#echo '<pre>';
#print_r($_POST);
#echo '</pre>';
#die();

/** On test la presence du POST */
if (filter_has_var(INPUT_POST, 'info')) {

    /** On parse le POST */
    parse_str(filter_input(INPUT_POST, 'info'), $infoIntervenant);
    $infoIntervenant = (object)$infoIntervenant;

    /** @var $dateNaissance On test la saisie de la date de naissance */
    $dateNaissance = DateTime::createFromFormat('d/m/Y', $infoIntervenant->ztDateNaissance);
    $dateFinSejour = DateTime::createFromFormat('d/m/Y', $infoIntervenant->ztDateExpiration);
    $dateContrat = DateTime::createFromFormat('d/m/Y', $infoIntervenant->ztDateContratCadre);
    $dateUrssaf = DateTime::createFromFormat('d/m/Y', $infoIntervenant->ztDateUrssaf);

    #die('>>'.$infoIntervenant->ztDateCreation);

    /** -------------------------------------------------------------------------------------------
     * Modification / Creation
     */
    if (filter_input(INPUT_POST, 'ztDateCreation') != '') {
        $dateCreation = DateTime::createFromFormat('d/m/Y', filter_input(INPUT_POST, 'ztDateCreation'));
        $dateModification = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));
        $userModification = $_SESSION ['idUtilisateur'.PROJECT_NAME];
    }
    else {
        $dateCreation = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));
        $dateModification = $dateCreation;
        $userModification = $_SESSION ['idUtilisateur'.PROJECT_NAME];
    }

#print_r($dateCreation);

    /** Verification des variables obligatoires */
    if (!empty($infoIntervenant->ztNom) != '' && !empty($infoIntervenant->ztPrenom) && is_object($dateNaissance) && !empty($infoIntervenant->ztVilleNaissance) && !empty($infoIntervenant->ztNumSecuSocial)) {

        if (filter_has_var(INPUT_POST, 'idIntervenant'))
        {
            $idIntervenant = filter_input(INPUT_POST, 'idIntervenant');

            $ModificationIntervenant2Exc->bindValue(':idIntervenant', $idIntervenant);
            $ModificationIntervenant2Exc->bindValue(':nomIntervenant', $infoIntervenant->ztNom);
            $ModificationIntervenant2Exc->bindValue(':nomIntervenant_JF', $infoIntervenant->ztNomJF);
            $ModificationIntervenant2Exc->bindValue(':prenomIntervenant', $infoIntervenant->ztPrenom);
            $ModificationIntervenant2Exc->bindValue(':adresseIntervenant_A', $infoIntervenant->ztAdresseA);
            $ModificationIntervenant2Exc->bindValue(':adresseIntervenant_B', $infoIntervenant->ztAdresseB);
            $ModificationIntervenant2Exc->bindValue(':adresseIntervenant_C', $infoIntervenant->ztAdresseC);
            $ModificationIntervenant2Exc->bindValue(':telephoneIntervenant_A', $infoIntervenant->ztTelFixe);
            $ModificationIntervenant2Exc->bindValue(':telephoneIntervenant_B', $infoIntervenant->ztAutreTel);
            $ModificationIntervenant2Exc->bindValue(':faxIntervenant', $infoIntervenant->ztFax);
            $ModificationIntervenant2Exc->bindValue(':mobileIntervenant', $infoIntervenant->ztTelMobile);
            $ModificationIntervenant2Exc->bindValue(':mailIntervenant', $infoIntervenant->ztEmail);
            $ModificationIntervenant2Exc->bindValue(':numeroSS', $infoIntervenant->ztNumSecuSocial);
            $ModificationIntervenant2Exc->bindValue(':niveau', $infoIntervenant->zsNiveau);
            $ModificationIntervenant2Exc->bindValue(':siret', $infoIntervenant->ztNumSiret);

            $ModificationIntervenant2Exc->bindValue(':dateNaissance', (is_object($dateNaissance)) ? $dateNaissance->format('Y-m-d') : '');

            $ModificationIntervenant2Exc->bindValue(':villeNaissance', $infoIntervenant->ztVilleNaissance);
            $ModificationIntervenant2Exc->bindValue(':departementNaissance', $infoIntervenant->zsPaysNaissance);
            $ModificationIntervenant2Exc->bindValue(':paysNaissance', $infoIntervenant->zsPaysNaissance);
            $ModificationIntervenant2Exc->bindValue(':nationalite', $infoIntervenant->zsNationalite);
            $ModificationIntervenant2Exc->bindValue(':commentaireIntervenant', $infoIntervenant->ztCommentaire);
            $ModificationIntervenant2Exc->bindValue(':codePostalIntervenant', $infoIntervenant->ztCodePostal);
            $ModificationIntervenant2Exc->bindValue(':villeIntervenant', $infoIntervenant->ztVille);
            $ModificationIntervenant2Exc->bindValue(':latitude', $infoIntervenant->ztLatitude);
            $ModificationIntervenant2Exc->bindValue(':longitude', $infoIntervenant->ztLongitude);
            $ModificationIntervenant2Exc->bindValue(':genre', $infoIntervenant->zsGenre);
            $ModificationIntervenant2Exc->bindValue(':situationFamiliale', $infoIntervenant->zsSituation);
            $ModificationIntervenant2Exc->bindValue(':numeroCarteSejour', $infoIntervenant->ztCarteSejour);

            $ModificationIntervenant2Exc->bindValue(':dateFinSejour', (is_object($dateFinSejour)) ? $dateFinSejour->format('Y-m-d') : '');
            $ModificationIntervenant2Exc->bindValue(':dateContrat', (is_object($dateContrat)) ? $dateContrat->format('Y-m-d') : '');
            $ModificationIntervenant2Exc->bindValue(':dateUrssaf', (is_object($dateUrssaf)) ? $dateUrssaf->format('Y-m-d') : '');

            /** -----------------------------------------------------------------------------------
             * Modification des dates
             */
            $ModificationIntervenant2Exc->bindValue(':dateCreation', (is_object($dateCreation)) ? $dateCreation->format('Y-m-d') : '');
            $ModificationIntervenant2Exc->bindValue(':dateModification', (is_object($dateModification)) ? $dateModification->format('Y-m-d') : '');
            $ModificationIntervenant2Exc->bindValue(':idUtilisateur', $userModification);

            $ModificationIntervenant2Exc->bindValue(':nombreEnfant', $infoIntervenant->zsNbEnfant);
            $ModificationIntervenant2Exc->bindValue(':boolAutoEntrepreneuse', $infoIntervenant->zsAutoEntrepreneuse);
            $ModificationIntervenant2Exc->bindValue(':depNaissanceDUE', $infoIntervenant->zsDepartementNaissance);
            $ModificationIntervenant2Exc->bindValue(':numeroIBAN', $infoIntervenant->ztIban);
            $ModificationIntervenant2Exc->bindValue(':codeBIC', $infoIntervenant->ztCodeBic);

            $ModificationIntervenant2Exc->bindValue(':libelleBanque', $infoIntervenant->ztAdresseBanque);
            $ModificationIntervenant2Exc->bindValue(':codeBanque', $infoIntervenant->ztCodeBanque);
            $ModificationIntervenant2Exc->bindValue(':codeGuichet', $infoIntervenant->ztCodeGuichet);
            $ModificationIntervenant2Exc->bindValue(':numCompte', $infoIntervenant->ztNumCompte);
            $ModificationIntervenant2Exc->bindValue(':cleSecurite', $infoIntervenant->ztCle);

            if ($ModificationIntervenant2Exc->execute()) {

                $SuppressionCompetenceExc->bindValue(':idIntervenant', $idIntervenant);
                $SuppressionCompetenceExc->execute();

                $SuppressionDepartementExc->bindValue(':idIntervenant', $idIntervenant);
                $SuppressionDepartementExc->execute();

                $SuppressionExperienceExc->bindValue(':idIntervenant', $idIntervenant);
                $SuppressionExperienceExc->execute();

                $SuppressionQualificationExc->bindValue(':idIntervenant', $idIntervenant);
                $SuppressionQualificationExc->execute();

                foreach ($_POST['lstCompetence'] as $idCompetence) {
                    $AjoutCompetenceExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutCompetenceExc->bindValue(':id', $idCompetence);
                    $AjoutCompetenceExc->execute();
                }

                foreach ($_POST['lstDept'] as $idDepartement) {
                    $AjoutDepartementExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutDepartementExc->bindValue(':id', $idDepartement);
                    $AjoutDepartementExc->execute();
                }

                foreach ($_POST['lstExperience'] as $idExperience) {
                    $AjoutExperienceExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutExperienceExc->bindValue(':id', $idExperience);
                    $AjoutExperienceExc->execute();
                }

                foreach ($_POST['lstQualification'] as $idQualification) {
                    $AjoutQualificationExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutQualificationExc->bindValue(':id', $idQualification);
                    $AjoutQualificationExc->execute();
                }

                /** -------------------------------------------------------------------------------
                 * Qui qu'a modifié ?
                 */
                $sqlGetUser = '
                ';
                print json_encode(array(
                    'result' => $idIntervenant,
                    'dteModification' => $dateModification->format('d/m/Y'),
                    'userModification' => $_SESSION['prenomUtilisateur'.PROJECT_NAME].' '.$_SESSION['nomUtilisateur'.PROJECT_NAME],
                ));
            } else {

                print json_encode(array(
                    'result' => 0,
                    'info' => json_encode($ModificationIntervenant2Exc->errorInfo()),

                    'dteModification' => $dateModification->format('d/m/Y'),
                    'userModification' => $_SESSION['prenomUtilisateur'.PROJECT_NAME].' '.$_SESSION['nomUtilisateur'.PROJECT_NAME],

                ));
            }


        }
        else {
            $AjoutIntervenantExc->bindValue(':nomIntervenant', $infoIntervenant->ztNom);
            $AjoutIntervenantExc->bindValue(':nomIntervenant_JF', $infoIntervenant->ztNomJF);
            $AjoutIntervenantExc->bindValue(':prenomIntervenant', $infoIntervenant->ztPrenom);
            $AjoutIntervenantExc->bindValue(':adresseIntervenant_A', $infoIntervenant->ztAdresseA);
            $AjoutIntervenantExc->bindValue(':adresseIntervenant_B', $infoIntervenant->ztAdresseB);
            $AjoutIntervenantExc->bindValue(':adresseIntervenant_C', $infoIntervenant->ztAdresseC);
            $AjoutIntervenantExc->bindValue(':telephoneIntervenant_A', $infoIntervenant->ztTelFixe);
            $AjoutIntervenantExc->bindValue(':telephoneIntervenant_B', $infoIntervenant->ztAutreTel);
            $AjoutIntervenantExc->bindValue(':faxIntervenant', $infoIntervenant->ztFax);
            $AjoutIntervenantExc->bindValue(':mobileIntervenant', $infoIntervenant->ztTelMobile);
            $AjoutIntervenantExc->bindValue(':mailIntervenant', $infoIntervenant->ztEmail);
            $AjoutIntervenantExc->bindValue(':numeroSS', $infoIntervenant->ztNumSecuSocial);

            $AjoutIntervenantExc->bindValue(':dateNaissance', (is_object($dateNaissance)) ? $dateNaissance->format('Y-m-d') : '');

            $AjoutIntervenantExc->bindValue(':villeNaissance', $infoIntervenant->ztVilleNaissance);
            $AjoutIntervenantExc->bindValue(':departementNaissance', $infoIntervenant->zsPaysNaissance);
            $AjoutIntervenantExc->bindValue(':paysNaissance', $infoIntervenant->zsPaysNaissance);
            $AjoutIntervenantExc->bindValue(':nationalite', $infoIntervenant->zsNationalite);
            $AjoutIntervenantExc->bindValue(':commentaireIntervenant', $infoIntervenant->ztCommentaire);
            $AjoutIntervenantExc->bindValue(':codePostalIntervenant', $infoIntervenant->ztCodePostal);
            $AjoutIntervenantExc->bindValue(':villeIntervenant', $infoIntervenant->ztVille);
            $AjoutIntervenantExc->bindValue(':genre', $infoIntervenant->zsGenre);
            $AjoutIntervenantExc->bindValue(':situationFamiliale', $infoIntervenant->zsSituation);
            $AjoutIntervenantExc->bindValue(':numeroCarteSejour', $infoIntervenant->ztCarteSejour);

            $AjoutIntervenantExc->bindValue(':dateFinSejour', (is_object($dateFinSejour)) ? $dateFinSejour->format('Y-m-d') : '');
            $AjoutIntervenantExc->bindValue(':dateContrat', (is_object($dateContrat)) ? $dateContrat->format('Y-m-d') : '');
            $AjoutIntervenantExc->bindValue(':dateUrssaf', (is_object($dateUrssaf)) ? $dateUrssaf->format('Y-m-d') : '');

            /** -----------------------------------------------------------------------------------
             * Modification des dates
             */
            $AjoutIntervenantExc->bindValue(':dateCreation', (is_object($dateCreation)) ? $dateCreation->format('Y-m-d') : '');
            $AjoutIntervenantExc->bindValue(':dateModification', (is_object($dateCreation)) ? $dateModification->format('Y-m-d') : '');
            $AjoutIntervenantExc->bindValue(':idUtilisateur', $userModification);

            $AjoutIntervenantExc->bindValue(':nombreEnfant', $infoIntervenant->zsNbEnfant);
            $AjoutIntervenantExc->bindValue(':boolAutoEntrepreneuse', $infoIntervenant->zsAutoEntrepreneuse);
            $AjoutIntervenantExc->bindValue(':depNaissanceDUE', $infoIntervenant->zsDepartementNaissance);
            $AjoutIntervenantExc->bindValue(':numeroIBAN', $infoIntervenant->ztIban);
            $AjoutIntervenantExc->bindValue(':codeBIC', $infoIntervenant->ztCodeBic);

            $AjoutIntervenantExc->bindValue(':libelleBanque', $infoIntervenant->ztAdresseBanque);
            $AjoutIntervenantExc->bindValue(':codeBanque', $infoIntervenant->ztCodeBanque);
            $AjoutIntervenantExc->bindValue(':codeGuichet', $infoIntervenant->ztCodeGuichet);
            $AjoutIntervenantExc->bindValue(':numCompte', $infoIntervenant->ztNumCompte);
            $AjoutIntervenantExc->bindValue(':cleSecurite', $infoIntervenant->ztCle);

            $AjoutIntervenantExc->bindValue(':niveau', $infoIntervenant->zsNiveau);
            $AjoutIntervenantExc->bindValue(':siret', $infoIntervenant->ztNumSiret);

            if ($AjoutIntervenantExc->execute()) {

                /** @var $idIntervenant  IDENTIFIANT du nouvel intervenant */
                $idIntervenant = DbConnexion::getInstance()->lastInsertId();

                foreach ($_POST['lstCompetence'] as $idCompetence) {
                    $AjoutCompetenceExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutCompetenceExc->bindValue(':id', $idCompetence);
                    $AjoutCompetenceExc->execute();
                }

                foreach ($_POST['lstDept'] as $idDepartement) {
                    $AjoutDepartementExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutDepartementExc->bindValue(':id', $idDepartement);
                    $AjoutDepartementExc->execute();
                }

                foreach ($_POST['lstExperience'] as $idExperience) {
                    $AjoutExperienceExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutExperienceExc->bindValue(':id', $idExperience);
                    $AjoutExperienceExc->execute();
                }

                foreach ($_POST['lstQualification'] as $idQualification) {
                    $AjoutQualificationExc->bindValue(':idIntervenant', $idIntervenant);
                    $AjoutQualificationExc->bindValue(':id', $idQualification);
                    $AjoutQualificationExc->execute();
                }

                print json_encode(array(
                    'result' => $idIntervenant
                ));
            }
        }
    } else {
        print json_encode(array(
            'result' => 0
        ));
    }
}

