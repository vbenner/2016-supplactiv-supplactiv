<?php
/**
 * Retour d'un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

if(filter_has_var(INPUT_POST, 'idContrat')){
    if(filter_input(INPUT_POST, 'boolChk') == 1){
        $AjoutRetourContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'), PDO::PARAM_INT);
        $AjoutRetourContratExc->bindValue(':dateRetour', date('Y-m-d H:i:s'), PDO::PARAM_STR);
        $AjoutRetourContratExc->execute();
    }else {
        $SuppressionRetourContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'), PDO::PARAM_INT);
        $SuppressionRetourContratExc->execute();
    }
}