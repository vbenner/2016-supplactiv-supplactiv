<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Maj des informations principales d'un point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

if (filter_has_var(INPUT_POST, 'type') &&
    filter_has_var(INPUT_POST, 'idSousCategorie')) {

    switch (filter_input(INPUT_POST, 'type')) {
        case 1:
            $params = explode('_', filter_input(INPUT_POST, 'idSousCategorie'));
            $sqlUpdateSousCategorie = '
            UPDATE su_pdv_sous_categorie
            SET libelleSousCategorie = :libelle
            WHERE idSousCategorie = :id
            AND FK_idCategorie = :fk
            ';
            $updateSousCategorieExec = DbConnexion::getInstance()->prepare($sqlUpdateSousCategorie);
            $updateSousCategorieExec->bindValue(':id', $params[0]);
            $updateSousCategorieExec->bindValue(':fk', $params[1]);
            $updateSousCategorieExec->bindValue(':libelle', filter_input(INPUT_POST, 'ztLibelle'));
            print json_encode(array(
                'result' => ($updateSousCategorieExec->execute()) ? 1 : json_encode($updateSousCategorieExec->errorInfo())
            ));
            break;
    }
}
