<?php
/**
 * @author Vinent BENNER - Page UP
 * @detail Suppression des doublons
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
$sqlGetCIP = '
SELECT idPdv
FROM su_pdv
WHERE codeMerval = :cip
';
$getOldCIPExec = DbConnexion::getInstance()->prepare($sqlGetCIP);
$getOldCIPExec->bindValue(':cip', filter_input(INPUT_POST, 'pOld'), PDO::PARAM_STR);
$getOldCIPExec->execute();
if ($getOldCIPExec->rowCount() == 1) {
    $getOldCIP = $getOldCIPExec->fetch(PDO::FETCH_OBJ);
    $oldCIP = $getOldCIP->idPdv;
} else {

    print json_encode(array(
        'result' => 0,
        'raison' => 'Code CIP '.filter_input(INPUT_POST, 'pOld'). ' inconnu !'
    ));
    return;
}

$getNewCIPExec = DbConnexion::getInstance()->prepare($sqlGetCIP);
$getNewCIPExec->bindValue(':cip', filter_input(INPUT_POST, 'pNew'), PDO::PARAM_STR);
$getNewCIPExec->execute();
if ($getNewCIPExec->rowCount() == 1) {
    $getNewCIP = $getNewCIPExec->fetch(PDO::FETCH_OBJ);
    $newCIP = $getNewCIP->idPdv;
} else {
    print json_encode(array(
        'result' => 0,
        'raison' => 'Code CIP '.filter_input(INPUT_POST, 'pNew').' inconnu !'
    ));
    return;
}

/** -----------------------------------------------------------------------------------------------
 * Phase de REMPLACEMENT
 */
$sqlReplaceIntervention = '
UPDATE su_intervention
SET FK_idPdv = :new
WHERE FK_idPdv = :old
';
$replaceInterventionExec = DbConnexion::getInstance()->prepare($sqlReplaceIntervention);
$replaceInterventionExec->bindValue(':old', $oldCIP);
$replaceInterventionExec->bindValue(':new', $newCIP);
$replaceInterventionExec->execute();

$sqlReplaceInterlocuteur = '
UPDATE su_pdv_interlocuteur
SET FK_idPdv = :new
WHERE FK_idPdv = :old
';
$replaceInterlocuteurExec = DbConnexion::getInstance()->prepare($sqlReplaceInterlocuteur);
$replaceInterlocuteurExec->bindValue(':old', $oldCIP);
$replaceInterlocuteurExec->bindValue(':new', $newCIP);
$replaceInterlocuteurExec->execute();

/** -----------------------------------------------------------------------------------------------
 * A cause des DUPPLICATE KEYS, on va faire un SELECT et vérifier que la TARGET n'existe pas
 */
$sqlGetAllPDVKilometrage = '
SELECT *
FROM su_pdv_kilometrage
WHERE FK_idPdv = :old
';
$getAllPDVKilometrageExec = DbConnexion::getInstance()->prepare($sqlGetAllPDVKilometrage);
$getAllPDVKilometrageExec->bindValue(':old', $oldCIP);
$getAllPDVKilometrageExec->execute();
while ($getAllPDVKilometrage = $getAllPDVKilometrageExec->fetch(PDO::FETCH_OBJ)) {
    $intervenante = $getAllPDVKilometrage->FK_idIntervenant;

    /** -------------------------------------------------------------------------------------------
     * Target EXISTE ?
     */
    $sqlCheckPresenceTarget = '
    SELECT COUNT(*) AS LeCount
    FROM su_pdv_kilometrage
    WHERE FK_idPdv = :new
    AND FK_idIntervenant = :inter
    ';
    $checkPresenceTargetExec = DbConnexion::getInstance()->prepare($sqlCheckPresenceTarget);
    $checkPresenceTargetExec->bindValue(':new', $newCIP);
    $checkPresenceTargetExec->bindValue(':inter', $intervenante);
    $checkPresenceTargetExec->execute();
    $checkPresenceTarget = $checkPresenceTargetExec->fetch(PDO::FETCH_OBJ);
    if ($checkPresenceTarget->LeCount == 0) {
        /** ---------------------------------------------------------------------------------------
         * UPDATE
         */
        $sqlReplaceKilometrage = '
        UPDATE su_pdv_kilometrage
        SET FK_idPdv = :new
        WHERE FK_idPdv = :old
        ';
        $replaceKilometrageExec = DbConnexion::getInstance()->prepare($sqlReplaceKilometrage);
        $replaceKilometrageExec->bindValue(':old', $oldCIP);
        $replaceKilometrageExec->bindValue(':new', $newCIP);
        $replaceKilometrageExec->execute();
    } else {
        /** ---------------------------------------------------------------------------------------
         * DELETE
         */
        $sqlDeleteKilometrage = '
        DELETE FROM su_pdv_kilometrage
        WHERE FK_idPdv = :old
        ';
        $deleteKilometrageExec = DbConnexion::getInstance()->prepare($sqlDeleteKilometrage);
        $deleteKilometrageExec->bindValue(':old', $oldCIP);
        $deleteKilometrageExec->execute();
    }
}

$sqlDeleteOld = '
DELETE FROM su_pdv
WHERE idPdv = :old
';
$deleteOldExec  = DbConnexion::getInstance()->prepare($sqlDeleteOld);
$deleteOldExec->bindValue(':old', $oldCIP);
$deleteOldExec->execute();


/** -----------------------------------------------------------------------------------------------
 * Mise à jour du nouveau et on stocke l'ancien
 */
$sqlStoreOldCodeMerval = '
UPDATE su_pdv
SET codeMerval_old = :old
WHERE idPdv = :new
';
$storeOldCodeMervalExec  = DbConnexion::getInstance()->prepare($sqlStoreOldCodeMerval);
$storeOldCodeMervalExec->bindValue(':old', filter_input(INPUT_POST, 'pOld'));
$storeOldCodeMervalExec->bindValue(':new', $newCIP);
$storeOldCodeMervalExec->execute();

print json_encode(array(
    'result' => 1,
    'raison' => 'Suppression réussie !'
));
