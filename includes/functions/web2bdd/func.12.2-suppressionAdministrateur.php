<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Suppression d'un administrateur
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if(filter_has_var(INPUT_POST, 'idUtilisateur') && isset($_SESSION ['idUtilisateur'.PROJECT_NAME])){

    /** Suppression de l'administrateur */
    $SuppressionAdministrateurAgenceExc->bindValue(':idUtilisateur', filter_input(INPUT_POST, 'idUtilisateur'));
    print json_encode(array(
        'result' => ($SuppressionAdministrateurAgenceExc->execute()) ? 1 : 0
    ));
}