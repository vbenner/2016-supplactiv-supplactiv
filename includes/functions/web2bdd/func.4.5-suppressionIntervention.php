<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Suppression d'une intervention (complete ou partielle)
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idIntervention' )) {

    if(filter_input(INPUT_POST, 'boolAll') == 'true'){
        $SuppressionInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'), PDO::PARAM_INT);
        $SuppressionInterventionExc->execute();
    }

    if(filter_input(INPUT_POST, 'boolDate') == 'true'){
        $SuppressionDateInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'), PDO::PARAM_INT);
        $SuppressionDateInterventionExc->execute();
    }

    if(filter_input(INPUT_POST, 'boolContrat') == 'true'){
        $SuppressionContratInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'), PDO::PARAM_INT);
        $SuppressionContratInterventionExc->execute();
    }
}
