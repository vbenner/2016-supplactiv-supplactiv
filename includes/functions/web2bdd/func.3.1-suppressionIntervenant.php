<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Suppression d'un intervenant
 */
ini_set('display_errors', 'on');

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';


/** On test la presence du POST */
if(filter_has_var(INPUT_POST, 'idIntervenant') && isset($_SESSION ['idUtilisateur'.PROJECT_NAME])) {

    $ReinitAffiliationInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $ReinitAffiliationInterventionExc->execute();

    $SuppressionIntervenantExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    print json_encode(array(
        'result' => ($SuppressionIntervenantExc->execute()) ? 1 : 0
    ));
}