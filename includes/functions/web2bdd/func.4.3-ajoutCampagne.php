<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'une campagne
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idClient' )) {

    $dateDCampagne = DateTime::createFromFormat('d/m/Y', filter_input(INPUT_POST, 'dateDCampagne'));
    $dateFCampagne = DateTime::createFromFormat('d/m/Y', filter_input(INPUT_POST, 'dateFCampagne'));

    if($dateDCampagne < $dateFCampagne){

        $sqlAjoutCampagne = '
        INSERT INTO su_campagne(FK_idInterlocuteurAgence, FK_idInterlocuteurClient, libelleCampagne, descriptionCampagne, dateDebut, dateFin, tarifAnimation, tarifFormation, tarifMarchandising)
        VALUES(:idInterlocuteurAgence, :idInterlocuteurClient, :libelleCampagne, :desc, :dateD, :dateF, :tarifA, :tarifF, :tarifM)';
        $AjoutCampagneExc = DbConnexion::getInstance()->prepare($sqlAjoutCampagne);


        $AjoutCampagneExc->bindValue(':idInterlocuteurAgence', filter_input(INPUT_POST, 'ztIntAgence'));
        $AjoutCampagneExc->bindValue(':idInterlocuteurClient', filter_input(INPUT_POST, 'ztIntClient'));
        $AjoutCampagneExc->bindValue(':libelleCampagne', filter_input(INPUT_POST, 'ztlibelleCampagne'));
        $AjoutCampagneExc->bindValue(':desc', filter_input(INPUT_POST, 'ztCommentaireC'));
        $AjoutCampagneExc->bindValue(':dateD', $dateDCampagne->format('Y-m-d'));
        $AjoutCampagneExc->bindValue(':dateF', $dateFCampagne->format('Y-m-d'));
        $AjoutCampagneExc->bindValue(':tarifA', filter_input(INPUT_POST, 'ztAnimation'));
        $AjoutCampagneExc->bindValue(':tarifF', filter_input(INPUT_POST, 'ztFormation'));
        $AjoutCampagneExc->bindValue(':tarifM', filter_input(INPUT_POST, 'ztMerchandising'));
        print json_encode(array(
            'result' => ($AjoutCampagneExc->execute()) ? 1 : 0
        ));

    }else {
        print json_encode(array(
            'result' => 0
        ));
    }

}