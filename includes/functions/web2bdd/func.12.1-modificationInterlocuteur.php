<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Liste des interlocuterus agence
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

if(filter_has_var(INPUT_POST, 'idUtilisateur')){

    $ModificationInterlocuteurExc->bindValue(':id', filter_input(INPUT_POST, 'idUtilisateur'));
    $ModificationInterlocuteurExc->bindValue(':nom', filter_input(INPUT_POST, 'nomUtilisateur'));
    $ModificationInterlocuteurExc->bindValue(':prenom', filter_input(INPUT_POST, 'prenomUtilisateur'));
    $ModificationInterlocuteurExc->bindValue(':tel', filter_input(INPUT_POST, 'telUtilisateur'));
    $ModificationInterlocuteurExc->bindValue(':mail', filter_input(INPUT_POST, 'mailUtilisateur'));


    $ModificationInterlocuteurExc->execute();
}
