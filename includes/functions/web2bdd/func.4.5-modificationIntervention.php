<?php
/**
 * Modification des informations d'une intervention
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
$hasHeureSupp = false;
if(filter_has_var(INPUT_POST, 'idIntervention')) {

    /** -------------------------------------------------------------------------------------------
     * Modification de la date d'intervention
     */
    if(
        filter_input(INPUT_POST, 'dateIntervention') != '' &&
        filter_input(INPUT_POST, 'heureD') != '' &&
        filter_input(INPUT_POST, 'heureF') != '' ){

        $dateIntervention = DateTime::createFromFormat('d/m/Y', filter_input(INPUT_POST, 'dateIntervention'));

        /** ---------------------------------------------------------------------------------------
         * On doit ne pas avoir d'intervention A CHEVAL
         */
        $sqlHasDejaUneIntervention = '
        SELECT COUNT(*) AS LeCount
        FROM su_intervention
        WHERE idIntervention != :idintervention
        AND FK_idIntervenant = :idintervenant
        AND (
            dateDebut BETWEEN :limiteinf AND :limitesupp
            OR
            dateFin BETWEEN :limiteinf AND :limitesupp
        )
        ';
        $hasDejaUneInterventionExec = DbConnexion::getInstance()->prepare($sqlHasDejaUneIntervention);
        $hasDejaUneInterventionExec->bindValue(':idintervention', filter_input(INPUT_POST, 'idIntervention'));
        $hasDejaUneInterventionExec->bindValue(':idintervenant', filter_input(INPUT_POST, 'idIntervenant'));
        $hasDejaUneInterventionExec->bindValue(':limiteinf', $dateIntervention->format('Y-m-d').' '.filter_input(INPUT_POST, 'heureD').':00');
        $hasDejaUneInterventionExec->bindValue(':limitesupp', $dateIntervention->format('Y-m-d').' '.filter_input(INPUT_POST, 'heureF').':00');
        $hasDejaUneInterventionExec->execute();
        $count = $hasDejaUneInterventionExec->fetch(PDO::FETCH_OBJ);
        if ($count->LeCount > 0) {
            print json_encode(array(
                'error' => 1,
                'info' => 'Il existe une intervention incompatible'
            ));
            return;
        }


        $ModificationDateInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
        $ModificationDateInterventionExc->bindValue(':dateDebut', $dateIntervention->format('Y-m-d').' '.filter_input(INPUT_POST, 'heureD').':00');
        $ModificationDateInterventionExc->bindValue(':dateFin', $dateIntervention->format('Y-m-d').' '.filter_input(INPUT_POST, 'heureF').':00');
        $ModificationDateInterventionExc->execute();

        $ModificationAffiliationInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
        $ModificationAffiliationInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
        $ModificationAffiliationInterventionExc->execute();
    }

    /** Modification de l'affiliation */
    else {
        $ModificationAffiliationInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
        $ModificationAffiliationInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
        $ModificationAffiliationInterventionExc->execute();
    }

    /** -------------------------------------------------------------------------------------------
     * Avec l'ID de l'intervention, on récupère la semaine en question
     */
    $sqlGetWeekOfCurrentInter = '
    SELECT dateDebut, FK_idIntervenant, FK_idContrat
    FROM su_intervention
    WHERE idIntervention = :idIntervention
    ';
    $getWeekOfCurrentInterExec = DbConnexion::getInstance()->prepare($sqlGetWeekOfCurrentInter);
    $getWeekOfCurrentInterExec->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
    $getWeekOfCurrentInterExec->execute();
    $getWeekOfCurrentInter = $getWeekOfCurrentInterExec->fetch(PDO::FETCH_OBJ);

    $lundi = date("Y-m-d 00:00:00", strtotime('last monday', strtotime('next week', strtotime($getWeekOfCurrentInter->dateDebut))));
    $dimanche = date("Y-m-d 23:59:59", strtotime('last sunday', strtotime('next week', strtotime($getWeekOfCurrentInter->dateDebut))));

    /** -------------------------------------------------------------------------------------------
     * On regarde maintenant au niveau de ce contrat ET de cette semaine uniquement
     */
    $sqlGetDataOfThisWeek = '
    SELECT *
    FROM su_intervention
    WHERE FK_idIntervenant = :idIntervenant
    AND FK_idContrat = :idContrat
    AND dateDebut >= :debut
    AND dateFin <= :fin
    ';
    $getDataOfThisWeekExec = DbConnexion::getInstance()->prepare($sqlGetDataOfThisWeek);
    $getDataOfThisWeekExec->bindValue(':idContrat', $getWeekOfCurrentInter->FK_idContrat);
    $getDataOfThisWeekExec->bindValue(':idIntervenant', $getWeekOfCurrentInter->FK_idIntervenant);
    $getDataOfThisWeekExec->bindValue(':debut', $lundi);
    $getDataOfThisWeekExec->bindValue(':fin', $dimanche);
    $getDataOfThisWeekExec->execute();
    $duree = 0;
    while ($inter = $getDataOfThisWeekExec->fetch(PDO::FETCH_OBJ)) {
        $hourdiff = round((strtotime($inter->dateFin) - strtotime($inter->dateDebut)) / 3600, 2);
        if ($hourdiff > 7) {
            $hourdiff -= 1;
        }
        $duree += $hourdiff;
    }

    if ($duree > 35) {
        $hasHeureSupp = true;
    }
    #echo 'Du '.$lundi.' au '.$dimanche.' - Duree = '.$duree;
    #die();
    /** -------------------------------------------------------------------------------------------
     * Avec cette semaine et cette ANNEE, on en déduit la date MIN et la date MAX
     */
    print json_encode(array(
        'heuresupp' => $hasHeureSupp ? 1 : 0
    ));
}