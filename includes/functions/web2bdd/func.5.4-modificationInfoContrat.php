<?php
/**
 * Modification des informations contrats
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idContrat')) { //} && filter_has_var(INPUT_POST, 'detail')) {

    /** -------------------------------------------------------------------------------------------
     * Pour la modification des contrats, on doit pouvoir aussi modifier les données
     * de frais téléphoniques et de repas
     */

    /** -------------------------------------------------------------------------------------------
     * Attention, si AUTO, on ne cible pas les mêmes colonnes
     */
    $MajInformationContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $MajInformationContratExc->bindValue(':societeA', filter_input(INPUT_POST, 'societeA'));
    $MajInformationContratExc->bindValue(':societeB', filter_input(INPUT_POST, 'societeB'));
    $MajInformationContratExc->bindValue(':detail', filter_input(INPUT_POST, 'detail'));

    $MajInformationContratExc->bindValue(':tauxh', filter_input(INPUT_POST, 'pTxH'));

    // Mise à jour du forfait JN
    $MajInformationContratExc->bindValue(':forfaitj', filter_input(INPUT_POST, 'pForfaitJ'));
    $MajInformationContratExc->bindValue(':forfaitjn', filter_input(INPUT_POST, 'pForfaitJN'));

    $MajInformationContratExc->bindValue(':coutkm', filter_input(INPUT_POST, 'pCoutKm'));

    if (filter_has_var(INPUT_POST, 'boolFraisInclus')) {
        $MajInformationContratExc->bindValue(':boolFraisInclus', filter_input(INPUT_POST, 'boolFraisInclus'));
    }
    else {
        $MajInformationContratExc->bindValue(':boolFraisInclus', 'NON');
    }

    $res = $MajInformationContratExc->execute();

    /** -------------------------------------------------------------------------------------------
     * On modifie aussi la partie des frais
     * -> sur un contrat, on a des interventions
     *
     * $sqlAjoutFraisIntervention = '
     *   INSERT INTO su_intervention_frais(FK_idIntervention, fraisRepas, fraisTelephone)
     *   VALUES(:idIntervention, :fraisRepas, :fraisTel)
     *   ON DUPLICATE KEY UPDATE fraisRepas = :fraisRepas, fraisTelephone = :fraisTel';
     *   $AjoutFraisInterventionExc = DbConnexion::getInstance()->prepare($sqlAjoutFraisIntervention);
     *
     */
    $sqlUpdateIntervention = '
    UPDATE su_intervention_frais
    SET fraisRepas = :repas, fraisTelephone = :telephone
    WHERE FK_idIntervention = :idinter
    ';
    $updateInterventionExec = DbConnexion::getInstance()->prepare($sqlUpdateIntervention);

    $sqlGetListeIntervention = '    
    SELECT *
    FROM su_intervention
    WHERE FK_idContrat = :idContrat
    ';
    $getListeInterventionExec = DbConnexion::getInstance()->prepare($sqlGetListeIntervention);
    $getListeInterventionExec->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $getListeInterventionExec->execute();
    while ($listeIntervention = $getListeInterventionExec->fetch(PDO::FETCH_OBJ)){
        $idInter = $listeIntervention->idIntervention;

        $updateInterventionExec->bindValue(':idinter', $idInter);
        $updateInterventionExec->bindValue(':repas', filter_input(INPUT_POST, 'pForfaitRepas'));
        $updateInterventionExec->bindValue(':telephone', filter_input(INPUT_POST, 'pForfaitTel'));
        $updateInterventionExec->execute();
    }
    print json_encode(array(
        'result' => ($res ? 1 : 0)
    ));
}