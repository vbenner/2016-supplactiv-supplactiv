<?php
/**
 * Suppression des dates d'intervention
 *
 * @author Vincent BENNER - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if(isset($_POST['listeInterventions_4_5'])){

    /** On enregistre le POST en session */
    $_SESSION['listeInterventions_4_5'] = $_POST['listeInterventions_4_5'];

    $nbTotal = 0;

    /** On enregistre les dates */
    foreach($_SESSION['listeInterventions_4_5'] as $idInter) {
        $SuppressionDateInterventionExc->bindValue(':idIntervention', $idInter);
        $nbTotal += ($SuppressionDateInterventionExc->execute()) ? 1 : 0;
    }

    print json_encode(array(
        'totalOK' => $nbTotal,
        'total' => sizeof($_SESSION['listeInterventions_4_5'])
    ));
}
