<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Modification des frais interventions pour une intervenante
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** ON test la presence des POSTS */
if(filter_has_var(INPUT_POST, 'dateFrais') && filter_has_var(INPUT_POST, 'idIntervenant')){

    /** On parcours la liste des KM */
    $listeKm = preg_split('/\|/', filter_input(INPUT_POST, 'kms'));
    foreach($listeKm as $InfoKM){
        if(!empty($InfoKM)){
            $Info = preg_split('/_/', $InfoKM);

            /** Modification des KM */
            $ModificationKmIntervenantExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
            $ModificationKmIntervenantExc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'));
            $ModificationKmIntervenantExc->bindValue(':tarif', $Info[0]);
            $ModificationKmIntervenantExc->bindValue(':nbKM', $Info[1]);
            $ModificationKmIntervenantExc->execute();
        }
    }

    $ModificationFraisInterventionExc->bindValue(':montantPrimeRdv', filter_input(INPUT_POST, 'ztPrimeRDV'));
    $ModificationFraisInterventionExc->bindValue(':montantIndemniteFormation', filter_input(INPUT_POST, 'ztIndemniteFormation'));
    $ModificationFraisInterventionExc->bindValue(':montantIndemniteTelephone', filter_input(INPUT_POST, 'ztIndemniteTelephone'));
    $ModificationFraisInterventionExc->bindValue(':montantAutreFrais', filter_input(INPUT_POST, 'ztAutreFrais'));
    $ModificationFraisInterventionExc->bindValue(':montantFraisJustificatif', filter_input(INPUT_POST, 'ztFraisJustificatif'));
    $ModificationFraisInterventionExc->bindValue(':montantFraisJustificatifTVA', filter_input(INPUT_POST, 'ztFraisJustificatifTVA'));
    $ModificationFraisInterventionExc->bindValue(':montantPrimeObjectif', filter_input(INPUT_POST, 'ztPrimeObjectif'));
    $ModificationFraisInterventionExc->bindValue(':montantIndemniteAnnulation', filter_input(INPUT_POST, 'ztInterventionAnnulee'));
    $ModificationFraisInterventionExc->bindValue(':montantAutrePrime', filter_input(INPUT_POST, 'ztAutrePrime'));
    $ModificationFraisInterventionExc->bindValue(':montantRepriseAccompte', filter_input(INPUT_POST, 'ztRepriseAcompte'));
    $ModificationFraisInterventionExc->bindValue(':commentairePrimeObjectif', filter_input(INPUT_POST, 'ztCommentairePrimeObjectif'));
    $ModificationFraisInterventionExc->bindValue(':commentaireIndemniteAnnulation', filter_input(INPUT_POST, 'ztCommentaireInterventionAnnulee'));
    $ModificationFraisInterventionExc->bindValue(':commentaireAutrePrime', filter_input(INPUT_POST, 'ztCommentaireAutrePrime'));
    $ModificationFraisInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $ModificationFraisInterventionExc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'));

    print json_encode(array(
        'result' => ($ModificationFraisInterventionExc->execute()) ? 1 : 0
    ));


}