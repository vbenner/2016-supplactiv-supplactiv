<?php

error_reporting(E_ALL);

/** ---------------------------------------------------------------------------------------------
 * Modification (uniquement) de la PHOTO
 *
 * @author Vincent BENNER - Page UP
 * @copyright Page UP
 */

/** ---------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** ---------------------------------------------------------------------------------------------
 * Requete sql
 */
$sqlModificationPhoto = '
UPDATE su_intervenant SET
  b64_photo = :photo
WHERE idIntervenant = :idIntervenant
';

$modificationPhotoExec = DbConnexion::getInstance()->prepare($sqlModificationPhoto);

if(filter_has_var(INPUT_POST, 'photo')){

    if (filter_input(INPUT_POST, 'photo') != '') {


        $tmp = explode(',', filter_input(INPUT_POST, 'photo'));
        $photo = $tmp[1];
        $data = base64_decode($photo);

       #echo $data;

        $fic = '../../../download/id.'.filter_input(INPUT_POST, 'idIntervenant').'.jpg';
        $list = glob($fic.'.*');
        if (count($list) == 0) {
        } else {
            $del = $list[0];
            unlink ($del);
        }
        file_put_contents($fic, $data);


        #try {
        #    switch (exif_imagetype($fic)) {
        #        case 2 :
        #            rename($fic, $fic . '.jpg');
        #            $fic = $fic . '.jpg';
        #            break;
        #        case 3 :
        #            rename($fic, $fic . '.png');
        #            $fic = $fic . '.png';
        #            break;
        #        default:
        #            rename($fic, $fic . '.jpg');
        #            $fic = $fic . '.jpg';
        #    }
        #} catch (Exception $e) {
        #    //str_($fic, $fic . '.jpg');
        #    //$fic = $fic . '.jpg';
        #}
        #echo 'FIC -> '.$fic;
        #return;

        $modificationPhotoExec->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
        $modificationPhotoExec->bindValue(':photo', $fic, PDO::PARAM_STR);
        $res = $modificationPhotoExec->execute();

        print json_encode(array(
            'result' => $res,
            'photo' => str_replace('../../../', '', $fic)
        ));

    } else {

        $res = array_map('unlink', glob('../../../download/id.'.filter_input(INPUT_POST, 'idIntervenant').'.*'));
        print json_encode(array(
            'result' => $res,
            'photo' => ''
        ));
    }

} else {

    print_r($_REQUEST);
    
}