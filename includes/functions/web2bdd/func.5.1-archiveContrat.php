<?php
/**
 * Suppression d'un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requete sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';
ini_set('display_errors', 'on');

if(filter_has_var(INPUT_POST, 'idContrat')) {

    $ArchiveContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $ArchiveContratExc->bindValue(':boolContratArchive', filter_input(INPUT_POST, 'boolArchive'));
    print json_encode(array(
       'result' => ($ArchiveContratExc->execute()) ? 1 : 0
    ));
}