<?php
/**
 * Modification d'un interlocuteur client
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idInterlocuteur')) {

    /** Suppression */
    if(filter_has_var(INPUT_POST, 'boolSup')){
        $SuppressionInterlocuteurClientExc->bindValue(':idInterlocuteur', filter_input(INPUT_POST, 'idInterlocuteur'));
        print json_encode(array(
            'result' => ($SuppressionInterlocuteurClientExc->execute()) ? 1 : 0
        ));
    }

    /** Ajout */
    elseif(filter_input(INPUT_POST, 'idInterlocuteur') == 'NV'){
        $AjoutInterlocuteurClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient'));
        $AjoutInterlocuteurClientExc->bindValue(':nom', filter_input(INPUT_POST, 'nom'));
        $AjoutInterlocuteurClientExc->bindValue(':prenom', filter_input(INPUT_POST, 'prenom'));
        $AjoutInterlocuteurClientExc->bindValue(':tel', filter_input(INPUT_POST, 'tel'));
        $AjoutInterlocuteurClientExc->bindValue(':mobile', filter_input(INPUT_POST, 'mobile'));
        $AjoutInterlocuteurClientExc->bindValue(':email', filter_input(INPUT_POST, 'email'));
        $AjoutInterlocuteurClientExc->bindValue(':statut', filter_input(INPUT_POST, 'statut'));
        $AjoutInterlocuteurClientExc->bindValue(':sexe', filter_input(INPUT_POST, 'sexe'));

        print json_encode(array(
            'result' => ($AjoutInterlocuteurClientExc->execute()) ? 1 : $AjoutInterlocuteurClientExc->errorInfo()
        ));
    }

    /** Modification */
    else {
        $ModificationInterlocuteurClientExc->bindValue(':idInterlocuteur', filter_input(INPUT_POST, 'idInterlocuteur'));
        $ModificationInterlocuteurClientExc->bindValue(':nom', filter_input(INPUT_POST, 'nom'));
        $ModificationInterlocuteurClientExc->bindValue(':prenom', filter_input(INPUT_POST, 'prenom'));
        $ModificationInterlocuteurClientExc->bindValue(':tel', filter_input(INPUT_POST, 'tel'));
        $ModificationInterlocuteurClientExc->bindValue(':mobile', filter_input(INPUT_POST, 'mobile'));
        $ModificationInterlocuteurClientExc->bindValue(':email', filter_input(INPUT_POST, 'email'));
        $ModificationInterlocuteurClientExc->bindValue(':statut', filter_input(INPUT_POST, 'statut'));
        $ModificationInterlocuteurClientExc->bindValue(':sexe', filter_input(INPUT_POST, 'sexe'));

        print json_encode(array(
            'result' => ($ModificationInterlocuteurClientExc->execute()) ? 1 : 0
        ));
    }
}