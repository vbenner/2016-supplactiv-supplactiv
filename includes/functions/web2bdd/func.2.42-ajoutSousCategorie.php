<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** On test la presence du POST */
if (filter_has_var(INPUT_POST, 'info')) {

    /** On parse le POST */
    parse_str(filter_input(INPUT_POST, 'info'), $infoSousCategorie);
    $infoSousCategorie = (object)$infoSousCategorie;

    /** Verification des variables obligatoires */
    if (!empty($infoSousCategorie->ztLibelle) != '' ) {

        /** ---------------------------------------------------------------------------------------
         * Get MAX (idSousCategorie)
         */
        $sqlGetMax = '
        SELECT MAX(idSousCategorie) AS LeMax
        FROM su_pdv_sous_categorie
        WHERE FK_idCategorie = :fk
        ';
        $getMaxExec = DbConnexion::getInstance()->prepare($sqlGetMax);
        $getMaxExec->bindValue(':fk', $infoSousCategorie->ztCategorie, PDO::PARAM_INT);
        if ($getMaxExec->execute()) {
            $getMax = $getMaxExec->fetch(PDO::FETCH_OBJ);
            $leMax = is_null($getMax->LeMax) ? 1 : $getMax->LeMax + 1;

            $sqlAjoutSousCategorie = '
            INSERT INTO su_pdv_sous_categorie (libelleSousCategorie, FK_idCategorie, idSousCategorie)
            VALUES (:libelle, :fk, :id)
            ';
            $ajoutSousCategorieExec = DbConnexion::getInstance()->prepare($sqlAjoutSousCategorie);
            $ajoutSousCategorieExec->bindValue(':libelle', strtoupper(addslashes($infoSousCategorie->ztLibelle)), PDO::PARAM_STR);
            $ajoutSousCategorieExec->bindValue(':fk', $infoSousCategorie->ztCategorie, PDO::PARAM_INT);
            $ajoutSousCategorieExec->bindValue(':id', $leMax, PDO::PARAM_INT);

            if ($ajoutSousCategorieExec->execute()) {
                print json_encode(array(
                    'result' => $infoSousCategorie->ztCategorie
                ));
            }
        }
    } else {
        print json_encode(array(
            'result' => 0
        ));
    }
}

