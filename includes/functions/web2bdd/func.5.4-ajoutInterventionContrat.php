<?php
/**
 * Reto
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

ini_set('display_errors','on');
/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

if(filter_has_var(INPUT_POST, 'idContrat') && filter_has_var(INPUT_POST, 'idIntervention')){

    $RechercheFraisContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $RechercheFraisContratExc->execute();
    $InfoFrais = $RechercheFraisContratExc->fetch(PDO::FETCH_OBJ);
    $AjoutInterventionContratExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
    $AjoutInterventionContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    print json_encode(array(
        'result' => ($AjoutInterventionContratExc->execute()) ? 1 : 0
    ));

    $AjoutFraisInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
    $AjoutFraisInterventionExc->bindValue(':fraisRepas', $InfoFrais->fraisRepas);
    $AjoutFraisInterventionExc->bindValue(':fraisTel', $InfoFrais->fraisTelephone);
    $AjoutFraisInterventionExc->execute();
}