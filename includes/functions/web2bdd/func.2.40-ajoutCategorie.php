<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';


/** On test la presence du POST */
if (filter_has_var(INPUT_POST, 'info')) {

    /** On parse le POST */
    parse_str(filter_input(INPUT_POST, 'info'), $infoCategorie);
    $infoCategorie = (object)$infoCategorie;

    /** Verification des variables obligatoires */
    if (!empty($infoCategorie->ztLibelle) != '' ) {

        $sqlAjoutCategorie = '
        INSERT INTO su_pdv_categorie (libelleCategoriePdv)
        VALUES (:libelle)
        ';
        $ajoutCategorieExec = DbConnexion::getInstance()->prepare($sqlAjoutCategorie);
        $ajoutCategorieExec->bindValue(':libelle', $infoCategorie->ztLibelle, PDO::PARAM_STR);
        if ($ajoutCategorieExec->execute()) {

            /** @var $idCategorie  IDENTIFIANT  */
            $idCategorie = DbConnexion::getInstance()->lastInsertId();

            print json_encode(array(
                'result' => $idCategorie
            ));
        }
    } else {
        print json_encode(array(
            'result' => 0
        ));
    }
}

