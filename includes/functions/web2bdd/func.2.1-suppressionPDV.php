<?php

/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER / PAGE UP
 * @detail Suppression d'un PDV
 */
ini_set('display_errors', 'on');

/** -----------------------------------------------------------------------------------------------
 * Connexion a la base de donnees
 */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Fichier SQL
 */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** -----------------------------------------------------------------------------------------------
 * On test la presence du POST
 */
if(filter_has_var(INPUT_POST, 'idPDV') && isset($_SESSION ['idUtilisateur'.PROJECT_NAME])) {

    $SuppressionInterlocuteurPDVExec->bindValue(':idPDV', filter_input(INPUT_POST, 'idPDV'));
    $SuppressionInterlocuteurPDVExec->execute();

    $SuppressionPDVExc->bindValue(':idPDV', filter_input(INPUT_POST, 'idPDV'));
    print json_encode(array(
        'result' => ($SuppressionPDVExc->execute()) ? 1 : 0
    ));
}