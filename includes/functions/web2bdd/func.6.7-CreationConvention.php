<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Creation d'une convention
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

$sqlCreationConvention = '
INSERT INTO su_contrat_cidd (dateContrat, dureeMensuelle, remunerationBrute, indemniteForfaitaire, listePDV, FK_idMission, FK_idIntervenant)
VALUES(:DateConvention, :DureeMensuelle, :Remuneration,  :IndemniteForfaitaire, :ListePdv , :IdMission, :IdIntervenant)';
$CreationConventionExc = DbConnexion::getInstance()->prepare($sqlCreationConvention);

$CreationConventionExc->bindValue(':DateConvention', $_POST['AnneeConvention'] . '-' . $arrayMonthCorres2[$_POST['MoisConvention']] . '-01', PDO::PARAM_STR);
$CreationConventionExc->bindValue(':DureeMensuelle', $_POST['DureeMensuelle'], PDO::PARAM_STR);
$CreationConventionExc->bindValue(':Remuneration', str_replace(',', '.', $_POST['TauxHoraire']), PDO::PARAM_STR);
$CreationConventionExc->bindValue(':IndemniteForfaitaire', str_replace(',', '.', $_POST['IndemniteF']), PDO::PARAM_STR);
$CreationConventionExc->bindValue(':ListePdv', $_POST['ListePdv'], PDO::PARAM_STR);
$CreationConventionExc->bindValue(':IdIntervenant', $_POST['IntervenantConvention'], PDO::PARAM_INT);
$CreationConventionExc->bindValue(':IdMission', $_SESSION['ID_Mission_CreationContrat_6_7'], PDO::PARAM_INT);
if ($CreationConventionExc->execute()) {
    @$_SESSION['IDContrat_Recherche_6_5'] = DbConnexion::getInstance()->lastInsertId();
    echo 'OK';
} else echo 'KO';

