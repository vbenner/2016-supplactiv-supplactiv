<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'intervention
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idPdv' ) && filter_has_var ( INPUT_POST, 'idIntervenant' ) && filter_has_var ( INPUT_POST, 'idFrequence' )) {

    $idIntervenant = (filter_input(INPUT_POST, 'idIntervenant') == 'ALL') ? 1 : filter_input(INPUT_POST, 'idIntervenant');

    for($i = 0; $i < filter_input(INPUT_POST, 'idFrequence'); $i++){
        $AjoutInterventionExc->bindValue(':idIntervenant', $idIntervenant, PDO::PARAM_INT);
        $AjoutInterventionExc->bindValue(':idPdv', filter_input ( INPUT_POST, 'idPdv' ), PDO::PARAM_INT);
        $AjoutInterventionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'), PDO::PARAM_INT);
        $AjoutInterventionExc->execute();
    }
}