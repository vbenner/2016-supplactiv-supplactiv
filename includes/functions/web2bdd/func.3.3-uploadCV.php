<?php
/**
 * Suppression d' un client
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Répertoire de destination...
 */
$dossier_pj      = '../../../download/CV/';
if(isset($_FILES['cv']['name'])) {

    /** -------------------------------------------------------------------------------------------
     * On récupère toutes les infos du fichier .XLS / .XLSX
     */
    $fic = basename($_FILES['cv']['name']);

    if(move_uploaded_file($_FILES['cv']['tmp_name'], $dossier_pj . 'cv.'.$_REQUEST['idInter'].'.pdf')) {
        #$b64Doc = chunk_split(base64_encode(file_get_contents($dossier_pj . $fic)));
        $sqlUpdateCV = '
        UPDATE su_intervenant
        SET b64_cv = :cv
        WHERE idIntervenant = :id
        ';
        $updateCVExec = DbConnexion::getInstance()->prepare($sqlUpdateCV);
        $updateCVExec->bindValue(':cv', '/download/CV/'.'cv.'.$_REQUEST['idInter'].'.pdf', PDO::PARAM_STR);
        $updateCVExec->bindValue(':id', $_REQUEST['idInter'], PDO::PARAM_STR);
        $updateCVExec->execute();
    }
}

print json_encode(array(
    'res' => ADRESSE_BO.'/download/CV/'.'cv.'.$_REQUEST['idInter'].'.pdf'
));