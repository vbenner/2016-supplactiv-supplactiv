<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'une mission
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idCampagne' ) && filter_has_var ( INPUT_POST, 'libelleMission' )) {

    $AjoutMissionExc->bindValue(':idCampagne', filter_input(INPUT_POST, 'idCampagne'));
    $AjoutMissionExc->bindValue(':libelleMission', filter_input(INPUT_POST, 'libelleMission'));
    $AjoutMissionExc->bindValue(':typeMission', filter_input(INPUT_POST, 'typeMission'));
    $AjoutMissionExc->bindValue(':gammeProduit', filter_input(INPUT_POST, 'gammeProduit'));
    $AjoutMissionExc->bindValue(':description', filter_input(INPUT_POST, 'descriptionProduit'));
    $AjoutMissionExc->bindValue(':nbInterPrevisionnel', filter_input(INPUT_POST, 'nbInterPrevisionnel'));
    $AjoutMissionExc->bindValue(':fraisGestion', filter_input(INPUT_POST, 'fraisGestion'));
    $AjoutMissionExc->bindValue(':chargeMission', filter_input(INPUT_POST, 'chargeMission'));

    print json_encode(array(
        'result' =>( $AjoutMissionExc->execute()) ? 1 : 0,
        'idMission' => DbConnexion::getInstance()->lastInsertId()
    ));

}

