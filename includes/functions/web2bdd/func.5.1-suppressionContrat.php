<?php
/**
 * Suppression d'un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requete sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

if(filter_has_var(INPUT_POST, 'idContrat')) {

    /** -------------------------------------------------------------------------------------------
     * Suppression également des interventions liées à ce contrat
     */
    $sqlGetListeInterventions = '
    SELECT * 
    FROM su_intervention 
    WHERE FK_idContrat = :idContrat
    ';

    $getListeInterventionsExec = DbConnexion::getInstance()->prepare($sqlGetListeInterventions);
    $getListeInterventionsExec->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'), PDO::PARAM_INT);
    if ($getListeInterventionsExec->execute()) {
        while($intervention = $getListeInterventionsExec->fetch(PDO::FETCH_OBJ)) {

            if(filter_input(INPUT_POST, 'boolDate') == 'true'){

                $SuppressionDateInterventionExc->bindValue(':idIntervention', $intervention->idIntervention, PDO::PARAM_INT);
                $SuppressionDateInterventionExc->execute();

                $SuppressionContratInterventionExc->bindValue(':idIntervention', $intervention->idIntervention, PDO::PARAM_INT);
                $SuppressionContratInterventionExc->execute();

            }

            if(filter_input(INPUT_POST, 'boolAll') == 'true'){

                $SuppressionInterventionExc->bindValue(':idIntervention', $intervention->idIntervention, PDO::PARAM_INT);
                $SuppressionInterventionExc->execute();
            }

            if(
                filter_input(INPUT_POST, 'boolDate') == 'false' &&
                filter_input(INPUT_POST, 'boolAll') == 'false'
            ) {
                $SuppressionContratInterventionExc->bindValue(':idIntervention', $intervention->idIntervention, PDO::PARAM_INT);
                $SuppressionContratInterventionExc->execute();
            }
        }
    }

    $SuppressionContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));

    print json_encode(array(
       'result' => ($SuppressionContratExc->execute()) ? 1 : 0
    ));
}