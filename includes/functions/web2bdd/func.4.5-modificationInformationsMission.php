<?php
/**
 * Modification des informations mission
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idMission')) {

    $oldMission = filter_input(INPUT_POST, 'missionPrec') != '' ?
        filter_input(INPUT_POST, 'missionPrec') : 0;
    $interPrev = filter_input(INPUT_POST, 'nbInterPrevisionnel') != '' ?
        filter_input(INPUT_POST, 'nbInterPrevisionnel') : null;
    $fraisGestion = filter_input(INPUT_POST, 'fraisGestion') != '' ?
        filter_input(INPUT_POST, 'fraisGestion') : 0;

    $ModificationInformationsMissionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'));
    $ModificationInformationsMissionExc->bindValue(':libelleMission', filter_input(INPUT_POST, 'libelleMission'));
    $ModificationInformationsMissionExc->bindValue(':gammeProduit', filter_input(INPUT_POST, 'gammeProduit'));
    $ModificationInformationsMissionExc->bindValue(':typeMission', filter_input(INPUT_POST, 'typeMission'));
    $ModificationInformationsMissionExc->bindValue(':chargeMission', filter_input(INPUT_POST, 'chargeMission'));
    $ModificationInformationsMissionExc->bindValue(':commentaireMission', filter_input(INPUT_POST, 'commentaireMission'));

    $ModificationInformationsMissionExc->bindValue(':fraisGestion', $fraisGestion);
    $ModificationInformationsMissionExc->bindValue(':nbInterPrevisionnel', $interPrev);
    $ModificationInformationsMissionExc->bindValue(':missionPrec', $oldMission);

    print json_encode(array(
        'result' => ($ModificationInformationsMissionExc->execute()) ? 1 : $ModificationInformationsMissionExc->errorInfo()
    ));
}