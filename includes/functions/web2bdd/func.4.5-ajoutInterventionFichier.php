<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'intervention
 */
ini_set('display_errors','on');
/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idPdv' ) && filter_has_var ( INPUT_POST, 'idIntervenant' ) && filter_has_var ( INPUT_POST, 'idMission' )) {

    $dateDebut = DateTime::createFromFormat('d/m/Y H:i:s',filter_input( INPUT_POST, 'dateD' ).':00');
    $dateFin = DateTime::createFromFormat('d/m/Y H:i:s', filter_input( INPUT_POST, 'dateF' ).':00');


    $AjoutInterventionAvecDateExc->bindValue(':idIntervenant', filter_input( INPUT_POST, 'idIntervenant' ), PDO::PARAM_INT);
    $AjoutInterventionAvecDateExc->bindValue(':idPdv', filter_input( INPUT_POST, 'idPdv' ), PDO::PARAM_INT);
    $AjoutInterventionAvecDateExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'), PDO::PARAM_INT);
    $AjoutInterventionAvecDateExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    $AjoutInterventionAvecDateExc->bindValue(':dateFin', $dateFin->format('Y-m-d H:i:s'), PDO::PARAM_STR);
    if($AjoutInterventionAvecDateExc->execute()){
        print json_encode(array(
            'result' => 1,
            'idIntervention' => DbConnexion::getInstance()->lastInsertId()
        ));
    }
}