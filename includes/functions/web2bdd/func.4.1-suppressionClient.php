<?php
/**
 * Suppression d' un client
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence des POST */
if(filter_has_var(INPUT_POST, 'idClient')) {
    $SuppressionClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient'));

    print json_encode(array(
        'result' => ($SuppressionClientExc->execute()) ? 1 : $SuppressionClientExc->errorInfo()
    ));
}