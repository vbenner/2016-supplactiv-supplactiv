<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/** On test la presence du POST */
if (filter_has_var(INPUT_POST, 'idSalarie')) {

    //idSalarie == 0 --> AJOUT
    if (filter_input(INPUT_POST, 'idSalarie') == 0) {
        $post = (object)$_POST;

        $sqlInsert = '
        INSERT INTO du_utilisateur (FK_idTypeUtilisateur, nomUtilisateur, prenomUtilisateur, 
            immatUtilisateur, passUtilisateur,
            mailUtilisateur, boolActif, villeIntervenant, codePostalIntervenant, 
            adresseIntervenant_A, adresseIntervenant_B, mailManager,
            typeIntervenantExpensia, contratIntervenant
         )
        VALUES (
            :FK_idTypeUtilisateur,
            :nomUtilisateur, :prenomUtilisateur, :immatUtilisateur, :passUtilisateur,
            :mailUtilisateur, 1, :villeIntervenant, :codePostalIntervenant, 
            :adresseIntervenant_A, :adresseIntervenant_B, :mailManager,
            :typeIntervenantExpensia, :contratIntervenant
        )
        ';
        $insertExec = DbConnexion::getInstance()->prepare($sqlInsert);
        $insertExec->bindValue(':FK_idTypeUtilisateur', $post->typeSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':nomUtilisateur', $post->nomSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':prenomUtilisateur', $post->prenomSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':immatUtilisateur', $post->immatSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':passUtilisateur', hash('sha256', $post->passwordSalarie), PDO::PARAM_STR);
        $insertExec->bindValue(':mailUtilisateur', $post->mailSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':villeIntervenant', $post->villeSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':codePostalIntervenant', $post->cpSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':adresseIntervenant_A', $post->adresseSalarieA, PDO::PARAM_STR);
        $insertExec->bindValue(':adresseIntervenant_B', $post->adresseSalarieB, PDO::PARAM_STR);

        $insertExec->bindValue(':typeIntervenantExpensia', $post->typeSalarieExpensia, PDO::PARAM_STR);
        $insertExec->bindValue(':contratIntervenant', $post->contratSalarie, PDO::PARAM_STR);
        $insertExec->bindValue(':mailManager', $post->mailManager, PDO::PARAM_STR);
        if ($insertExec->execute()) {
            $idSalarie = DbConnexion::getInstance()->lastInsertId();

            print json_encode(array(
                'result' => $idSalarie
            ));
        }
        else {
            print json_encode(array(
                'toto' => $insertExec->errorInfo()
            ));

        }
    }
    else {
        $post = (object)$_POST;
        $sqlUpdate = '
        UPDATE du_utilisateur
        SET FK_idTypeUtilisateur = :FK_idTypeUtilisateur,
            boolActif = :boolActif, 
            nomUtilisateur = :nomUtilisateur,
            prenomUtilisateur = :prenomUtilisateur, 
            immatUtilisateur = :immatUtilisateur, 
            mailUtilisateur = :mailUtilisateur,
            villeIntervenant = :villeIntervenant,
            codePostalIntervenant = :codePostalIntervenant,
            adresseIntervenant_A = :adresseIntervenant_A,
            adresseIntervenant_B = :adresseIntervenant_B,
            mailManager = :mailManager,
            typeIntervenantExpensia = :typeIntervenantExpensia,
            contratIntervenant = :contratIntervenant
       ';
        if ($post->passwordSalarie != '') {
            $sqlUpdate .= ', passUtilisateur = :passUtilisateur';
        }
        $sqlUpdate .= 'WHERE idUtilisateur = :idUtilisateur';
        $updateExec = DbConnexion::getInstance()->prepare($sqlUpdate);

        $updateExec->bindValue(':FK_idTypeUtilisateur', $post->typeSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':boolActif', $post->boolActif, PDO::PARAM_INT);
        $updateExec->bindValue(':nomUtilisateur', $post->nomSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':prenomUtilisateur', $post->prenomSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':immatUtilisateur', $post->immatSalarie, PDO::PARAM_STR);
        if ($post->passwordSalarie != '') {
            $updateExec->bindValue(':passUtilisateur', hash('sha256', $post->passwordSalarie), PDO::PARAM_STR);
        }
        $updateExec->bindValue(':mailUtilisateur', $post->mailSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':villeIntervenant', $post->villeSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':codePostalIntervenant', $post->cpSalarie, PDO::PARAM_STR);
        $updateExec->bindValue(':adresseIntervenant_A', $post->adresseSalarieA, PDO::PARAM_STR);
        $updateExec->bindValue(':adresseIntervenant_B', $post->adresseSalarieB, PDO::PARAM_STR);

        $updateExec->bindValue(':mailManager', $post->mailManager, PDO::PARAM_STR);
        $updateExec->bindValue(':typeIntervenantExpensia', $post->typeSalarieExpensia, PDO::PARAM_STR);
        $updateExec->bindValue(':contratIntervenant', $post->contratSalarie, PDO::PARAM_STR);

        $updateExec->bindValue(':idUtilisateur', $post->idSalarie, PDO::PARAM_STR);

        if ($updateExec->execute()) {
            print json_encode(array(
                'result' => $post->idSalarie
            ));
        }
        else {
            print json_encode(array(
                'result' => 0
            ));
        }
    }
}
else {
    print json_encode(array(
        'result' => 0
    ));
}

