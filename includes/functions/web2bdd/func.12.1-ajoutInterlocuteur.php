<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'un interlocuteur
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';

if(filter_has_var(INPUT_POST, 'ztMailInterlocuteur')){

    $AjoutInterlocuteurAgenceExc->bindValue(':tel', filter_input(INPUT_POST, 'ztTelInterlocuteur'));
    $AjoutInterlocuteurAgenceExc->bindValue(':nom', filter_input(INPUT_POST, 'ztNomInterlocuteur'));
    $AjoutInterlocuteurAgenceExc->bindValue(':prenom', filter_input(INPUT_POST, 'ztPrenomInterlocuteur'));
    $AjoutInterlocuteurAgenceExc->bindValue(':mail', filter_input(INPUT_POST, 'ztMailInterlocuteur'));

    print json_encode(array(
        'result' => ($AjoutInterlocuteurAgenceExc->execute()) ? 1 : 0
    ));
}

