<?php
/**
 * Enregistrement des dates DUE
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** On test la presence du POST */
if(isset($_POST['listeContrat_14_1'])){

    /** On enregistre le POST en session */
    $_SESSION['listeContrat_14_1'] = $_POST['listeContrat_14_1'];

    $nbTotal = 0;

    /** On enregistre les dates */
    foreach($_SESSION['listeContrat_14_1'] as $idContrat) {
        $InsertionDateDueExc->bindValue(':idContrat', $idContrat);
        $InsertionDateDueExc->bindValue(':date', date('Y-m-d H:i:s'));
        $nbTotal += ($InsertionDateDueExc->execute()) ? 1 : 0;
    }

    print json_encode(array(
        'totalOK' => $nbTotal,
        'total' => sizeof($_SESSION['listeContrat_14_1'])
    ));
}
