<?php

/** ---------------------------------------------------------------------------------------------
 * Modification (uniquement) du flag ACTIF
 *
 * @author Vincent BENNER - Page UP
 * @copyright Page UP
 */

/** ---------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** ---------------------------------------------------------------------------------------------
 * Requete sql
 */
$sqlModificationFlagActif = '
UPDATE su_pdv SET
  boolActif = NOT (boolActif)  
WHERE idPdv = :idPDV
';

$sqlGetNewFlagValue = '
SELECT boolActif
FROM su_pdv  
WHERE idPdv = :idPDV
';
$modificationFlagActifExec = DbConnexion::getInstance()->prepare($sqlModificationFlagActif);
$getNewFlagValueExec = DbConnexion::getInstance()->prepare($sqlGetNewFlagValue);

if(filter_has_var(INPUT_POST, 'boolActif')){
    $modificationFlagActifExec->bindValue(':idPDV', filter_input(INPUT_POST, 'idPDV'));
    $modificationFlagActifExec->execute();

    $getNewFlagValueExec->bindValue(':idPDV', filter_input(INPUT_POST, 'idPDV'));
    $getNewFlagValueExec->execute();
    $getNewFlagValue = $getNewFlagValueExec->fetch(PDO::FETCH_OBJ);


    print json_encode(array(
        'result' => $getNewFlagValue->boolActif
    ));
}