<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Maj des informations principales d'un point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.web2bdd.php';
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

if(filter_has_var(INPUT_POST, 'type') && filter_has_var(INPUT_POST, 'idPdv')){


    switch(filter_input(INPUT_POST, 'type')) {
        case 1:
            /** -----------------------------------------------------------------------------------
             * Détection CODE MERVAL
             */
            $sqlDetectDoublon = '
            SELECT COUNT(idPdv) AS LeCount
            FROM su_pdv
            WHERE idPdv <> :idPdv
            AND codeMerval = :codeMerval
            ';
            $detectDoublonExec = DbConnexion::getInstance()->prepare($sqlDetectDoublon);
            $detectDoublonExec->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
            $detectDoublonExec->bindValue(':codeMerval', filter_input(INPUT_POST, 'ztMerval'));
            $detectDoublonExec->execute();
            $detectDoublon = $detectDoublonExec->fetchObject();
            $count = $detectDoublon->LeCount;
            if ($count == 1) {
                print json_encode(array(
                    'result' => 'Ce code CIP est déjà utilisé'
                ));
                return;
            }

            $MajInformationsPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
            $MajInformationsPdvExc->bindValue(':libellePdv', filter_input(INPUT_POST, 'ztLibelle'));
            $MajInformationsPdvExc->bindValue(':adressePdv_A', filter_input(INPUT_POST, 'ztAdresseA'));
            $MajInformationsPdvExc->bindValue(':adressePdv_B', filter_input(INPUT_POST, 'ztAdresseB'));
            $MajInformationsPdvExc->bindValue(':adressePdv_C', filter_input(INPUT_POST, 'ztAdresseC'));
            $MajInformationsPdvExc->bindValue(':codePostalPdv', filter_input(INPUT_POST, 'ztCodePostal'));
            $MajInformationsPdvExc->bindValue(':villePdv', filter_input(INPUT_POST, 'ztVille'));
            $MajInformationsPdvExc->bindValue(':latitude', filter_input(INPUT_POST, 'ztLatitude'));
            $MajInformationsPdvExc->bindValue(':longitude', filter_input(INPUT_POST, 'ztLongitude'));
            $MajInformationsPdvExc->bindValue(':commentairePdv', filter_input(INPUT_POST, 'ztCommentaire'));
            $MajInformationsPdvExc->bindValue(':commentairePdv_EM', filter_input(INPUT_POST, 'ztCommentaireEM'));
            $MajInformationsPdvExc->bindValue(':codeMerval', filter_input(INPUT_POST, 'ztMerval'));
            $MajInformationsPdvExc->bindValue(':codeMerval_old', filter_input(INPUT_POST, 'ztOldMerval'));

            $MajInformationsPdvExc->bindValue(':categorie', filter_input(INPUT_POST, 'zsCategorie'));
            $MajInformationsPdvExc->bindValue(':souscategorie', filter_input(INPUT_POST, 'zsSousCategorie'));

            $MajInformationsPdvExc->bindValue(':ciblage', filter_input(INPUT_POST, 'zsCiblage'));

            print json_encode(array(
                'result' => ($MajInformationsPdvExc->execute()) ? 1 : json_encode($MajInformationsPdvExc->errorInfo())
            ));
            break;
        case 2:
            $MajInformationsContactPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
            $MajInformationsContactPdvExc->bindValue(':nomTitulairePdv_A', filter_input(INPUT_POST, 'ztNomTitulaireA'));
            $MajInformationsContactPdvExc->bindValue(':prenomTitulairePdv_A', filter_input(INPUT_POST, 'ztPrenomTitulaireA'));
            $MajInformationsContactPdvExc->bindValue(':nomTitulairePdv_B', filter_input(INPUT_POST, 'ztNomTitulaireB'));
            $MajInformationsContactPdvExc->bindValue(':prenomTitulairePdv_B', filter_input(INPUT_POST, 'ztPrenomTitulaireB'));
            $MajInformationsContactPdvExc->bindValue(':telephoneMagasin', filter_input(INPUT_POST, 'ztTelephoneTitulaire'));
            $MajInformationsContactPdvExc->bindValue(':faxPdv', filter_input(INPUT_POST, 'ztFaxTitulaire'));
            $MajInformationsContactPdvExc->bindValue(':emailPdv', filter_input(INPUT_POST, 'ztEmailTitulaire'));
            print json_encode(array(
                'result' => ($MajInformationsContactPdvExc->execute()) ? 1 : json_encode($MajInformationsContactPdvExc->errorInfo())
            ));
        break;
        case 3:
            $RechercheInterlocuteurPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
            $RechercheInterlocuteurPdvExc->execute();
            if($RechercheInterlocuteurPdvExc->rowCount() > 0){
                
                $InfoInterlocuteur = $RechercheInterlocuteurPdvExc->fetch(PDO::FETCH_OBJ);
             
                $MajInterlocuteurPdvExc->bindValue(':idInterlocuteur', $InfoInterlocuteur->idInterlocuteurPdv);
                $MajInterlocuteurPdvExc->bindValue(':nomInterlocuteurPdv', filter_input(INPUT_POST, 'ztNomInterlocuteur'));
                $MajInterlocuteurPdvExc->bindValue(':prenomInterlocuteurPdv', filter_input(INPUT_POST, 'ztPrenomInterlocuteur'));
                $MajInterlocuteurPdvExc->bindValue(':telephoneInterlocuteurPdv', filter_input(INPUT_POST, 'ztTelInterlocuteur_A'));
                $MajInterlocuteurPdvExc->bindValue(':faxInterlocuteurPdv', filter_input(INPUT_POST, 'ztTelInterlocuteur_B'));
                $MajInterlocuteurPdvExc->bindValue(':mailInterlocuteurPdv', filter_input(INPUT_POST, 'ztEmailInterlocuteur'));
                $MajInterlocuteurPdvExc->bindValue(':fonctionInterlocuteurPdv', filter_input(INPUT_POST, 'zsFonctionInterlocuteur'));
                print json_encode(array(
                    'result' => ($MajInterlocuteurPdvExc->execute()) ? 1 : json_encode($MajInterlocuteurPdvExc->errorInfo())
                ));
            }else {
                $AjoutInterlocuteurPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
                $AjoutInterlocuteurPdvExc->bindValue(':nomInterlocuteurPdv', filter_input(INPUT_POST, 'ztNomInterlocuteur'));
                $AjoutInterlocuteurPdvExc->bindValue(':prenomInterlocuteurPdv', filter_input(INPUT_POST, 'ztPrenomInterlocuteur'));
                $AjoutInterlocuteurPdvExc->bindValue(':telephoneInterlocuteurPdv', filter_input(INPUT_POST, 'ztTelInterlocuteur_A'));
                $AjoutInterlocuteurPdvExc->bindValue(':faxInterlocuteurPdv', filter_input(INPUT_POST, 'ztTelInterlocuteur_B'));
                $AjoutInterlocuteurPdvExc->bindValue(':mailInterlocuteurPdv', filter_input(INPUT_POST, 'ztEmailInterlocuteur'));
                $AjoutInterlocuteurPdvExc->bindValue(':fonctionInterlocuteurPdv', filter_input(INPUT_POST, 'zsFonctionInterlocuteur'));
                print json_encode(array(
                    'result' => ($AjoutInterlocuteurPdvExc->execute()) ? 1 : json_encode($AjoutInterlocuteurPdvExc->errorInfo())
                ));
            }
        break;
    }
}
