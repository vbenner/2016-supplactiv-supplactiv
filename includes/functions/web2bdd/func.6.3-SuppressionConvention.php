<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Suppression d'une convention
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

# ------------------------------------------------------------------------------
# PREPA REQUETE - Modification d'une affiliation 
# ------------------------------------------------------------------------------
$sqlModificationIntervention = "
DELETE FROM su_contrat_cidd WHERE idContrat = :IdContrat";
$ModificationInterventionExc = DbConnexion::getInstance()->prepare($sqlModificationIntervention);


# ------------------------------------------------------------------------------
# Passage du post en objet
# ------------------------------------------------------------------------------
$InfoIntervention = (object)$_POST;

$ModificationInterventionExc->bindValue(':IdContrat', 	$_POST['IdContrat'], 	PDO::PARAM_INT);
echo ($ModificationInterventionExc->execute()) ? 'OK' : 'KO';

?>