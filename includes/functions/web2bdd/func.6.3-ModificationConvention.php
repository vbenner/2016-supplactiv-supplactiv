<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Modification d'une convention
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';


$sqlModificationConvention = '
UPDATE su_contrat_cidd SET
	dateContrat 			= :DateConvention,
	dureeMensuelle 		= :DureeMensuelle,
	remunerationBrute 		= :Remuneration,
	indemniteForfaitaire 	= :IndemniteForfaitaire,
	listePDV 				= :ListePdv
WHERE idContrat = :IdContrat';
$ModificationConventionExc = DbConnexion::getInstance()->prepare($sqlModificationConvention);

$ModificationConventionExc->bindValue(':IdContrat', 			$_SESSION['IDContrat_Recherche_6_5'], 					PDO::PARAM_INT); 
$ModificationConventionExc->bindValue(':DateConvention', 		$_POST['AnneeConvention'] . '-' . $arrayMonthCorres2[$_POST['MoisConvention']] . '-01', PDO::PARAM_STR);
$ModificationConventionExc->bindValue(':DureeMensuelle', 		$_POST['DureeMensuelle'], 								PDO::PARAM_STR); 
$ModificationConventionExc->bindValue(':Remuneration', 			$_POST['TauxHoraire'], 									PDO::PARAM_STR); 
$ModificationConventionExc->bindValue(':IndemniteForfaitaire', 	$_POST['IndemniteF'], 									PDO::PARAM_STR); 
$ModificationConventionExc->bindValue(':ListePdv', 				$_POST['ListePdv'], 									PDO::PARAM_STR); 
echo ($ModificationConventionExc->execute()) ? 'OK' : 'KO';

