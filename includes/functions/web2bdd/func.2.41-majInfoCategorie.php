<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Maj des informations principales d'un point de vente
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

if (filter_has_var(INPUT_POST, 'type') &&
    filter_has_var(INPUT_POST, 'idCategorie')) {

    switch (filter_input(INPUT_POST, 'type')) {
        case 1:
            $sqlUpdateCategorie = '
            UPDATE su_pdv_categorie
            SET libelleCategoriePdv = :libelle
            WHERE idCategoriePdv = :id
            ';
            $updateCategorieExec = DbConnexion::getInstance()->prepare($sqlUpdateCategorie);
            $updateCategorieExec->bindValue(':id', filter_input(INPUT_POST, 'idCategorie'));
            $updateCategorieExec->bindValue(':libelle', filter_input(INPUT_POST, 'ztLibelle'));
            print json_encode(array(
                'result' => ($updateCategorieExec->execute()) ? 1 : json_encode($updateCategorieExec->errorInfo())
            ));
            break;
        case 2:
            $MajInformationsContactPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'));
            $MajInformationsContactPdvExc->bindValue(':nomTitulairePdv_A', filter_input(INPUT_POST, 'ztNomTitulaireA'));
            $MajInformationsContactPdvExc->bindValue(':prenomTitulairePdv_A', filter_input(INPUT_POST, 'ztPrenomTitulaireA'));
            $MajInformationsContactPdvExc->bindValue(':nomTitulairePdv_B', filter_input(INPUT_POST, 'ztNomTitulaireB'));
            $MajInformationsContactPdvExc->bindValue(':prenomTitulairePdv_B', filter_input(INPUT_POST, 'ztPrenomTitulaireB'));
            $MajInformationsContactPdvExc->bindValue(':telephoneMagasin', filter_input(INPUT_POST, 'ztTelephoneTitulaire'));
            $MajInformationsContactPdvExc->bindValue(':faxPdv', filter_input(INPUT_POST, 'ztFaxTitulaire'));
            $MajInformationsContactPdvExc->bindValue(':emailPdv', filter_input(INPUT_POST, 'ztEmailTitulaire'));
            print json_encode(array(
                'result' => ($MajInformationsContactPdvExc->execute()) ? 1 : json_encode($MajInformationsContactPdvExc->errorInfo())
            ));
            break;
    }
}
