<?php

/** ---------------------------------------------------------------------------------------------
 * Modification (uniquement) de la PHOTO
 *
 * @author Vincent BENNER - Page UP
 * @copyright Page UP
 */

/** ---------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** ---------------------------------------------------------------------------------------------
 * Requete sql
 */
$sqlModificationCV = '
UPDATE su_intervenant
SET  b64_cv = :cv
WHERE idIntervenant = :id
';

$modificationCVExec = DbConnexion::getInstance()->prepare($sqlModificationCV);
$modificationCVExec->bindValue(':id', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
$modificationCVExec->bindValue(':cv', filter_input(INPUT_POST, 'cv'), PDO::PARAM_STR);
$res = $modificationCVExec->execute();

print json_encode(array(
    'result' => $res,
    'cv' => ''
));
