<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$currYear = date('Y');
$yearC = array();
$noms = array();
$dissociate = $_POST['dissociate'] == 1 ? true : false;

/** -----------------------------------------------------------------------------------------------
 * On est obligé de mettre tous les noms des interlocuteurs qui sont connus pendant ces dernières
 * 6 années (5 + 1 pour calculer le taux avec la formule Y-1
 * Du coup, on prépare déjà le gros tableau final
 */
$sqlGetAllInterlocuteurs = '
SELECT DISTINCT SU.nomInterlocuteurAgence AS nom
FROM su_campagne SC
  INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
WHERE dateDebut >= :du
AND dateFin <= :au
AND SU.nomInterlocuteurAgence <> \'\'
ORDER BY nom ASC
';

$getAllInterlocuteursExec = DbConnexion::getInstance()->prepare($sqlGetAllInterlocuteurs);
$getAllInterlocuteursExec->bindValue(':du', ($currYear-5).'-01-01', PDO::PARAM_STR);
$getAllInterlocuteursExec->bindValue(':au', ($currYear).'-12-31', PDO::PARAM_STR);
$getAllInterlocuteursExec->execute();


while($getAllInterlocuteurs = $getAllInterlocuteursExec->fetch(PDO::FETCH_OBJ)) {

    // --------------------------------------------------------------------------------------------
    // On récupère juste pour simplifier la suite
    // --------------------------------------------------------------------------------------------
    $nom = $getAllInterlocuteurs->nom;

    // --------------------------------------------------------------------------------------------
    // On va se souvenir des noms
    // --------------------------------------------------------------------------------------------
    $noms[] = $nom;

    $tab = array();
    for ($i = $currYear-5 ; $i <= $currYear ; $i++ ){
        $tab[$i]['year'] = $i;
        $tab[$i]['nb'] = 0;
        $tab[$i]['txi'] = '-';
        $tab[$i]['txy'] = '-';
    }
    if ($dissociate == 1) {
        $yearC[$nom] = array(
            'nom' => $nom,
            'years' => $tab
        );
    } else {
        $yearC['_CAMPAGNE'] = array(
            'nom' => $nom,
            'years' => $tab
        );
    }
}
#echo '<pre>';
#print_r($noms);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 */
for ($i = $currYear-5 ; $i <= $currYear ; $i++ ){

    $sqlCountCampagnes = '
    SELECT COUNT(SC.idCampagne) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_campagne SC
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE dateDebut >= :du
    AND dateFin <= :au
    AND SU.nomInterlocuteurAgence <> \'\' 
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countCampagnesExec = DbConnexion::getInstance()->prepare($sqlCountCampagnes);
    $countCampagnesExec->bindValue(':du', $i.'-01-01', PDO::PARAM_STR);
    $countCampagnesExec->bindValue(':au', $i.'-12-31', PDO::PARAM_STR);
    $countCampagnesExec->execute();
    while ($count = $countCampagnesExec->fetch(PDO::FETCH_OBJ)) {
        if ($dissociate == 1) {
            $yearC[$count->nom]['years'][$i]['nb'] = $count->leCount;
        }
        else {
            $yearC['_CAMPAGNE']['years'][$i]['nb'] += $count->leCount;
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on fait les calculs de taux salarié / salarié
 * Mais on ne fera pas l'année $currYear - 5
 */
if ($dissociate) {
    foreach ($noms as $dummy => $nom) {

        /** ---------------------------------------------------------------------------------------
         * Pour les années, c'est assez facile, on n'a que les 5 années dans la liste
         */
        $hasData = 0;
        for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
            if ($yearC[$nom]['years'][$i-1]['nb'] != 0) {
                $yearC[$nom]['years'][$i]['txy'] = (($yearC[$nom]['years'][$i]['nb'] - $yearC[$nom]['years'][$i-1]['nb']) / $yearC[$nom]['years'][$i-1]['nb']);
                $yearC[$nom]['years'][$i]['txy'] = round(100 * $yearC[$nom]['years'][$i]['txy'], 2 ).' %';
            }
            else {
                $yearC[$nom]['years'][$i]['txy'] = '-';
            }
        }
        unset($yearC[$nom]['years'][$currYear-5]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = $currYear - 4 ;  $i <= $currYear ; $i++ ) {
            $hasData += $yearC[$nom]['years'][$i]['nb'];
        }
        if ($hasData == 0) {
            unset($yearC[$nom]);
        }
    }
}
else {
    $yearC['_CAMPAGNE']['nom'] = '-';
    for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
        if ($yearC['_CAMPAGNE']['years'][$i-1]['nb'] != 0) {
            $yearC['_CAMPAGNE']['years'][$i]['txy'] = (($yearC['_CAMPAGNE']['years'][$i]['nb'] - $yearC['_CAMPAGNE']['years'][$i-1]['nb']) / $yearC['_CAMPAGNE']['years'][$i-1]['nb']);
            $yearC['_CAMPAGNE']['years'][$i]['txy'] = round(100 * $yearC['_CAMPAGNE']['years'][$i]['txy'], 2 ).' %';
        }
        else {
            $yearC['_CAMPAGNE']['years'][$i]['txy'] = '-';
        }
    }
    unset($yearC['_CAMPAGNE']['years'][$currYear-5]);
}

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'DATAY' => $yearC,
        //'DATAT' => $trimestre,
        //'DATAS' => $semestre,
    )
);