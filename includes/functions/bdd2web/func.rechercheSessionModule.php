<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche / Initialisation des sessions suivant le module passé en parametre
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

$dateFin = new DateTime('last day of this month');

/** On test la presence du module */
if(filter_has_var(INPUT_POST, 'idModule') && filter_input(INPUT_POST, 'idModule') != ''){

    /** Action specifique suivant le module */
    switch(filter_input(INPUT_POST, 'idModule')){
        case '5_1':
        case '13_0':
        case '14_1':
        case '5_2':
        case '17_5':
        case '20_1':
            foreach($_POST as $libellePost => $defaut){
                if(substr($libellePost, 0, 4) !== 'date'){
                    $_SESSION[$libellePost] = (isset($_SESSION[$libellePost]) && !empty($_SESSION[$libellePost])) ? $_SESSION[$libellePost] : $defaut;
                }else {
                    $_SESSION[$libellePost] = (isset($_SESSION[$libellePost]) && !empty($_SESSION[$libellePost])) ? $_SESSION[$libellePost] : ((substr($libellePost, 0, 14) == 'dateRechercheD') ? date('01/m/Y') : $dateFin->format('d/m/Y'));
                }
            }
        break;
    }

    $listeSession = array();
    foreach($_POST as $libellePost => $defaut){
        $listeSession[$libellePost] = $_SESSION[$libellePost];
    }

    print json_encode(array('sessions' => $listeSession));
}
