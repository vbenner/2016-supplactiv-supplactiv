<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur un client
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idClient' )) {

    /** Recherche du point de vente */
    $RechercheInfoClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient'), PDO::PARAM_INT);
    $RechercheInfoClientExc->execute();


    $RechercheInterlocuteurClientExc->bindValue(':idClient', filter_input(INPUT_POST, 'idClient'), PDO::PARAM_INT);
    $RechercheInterlocuteurClientExc->execute();

    $RechercheActiviteClientExc->execute();
    $RechercheFiliereClientExc->execute();

    $RechercheTitreInterlocuteurClientExc->execute();

    $RechercheInterlocuteurAgenceExc->execute();

    if($RechercheInfoClientExc->rowCount() > 0){
        print json_encode(array(
            'result' => 1,
            'infoClient' => $RechercheInfoClientExc->fetch(PDO::FETCH_OBJ),
            'activites' => $RechercheActiviteClientExc->fetchAll(PDO::FETCH_OBJ),
            'filieres' => $RechercheFiliereClientExc->fetchAll(PDO::FETCH_OBJ),
            'interlocuteurs' => $RechercheInterlocuteurClientExc->fetchAll(PDO::FETCH_OBJ),
            'interlocuteursAgence' => $RechercheInterlocuteurAgenceExc->fetchAll(PDO::FETCH_OBJ),
            'titres_interlocuteurs' => $RechercheTitreInterlocuteurClientExc->fetchAll(PDO::FETCH_OBJ)
        ));
    }else {
        print json_encode(array(
            'result' => 0,
            'activites' => $RechercheActiviteClientExc->fetchAll(PDO::FETCH_OBJ),
            'filieres' => $RechercheFiliereClientExc->fetchAll(PDO::FETCH_OBJ),
            'titres_interlocuteurs' => $RechercheTitreInterlocuteurClientExc->fetchAll(PDO::FETCH_OBJ)
        ));
    }
}