<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur un point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idPdv' )) {

    /** Recherche du point de vente */
    $RechercheInfoPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'), PDO::PARAM_INT);
    $RechercheInfoPdvExc->execute();
    if($RechercheInfoPdvExc->rowCount() > 0){
        print json_encode(array(
            'result' => 1,
            'infoPdv' => $RechercheInfoPdvExc->fetch(PDO::FETCH_OBJ)
        ));
    }else {
        print json_encode(array(
            'result' => 0
        ));
    }
}