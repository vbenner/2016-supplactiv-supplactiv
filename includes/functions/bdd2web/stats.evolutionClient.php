<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des CA
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Filtre facultatif
 */
$idCampagne = 0;
$currYear = date('Y');
if ($_POST['campagne'] != '') {
    $filtreCampagne = ' AND idCampagne = :idc ';
    $idCampagne = $_POST['campagne'];

    /** -------------------------------------------------------------------------------------------
     * On récupère l'année
     */
    $sqlGetCampagne = '
    SELECT *
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
    $getCampagneExec->bindValue(':idc', $idCampagne, PDO::PARAM_INT);
    $getCampagneExec->execute();
    $getCampagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);
    $currYear = substr($getCampagne->dateFin, 0, 4);
}
else {
    $filtreCampagne = '';
}

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$resultY = array();
$resultS = array();
$resultT = array();
$resultM = array();
$resultC = array();

$noms = array();

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des années avec tout ce qui va bien dedans
 * txi = taux vs taux (i - 1)
 * txy = taux vs taux (y - 1)
 */
$cpt = 0;

for ($i = $currYear - 5; $i <= $currYear; $i++) {
    $resultY[$i] = array(
        'nb' => 0,
        'txi' => '-',
        'txy' => '-',
    );
    /** -------------------------------------------------------------------------------------------
     * SEMESTRES
     */
    for ($j = 1; $j <= 2 ; $j++){
        $resultS[$i][$j] = array(
            'nb' => 0,
            'txi' => '-',
            'txy' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * TRIMESTRES
     */
    for ($j = 1; $j <= 4 ; $j++){
        $resultT[$i][$j] = array(
            'nb' => 0,
            'txi' => '-',
            'txy' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * MOIS
     */
    for ($j = 1; $j <= 12 ; $j++){
        $resultM[$i][$j] = array(
            'nb' => 0,
            'txi' => '-',
            'txy' => '-',
        );

        $resultMM[$cpt++] = array(
            'nb' => 0,
            'txi' => '-',
            'txy' => '-',
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des jours de interventions pour une campagne donnée regroupée par années
 * On fait déjà pour les animations
 *
 * Si on est en mode SANS filtre CAMPAGNE, c'est assez facile, on n'a pas de restriction
 */
if ($filtreCampagne == '') {

    /** -------------------------------------------------------------------------------------------
     * EXTRACTION GLOBALE
     * Par MOIS
     */

    // --------------------------------------------------------------------------------------------
    // On construit la requête (attention, PDO n'autorise pas le bind d'un champ
    // --------------------------------------------------------------------------------------------
    $sqlGetAllDays = "
    SELECT COUNT(idClient) AS lecount, YEAR(dateCreation) AS lannee, MONTH(dateCreation) AS lemois
    FROM su_client
    WHERE NOT ISNULL(dateCreation)
      AND dateCreation >= :du
      AND dateCreation <= :au
    GROUP BY lannee, lemois
    ORDER BY lannee, lemois
    ";

    $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
    $getAllDaysExec->bindValue(':du', ($currYear-5).'-01-01', PDO::PARAM_STR);
    $getAllDaysExec->bindValue(':au', ($currYear).'-12-31', PDO::PARAM_STR);
    $getAllDaysExec->execute();

    /** -------------------------------------------------------------------------------------------
     * Traitement
     */
    while($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

        // ----------------------------------------------------------------------------------------
        // Partie commune
        // ----------------------------------------------------------------------------------------
        $year = $getAllDays->lannee;

        // ----------------------------------------------------------------------------------------
        // La partie pour les mois
        // ----------------------------------------------------------------------------------------
        $mois = $getAllDays->lemois;
        $resultM[$year][$mois]['nb'] += $getAllDays->lecount;

        // ----------------------------------------------------------------------------------------
        // On tente une conversion de $year / $mois en indice à partir de 0
        // ----------------------------------------------------------------------------------------
        $indice = (($year-($currYear-5)) * 12 + $mois)-1;
        $resultMM[$indice]['nb'] += $getAllDays->lecount;

        // ----------------------------------------------------------------------------------------
        // Ensuite, on trie / trimestre
        // ----------------------------------------------------------------------------------------
        $trimestre = monthToTrimestre($getAllDays->lemois);
        $resultT[$year][$trimestre]['nb'] += $getAllDays->lecount;

        // ----------------------------------------------------------------------------------------
        // Ensuite, on trie / semestre
        // ----------------------------------------------------------------------------------------
        $semestre = monthToSemestre($getAllDays->lemois);
        $resultS[$year][$semestre]['nb'] += $getAllDays->lecount;

        // ----------------------------------------------------------------------------------------
        // Ensuite, on trie / yeas
        // ----------------------------------------------------------------------------------------
        $resultY[$year]['nb'] += $getAllDays->lecount;

    }

    /** -------------------------------------------------------------------------------------------
     * Liste des CLIENTS
     */
    $sqlGetClients = "
    SELECT DISTINCT su_client.libelleClient, YEAR(dateCreation) AS lannee
    FROM su_client
    WHERE NOT ISNULL(dateCreation)
      AND dateCreation >= :du
      AND dateCreation <= :au
    ORDER BY libelleClient
    ";
    $getClientsExec = DbConnexion::getInstance()->prepare($sqlGetClients);
    $getClientsExec->bindValue(':du', ($currYear-5).'-01-01', PDO::PARAM_STR);
    $getClientsExec->bindValue(':au', ($currYear).'-12-31', PDO::PARAM_STR);
    $getClientsExec->execute();
    while($getClients = $getClientsExec->fetch(PDO::FETCH_OBJ)) {

        // ----------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        $resultC[] = array(
            'LIB' => $getClients->libelleClient,
            'YEAR' => $getClients->lannee,
        );

    }


} else {

    /** -------------------------------------------------------------------------------------------
     * Pour le cas où la campagne est identifiée, on doit faire l'action pour chaque année et donc
     * reprendre la campagne précédente.
     */
    $campagnes[$currYear] = $_POST['campagne'];
    for ($i = $currYear-1 ; $i >= $currYear-5 ; $i-- ) {
        $campagnes[ $i ] = getCampagneIDPrec($campagnes[ $i + 1 ]);
    }

    /** -------------------------------------------------------------------------------------------
     * On fait donc maintenant l'analyse ID / ID (sans tenir compte des dates
     *
     * Traitement par ANNEE
     */
    foreach ($campagnes as $year => $idc) {

        /** ---------------------------------------------------------------------------------------
         * On traite les ANIMATIONS
         * TRAITEMENT MENSUEL
         */
        $sqlGetAllDays = "
		SELECT COUNT(idClient) AS lecount, YEAR(dateCreation) AS lannee, MONTH(dateCreation) AS lemois
		FROM su_client SCL
			INNER JOIN su_client_interlocuteur SCI ON SCI.FK_idClient = SCL.idClient
			INNER JOIN su_campagne SCC ON SCC.FK_idInterlocuteurClient = SCI.idInterlocuteurClient
        WHERE NOT ISNULL(dateCreation)
        AND SCC.idCampagne = :idc
		GROUP BY lannee, lemois
		ORDER BY lannee, lemois
        ";

        $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
        $getAllDaysExec->bindValue(':idc', $idc, PDO::PARAM_STR);
        $getAllDaysExec->execute();

        /** ---------------------------------------------------------------------------------------
         * Traitement
         */
        while($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

            // ------------------------------------------------------------------------------------
            // Partie commune
            // ------------------------------------------------------------------------------------
            $year = $getAllDays->lannee;

            // ------------------------------------------------------------------------------------
            // La partie pour les mois
            // ------------------------------------------------------------------------------------
            $mois = $getAllDays->lemois;
            $resultM[$year][$mois]['nb'] += $getAllDays->lecount;

            // ------------------------------------------------------------------------------------
            // Ensuite, on trie / trimestre
            // ------------------------------------------------------------------------------------
            $trimestre = monthToTrimestre($getAllDays->lemois);
            $resultT[$year][$trimestre]['nb'] += $getAllDays->lecount;

            // ------------------------------------------------------------------------------------
            // Ensuite, on trie / semestre
            // ------------------------------------------------------------------------------------
            $semestre = monthToSemestre($getAllDays->lemois);
            $resultS[$year][$semestre]['nb'] += $getAllDays->lecount;

            // ------------------------------------------------------------------------------------
            // Ensuite, on trie / year
            // ------------------------------------------------------------------------------------
            $resultY[$year]['nb'] += $getAllDays->lecount;

        }


        /** ---------------------------------------------------------------------------------------
         * On traite les ANIMATIONS
         * TRAITEMENT MENSUEL
         */
        $sqlGetAllClients = "
		SELECT DISTINCT SCL.libelleClient, YEAR(dateCreation) AS lannee
		FROM su_client SCL
			INNER JOIN su_client_interlocuteur SCI ON SCI.FK_idClient = SCL.idClient
			INNER JOIN su_campagne SCC ON SCC.FK_idInterlocuteurClient = SCI.idInterlocuteurClient
        WHERE NOT ISNULL(dateCreation)
        AND SCC.idCampagne = :idc
        ";

        #echo 'IDC = '.$idc.'<br/>';
        #die($sqlGetAllClients);

        $getAllClientsExec = DbConnexion::getInstance()->prepare($sqlGetAllClients);
        $getAllClientsExec->bindValue(':idc', $idc, PDO::PARAM_STR);
        $getAllClientsExec->execute();
        while($getAllClients = $getAllClientsExec->fetch(PDO::FETCH_OBJ)) {

            // ----------------------------------------------------------------------------------------
            // ----------------------------------------------------------------------------------------
            $resultC[] = array(
                'LIB' => $getAllClients->libelleClient,
                'YEAR' => $getAllClients->lannee,
            );
        }


    }
}


/** -----------------------------------------------------------------------------------------------
 * On vient de construire le tableau des années.
 * On va calculer maintenant les différents TAUX
 */

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 * Sans tenir compte de la première année
 */
for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {

    // --------------------------------------------------------------------------------------------
    // On traite les ANNEES d'abord
    // Traitement des années précédente
    // --------------------------------------------------------------------------------------------
    if ($resultY[$i-1]['nb'] != 0) {
        $resultY[$i]['txy'] = (($resultY[$i]['nb'] - $resultY[$i-1]['nb']) / $resultY[$i-1]['nb']);
        $resultY[$i]['txy'] = round(100 * $resultY[$i]['txy'], 2 ).' %';
    }
    else {
        $resultY[$i]['txy'] = '-';
    }

    // --------------------------------------------------------------------------------------------
    // On traite ensuite les SEMESTRES
    // --------------------------------------------------------------------------------------------
    for ($semestre = 1 ; $semestre <= 2 ; $semestre++ ) {

        /** ---------------------------------------------------------------------------------------
         * On compare donc le trimestre avec le trimestre de l'année d'avant
         * on fait (Y - (Y-1)) / (Y-1)
         */
        if ($resultS[$i-1][$semestre]['nb'] != 0) {
            $ca = $resultS[$i][$semestre]['nb'] ;
            $caPrec = $resultS[$i-1][$semestre]['nb'] ;
            $resultS[$i][$semestre]['txy'] = (($ca - $caPrec) / $caPrec);
            $resultS[$i][$semestre]['txy'] = round(100 * $resultS[$i][$semestre]['txy'], 2 ).' %';
        }
        else {
            $resultS[$i][$semestre]['txy'] = '-';
        }

        /** ---------------------------------------------------------------------------------------
         * Maintenant on compare avec le semestre précédent (i-1)
         * Ce qui est plus compliqué
         * 2018-02 => 2018-01
         * 2018-01 => 2017-02
         */
        $semestrePrec =($semestre == 1 ? 2 : 1);
        $yearPrec = ($semestre == 1 ? $i - 1 : $i);
        if ($resultS[$yearPrec][$semestrePrec]['nb'] != 0) {

            $ca = $resultS[$i][$semestre]['nb'] ;
            $caPrec = $resultS[$yearPrec][$semestrePrec]['nb'] ;

            $resultS[$i][$semestre]['txi'] = (($ca - $caPrec) / $caPrec);
            $resultS[$i][$semestre]['txi'] = round(100 * $resultS[$i][$semestre]['txi'], 2 ).' %';
        }
        else {
            $resultS[$i][$semestre]['txi'] = '-';
        }
    }


    // --------------------------------------------------------------------------------------------
    // On traite ensuite les TRIMESTRES
    // --------------------------------------------------------------------------------------------
    for ($trimestre = 1 ; $trimestre <= 4 ; $trimestre++ ) {

        /** ---------------------------------------------------------------------------------------
         * On compare donc le trimestre avec le trimestre de l'année d'avant
         * on fait (Y - (Y-1)) / (Y-1)
         */
        if ($resultT[$i-1][$trimestre]['ca_'.$suffix] != 0) {
            $ca = $resultT[$i][$trimestre]['nb'] ;
            $caPrec = $resultT[$i-1][$trimestre]['nb'] ;
            $resultT[$i][$trimestre]['txy'] = (($ca - $caPrec) / $caPrec);
            $resultT[$i][$trimestre]['txy'] = round(100 * $resultT[$i][$trimestre]['txy'], 2 ).' %';
        }
        else {
            $resultT[$i][$trimestre]['txy'] = '-';
        }

        /** ---------------------------------------------------------------------------------------
         * Maintenant on compare avec le trimestre précédent (i-1)
         * Ce qui est plus compliqué
         * 2018/4 => 2018/3
         * 2018/3 => 2018/2
         * 2018/2 => 2018/1
         * 2018/1 => 2017/4
         */
        $trimestrePrec =($trimestre == 1 ? 4 : $trimestre - 1);
        $yearPrec = ($trimestre == 1 ? $i - 1 : $i);
        if ($resultT[$yearPrec][$trimestrePrec]['nb'] != 0) {

            $ca = $resultT[$i][$trimestre]['nb'] ;
            $caPrec = $resultT[$yearPrec][$trimestrePrec]['nb'] ;

            $resultT[$i][$trimestre]['txi'] = (($ca - $caPrec) / $caPrec);
            $resultT[$i][$trimestre]['txi'] = round(100 * $resultT[$i][$trimestre]['txi'], 2 ).' %';
        }
        else {
            $resultT[$i][$trimestre]['txi'] = '-';
        }
    }



    // --------------------------------------------------------------------------------------------
    // On traite ensuite les MOIS
    // --------------------------------------------------------------------------------------------
    #echo '<pre>';
    #print_r($resultM);
    #echo '</pre>';
    #die();

    for ($mois = 1 ; $mois <= 12 ; $mois++ ) {

        /** ---------------------------------------------------------------------------------------
         * On compare donc le mois avec le mois de l'année d'avant
         * on fait (Y - (Y-1)) / (Y-1)
         */
        #if ($resultM[$i-1][$mois]['nb'] != 0) {
        #    $ca = $resultM[$i][$mois]['nb'] ;
        #    $caPrec = $resultM[$i-1][$mois]['nb'] ;
        #    $resultM[$i][$mois]['txy'] = (($ca - $caPrec) / $caPrec);
        #    $resultM[$i][$mois]['txy'] = round(100 * $resultM[$i][$mois]['txy'], 2 ).' %';
        #}
        #else {
        #    $resultM[$i][$mois]['txy'] = '-';
        #}

        /** ---------------------------------------------------------------------------------------
         * Maintenant on compare avec le trimestre précédent (i-1)
         * Ce qui est plus compliqué
         * 2018/4 => 2018/3
         * 2018/3 => 2018/2
         * 2018/2 => 2018/1
         * 2018/1 => 2017/4
         */
        $moisPrec = ($mois == 1 ? 12 : $mois - 1);
        $yearPrec = ($mois == 1 ? $i -1 : $i);
        #echo '------ ANALYSE : '.$moisPrec.'/'.$yearPrec.'<br/>';
        if ($resultM[$yearPrec][$moisPrec]['nb'] != 0) {

            $ca = $resultM[$i][$mois]['nb'] ;
            $caPrec = $resultM[$yearPrec][$moisPrec]['nb'] ;

            #echo ' - - - - >>>'.$moisPrec.'/'.$yearPrec.' est != 0 => on calcule entre '.$ca.' et '.$caPrec.'<br/>';

            $resultM[$i][$mois]['txi'] = (($ca - $caPrec) / $caPrec);
            #if ($moisPrec == 3 && $yearPrec == 2018) {
                #print "On calcule = (($ca - $caPrec) / $caPrec)".'<br/>';
                #print 'On trouve '.$resultM[$i][$mois]['txi'].' avec $i = '.$i.' et $mois = '.$mois.'<br/>';
                #die();
            #}

            $resultM[$i][$mois]['txi'] = round(100 * $resultM[$i][$mois]['txi'], 2 ).' %';

            #if ($moisPrec == 3 && $yearPrec == 2018) {
            #    echo '<pre>';
            #    print_r($resultM);
            #    echo '</pre>';
            #    die();
            #}
        }
        else {
            $resultM[$i][$mois]['txi'] = '-';

            #echo ' - - - - >>>'.'00'.'<br/>';
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * Pour les mois, on trzaite de 1 à 60
 */
for ($i = 1; $i <= 60; $i++) {

}

unset($resultY[$currYear-5]);
unset($resultS[$currYear-5]);
unset($resultT[$currYear-5]);

unset($resultM[$currYear-5]);
unset($resultM[$currYear-4]);
unset($resultM[$currYear-3]);
unset($resultM[$currYear-2]);

/** -----------------------------------------------------------------------------------------------
 */
$resultC = array_map("unserialize", array_unique(array_map("serialize", $resultC)));


/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'YEAR' => $currYear,
        'DATAY' => $resultY,
        'DATAS' => $resultS,
        'DATAT' => $resultT,
        'DATAM' => $resultM,
        'DATAC' => $resultC
    )
);

return;

function getCampagneIDPrec($id) {

    $sqlGetPreviousCampagne = '
    SELECT FK_idCampagnePrec
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getPreviousCampagneExec = DbConnexion::getInstance()->prepare($sqlGetPreviousCampagne);
    $getPreviousCampagneExec->bindValue(':idc', $id, PDO::PARAM_INT);
    $getPreviousCampagneExec->execute();
    $getPreviousCampagne = $getPreviousCampagneExec->fetch(PDO::FETCH_OBJ);
    return $getPreviousCampagne->FK_idCampagnePrec;
}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToTrimestre($mois) {

    return (1+(($mois-1)%3/2)*2);

}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToSemestre($mois) {

    return $mois < 7 ? 1 : 2;

}
