/**
SELECT idIntervenant, idPdv, DATE_FORMAT(dateDebut, '%Y-%m-%d'), COUNT(DATE_FORMAT(dateDebut, '%Y-%m-%d')) AS nbIntervention
FROM su_intervention
INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE FK_idMission = 121
GROUP BY idIntervenant, idPdv, DATE_FORMAT(dateDebut, '%Y-%m-%d')