<?php
/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER
 * @detail Recherche la liste des CAMPAGNES
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

$sqlGetListOfTrucs = '
SELECT *
FROM su_campagne SUC
ORDER BY SUC.libelleCampagne
';
$getListOfTrucsExec = DbConnexion::getInstance()->prepare($sqlGetListOfTrucs);
$getListOfTrucsExec->execute();

print json_encode(array(
    'items' => $getListOfTrucsExec->fetchAll(PDO::FETCH_OBJ)
));