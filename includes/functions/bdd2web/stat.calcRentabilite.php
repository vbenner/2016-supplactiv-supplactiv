<?php
/** -----------------------------------------------------------------------------------------------
 * Calcul de la Rentabilité
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 *
 * 1.0.1 - Prise en compte du PRORATA O/N
 * 1.0.0 - Release
 */
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);
#@set_time_limit(0);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des paramètres
 * (qui sont les ID)
 */
$campagne_id = filter_input(INPUT_POST, 'campagne');
$mission_id = filter_input(INPUT_POST, 'mission');
$intervenante_id = filter_input(INPUT_POST, 'intervenante');
$deb = filter_input(INPUT_POST, 'dateDeb');
$fin = filter_input(INPUT_POST, 'dateFin');
$calcWithProrata = filter_input(INPUT_POST, 'prorata');
$dateDeb = DateTime::createFromFormat('d/m/Y', $deb);
$dateFin = DateTime::createFromFormat('d/m/Y', $fin);

/** -----------------------------------------------------------------------------------------------
 * On récupère cette CAMPAGNE.
 */
$sqlGetCampagne = '
SELECT *
FROM su_campagne
WHERE idCampagne = :idc
';
$getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
$getCampagneExec->bindValue(':idc', $campagne_id, PDO::PARAM_INT);
$getCampagneExec->execute();
$campagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);

/** -----------------------------------------------------------------------------------------------
 * On en déduit la liste des TARIFS
 */
$tarif[ 'A' ] = $campagne->tarifAnimation;
$tarif[ 'F' ] = $campagne->tarifFormation;
$tarif[ 'M' ] = $campagne->tarifMerchandising;
$tarif[ 'F2' ] = $campagne->tarifFormation2;
$tarif[ 'F3' ] = $campagne->tarifFormation3;

/** -----------------------------------------------------------------------------------------------
 * Cette CAMPAGNE NOUS DONNE DES MISSIONS (la clé est différente suivant le paramètre qui arrive
 * 2019-07-16 - on a obligatoiremenet la mission
 */
$sqlGetMissions = '
SELECT *
FROM su_mission
WHERE idMission = :id
';
$getMissionsExec = DbConnexion::getInstance()->prepare($sqlGetMissions);
$getMissionsExec->bindValue(':id', $mission_id != '' ? $mission_id : $campagne_id, PDO::PARAM_INT);
$getMissionsExec->execute();
$typeMission = 0;

/** -----------------------------------------------------------------------------------------------
 * On parcourt maintenant la liste des MISSION
 */
$salaire = array();
#ob_start();
while ($mission = $getMissionsExec->fetch(PDO::FETCH_OBJ)) {

    $typeMission = $mission->FK_idTypeMission;

    /** -------------------------------------------------------------------------------------------
     * POUR CHAQUE MISSION, il y aura des INTERVENTIONS (ou pas suivant le filtre DATE / INTERV.
     *
     * On prend déjà les salaireBrut != 0 (Animation)
     */
    $sqlGetInterventions = '
    SELECT WEEK(dateDebut) AS lasemaine, MONTH(dateDebut) AS lemois, YEAR(dateDebut) AS lannee, 
      SUI.dateDebut, SUI.dateFin, SUC.salaireBrut, SUC.forfaitJournalierNet,
      SUI.FK_idIntervenant, SUI.idIntervention, SUI.FK_idMission,
      SUS.nomIntervenant
    FROM su_intervention SUI
      INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
      INNER JOIN su_intervenant SUS ON SUS.idIntervenant = SUI.FK_idIntervenant
    WHERE SUI.FK_idMission = :idm
    AND SUC.salaireBrut != 0
    AND SUS.boolAutoEntrepreneuse = "NON"
    ';

    /** -------------------------------------------------------------------------------------------
     * On peut éventuellement ajouter des clauses supplémentaires
     */
    $sqlGetInterventions .= ($intervenante_id != 'ALL' ? ' AND FK_idIntervenant = :idi ' : '');
    $sqlGetInterventions .= ($dateDeb != '' ? ' AND dateDebut >= :dated ' : '');
    $sqlGetInterventions .= ($dateFin != '' ? ' AND dateFin <= :datef ' : '');

    /** -------------------------------------------------------------------------------------------
     * Et on doit absolument filtrer pour que ça marche !
     */
    $sqlGetInterventions .= '
    ORDER BY nomIntervenant, lasemaine, dateDebut
    ';

    $getInterventionsExec = DbConnexion::getInstance()->prepare($sqlGetInterventions);
    $getInterventionsExec->bindValue(':idm', $mission->idMission, PDO::PARAM_INT);
    if ($intervenante_id != 'ALL') {
        $getInterventionsExec->bindValue(':idi', $intervenante_id, PDO::PARAM_INT);
    }
    if ($dateDeb != '') {
        $getInterventionsExec->bindValue(':dated', $dateDeb->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
    }
    if ($dateFin != '') {
        $getInterventionsExec->bindValue(':datef', $dateFin->format('Y-m-d') . ' 23:59:59', PDO::PARAM_STR);
    }
    $getInterventionsExec->execute();

    #die('sql');

    /** -------------------------------------------------------------------------------------------
     * Et maintenant, on a la liste des INTERVENTIONS qui nous donne les heures et le brut de
     * base. On travaille / semaine et / intervenante (d'où le tri dans la requête). Tout
     * changement d'ID et de numéro de semaine remet à 0 la méthode de calcul des heures supps
     * hebdomadaire.
     */
    $fromScratch = true;
    $nWeekPrec = 0;
    $nIdIntervenantPrec = 0;
    $equivalentIdNom = array();

    while ($intervention = $getInterventionsExec->fetch(PDO::FETCH_OBJ)) {

        /** ---------------------------------------------------------------------------------------
         * Variables juste pour simplifier les tableaux
         */
        $idMission = $intervention->FK_idMission;
        $idIntervenant = $intervention->FK_idIntervenant;
        $idIntervention = $intervention->idIntervention;
        $year = $intervention->lannee;
        $month = $intervention->lemois;
        $week = $intervention->lasemaine;

        /** ---------------------------------------------------------------------------------------
         * Init de base
         */
        if ($nWeekPrec == 0) {
            $nWeekPrec = $week;
        }
        if ($nIdIntervenantPrec == 0) {
            $nIdIntervenantPrec = $idIntervenant;
        }


        /** ---------------------------------------------------------------------------------------
         * Il faudra recommencer la méthode de calcul
         */
        if ($week != $nWeekPrec || $idIntervenant != $nIdIntervenantPrec) {
            #die('ON CHANGE -> '.$week.'/'.$nWeekPrec.' et '.$idIntervenant.'/'.$nIdIntervenantPrec);
            $fromScratch = true;
        }
        else {
            $fromScratch = false;
        }

        /** ---------------------------------------------------------------------------------------
         * Quelle est la durée liéee à cette intervention
         * Avec recalcul si >= 8 (1 heure de pause)
         */
        $hours = abs(round((strtotime($intervention->dateFin) - strtotime($intervention->dateDebut)) / 3600, 1));
        $hours -= ($hours >= 8 ? 1 : 0);

        #die( 'D' );

        /** ---------------------------------------------------------------------------------------
         * En fonction de l'intervenant on a un tx horaire
         */
        $txhoraire = $intervention->salaireBrut;

        /** ---------------------------------------------------------------------------------------
         * Si le tableau n'a pas été initialisé pour un mois / intervenant, on le fait et on
         * calcule directement les frais chargés
         */
        $equivalentIdNom[ $idIntervenant ] = $intervention->nomIntervenant;
        if (!isset($salaire[ $idIntervenant ][ $year ][ $month ][ 'brut' ])) {
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'heures' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'brut' ] = 0;

            $salaire[ $idIntervenant ][ $year ][ $month ][ 'repas' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'telephone' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'fraisc' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'fraisnc' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'km' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'brutcharge' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'brutreference' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'net' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'netfinal' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ][ 'chargesp' ] = 0;

            /** -----------------------------------------------------------------------------------
             * De façon temporaire, on ajoute une cellule pour les semaines et on la supprimera
             * juste après (à la fin du traitement des interventions
             */
            $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] = 0;
            $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] = 0;
        }

        #echo '<pre>';
        #print_r($salaire);
        #echo '</pre>';


        /** ---------------------------------------------------------------------------------------
         * Avec cette intervention, on en déduit les frais de repas
         * --> on les cumule ici car ils sont liés à une intervention et non à une DATE
         */
        #die('MISISON '.$mission->FK_idTypeMission);
        $salaire[ $idIntervenant ][ $year ][ $month ][ 'repas' ] += getFraisRepas($idIntervention, $mission->FK_idTypeMission, $idIntervenant);
        $salaire[ $idIntervenant ][ $year ][ $month ][ 'telephone' ] += getFraisTelephone($idIntervention, $mission->FK_idTypeMission, $idIntervenant);

        /** ---------------------------------------------------------------------------------------
         * CALCUL DES HEURES SUPPS
         * [0-35] -> Tx normal
         * [35.01 à 42] -> Tx * 1.25
         * [42.01 à ∞[ -> Tx * 1.50
         */

        /** ---------------------------------------------------------------------------------------
         * Si on repart de 0, il n'y a rien à faire, on a juste à ajouter les heures pour
         * connaître le brut. Forcément on ajoute moins de 35h....
         */
        if ($fromScratch) {

            $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $hours;
            $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $hours * $txhoraire;

            #echo '1 > ' . $idIntervention . " > $hours * $txhoraire" . '<br/>';

        }
        else {

            /** -----------------------------------------------------------------------------------
             * Test de base, on a déjà dépassé les 42 heures donc systématiquement on fait * 1.5
             */
            if ($salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] + $hours > 42) {
                $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $hours;
                $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $hours * $txhoraire * 1.5;

                #echo '2 > ' . $idIntervention . " > " . "$hours * $txhoraire * 1.5" . '<br/>';
            }
            else {

                /** -------------------------------------------------------------------------------
                 * On aura ici 2 actions à faire, premièrement, compléter jusqu'à 35h et
                 * ventiller le reliquat avec un tx à 1.25
                 */
                if ($salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] + $hours > 35) {

                    $pourcompleter = 35 - $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ];

                    /** ---------------------------------------------------------------------------
                     * On complète donc jusqu'à 35h
                     */
                    $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $pourcompleter;
                    $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $pourcompleter * $txhoraire;

                    #echo '3 > ' . "$pourcompleter * $txhoraire" . '<br/>';

                    /** ---------------------------------------------------------------------------
                     * On a donc un reliquat d'heures
                     * (par exemple 8h de travail et on était à 37h sur la semaine)
                     */
                    $reliquat = $hours - $pourcompleter;

                    /** ---------------------------------------------------------------------------
                     * On va quand même vérifier qu'on ne dépasse pas 42h maintenant
                     */
                    if ($salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] + $reliquat > 42) {

                        /** -----------------------------------------------------------------------
                         * On recomplète donc à partir des 37h, soit 5h à rerecompléter
                         */
                        $pourrecompleter = 42 - $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ];
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $pourrecompleter;
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $pourrecompleter * $txhoraire * 1.25;

                        #echo '4 > ' . "$pourrecompleter * $txhoraire * 1.25" . '<br/>';

                        /** -----------------------------------------------------------------------
                         * On enlève au final le re-reliquat au reliquat précérent
                         * et là, on majore de 50% BIM !
                         */
                        $rereliquat = $reliquat - $pourrecompleter;
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $rereliquat;
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $rereliquat * $txhoraire * 1.50;

                        #echo '5 > ' . "$rereliquat * $txhoraire * 1.50" . '<br/>';

                    }
                    else {
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $reliquat;
                        $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $reliquat * $txhoraire * 1.25;

                        #echo '6 > ' . "$reliquat * $txhoraire * 1.25" . '<br/>';

                    }
                }
                else {
                    $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'heures' ] += $hours;
                    $salaire[ $idIntervenant ][ $year ][ $month ]['weeks'][ $week ][ 'brut' ] += $hours * $txhoraire;

                    #echo "7 > " . $idIntervention . " > " . "$hours * $txhoraire" . '<br/>';

                }
            }
        }


        /** ---------------------------------------------------------------------------------------
         * On prépare le calcul de la prochaine itération de la boucle
         */
        $nWeekPrec = $week;
        $nIdIntervenantPrec = $idIntervenant;

    }

    #echo '<pre>';
    #print_r($salaire);
    #echo '</pre>';
    #die();

    /** -------------------------------------------------------------------------------------------
     * On remonte donc de 1, la somme du brut et des heures
     */
    foreach ($salaire as $idinter => $uneAnnee) {
        foreach ($uneAnnee as $year => $unMois) {
            foreach ($unMois as $month => $uneSemaine) {

                #echo '<pre>';
                #print_r($uneSemaine);
                #echo '</pre>';

                foreach ($uneSemaine['weeks'] as $semaine => $detail) {

                    #echo '$semaine = '.$semaine.'</br>';
                    $salaire[ $idinter ][ $year ][ $month ][ 'brut' ] +=
                        $salaire[ $idinter ][ $year ][ $month ]['weeks'][ $semaine ] [ 'brut' ];
                    $salaire[ $idinter ][ $year ][ $month ][ 'heures' ] +=
                        $salaire[ $idinter ][ $year ][ $month ]['weeks'][ $semaine ] [ 'heures' ];
                }

                unset($salaire[ $idinter ][ $year ][ $month ]['weeks']);
            }
        }
    }

    /** -------------------------------------------------------------------------------------------
     * A partir de là, on a un salaire MENSUEL pour chaque ANIMATRICE / MOIS
     */
    #echo '<pre>';
    #print_r($salaire);
    #echo '</pre><br/><br/>';
    #die();

    /** -------------------------------------------------------------------------------------------
     * Maintenant, pour chaque INTERVENANT / ANNEE / MOIS, on récupère les données des
     * primes
     *
     */
    foreach ($salaire as $idinter => $uneAnnee) {
        foreach ($uneAnnee as $year => $unMois) {
            foreach ($unMois as $month => $uneSemaine) {

                /** -------------------------------------------------------------------------------
                 * On ajoute les frais chargés
                 * 2019-09-17
                 * Suppression des frais chargés.
                 */
                $hasPrime = false;
                #$fraisc = getFraisCharges($year, $month, $idinter);
                $fraisc = 0;
                #if ($fraisc != 0 ) {
                #    $hasPrime = true;
                #}
                $salaire[ $idinter ][ $year ][ $month ][ 'fraisc' ] = $fraisc;

                /** -------------------------------------------------------------------------------
                 * Ce qui modifie le BRUT
                 * -> brut chargé
                 */
                $brut = $salaire[ $idinter ][ $year ][ $month ][ 'brut' ];
                $brut += $fraisc;
                $salaire[ $idinter ][ $year ][ $month ][ 'brutcharge' ] = $brut;

                /** -------------------------------------------------------------------------------
                 * Ajout pour les CP (10%)
                 * Ajout pour la précarité (10%)
                 * (= brut de reference)
                 */
                $brut *= 1.1;
                $brut *= 1.1;
                $salaire[ $idinter ][ $year ][ $month ][ 'brutreference' ] = $brut;

                /** -------------------------------------------------------------------------------
                 */

                /** -------------------------------------------------------------------------------
                 * On enlève les charges salariales
                 * 22.46
                 * --> on obtient le NET
                 * --> ce montant ne sera pas utilisé par la suite
                 */
                $net = $brut * (1 - (22.46 / 100));
                $salaire[ $idinter ][ $year ][ $month ][ 'net' ] = $net;


                /** -------------------------------------------------------------------------------
                 * On ajoute les frais non chargés
                 * + frais de repas
                 * + téléphone
                 * + frais km
                 *
                 * Si on est en mode NON PRORATISE, on ira récupérer à la mission
                 * @@@VB 2019-11-28
                 */
                $fraisnc = getFraisNonCharges($year, $month, $idinter, $calcWithProrata, $idMission);
                $kms = getFraisKilometres($year, $month, $idinter, $calcWithProrata, $idMission);
                $salaire[ $idinter ][ $year ][ $month ][ 'fraisnc' ] += $fraisnc;
                $salaire[ $idinter ][ $year ][ $month ][ 'km' ] += $kms;

                $net += $fraisnc;
                $net += $salaire[ $idinter ][ $year ][ $month ][ 'repas' ];
                $net += $salaire[ $idinter ][ $year ][ $month ][ 'telephone' ];
                $net += $kms;

                $salaire[ $idinter ][ $year ][ $month ][ 'net' ] = $net;

                /** -------------------------------------------------------------------------------
                 * On enlève la reprise ACOMPTE
                 *
                 * -> on ne tient plus compte de cette reprise (elle est forcément lissée
                 * sur 2 mois consécutifs
                 */
                #$reprise = getRepriseAccompte($year, $month, $idinter);
                #$net -= $reprise;

                /** -------------------------------------------------------------------------------
                 * On a donc maintenant le net FINAL
                 */
                $salaire[ $idinter ][ $year ][ $month ][ 'netfinal' ] = $net;

                /** --------------------------------------------------------------
                 * Si prime, tx = 41, sinon, ça dépend des animations / formations
                 * Si campagne d'animation --> 26%
                 * Si campagne de formation --> 28%
                 * Le tx 41 l'emporte
                 */
                if ($typeMission == 2) {
                    $txBase = 28;
                }
                else {
                    $txBase = 26;
                }
                if ($hasPrime) {
                    $txBase = 41;
                }

                $salaire[ $idinter ][ $year ][ $month ][ 'chargesp' ] =
                    $salaire[ $idinter ][ $year ][ $month ][ 'brutreference' ] * $txBase /100;

                /** -------------------------------------------------------------------------------
                 * $campagne_id = filter_input(INPUT_POST, 'campagne');
                 * $mission_id = filter_input(INPUT_POST, 'mission');
                 * $idinter = filter_input(INPUT_POST, 'intervenante');
                 */
                #print "$campagne_id, $mission_id, $idinter, $month, $year".'<br/>';
                $salaire[ $idinter ][ $year ][ $month ][ 'count' ] =
                    getCountIntervention($campagne_id, $mission_id, $idinter, $month, $year);

                $salaire[ $idinter ][ $year ][ $month ][ 'counttot' ] =
                    getCountIntervention(0, 0, $idinter, $month, $year);

                /** -------------------------------------------------------------------------------
                 * Le prorata va servir pour les frais
                 */
                $prorata = 1;
                if ($calcWithProrata) {
                    if ($salaire[$idinter][$year][$month]['counttot'] != 0) {
                        $prorata = $salaire[$idinter][$year][$month]['count'] /
                            $salaire[$idinter][$year][$month]['counttot'];
                    }

                    $salaire[ $idinter ][ $year ][ $month ][ 'coutemployeur' ] =
                        $salaire[ $idinter ][ $year ][ $month ][ 'brutreference' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'chargesp' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'repas' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'telephone' ] +
                        $prorata * (
                            $salaire[ $idinter ][ $year ][ $month ][ 'fraisnc' ] +
                            $salaire[ $idinter ][ $year ][ $month ][ 'km' ]
                        );
                } else {
                    $salaire[ $idinter ][ $year ][ $month ][ 'coutemployeur' ] =
                        $salaire[ $idinter ][ $year ][ $month ][ 'brutreference' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'chargesp' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'repas' ] +
                        $salaire[ $idinter ][ $year ][ $month ][ 'telephone' ] +
                        $prorata * (
                            $salaire[ $idinter ][ $year ][ $month ][ 'fraisnc' ] +
                            $salaire[ $idinter ][ $year ][ $month ][ 'km' ]
                        );
                }
                #$salaire[ $idinter ][ $year ][ $month ][ 'nbinter' ]  =
                #    getNbId($intervenante_id, $year, $month, $mission);
            }
            #echo 'Prorata = '.$prorata;
            #echo '<pre>';
            #print_r($salaire);
            #echo '</pre><br/><br/>';
            #die();
        }
    }

    #echo '<pre>';
    #print_r($salaire);
    #echo '</pre>';

    /** -------------------------------------------------------------------------------------------
     * Maintenant, on a la liste des SALAIRES versés pour chaque MISSION (et donc on en
     * revient à la CAMPAGNE)
     */

}

#echo '<pre>';
#print_r($mission);
#echo '</pre>';
#print 'DADA = '.$dateDeb.'<br/>'.'ZERZER '.$dateFin;
#return;

print (
json_encode(
    array(
        'PRIXJOUR' => $tarif,
        'NBINTERV' => getCountIntervention($campagne_id, $mission_id, $intervenante_id, 0, 0, $deb, $fin),
        'NBF1' => getCountFormation(1, $campagne_id, $mission_id, $intervenante_id, 0, 0, $deb, $fin),
        'NBF2' => getCountFormation(2, $campagne_id, $mission_id, $intervenante_id, 0, 0, $deb, $fin),
        'NBF3' => getCountFormation(3, $campagne_id, $mission_id, $intervenante_id, 0, 0, $deb, $fin),
        'FRAISGESTION' => 0,
        'SALAIRES' => $salaire,
        'KEYS' => $equivalentIdNom,
        'TYPEM' => $typeMission
    )
)
);

return;

/** -----------------------------------------------------------------------------------------------
 * On récupère le nombre de journées d'interventions pour cette CAMPAGNE
 */
function getCountIntervention($idCampagne, $idMission = 0, $idIntervenant = 0, $mois = 0, $year = 0,
      $from = '', $to = '')
{

    /** -------------------------------------------------------------------------------------------
     * Requête de base
     * On ne prend pas les AUTO
     */
    $sqlRechercheMissionCampagne = '
    SELECT COUNT(SUI.idIntervention) AS LeCount
    FROM su_intervention SUI
        INNER JOIN su_mission SM ON SUI.FK_idMission = SM.idMission
        INNER JOIN su_campagne SC ON SM.FK_idCampagne = SC.idCampagne
        INNER JOIN su_intervenant SII on SUI.FK_idIntervenant = SII.idIntervenant
    WHERE
        NOT ISNULL(SUI.FK_idContrat)
        AND SII.boolAutoEntrepreneuse = \'NON\'
    ';

    /** -------------------------------------------------------------------------------------------
     * Add-ins
     */
    if ($idCampagne != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idCampagne = :idc ';
    }
    if ($idMission != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idMission = :idm ';
    }
    if ($idIntervenant != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idIntervenant = :idi ';
    }
    if ($mois != 0) {
        $sqlRechercheMissionCampagne .= ' AND MONTH(SUI.dateDebut) = :mois ';
    }
    if ($year != 0) {
        $sqlRechercheMissionCampagne .= ' AND YEAR(SUI.dateDebut) = :year ';
    }
    if ($from != '') {
        $sqlRechercheMissionCampagne .= ' AND (SUI.dateDebut) >= :from ';
    }
    if ($to != '') {
        $sqlRechercheMissionCampagne .= ' AND (SUI.dateDebut) <= :to ';
    }

    #echo $sqlRechercheMissionCampagne.'<br/>';

    /** -------------------------------------------------------------------------------------------
     * Bind / exec
     */
    $rechercheMissionCampagneExec = DbConnexion::getInstance()->prepare($sqlRechercheMissionCampagne);

    /** -------------------------------------------------------------------------------------------
     * Bind / exec
     */
    if ($idCampagne != 0) {
        $rechercheMissionCampagneExec->bindValue(':idc', $idCampagne, PDO::PARAM_INT);
    }
    if ($idMission != 0) {
        $rechercheMissionCampagneExec->bindValue(':idm', $idMission, PDO::PARAM_INT);
    }
    if ($idIntervenant != 0) {
        $rechercheMissionCampagneExec->bindValue(':idi', $idIntervenant, PDO::PARAM_INT);
    }
    if ($mois != 0) {
        $rechercheMissionCampagneExec->bindValue(':mois', $mois, PDO::PARAM_INT);
    }
    if ($year != 0) {
        $rechercheMissionCampagneExec->bindValue(':year', $year, PDO::PARAM_INT);
    }
    if ($from != '') {
        $dateDeb = DateTime::createFromFormat('d/m/Y', $from);
        $rechercheMissionCampagneExec->bindValue(':from', $dateDeb->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
    }
    if ($to != '') {
        $dateFin = DateTime::createFromFormat('d/m/Y', $to);
        $rechercheMissionCampagneExec->bindValue(':to', $dateFin->format('Y-m-d'). ' 23:59:59', PDO::PARAM_STR);
    }

    /** -------------------------------------------------------------------------------------------
     * EXECUTE
     */
    $rechercheMissionCampagneExec->execute();
    $rechercheMissionCampagne = $rechercheMissionCampagneExec->fetch(PDO::FETCH_OBJ);
    return $rechercheMissionCampagne->LeCount;
}

/** -----------------------------------------------------------------------------------------------
 */
function getCountFormation($nbF, $idCampagne, $idMission = 0, $idIntervenant = 0, $mois = 0, $year = 0,
                           $from = '', $to = '')
{

    /** -------------------------------------------------------------------------------------------
     * Requête de base
     * On ne prend pas les AUTO
     */
    $sqlRechercheMissionCampagne = '
    SELECT DATE(SUI.dateDebut), COUNT(1) AS NUMF, (SUI.FK_idIntervenant)
    FROM su_intervention SUI
        INNER JOIN su_mission SM ON SUI.FK_idMission = SM.idMission
        INNER JOIN su_campagne SC ON SM.FK_idCampagne = SC.idCampagne
        INNER JOIN su_intervenant SII on SUI.FK_idIntervenant = SII.idIntervenant
    WHERE
        NOT ISNULL(SUI.FK_idContrat)
        AND SM.FK_idTypeMission = 2
        AND SII.boolAutoEntrepreneuse = \'NON\'
    ';

    /** -------------------------------------------------------------------------------------------
     * Add-ins
     */
    if ($idCampagne != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idCampagne = :idc ';
    }
    if ($idMission != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idMission = :idm ';
    }
    if ($idIntervenant != 0) {
        $sqlRechercheMissionCampagne .= ' AND FK_idIntervenant = :idi ';
    }
    if ($mois != 0) {
        $sqlRechercheMissionCampagne .= ' AND MONTH(SUI.dateDebut) = :mois ';
    }
    if ($year != 0) {
        $sqlRechercheMissionCampagne .= ' AND YEAR(SUI.dateDebut) = :year ';
    }
    if ($from != '') {
        $sqlRechercheMissionCampagne .= ' AND (SUI.dateDebut) >= :from ';
    }
    if ($to != '') {
        $sqlRechercheMissionCampagne .= ' AND (SUI.dateDebut) <= :to ';
    }
    $sqlRechercheMissionCampagne .= ' GROUP BY DATE(SUI.dateDebut), 1, SUI.FK_idIntervenant ';

    #echo $sqlRechercheMissionCampagne.'<br/>';

    /** -------------------------------------------------------------------------------------------
     * Bind / exec
     */
    $rechercheMissionCampagneExec = DbConnexion::getInstance()->prepare($sqlRechercheMissionCampagne);

    /** -------------------------------------------------------------------------------------------
     * Bind / exec
     */
    if ($idCampagne != 0) {
        $rechercheMissionCampagneExec->bindValue(':idc', $idCampagne, PDO::PARAM_INT);
    }
    if ($idMission != 0) {
        $rechercheMissionCampagneExec->bindValue(':idm', $idMission, PDO::PARAM_INT);
    }
    if ($idIntervenant != 0) {
        $rechercheMissionCampagneExec->bindValue(':idi', $idIntervenant, PDO::PARAM_INT);
    }
    if ($mois != 0) {
        $rechercheMissionCampagneExec->bindValue(':mois', $mois, PDO::PARAM_INT);
    }
    if ($year != 0) {
        $rechercheMissionCampagneExec->bindValue(':year', $year, PDO::PARAM_INT);
    }
    if ($from != '') {
        $dateDeb = DateTime::createFromFormat('d/m/Y', $from);
        $rechercheMissionCampagneExec->bindValue(':from', $dateDeb->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
    }
    if ($to != '') {
        $dateFin = DateTime::createFromFormat('d/m/Y', $to);
        $rechercheMissionCampagneExec->bindValue(':to', $dateFin->format('Y-m-d'). ' 23:59:59', PDO::PARAM_STR);
    }

    /** -------------------------------------------------------------------------------------------
     * EXECUTE
     */
    $total = 0;
    $rechercheMissionCampagneExec->execute();
    while ($rechercheMissionCampagne = $rechercheMissionCampagneExec->fetch(PDO::FETCH_OBJ)) {
        #echo $nbF.'- - - - <pre>';
        #print_r($rechercheMissionCampagne);
        #echo '</pre><br/>';
        if ($nbF >= 3) {
            $total += ($rechercheMissionCampagne->NUMF >= $nbF ? 1 : 0);
        }
        else {
            $total += ($rechercheMissionCampagne->NUMF == $nbF ? 1 : 0);
        }
    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * Récupération des frais téléphoniques
 */
function getFraisTelephone($intervention, $typeMission = 0, $idIntervenant = 0)
{
    /** -------------------------------------------------------------------------------------------
     * Les Frais se trouvent dans 1 table
     */
    $ratio = 1;
    if ($typeMission == 2) {
        /** ---------------------------------------------------------------------------------------
         * Combien d'interventions ce jour ?
         */
        $sqlCountInter = '
        SELECT COUNT(dateDebut) AS LeCount
        FROM su_intervention
        WHERE DATE_FORMAT(dateDebut, \'%d-%m-%Y\') = (
		    SELECT DATE_FORMAT(dateDebut, \'%d-%m-%Y\') 
		    FROM su_intervention SUI
            INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
            WHERE FK_idIntervention = :idintervention
	    )
	    AND FK_idIntervenant = :idintervenant
        ';
        $countInterExec = DbConnexion::getInstance()->prepare($sqlCountInter);
        $countInterExec->bindValue(':idintervention', $intervention, PDO::PARAM_INT);
        $countInterExec->bindValue(':idintervenant', $idIntervenant, PDO::PARAM_INT);
        $countInterExec->execute();
        $countInter = $countInterExec->fetch(PDO::FETCH_OBJ);
        $ratio = $countInter->LeCount;
    }
    $sqlListeFrais = '
    SELECT *
    FROM su_intervention_frais
    WHERE FK_idIntervention = :id
    ';
    $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
    $listeFraisExec->bindValue(':id', $intervention, PDO::PARAM_INT);
    $listeFraisExec->execute();
    $total = 0;
    while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
        $total += ($frais->fraisTelephone / $ratio);
    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * Récupération des frais de repas
 * Pas de notion de PRORATA car on est à l'intervention, donc déjà lié à une mission
 */
function getFraisRepas($intervention, $typeMission = 0, $idIntervenant = 0)
{

    /** -------------------------------------------------------------------------------------------
     * Les Frais se trouvent dans 1 table
     *
     */
    $ratio = 1;
    if ($typeMission == 2) {
        /** ---------------------------------------------------------------------------------------
         * Combien d'interventions ce jour ?
         */
        $sqlCountInter = '
        SELECT COUNT(dateDebut) AS LeCount
        FROM su_intervention
        WHERE DATE_FORMAT(dateDebut, \'%d-%m-%Y\') = (
		    SELECT DATE_FORMAT(dateDebut, \'%d-%m-%Y\') 
		    FROM su_intervention SUI
            INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
            WHERE FK_idIntervention = :idintervention
	    )
	    AND FK_idIntervenant = :idintervenant
        ';
        $countInterExec = DbConnexion::getInstance()->prepare($sqlCountInter);
        $countInterExec->bindValue(':idintervention', $intervention, PDO::PARAM_INT);
        $countInterExec->bindValue(':idintervenant', $idIntervenant, PDO::PARAM_INT);
        $countInterExec->execute();
        $countInter = $countInterExec->fetch(PDO::FETCH_OBJ);

        #echo '<pre>';
        #print_r($countInter);
        #echo '</pre><br/>';

        $ratio = $countInter->LeCount;
        #echo ('RATIO = '.$ratio.' - '.$intervention.' / '. $idIntervenant);
    }

    $sqlListeFrais = '
    SELECT *
    FROM su_intervention_frais
    WHERE FK_idIntervention = :id
    ';
    $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
    $listeFraisExec->bindValue(':id', $intervention, PDO::PARAM_INT);
    $listeFraisExec->execute();

    $total = 0;
    while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
        $total += ($frais->fraisRepas / $ratio);
    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des kilomètres
 */
function getFraisKilometres($annee, $mois, $intervenant, $calcWithProrata = 1, $idMission = 0)
{

    /** -------------------------------------------------------------------------------------------
     * Les Frais se trouvent dans 2 tables
     */
    if ($calcWithProrata == 1) {
        $sqlListeFrais = '
        SELECT *
        FROM su_intervenant_km
        WHERE dateKilometre LIKE :date 
        AND FK_idIntervenant = :idi
        ';
        $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
        $listeFraisExec->bindValue(':date', $annee . '-' .
            str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
        $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
        $listeFraisExec->execute();

        $total = 0;
        while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
            $total += $frais->tarifKilometre * $frais->totalKilometre;
        }
    }
    else {
        $sqlListeFrais = '
        SELECT *
        FROM su_intervenant_km_detail
        WHERE dateKilometre LIKE :date 
        AND FK_idIntervenant = :idi
        AND FK_idMission = :idm
        ';
        $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
        $listeFraisExec->bindValue(':date', $annee . '-' .
            str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
        $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
        $listeFraisExec->bindValue(':idm', $idMission, PDO::PARAM_INT);
        $listeFraisExec->execute();

        $total = 0;
        while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
            $total += $frais->tarifKilometre * $frais->totalKilometre;
        }    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * Liste de toutes les primes
 */
function getFraisNonCharges($annee, $mois, $intervenant, $calcWithProrata = 1, $idMission = 0)
{

    /** -------------------------------------------------------------------------------------------
     * Les Frais se trouvent dans 1 table suivant PRORATA ou NON
     */
    if ($calcWithProrata == 1) {
        $sqlListeFrais = '
        SELECT *
        FROM su_intervenant_frais
        WHERE dateFrais LIKE :date 
        AND FK_idIntervenant = :idi
        ';
        $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
        $listeFraisExec->bindValue(':date', $annee . '-' .
            str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
        $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
        $listeFraisExec->execute();

        $total = 0;
        while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
            $total += ($frais->montantFraisJustificatif +
                $frais->montantAutreFrais);
        }
    }
    else {
        $sqlListeFrais = '
        SELECT *
        FROM su_intervenant_frais_detail
        WHERE dateFrais LIKE :date 
        AND FK_idIntervenant = :idi
        AND FK_idMission = :idm
        ';
        $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
        $listeFraisExec->bindValue(':date', $annee . '-' .
            str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
        $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
        $listeFraisExec->bindValue(':idm', $idMission, PDO::PARAM_INT);
        $listeFraisExec->execute();

        $total = 0;
        while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
            $total += ($frais->montantFraisJustificatif +
                $frais->montantAutreFrais);
        }
    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * Liste de toutes les primes
 */
function getFraisCharges($annee, $mois, $intervenant)
{

    $sqlListeFrais = '
    SELECT *
    FROM su_intervenant_frais
    WHERE dateFrais LIKE :date 
    AND FK_idIntervenant = :idi
    ';
    $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
    $listeFraisExec->bindValue(':date', $annee . '-' .
        str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
    $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
    $listeFraisExec->execute();

    $total = 0;
    while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
        #$total += ($frais->montantPrimeObjectif +
        #    $frais->montantIndemniteAnnulation +
        #    $frais->montantAutrePrime +
        #    $frais->montantPrimeRDV +
        #    $frais->montantIndemniteFormation
        #);
        /** ---------------------------------------------------------------------------------------
         * 2019-07-03 - Appel dominique
         * On ne tient plus compte de Prime Objectif
         * AutrePrime
         * PrimeRDV
         */
        $total += (
            $frais->montantIndemniteAnnulation +
            $frais->montantIndemniteFormation
        );
    }
    return $total;
}

/** -----------------------------------------------------------------------------------------------
 * Récupération de la reprise acompte
 */
function getRepriseAccompte($annee, $mois, $intervenant)
{

    $sqlListeFrais = '
    SELECT *
    FROM su_intervenant_frais
    WHERE dateFrais LIKE :date 
    AND FK_idIntervenant = :idi
    ';
    $listeFraisExec = DbConnexion::getInstance()->prepare($sqlListeFrais);
    $listeFraisExec->bindValue(':date', $annee . '-' .
        str_pad($mois, 2, '0', STR_PAD_LEFT) . '-%', PDO::PARAM_STR);
    $listeFraisExec->bindValue(':idi', $intervenant, PDO::PARAM_INT);
    $listeFraisExec->execute();

    $total = 0;
    while ($frais = $listeFraisExec->fetch(PDO::FETCH_OBJ)) {
        $total = $frais->montantRepriseAccompte;
    }
    return $total;
}