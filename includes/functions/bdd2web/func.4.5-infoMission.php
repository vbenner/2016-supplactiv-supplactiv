<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur une mission
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idMission' )) {

    $RechercheInfoMissionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'));
    $RechercheInfoMissionExc->execute();


    $RechercheTypeMissionExc->execute();

    /** Recherche des statuts d'interventions */
    $listeStatut = array(1 => 0, 2 => 0, 3 => 0, 4 => 0);
    $RechercheStatutInterventionMissionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'));
    $RechercheStatutInterventionMissionExc->execute();
    while($InfoStatut = $RechercheStatutInterventionMissionExc->fetch(PDO::FETCH_OBJ)){
        if(!is_null($InfoStatut->FK_idContrat)){
            $listeStatut[1]++;
        }else {
            if($InfoStatut->FK_idIntervenant == 1){
                if(is_null($InfoStatut->dateDebut)){
                    $listeStatut[4]++;
                }else $listeStatut[3]++;
            }else {
                if(is_null($InfoStatut->dateDebut)){
                    $listeStatut[3]++;
                }else $listeStatut[2]++;
            }
        }
    }


    /** -------------------------------------------------------------------------------------------
     * On ajoute la liste des utilisateurs
     */
    $sqlGetDecideurs = '
    SELECT idUtilisateur, nomUtilisateur
    FROM du_utilisateur
    WHERE idUtilisateur <> 1
    AND idUtilisateur <> 1170
    ORDER BY nomUtilisateur
    ';
    $getListeDecideursExc = DbConnexion::getInstance()->prepare($sqlGetDecideurs);
    $getListeDecideursExc->execute();

    print json_encode(array(
        'info' => $RechercheInfoMissionExc->fetch(PDO::FETCH_OBJ),
        'statuts' => $listeStatut,
        'types' => $RechercheTypeMissionExc->fetchAll(PDO::FETCH_OBJ),
        'users' => $getListeDecideursExc->fetchAll(PDO::FETCH_OBJ)
    ));
}