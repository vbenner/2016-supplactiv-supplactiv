<?php
/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER
 * @detail Recherche la liste des MISSIONS
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Avec la campagne, on a la date de début et de fin
 */
$sqlGetCampagne = '
SELECT DATE_FORMAT(dateDebut, \'%d/%m/%Y\') as dateDebut,
    DATE_FORMAT(dateFin, \'%d/%m/%Y\') as dateFin
FROM su_campagne
WHERE idCampagne = :idc
';
$getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
$getCampagneExec->bindValue(':idc', filter_input(INPUT_POST, 'id'), PDO::PARAM_INT);
$getCampagneExec->execute();
$getCampagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);
$from = $getCampagne->dateDebut;
$to = $getCampagne->dateFin;

/** -----------------------------------------------------------------------------------------------
 * Filtre Campagne ?
 */
$campagne = '';
$annee = '';
if (filter_has_var(INPUT_POST, 'id')) {
    $campagne = ' WHERE FK_idCampagne = :idc ';
}
if (filter_has_var(INPUT_POST, 'annee')) {
    $annee = ' INNER JOIN su_campagne SC ON SC.idCampagne = SU.FK_idCampagne
                  WHERE YEAR(SC.dateDebut) = :year
                ';
}
$sqlGetListOfTrucs = '
SELECT *
FROM su_mission SU '.$campagne.$annee.'
ORDER BY SU.libelleMission
';

$getListOfTrucsExec = DbConnexion::getInstance()->prepare($sqlGetListOfTrucs);
if ($campagne != '') {
    $getListOfTrucsExec->bindValue(':idc', filter_input(INPUT_POST, 'id'), PDO::PARAM_INT);
}
if ($annee != '') {
    $getListOfTrucsExec->bindValue(':year', filter_input(INPUT_POST, 'annee'), PDO::PARAM_INT);
}
$getListOfTrucsExec->execute();

print json_encode(array(
    'items' => $getListOfTrucsExec->fetchAll(PDO::FETCH_OBJ),
    'from' => $from,
    'to' => $to,
));