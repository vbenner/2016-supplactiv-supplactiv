<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des campagnes
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$listeAffiliation = array(
    'campagnes' => array()
);

/** Recherche des campagnes */
$RechercheCampagneExc->execute();
while($InfoCampagne = $RechercheCampagneExc->fetch(PDO::FETCH_OBJ)){
    array_push($listeAffiliation['campagnes'], $InfoCampagne);
}

print json_encode($listeAffiliation);
