<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des frais interventions pour une intervenante
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** ON test la presence des POSTS */
if(filter_has_var(INPUT_POST, 'dateFrais') && filter_has_var(INPUT_POST, 'idIntervenant')){

    /** Recherche des frais d'intervention */
    $RechercheFraisInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
    $RechercheFraisInterventionExc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
    $RechercheFraisInterventionExc->execute();

    /** Recherche des differents KM possible */
    $RechercheKmInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
    $RechercheKmInterventionExc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
    $RechercheKmInterventionExc->execute();

    print json_encode(array(
        'result' => $RechercheFraisInterventionExc->rowCount(),
        'frais' => $RechercheFraisInterventionExc->fetch(PDO::FETCH_OBJ),
        'kms' => $RechercheKmInterventionExc->fetchAll(PDO::FETCH_OBJ)
    ));
}