<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche de la presence d'un email dans la BDD utilisateur
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$RecherchePresenceMailExc->bindValue(':mailUtilisateur', trim(filter_input(INPUT_POST, 'ztEmailAdministrateur')));
$RecherchePresenceMailExc->execute();

print json_encode(array(
    'result' => ($RecherchePresenceMailExc->rowCount() > 0) ? 1 : 0
));
