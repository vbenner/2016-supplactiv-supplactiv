<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des CA
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Par défaut, l'année est l'année en cours sauf si on spécifie la campagne
 */
$currYear = date('Y');

/** -----------------------------------------------------------------------------------------------
 * Filtre facultatif
 */
$idCampagne = 0;
if ($_POST['campagne'] != '') {
    $filtreCampagne = ' AND idCampagne = :idc ';
    $idCampagne = $_POST['campagne'];
    /** -------------------------------------------------------------------------------------------
     * On récupère l'année
     */
    $sqlGetCampagne = '
    SELECT *
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
    $getCampagneExec->bindValue(':idc', $idCampagne, PDO::PARAM_INT);
    $getCampagneExec->execute();
    $getCampagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);
    $currYear = substr($getCampagne->dateFin, 0, 4);
}
else {
    $filtreCampagne = '';
}

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$resultY = array();
$resultS = array();
$resultT = array();
$resultM = array();
$noms = array();

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des années avec tout ce qui va bien dedans
 * txi = taux vs taux (i - 1)
 * txy = taux vs taux (y - 1)
 */
for ($i = $currYear - 5; $i <= $currYear; $i++) {
    $resultY[$i] = array(
        'nb' => 0,
        'ca' => 0,
        'txy' => '-',
        'nb_a' => 0,
        'ca_a' => 0,
        'txy_a' => '-',
        'nb_f' => 0,
        'ca_f' => 0,
        'txy_f' => '-',
    );
    /** -------------------------------------------------------------------------------------------
     * SEMESTRES
     */
    for ($j = 1; $j <= 2 ; $j++){
        $resultS[$i][$j] = array(
            'nb' => 0,
            'ca' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'ca_a' => 0,
            'txy_a' => '-',
            'nb_f' => 0,
            'ca_f' => 0,
            'txy_f' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * TRIMESTRES
     */
    for ($j = 1; $j <= 4 ; $j++){
        $resultT[$i][$j] = array(
            'nb' => 0,
            'ca' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'ca_a' => 0,
            'txy_a' => '-',
            'nb_f' => 0,
            'ca_f' => 0,
            'txy_f' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * MOIS
     */
    for ($j = 1; $j <= 12 ; $j++){
        $resultM[$i][$j] = array(
            'nb' => 0,
            'ca' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'ca_a' => 0,
            'txy_a' => '-',
            'nb_f' => 0,
            'ca_f' => 0,
            'txy_f' => '-',
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * On pourra faire 2 fois les actions par rapport
 */
$inter = array (
    array( 'id' => 3, 'field' => 'tarifAnimation', 'suffix' => 'a'),
    array( 'id' => 2, 'field' => 'tarifFormation', 'suffix' => 'f'),
);

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des jours de interventions pour une campagne donnée regroupée par années
 * On fait déjà pour les animations
 *
 * Si on est en mode SANS filtre CAMPAGNE, c'est assez facile, on n'a pas de restriction
 */
if ($filtreCampagne == '') {

    /** -------------------------------------------------------------------------------------------
     * EXTRACTION GLOBALE
     * Par MOIS
     */
    foreach ($inter as $key => $val) {

        // ----------------------------------------------------------------------------------------
        // La partie évolutive
        // ----------------------------------------------------------------------------------------
        $field = $val['field'];
        $type = $val['id'];
        $suffix = $val['suffix'];

        // ----------------------------------------------------------------------------------------
        // On construit la requête (attention, PDO n'autorise pas le bind d'un champ
        // ----------------------------------------------------------------------------------------
        $sqlGetAllDays = "
        SELECT COUNT(id) AS LeCount, idCampagne as idc, 
          ".$field." as tarif,
          DATE_FORMAT(LaDate, '%Y') as lannee, MONTH(LaDate) AS lemois
        FROM (
          SELECT SI.idIntervention AS id, 
            DATE_FORMAT(SI.dateDebut, '%Y-%m-%d') AS LaDate, SC.idCampagne, SC.".$field." 
          FROM su_intervention SI
            INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
            INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
          WHERE SI.dateDebut >= :du
          AND SI.dateFin <= :au
          AND NOT ISNULL(SI.FK_idContrat)
          AND SM.FK_idTypeMission = :type
          ORDER BY LaDate ASC
        ) AS Tabl
        GROUP BY idc, lannee, lemois
        ORDER BY lannee
        ";

        $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
        $getAllDaysExec->bindValue(':du', ($currYear-5).'-01-01', PDO::PARAM_STR);
        $getAllDaysExec->bindValue(':au', ($currYear).'-12-31', PDO::PARAM_STR);
        $getAllDaysExec->bindValue(':type', $type, PDO::PARAM_INT);
        $getAllDaysExec->execute();

        /** ---------------------------------------------------------------------------------------
         * Traitement
         */
        while($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

            // ----------------------------------------------------------------------------------------
            // Partie commune
            // ----------------------------------------------------------------------------------------
            $year = $getAllDays->lannee;

            // ----------------------------------------------------------------------------------------
            // La partie pour les mois
            // ----------------------------------------------------------------------------------------
            $mois = $getAllDays->lemois;
            $resultM[$year][$mois]['nb_'.$suffix] += $getAllDays->LeCount;
            $resultM[$year][$mois]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / trimestre
            // ----------------------------------------------------------------------------------------
            $trimestre = monthToTrimestre($getAllDays->lemois);
            $resultT[$year][$trimestre]['nb_'.$suffix] += $getAllDays->LeCount;
            $resultT[$year][$trimestre]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / semestre
            // ----------------------------------------------------------------------------------------
            $semestre = monthToSemestre($getAllDays->lemois);
            $resultS[$year][$semestre]['nb_'.$suffix] += $getAllDays->LeCount;
            $resultS[$year][$semestre]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / year
            // ----------------------------------------------------------------------------------------
            $resultY[$year]['nb_'.$suffix] += $getAllDays->LeCount;
            $resultY[$year]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / yeas
            // ----------------------------------------------------------------------------------------
            $resultY[$year]['nb'] += $getAllDays->LeCount;
            $resultY[$year]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
            $resultS[$year][$semestre]['nb'] += $getAllDays->LeCount;
            $resultS[$year][$semestre]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
            $resultT[$year][$trimestre]['nb'] += $getAllDays->LeCount;
            $resultT[$year][$trimestre]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
            $resultM[$year][$mois]['nb'] += $getAllDays->LeCount;
            $resultM[$year][$mois]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;

        }
    }
}
else {

    /** -------------------------------------------------------------------------------------------
     * Pour le cas où la campagne est identifiée, on doit faire l'action pour chaque année et donc
     * reprendre la campagne précédente.
     */
    $campagnes[$currYear] = $_POST['campagne'];
    for ($i = $currYear-1 ; $i >= $currYear-5 ; $i-- ) {
        $campagnes[ $i ] = getCampagneIDPrec($campagnes[ $i + 1 ]);
    }

    #echo '# # # # # # # # # # # # # # # #<pre>';
    #print_r($campagnes);
    #echo '</pre><br/><br/>';

    #echo '# # # # # # # # # # # # # # # #<pre>';
    #print_r($inter);
    #echo '</pre><br/><br/>';
    #die();

    /** -------------------------------------------------------------------------------------------
     * On fait donc maintenant l'analyse ID / ID (sans tenir compte des dates
     *
     * Traitement par ANNEE
     */
    foreach ($campagnes as $year => $idc) {

        foreach ($inter as $key => $val) {

            // ------------------------------------------------------------------------------------
            // La partie évolutive
            // ------------------------------------------------------------------------------------
            $field = $val['field'];
            $type = $val['id'];
            $suffix = $val['suffix'];


            /** -----------------------------------------------------------------------------------
             * On traite les ANIMATIONS
             * TRAITEMENT MENSUEL
             */
            $sqlGetAllDays = '
            SELECT COUNT(id) AS LeCount, idCampagne as idc, 
              ' . $field . ' as tarif, DATE_FORMAT(LaDate, \'%Y\') as lannee,
              MONTH(LaDate) as lemois
            FROM (
              SELECT SI.idIntervention AS id, 
                DATE_FORMAT(SI.dateDebut, \'%Y-%m-%d\') as LaDate, 
                SC.idCampagne, SC.' . $field . '
              FROM su_intervention SI
                INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
                INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
              WHERE NOT ISNULL(SI.FK_idContrat)
              AND SM.FK_idTypeMission = :type
              AND SC.idCampagne = :idc
        	  AND NOT ISNULL(SI.dateDebut)
              ORDER BY LaDate ASC
            ) AS Tabl
            GROUP BY idc, lannee, lemois
            ORDER BY lannee
            ';

            #if ($field == 'tarifAnimation') {
            #    echo $sqlGetAllDays.'<br/>';
            #    echo $idc.'<br/>';
            #    echo $type.'<br/>';
            #    die();
            #}

            $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
            $getAllDaysExec->bindValue(':idc', $idc, PDO::PARAM_STR);
            $getAllDaysExec->bindValue(':type', $type, PDO::PARAM_INT);
            $getAllDaysExec->execute();

            /** -----------------------------------------------------------------------------------
             * Traitement
             */

            #echo ' - - - - - - - - - - - - - - - - - <br/>'.
            #    $field.'<br/>'.
            #    $type.'<br/>'.
            #    $suffix.'<br/>- - - - - - - - - - - - - - - - <br/>';

            while($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

                #echo '- - - - - - - - - - - - - - - - - - - - <pre>';
                #print_r($resultY);
                #echo '</pre><br/><br/>';

                // --------------------------------------------------------------------------------
                // Partie commune
                // --------------------------------------------------------------------------------
                $year = (integer)$getAllDays->lannee;
                #if ($year == 0) {
                #
                #    echo '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -'.'<pre>';
                #    print_r($getAllDays);
                #    echo '</pre>'.'<br/><br/>';
                #    echo 'Field = '.$field.'<br/>';
                #    echo 'Suffix = '.$suffix.'<br/>';
                #    echo 'IDC = '.$idc.'<br/>';
                #    echo 'TYPE = '.$type.'<br/>';
                #    die();
                #}
                // --------------------------------------------------------------------------------
                // La partie pour les mois
                // --------------------------------------------------------------------------------
                $mois = $getAllDays->lemois;
                $resultM[$year][$mois]['nb_'.$suffix] += $getAllDays->LeCount;
                $resultM[$year][$mois]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / trimestre
                // --------------------------------------------------------------------------------
                $trimestre = monthToTrimestre($getAllDays->lemois);
                $resultT[$year][$trimestre]['nb_'.$suffix] += $getAllDays->LeCount;
                $resultT[$year][$trimestre]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / semestre
                // --------------------------------------------------------------------------------
                $semestre = monthToSemestre($getAllDays->lemois);
                $resultS[$year][$semestre]['nb_'.$suffix] += $getAllDays->LeCount;
                $resultS[$year][$semestre]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / year
                // --------------------------------------------------------------------------------
                $resultY[$year]['nb_'.$suffix] += $getAllDays->LeCount;
                $resultY[$year]['ca_'.$suffix] += $getAllDays->LeCount * $getAllDays->tarif;

                $resultY[$year]['nb'] += $getAllDays->LeCount;
                $resultY[$year]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
                $resultS[$year][$semestre]['nb'] += $getAllDays->LeCount;
                $resultS[$year][$semestre]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
                $resultT[$year][$trimestre]['nb'] += $getAllDays->LeCount;
                $resultT[$year][$trimestre]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;
                $resultM[$year][$mois]['nb'] += $getAllDays->LeCount;
                $resultM[$year][$mois]['ca'] += $getAllDays->LeCount * $getAllDays->tarif;

            }
        }
    }
}

#echo '<pre>';
#print_r($resultY);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * On vient de construire le tableau des années.
 * On va calculer maintenant les différents TAUX
 */

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 * Sans tenir compte de la première année
 */

#echo '<pre>';
#print_r($resultY);
#echo '</pre>';
#die();

for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {

    // ----------------------------------------------------------------------------------------
    // On traite les ANNEES d'abord
    //
    // ----------------------------------------------------------------------------------------
    #$resultY[$i-1]['ca'] += $resultY[$i-1]['ca_a'] +  $resultY[$i-1]['ca_f'];
    #$resultY[$i-1]['nb'] += $resultY[$i-1]['nb_a'] +  $resultY[$i-1]['nb_f'];
    if ($resultY[$i-1]['ca'] != 0) {
        $resultY[$i]['txy'] = (($resultY[$i]['ca'] - $resultY[$i-1]['ca']) / $resultY[$i-1]['ca']);
        $resultY[$i]['txy'] = round(100 * $resultY[$i]['txy'], 2 ).' %';
    }
    else {
        $resultY[$i]['txy'] = '-';
    }

    
    /** -------------------------------------------------------------------------------------------
     * Partie facile : on traite les ANNEES
     */
    foreach ($inter as $key => $val) {

        // ----------------------------------------------------------------------------------------
        // La partie évolutive
        // ----------------------------------------------------------------------------------------
        $suffix = $val[ 'suffix' ];

        // ----------------------------------------------------------------------------------------
        // On traite les ANNEES d'abord
        // ----------------------------------------------------------------------------------------
        if ($resultY[$i-1]['ca_'.$suffix] != 0) {
            $resultY[$i]['txy_'.$suffix] = (($resultY[$i]['ca_'.$suffix] - $resultY[$i-1]['ca_'.$suffix]) / $resultY[$i-1]['ca_'.$suffix]);
            $resultY[$i]['txy_'.$suffix] = round(100 * $resultY[$i]['txy_'.$suffix], 2 ).' %';
        }
        else {
            $resultY[$i]['txy_'.$suffix] = '-';
        }

        // ----------------------------------------------------------------------------------------
        // On traite ensuite les SEMESTRES
        // ----------------------------------------------------------------------------------------
        for ($semestre = 1 ; $semestre <= 2 ; $semestre++ ) {

            /** -----------------------------------------------------------------------------------
             * On compare donc le trimestre avec le trimestre de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultS[$i-1][$semestre]['ca'] != 0) {
                $ca = $resultS[$i][$semestre]['ca'] ;
                $caPrec = $resultS[$i-1][$semestre]['ca'] ;
                $resultS[$i][$semestre]['txy'] = (($ca - $caPrec) / $caPrec);
                $resultS[$i][$semestre]['txy'] = round(100 * $resultS[$i][$semestre]['txy'], 2 ).' %';
            }
            else {
                $resultS[$i][$semestre]['txy'] = '-';
            }

            if ($resultS[$i-1][$semestre]['ca_'.$suffix] != 0) {
                $ca = $resultS[$i][$semestre]['ca_'.$suffix] ;
                $caPrec = $resultS[$i-1][$semestre]['ca_'.$suffix] ;
                $resultS[$i][$semestre]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultS[$i][$semestre]['txy_'.$suffix] = round(100 * $resultS[$i][$semestre]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultS[$i][$semestre]['txy_'.$suffix] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le semestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018-02 => 2018-01
             * 2018-01 => 2017-02
             */
            $semestrePrec =($semestre == 1 ? 2 : 1);
            $yearPrec = ($semestre == 1 ? $i - 1 : $i);
            if ($resultS[$yearPrec][$semestrePrec]['ca_'.$suffix] != 0) {

                $ca = $resultS[$i][$semestre]['ca_'.$suffix] ;
                $caPrec = $resultS[$yearPrec][$semestrePrec]['ca_'.$suffix] ;

                $resultS[$i][$semestre]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultS[$i][$semestre]['txy_'.$suffix] = round(100 * $resultS[$i][$semestre]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultS[$i][$semestre]['txy_'.$suffix] = '-';
            }
        }


        // ----------------------------------------------------------------------------------------
        // On traite ensuite les TRIMESTRES
        // ----------------------------------------------------------------------------------------
        for ($trimestre = 1 ; $trimestre <= 4 ; $trimestre++ ) {

            /** -----------------------------------------------------------------------------------
             * On compare donc le trimestre avec le trimestre de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultT[$i-1][$trimestre]['ca'] != 0) {
                $ca = $resultT[$i][$trimestre]['ca'] ;
                $caPrec = $resultT[$i-1][$trimestre]['ca'] ;
                $resultT[$i][$trimestre]['txy'] = (($ca - $caPrec) / $caPrec);
                $resultT[$i][$trimestre]['txy'] = round(100 * $resultT[$i][$trimestre]['txy'], 2 ).' %';
            }
            else {
                $resultT[$i][$trimestre]['txy'] = '-';
            }


            if ($resultT[$i-1][$trimestre]['ca_'.$suffix] != 0) {
                $ca = $resultT[$i][$trimestre]['ca_'.$suffix] ;
                $caPrec = $resultT[$i-1][$trimestre]['ca_'.$suffix] ;
                $resultT[$i][$trimestre]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultT[$i][$trimestre]['txy_'.$suffix] = round(100 * $resultT[$i][$trimestre]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultT[$i][$trimestre]['txy_'.$suffix] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le trimestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018/4 => 2018/3
             * 2018/3 => 2018/2
             * 2018/2 => 2018/1
             * 2018/1 => 2017/4
             */
            $trimestrePrec =($trimestre == 1 ? 4 : $trimestre - 1);
            $yearPrec = ($trimestre == 1 ? $i - 1 : $i);
            if ($resultT[$yearPrec][$trimestrePrec]['ca_'.$suffix] != 0) {

                $ca = $resultT[$i][$trimestre]['ca_'.$suffix] ;
                $caPrec = $resultT[$yearPrec][$trimestrePrec]['ca_'.$suffix] ;

                $resultT[$i][$trimestre]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultT[$i][$trimestre]['txy_'.$suffix] = round(100 * $resultT[$i][$trimestre]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultT[$i][$trimestre]['txy_'.$suffix] = '-';
            }
        }



        // ----------------------------------------------------------------------------------------
        // On traite ensuite les MOIS
        // ----------------------------------------------------------------------------------------
        for ($mois = 1 ; $mois <= 12 ; $mois++ ) {

            if ($resultM[$i-1][$mois]['ca'] != 0) {
                $ca = $resultM[$i][$mois]['ca'] ;
                $caPrec = $resultM[$i-1][$mois]['ca'] ;
                $resultM[$i][$mois]['txy'] = (($ca - $caPrec) / $caPrec);
                $resultM[$i][$mois]['txy'] = round(100 * $resultM[$i][$mois]['txy'], 2 ).' %';
            }
            else {
                $resultM[$i][$mois]['txy'] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * On compare donc le mois avec le mois de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultM[$i-1][$mois]['ca_'.$suffix] != 0) {
                $ca = $resultM[$i][$mois]['ca_'.$suffix] ;
                $caPrec = $resultM[$i-1][$mois]['ca_'.$suffix] ;
                $resultM[$i][$mois]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultM[$i][$mois]['txy_'.$suffix] = round(100 * $resultM[$i][$mois]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultM[$i][$mois]['txy_'.$suffix] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le trimestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018/4 => 2018/3
             * 2018/3 => 2018/2
             * 2018/2 => 2018/1
             * 2018/1 => 2017/4
             */
            $moisPrec =($mois == 1 ? 12 : $mois - 1);
            $yearPrec = ($mois == 1 ? $i - 1 : $i);
            if ($resultM[$yearPrec][$moisPrec]['ca_'.$suffix] != 0) {

                $ca = $resultM[$i][$mois]['ca_'.$suffix] ;
                $caPrec = $resultM[$yearPrec][$moisPrec]['ca_'.$suffix] ;

                $resultM[$i][$mois]['txy_'.$suffix] = (($ca - $caPrec) / $caPrec);
                $resultM[$i][$mois]['txy_'.$suffix] = round(100 * $resultM[$i][$mois]['txy_'.$suffix], 2 ).' %';
            }
            else {
                $resultM[$i][$mois]['txy_'.$suffix] = '-';
            }
        }



    }
}
unset($resultY[$currYear-5]);
unset($resultS[$currYear-5]);
unset($resultT[$currYear-5]);

unset($resultM[$currYear-5]);
unset($resultM[$currYear-4]);
unset($resultM[$currYear-3]);
unset($resultM[$currYear-2]);


/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'YEAR' => $currYear,
        'DATAY' => $resultY,
        'DATAS' => $resultS,
        'DATAT' => $resultT,
        'DATAM' => $resultM,
    )
);

return;

function getCampagneIDPrec($id) {

    $sqlGetPreviousCampagne = '
    SELECT FK_idCampagnePrec
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getPreviousCampagneExec = DbConnexion::getInstance()->prepare($sqlGetPreviousCampagne);
    $getPreviousCampagneExec->bindValue(':idc', $id, PDO::PARAM_INT);
    $getPreviousCampagneExec->execute();
    $getPreviousCampagne = $getPreviousCampagneExec->fetch(PDO::FETCH_OBJ);
    return $getPreviousCampagne->FK_idCampagnePrec;
}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToTrimestre($mois) {

    return (1+(($mois-1)%3/2)*2);

}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToSemestre($mois) {

    return $mois < 7 ? 1 : 2;

}
