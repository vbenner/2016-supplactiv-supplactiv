<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des points de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idIntervention' )) {

    /** Recherche des infos sur l'intervention */
    $RechercheInfoInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
    $RechercheInfoInterventionExc->execute();

    /** Recherche des intervenants */
    $RechercheIntervenantExc->execute();

    print json_encode(array(
        'info' => $RechercheInfoInterventionExc->fetch(PDO::FETCH_OBJ),
        'intervenants' => $RechercheIntervenantExc->fetchAll(PDO::FETCH_OBJ)
    ));
}