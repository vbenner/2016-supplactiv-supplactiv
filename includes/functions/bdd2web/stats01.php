<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Valeur annuelle (année en cours
 */
$result = array();
$sqlCountPDV = '
SELECT COUNT(FK_idPdv) AS LeCount
FROM su_intervention
WHERE dateDebut >= :deb
AND dateFin <= :fin
';
$countPDVExec = DbConnexion::getInstance()->prepare($sqlCountPDV);
$lib = array('Janv', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Déc');
for ($mois = 1; $mois <= 12; $mois++) {

    /** -------------------------------------------------------------------------------------------
     * On compte
     */
    $deb = date('Y-'.str_pad($mois, 2, '0', STR_PAD_LEFT).'-01 00:00:00');
    $fin = date("Y-m-t", strtotime($deb)).' 23:59:59';
    $countPDVExec->bindValue(':deb', $deb, PDO::PARAM_STR);
    $countPDVExec->bindValue(':fin', $fin, PDO::PARAM_STR);
    $countPDVExec->execute();
    $countPDV = $countPDVExec->fetch(PDO::FETCH_OBJ);

    $result[] = array(
        'month' => $lib[$mois-1],
        'pdv' => $countPDV->LeCount,
    );

}
print(json_encode($result));
