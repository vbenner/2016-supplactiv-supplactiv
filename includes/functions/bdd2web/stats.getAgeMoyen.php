<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des points de vente les plus / moins animés
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();


/** -----------------------------------------------------------------------------------------------
 * Calcul du nombre d'intervenants ACTIFS + age moyen
 * Il faut que les intervenants aient un contrat sur l'année
 */
$year = date('Y');
$month = date('m');
$sqlAgeMoyenInter = '
SELECT ROUND(AVG(FLOOR(DATEDIFF(NOW(), dateNaissance) / 365)), 2) AS lage, 
  COUNT(DISTINCT idIntervenant) AS lecount
FROM su_intervenant SUI
  INNER JOIN su_intervention SUIV ON SUI.idIntervenant = SUIV.FK_idIntervenant
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUIV.FK_idContrat
WHERE SUI.boolActif = 1
AND YEAR(SUIV.dateDebut) = :yeardu
AND MONTH(SUIV.dateDebut) = :month
';
$ageMoyenInterExec = DbConnexion::getInstance()->prepare($sqlAgeMoyenInter);
$ageMoyenInterExec->bindValue(':yeardu', $year, PDO::PARAM_STR);
$ageMoyenInterExec->bindValue(':month', $month, PDO::PARAM_STR);
$ageMoyenInterExec->execute();
$age = $ageMoyenInterExec->fetch(PDO::FETCH_OBJ);

/** -----------------------------------------------------------------------------------------------
 * Calcul du nombre d'intervenants ACTIFS + age moyen / HOMME
 */
$sqlAgeMoyenInter = '
SELECT ROUND(AVG(FLOOR(DATEDIFF(NOW(), dateNaissance) / 365)), 2) AS lage, 
  COUNT(DISTINCT idIntervenant) as lecount
FROM su_intervenant SUI
  INNER JOIN su_intervention SUIV on SUI.idIntervenant = SUIV.FK_idIntervenant
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUIV.FK_idContrat
WHERE SUI.boolActif = 1
AND genre = \'R\'
AND YEAR(SUIV.dateDebut) = :yeardu
AND MONTH(SUIV.dateDebut) = :month
';
$ageMoyenInterExec = DbConnexion::getInstance()->prepare($sqlAgeMoyenInter);
$ageMoyenInterExec->bindValue(':yeardu', $year, PDO::PARAM_STR);
$ageMoyenInterExec->bindValue(':month', $month, PDO::PARAM_STR);
$ageMoyenInterExec->execute();
$ageHomme = $ageMoyenInterExec->fetch(PDO::FETCH_OBJ);

/** -----------------------------------------------------------------------------------------------
 * Calcul du nombre d'intervenants ACTIFS + age moyen / HOMME
 */
$sqlAgeMoyenInter = '
SELECT ROUND(AVG(FLOOR(DATEDIFF(NOW(), dateNaissance) / 365)), 2) AS lage, 
  COUNT(DISTINCT idIntervenant) as lecount
FROM su_intervenant SUI
  INNER JOIN su_intervention SUIV on SUI.idIntervenant = SUIV.FK_idIntervenant
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUIV.FK_idContrat
WHERE SUI.boolActif = 1
AND YEAR(SUIV.dateDebut) = :yeardu
AND MONTH(SUIV.dateDebut) = :month
AND (genre = \'E\' OR genre = \'L\')
';
$ageMoyenInterExec = DbConnexion::getInstance()->prepare($sqlAgeMoyenInter);
$ageMoyenInterExec->bindValue(':yeardu', $year, PDO::PARAM_STR);
$ageMoyenInterExec->bindValue(':month', $month, PDO::PARAM_STR);
$ageMoyenInterExec->execute();
$ageFemme = $ageMoyenInterExec->fetch(PDO::FETCH_OBJ);

print json_encode(
    array(
        'RES' => 1,
        'AGE' => is_null($age->lage) ? 0 : $age->lage,
        'COUNT' => $age->lecount,
        'AGE_HOMME' => is_null($ageHomme->lage) ? 0 : $ageHomme->lage,
        'COUNT_HOMME' => $ageHomme->lecount,
        'AGE_FEMME' => is_null($ageFemme->lage) ? 0 : $ageFemme->lage,
        'COUNT_FEMME' => $ageFemme->lecount,
        'YEAR'=> $year,
        'MONTH'=> $month,
    )
);