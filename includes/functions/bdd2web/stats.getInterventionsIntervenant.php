<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des interventions / intervenant
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$data = array();
$keys = array();

$year = filter_input(INPUT_POST, 'annee');
$auto = filter_input(INPUT_POST, 'auto');
$inter = filter_input(INPUT_POST, 'inter');
$sansinter = filter_input(INPUT_POST, 'sansinter');

/** -----------------------------------------------------------------------------------------------
 * Qui a eu un contrat mois / mois
 */
$sqlGetListeInterventions = '
SELECT DISTINCT(SI.idIntervenant), SI.nomIntervenant, MONTH(SIN.dateDebut) AS LeMois, 
  COUNT(SIN.idIntervention) AS LeCount,
  SUM(IF(SIM.FK_idTypeMission = 3, 1, 0 ))AS LeCountA,
  SUM(IF(SIM.FK_idTypeMission = 2, 1, 0 ))AS LeCountF
FROM su_intervenant SI
	INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SI.idIntervenant
    INNER JOIN su_mission SIM ON SIM.idMission = SIN.FK_idMission
WHERE YEAR(SIN.dateDebut) = :year
'.
    ( $auto != 'ALL' ? ' AND boolAutoEntrepreneuse = :auto ' : '') .' '.
    ( $inter != 'ALL' ? ' AND SI.idIntervenant = :inter ' : '') . ' '.
'
AND SI.idIntervenant != 1
GROUP BY SI.idIntervenant, LeMois
ORDER BY nomIntervenant, LeMois
';

/** -----------------------------------------------------------------------------------------------
 * Preparation du Tableau des mois
 */
for ($i = 1 ; $i <= 13 ; $i++) {
    for ($j = 0; $j <= 1; $j++) {
        $month[$year-$j][$i] = array(
            'COUNT' => 0,
            'COUNTA' => 0,
            'COUNTF' => 0,
            'PERCENT' => '',
            'PERCENTA' => '',
            'PERCENTF' => ''
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * On récupère dans un premier TEMPS toute les valeurs
 */
for ($i = 0; $i <= 1 ; $i++) {
    $getListeInterventionsExec = DbConnexion::getInstance()->prepare($sqlGetListeInterventions);
    $getListeInterventionsExec->bindValue(':year', ($year-$i), PDO::PARAM_STR);
    if( $auto != 'ALL') {
        $getListeInterventionsExec->bindValue(':auto', $auto, PDO::PARAM_STR);
    }
    if( $inter != 'ALL') {
        $getListeInterventionsExec->bindValue(':inter', $inter, PDO::PARAM_INT);
    }
    $getListeInterventionsExec->execute();
    while ($listeInterventions = $getListeInterventionsExec->fetch(PDO::FETCH_OBJ)) {

        /** ---------------------------------------------------------------------------------------
         *
         */
        if (isset($data[$listeInterventions->idIntervenant])) {
            $data[$listeInterventions->idIntervenant]['DETAIL']
                [$year-$i][$listeInterventions->LeMois]['COUNT'] += $listeInterventions->LeCount;
            $data[$listeInterventions->idIntervenant]['DETAIL']
                [$year-$i][$listeInterventions->LeMois]['COUNTA'] += $listeInterventions->LeCountA;
            $data[$listeInterventions->idIntervenant]['DETAIL']
                [$year-$i][$listeInterventions->LeMois]['COUNTF'] += $listeInterventions->LeCountF;

        }
        else {
            /** -----------------------------------------------------------------------------------
             * Attention, même si on a l'intervenant qu'une année sur 2 il faut quand même
             * bien compléter les 2 années
             */
            $data[$listeInterventions->idIntervenant]['NOM'] = $listeInterventions->nomIntervenant;
            $data[$listeInterventions->idIntervenant]['DETAIL'] = $month;
            $data[$listeInterventions->idIntervenant]['DETAIL'][$year-$i][$listeInterventions->LeMois]['COUNT'] = $listeInterventions->LeCount;
            $data[$listeInterventions->idIntervenant]['DETAIL'][$year-$i][$listeInterventions->LeMois]['COUNTA'] = $listeInterventions->LeCountA;
            $data[$listeInterventions->idIntervenant]['DETAIL'][$year-$i][$listeInterventions->LeMois]['COUNTF'] = $listeInterventions->LeCountF;
        }
    }
}

#echo $sqlGetListeInterventions;
#die($data);

/** -----------------------------------------------------------------------------------------------
 * On fait le cumul
 */
foreach($data as $key => $val) {

    for ($j = 0; $j <= 1; $j++) {
        for ($i = 1; $i <= 12 ; $i++) {
            $data[$key]['DETAIL'][$year-$j][13]['COUNT'] += $val['DETAIL'][$year-$j][$i]['COUNT'];
            $data[$key]['DETAIL'][$year-$j][13]['COUNTA'] += $val['DETAIL'][$year-$j][$i]['COUNTA'];
            $data[$key]['DETAIL'][$year-$j][13]['COUNTF'] += $val['DETAIL'][$year-$j][$i]['COUNTF'];
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
foreach($data as $key => $val) {

    /** -------------------------------------------------------------------------------------------
     * Et on fait ça mois / mois
     */
    for ($i = 1; $i <= 12 ; $i++) {
        if ($val['DETAIL'][$year-1][$i]['COUNT'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENT'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNT'] -
                        $val['DETAIL'][$year-1][$i]['COUNT'])/$val['DETAIL'][$year-1][$i]['COUNT'], 2).' %';
        }

        if ($val['DETAIL'][$year-1][$i]['COUNTA'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENTA'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNTA'] -
                        $val['DETAIL'][$year-1][$i]['COUNTA'])/$val['DETAIL'][$year-1][$i]['COUNTA'], 2).' %';
        }

        if ($val['DETAIL'][$year-1][$i]['COUNTF'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENTF'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNTF'] -
                        $val['DETAIL'][$year-1][$i]['COUNTF'])/$val['DETAIL'][$year-1][$i]['COUNTF'], 2).' %';
        }

    }

    /** -------------------------------------------------------------------------------------------
     * On remonte d'un niveau
     */
    #$data[$key] = $data[$key][$year];
    #unset ($data[$key][$year]);
    #unset ($data[$key]['DETAIL'][$year-1]);
}

/** -----------------------------------------------------------------------------------------------
 * Si SANSINTER = TOUT, on ne fait pas de traitement
 * Si SANSINTER = OUI, on n'affiche que ceux qui ont des CUMUL = 0
 * Si SANSINTER = NON, on n'affiche que ceux qui ont des CUMUL > 0
 */
foreach($data as $key => $val) {

    if ($sansinter == 'OUI') {
        if ($val['DETAIL'][$year][13]['COUNT'] != 0) {
            unset ($data[$key]);
        }
    }
    elseif ($sansinter == 'NON') {
        if ($val['DETAIL'][$year][13]['COUNT'] == 0) {
            unset ($data[$key]);
        }
    }
}

foreach ($data as $key => $val) {
    $keys[] = $key;
}

print json_encode(
    array(
        'YEAR' => $year,
        'RES' => 1,
        'DATA' => $data,
        'KEYS' => $keys,
        'SQL' => $sqlGetListeMissions
    )
);