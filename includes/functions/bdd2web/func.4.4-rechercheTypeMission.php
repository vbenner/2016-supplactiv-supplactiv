<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des types de mission
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$RechercheTypeMissionExc->execute();
print json_encode($RechercheTypeMissionExc->fetchAll(PDO::FETCH_OBJ));