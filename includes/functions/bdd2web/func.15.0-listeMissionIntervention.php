<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des interventions de mission
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test si l'idCategorie est presente ou non */
if(filter_has_var(INPUT_POST, 'idMission')){

    /** Recherche des informations sur la mission */
    $RechercheInfoMissionExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'));
    $RechercheInfoMissionExc->execute();
    $InfoMission = $RechercheInfoMissionExc->fetch(PDO::FETCH_OBJ);

    /** @var $dateD Date de debut de mission */
    $dateD = new DateTime($InfoMission->dateDebut);

    /** @var $dateF Date de fin de mission */
    $dateF = new DateTime($InfoMission->dateFin);

    /** @var $boolFin Initialisation du booleen de fin */
    $boolFin = false;

    /** @var $arrayDaysMission Liste des informations de mission */
    $arrayDaysMission = array();

    /** On parcours les dates de la campagne */
    while(!$boolFin){

        /** Ajout d'une journee */
        $dateD->add(new DateInterval('P1D'));

        if($dateD <= $dateF){

            if(!isset($arrayDaysMission[$dateD->format('n')])){
                $arrayDaysMission[$dateD->format('n')] = [
                    'LIBELLE' => $dateD->format('m').' - '.$arrayMonthCorres[$dateD->format('n')]['libelleMax'].' '.$dateD->format('Y'),
                    'LIBELLEMIN' => $arrayMonthCorres[$dateD->format('n')]['libelleMin'],
                    'JOUR' => []
                ];
            }

            array_push($arrayDaysMission[$dateD->format('n')]['JOUR'], [
                'J' => $arrayDayCorres[$dateD->format('N')]['libelleMin'],
                'ID' => $dateD->format('d'),
                'D' => $dateD->format('Y-m-d'),
                'W' => $arrayDayCorres[$dateD->format('N')]['boolWD']
            ]);

        } else $boolFin = true;

    }



    $lstIntervention = array();
    $RechercheInterventionExc->bindValue(':idMission', $InfoMission->idMission, PDO::PARAM_INT);
    $RechercheInterventionExc->execute();
    while($InfoIntervention = $RechercheInterventionExc->fetch(PDO::FETCH_OBJ)){

        $tdMois = ''; $tdJour = '';
        foreach($arrayDaysMission as $idMois => $lstJour){
            $tdMois .= '<td class="warning" id="cptM_'.$idMois.'_'.$InfoIntervention->idIntervenant.'_'.$InfoIntervention->idPdv.'"></td>';
        }

        foreach($arrayDaysMission as $idMois => $lstJour){
            foreach($lstJour['JOUR'] as $infoJour){
                $tdJour .= '<td id="cptJ_'.$infoJour['D'].'_'.$InfoIntervention->idIntervenant.'_'.$InfoIntervention->idPdv.'" class="'.((!$infoJour['W'])? 'warning' : '').'"></td>';
            }
        }
        array_push($lstIntervention, [
           'intervenant' => $InfoIntervention->nomIntervenant.' '.$InfoIntervention->prenomIntervenant,
            'cip' => $InfoIntervention->codeMerval,
            'libelle' => $InfoIntervention->libellePdv,
            'adr' => $InfoIntervention->adressePdv_A,
            'cp' => $InfoIntervention->codePostalPdv,
            'ville' => $InfoIntervention->villePdv,
            'tel' => formatTel($InfoIntervention->telephoneMagasin),
            'mois' => $tdMois,
            'jour' => $tdJour
        ]);
    }

    print json_encode(['interventions' => $lstIntervention]);
}