<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des intervenants / campagne et date de mission
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$listeAffiliation = array(
    'intervenants' => array(),
    'campagnes' => array(),
    'dates' => array()
);

/** Recherche des intervenants */
$RechercheIntervenantExc->execute();
$listeAffiliation['intervenants'] = $RechercheIntervenantExc->fetchAll(PDO::FETCH_OBJ);

$lstAnnee = array();

/** Recherche des campagnes */
$RechercheCampagneExc->execute();
while($InfoCampagne = $RechercheCampagneExc->fetch(PDO::FETCH_OBJ)){
    array_push($listeAffiliation['campagnes'], $InfoCampagne);
    $lstAnnee[$InfoCampagne->anneeCampagne] = $InfoCampagne->anneeCampagne;
}

arsort($lstAnnee);
$listeAffiliation['dates'] = $lstAnnee;
$listeAffiliation['dates_d'] = date('Y');
print json_encode($listeAffiliation);
