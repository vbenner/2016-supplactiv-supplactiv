<?php
/**
 * Liste des missions pour une campagne
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

$RechercheMissionCampagneExc->bindValue(':idCampagne', filter_input(INPUT_GET, 'idCampagne'));
$RechercheMissionCampagneExc->execute();

print json_encode(array(
    'missions' => $RechercheMissionCampagneExc->fetchAll(PDO::FETCH_OBJ)
));
