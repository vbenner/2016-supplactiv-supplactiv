<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
$sqlGetInfoCategorie = '
SELECT libelleCategoriePdv
FROM su_pdv_categorie
WHERE idCategoriePdv = :id
';
$getInfoCategorieExec = DbConnexion::getInstance()->prepare($sqlGetInfoCategorie);
$getInfoCategorieExec->bindValue(':id', filter_input(INPUT_POST, 'idCategorie'), PDO::PARAM_INT);
$getInfoCategorieExec->execute();
$getInfoCategorie = $getInfoCategorieExec->fetch(PDO::FETCH_OBJ);
/** Recherche des categories de point de vente */
print json_encode(array(
    'result' => 1,
    'categorie' => $getInfoCategorie->libelleCategoriePdv
));
