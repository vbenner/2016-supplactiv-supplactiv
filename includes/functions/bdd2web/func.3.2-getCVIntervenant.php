<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
$sqlGetCV = '
SELECT b64_cv
FROM su_intervenant
WHERE idIntervenant = :id
';

$getCVExec = DbConnexion::getInstance()->prepare($sqlGetCV);
$getCVExec->bindValue(':id', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
$getCVExec->execute();
$getCV = $getCVExec->fetch(PDO::FETCH_OBJ);

print json_encode(array(
    'cv' => ($getCV->b64_cv != '' ? ADRESSE_BO.$getCV->b64_cv : '')
));
