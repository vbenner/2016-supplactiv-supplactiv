<?php
/**
 * Retour des interventions encore disponible pour un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';


if(
    filter_has_var(INPUT_POST, 'idIntervenant') &&
    filter_has_var(INPUT_POST, 'idMission') &&
    filter_has_var(INPUT_POST, 'dateDebut')
){

    $tmp = explode('+', filter_input(INPUT_POST, 'dateDebut'));
    $DT = new DateTime($tmp[0]);
    $DT2 = $DT;
    $DT2->modify( 'first day of next month' );
    $DT2->modify( '- 1 day');

    $RechercheInterventionDisponibleExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $RechercheInterventionDisponibleExc->bindValue(':idMission', filter_input(INPUT_POST, 'idMission'));
    $RechercheInterventionDisponibleExc->bindValue(':dateDebut', $DT->format('Y-m-').'01 00:00:00');
    $RechercheInterventionDisponibleExc->bindValue(':dateFin', $DT2->format('Y-m-d').' 23:59:59');
    $RechercheInterventionDisponibleExc->execute();

    print json_encode(array(
        'interventions' => $RechercheInterventionDisponibleExc->fetchAll(PDO::FETCH_OBJ)
    ));
}
