<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des missions / intervenant
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$data = array();
$datam = array();
$keys = array();

$inter = filter_input(INPUT_POST, 'intervenante');
$year = filter_input(INPUT_POST, 'annee');
$auto = filter_input(INPUT_POST, 'auto');

/** -----------------------------------------------------------------------------------------------
 * Qui a eu un contrat mois / mois
 */
$sqlGetListeMissions = '
SELECT DISTINCT(SI.idIntervenant), SI.nomIntervenant, COUNT(DISTINCT SIM.idMission) AS LeCount
FROM su_intervenant SI
	INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SI.idIntervenant
	INNER JOIN su_mission SIM ON SIM.idMission = SIN.FK_idMission
	INNER JOIN su_campagne SUC ON SUC.idCampagne = SIM.FK_idCampagne
WHERE YEAR(SUC.dateDebut) = :year
'.
    ( $inter != 'ALL' ? ' AND idIntervenant = :inter ' : '') .
    ( $auto != 'ALL' ? ' AND boolAutoEntrepreneuse = :auto ' : '') .
'
AND SI.idIntervenant != -1
GROUP BY SI.idIntervenant
ORDER BY nomIntervenant
';

/** -----------------------------------------------------------------------------------------------
 * Intervenant avec compteur Y, Y-1
 */

/** -----------------------------------------------------------------------------------------------
 * On récupère dans un premier TEMPS toute les valeurs
 */
for ($i = 0; $i <= 1 ; $i++) {
    $getListeMissionsExec = DbConnexion::getInstance()->prepare($sqlGetListeMissions);
    $getListeMissionsExec->bindValue(':year', ($year-$i), PDO::PARAM_STR);
    if( $inter != 'ALL') {
        $getListeMissionsExec->bindValue(':inter', $inter, PDO::PARAM_INT);
    }
    if( $auto != 'ALL') {
        $getListeMissionsExec->bindValue(':auto', $auto, PDO::PARAM_STR);
    }
    $getListeMissionsExec->execute();
    while ($getListeMissions = $getListeMissionsExec->fetch(PDO::FETCH_OBJ)) {
        /** ---------------------------------------------------------------------------------------
         *
         */
        if (isset($data[$getListeMissions->idIntervenant])) {
            $data[$getListeMissions->idIntervenant][($year-$i)]['COUNT'] = $getListeMissions->LeCount;
        }
        else {
            $data[$getListeMissions->idIntervenant][($year-$i)] = array(
                'NOM' => $getListeMissions->nomIntervenant,
                'COUNT' => $getListeMissions->LeCount,
                'PERCENT' => ''
            );
        }
    }
}

#echo '<pre>';
#print_r($data);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
foreach($data as $key => $val) {

    /** -------------------------------------------------------------------------------------------
     * On fait le calcul du pourcentage
     */
    if ($val[$year-1]['COUNT'] != 0) {
        $data[$key][$year]['PERCENT'] = round(100*($data[$key][$year]['COUNT'] - $val[$year-1]['COUNT'])/$val[$year-1]['COUNT'], 2).' %';
    }
    /** -------------------------------------------------------------------------------------------
     * On remonte d'un niveau
     */
    $keys[] = $key;
    $data[$key] = $data[$key][$year];
    unset ($data[$key][$year]);
    unset ($data[$key][$year-1]);
}

/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on va récupérer la liste des missions liées à l'année
 */
$sqlGetListeMissions = '
SELECT SI.nomIntervenant, SIM.libelleMission
FROM su_intervenant SI
	INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SI.idIntervenant
	INNER JOIN su_mission SIM ON SIM.idMission = SIN.FK_idMission
	INNER JOIN su_campagne SUC ON SUC.idCampagne = SIM.FK_idCampagne
WHERE YEAR(SUC.dateDebut) = :year
'.
    ( $inter != 'ALL' ? ' AND idIntervenant = :inter ' : '') .
    ( $auto != 'ALL' ? ' AND boolAutoEntrepreneuse = :auto ' : '') .
    '
AND SI.idIntervenant != -1
GROUP BY SI.nomIntervenant, SIM.libelleMission
ORDER BY nomIntervenant, libelleMission
';
$getListeMissionsExec = DbConnexion::getInstance()->prepare($sqlGetListeMissions);
$getListeMissionsExec->bindValue(':year', ($year), PDO::PARAM_STR);
if( $inter != 'ALL') {
    $getListeMissionsExec->bindValue(':inter', $inter, PDO::PARAM_INT);
}
if( $auto != 'ALL') {
    $getListeMissionsExec->bindValue(':auto', $auto, PDO::PARAM_STR);
}
$getListeMissionsExec->execute();
while ($getListeMissions = $getListeMissionsExec->fetch(PDO::FETCH_OBJ)) {
    /** ---------------------------------------------------------------------------------------
     *
     */
    $datam[] = array(
        'NOM' =>$getListeMissions->nomIntervenant,
        'MISSION' => $getListeMissions->libelleMission,
    );
}


print json_encode(
    array(
        'RES' => 1,
        'DATA' => $data,
        'DATAM' => $datam,
        'KEYS' => $keys,
        'SQL' => $sqlGetListeMissions,
        'YEAR' => $year,
        'INTER' => $inter,
        'AUTO' => $auto,
    )
);