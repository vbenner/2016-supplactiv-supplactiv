<?php
/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER / Page Up
 * @detail Recherche la campagne précédente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * On a la campagne en cours
 */
$sqlGetCampagnePrec = '
SELECT *
FROM su_campagne
WHERE dateFin < 
  (
    SELECT dateDebut
    FROM su_campagne
    WHERE idCampagne = :idc
  )
ORDER BY libelleCampagne
	';
$getCampagnePrecExec = DbConnexion::getInstance()->prepare($sqlGetCampagnePrec);
$getCampagnePrecExec->bindValue(':idc', $_POST['idCampagne'], PDO::PARAM_INT);
$getCampagnePrecExec->execute();

print json_encode($getCampagnePrecExec->fetchAll(PDO::FETCH_OBJ));