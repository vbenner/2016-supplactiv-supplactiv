<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des journées / intervenant
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$data = array();
$keys = array();
$month = array();

/** -----------------------------------------------------------------------------------------------
 *
 */
$tabmissions = array();

/** -----------------------------------------------------------------------------------------------
 * La MISSION devient FACULTATIVE....
 */
$year = filter_input(INPUT_POST, 'annee');
$mission = filter_input(INPUT_POST, 'mission');
$inter = filter_input(INPUT_POST, 'inter');
$auto = filter_input(INPUT_POST, 'auto');

$missionPrec = '';

/** -----------------------------------------------------------------------------------------------
 * Preparation du Tableau des mois
 */
for ($i = 1 ; $i <= 13 ; $i++) {
    for ($j = 0; $j <= 1; $j++) {
        $month[$year-$j][$i] = array(
            'COUNT' => 0,
            'PERCENT' => '',
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * Liste des INTERVNENANT
 * Si il n'y a pas de mission, c'est assez facile, on n'a que l'année à récupérer.
 * Si il y a la mission, on va être obligé de récupérer l'id de la mission précédente
 */
$sqlGetListeIntervenant = '
SELECT DISTINCT SAI.idIntervenant AS id, SAI.nomIntervenant AS nom
FROM su_intervenant SAI
	INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SAI.idIntervenant
	INNER JOIN su_pdv SUP ON SUP.idPdv = SIN.FK_idPdv
WHERE SAI.idIntervenant != 1
'.($inter != 'ALL' ? ' AND SAI.idIntervenant = :inter ' : '').'
'.($auto != 'ALL' ? ' AND SAI.boolAutoEntrepreneuse = :auto ' : '').'
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)
AND NOT ISNULL(SIN.FK_idContrat)
';

if ($mission != '') {
    $sqlGetListeIntervenant .= ' 
    AND (
        FK_idMission = :mission 
        OR FK_idMission = :missionprec
    )';
}
$getListeIntervenantExec = DbConnexion::getInstance()->prepare($sqlGetListeIntervenant);
if ($inter != 'ALL') {
    $getListeIntervenantExec->bindValue(':inter', $inter, PDO::PARAM_INT);
}
if ($auto != 'ALL') {
    $getListeIntervenantExec->bindValue(':auto', $auto, PDO::PARAM_STR);
}
$getListeIntervenantExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeIntervenantExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);

if ($mission != '') {
    /** -------------------------------------------------------------------------------------------
     * Quelle est la mission précédente ?
     */
    $sqlGetMissionPrec = '
    SELECT FK_idMissionPrec
    FROM su_mission
    WHERE idMission = :mission
    ';

    $getMissionPrecExec = DbConnexion::getInstance()->prepare($sqlGetMissionPrec);
    $getMissionPrecExec->bindValue(':mission', $mission, PDO::PARAM_INT);
    $getMissionPrecExec->execute();
    $getMissionPrec = $getMissionPrecExec->fetch(PDO::FETCH_OBJ);
    $missionPrec = $getMissionPrec->FK_idMissionPrec;
    $getListeIntervenantExec->bindValue(':mission', $mission, PDO::PARAM_INT);
    $getListeIntervenantExec->bindValue(':missionprec', $missionPrec, PDO::PARAM_INT);

}
$getListeIntervenantExec->execute();
while ($listeIntervenant = $getListeIntervenantExec->fetch(PDO::FETCH_OBJ)) {
    $data [$listeIntervenant->id] = array (
        'NOM' => $listeIntervenant->nom,
        'DETAIL' => $month
    );
}

/** -----------------------------------------------------------------------------------------------
 * On refait maintenant la même requête mais on va récupérer les détails
 */
$sqlGetListeIntervenant = '
SELECT DISTINCT SAI.idIntervenant AS id, SAI.nomIntervenant AS nom,
  COUNT( DISTINCT(DATE_FORMAT(SIN.dateDebut, \'%Y%m%d\'))) AS LeCount,
	YEAR(SIN.dateDebut) AS LAnnee, MONTH(SIN.dateDebut) AS LeMois
FROM su_intervenant SAI
	INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SAI.idIntervenant
	INNER JOIN su_pdv SUP ON SUP.idPdv = SIN.FK_idPdv
WHERE SAI.idIntervenant != 1
AND NOT ISNULL(SIN.FK_idContrat)
'.($inter != 'ALL' ? ' AND SAI.idIntervenant = :inter ' : '').'
'.($auto != 'ALL' ? ' AND SAI.boolAutoEntrepreneuse = :auto ' : '').'
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)';

if ($mission != '') {
    $sqlGetListeIntervenant .= ' 
    AND (
        FK_idMission = :mission 
        OR FK_idMission = :missionprec
    )';
}

$sqlGetListeIntervenant .= ' 
GROUP BY idIntervenant, LAnnee, LeMois
ORDER BY nom, LAnnee, LeMois
';

$getListeIntervenantExec = DbConnexion::getInstance()->prepare($sqlGetListeIntervenant);
$getListeIntervenantExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeIntervenantExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
if ($inter != 'ALL') {
    $getListeIntervenantExec->bindValue(':inter', $inter, PDO::PARAM_INT);
}
if ($auto != 'ALL') {
    $getListeIntervenantExec->bindValue(':auto', $auto, PDO::PARAM_STR);
}

if ($mission != '') {
    $getListeIntervenantExec->bindValue(':mission', $mission, PDO::PARAM_INT);
    $getListeIntervenantExec->bindValue(':missionprec', $missionPrec, PDO::PARAM_INT);
}

$getListeIntervenantExec->execute();
while ($listeIntervenant = $getListeIntervenantExec->fetch(PDO::FETCH_OBJ)) {
    $data[$listeIntervenant->id]['DETAIL'][$listeIntervenant->LAnnee][$listeIntervenant->LeMois]['COUNT'] = $listeIntervenant->LeCount;
}

/** -----------------------------------------------------------------------------------------------
 * Si on n'a pas la mission, on va essayer de SAVOIR sur quelles missions on a des données
 * dans le cas où l'intervenante est choisie)
 */
if ($mission == '' && $inter != '') {

    $sqlGetListeMissionIntervenant = '
    SELECT DISTINCT SAI.idIntervenant AS id, SAI.nomIntervenant AS nom,
    	SIN.FK_idMission AS mission, libelleMission,
        COUNT( DISTINCT(DATE_FORMAT(SIN.dateDebut, \'%Y%m%d\'))) AS LeCount,
	    YEAR(SIN.dateDebut) AS LAnnee, MONTH(SIN.dateDebut) AS LeMois
    FROM su_intervenant SAI
	    INNER JOIN su_intervention SIN ON SIN.FK_idIntervenant = SAI.idIntervenant
	    INNER JOIN su_pdv SUP ON SUP.idPdv = SIN.FK_idPdv
	    INNER JOIN su_mission SUU ON SUU.idMission = SIN.FK_idMission
    WHERE SAI.idIntervenant = :inter
    '.($auto != 'ALL' ? ' AND SAI.boolAutoEntrepreneuse = :auto ' : '').'
    AND NOT ISNULL(SIN.FK_idContrat)
    AND (
	    YEAR(SIN.dateDebut) = :year 
	    OR YEAR(SIN.dateDebut) = :yearprec
    ) 
    GROUP BY mission, LAnnee, LeMois
    ORDER BY libelleMission, LAnnee, LeMois
    ';
    $getListeMissionIntervenantExec = DbConnexion::getInstance()->prepare($sqlGetListeMissionIntervenant);
    $getListeMissionIntervenantExec->bindValue(':inter', $inter, PDO::PARAM_INT);
    if ($auto != 'ALL') {
        $getListeMissionIntervenantExec->bindValue(':auto', $auto, PDO::PARAM_STR);
    }
    $getListeMissionIntervenantExec->bindValue(':year', $year, PDO::PARAM_INT);
    $getListeMissionIntervenantExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
    $getListeMissionIntervenantExec->execute();
    while ($getListeMission = $getListeMissionIntervenantExec->fetch(PDO::FETCH_OBJ)) {
        if (isset($tabmissions[$getListeMission->libelleMission])) {
            $tabmissions[$getListeMission->libelleMission][($getListeMission->LAnnee == $year ? 'B' : 'A')]['DATA'][$getListeMission->LeMois] = $getListeMission->LeCount;
            $tabmissions[$getListeMission->libelleMission][($getListeMission->LAnnee == $year ? 'B' : 'A')]['DATA'][13] += $getListeMission->LeCount;
        }
        else {
            $tabmissions[$getListeMission->libelleMission] = array(
            );
            for ($i = 1; $i <= 13 ; $i++) {
                $tabmissions[$getListeMission->libelleMission]['B']['DATA'][$i] = 0;
                $tabmissions[$getListeMission->libelleMission]['A']['DATA'][$i] = 0;
            }
            $tabmissions[$getListeMission->libelleMission][($getListeMission->LAnnee == $year ? 'B' : 'A')]['DATA'][$getListeMission->LeMois] = $getListeMission->LeCount;
            $tabmissions[$getListeMission->libelleMission][($getListeMission->LAnnee == $year ? 'B' : 'A')]['DATA'][13] += $getListeMission->LeCount;
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
foreach($data as $key => $val) {

    /** -------------------------------------------------------------------------------------------
     * On fait le calcul du pourcentage
     */
    for ($i = 1; $i <= 12 ; $i++) {
        if ($val['DETAIL'][$year-1][$i]['COUNT'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENT'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNT'] - $val['DETAIL'][$year-1][$i]['COUNT'])/$val['DETAIL'][$year-1][$i]['COUNT'], 2).' %';
        }
        else {
            $data[$key]['DETAIL'][$year][$i]['PERCENT'] = '-';
        }
        $data[$key]['DETAIL'][$year][13]['COUNT'] += $data[$key]['DETAIL'][$year][$i]['COUNT'];
    }

    /** -------------------------------------------------------------------------------------------
     * On remonte d'un niveau
     */
    #unset ($data[$key]['DETAIL'][$year-1]);
}

/** -----------------------------------------------------------------------------------------------
 * On enlève ceux qui ne font rien
 */
foreach($data as $key => $val) {
    if ($val['DETAIL'][$year][13]['COUNT'] == 0) {
        unset($data[$key]);
    }
}

/** -----------------------------------------------------------------------------------------------
 * On recrée les clés
 */
foreach($data as $key => $val) {
    $keys[] = $key;
}

print json_encode(
    array(
        'RES' => 1,
        'DATA' => $data,
        'KEYS' => $keys,
        'MISSION'=> $tabmissions,
        'SQL' => $sqlGetListeIntervenant
    )
);