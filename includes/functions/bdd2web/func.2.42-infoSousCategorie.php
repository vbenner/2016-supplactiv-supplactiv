<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
$sqlGetInfoSousCategorie = '
SELECT libelleSousCategorie
FROM su_pdv_sous_categorie
WHERE FK_idCategorie = :id
ORDER BY libelleSousCategorie
';
$getInfoSousCategorieExec = DbConnexion::getInstance()->prepare($sqlGetInfoSousCategorie);
$getInfoSousCategorieExec->bindValue(':id', filter_input(INPUT_POST, 'idSousCategorie'), PDO::PARAM_INT);
$getInfoSousCategorieExec->execute();
$getInfoSousCategorie = $getInfoSousCategorieExec->fetch(PDO::FETCH_OBJ);
/** Recherche des categories de point de vente */
print json_encode(array(
    'result' => 1,
    'categorie' => $getInfoSousCategorie->libelleSousCategorie
));
