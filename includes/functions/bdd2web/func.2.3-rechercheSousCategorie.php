<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test si l'idCategorie est presente ou non */
if(filter_has_var(INPUT_POST, 'idCategorie')){

    /** Recherche des sous-categories de point de vente */
    $RechercheSousCategoriePdvExc->bindValue(':idCategorie', filter_input(INPUT_POST, 'idCategorie'), PDO::PARAM_INT);
    $RechercheSousCategoriePdvExc->execute();
    print json_encode(array(
        'result' => 1,
        'scategories' => $RechercheSousCategoriePdvExc->fetchAll(PDO::FETCH_OBJ)
    ));
}