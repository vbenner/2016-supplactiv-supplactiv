<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des INTERVENTIONS
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$currYear = date('Y');
$yearI = array();
$semestreI = array();
$trimestreI = array();
$monthI = array();
$noms = array();
$dissociate = $_POST[ 'dissociate' ] == 1 ? true : false;

/** -----------------------------------------------------------------------------------------------
 * On est obligé de mettre tous les noms des interlocuteurs qui sont connus pendant ces dernières
 * 6 années (5 + 1 pour calculer le taux avec la formule Y-1
 * Du coup, on prépare déjà le gros tableau final
 */
$sqlGetAllInterlocuteurs = '
SELECT DISTINCT SU.nomInterlocuteurAgence AS nom
FROM su_intervention SI
  INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
  INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
  INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
WHERE SI.dateDebut >= :du
AND SI.dateFin <= :au
AND SM.FK_idTypeMission = 2
AND SU.nomInterlocuteurAgence <> \'\'
AND NOT ISNULL(SI.FK_idContrat) 
ORDER BY nom ASC
';
$getAllInterlocuteursExec = DbConnexion::getInstance()->prepare($sqlGetAllInterlocuteurs);
$getAllInterlocuteursExec->bindValue(':du', ($currYear - 5) . '-01-01', PDO::PARAM_STR);
$getAllInterlocuteursExec->bindValue(':au', ($currYear) . '-12-31', PDO::PARAM_STR);
$getAllInterlocuteursExec->execute();
while ($getAllInterlocuteurs = $getAllInterlocuteursExec->fetch(PDO::FETCH_OBJ)) {

    // --------------------------------------------------------------------------------------------
    // On récupère juste pour simplifier la suite
    // --------------------------------------------------------------------------------------------
    $nom = $getAllInterlocuteurs->nom;

    // --------------------------------------------------------------------------------------------
    // On va se souvenir des noms
    // --------------------------------------------------------------------------------------------
    $noms[] = $nom;

    // --------------------------------------------------------------------------------------------
    // On gère les ANNEES (6 en tout)
    // --------------------------------------------------------------------------------------------
    $tab = array();
    for ($i = $currYear - 5; $i <= $currYear; $i++) {
        $tab[ $i ][ 'year' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'txy' ] = '-';
    }
    if ($dissociate == 1) {
        $yearI[ $nom ] = array(
            'nom' => $nom,
            'years' => $tab
        );
    }
    else {
        $yearI[ '_INTERVENTION' ] = array(
            'nom' => $nom,
            'years' => $tab
        );
    }

    // --------------------------------------------------------------------------------------------
    // On gère les SEMESTRES
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 12....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1; $i <= 12; $i++) {
        $tab[ $i ][ 'semestre' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'txy' ] = '-';
        $tab[ $i ][ 'txi' ] = '-';
    }
    if ($dissociate == 1) {
        $semestreI[ $nom ] = array(
            'nom' => $nom,
            'semestres' => $tab
        );
    }
    else {
        $semestreI[ '_INTERVENTION' ] = array(
            'nom' => $nom,
            'semestres' => $tab
        );
    }
    #$semestreI[$nom] = array(
    #    'nom' => $nom,
    #    'semestres' => $tab
    #);

    // --------------------------------------------------------------------------------------------
    // On gère les TRIMESTRES
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 24....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1; $i <= 24; $i++) {
        $tab[ $i ][ 'trimestres' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'txy' ] = '-';
        $tab[ $i ][ 'txi' ] = '-';
    }
    if ($dissociate == 1) {
        $trimestreI[ $nom ] = array(
            'nom' => $nom,
            'trimestres' => $tab
        );
    }
    else {
        $trimestreI[ '_INTERVENTION' ] = array(
            'nom' => $nom,
            'trimestres' => $tab
        );
    }
    #$trimestreI[$nom] = array(
    #    'nom' => $nom,
    #    'trimestres' => $tab
    #);

    // --------------------------------------------------------------------------------------------
    // On gère les MOIS
    // On gère sur 36 mois mais on n'en gardera que 24....
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 24....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1; $i <= 36; $i++) {
        $tab[ $i ][ 'months' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'txy' ] = '-';
        $tab[ $i ][ 'txi' ] = '-';
    }
    if ($dissociate == 1) {
        $monthI[ $nom ] = array(
            'nom' => $nom,
            'months' => $tab
        );
    }
    else {
        $monthI[ '_INTERVENTION' ] = array(
            'nom' => $nom,
            'months' => $tab
        );
    }
    #$monthI[$nom] = array(
    #    'nom' => $nom,
    #    'months' => $tab
    #);

}

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 */
for ($i = $currYear - 5; $i <= $currYear; $i++) {

    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SM.FK_idTypeMission = 2
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $i . '-01-01 00:00:00', PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $i . '-12-31 23:59:59', PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        if ($dissociate == 1) {
            $yearI[ $count->nom ][ 'years' ][ $i ][ 'nb' ] = $count->leCount;
        }
        else {
            $yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'nb' ] += $count->leCount;
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais SEMESTRE / SEMESTRE
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 5;
for ($i = 1; $i <= 12; $i++) {

    //
    if ($i % 2 == 0) {
        $du = $year . '-07-01 00:00:00';
        $au = $year . '-12-31 23:59:59';
    }
    else {
        $du = $year . '-01-01 00:00:00';
        $au = $year . '-06-30 23:59:59';
    }


    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SM.FK_idTypeMission = 2
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        if ($dissociate == 1) {
            $semestreI[ $count->nom ][ 'semestres' ][ $i ][ 'nb' ] = $count->leCount;
        }
        else {
            $semestreI[ '_INTERVENTION' ][ 'semestres' ][ $i ][ 'nb' ] += $count->leCount;
        }
    }

    if ($i % 2 == 0) {
        $year++;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais TRIMESTRE / TRIMESTRE
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 5;
for ($i = 1; $i <= 24; $i++) {

    // Détermine le numéro du trimestre
    $num = (1 + (($i - 1) % 4 / 3) * 3);
    if ($num == 1) {
        $du = $year . '-01-01 00:00:00';
        $au = $year . '-03-31 23:59:59';
    }
    elseif ($num == 2) {
        $du = $year . '-04-01 00:00:00';
        $au = $year . '-06-30 23:59:59';
    }
    elseif ($num == 3) {
        $du = $year . '-07-01 00:00:00';
        $au = $year . '-09-31 23:59:59';
    }
    else {
        $du = $year . '-10-01 00:00:00';
        $au = $year . '-12-31 23:59:59';
    }


    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SM.FK_idTypeMission = 2
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        #$trimestreI[$count->nom]['trimestres'][$i]['nb'] = $count->leCount;
        if ($dissociate == 1) {
            $trimestreI[ $count->nom ][ 'trimestres' ][ $i ][ 'nb' ] = $count->leCount;
        }
        else {
            $trimestreI[ '_INTERVENTION' ][ 'trimestres' ][ $i ][ 'nb' ] += $count->leCount;
        }
    }

    if ($num == 4) {
        $year++;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais MOIS / MOIS
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 2;
for ($i = 1; $i <= 36; $i++) {

    // Détermine le numéro du mois
    $num = (1 + (($i - 1) % 12 / 2) * 2);
    if ($num <= 11) {
        $du = $year . '-' . str_pad($num, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
        $au = $year . '-' . str_pad($num + 1, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
    }
    else {
        $du = $year . '-' . str_pad($num, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
        $au = $year + 1 . '-01-01 00:00:00';
    }

    #echo '- - - - - - - - - - -'."\r\n";
    #echo $year."\r\n";
    #echo $du."\r\n";
    #echo $au."\r\n";
    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin < :au
    AND SM.FK_idTypeMission = 2 
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        #$monthI[$count->nom]['months'][$i]['nb'] = $count->leCount;
        if ($dissociate == 1) {
            $monthI[ $count->nom ][ 'months' ][ $i ][ 'nb' ] = $count->leCount;
        }
        else {
            $monthI[ '_INTERVENTION' ][ 'months' ][ $i ][ 'nb' ] += $count->leCount;
        }
    }

    if ($num == 12) {
        $year++;
    }
}


/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on fait les calculs de taux salarié / salarié
 * Mais on ne fera pas l'année $currYear - 5
 */
if ($dissociate) {

    /** -------------------------------------------------------------------------------------------
     * Année / année
     */
    $hasData = 0;
    foreach ($noms as $nom) {
        for ($i = $currYear - 4; $i <= $currYear; $i++) {
            $hasData += $yearI[ $nom ][ 'years' ][ $i ][ 'nb' ];
            if ($yearI[ $nom ][ 'years' ][ $i - 1 ][ 'nb' ] != 0) {
                $yearI[ $nom ][ 'years' ][ $i ][ 'txy' ] = (($yearI[ $nom ][ 'years' ][ $i ][ 'nb' ] - $yearI[ $nom ][ 'years' ][ $i - 1 ][ 'nb' ]) / $yearI[ $nom ][ 'years' ][ $i - 1 ][ 'nb' ]);
                $yearI[ $nom ][ 'years' ][ $i ][ 'txy' ] = round(100 * $yearI[ $nom ][ 'years' ][ $i ][ 'txy' ], 2) . ' %';
            }
            else {
                $yearI[ $nom ][ 'years' ][ $i ][ 'txy' ] = '-';
            }
        }
        unset($yearI[ $nom ][ 'years' ][ $currYear - 5 ]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        if ($hasData == 0) {
            unset($yearI[$nom]);
        }
    }

    /** -------------------------------------------------------------------------------------------
     * Semestre / Semestre
     */
    foreach ($noms as $nom) {

        // ----------------------------------------------------------------------------------------
        // Décalage de 1 = semestre s-1
        // ----------------------------------------------------------------------------------------
        $hasData = 0;
        $decalage = 1;
        $target = 'txi';
        $tableau = 'semestreI';
        $interval = 'semestres';
        $bsup = 12;
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        // Décalage de 2 = semestre s (de l'année - 1)
        $decalage = 2;
        $target = 'txy';
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        unset($semestreI[ $nom ][ 'semestres' ][ 1 ]);
        unset($semestreI[ $nom ][ 'semestres' ][ 2 ]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = $decalage + 1 ; $i <= $bsup; $i++) {
            $hasData += $semestreI[ $nom ][ 'semestres' ][ $i ][ 'nb' ];
        }
        if ($hasData == 0) {
            unset($semestreI[$nom]);
        }

    }

    /** -------------------------------------------------------------------------------------------
     * Trimestres
     */
    foreach ($noms as $nom) {

        // Décalage de 1 = trimestre s-1
        $decalage = 1;
        $hasData = 0;
        $target = 'txi';
        $tableau = 'trimestreI';
        $interval = 'trimestres';
        $bsup = 24;
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        // Décalage de 4 = trimestre t (de l'année - 1)
        $decalage = 4;
        $target = 'txy';
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        unset($trimestreI[ $nom ][ 'trimestres' ][ 1 ]);
        unset($trimestreI[ $nom ][ 'trimestres' ][ 2 ]);
        unset($trimestreI[ $nom ][ 'trimestres' ][ 3 ]);
        unset($trimestreI[ $nom ][ 'trimestres' ][ 4 ]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = $decalage + 1 ; $i <= $bsup; $i++) {
            $hasData += $trimestreI[ $nom ][ 'trimestres' ][ $i ][ 'nb' ];
        }
        if ($hasData == 0) {
            unset($trimestreI[$nom]);
        }

    }

    /** -------------------------------------------------------------------------------------------
     * Mois
     */
    foreach ($noms as $nom) {

        #for ($i = 2; $i <= 36; $i++) {
        #    if ($monthI[ $nom ][ 'months' ][ $i - 1 ][ 'nb' ] != 0) {
        #        $monthI[ $nom ][ 'months' ][ $i ][ 'txy' ] = (($monthI[ $nom ][ 'months' ][ $i ][ 'nb' ] - $monthI[ $nom ][ 'months' ][ $i - 1 ][ 'nb' ]) / $monthI[ $nom ][ 'months' ][ $i - 1 ][ 'nb' ]);
        #        $monthI[ $nom ][ 'months' ][ $i ][ 'txy' ] = round(100 * $monthI[ $nom ][ 'months' ][ $i ][ 'txy' ], 2) . ' %';
        #    }
        #    else {
        #        $monthI[ $nom ][ 'months' ][ $i - 1 ][ 'txy' ] = '-';
        #    }
        #}

        // Décalage de 1 = mois s-1
        $decalage = 1;
        $hasData = 0;
        $target = 'txi';
        $tableau = 'monthI';
        $interval = 'months';
        $bsup = 36;
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        // Décalage de 4 = trimestre t (de l'année - 1)
        $decalage = 12;
        $target = 'txy';
        for ($i = 1 + $decalage; $i <= $bsup; $i++) {
            if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
            }
            else {
                ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            unset($monthI[ $nom ][ 'months' ][ $i ]);
        }

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = $decalage + 1 ; $i <= $bsup; $i++) {
            $hasData += $monthI[ $nom ][ 'months' ][ $i ][ 'nb' ];
        }
        if ($hasData == 0) {
            unset($monthI[$nom]);
        }
    }

}
else {

    /** -------------------------------------------------------------------------------------------
     * Années
     */
    $yearI[ '_INTERVENTION' ][ 'nom' ] = '-';
    for ($i = $currYear - 4; $i <= $currYear; $i++) {
        if ($yearI[ '_INTERVENTION' ][ 'years' ][ $i - 1 ][ 'nb' ] != 0) {
            $yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'txy' ] = (($yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'nb' ] - $yearI[ '_INTERVENTION' ][ 'years' ][ $i - 1 ][ 'nb' ]) / $yearI[ '_INTERVENTION' ][ 'years' ][ $i - 1 ][ 'nb' ]);
            $yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'txy' ] = round(100 * $yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'txy' ], 2) . ' %';
        }
        else {
            $yearI[ '_INTERVENTION' ][ 'years' ][ $i ][ 'txy' ] = '-';
        }
    }
    unset($yearI[ '_INTERVENTION' ][ 'years' ][ $currYear - 5 ]);

    /** -------------------------------------------------------------------------------------------
     * Semestres
     */
    $semestreI[ '_INTERVENTION' ][ 'nom' ] = '-';

    $decalage = 1;
    $target = 'txi';
    $tableau = 'semestreI';
    $interval = 'semestres';
    $bsup = 12;
    $nom = '_INTERVENTION';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }

    $decalage = 2;
    $target = 'txy';
    $bsup = 12;
    $nom = '_INTERVENTION';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }

    unset($semestreI[ '_INTERVENTION' ][ 'semestres' ][ 1 ]);
    unset($semestreI[ '_INTERVENTION' ][ 'semestres' ][ 2 ]);



    /** -------------------------------------------------------------------------------------------
     * Trimestres
     */
    $trimestreI[ '_INTERVENTION' ][ 'nom' ] = '-';

    // Décalage de 1 = trimestre t-1
    $decalage = 1;
    $target = 'txi';
    $tableau = 'trimestreI';
    $interval = 'trimestres';
    $bsup = 24;
    $nom = '_INTERVENTION';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }

    // Décalage de 4 = trimestre t (de l'année - 1)
    $decalage = 4;
    $target = 'txy';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }

    unset($trimestreI[ '_INTERVENTION' ][ 'trimestres' ][ 1 ]);
    unset($trimestreI[ '_INTERVENTION' ][ 'trimestres' ][ 2 ]);
    unset($trimestreI[ '_INTERVENTION' ][ 'trimestres' ][ 3 ]);
    unset($trimestreI[ '_INTERVENTION' ][ 'trimestres' ][ 4 ]);

    /** -------------------------------------------------------------------------------------------
     * Mois
     */
    $monthI[ '_INTERVENTION' ][ 'nom' ] = '-';



    // Décalage de 1 = mois s-1
    $decalage = 1;
    $target = 'txi';
    $tableau = 'monthI';
    $interval = 'months';
    $bsup = 36;
    $nom = '_INTERVENTION';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }

    // Décalage de 4 = trimestre t (de l'année - 1)
    $decalage = 12;
    $target = 'txy';
    for ($i = 1 + $decalage; $i <= $bsup; $i++) {
        if (${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ] != 0) {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = ((${$tableau}[ $nom ][ $interval ][ $i ][ 'nb' ] - ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]) / ${$tableau}[ $nom ][ $interval ][ $i - $decalage ][ 'nb' ]);
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = round(100 * ${$tableau}[ $nom ][ $interval ][ $i ][ $target ], 2) . ' %';
        }
        else {
            ${$tableau}[ $nom ][ $interval ][ $i ][ $target ] = '-';
        }
    }


    #for ($i = 2; $i <= 36; $i++) {
    #    if ($monthI[ '_INTERVENTION' ][ 'months' ][ $i - 1 ][ 'nb' ] != 0) {
    #        $monthI[ '_INTERVENTION' ][ 'months' ][ $i ][ 'txy' ] = (($monthI[ '_INTERVENTION' ][ 'months' ][ $i ][ 'nb' ] - $monthI[ '_INTERVENTION' ][ 'months' ][ $i - 1 ][ 'nb' ]) / $monthI[ '_INTERVENTION' ][ 'months' ][ $i - 1 ][ 'nb' ]);
    #        $monthI[ '_INTERVENTION' ][ 'months' ][ $i ][ 'txy' ] = round(100 * $monthI[ '_INTERVENTION' ][ 'months' ][ $i ][ 'txy' ], 2) . ' %';
    #    }
    #    else {
    #        $monthI[ '_INTERVENTION' ][ 'months' ][ $i - 1 ][ 'txy' ] = '-';
    #    }
    #}


    for ($i = 1; $i <= 12; $i++) {
        unset($monthI[ '_INTERVENTION' ][ 'months' ][ $i ]);
    }

}


/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'DATAY' => $yearI,
        'DATAS' => $semestreI,
        'DATAT' => $trimestreI,
        'DATAM' => $monthI,
    )
);
