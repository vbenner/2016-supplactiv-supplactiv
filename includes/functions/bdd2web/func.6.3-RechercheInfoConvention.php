<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur une convention
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';
# ------------------------------------------------------------------------------------
# Ajoute des caracteres a une chaine
# ------------------------------------------------------------------------------------
function ajoutCrc($Ch,$Taille,$Carac){
	$C = '';
	for($i = strlen($Ch); $i < $Taille; $i++){
		$C .= $Carac;
	}	
	return $C.$Ch;
}


$sqlRechercheContrat = "
SELECT *
FROM su_mission
	INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idMission = su_mission.idMission
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_contrat_cidd.FK_idIntervenant
WHERE idContrat = :IdContrat";
$RechercheContratExc = DbConnexion::getInstance()->prepare($sqlRechercheContrat);


$RechercheContratExc->bindValue(':IdContrat', $_POST['IdContrat'], PDO::PARAM_INT); 
$RechercheContratExc->execute();
while($InfoContrat = $RechercheContratExc->fetch(PDO::FETCH_OBJ)):
	echo 'Confirmez-vous la suppression du contrat <strong>'.ajoutCrc($InfoContrat->idContrat,6,'0').'</strong> de l\'intervenant <strong>'.$InfoContrat->nomIntervenant.' '.$InfoContrat->prenomIntervenant.'</strong> ? ';
endwhile;

?>