<?php
/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER / Page Up
 * @detail Recherche la mission précédente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * On a la campagne en cours et aussi la mission
 */
$sqlGetMissionPrec = '
SELECT *
FROM su_mission M
WHERE M.FK_idCampagne = (
	SELECT FK_idCampagnePrec
	FROM su_campagne
	WHERE idCampagne = :idc
)
ORDER BY libelleMission
';
$getMissionPrecExec = DbConnexion::getInstance()->prepare($sqlGetMissionPrec);
$getMissionPrecExec->bindValue(':idc', $_POST['idCampagne'], PDO::PARAM_INT);
$getMissionPrecExec->execute();

print json_encode($getMissionPrecExec->fetchAll(PDO::FETCH_OBJ));