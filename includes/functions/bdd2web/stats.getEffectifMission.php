<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des effectifs / mois (séparé / auto / pas auto)
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

$mission = filter_input(INPUT_POST, 'mission');
$year = filter_input(INPUT_POST, 'annee');

/** -----------------------------------------------------------------------------------------------
 * Qui a eu un contrat mois / mois
 */
$sqlGetMois = '
SELECT DISTINCT FK_idIntervenant, MONTH(dateCreationContrat) AS leMonth, 
  SI.boolAutoEntrepreneuse as auto
FROM su_contrat SUC
  INNER JOIN su_intervention SUI ON SUI.FK_idContrat = SUC.idContrat
  INNER JOIN su_intervenant SI ON SI.idIntervenant = SUI.FK_idIntervenant
WHERE YEAR(dateCreationContrat) = :year
AND FK_idMission = :mission
ORDER BY leMonth
';

$data[$year] = array();
$data[$year-1] = array();
for ($i = 1; $i <= 13 ; $i++) {
    $data[$year][$i] = array(
        'COUNT' => 0,
        'COUNT_AUTO' => 0,
        'COUNT_PASAUTO' => 0,
        'PERCENT' => '',
        'PERCENT_AUTO' => '',
        'PERCENT_PASAUTO' => '',
    );
    $data[$year-1][$i] = array(
        'COUNT' => 0,
        'COUNT_AUTO' => 0,
        'COUNT_PASAUTO' => 0,
        'PERCENT' => '',
        'PERCENT_AUTO' => '',
        'PERCENT_PASAUTO' => '',
    );
}

/** -----------------------------------------------------------------------------------------------
 * On récupère dans un premier TEMPS toute les valeurs
 */
for ($i = 1; $i >= 0; $i--) {

    /** -------------------------------------------------------------------------------------------
     * Si on est sur l'année en cours, la mission est la mission ! sinon on regarde la mission
     * précédente
     */
    if ($i == 0) {
        // rien ne change....
        $mission = filter_input(INPUT_POST, 'mission');;
    }
    else {
        // Mission = mission PREC
        $sqlGetMissionPrec = '
        SELECT FK_idMissionPrec
        FROM su_mission
        WHERE idMission = :mission
        ';
        $getMissionPrecExec = DbConnexion::getInstance()->prepare($sqlGetMissionPrec);
        $getMissionPrecExec->bindValue(':mission', $mission, PDO::PARAM_INT);
        $getMissionPrecExec->execute();
        $getMissionPrec = $getMissionPrecExec->fetch(PDO::FETCH_OBJ);
        $mission = $getMissionPrec->FK_idMissionPrec;
    }

    $getMoisExec = DbConnexion::getInstance()->prepare($sqlGetMois);
    $getMoisExec->bindValue(':year', ($year-$i), PDO::PARAM_STR);
    $getMoisExec->bindValue(':mission', $mission, PDO::PARAM_STR);
    $getMoisExec->execute();
    while ($getMois = $getMoisExec->fetch(PDO::FETCH_OBJ)) {

        /** -------------------------------------------------------------------------------------------
         * On regarde dans quelle case ventiler
         */
        $mois = $getMois->leMonth;
        $auto = $getMois->auto;

        $data[$year-$i][$mois]['COUNT'] += 1;
        if ($auto == 'OUI') {
            $data[$year-$i][$mois]['COUNT_AUTO'] += 1;
        }
        if ($auto == 'NON') {
            $data[$year-$i][$mois]['COUNT_PASAUTO'] += 1;
        }

        $data[$year-$i][13]['COUNT'] += 1;
        if ($auto == 'OUI') {
            $data[$year-$i][13]['COUNT_AUTO'] += 1;
        }
        if ($auto == 'NON') {
            $data[$year-$i][13]['COUNT_PASAUTO'] += 1;
        }
    }
}

/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
for ($i = 1; $i <= 13 ; $i++) {

    /** -------------------------------------------------------------------------------------------
     * Evolution globale
     */
    if ($data[$year-1][$i]['COUNT'] != 0) {
        $data[$year][$i]['PERCENT'] = (($data[$year][$i]['COUNT'] - $data[$year-1][$i]['COUNT']) / $data[$year-1][$i]['COUNT']);
        $data[$year][$i]['PERCENT'] = round(100 * $data[$year][$i]['PERCENT'] , 2 ).' %';
    }
    else {
        $data[$year][$i]['PERCENT'] = '-';
    }
}


/** -----------------------------------------------------------------------------------------------
 * L'année d'avant ne sert plus
 */
unset ($data[$year-1]);

print json_encode(
    array(
        'RES' => 1,
        'DATA' => $data[$year],
    )
);
return;


/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
for ($i = 1; $i <= 12 ; $i++) {
    if ($data[$year-1][$i]['COUNT'] != 0 && $data[$year][$i]['COUNT'] != 0) {
        $data[$year][$i]['PERCENT'] = round(100*($data[$year][$i]['COUNT'] - $data[$year-1][$i]['COUNT'])/$data[$year-1][$i]['COUNT'], 2).' %';
    }
    else {
        $data[$year][$i]['PERCENT'] = '-';
    }
}

/** -----------------------------------------------------------------------------------------------
 * L'année d'avant ne sert plus
 */
unset ($data[$year-1]);

print json_encode(
    array(
        'RES' => 1,
        'DATA' => $data[$year],
    )
);