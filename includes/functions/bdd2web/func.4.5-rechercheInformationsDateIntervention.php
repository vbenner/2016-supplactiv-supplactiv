<?php
ini_set('display_errors', 'on');
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur une date, on recherche si
 * pour cette date l'intervenante a déja des interventions ou si le point de vente
 * a deja d'autres interventions
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idIntervention' ) && filter_has_var ( INPUT_POST, 'dateIntervention' )) {


    $listeIntervention = array();

    $dateDRecherche = $dateFRecherche = null;
    if(filter_input( INPUT_POST, 'dateIntervention' ) != '') {
        /** @var $dateRecherche Formatage de la date */
        $dateRecherche = new DateTime(dateFrToSql(filter_input(INPUT_POST, 'dateIntervention'), ' 00:00:00'));

        $dateDRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . filter_input(INPUT_POST, 'heureDebut') . ':00');
        $dateFRecherche = new DateTime($dateRecherche->format('Y-m-d') . ' ' . filter_input(INPUT_POST, 'heureFin') . ':00');


        /** On recherche les dates d'interventions */
        $RechercheAutreInterventionExc->bindValue(':dateDebut', $dateRecherche->format('Y-m-d') . ' 00:00:00');
        $RechercheAutreInterventionExc->bindValue(':dateFin', $dateRecherche->format('Y-m-d') . ' 23:59:59');
        $RechercheAutreInterventionExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'));
        $RechercheAutreInterventionExc->execute();
        while ($InfoIntervention = $RechercheAutreInterventionExc->fetch(PDO::FETCH_OBJ)) {

            /** Initialisation des Booleen de camparaison */
            $boolDate = $boolIntervenant = $boolPdv = false;
            $dateD = $dateF = null;
            if (!is_null($InfoIntervention->dateDebut)) {
                $dateD = new DateTime($InfoIntervention->dateDebut);
                $dateF = new DateTime($InfoIntervention->dateFin);
            }

            if(!is_null($dateD) && !is_null($dateDRecherche) && (
                    ($dateDRecherche <= $dateD && $dateFRecherche >= $dateD) ||
                    ($dateDRecherche <= $dateF && $dateFRecherche >= $dateF) ||
                    ($dateDRecherche >= $dateD && $dateFRecherche <= $dateF))){
                $boolDate = true;
            }

            /** Cas 1 -> Le point de vente est le meme que celui de l'intervention
             * NON bloquant
             */
            if ($InfoIntervention->FK_idPdv == filter_input(INPUT_POST, 'idPdv') && $boolDate){
               $boolPdv = true;
            }

            /** Cas 2 -> L'intervenant est le meme que celui de l'intervention
             * BLOQUANT si horaire similaire
             */
            if ($InfoIntervention->FK_idIntervenant == filter_input(INPUT_POST, 'idIntervenant') && $InfoIntervention->FK_idIntervenant != 1 && $boolDate) {
                $boolIntervenant = true;
            }

            if($boolDate && ($boolPdv || $boolIntervenant)){
                $listeIntervention[$InfoIntervention->idIntervention] = array(
                    'info' => $InfoIntervention,
                    'boolDate' => $boolDate,
                    'boolPdv' => $boolPdv,
                    'boolIntervenant' => $boolIntervenant
                );
            }
        }
    }

    print json_encode($listeIntervention);
}