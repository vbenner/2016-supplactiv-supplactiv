<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Liste des intervenantes
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$LstIntervenant = array();

ini_set('display_errors', 'on');
/**
 * Recherche des intervenant travaillant pour le mois en cours
 */
$sqlRechercheIntervenantMois60 = "
SELECT idIntervenant, nomIntervenant, prenomIntervenant
FROM su_intervenant
    INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
WHERE boolAutoEntrepreneuse = 'NON'
AND boolActif = TRUE
GROUP BY idIntervenant, nomIntervenant, prenomIntervenant
";
$RechercheIntervenantMois60Exc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenantMois60);

/**
 * Recherche tarif KM
 */
$sqlRechercheContratKm = "
SELECT tarifKM
FROM su_intervenant
    INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
    INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE FK_idIntervenant = :idIntervenant AND dateDebut >= :dateDebut AND dateFin <= :dateFin
GROUP BY tarifKM";
$RechercheContratKm60Exc = DbConnexion::getInstance()->prepare($sqlRechercheContratKm);

# ------------------------------------------------------------------------------------
# PREPA REQUETE - Recherche des frais
# ------------------------------------------------------------------------------------
$sqlRechercheFraisIntervention = "
SELECT *
FROM su_intervenant_frais
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervenant_frais.FK_idIntervenant
WHERE dateFrais = :dateFrais AND FK_idIntervenant = :idIntervenant";
$RechercheFraisIntervention60Exc = DbConnexion::getInstance()->prepare($sqlRechercheFraisIntervention);

# ------------------------------------------------------------------------------------
# PREPA REQUETE - Insertion des frais d'intervention
# ------------------------------------------------------------------------------------
$sqlInsertionFrais = "
INSERT INTO su_intervenant_frais( FK_idIntervenant, dateFrais)
VALUES (:idIntervenant, :dateFrais)";
$InsertionFraisInit60Exc = DbConnexion::getInstance()->prepare($sqlInsertionFrais);

# ------------------------------------------------------------------------------------
# PREPA REQUETE - Insertion des KM par Intervenant
# ------------------------------------------------------------------------------------
$sqlInsertionKm = "
INSERT INTO su_intervenant_km (FK_idIntervenant, tarifKilometre, dateKilometre, totalKilometre)
VALUES(:idIntervenant, :tarifKilometre, :dateKilometre, :totalKilometre)";
$InsertionKmInit60Exc = DbConnexion::getInstance()->prepare($sqlInsertionKm);

$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 9) {
    $DT->sub(new DateInterval('P1M'));
}

/** Initialisation des frais intervenant */
$RechercheIntervenantMois60Exc->bindValue(':dateDebut', $DT->format('Y-m').'-01 00:00:00');
$RechercheIntervenantMois60Exc->bindValue(':dateFin', $DT->format('Y-m').'-31 23:59:59');
if(!$RechercheIntervenantMois60Exc->execute())print_r($RechercheIntervenantMois60Exc->errorInfo());
while($InfoIntervenant = $RechercheIntervenantMois60Exc->fetch(PDO::FETCH_OBJ)) {

    /** Recherche des frais */
    $RechercheFraisIntervention60Exc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
    $RechercheFraisIntervention60Exc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
    $RechercheFraisIntervention60Exc->execute();
    $boolFrais = ($RechercheFraisIntervention60Exc->rowCount() > 0) ? true : false;

    /** Insertion des KM */
    $RechercheContratKm60Exc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
    $RechercheContratKm60Exc->bindValue(':dateDebut', $DT->format('Y-m').'-01 00:00:00');
    $RechercheContratKm60Exc->bindValue(':dateFin', $DT->format('Y-m').'-31 23:59:59');
    $RechercheContratKm60Exc->execute();
    while ($InfoKm = $RechercheContratKm60Exc->fetch(PDO::FETCH_OBJ)) {
        $InsertionKmInit60Exc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
        $InsertionKmInit60Exc->bindValue(':dateKilometre', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
        $InsertionKmInit60Exc->bindValue(':tarifKilometre', $InfoKm->tarifKM, PDO::PARAM_STR);
        $InsertionKmInit60Exc->bindValue(':totalKilometre', 0, PDO::PARAM_INT);
        $InsertionKmInit60Exc->execute();
    }

    # ------------------------------------------------------------------------------------
    # On initialise les frais si ce n'est pas encore fait
    # ------------------------------------------------------------------------------------
    if (!$boolFrais) {
        $InsertionFraisInit60Exc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
        $InsertionFraisInit60Exc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
        $InsertionFraisInit60Exc->execute();
    }
}


/** ON test la presence des POSTS */
if(filter_has_var(INPUT_POST, 'dateFrais')) {
    $RechercheIntervenantMoisExc->bindValue(':dateFrais', filter_input(INPUT_POST, 'dateFrais'), PDO::PARAM_STR);
    $RechercheIntervenantMoisExc->execute();

    print json_encode(array(
        'intervenants' => $RechercheIntervenantMoisExc->fetchAll(PDO::FETCH_OBJ)
    ));
}else {
    $RechercheListeIntervenantNoAutoExec->execute();
    print json_encode(array(
        'intervenants' => $RechercheListeIntervenantNoAutoExec->fetchAll(PDO::FETCH_OBJ)
    ));
}
