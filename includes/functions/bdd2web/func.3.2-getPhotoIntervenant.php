<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
$sqlGetPhoto = '
SELECT b64_photo
FROM su_intervenant
WHERE idIntervenant = :id
';

$getPhotoExec = DbConnexion::getInstance()->prepare($sqlGetPhoto);
$getPhotoExec->bindValue(':id', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
$getPhotoExec->execute();
$getPhoto = $getPhotoExec->fetch(PDO::FETCH_OBJ);
if ($getPhoto->b64_photo != ''){
    $data = base64_decode($getPhoto->b64_photo);
    $fic = '../../../download/id.'.filter_input(INPUT_POST, 'idIntervenant');
    $list = glob($fic.'.*');
    if (count($list) == 0) {
        file_put_contents($fic, $data);

        switch (exif_imagetype ($fic)) {
            case 2 :
                rename ( $fic , $fic.'.jpg');
                $fic = $fic.'.jpg';
                break;
            case 3 :
                $fic = $fic.'.jpg';
                rename ( $fic , $fic.'.png');
                break;
        }
    } else {
        $fic = $list[0];
    }
    print json_encode(array(
        'photo' => str_replace('../../../', '', $fic)
    ));
} else {
    print json_encode(array(
        'photo' => ''
    ));
}
