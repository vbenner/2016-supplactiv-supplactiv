<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

/** -----------------------------------------------------------------------------------------------
 * Suivant ce qu'on demande
 */
$campagne = filter_input(INPUT_POST, 'campagne');
$mission = filter_input(INPUT_POST, 'mission');
#$from = filter_input(INPUT_POST, 'from');
#$from = DateTime::createFromFormat('d/m/Y', $from);
#$to = filter_input(INPUT_POST, 'to');
#$to = DateTime::createFromFormat('d/m/Y', $to);

/** -----------------------------------------------------------------------------------------------
 * CAMPAGNE DONNE ANNEE
 */
$sqlGetCampagne = '
SELECT *
FROM su_campagne
WHERE idCampagne = :idc
';
$getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
$getCampagneExec->bindValue(':idc', $campagne, PDO::PARAM_INT);
$getCampagneExec->execute();
$getCampagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);
$year = substr($getCampagne->dateFin, 0, 4);

/** -----------------------------------------------------------------------------------------------
 * Mission ou campagne ?
 */
$filter = '';
if ($mission != '') {
    $filter = ' idMission = :id ';
    $id = $mission;

    $sqlGetPrevMission = '
    SELECT FK_idMissionPrec AS prec
    FROM su_mission
    WHERE idMission = :id
    ';
    $getPrevMissionExec = DbConnexion::getInstance()->prepare($sqlGetPrevMission);
    $getPrevMissionExec->bindValue(':id', $id, PDO::PARAM_INT);
    $getPrevMissionExec->execute();
    $getPrevMission = $getPrevMissionExec->fetch(PDO::FETCH_OBJ);
    $idPrec = $getPrevMission->prec;

}
else {
    /** -------------------------------------------------------------------------------------------
     * Avec cette campagne, on peut maintenant connaitre la campagne précédente...
     */
    $id = $campagne;
    $filter = ' idCampagne = :id ';

    $sqlGetPrevCampagne = '
    SELECT FK_idCampagnePrec AS prec
    FROM su_campagne
    WHERE idCampagne = :id
    ';
    $getPrevCampagneExec = DbConnexion::getInstance()->prepare($sqlGetPrevCampagne);
    $getPrevCampagneExec->bindValue(':id', $id, PDO::PARAM_INT);
    $getPrevCampagneExec->execute();
    $getPrevCampagne = $getPrevCampagneExec->fetch(PDO::FETCH_OBJ);
    $idPrec = $getPrevCampagne->prec;
}

/** -----------------------------------------------------------------------------------------------
 * Tous les mois avec $j = année Y-1 et Y
 *
 * Donc on aura $month[YEAR][MONTH]
 *
 */
$month = $trimestre = $semestre = array();
for ($j = 0; $j <= 1; $j++) {
    for ($i = 1; $i <= 12 ; $i++) {
        $month[$j][$i] = 0;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des INTERVENTIONS de l'année Y-1 et ensuite celle de l'année Y
 */
$sqlGetInterventions = '
SELECT COUNT(*) AS LeCount, CAST(DATE_FORMAT(SI.dateDebut, \'%c\') AS UNSIGNED) AS mois
FROM su_intervention SI
  INNER JOIN su_mission SM on SM.idMission = SI.FK_idMission
  INNER JOIN su_campagne SC on SM.FK_idCampagne = SC.idCampagne
WHERE
'.$filter.'
AND NOT ISNULL(FK_idContrat)
AND DATE_FORMAT(SI.dateDebut, \'%Y\') = :year
GROUP BY mois
ORDER BY mois
';
$getInterventionsExec = DbConnexion::getInstance()->prepare($sqlGetInterventions);

/* ------------------------------------------------------------------------------------------------
 * YEAR
 */
$getInterventionsExec->bindValue(':id', $id, PDO::PARAM_INT);
$getInterventionsExec->bindValue(':year', $year, PDO::PARAM_INT);
$getInterventionsExec->execute();
while($getInterventions = $getInterventionsExec->fetch(PDO::FETCH_OBJ)) {
    $month[1][$getInterventions->mois] = $getInterventions->LeCount;
}

/* ------------------------------------------------------------------------------------------------
 * YEAR PREC (avec aussi l'id de la campagne / mission précédente
 */
$year--;
$getInterventionsExec->bindValue(':id', $idPrec, PDO::PARAM_INT);
$getInterventionsExec->bindValue(':year', $year, PDO::PARAM_INT);
$getInterventionsExec->execute();
while($getInterventions = $getInterventionsExec->fetch(PDO::FETCH_OBJ)) {
    $month[0][$getInterventions->mois] = $getInterventions->LeCount;
}

/** -----------------------------------------------------------------------------------------------
 * On a toutes les données mensuelles, on les transforme en données SEMESTRIELLES
 */
for ($i = 1 ; $i <= 12 ; $i++) {

    $t = (floor(($i - 1 ) / 3)) + 1;
    $s = (floor(($i - 1 ) / 6)) + 1;
    $trimestre[0][$t] += $month[0][$i];
    $trimestre[1][$t] += $month[1][$i];

    $semestre[0][$s] += $month[0][$i];
    $semestre[1][$s] += $month[1][$i];
}

/** ----------------------------------------------------------------------------------------------
 * On fait les calculs
 */
#for ($i = 1 ; $i <= 4 ; $i++) {
#    #$trimestre[]
#}
/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 * (on remonte l'année vu qu'on a fait un -- pour avoir l'année précédente).
 */
$year++;
print json_encode(
    array(
        'RES' => 1,
        'DATAM' => $month,
        'DATAT' => $trimestre,
        'DATAS' => $semestre,
        'YEAR' => $year
    )
);