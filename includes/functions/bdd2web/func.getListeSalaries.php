<?php
/**
 * @author Vincent BENNER
 * @detail Recherche la liste des salariés
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

$sqlGetListeSalaries = '
SELECT *
FROM du_utilisateur DU
  INNER JOIN du_utilisateur_type DUT ON DUT.idTypeUtilisateur = DU.FK_idTypeUtilisateur
WHERE DUT.idTypeUtilisateur != 1
';
$getListeSalariesExec = DbConnexion::getInstance()->prepare($sqlGetListeSalaries);
$getListeSalariesExec->execute();

print json_encode(array(
    'salaries' => $getListeSalariesExec->fetchAll(PDO::FETCH_OBJ)
));