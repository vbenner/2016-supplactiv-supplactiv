<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des rémunérations / année et / intervenant
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

$intervenant = filter_input(INPUT_POST, 'intervenant');

/** -----------------------------------------------------------------------------------------------
 * SELECTION DES TAUX
 * avec un éventuel filtre sur les intervenant ?
 */
$taux = array();
$sqlGetTaux = '
SELECT DISTINCT(SUC.salaireBrut) AS tx
FROM su_intervention SUI
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
WHERE
  YEAR(SUI.dateDebut) = :annee
';
if ($intervenant != 'ALL') {
    $sqlGetTaux .= ' AND SUI.FK_idIntervenant = :idinter ';
}
$sqlGetTaux.=' 
AND SUC.salaireBrut <> 0
ORDER BY tx 
';

$getTauxExec = DbConnexion::getInstance()->prepare($sqlGetTaux);
$getTauxExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
if ($intervenant != 'ALL') {
    $getTauxExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
}
$getTauxExec->execute();

$tabTx = array();
/** -----------------------------------------------------------------------------------------------
 * STEP 1
 * On obtient à ma fin une matrice croisée MOIS x TAUX
 */
while ($getTaux = $getTauxExec->fetch(PDO::FETCH_OBJ)) {
    $mois = array();
    for ($i = 1; $i <= 12; $i++) {
        $mois [$i] = 0;
    }
    $taux[$getTaux->tx] = array(
        'mois' => $mois
    );
    $tabTx[] = $getTaux->tx;
}

/** -----------------------------------------------------------------------------------------------
 * STEP 2
 * Récupération des données en partant cette fois ci des taux
 */
foreach ($taux as $untaux => $val) {

    $sqlGetCountByTaux = '
    SELECT COUNT(DISTINCT SUI.FK_idIntervenant) AS lecount, MONTH(SUI.dateDebut) as lemois
    FROM su_intervention SUI
      INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
    WHERE
      YEAR(dateDebut) = :annee
    AND salaireBrut = :taux
    ';

    if ($intervenant != 'ALL') {
        $sqlGetCountByTaux .= 'AND SUI.FK_idIntervenant = :idinter ';
    }

    $sqlGetCountByTaux .= '
    GROUP BY lemois
    ';

    $getCountByTauxExec = DbConnexion::getInstance()->prepare($sqlGetCountByTaux);
    $getCountByTauxExec->bindValue(':taux', $untaux, PDO::PARAM_STR);
    $getCountByTauxExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
    if ($intervenant != 'ALL') {
        $getCountByTauxExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
    }
    $getCountByTauxExec->execute();
    while ($getTaux = $getCountByTauxExec->fetch(PDO::FETCH_OBJ)) {
        $taux[ $untaux ][ 'mois' ][ $getTaux->lemois ] = $getTaux->lecount;
    }
}


/** -----------------------------------------------------------------------------------------------
 * SELECTION DES KILOMETRES
 * avec un éventuel filtre sur les intervenant ?
 */
$km = array();
$sqlGetKilometres = '
SELECT DISTINCT(SUC.tarifKM) AS tx
FROM su_intervention SUI
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
WHERE
  YEAR(SUI.dateDebut) = :annee
';
if ($intervenant != 'ALL') {
    $sqlGetKilometres .= ' AND SUI.FK_idIntervenant = :idinter ';
}
$sqlGetKilometres.=' 
AND SUC.tarifKM <> 0
ORDER BY tx ';

$getKilometresExec = DbConnexion::getInstance()->prepare($sqlGetKilometres);
$getKilometresExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
if ($intervenant != 'ALL') {
    $getKilometresExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
}
$getKilometresExec->execute();



$tabKms = array();
/** -----------------------------------------------------------------------------------------------
 * STEP 3
 * On obtient à ma fin une matrice croisée MOIS x TAUX
 */
while ($getKilometres = $getKilometresExec->fetch(PDO::FETCH_OBJ)) {
    $mois = array();
    for ($i = 1; $i <= 12; $i++) {
        $mois [$i] = 0;
    }
    $km[$getKilometres->tx] = array(
        'mois' => $mois
    );
    $tabKms[] = $getKilometres->tx;
}

/** -----------------------------------------------------------------------------------------------
 * STEP 4
 * Récupération des données en partant cette fois ci des taux
 */
foreach ($km as $untaux => $val) {

    $sqlGetCountByTaux = '
    SELECT COUNT(DISTINCT SUI.FK_idIntervenant) AS lecount, MONTH(SUI.dateDebut) as lemois
    FROM su_intervention SUI
      INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
    WHERE
      YEAR(dateDebut) = :annee
    AND tarifKM = :taux
    ';

    if ($intervenant != 'ALL') {
        $sqlGetCountByTaux .= 'AND SUI.FK_idIntervenant = :idinter ';
    }

    $sqlGetCountByTaux .= '
    GROUP BY lemois
    ';

    $getCountByTauxExec = DbConnexion::getInstance()->prepare($sqlGetCountByTaux);
    $getCountByTauxExec->bindValue(':taux', $untaux, PDO::PARAM_STR);
    $getCountByTauxExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
    if ($intervenant != 'ALL') {
        $getCountByTauxExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
    }

    $getCountByTauxExec->execute();
    while ($getTaux = $getCountByTauxExec->fetch(PDO::FETCH_OBJ)) {
        $km[ $untaux ][ 'mois' ][ $getTaux->lemois ] = $getTaux->lecount;
    }
}


/** -----------------------------------------------------------------------------------------------
 * SELECTION DES TAUX FORFAIT TELEPHONIQUE
 * avec un éventuel filtre sur les intervenant ?
 */
$tel = array();
$sqlGetForfaitsTelephoniques = '
SELECT DISTINCT(SUF.fraisTelephone) AS tx
FROM su_intervention SUI
  INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
WHERE
  YEAR(SUI.dateDebut) = :annee
';
if ($intervenant != 'ALL') {
    $sqlGetForfaitsTelephoniques .= ' AND SUI.FK_idIntervenant = :idinter ';
}
$sqlGetForfaitsTelephoniques.=' 
ORDER BY tx ';

$getForfaitsTelephoniquesExec = DbConnexion::getInstance()->prepare($sqlGetForfaitsTelephoniques);
$getForfaitsTelephoniquesExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
if ($intervenant != 'ALL') {
    $getForfaitsTelephoniquesExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
}
$getForfaitsTelephoniquesExec->execute();



$tabTels = array();
/** -----------------------------------------------------------------------------------------------
 * STEP 3
 * On obtient à ma fin une matrice croisée MOIS x TAUX
 */
while ($getForfaitsTelephoniques = $getForfaitsTelephoniquesExec->fetch(PDO::FETCH_OBJ)) {
    $mois = array();
    for ($i = 1; $i <= 12; $i++) {
        $mois [$i] = 0;
    }
    $tel[$getForfaitsTelephoniques->tx] = array(
        'mois' => $mois
    );
    $tabTels[] = $getForfaitsTelephoniques->tx;
}

/** -----------------------------------------------------------------------------------------------
 * STEP 4
 * Récupération des données en partant cette fois ci des taux
 */
foreach ($tel as $untaux => $val) {

    $sqlGetCountByTaux = '
    SELECT COUNT(DISTINCT SUI.FK_idIntervenant) AS lecount, MONTH(SUI.dateDebut) as lemois
    FROM su_intervention SUI
      INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
      INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
    WHERE
      YEAR(dateDebut) = :annee
    AND SUF.fraisTelephone = :taux
    ';

    if ($intervenant != 'ALL') {
        $sqlGetCountByTaux .= 'AND SUI.FK_idIntervenant = :idinter ';
    }

    $sqlGetCountByTaux .= '
    GROUP BY lemois
    ';

    $getCountByTauxExec = DbConnexion::getInstance()->prepare($sqlGetCountByTaux);
    $getCountByTauxExec->bindValue(':taux', $untaux, PDO::PARAM_STR);
    $getCountByTauxExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
    if ($intervenant != 'ALL') {
        $getCountByTauxExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
    }

    $getCountByTauxExec->execute();
    while ($getTaux = $getCountByTauxExec->fetch(PDO::FETCH_OBJ)) {
        $tel[ $untaux ][ 'mois' ][ $getTaux->lemois ] = $getTaux->lecount;
    }
}

/** -----------------------------------------------------------------------------------------------
 * SELECTION DES TAUX FORFAIT REPAS
 * avec un éventuel filtre sur les intervenant ?
 */
$repas = array();
$sqlGetForfaitsRepas = '
SELECT DISTINCT(SUF.fraisRepas) AS tx
FROM su_intervention SUI
  INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
  INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
WHERE
  YEAR(SUI.dateDebut) = :annee
';
if ($intervenant != 'ALL') {
    $sqlGetForfaitsRepas .= ' AND SUI.FK_idIntervenant = :idinter ';
}
$sqlGetForfaitsRepas.=' 
AND SUF.fraisRepas <> 0
ORDER BY tx ';

$getForfaitsRepasExec = DbConnexion::getInstance()->prepare($sqlGetForfaitsRepas);
$getForfaitsRepasExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
if ($intervenant != 'ALL') {
    $getForfaitsRepasExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
}
$getForfaitsRepasExec->execute();



$tabRepas = array();
/** -----------------------------------------------------------------------------------------------
 * STEP 3
 * On obtient à ma fin une matrice croisée MOIS x TAUX
 */
while ($getForfaitsRepas = $getForfaitsRepasExec->fetch(PDO::FETCH_OBJ)) {
    $mois = array();
    for ($i = 1; $i <= 12; $i++) {
        $mois [$i] = 0;
    }
    $repas[$getForfaitsRepas->tx] = array(
        'mois' => $mois
    );
    $tabRepas[] = $getForfaitsRepas->tx;
}

/** -----------------------------------------------------------------------------------------------
 * STEP 4
 * Récupération des données en partant cette fois ci des taux
 */
foreach ($repas as $untaux => $val) {

    $sqlGetCountByTaux = '
    SELECT COUNT(DISTINCT SUI.FK_idIntervenant) AS lecount, MONTH(SUI.dateDebut) as lemois
    FROM su_intervention SUI
      INNER JOIN su_contrat SUC ON SUC.idContrat = SUI.FK_idContrat
      INNER JOIN su_intervention_frais SUF ON SUF.FK_idIntervention = SUI.idIntervention
    WHERE
      YEAR(dateDebut) = :annee
    AND SUF.fraisRepas = :taux
    ';

    if ($intervenant != 'ALL') {
        $sqlGetCountByTaux .= 'AND SUI.FK_idIntervenant = :idinter ';
    }

    $sqlGetCountByTaux .= '
    GROUP BY lemois
    ';

    $getCountByTauxExec = DbConnexion::getInstance()->prepare($sqlGetCountByTaux);
    $getCountByTauxExec->bindValue(':taux', $untaux, PDO::PARAM_STR);
    $getCountByTauxExec->bindValue(':annee', $_POST['annee'], PDO::PARAM_STR);
    if ($intervenant != 'ALL') {
        $getCountByTauxExec->bindValue(':idinter', $intervenant, PDO::PARAM_INT);
    }

    $getCountByTauxExec->execute();
    while ($getTaux = $getCountByTauxExec->fetch(PDO::FETCH_OBJ)) {
        $repas[ $untaux ][ 'mois' ][ $getTaux->lemois ] = $getTaux->lecount;
    }
}

#print_r($repas);

print json_encode(
    array(
        'RES' => 1,
        'KEYS' => $tabTx,
        'TAUX' => $taux,
        'KEYS_KM' => $tabKms,
        'TAUX_KM' => $km,
        'KEYS_TEL' => $tabTels,
        'TAUX_TEL' => $tel,
        'KEYS_REPAS' => $tabRepas,
        'TAUX_REPAS' => $repas
    )
);