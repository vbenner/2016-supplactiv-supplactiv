<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** Recherche des categories de point de vente */
$RechercheCategoriePdvExc->execute();
print json_encode(array(
    'result' => 1,
    'categories' => $RechercheCategoriePdvExc->fetchAll(PDO::FETCH_OBJ)
));
