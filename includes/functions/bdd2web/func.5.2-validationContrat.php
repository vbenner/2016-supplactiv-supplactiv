<?php
/**
 * Verification des informations sur un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
#ini_set('display_errors', 'on');
/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

/** -----------------------------------------------------------------------------------------------
 * Utilisation d'un mode DEBUG pour le cas de Julie GARNIER
 */
$inter = 0;
$mission = 0;
if (isset($_POST['pInter52'])) {
    $inter = $_POST['pInter52'];
}
else {
    $inter = $_SESSION['idIntervenant_5_2'];
}
if (isset($_POST['pMission52'])) {
    $mission = $_POST['pMission52'];
}
else {
    $mission = $_SESSION['idMission_5_2'];
}

$RechercheInfoMinIntervenantExc->bindValue(':idIntervenant', $inter, PDO::PARAM_INT);
$RechercheInfoMinIntervenantExc->execute();

$RechercheInfoMissionExc->bindValue(':idMission', $mission, PDO::PARAM_INT);
$RechercheInfoMissionExc->execute();
$InfoMission = $RechercheInfoMissionExc->fetch(PDO::FETCH_OBJ);

if (filter_input(INPUT_POST, 'boolConfirme') == 0) {
    $boolErreurDate = false;
    $datePrecedente = null;
    $dureeTotal = array();

    $listeIntervention = array();
    /** On parcours les interventions */

    /** -------------------------------------------------------------------------------------------
     * STEP #1 -
     * Pour chaque intervention, on va calculer le nombre d'heures hebdomadaires
     */
    $tabWeek = array();
    foreach ($_POST['listeIntervention'] as $idIntervention) {

        /** Recherche des informatiosn sur l'intervention */
        $RechercheDureeInterventionExc->bindValue(':idIntervention', $idIntervention);
        $RechercheDureeInterventionExc->execute();
        $InfoDuree = $RechercheDureeInterventionExc->fetch(PDO::FETCH_OBJ);

        array_push($listeIntervention, $InfoDuree);

        /** On isole déjà SEMAINE / SEMAINE -----------------------------------------------------*/
        $date = new DateTime($InfoDuree->dateDebut);
        $week = $date->format("W");
        if (isset($tabWeek[$week])) {

        } else {
            $tabWeek[$week] = 0;
        }

        /** On ajoute maintenant la durée -------------------------------------------------------*/
        if (round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2) > 7) {
            $duree = round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2) - 1;
        }
        else {
            $duree = round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2);
        }
        #echo $idIntervention.' - Semaine '.$week.' -> '.$duree.'<br/>';
        $tabWeek[$week] += $duree;

        /** On test si la date d'intervention est dans le meme mois que la precedente */
        $dateD = new DateTime($InfoDuree->dateDebut);
        if (!is_null($datePrecedente)) {
            $boolErreurDate = ($datePrecedente->format('Y-m') != $dateD->format('Y-m')) ? true : $boolErreurDate;
        }

        $datePrecedente = $dateD;
        if (!$boolErreurDate) {
            @$dureeTotal[$dateD->format('Y-m-d')] += $InfoDuree->dureeSeconde;
        }
    }

    #echo '<pre>';
    #print_r($tabWeek);
    #echo '</pre>';
    #die();

    /** -------------------------------------------------------------------------------------------
     * STEP #2
     * On va ajouter aussi les trucs des autres INTERVENTIONS plannifiées dans les mêmes semaines
     * mais on refait le même traitement.
     */
    foreach ($tabWeek as $uneWeek => $value) {

        /** ---------------------------------------------------------------------------------------
         * On recherche des trucs dans la même MISSION
         */
        $sqlGetInterventionSameWeek = '
        SELECT idIntervention, dateDebut, dateFin,  
            libellePdv, codePostalPdv, villePdv, DATE_FORMAT(dateDebut, "%d/%m/%Y") AS dteIntervention, 
            DATE_FORMAT(dateDebut, "%H:%i") AS dteHDIntervention, DATE_FORMAT(dateFin, "%H:%i") AS dteHFIntervention
        FROM su_intervention
            INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
        WHERE idIntervention NOT IN ('.implode(',', $_POST['listeIntervention']).')
        AND FK_idIntervenant = :idinter 
        AND su_intervention.FK_idMission = :idmission 
        AND NOT ISNULL(FK_idContrat)
        AND DATE_FORMAT(dateDebut, "%v") = :week
        AND DATE_FORMAT(dateDebut, "%Y") = :year
        ';

        $getInterventionSameWeekExec = DbConnexion::getInstance()->prepare($sqlGetInterventionSameWeek);
        $getInterventionSameWeekExec->bindValue(':idinter', $inter, PDO::PARAM_INT);
        $getInterventionSameWeekExec->bindValue(':idinter', $mission, PDO::PARAM_INT);
        $getInterventionSameWeekExec->bindValue(':week', $uneWeek, PDO::PARAM_INT);
        $getInterventionSameWeekExec->bindValue(':year', $date->format("Y"), PDO::PARAM_INT);
        $getInterventionSameWeekExec->execute();
        while ($uneAutreInter = $getInterventionSameWeekExec->fetch(PDO::FETCH_OBJ)) {


            /** Recherche des informatiosn sur l'intervention */
            $RechercheDureeInterventionExc->bindValue(':idIntervention', $uneAutreInter->idIntervention);
            $RechercheDureeInterventionExc->execute();
            $InfoDuree = $RechercheDureeInterventionExc->fetch(PDO::FETCH_OBJ);

            /** On ajoute maintenant la durée -------------------------------------------------------*/
            if (round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2) > 7) {
                $duree = round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2) - 1;
            }
            else {
                $duree = round((strtotime($InfoDuree->dateFin) - strtotime($InfoDuree->dateDebut))/3600, 2);
            }
            #echo $idIntervention.' - Semaine '.$week.' -> '.$duree.'<br/>';
            $tabWeek[$week] += $duree;

            /** On test si la date d'intervention est dans le meme mois que la precedente */
            $dateD = new DateTime($InfoDuree->dateDebut);
            if (!$boolErreurDate) {
                @$dureeTotal[$dateD->format('Y-m-d')] += $InfoDuree->dureeSeconde;
            }
        }
    }

    /** On ajoute maintenant la durée -----------------------------------------------------------*/
    $hasHeuresSupp = false;
    foreach ($tabWeek as $uneWeek) {
        if ($uneWeek > 35) {
            $hasHeuresSupp = true;
        }
    }
    $dureeTotalFinal = 0;
    foreach ($dureeTotal as $date => $nbSeconde) {
        $dureeTotalFinal += ($nbSeconde >= 28800) ? $nbSeconde - 3600 : $nbSeconde;
    }

    /** On regarde aussi pour la date de fin de titre de séjour ---------------------------------*/
    $sqlGetInfoSejour = '
    SELECT dateFinSejour
    FROM su_intervenant
    WHERE idIntervenant = :idinter
    ';
    $getInfoSejourExec = DbConnexion::getInstance()->prepare($sqlGetInfoSejour);
    $getInfoSejourExec->bindValue(':idinter', $inter, PDO::PARAM_STR);
    $getInfoSejourExec->execute();
    $getInfo = $getInfoSejourExec->fetch(PDO::FETCH_OBJ);
    $titreInfo = array();
    $color = '';
    $txt = '';
    if ($getInfo->dateFinSejour != '0000-00-00' &&
        $getInfo->dateFinSejour != ''
    ) {
        $dayNow = date("Y-m-d");
        $monthEnd = date("Y-m-d", strtotime($dayNow . ' + 1 month'));
        if (

            $getInfo->dateFinSejour > $dayNow &&
            $getInfo->dateFinSejour <= $monthEnd
        ) {
            $color = 'warning';
            $txt = 'Attention, le titre de séjour de cet intervenant expire le '.dateSqlToFr(
                        $getInfo->dateFinSejour, false, false, false);
        }

        if ($getInfo->dateFinSejour <= $dayNow) {

            $color = 'danger';
            $txt = 'Attention, le titre de séjour de cet intervenant a expiré le '.dateSqlToFr(
                    $getInfo->dateFinSejour, false, false, false
                );
        }
    }

    print json_encode(array(
        'boolDate' => ($boolErreurDate) ? 1 : 0,
        'dureeTotalFinal' => formatSecToMin($dureeTotalFinal),
        'intervenant' => $RechercheInfoMinIntervenantExc->fetch(PDO::FETCH_OBJ),
        'mission' => $InfoMission,
        'infoSaisi' => $_POST,
        'sessionActuelle' => $_SESSION,
        'listeIntervention' => $listeIntervention,
        'dureeHeureSupp' => $hasHeuresSupp ? 1 : 0,
        'sejour' => array(
            'color' => $color,
            'txt' => $txt)
    ));
}
else {

    #echo '<pre>';
    #print_r($_POST);
    #echo '</pre>';
    #echo '<pre>';
    #print_r($_SESSION);
    #echo '</pre>';
    #die();


    /** -------------------------------------------------------------------------------------------
     * Mise à jour du 2018-09-28
     */
    $AjoutContratExc->bindValue(':salaireBrut', filter_input(INPUT_POST, 'ztTauxHoraire'));
    $AjoutContratExc->bindValue(':tarifKM', filter_input(INPUT_POST, 'ztTarifKM'));
    $AjoutContratExc->bindValue(':fraisTel', filter_input(INPUT_POST, 'zsTelephone'));
    $AjoutContratExc->bindValue(':veP', filter_input(INPUT_POST, 'zsVehiculePerso'));
    $AjoutContratExc->bindValue(':veF', filter_input(INPUT_POST, 'zsVehiculeFonction'));
    $AjoutContratExc->bindValue(':forfaitJ', filter_input(INPUT_POST, 'ztForfaitBrut'));
    $AjoutContratExc->bindValue(':forfaitN', filter_input(INPUT_POST, 'ztForfaitNet'));
    $AjoutContratExc->bindValue(':societe', $InfoMission->libelleClient);
    $AjoutContratExc->bindValue(':date', date('Y-m-d'));
    $AjoutContratExc->bindValue(':autoentre', filter_input(INPUT_POST, 'boolAuto'));
    $AjoutContratExc->bindValue(':boolFraisInclus', filter_input(INPUT_POST, 'boolFraisInclus'));

    /** -------------------------------------------------------------------------------------------
     * On force à 0 si on met TELEPHONE == NON
     */
    $fraisTel = 0;
    if (filter_input(INPUT_POST, 'zsTelephone') == 'OUI') {
        $fraisTel = filter_input(INPUT_POST, 'ztForfaitTel');
    }

    if ($AjoutContratExc->execute()) {
        $idContrat = DbConnexion::getInstance()->lastInsertId();

        foreach ($_POST['listeIntervention'] as $idIntervention) {
            $AjoutFraisInterventionExc->bindValue(':idIntervention', $idIntervention);
            $AjoutFraisInterventionExc->bindValue(':fraisRepas', filter_input(INPUT_POST, 'ztForfaitRepas'));
            $AjoutFraisInterventionExc->bindValue(':fraisTel', $fraisTel);
            $AjoutFraisInterventionExc->execute();

            $AffiliationInterventionContratExc->bindValue(':idIntervention', $idIntervention);
            $AffiliationInterventionContratExc->bindValue(':idContrat', $idContrat);
            $AffiliationInterventionContratExc->execute();
        }

        print json_encode(array(
            'idContrat' => $idContrat
        ));
    } else {
        print_r($AjoutContratExc->errorInfo());
        #die();
    }
}