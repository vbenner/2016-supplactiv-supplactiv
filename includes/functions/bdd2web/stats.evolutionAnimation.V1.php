<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des ANIMATIONS
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$currYear = date('Y');
$yearI = array();
$semestreI = array();
$trimestreI = array();
$monthI = array();
$noms = array();

/** -----------------------------------------------------------------------------------------------
 * On est obligé de mettre tous les noms des interlocuteurs qui sont connus pendant ces dernières
 * 6 années (5 + 1 pour calculer le taux avec la formule Y-1
 * Du coup, on prépare déjà le gros tableau final
 */
$sqlGetAllInterlocuteurs = '
SELECT DISTINCT SU.nomInterlocuteurAgence AS nom
FROM su_intervention SI
  INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
  INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
  INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
WHERE SI.dateDebut >= :du
AND SI.dateFin <= :au
AND SU.nomInterlocuteurAgence <> \'\'
AND NOT ISNULL(SI.FK_idContrat) 
AND SM.FK_idTypeMission = 3
ORDER BY nom ASC
';
$getAllInterlocuteursExec = DbConnexion::getInstance()->prepare($sqlGetAllInterlocuteurs);
$getAllInterlocuteursExec->bindValue(':du', ($currYear-5).'-01-01', PDO::PARAM_STR);
$getAllInterlocuteursExec->bindValue(':au', ($currYear).'-12-31', PDO::PARAM_STR);
$getAllInterlocuteursExec->execute();
while($getAllInterlocuteurs = $getAllInterlocuteursExec->fetch(PDO::FETCH_OBJ)) {

    // --------------------------------------------------------------------------------------------
    // On récupère juste pour simplifier la suite
    // --------------------------------------------------------------------------------------------
    $nom = $getAllInterlocuteurs->nom;

    // --------------------------------------------------------------------------------------------
    // On va se souvenir des noms
    // --------------------------------------------------------------------------------------------
    $noms[] = $nom;

    // --------------------------------------------------------------------------------------------
    // On gère les ANNEES (6 en tout)
    // --------------------------------------------------------------------------------------------
    $tab = array();
    for ($i = $currYear-5 ; $i <= $currYear ; $i++ ){
        $tab[$i]['year'] = $i;
        $tab[$i]['nb'] = 0;
        $tab[$i]['tx'] = '-';
    }
    $yearI[$nom] = array(
        'nom' => $nom,
        'years' => $tab
    );

    // --------------------------------------------------------------------------------------------
    // On gère les SEMESTRES
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 12....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1 ; $i <= 12 ; $i++ ) {
        $tab[ $i ][ 'semestre' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'tx' ] = '-';
    }
    $semestreI[$nom] = array(
        'nom' => $nom,
        'semestres' => $tab
    );

    // --------------------------------------------------------------------------------------------
    // On gère les TRIMESTRES
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 24....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1 ; $i <= 24 ; $i++ ) {
        $tab[ $i ][ 'trimestres' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'tx' ] = '-';
    }
    $trimestreI[$nom] = array(
        'nom' => $nom,
        'trimestres' => $tab
    );

    // --------------------------------------------------------------------------------------------
    // On gère les MOIS
    // On gère sur 36 mois mais on n'en gardera que 24....
    // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 24....
    // --------------------------------------------------------------------------------------------
    unset($tab);
    $tab = array();
    for ($i = 1 ; $i <= 36 ; $i++ ) {
        $tab[ $i ][ 'months' ] = $i;
        $tab[ $i ][ 'nb' ] = 0;
        $tab[ $i ][ 'tx' ] = '-';
    }
    $monthI[$nom] = array(
        'nom' => $nom,
        'months' => $tab
    );

}

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 */
for ($i = $currYear-5 ; $i <= $currYear ; $i++ ){

    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    AND SM.FK_idTypeMission = 3
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $i.'-01-01 00:00:00', PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $i.'-12-31 23:59:59', PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        $yearI[$count->nom]['years'][$i]['nb'] = $count->leCount;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais SEMESTRE / SEMESTRE
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 5;
for ($i = 1 ; $i <= 12 ; $i++ ) {

    //
    if ($i % 2 == 0) {
        $du = $year.'-07-01 00:00:00';
        $au = $year.'-12-31 23:59:59';
    } else {
        $du = $year.'-01-01 00:00:00';
        $au = $year.'-06-30 23:59:59';
    }


    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    AND SM.FK_idTypeMission = 3
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        $semestreI[$count->nom]['semestres'][$i]['nb'] = $count->leCount;
    }

    if ($i % 2 == 0) {
        $year++;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais TRIMESTRE / TRIMESTRE
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 5;
for ($i = 1 ; $i <= 24 ; $i++ ) {

    // Détermine le numéro du trimestre
    $num =(1+(($i-1)%4/3)*3);
    if ($num == 1) {
        $du = $year . '-01-01 00:00:00';
        $au = $year . '-03-31 23:59:59';
    } elseif ($num == 2) {
        $du = $year.'-04-01 00:00:00';
        $au = $year.'-06-30 23:59:59';
    } elseif ($num == 3) {
        $du = $year.'-07-01 00:00:00';
        $au = $year.'-09-31 23:59:59';
    } else {
        $du = $year.'-10-01 00:00:00';
        $au = $year.'-12-31 23:59:59';
    }


    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin <= :au
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    AND SM.FK_idTypeMission = 3
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        $trimestreI[$count->nom]['trimestres'][$i]['nb'] = $count->leCount;
    }

    if ($num == 4) {
        $year++;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais MOIS / MOIS
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 2;
for ($i = 1 ; $i <= 36 ; $i++ ) {

    // Détermine le numéro du mois
    $num =(1+(($i-1)%12/2)*2);
    if ($num <= 11) {
        $du = $year . '-' . str_pad($num, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
        $au = $year . '-' . str_pad($num+1, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
    } else {
        $du = $year . '-' . str_pad($num, 2, '0', STR_PAD_LEFT) . '-01 00:00:00';
        $au = $year+1 . '-01-01 00:00:00';
    }

    #echo '- - - - - - - - - - -'."\r\n";
    #echo $year."\r\n";
    #echo $du."\r\n";
    #echo $au."\r\n";
    $sqlCountInterventions = '
    SELECT COUNT(SI.idIntervention) AS leCount, SU.nomInterlocuteurAgence AS nom
    FROM su_intervention SI
      INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
      INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
      INNER JOIN su_agence_interlocuteur SU ON SC.FK_idInterlocuteurAgence = SU.idInterlocuteurAgence
    WHERE SI.dateDebut >= :du
    AND SI.dateFin < :au
    AND SU.nomInterlocuteurAgence <> \'\'
    AND NOT ISNULL(SI.FK_idContrat) 
    AND SM.FK_idTypeMission = 3
    GROUP BY nom
    ORDER BY nom ASC
    ';
    $countInterventionsExec = DbConnexion::getInstance()->prepare($sqlCountInterventions);
    $countInterventionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countInterventionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countInterventionsExec->execute();
    while ($count = $countInterventionsExec->fetch(PDO::FETCH_OBJ)) {
        $monthI[$count->nom]['months'][$i]['nb'] = $count->leCount;
    }

    if ($num == 12) {
        $year++;
    }
}

/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on fait les calculs de taux salarié / salarié
 * Mais on ne fera pas l'année $currYear - 5
 */
foreach ($noms as $nom) {
    for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
        if ($yearI[$nom]['years'][$i-1]['nb'] != 0) {
            $yearI[$nom]['years'][$i]['tx'] = (($yearI[$nom]['years'][$i]['nb'] - $yearI[$nom]['years'][$i-1]['nb']) / $yearI[$nom]['years'][$i-1]['nb']);
            $yearI[$nom]['years'][$i]['tx'] = round(100 * $yearI[$nom]['years'][$i]['tx'], 2 ).' %';
        }
        else {
            $yearI[$nom]['years'][$i-1]['tx'] = '-';
        }
    }
    unset($yearI[$nom]['years'][$currYear-5]);
}

/** -----------------------------------------------------------------------------------------------
 * Et on fait la même chose aussi pour les semestres
 */
foreach ($noms as $nom) {
    for ($i = 2 ; $i <= 12 ; $i++ ) {
        if ($semestreI[$nom]['semestres'][$i-1]['nb'] != 0) {
            $semestreI[$nom]['semestres'][$i]['tx'] = (($semestreI[$nom]['semestres'][$i]['nb'] - $semestreI[$nom]['semestres'][$i-1]['nb']) / $semestreI[$nom]['semestres'][$i-1]['nb']);
            $semestreI[$nom]['semestres'][$i]['tx'] = round(100 * $semestreI[$nom]['semestres'][$i]['tx'], 2 ).' %';
        }
        else {
            $semestreI[$nom]['semestres'][$i-1]['nb']['tx'] = '-';
        }
    }
    unset($semestreI[$nom]['semestres'][1]);
    unset($semestreI[$nom]['semestres'][2]);
}

/** -----------------------------------------------------------------------------------------------
 * Et on fait la même chose aussi pour les trimestres
 */
foreach ($noms as $nom) {
    for ($i = 2 ; $i <= 24 ; $i++ ) {
        if ($trimestreI[$nom]['trimestres'][$i-1]['nb'] != 0) {
            $trimestreI[$nom]['trimestres'][$i]['tx'] = (($trimestreI[$nom]['trimestres'][$i]['nb'] - $trimestreI[$nom]['trimestres'][$i-1]['nb']) / $trimestreI[$nom]['trimestres'][$i-1]['nb']);
            $trimestreI[$nom]['trimestres'][$i]['tx'] = round(100 * $trimestreI[$nom]['trimestres'][$i]['tx'], 2 ).' %';
        }
        else {
            $trimestreI[$nom]['trimestres'][$i-1]['tx'] = '-';
        }
    }
    unset($trimestreI[$nom]['trimestres'][1]);
    unset($trimestreI[$nom]['trimestres'][2]);
    unset($trimestreI[$nom]['trimestres'][3]);
    unset($trimestreI[$nom]['trimestres'][4]);
}

/** -----------------------------------------------------------------------------------------------
 * Et on fait la même chose aussi pour les mois
 */
foreach ($noms as $nom) {
    for ($i = 2 ; $i <= 36 ; $i++ ) {
        if ($monthI[$nom]['months'][$i-1]['nb'] != 0) {
            $monthI[$nom]['months'][$i]['tx'] = (($monthI[$nom]['months'][$i]['nb'] - $monthI[$nom]['months'][$i-1]['nb']) / $monthI[$nom]['months'][$i-1]['nb']);
            $monthI[$nom]['months'][$i]['tx'] = round(100 * $monthI[$nom]['months'][$i]['tx'], 2 ).' %';
        }
        else {
            $monthI[$nom]['months'][$i-1]['tx'] = '-';
        }
    }
    for ($i = 1; $i <= 12 ; $i++) {
        unset($monthI[$nom]['months'][$i]);
    }
    #unset($monthI[$nom]['months'][1]);
    #unset($trimestreI[$nom]['trimestres'][2]);
    #unset($trimestreI[$nom]['trimestres'][3]);
    #unset($trimestreI[$nom]['trimestres'][4]);
}

/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'DATAY' => $yearI,
        'DATAS' => $semestreI,
        'DATAT' => $trimestreI,
        'DATAM' => $monthI,
    )
);