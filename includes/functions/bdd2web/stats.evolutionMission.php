<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des MISSIONS
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$currYear = date('Y');
$yearM = array();
$semestreM = array();
$noms = array();
$dissociate = $_POST[ 'dissociate' ] == 1 ? true : false;

/** -----------------------------------------------------------------------------------------------
 * On est obligé de mettre tous les noms des interlocuteurs qui sont connus pendant ces dernières
 * 6 années (5 + 1 pour calculer le taux avec la formule Y-1
 * Du coup, on prépare déjà le gros tableau final
 */
$sqlGetAllInterlocuteurs = '
SELECT DISTINCT SU.nomUtilisateur AS nom,
    SU1.nomInterlocuteurAgence AS nom1
FROM su_mission SM
  INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
  LEFT JOIN du_utilisateur SU ON SU.idUtilisateur = SM.decideur_id
  LEFT JOIN su_agence_interlocuteur SU1 ON SU1.idInterlocuteurAgence = SM.decideur_id
WHERE dateDebut >= :du
AND dateFin <= :au
ORDER BY nom, nom1 ASC
';

$getAllInterlocuteursExec = DbConnexion::getInstance()->prepare($sqlGetAllInterlocuteurs);
$getAllInterlocuteursExec->bindValue(':du', ($currYear - 5) . '-01-01', PDO::PARAM_STR);
$getAllInterlocuteursExec->bindValue(':au', ($currYear) . '-12-31', PDO::PARAM_STR);
$getAllInterlocuteursExec->execute();
while ($getAllInterlocuteurs = $getAllInterlocuteursExec->fetch(PDO::FETCH_OBJ)) {

    // --------------------------------------------------------------------------------------------
    // On récupère juste pour simplifier la suite
    // Si le nom n'est pas dans la table des UTILISATEURS, on va le cercher dans la table

    // --------------------------------------------------------------------------------------------
    $nom = $getAllInterlocuteurs->nom;
    $nom1 = $getAllInterlocuteurs->nom1;

    if (!($nom == '' && $nom1 == '')) {

        // ----------------------------------------------------------------------------------------
        // On va se souvenir des noms
        // ----------------------------------------------------------------------------------------
        $tmp = ($nom != '' ? $nom : $nom1);
        $noms[] = $tmp;

        // ----------------------------------------------------------------------------------------
        // On gère les ANNEES (6 en tout)
        // ----------------------------------------------------------------------------------------
        $tab = array();
        for ($i = $currYear - 5; $i <= $currYear; $i++) {
            $tab[ $i ][ 'year' ] = $i;
            $tab[ $i ][ 'nb' ] = 0;
            $tab[ $i ][ 'tx' ] = '-';
        }
        if ($dissociate == 1) {
            $yearM[$tmp] = array(
                'nom' => $tmp,
                'years' => $tab
            );
        } else {
            $yearM['_MISSION'] = array(
                'nom' => $tmp,
                'years' => $tab
            );
        }

        // ----------------------------------------------------------------------------------------
        // On gère les ANNEES (6 en tout)
        // On se moque un peu de la numérotation, du moment qu'on compte bien de 1 à 12....
        // ----------------------------------------------------------------------------------------
        unset($tab);
        $tab = array();
        for ($i = 1 ; $i <= 12 ; $i++ ) {
            $tab[ $i ][ 'semestre' ] = $i;
            $tab[ $i ][ 'nb' ] = 0;
            $tab[ $i ][ 'txy' ] = '-';
        }
        if ($dissociate == 1) {
            $semestreM[$tmp] = array(
                'nom' => $tmp,
                'semestres' => $tab
            );
        } else {
            $semestreM['_MISSION'] = array(
                'nom' => $tmp,
                'semestres' => $tab
            );
        }
    }
    else {
    }
}


/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 */
for ($i = $currYear - 5; $i <= $currYear; $i++) {

    $sqlCountMissions = '
    SELECT
        COUNT(SM.idMission) AS leCount,
        SU.nomUtilisateur AS nom,
        SU1.nomInterlocuteurAgence AS nom1
    FROM
        su_mission SM
    INNER JOIN su_campagne SC ON
        SC.idCampagne = SM.FK_idCampagne
    LEFT JOIN du_utilisateur SU ON
        SU.idUtilisateur = SM.decideur_id
    LEFT JOIN su_agence_interlocuteur SU1 ON
        SU1.idInterlocuteurAgence = SM.decideur_id
    WHERE dateDebut >= :du
    AND dateFin <= :au
    GROUP BY nom, nom1
    ORDER BY nom, nom1
    ';
    $countMissionsExec = DbConnexion::getInstance()->prepare($sqlCountMissions);
    $countMissionsExec->bindValue(':du', $i . '-01-01', PDO::PARAM_STR);
    $countMissionsExec->bindValue(':au', $i . '-12-31', PDO::PARAM_STR);
    $countMissionsExec->execute();
    while ($count = $countMissionsExec->fetch(PDO::FETCH_OBJ)) {

        if (!($count->nom == '' && $count->nom1 == '')) {
            if ($dissociate == 1) {
                $tmp = ($count->nom != '' ? $count->nom : $count->nom1);
                $yearM[ $tmp ][ 'years' ][ $i ][ 'nb' ] = $count->leCount;
            }
            else {
                $yearM[ '_MISSION' ][ 'years' ][ $i ][ 'nb' ] += $count->leCount;
            }
        }
    }
}



/** -----------------------------------------------------------------------------------------------
 * On procède à la même analyse mais SEMESTRE / SEMESTRE
 * Avec une vieille formule mathématique des familles
 */
$year = $currYear - 5;
for ($i = 1 ; $i <= 12 ; $i++ ) {

    //
    if ($i % 2 == 0) {
        $du = $year.'-07-01 00:00:00';
        $au = $year.'-12-31 23:59:59';
    } else {
        $du = $year.'-01-01 00:00:00';
        $au = $year.'-06-30 23:59:59';
    }

    $sqlCountMissions = '
    SELECT
        COUNT(SM.idMission) AS leCount,
        SU.nomUtilisateur AS nom,
        SU1.nomInterlocuteurAgence AS nom1
    FROM
        su_mission SM
    INNER JOIN su_campagne SC ON
        SC.idCampagne = SM.FK_idCampagne
    LEFT JOIN du_utilisateur SU ON
        SU.idUtilisateur = SM.decideur_id
    LEFT JOIN su_agence_interlocuteur SU1 ON
        SU1.idInterlocuteurAgence = SM.decideur_id
    WHERE dateDebut >= :du
    AND dateFin <= :au
    GROUP BY nom, nom1
    ORDER BY nom, nom1
    ';
    $countMissionsExec = DbConnexion::getInstance()->prepare($sqlCountMissions);
    $countMissionsExec->bindValue(':du', $du, PDO::PARAM_STR);
    $countMissionsExec->bindValue(':au', $au, PDO::PARAM_STR);
    $countMissionsExec->execute();

    #echo $sqlCountMissions.'<br/>';
    #echo $du.'<br/>';
    #echo $au.'<br/>';
    #die();
    while ($count = $countMissionsExec->fetch(PDO::FETCH_OBJ)) {

        if (!($count->nom == '' && $count->nom1 == '')) {
            $tmp = $count->nom != '' ? $count->nom : $count->nom1;
            if ($dissociate == 1) {
                $semestreM[ $tmp ][ 'semestres' ][ $i ][ 'nb' ] = $count->leCount;
            }
            else {
                $semestreM[ '_MISSION' ][ 'semestres' ][ $i ][ 'nb' ] += $count->leCount;
            }
        }
    }

    if ($i % 2 == 0) {
        $year++;
    }
}



/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on fait les calculs de taux salarié / salarié
 * Mais on ne fera pas l'année $currYear - 5
 */

if ($dissociate) {
    foreach ($noms as $nom) {
        $hasData = 0;
        for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
            if ($yearM[$nom]['years'][$i-1]['nb'] != 0) {
                $yearM[$nom]['years'][$i]['txy'] = (($yearM[$nom]['years'][$i]['nb'] - $yearM[$nom]['years'][$i-1]['nb']) / $yearM[$nom]['years'][$i-1]['nb']);
                $yearM[$nom]['years'][$i]['txy'] = round(100 * $yearM[$nom]['years'][$i]['txy'], 2 ).' %';
            }
            else {
                $yearM[$nom]['years'][$i]['txy'] = '-';
            }
        }
        unset($yearM[ $nom ][ 'years' ][ $currYear - 5 ]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
            $hasData += $yearM[$nom]['years'][$i]['nb'];
        }
        if ($hasData == 0) {
            unset($yearM[$nom]);
        }

    }

    /** -------------------------------------------------------------------------------------------
     * Semestre / Semestre
     * Attention, on va faire le calcul avec un décalage de 2 !
     */
    foreach ($noms as $nom) {
        $hasData = 0;
        for ($i = 3 ; $i <= 12 ; $i++ ) {
            $hasData += $semestreM[$nom]['semestres'][$i]['nb'];
            if ($semestreM[$nom]['semestres'][$i-2]['nb'] != 0) {
                $semestreM[$nom]['semestres'][$i]['txy'] = (($semestreM[$nom]['semestres'][$i]['nb'] - $semestreM[$nom]['semestres'][$i-2]['nb']) / $semestreM[$nom]['semestres'][$i-2]['nb']);
                $semestreM[$nom]['semestres'][$i]['txy'] = round(100 * $semestreM[$nom]['semestres'][$i]['txy'], 2 ).' %';
            }
            else {
                $semestreM[$nom]['semestres'][$i]['txy'] = '-';
            }
        }
        unset($semestreM[$nom]['semestres'][1]);
        unset($semestreM[$nom]['semestres'][2]);

        /** ---------------------------------------------------------------------------------------
         * Si le CP n'a pas de données sur les 5 dernières années, on le supprime
         * besoin que de 1 à 5
         */
        for ($i = 3 ; $i <= 12 ; $i++ ) {
            $hasData += $semestreM[$nom]['semestres'][$i]['nb'];
        }
        if ($hasData == 0) {
            unset($yearM[$nom]);
        }

    }

}
else {
    $yearM[ '_MISSION' ][ 'nom' ] = '-';
    for ($i = $currYear-4 ; $i <= $currYear ; $i++ ) {
        if ($yearM['_MISSION']['years'][$i-1]['nb'] != 0) {
            $yearM['_MISSION']['years'][$i]['txy'] = (($yearM['_MISSION']['years'][$i]['nb'] - $yearM['_MISSION']['years'][$i-1]['nb']) / $yearM['_MISSION']['years'][$i-1]['nb']);
            $yearM['_MISSION']['years'][$i]['txy'] = round(100 * $yearM['_MISSION']['years'][$i]['txy'], 2 ).' %';
        }
        else {
            $yearM['_MISSION']['years'][$i]['txy'] = '-';
        }
    }
    unset($yearM[ '_MISSION' ][ 'years' ][ $currYear - 5 ]);

    /** -------------------------------------------------------------------------------------------
     * Semestres
     */
    $semestreM[ '_MISSION' ][ 'nom' ] = '-';
    for ($i = 3 ; $i <= 12 ; $i++ ) {
        if ($semestreM['_MISSION']['semestres'][$i-2]['nb'] != 0) {
            $semestreM['_MISSION']['semestres'][$i]['txy'] = (($semestreM['_MISSION']['semestres'][$i]['nb'] - $semestreM['_MISSION']['semestres'][$i-2]['nb']) / $semestreM['_MISSION']['semestres'][$i-2]['nb']);
            $semestreM['_MISSION']['semestres'][$i]['txy'] = round(100 * $semestreM['_MISSION']['semestres'][$i]['txy'], 2 ).' %';
        }
        else {
            $semestreM['_MISSION']['semestres'][$i]['txy'] = '-';
        }
    }
    unset($semestreM['_MISSION']['semestres'][1]);
    unset($semestreM['_MISSION']['semestres'][2]);

}

/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'DATAY' => $yearM,
        'DATAS' => $semestreM,
        //'DATAT' => $trimestre,
        //'DATAS' => $semestre,
    )
);