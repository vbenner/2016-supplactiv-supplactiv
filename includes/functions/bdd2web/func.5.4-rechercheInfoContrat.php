<?php
/**
 * Recherche des informations sur un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

if(filter_has_var(INPUT_POST, 'idContrat')){
    $RechercheInfoContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $RechercheInfoContratExc->execute();


    $listeTypePdv = array(); $dureeTotal = array();
    $RechercheInterventionContratExc->bindValue(':idContrat', filter_input(INPUT_POST, 'idContrat'));
    $RechercheInterventionContratExc->execute();
    while($InfoIntervention = $RechercheInterventionContratExc->fetch(PDO::FETCH_OBJ)){
        $listeTypePdv[$InfoIntervention->libelleCategoriePdv]++;

        $DT = new DateTime($InfoIntervention->dateDebut);
        @$dureeTotal[$DT->format('Y-m-d')] += $InfoIntervention->dureeSeconde;

    }

    $dureeTotalFinal = 0;
    foreach ($dureeTotal as $date => $nbSeconde) {
        $dureeTotalFinal += ($nbSeconde >= 28800) ? $nbSeconde-3600 : $nbSeconde;
    }

    $RechercheInterventionContratExc->execute();

    print json_encode(array(
        'contrat' => $RechercheInfoContratExc->fetch(PDO::FETCH_OBJ),
        'interventions' => $RechercheInterventionContratExc->fetchAll(PDO::FETCH_OBJ),
        'typePdv' => $listeTypePdv,
        'duree' => formatSecToMin($dureeTotalFinal)
    ));
}