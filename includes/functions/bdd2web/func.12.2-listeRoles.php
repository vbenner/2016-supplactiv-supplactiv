<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des rôles utilsateurs
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

/** -----------------------------------------------------------------------------------------------
 * Récupération des rôles
 */
$roles = array();
$sqlGetListeRoles = '
SELECT *
FROM du_utilisateur_type
WHERE idTypeUtilisateur != 1 
ORDER BY idTypeUtilisateur
';
$getListeRolesExec = DbConnexion::getInstance()->prepare($sqlGetListeRoles);
$getListeRolesExec->execute();
while ($getListeRoles = $getListeRolesExec->fetch(PDO::FETCH_OBJ)) {
    $roles[] = array(
        'id' => $getListeRoles->idTypeUtilisateur,
        'lib' => $getListeRoles->libelleTypeUtilisateur
    );
}
$arOutput['roles'] = $roles;

/** -----------------------------------------------------------------------------------------------
 * Retour
 */
print json_encode($arOutput);
