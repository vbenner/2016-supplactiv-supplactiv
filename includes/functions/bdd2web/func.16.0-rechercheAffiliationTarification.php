<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des affiliations de tarification
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$listeAffiliation = array(
    'intervenants' => array(),
    'campagnes' => array()
);

/** Recherche des affiliations */
$RechercheAffiliationTarificationCampagneExc->execute();
while($InfoAffiliation = $RechercheAffiliationTarificationCampagneExc->fetch(PDO::FETCH_OBJ)){
    $listeAffiliation['intervenants'][$InfoAffiliation->idIntervenant] = addCaracToString($InfoAffiliation->idIntervenant, 5, '0').' - '.$InfoAffiliation->nomIntervenant.' '.$InfoAffiliation->prenomIntervenant;
    $listeAffiliation['campagnes'][$InfoAffiliation->idCampagne] = $InfoAffiliation->libelleCampagne;
}

print json_encode($listeAffiliation);