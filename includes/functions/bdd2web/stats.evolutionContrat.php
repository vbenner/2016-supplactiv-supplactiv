<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des évolutions des CA
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Filtre facultatif
 */
$currYear = date('Y');
$idCampagne = 0;
if ($_POST[ 'campagne' ] != '') {
    $filtreCampagne = ' AND idCampagne = :idc ';
    $idCampagne = $_POST[ 'campagne' ];

    /** -------------------------------------------------------------------------------------------
     * On récupère l'année
     */
    $sqlGetCampagne = '
    SELECT *
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getCampagneExec = DbConnexion::getInstance()->prepare($sqlGetCampagne);
    $getCampagneExec->bindValue(':idc', $idCampagne, PDO::PARAM_INT);
    $getCampagneExec->execute();
    $getCampagne = $getCampagneExec->fetch(PDO::FETCH_OBJ);
    $currYear = substr($getCampagne->dateFin, 0, 4);

}
else {
    $filtreCampagne = '';
}

/** -----------------------------------------------------------------------------------------------
 * Init - Tous les tableau VS tableau précédent
 * On
 */
$resultY = array();
$resultS = array();
$resultT = array();
$resultM = array();
$noms = array();

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des années avec tout ce qui va bien dedans
 * txi = taux vs taux (i - 1)
 * txy = taux vs taux (y - 1)
 */
for ($i = $currYear - 5; $i <= $currYear; $i++) {
    $resultY[ $i ] = array(
        'nb' => 0,
        'txy' => '-',
        'nb_a' => 0,
        'txy_a' => '-',
        'nb_f' => 0,
        'txy_f' => '-',
    );
    /** -------------------------------------------------------------------------------------------
     * SEMESTRES
     */
    for ($j = 1; $j <= 2; $j++) {
        $resultS[ $i ][ $j ] = array(
            'nb' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'txi_a' => '-',
            'txy_a' => '-',
            'nb_f' => 0,
            'txi_f' => '-',
            'txy_f' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * TRIMESTRES
     */
    for ($j = 1; $j <= 4; $j++) {
        $resultT[ $i ][ $j ] = array(
            'nb' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'txi_a' => '-',
            'txy_a' => '-',
            'nb_f' => 0,
            'txi_f' => '-',
            'txy_f' => '-',
        );
    }
    /** -------------------------------------------------------------------------------------------
     * MOIS
     */
    for ($j = 1; $j <= 12; $j++) {
        $resultM[ $i ][ $j ] = array(
            'nb' => 0,
            'txy' => '-',
            'nb_a' => 0,
            'txi_a' => '-',
            'txy_a' => '-',
            'nb_f' => 0,
            'txi_f' => '-',
            'txy_f' => '-',
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * On pourra faire 2 fois les actions par rapport
 */
$inter = array(
    array('id' => 3, 'suffix' => 'a'),
    array('id' => 2, 'suffix' => 'f'),
);

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des jours de interventions pour une campagne donnée regroupée par années
 * On fait déjà pour les animations
 *
 * Si on est en mode SANS filtre CAMPAGNE, c'est assez facile, on n'a pas de restriction
 */
if ($filtreCampagne == '') {

    /** -------------------------------------------------------------------------------------------
     * EXTRACTION GLOBALE
     * Par MOIS
     */
    foreach ($inter as $key => $val) {

        // ----------------------------------------------------------------------------------------
        // La partie évolutive
        // ----------------------------------------------------------------------------------------
        $type = $val[ 'id' ];
        $suffix = $val[ 'suffix' ];

        // ----------------------------------------------------------------------------------------
        // On construit la requête (attention, PDO n'autorise pas le bind d'un champ
        // ----------------------------------------------------------------------------------------
        $sqlGetAllDays = "
        SELECT YEAR(SCO.dateCreationContrat) AS lannee, MONTH(SCO.dateCreationContrat) as lemois, 
		  COUNT(DISTINCT SCO.idContrat) AS lecount
        FROM su_contrat SCO
          INNER JOIN su_intervention SI ON SCO.idContrat = SI.FK_idContrat
          INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
          INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
        WHERE SCO.dateCreationContrat >= :du
        AND SCO.dateCreationContrat <= :au
        AND SM.FK_idTypeMission = :type
		GROUP BY lannee, lemois
        ORDER BY lannee, lemois
        ";

        $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
        $getAllDaysExec->bindValue(':du', ($currYear - 5) . '-01-01', PDO::PARAM_STR);
        $getAllDaysExec->bindValue(':au', ($currYear) . '-12-31', PDO::PARAM_STR);
        $getAllDaysExec->bindValue(':type', $type, PDO::PARAM_INT);
        $getAllDaysExec->execute();

        /** ---------------------------------------------------------------------------------------
         * Traitement
         */
        while ($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

            // ----------------------------------------------------------------------------------------
            // Partie commune
            // ----------------------------------------------------------------------------------------
            $year = $getAllDays->lannee;

            // ----------------------------------------------------------------------------------------
            // La partie pour les mois
            // ----------------------------------------------------------------------------------------
            $mois = $getAllDays->lemois;
            $resultM[ $year ][ $mois ][ 'nb_' . $suffix ] += $getAllDays->lecount;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / trimestre
            // ----------------------------------------------------------------------------------------
            $trimestre = monthToTrimestre($getAllDays->lemois);
            $resultT[ $year ][ $trimestre ][ 'nb_' . $suffix ] += $getAllDays->lecount;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / semestre
            // ----------------------------------------------------------------------------------------
            $semestre = monthToSemestre($getAllDays->lemois);
            $resultS[ $year ][ $semestre ][ 'nb_' . $suffix ] += $getAllDays->lecount;

            // ----------------------------------------------------------------------------------------
            // Ensuite, on trie / year
            // ----------------------------------------------------------------------------------------
            $resultY[ $year ][ 'nb_' . $suffix ] += $getAllDays->lecount;

            // ----------------------------------------------------------------------------------------
            // Cumul de A + F
            // ----------------------------------------------------------------------------------------
            $resultY[ $year ][ 'nb' ] += $getAllDays->lecount;

        }
    }
}
else {

    /** -------------------------------------------------------------------------------------------
     * Pour le cas où la campagne est identifiée, on doit faire l'action pour chaque année et donc
     * reprendre la campagne précédente.
     */
    $campagnes[ $currYear ] = $_POST[ 'campagne' ];
    for ($i = $currYear - 1; $i >= $currYear - 4; $i--) {
        $campagnes[ $i ] = getCampagneIDPrec($campagnes[ $i + 1 ]);
    }

    #echo '<pre>';
    #print_r($campagnes);
    #echo '</pre>';

    /** -------------------------------------------------------------------------------------------
     * On fait donc maintenant l'analyse ID / ID (sans tenir compte des dates
     *
     * Traitement par ANNEE
     */
    foreach ($campagnes as $year => $idc) {

        foreach ($inter as $key => $val) {

            // ------------------------------------------------------------------------------------
            // La partie évolutive
            // ------------------------------------------------------------------------------------
            $type = $val[ 'id' ];
            $suffix = $val[ 'suffix' ];


            /** -----------------------------------------------------------------------------------
             * On traite les ANIMATIONS
             * TRAITEMENT MENSUEL
             */
            $sqlGetAllDays = "
            SELECT YEAR(SI.dateDebut) AS lannee, MONTH(SI.dateDebut) as lemois, 
		      COUNT(DISTINCT SCO.idContrat) AS lecount
            FROM su_contrat SCO
              INNER JOIN su_intervention SI ON SCO.idContrat = SI.FK_idContrat
              INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
              INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
            WHERE SM.FK_idTypeMission = :type
            AND SC.idCampagne = :idc
		    GROUP BY lannee, lemois
            ORDER BY lannee, lemois
            ";

            $getAllDaysExec = DbConnexion::getInstance()->prepare($sqlGetAllDays);
            $getAllDaysExec->bindValue(':idc', $idc, PDO::PARAM_STR);
            $getAllDaysExec->bindValue(':type', $type, PDO::PARAM_INT);
            $getAllDaysExec->execute();

            /** -----------------------------------------------------------------------------------
             * Traitement
             */
            while ($getAllDays = $getAllDaysExec->fetch(PDO::FETCH_OBJ)) {

                // --------------------------------------------------------------------------------
                // Partie commune
                // --------------------------------------------------------------------------------
                $year = $getAllDays->lannee;

                // --------------------------------------------------------------------------------
                // La partie pour les mois
                // --------------------------------------------------------------------------------
                $mois = $getAllDays->lemois;
                $resultM[ $year ][ $mois ][ 'nb_' . $suffix ] += $getAllDays->lecount;
                $resultM[ $year ][ $mois ][ 'nb' ] += $getAllDays->lecount;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / trimestre
                // --------------------------------------------------------------------------------
                $trimestre = monthToTrimestre($getAllDays->lemois);
                $resultT[ $year ][ $trimestre ][ 'nb_' . $suffix ] += $getAllDays->lecount;
                $resultT[ $year ][ $trimestre ][ 'nb' ] += $getAllDays->lecount;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / semestre
                // --------------------------------------------------------------------------------
                $semestre = monthToSemestre($getAllDays->lemois);
                $resultS[ $year ][ $semestre ][ 'nb_' . $suffix ] += $getAllDays->lecount;
                $resultS[ $year ][ $semestre ][ 'nb' ] += $getAllDays->lecount;

                // --------------------------------------------------------------------------------
                // Ensuite, on trie / year
                // --------------------------------------------------------------------------------
                $resultY[ $year ][ 'nb_' . $suffix ] += $getAllDays->lecount;

                // --------------------------------------------------------------------------------
                // Cumul de A + F
                // --------------------------------------------------------------------------------
                $resultY[ $year ][ 'nb' ] += $getAllDays->lecount;

            }
        }
    }
}

#echo '<pre>';
#print_r($resultY);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * On vient de construire le tableau des années.
 * On va calculer maintenant les différents TAUX
 */

/** -----------------------------------------------------------------------------------------------
 * Maintenant, on fait l'analyse ANNEE / ANNEE
 * Sans tenir compte de la première année
 */
for ($i = $currYear - 4; $i <= $currYear; $i++) {

    /** -------------------------------------------------------------------------------------------
     * Evolution globale
     */
    if ($resultY[ $i - 1 ][ 'nb' ] != 0) {
        $resultY[ $i ][ 'txy' ] = (($resultY[ $i ][ 'nb' ] - $resultY[ $i - 1 ][ 'nb' ]) / $resultY[ $i - 1 ][ 'nb' ]);
        $resultY[ $i ][ 'txy' ] = round(100 * $resultY[ $i ][ 'txy' ], 2) . ' %';
    }
    else {
        $resultY[ $i ][ 'txy' ] = '-';
    }

    /** -------------------------------------------------------------------------------------------
     * Partie facile : on traite les ANNEES
     */
    foreach ($inter as $key => $val) {

        // ----------------------------------------------------------------------------------------
        // La partie évolutive
        // ----------------------------------------------------------------------------------------
        $suffix = $val[ 'suffix' ];

        // ----------------------------------------------------------------------------------------
        // On traite les ANNEES d'abord
        // Traitement des années précédente
        // ----------------------------------------------------------------------------------------
        if ($resultY[ $i - 1 ][ 'nb_' . $suffix ] != 0) {

            $resultY[ $i ][ 'txy_' . $suffix ] = (($resultY[ $i ][ 'nb_' . $suffix ] - $resultY[ $i - 1 ][ 'nb_' . $suffix ]) / $resultY[ $i - 1 ][ 'nb_' . $suffix ]);
            $resultY[ $i ][ 'txy_' . $suffix ] = round(100 * $resultY[ $i ][ 'txy_' . $suffix ], 2) . ' %';

            $resultY[ $i ][ 'txi_' . $suffix ] = (($resultY[ $i ][ 'nb_' . $suffix ] - $resultY[ $i - 1 ][ 'nb_' . $suffix ]) / $resultY[ $i - 1 ][ 'nb_' . $suffix ]);
            $resultY[ $i ][ 'txi_' . $suffix ] = round(100 * $resultY[ $i ][ 'txi_' . $suffix ], 2) . ' %';
        }
        else {
            $resultY[ $i ][ 'txy_' . $suffix ] = '-';
            $resultY[ $i ][ 'txi_' . $suffix ] = '-';
        }

        // ----------------------------------------------------------------------------------------
        // On traite ensuite les SEMESTRES
        // ----------------------------------------------------------------------------------------
        for ($semestre = 1; $semestre <= 2; $semestre++) {

            /** -----------------------------------------------------------------------------------
             * On compare donc le trimestre avec le trimestre de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultS[ $i - 1 ][ $semestre ][ 'nb' ] != 0) {
                $ca = $resultS[ $i ][ $semestre ][ 'nb' ];
                $caPrec = $resultS[ $i - 1 ][ $semestre ][ 'nb' ];
                $resultS[ $i ][ $semestre ][ 'txy' ] = (($ca - $caPrec) / $caPrec);
                $resultS[ $i ][ $semestre ][ 'txy' ] = round(100 * $resultS[ $i ][ $semestre ][ 'txy' ], 2) . ' %';
            }
            else {
                $resultS[ $i ][ $semestre ][ 'txy' ] = '-';
            }

            if ($resultS[ $i - 1 ][ $semestre ][ 'nb_' . $suffix ] != 0) {
                $ca = $resultS[ $i ][ $semestre ][ 'nb_' . $suffix ];
                $caPrec = $resultS[ $i - 1 ][ $semestre ][ 'nb_' . $suffix ];
                $resultS[ $i ][ $semestre ][ 'txy_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultS[ $i ][ $semestre ][ 'txy_' . $suffix ] = round(100 * $resultS[ $i ][ $semestre ][ 'txy_' . $suffix ], 2) . ' %';
            }
            else {
                $resultS[ $i ][ $semestre ][ 'txy_' . $suffix ] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le semestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018-02 => 2018-01
             * 2018-01 => 2017-02
             */
            $semestrePrec = ($semestre == 1 ? 2 : 1);
            $yearPrec = ($semestre == 1 ? $i - 1 : $i);
            if ($resultS[ $yearPrec ][ $semestrePrec ][ 'nb_' . $suffix ] != 0) {

                $ca = $resultS[ $i ][ $semestre ][ 'nb_' . $suffix ];
                $caPrec = $resultS[ $yearPrec ][ $semestrePrec ][ 'nb_' . $suffix ];

                $resultS[ $i ][ $semestre ][ 'txi_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultS[ $i ][ $semestre ][ 'txi_' . $suffix ] = round(100 * $resultS[ $i ][ $semestre ][ 'txi_' . $suffix ], 2) . ' %';
            }
            else {
                $resultS[ $i ][ $semestre ][ 'txi_' . $suffix ] = '-';
            }
        }


        // ----------------------------------------------------------------------------------------
        // On traite ensuite les TRIMESTRES
        // ----------------------------------------------------------------------------------------
        for ($trimestre = 1; $trimestre <= 4; $trimestre++) {

            /** -----------------------------------------------------------------------------------
             * On compare donc le trimestre avec le trimestre de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultT[ $i - 1 ][ $trimestre ][ 'nb' ] != 0) {
                $ca = $resultT[ $i ][ $trimestre ][ 'nb' ];
                $caPrec = $resultT[ $i - 1 ][ $trimestre ][ 'nb' ];
                $resultT[ $i ][ $trimestre ][ 'txy' ] = (($ca - $caPrec) / $caPrec);
                $resultT[ $i ][ $trimestre ][ 'txy' ] = round(100 * $resultT[ $i ][ $trimestre ][ 'txy' ], 2) . ' %';
            }
            else {
                $resultT[ $i ][ $trimestre ][ 'txy' ] = '-';
            }

            if ($resultT[ $i - 1 ][ $trimestre ][ 'ca_' . $suffix ] != 0) {
                $ca = $resultT[ $i ][ $trimestre ][ 'nb_' . $suffix ];
                $caPrec = $resultT[ $i - 1 ][ $trimestre ][ 'nb_' . $suffix ];
                $resultT[ $i ][ $trimestre ][ 'txy_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultT[ $i ][ $trimestre ][ 'txy_' . $suffix ] = round(100 * $resultT[ $i ][ $trimestre ][ 'txy_' . $suffix ], 2) . ' %';
            }
            else {
                $resultT[ $i ][ $trimestre ][ 'txy_' . $suffix ] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le trimestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018/4 => 2018/3
             * 2018/3 => 2018/2
             * 2018/2 => 2018/1
             * 2018/1 => 2017/4
             */
            $trimestrePrec = ($trimestre == 1 ? 4 : $trimestre - 1);
            $yearPrec = ($trimestre == 1 ? $i - 1 : $i);
            if ($resultT[ $yearPrec ][ $trimestrePrec ][ 'nb_' . $suffix ] != 0) {

                $ca = $resultT[ $i ][ $trimestre ][ 'nb_' . $suffix ];
                $caPrec = $resultT[ $yearPrec ][ $trimestrePrec ][ 'nb_' . $suffix ];

                $resultT[ $i ][ $trimestre ][ 'txi_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultT[ $i ][ $trimestre ][ 'txi_' . $suffix ] = round(100 * $resultT[ $i ][ $trimestre ][ 'txi_' . $suffix ], 2) . ' %';
            }
            else {
                $resultT[ $i ][ $trimestre ][ 'txi_' . $suffix ] = '-';
            }
        }


        // ----------------------------------------------------------------------------------------
        // On traite ensuite les MOIS
        // ----------------------------------------------------------------------------------------
        for ($mois = 1; $mois <= 12; $mois++) {

            /** -----------------------------------------------------------------------------------
             * On compare donc le mois avec le mois de l'année d'avant
             * on fait (Y - (Y-1)) / (Y-1)
             */
            if ($resultM[ $i - 1 ][ $mois ][ 'nb' ] != 0) {
                $ca = $resultM[ $i ][ $mois ][ 'nb' ];
                $caPrec = $resultM[ $i - 1 ][ $mois ][ 'nb' ];
                $resultM[ $i ][ $mois ][ 'txy' ] = (($ca - $caPrec) / $caPrec);
                $resultM[ $i ][ $mois ][ 'txy' ] = round(100 * $resultM[ $i ][ $mois ][ 'txy' ], 2) . ' %';
            }
            else {
                $resultM[ $i ][ $mois ][ 'txy' ] = '-';
            }


            if ($resultM[ $i - 1 ][ $mois ][ 'nb_' . $suffix ] != 0) {
                $ca = $resultM[ $i ][ $mois ][ 'nb_' . $suffix ];
                $caPrec = $resultM[ $i - 1 ][ $mois ][ 'nb_' . $suffix ];
                $resultM[ $i ][ $mois ][ 'txy_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultM[ $i ][ $mois ][ 'txy_' . $suffix ] = round(100 * $resultM[ $i ][ $mois ][ 'txy_' . $suffix ], 2) . ' %';
            }
            else {
                $resultM[ $i ][ $mois ][ 'txy_' . $suffix ] = '-';
            }

            /** -----------------------------------------------------------------------------------
             * Maintenant on compare avec le trimestre précédent (i-1)
             * Ce qui est plus compliqué
             * 2018/4 => 2018/3
             * 2018/3 => 2018/2
             * 2018/2 => 2018/1
             * 2018/1 => 2017/4
             */
            $moisPrec = ($mois == 1 ? 12 : $mois - 1);
            $yearPrec = ($mois == 1 ? $i - 1 : $i);
            if ($resultM[ $yearPrec ][ $moisPrec ][ 'nb_' . $suffix ] != 0) {

                $ca = $resultM[ $i ][ $mois ][ 'nb_' . $suffix ];
                $caPrec = $resultM[ $yearPrec ][ $moisPrec ][ 'nb_' . $suffix ];

                $resultM[ $i ][ $mois ][ 'txi_' . $suffix ] = (($ca - $caPrec) / $caPrec);
                $resultM[ $i ][ $mois ][ 'txi_' . $suffix ] = round(100 * $resultM[ $i ][ $mois ][ 'txi_' . $suffix ], 2) . ' %';
            }
            else {
                $resultM[ $i ][ $mois ][ 'txi_' . $suffix ] = '-';
            }
        }


    }
}
unset($resultY[ $currYear - 5 ]);
unset($resultS[ $currYear - 5 ]);
unset($resultT[ $currYear - 5 ]);

unset($resultM[ $currYear - 5 ]);
unset($resultM[ $currYear - 4 ]);
unset($resultM[ $currYear - 3 ]);
unset($resultM[ $currYear - 2 ]);


/** -----------------------------------------------------------------------------------------------
 * On obtient donc ici le tableau des ANNEES avec les chiffres de 0 à 5 mais, au final, on n'a
 * besoin que de 1 à 5
 */

/** -----------------------------------------------------------------------------------------------
 * On a tout ce qui faut maintenant !
 */
print json_encode(
    array(
        'RES' => 1,
        'YEAR' => $currYear,
        'DATAY' => $resultY,
        'DATAS' => $resultS,
        'DATAT' => $resultT,
        'DATAM' => $resultM,
    )
);

return;

function getCampagneIDPrec($id)
{

    $sqlGetPreviousCampagne = '
    SELECT FK_idCampagnePrec
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getPreviousCampagneExec = DbConnexion::getInstance()->prepare($sqlGetPreviousCampagne);
    $getPreviousCampagneExec->bindValue(':idc', $id, PDO::PARAM_INT);
    $getPreviousCampagneExec->execute();
    $getPreviousCampagne = $getPreviousCampagneExec->fetch(PDO::FETCH_OBJ);
    return $getPreviousCampagne->FK_idCampagnePrec;
}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToTrimestre($mois)
{

    return (1 + (($mois - 1) % 3 / 2) * 2);

}

/** -----------------------------------------------------------------------------------------------
 * @param $mois
 * @return float|int
 */
function monthToSemestre($mois)
{

    return $mois < 7 ? 1 : 2;

}
