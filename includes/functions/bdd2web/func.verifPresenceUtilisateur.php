<?php
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Recherche d'un utilisateur
// @detail :
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Demarrage de la session
// ------------------------------------------------------------------------------------
session_start ();

// ------------------------------------------------------------------------------------
// Db Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

// ------------------------------------------------------------------------------------
// PREPA REQUETE - Recherche de l'utilisateur
// ------------------------------------------------------------------------------------
$sqlRechercheUtilisateur = '
SELECT *
FROM du_utilisateur 
	INNER JOIN du_utilisateur_type ON du_utilisateur_type.idTypeUtilisateur = du_utilisateur.FK_idTypeUtilisateur
WHERE mailUtilisateur = :username AND passUtilisateur = :password';
$RechercheUtilisateurExc = DbConnexion::getInstance ()->prepare ( $sqlRechercheUtilisateur );

// ------------------------------------------------------------------------------------
// On verifie les POST
// ------------------------------------------------------------------------------------
if (filter_has_var ( INPUT_POST, 'nomUsr' ) && filter_has_var ( INPUT_POST, 'passUsr' )) {
	
	// ------------------------------------------------------------------------------------
	// Recherche de l'utilisateur dans la BDD
	// ------------------------------------------------------------------------------------
	$RechercheUtilisateurExc->bindValue ( ':username', strtolower ( filter_input(INPUT_POST, 'nomUsr') ), PDO::PARAM_STR );
	$RechercheUtilisateurExc->bindValue ( ':password', hash( 'sha512', filter_input(INPUT_POST, 'passUsr') ), PDO::PARAM_STR );
	$RechercheUtilisateurExc->execute ();
	
	// ------------------------------------------------------------------------------------
	// Si l'utilisateur a ete trouve
	// ------------------------------------------------------------------------------------
	if ($RechercheUtilisateurExc->rowCount () > 0) {

		// ------------------------------------------------------------------------------------
		// On liste les infos de l'utilisateur et enregistre en session
		// ------------------------------------------------------------------------------------
		$InfoUtilisateur = $RechercheUtilisateurExc->fetch ( PDO::FETCH_OBJ );

        // ------------------------------------------------------------------------------------
        // On va stocker la dernière connexion
        // ------------------------------------------------------------------------------------
        $sqlUpdateLastConnection = '
        INSERT INTO du_utilisateur_connexion
          (FK_idUtilisateur, lastConnexion, userIP)
        VALUES (:idu, :date, :ip)
        ';
        $updateLastConnectionExec = DbConnexion::getInstance()->prepare($sqlUpdateLastConnection);
        $updateLastConnectionExec->bindValue(':idu', $InfoUtilisateur->idUtilisateur, PDO::PARAM_INT);
        $updateLastConnectionExec->bindValue(':date',date('Y-m-d H:i:s'), PDO::PARAM_STR);
        $updateLastConnectionExec->bindValue(':ip', getUserIP(), PDO::PARAM_STR);
        $updateLastConnectionExec->execute ();

        #die($sqlUpdateLastConnection);

		$token_clair = KEY_ID . "http://" . $_SERVER ["SERVER_NAME"] . $_SERVER ['HTTP_USER_AGENT'];
		$informations = time () . "-" . $InfoUtilisateur->idUtilisateur;
		$token = hash ( 'sha512', $token_clair . $informations );
		setcookie ( "session_token", $token, time () + TIME_VALIDITY );
		setcookie ( "session_informations", $informations, time () + TIME_VALIDITY );
		
		$_SESSION ['session_token'.PROJECT_NAME] = $token;
		$_SESSION ['session_informations'.PROJECT_NAME] = $informations;
		
		$_SESSION ['idUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->idUtilisateur;
		$_SESSION ['nomUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->nomUtilisateur;
		$_SESSION ['prenomUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->prenomUtilisateur;
		$_SESSION ['emailUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->mailUtilisateur;
		$_SESSION ['FK_idTypeUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->FK_idTypeUtilisateur;
		$_SESSION ['libelleTypeUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->libelleTypeUtilisateur;
		$_SESSION ['boolLock'.PROJECT_NAME] = 0;
		echo json_encode ( array (
				'STATUS' => 1 
		) ); 
		
		
	} 	
	// ------------------------------------------------------------------------------------
	// L'utilisateur n'a pas ete trouve
	// ------------------------------------------------------------------------------------
	else {
		echo json_encode ( array (
            'STATUS' => 0,
            'PASS' => filter_input(INPUT_POST, 'passUsr')
		) );
	}
} 

// ------------------------------------------------------------------------------------
// Variable POST non presente
// ------------------------------------------------------------------------------------
else {
	echo json_encode ( array (
        'STATUS' => 0 ,
        'PASS' => filter_input(INPUT_POST, 'passUsr')
    ) );
}

function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

?>