<?php

/** -----------------------------------------------------------------------------------------------
 * On récupère les infos du PDV (éventuellement on géocode)
 */

require_once dirname(__FILE__) . '/../../../_config/config.sql.php';
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** -----------------------------------------------------------------------------------------------
 * On recherche le PDV
 */
$RechercheInfoPdvExc->bindValue(':idPdv', filter_input(INPUT_POST, 'idPdv'), PDO::PARAM_INT);
$RechercheInfoPdvExc->execute();

$PDV = $RechercheInfoPdvExc->fetch(PDO::FETCH_OBJ);
$lat = 0;
$lng = 0;

if ($PDV->latitude === null || $PDV->longitude === null || $PDV->latitude == 0.0 || $PDV->longitude == 0.0) {

    /** -------------------------------------------------------------------------------------------
     * On géocode rapidement
     */
    $url = ('https://maps.googleapis.com/maps/api/geocode/json?address=' . $PDV->adressePdv_A . ',' . $PDV->adressePdv_B . ',' . $PDV->codePostalPdv . ',' . $PDV->villePdv . '&region=FR&key=AIzaSyCYinfTYbQZNLJYFIYqgikVtjj5RITpm5k');
    $url = str_replace(" ", "+", $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response, TRUE);

    /** -------------------------------------------------------------------------------------------
     * Si tout est OK
     */
    if ($result[ 'status' ] == 'OK') {

        $lat = $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lat' ];
        $lng = $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lng' ];
        $latlon = array(
            'lat' => $lat,
            'lng' => $lng,
        );

        $sqlUpdatePDV = '
        UPDATE su_pdv
        SET latitude = :lat, longitude = :lng
        WHERE idPdv = :pdv
        ';
        $updatePDVExec = DbConnexion::getInstance()->prepare($sqlUpdatePDV);
        $updatePDVExec->bindValue(':pdv', filter_input(INPUT_POST, 'idPdv'), PDO::PARAM_INT);
        $updatePDVExec->bindValue(':lat', $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lat' ], PDO::PARAM_INT);
        $updatePDVExec->bindValue(':lng', $result[ 'results' ][ 0 ][ 'geometry' ][ 'location' ][ 'lng' ], PDO::PARAM_INT);
        $updatePDVExec->execute();

    }
    else {

        $latlon = array(
            'lat' => 0,
            'lng' => 0,
        );

    }


}
else {

    $lat = $PDV->latitude;
    $lng = $PDV->longitude;

    $latlon = array(
        'lat' => $lat,
        'lng' => $lng,
    );

}

/** -----------------------------------------------------------------------------------------------
 * Et maintenant, on prend la liste des intervenantes les plus proches...
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$inter = array();
if ($lat != 0) {
    $sqlListeIntervenantes = '
    SELECT nomIntervenant, prenomIntervenant, latitude, longitude,  
    ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians( latitude ) ) ) ) AS distance 
    FROM su_intervenant
    HAVING distance < 20 
    ORDER BY distance 
    LIMIT 0 , 5;
    ';

    $listeIntervenantesExec = DbConnexion::getInstance()->prepare($sqlListeIntervenantes);
    $listeIntervenantesExec->execute();
    while ($intervenante = $listeIntervenantesExec->fetch(PDO::FETCH_OBJ)) {
        $inter[] = array(
            'NOM' => $intervenante->nomIntervenant . ' ' . $intervenante->prenomIntervenant,
            'LAT' => $intervenante->latitude,
            'LON' => $intervenante->longitude,
            'DIST' => $intervenante->distance,
        );
    }
}


print json_encode(array(
    'result' => 1,
    'coord' => $latlon,
    'inter' => $inter,
));
