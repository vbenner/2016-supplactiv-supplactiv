<?php
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Recherche d'un utilisateur
// @detail :
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Db Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

// ------------------------------------------------------------------------------------
// PREPA REQUETE - Recherche de l'utilisateur
// ------------------------------------------------------------------------------------
$sqlRechercheUtilisateur = '
SELECT *
FROM du_utilisateur 
	INNER JOIN du_utilisateur_type ON du_utilisateur_type.idTypeUtilisateur = du_utilisateur.FK_idTypeUtilisateur
WHERE mailUtilisateur = :username AND passUtilisateur = :password';
$RechercheUtilisateurExc = DbConnexion::getInstance ()->prepare ( $sqlRechercheUtilisateur );

// ------------------------------------------------------------------------------------
// On verifie les POST
// ------------------------------------------------------------------------------------
if (filter_has_var ( INPUT_POST, 'username' ) && filter_has_var ( INPUT_POST, 'password' )) {

    // ------------------------------------------------------------------------------------
    // Recherche de l'utilisateur dans la BDD
    // ------------------------------------------------------------------------------------
    $RechercheUtilisateurExc->bindValue ( ':username', strtolower ( filter_input(INPUT_POST, 'username') ), PDO::PARAM_STR );
    $RechercheUtilisateurExc->bindValue ( ':password', hash ( 'sha512', filter_input(INPUT_POST, 'password') ), PDO::PARAM_STR );
    $RechercheUtilisateurExc->execute ();

    // ------------------------------------------------------------------------------------
    // Si l'utilisateur a ete trouve
    // ------------------------------------------------------------------------------------
    if ($RechercheUtilisateurExc->rowCount () > 0) {
        // ------------------------------------------------------------------------------------
        // On liste les infos de l'utilisateur et enregistre en session
        // ------------------------------------------------------------------------------------
        $InfoUtilisateur = $RechercheUtilisateurExc->fetch ( PDO::FETCH_OBJ );


        $token_clair = KEY_ID . "http://" . $_SERVER ["SERVER_NAME"] . $_SERVER ['HTTP_USER_AGENT'];
        $informations = time () . "-" . $InfoUtilisateur->idUtilisateur;
        $token = hash ( 'sha512', $token_clair . $informations );
        setcookie ( "session_token", $token, time () + TIME_VALIDITY );
        setcookie ( "session_informations", $informations, time () + TIME_VALIDITY );

        $_SESSION ['session_token'.PROJECT_NAME] = $token;
        $_SESSION ['session_informations'.PROJECT_NAME] = $informations;

        $_SESSION ['idUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->idUtilisateur;
        $_SESSION ['nomUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->nomUtilisateur;
        $_SESSION ['prenomUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->prenomUtilisateur;
        $_SESSION ['emailUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->mailUtilisateur;
        $_SESSION ['FK_idTypeUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->FK_idTypeUtilisateur;
        $_SESSION ['libelleTypeUtilisateur'.PROJECT_NAME] = $InfoUtilisateur->libelleTypeUtilisateur;
        $_SESSION ['boolLock'.PROJECT_NAME] = 0;
        echo json_encode ( array (
            'STATUS' => 1
        ) );


    }
    // ------------------------------------------------------------------------------------
    // L'utilisateur n'a pas ete trouve
    // ------------------------------------------------------------------------------------
    else {
        echo json_encode ( array (
            'STATUS' => 0
        ) );
    }
}

// ------------------------------------------------------------------------------------
// Variable POST non presente
// ------------------------------------------------------------------------------------
else {
    echo json_encode ( array (
        'STATUS' => 0
    ) );
}