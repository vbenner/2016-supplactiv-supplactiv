<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des intervenantes en fonction des dates de recherche
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** Initialisation des dates de recherche */
$dateFin = new DateTime('last day of this month');
$_SESSION['dateRechercheD_5_1'] = (isset($_SESSION['dateRechercheD_5_1'])) ? $_SESSION['dateRechercheD_5_1'] : date('01/m/Y');
$_SESSION['dateRechercheF_5_1'] = (isset($_SESSION['dateRechercheF_5_1'])) ? $_SESSION['dateRechercheF_5_1'] : $dateFin->format('d/m/Y');

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_5_1']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_5_1']);

/** Recherche des CONTRATS */
$RechercheIntervenanteContratExc->bindValue(':boolArchive', $_SESSION['boolArchive_5_1'], PDO::PARAM_INT);
$RechercheIntervenanteContratExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheIntervenanteContratExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheIntervenanteContratExc->execute();
print json_encode(array(
    'result' => 1,
    'intervenantes' => $RechercheIntervenanteContratExc->fetchAll(PDO::FETCH_OBJ)
));
