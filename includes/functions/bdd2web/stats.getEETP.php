<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des rémunérations / année et / intervenant
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();
$type = $_POST['intervenant'];
$intervenant = filter_input(INPUT_POST, 'intervenant');

/** -----------------------------------------------------------------------------------------------
 * On récupère d'abord la liste des AUTO
 */
$hasWhere = false;
switch ($intervenant) {
    case 'ALL':
        $whereAuto = ' ';
        break;
    case 'OUI':
        $whereAuto = ' WHERE boolAutoEntrepreneuse = \'OUI\' ';
        $hasWhere = true;
        break;
    case 'NON':
        $whereAuto = ' WHERE boolAutoEntrepreneuse = \'NON\' ';
        $hasWhere = true;
        break;
}

/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des salariés ayant eu un contrat durant les 5 derniers mois
 */
$sqlGetlisteSalaries = '
SELECT *
FROM su_intervenant SUI
INNER JOIN su_intervention SUV ON SUV.FK_idIntervenant = SUI.idIntervenant
'.$whereAuto.
($hasWhere ? ' AND ' : ' WHERE ').' NOT ISNULL(SUV.FK_idContrat)
'.' AND dateDebut >= :date
ORDER BY nomIntervenant, dateDebut
';

/** -----------------------------------------------------------------------------------------------
 * On remonte au premier du mois en cours et paf, on calcule 5 mois avant
 */

/** -----------------------------------------------------------------------------------------------
 * Tableau de conversion des mois
 */
for ($i = 4 ; $i >= 0 ; $i-- ) {
    $tmp = new DateTime('first day of this month');
    $tmp->modify('-'.$i.' months');
    $tabConvertMonth [intval($tmp->format('m'))] = 5-$i;
}

$dateStart = new DateTime('first day of this month');
$dateStart->modify('-4 months');
$dateStart->format('Y-m-d '.'00:00:00');

$getlisteSalariesExec = DbConnexion::getInstance()->prepare($sqlGetlisteSalaries);
$getlisteSalariesExec->bindValue(':date', $dateStart->format('Y-m-d '.'00:00:00'), PDO::PARAM_STR);
$getlisteSalariesExec->execute();

#echo $dateStart->format('Y-m-d '.'00:00:00');
$emptyDay = array();
for ($i = 1; $i <= 31 ; $i++) {
    $emptyDay[$i] = 0;
}
$emptyMonth = array();
for ($i = 1; $i <= 5 ; $i++) {
    $emptyMonth[$i] = array(
        'cumul' => 0,
        'etp' => 0,
        'days' => $emptyDay
    );
}

/** -----------------------------------------------------------------------------------------------
 * Comme on traite les auto et les pas-auto séparément, autant faire 2 tableaux
 */
$auto = $pasauto = array();
while ($salarie = $getlisteSalariesExec->fetch(PDO::FETCH_OBJ)) {

    #echo '<pre>';
    #print_r($salarie);
    #echo '</pre>';
    #die();
    $isAuto = $salarie->boolAutoEntrepreneuse == 'OUI' ? 1 : 0;
    if (!isset(${($isAuto ? 'auto' : 'pasauto')}[$salarie->idIntervenant])) {
        ${($isAuto ? 'auto' : 'pasauto')}[$salarie->idIntervenant] = array(
            'nom' => $salarie->nomIntervenant.' '.$salarie->prenomIntervenant,
            'auto' => $salarie->boolAutoEntrepreneuse,
            'detail' => $emptyMonth
        );
    }

    /** -------------------------------------------------------------------------------------------
     * On ne fait que faire le calcul date / date + durée cumulée
     * Cette étape permet de connaître le mois et le numéro du jour
     */
    #$d1 = new DateTime($salarie->dateDebut);
    #$d2 = new DateTime(date('Y-m-d'));
    #$interval = $d2->diff($d1);
    $mois = $tabConvertMonth[intval(date('m', strtotime($salarie->dateDebut)))];
    $day = intval(date('d', strtotime($salarie->dateDebut)));

    /** -------------------------------------------------------------------------------------------
     * Avec cette étape, on connait la durée en minutes
     */
    $d1 = strtotime($salarie->dateDebut);
    $d2 = strtotime($salarie->dateFin);
    $duree =  (($d2 - $d1) / 60)/60;

    #die('Mois = '.$mois.' -> '.$day.' : '.$duree);
    if ($salarie->idIntervenant == 1611) {
        #echo $mois.'/'.$day.' - '.$salarie->dateDebut.' -> '.$salarie->dateFin.' = '.$duree."<br/>";
        #echo '<pre>';
        #print_r($pasauto);
        #echo '</pre>';
    }

    ${($isAuto ? 'auto' : 'pasauto')}[$salarie->idIntervenant]['detail'][$mois]['days'][$day] += $duree;

    #print 'Mois = '.$mois.' -> '.$day.' : '.$duree;
    #echo '<pre>';
    #print_r($pasauto);
    #echo '</pre>';
    #die();

}

#echo '<pre>';
#print_r($pasauto);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * On fait maintenant une division mois / mois de la somme des days
 * On divise tout par 151.66
 */
$limite = $_POST['limite'] != '' ? (double)$_POST['limite'] : 151.66;
foreach ($auto as $key => $val) {
    $keep = false;
    for ($i = 1; $i <= 5 ; $i++) {
        for ($j = 1; $j <= 31 ; $j++) {
            $auto[$key]['detail'][$i]['cumul'] += $auto[$key]['detail'][$i]['days'][$j];
        }
        $auto[$key]['detail'][$i]['etp'] = $auto[$key]['detail'][$i]['cumul'] / $limite;
        $keep = $auto[$key]['detail'][$i]['cumul'] > $limite ? true : $keep;

    }
    if (!$keep) {
        unset ($auto[$key]);
    }
}

foreach ($pasauto as $key => $val) {
    $keep = false;
    for ($i = 1; $i <= 5 ; $i++) {
        for ($j = 1; $j <= 31 ; $j++) {
            $pasauto[$key]['detail'][$i]['cumul'] += $pasauto[$key]['detail'][$i]['days'][$j];
        }
        $pasauto[$key]['detail'][$i]['etp'] = $pasauto[$key]['detail'][$i]['cumul'] / $limite;
        $keep = $pasauto[$key]['detail'][$i]['cumul'] > $limite ? true : $keep;

    }
    if (!$keep) {
        unset ($pasauto[$key]);
    }
}

#echo '<pre>';
#print_r($pasauto);
#echo '</pre>';
#die();

$keys_auto = array(
    'KEYS' => array(
));
$keys_pasauto = array(
    'KEYS' => array(
));

$tabAuto = array();
$tabPasAuto = array();
/** -----------------------------------------------------------------------------------------------
 * On va tout remonter d'un niveau
 */
foreach ($auto as $key => $val) {
    $tabAuto [] = $val;
}

foreach ($auto as $key => $val) {
    $keys_auto['KEYS'][] = $key;
}

foreach ($pasauto as $key => $val) {
    $tabPasAuto [] = $val;
}

foreach ($pasauto as $key => $val) {
    $keys_pasauto['KEYS'][] = $key;
}


print json_encode(
    array(
        'RES' => 1,
        'SQL' => $sqlGetlisteSalaries,
        'KEY_AUTO' => $keys_auto,
        'AUTO' => $auto,
        'VAL_AUTO' => $tabAuto,
        'KEY_PASAUTO' => $keys_pasauto,
        'PASAUTO' => $pasauto,
    )
);