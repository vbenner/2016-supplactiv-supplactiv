<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des nouveaux entrants / mois et / année
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

/** -----------------------------------------------------------------------------------------------
 * On va prendre l'année en cours et celle de l'année précédente
 */
$mois = array();
$txy = array();
for ($i = 1; $i <= 13 ; $i++) {
    $mois[$_POST['annee']][$i] = 0;
    $mois[$_POST['annee']-1][$i] = 0;
    $txy[$i] = 0;
}

/** -----------------------------------------------------------------------------------------------
 * Calcul du nombre d'intervenants ACTIFS + age moyen
 * Dissocier les auto ?
 */
$sqlNouveauxEntrants = '
SELECT MONTH(dateCreation) AS lemois, COUNT(DISTINCT idIntervenant) AS lecount
FROM su_intervenant SUI 
    INNER JOIN su_intervention SUN ON SUN.FK_idIntervenant = SUI.idIntervenant 
WHERE YEAR(dateCreation) = :annee
AND NOT ISNULL(FK_idContrat)
GROUP BY lemois
';
for ($i = $_POST['annee']-1; $i <= $_POST['annee']; $i++) {
    $nouveauxEntrantsExec = DbConnexion::getInstance()->prepare($sqlNouveauxEntrants);
    $nouveauxEntrantsExec->bindValue(':annee', $i, PDO::PARAM_INT);
    $nouveauxEntrantsExec->execute();
    while($nouveauxEntrants = $nouveauxEntrantsExec->fetch(PDO::FETCH_OBJ)) {
        $mois[$i][$nouveauxEntrants->lemois] = $nouveauxEntrants->lecount;
        $mois[$i][13] += $nouveauxEntrants->lecount;
    }
}

/** -----------------------------------------------------------------------------------------------
 * On calcule le TAUX
 */
for ($i = 1 ; $i <= 13 ; $i++) {

    /** -------------------------------------------------------------------------------------------
     * Evolution globale
     */
    if ($mois[$_POST['annee']-1][$i] != 0) {
        $txy[$i] = (($mois[$_POST['annee']][$i] - $mois[$_POST['annee']-1][$i]) / $mois[$_POST['annee']-1][$i]);
        $txy[$i] = round(100 * $txy[$i], 2 ).' %';
    }
    else {
        $txy[$i] = '-';
    }
}

/** -----------------------------------------------------------------------------------------------
 *
 */
print json_encode(
    array(
        'YEAR' => $_POST['annee'],
        'RES' => 1,
        'MOIS' => $mois,
        'TXY' => $txy
    )
);