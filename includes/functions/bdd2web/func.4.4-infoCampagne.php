<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur une campagne
 */
ini_set('display_errors','on');
/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idCampagne' )) {

    $listeTarif = array();

    /** Recherche des tarifs campagnes */
    $RechercheTarifCampagneExc->bindValue(':idCampagne', filter_input ( INPUT_POST, 'idCampagne' ));
    $RechercheTarifCampagneExc->execute();
    while($InfoCampagne = $RechercheTarifCampagneExc->fetch(PDO::FETCH_OBJ)){

        $tarif = 0;
        switch($InfoCampagne->FK_idTypeMission){
            case 1: $tarif = $InfoCampagne->tarifMarchandising*$InfoCampagne->nbIntervention; break;
            case 2: $tarif = $InfoCampagne->tarifFormation*$InfoCampagne->nbIntervention; break;
            case 3: $tarif = $InfoCampagne->tarifAnimation*$InfoCampagne->nbIntervention; break;
        }

        $listeTarif[$InfoCampagne->FK_idTypeMission] = array(
            'nbIntervention' => $InfoCampagne->nbIntervention,
            'tarif' => $tarif,
            'typeMission' => $InfoCampagne->FK_idTypeMission
        );
    }
    
    $listeTarif2 = array(); 
    $nbJourType = array(
        1 => array('j' => array(), 'i' => 0, 'tarif' => 0, 'tarifT' => 0), 
        2 => array('j' => array(), 'i' => 0, 'tarif' => 0, 'tarifT' => 0), 
        3 => array('j' => array(), 'i' => 0, 'tarif' => 0, 'tarifT' => 0)
    );
    
    /** Recherche des tarifs campagnes */
    $RechercheTarifCampagne2Exc->bindValue(':idCampagne', filter_input ( INPUT_POST, 'idCampagne' ));
    $RechercheTarifCampagne2Exc->execute();
    while($InfoCampagne = $RechercheTarifCampagne2Exc->fetch(PDO::FETCH_OBJ)){

       if(!isset($nbJourType[$InfoCampagne->FK_idTypeMission]['j'][$InfoCampagne->dateD])){
            $nbJourType[$InfoCampagne->FK_idTypeMission]['j'][$InfoCampagne->dateD] = 0;   
       }
        
        $nbJourType[$InfoCampagne->FK_idTypeMission]['j'][$InfoCampagne->dateD]++;
        
        $nbJourType[$InfoCampagne->FK_idTypeMission]['i']++;
        
        
        switch($InfoCampagne->FK_idTypeMission){
            case 1: $nbJourType[$InfoCampagne->FK_idTypeMission]['tarif']  = $InfoCampagne->tarifMarchandising; break;
            case 2: $nbJourType[$InfoCampagne->FK_idTypeMission]['tarif']  = $InfoCampagne->tarifFormation; break;
            case 3: $nbJourType[$InfoCampagne->FK_idTypeMission]['tarif']  = $InfoCampagne->tarifAnimation; break;
        }
    }
    
    foreach($nbJourType as $idTypeMission => $info){
        
        $listeTarif[$idTypeMission] = array(
            'nbIntervention' => $info['i'],
            'nbJour' => sizeof($info['j']),
            'tarif' => $info['i']*$info['tarif'],
            'typeMission' => $idTypeMission
        );   
    }

    $fraisIntervenant = 0; $fraisSalaire = 0; $fraisTel = $montantPrime = $fraisRepas = $fraisAutre = 0;
    
    $dateRepas = $dateTel = array();
    $RechercheFraisCampagneExc->bindValue(':idCampagne', filter_input ( INPUT_POST, 'idCampagne' ));
    $RechercheFraisCampagneExc->execute();
    while($InfoFrais = $RechercheFraisCampagneExc->fetch(PDO::FETCH_OBJ)){
        $duree = ($InfoFrais->DUREE >= 28800) ? $InfoFrais->DUREE-3600 : $InfoFrais->DUREE;
        $fraisSalaire += $InfoFrais->salaireBrut*($duree/3600);
       // $fraisIntervenant += ($InfoFrais->boolVehiculePersonnel == 'OUI') ? ($InfoFrais->tarifKM*$InfoFrais->totalKm) : 0;
        $fraisTel += ($InfoFrais->autorisationFraisTel == 'OUI') ? $InfoFrais->fraisTelephone : 0;
        $montantPrime +=  $InfoFrais->montantPrime;
        $fraisRepas +=  $InfoFrais->fraisRepas;
        $fraisAutre +=  $InfoFrais->fraisAutre;
        
        $dateRepas[$InfoFrais->dateD] = $InfoFrais->fraisRepas;
        $dateTel[$InfoFrais->dateD] = $InfoFrais->fraisTelephone;
    }
    
    $fraisRepas2 = 0;
    foreach($dateRepas as $d=>$tarif){
        $fraisRepas2+=$tarif;
    }
    
    $fraisTel2 = 0;
    foreach($dateTel as $d=>$tarif){
        $fraisTel2+=$tarif;
    }
    
    $fraisIntervenant = $fraisTel2+$montantPrime+$fraisAutre+$fraisRepas2;

    $RechercheInfoCampagneExc->bindValue(':idCampagne', filter_input ( INPUT_POST, 'idCampagne' ));
    $RechercheInfoCampagneExc->execute();
    $InfoCampagne = $RechercheInfoCampagneExc->fetch(PDO::FETCH_OBJ);

    $RechercheInterlocuteurClientExc->bindValue(':idClient', $InfoCampagne->FK_idClient);
    $RechercheInterlocuteurClientExc->execute();
    $RechercheInterlocuteurAgenceExc->execute();

    print json_encode(array(
        'info' => $InfoCampagne,
        'tarif' => $listeTarif,
        'frais' => array(
            'fraisIntervenant' => round($fraisIntervenant, 2),
            'fraisSalaire' => round($fraisSalaire, 2),
            'fraisTel' => $fraisTel.' -- '.$fraisTel2,
            'montantPrime' => $montantPrime,
            'fraisAutre' => $fraisAutre,
            'fraisRepas' => $fraisRepas.' -- '.$fraisRepas2
        ),
        'interlocuteursClient' => $RechercheInterlocuteurClientExc->fetchAll(PDO::FETCH_OBJ),
        'interlocuteursAgence' => $RechercheInterlocuteurAgenceExc->fetchAll(PDO::FETCH_OBJ),
        'locked' => (($_SESSION ['idUtilisateur'.PROJECT_NAME] == 1145 ||
            $_SESSION ['idUtilisateur'.PROJECT_NAME] == 1169 ||
            $_SESSION ['idUtilisateur'.PROJECT_NAME] == 1172 ||
            $_SESSION ['idUtilisateur'.PROJECT_NAME] == 1
        ) ? 0 : 1)

    ));
}
