<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Liste des points de vente visité par une intervenante
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** @var $listePdv Liste des points de vente */
$listePdv = array();

/** On test la presence des sessions */
if(isset($_SESSION['idIntervenant_13_0']) && $_SESSION['idIntervenant_13_0'] != 'ALL') {

    /** Recherche des PDV */
    $RecherchePdvVisiteExc->bindValue(':idIntervenant', $_SESSION['idIntervenant_13_0'], PDO::PARAM_INT);
    $RecherchePdvVisiteExc->execute();
    $listePdv = $RecherchePdvVisiteExc->fetchAll(PDO::FETCH_OBJ);
}

print json_encode($listePdv);