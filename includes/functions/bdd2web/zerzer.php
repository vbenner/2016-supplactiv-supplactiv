<?php
$order = array (
    'code' => '',
    'status' => 0,
    'paid' => 0,
    'modifiedTime' => '',
    'totalPrice' => 0.0,
    'deliveryAdress' => '',
    'user' => array(
        'name' => '',
        'uid' => 'mail@test.fr',
        'phone' => ''
    ),
    'pickupTime' => '02/03/2020 15:00',
    'entries' => array (
        array(
            'entryNumber' => 0,
            'quantity' => 0,
            'quantityShipped' => 0,
            'totalPrice' => 0.0,
            'adress' => '',
            'product' => array(
                'ean' => '',
                'checkoutLabel' => '',
                'supercategories' => '',
                'eans' => array(
                    '', ''
                ),
            )
        ),
    ),
);