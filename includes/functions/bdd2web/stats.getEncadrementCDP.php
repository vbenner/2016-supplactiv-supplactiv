<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des encadrements / CDP
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$data = array();
$keys = array();
$month = array();

$year = filter_input(INPUT_POST, 'annee');

/** -----------------------------------------------------------------------------------------------
 * Preparation du Tableau des mois
 * A = Animation
 * F = Formation
 * I = Intervenant
 *
 * Le Pourcentage sera effectué sur le comptage total et le nombre d'intervention total
 */
for ($i = 1 ; $i <= 13 ; $i++) {
    for ($j = 0; $j <= 1; $j++) {
        $month[$year-$j][$i] = array(
            'COUNT' => 0,
            'COUNTA' => 0,
            'COUNTF' => 0,
            'COUNTI' => 0,
            'PERCENT' => '',
            'PERCENTI' => ''
        );
    }
}

/** -----------------------------------------------------------------------------------------------
 * Liste des CHEFS DE PROJETS
 * --> attention, on passe par l'interlocuteur de la mission et non plus celui de la campagne
 */
$sqlGetListeCDP = '
SELECT DISTINCT DUU.idUtilisateur, DUU.nomUtilisateur
FROM du_utilisateur DUU
	INNER JOIN su_mission SMI ON SMI.decideur_id = DUU.idUtilisateur
	INNER JOIN su_intervention SIN ON SIN.FK_idMission = SMI.idMission
WHERE DUU.idUtilisateur != 0
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)
';
$getListeCDPExec = DbConnexion::getInstance()->prepare($sqlGetListeCDP);
$getListeCDPExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeCDPExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
$getListeCDPExec->execute();
while ($getListeCDP = $getListeCDPExec->fetch(PDO::FETCH_OBJ)) {
    $data [$getListeCDP->idUtilisateur] = array (
        'NOM' => $getListeCDP->nomUtilisateur,
        'DETAIL' => $month
    );
}

#echo '<pre>';
#print_r($data);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * On refait maintenant la même requête mais on va récupérer les détails
 *
 * -> STEP #1 - MISSIONS DE TYPE Animation (3) --> TOUTE LA JOURNEE
 */
$sqlGetListeCDP = '
SELECT DISTINCT DUU.idUtilisateur AS id, DUU.nomUtilisateur AS nom, 
	COUNT(SIN.idIntervention) AS LeCount, YEAR(SIN.dateDebut) AS LAnnee,
	MONTH(SIN.dateDebut) AS LeMois, COUNT(DISTINCT SIN.FK_idIntervenant) AS LeCountI
FROM du_utilisateur DUU
	INNER JOIN su_mission SMI ON SMI.decideur_id = DUU.idUtilisateur
	INNER JOIN su_intervention SIN ON SIN.FK_idMission = SMI.idMission
WHERE DUU.idUtilisateur != 0
AND SMI.FK_idTypeMission = 3
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)
AND NOT ISNULL(SIN.FK_idContrat)
GROUP BY id, LAnnee, LeMois
ORDER BY nom, LAnnee, LeMois
';

$getListeCDPExec = DbConnexion::getInstance()->prepare($sqlGetListeCDP);
$getListeCDPExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeCDPExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
$getListeCDPExec->execute();
while ($getListeCDP = $getListeCDPExec->fetch(PDO::FETCH_OBJ)) {
    $data[$getListeCDP->id]['DETAIL'][$getListeCDP->LAnnee][$getListeCDP->LeMois]['COUNTA'] = (int)$getListeCDP->LeCount;
    $data[$getListeCDP->id]['DETAIL'][$getListeCDP->LAnnee][$getListeCDP->LeMois]['COUNTI'] += (int)$getListeCDP->LeCountI;
}


/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des FORMATIONS - ce qui est plus compliqué car les journées
 * #STEP 2 --> nombre d'interventions
 */
$sqlGetListeCDP = '
SELECT DISTINCT DUU.idUtilisateur AS id, DUU.nomUtilisateur AS nom, 
	COUNT(DISTINCT (DATE_FORMAT(SIN.dateDebut, "%Y-%m-%d"))) AS LeCount, 
	SIN.FK_idIntervenant AS intervenant,
	YEAR(SIN.dateDebut) AS LAnnee,
	MONTH(SIN.dateDebut) AS LeMois, COUNT(DISTINCT SIN.FK_idIntervenant) AS LeCountI
FROM du_utilisateur DUU
	INNER JOIN su_mission SMI ON SMI.decideur_id = DUU.idUtilisateur
	INNER JOIN su_intervention SIN ON SIN.FK_idMission = SMI.idMission
WHERE DUU.idUtilisateur != 0
AND SMI.FK_idTypeMission = 2
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)
GROUP BY id, LAnnee, LeMois, intervenant
ORDER BY nom, LAnnee, LeMois
';

$getListeCDPExec = DbConnexion::getInstance()->prepare($sqlGetListeCDP);
$getListeCDPExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeCDPExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
$getListeCDPExec->execute();
while ($getListeCDP = $getListeCDPExec->fetch(PDO::FETCH_OBJ)) {
    $data[$getListeCDP->id]['DETAIL'][$getListeCDP->LAnnee][$getListeCDP->LeMois]['COUNTF'] += (int)$getListeCDP->LeCount;
    $data[$getListeCDP->id]['DETAIL'][$getListeCDP->LAnnee][$getListeCDP->LeMois]['COUNTI'] += (int)$getListeCDP->LeCountI;
}


/** -----------------------------------------------------------------------------------------------
 * Le nombre d'intervenants
 * #STEP 3 --> nombre d'intervenants
 */
/*
$sqlGetListeCDP = '
SELECT DISTINCT DATE_FORMAT(SIN.dateDebut, \'%Y-%m-%d\') AS LaDate,
	DUU.idUtilisateur AS id, COUNT(DISTINCT SIN.FK_idIntervenant) AS LeCount
FROM du_utilisateur DUU
	INNER JOIN su_mission SMI ON SMI.decideur_id = DUU.idUtilisateur
	INNER JOIN su_intervention SIN ON SIN.FK_idMission = SMI.idMission
WHERE SMI.FK_idTypeMission = 2
AND (
	YEAR(SIN.dateDebut) = :year 
	OR YEAR(SIN.dateDebut) = :yearprec
)
GROUP BY LaDate, id
ORDER BY LaDate
';

$getListeCDPExec = DbConnexion::getInstance()->prepare($sqlGetListeCDP);
$getListeCDPExec->bindValue(':year', $year, PDO::PARAM_INT);
$getListeCDPExec->bindValue(':yearprec', ($year-1), PDO::PARAM_INT);
$getListeCDPExec->execute();
while ($getListeCDP = $getListeCDPExec->fetch(PDO::FETCH_OBJ)) {
    $annee = substr($getListeCDP->LaDate, 0, 4);
    $mois = (int)substr($getListeCDP->LaDate, 5, 2);
    $data[$getListeCDP->id]['DETAIL'][$annee][$mois]['COUNTI'] += $getListeCDP->LeCount;
}
*/
#echo '<pre>';
#print_r($data);
#echo '</pre>';
#die();

/** -----------------------------------------------------------------------------------------------
 * On fusionne le COUNT (COUNT A + COUNT F) (on le fait pour l'annee en cours et la précédente
 */
foreach($data as $key => $val) {
    /** -------------------------------------------------------------------------------------------
     * On fait le calcul du pourcentage
     */
    for ($i = 1; $i <= 12; $i++) {
        $data[$key]['DETAIL'][$year][$i]['COUNT'] = $data[$key]['DETAIL'][$year][$i]['COUNTA'] +
            $data[$key]['DETAIL'][$year][$i]['COUNTF'];
    }

    for ($i = 1; $i <= 12; $i++) {
        $data[$key]['DETAIL'][$year-1][$i]['COUNT'] = $data[$key]['DETAIL'][$year-1][$i]['COUNTA'] +
            $data[$key]['DETAIL'][$year-1][$i]['COUNTF'];
    }
}
/** -----------------------------------------------------------------------------------------------
 * Dans un deuxième temps, on va FAIRE LE POURCENTAGE
 */
foreach($data as $key => $val) {
    /** -------------------------------------------------------------------------------------------
     * On fait le calcul du pourcentage
     */
    for ($i = 1; $i <= 12 ; $i++) {
        $data[$key]['DETAIL'][$year][13]['COUNT'] += $data[$key]['DETAIL'][$year][$i]['COUNT'];
        $data[$key]['DETAIL'][$year][13]['COUNTI'] += $data[$key]['DETAIL'][$year][$i]['COUNTI'];
        $data[$key]['DETAIL'][$year][13]['COUNTA'] += $data[$key]['DETAIL'][$year][$i]['COUNTA'];
        $data[$key]['DETAIL'][$year][13]['COUNTF'] += $data[$key]['DETAIL'][$year][$i]['COUNTF'];
    }
    for ($i = 1; $i <= 13 ; $i++) {
        if ($val['DETAIL'][$year-1][$i]['COUNT'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENT'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNT'] - $val['DETAIL'][$year-1][$i]['COUNT'])/$val['DETAIL'][$year-1][$i]['COUNT'], 2).' %';
        }
        else {
            $data[$key]['DETAIL'][$year][$i]['PERCENT'] = '-';
        }

        if ($val['DETAIL'][$year-1][$i]['COUNTI'] != 0) {
            $data[$key]['DETAIL'][$year][$i]['PERCENTI'] = round(100*($data[$key]['DETAIL'][$year][$i]['COUNTI'] - $val['DETAIL'][$year-1][$i]['COUNTI'])/$val['DETAIL'][$year-1][$i]['COUNTI'], 2).' %';
        }
        else {
            $data[$key]['DETAIL'][$year][$i]['PERCENTI'] = '-';
        }

        #$data[$key]['DETAIL'][$year][13]['COUNT'] += $data[$key]['DETAIL'][$year][$i]['COUNT'];
        #$data[$key]['DETAIL'][$year][13]['COUNTI'] += $data[$key]['DETAIL'][$year][$i]['COUNTI'];
    }

    /** -------------------------------------------------------------------------------------------
     * On remonte d'un niveau
     */
    unset ($data[$key]['DETAIL'][$year-1]);
}

/** -----------------------------------------------------------------------------------------------
 * On enlève ceux qui ne font rien
 */
foreach($data as $key => $val) {
    if ($val['DETAIL'][$year][13]['COUNT'] == 0) {
        unset($data[$key]);
    }
}

/** -----------------------------------------------------------------------------------------------
 * On recrée les clés
 */
foreach($data as $key => $val) {
    $keys[] = $key;
}

print json_encode(
    array(
        'RES' => 1,
        'DATA' => $data,
        'KEYS' => $keys,
        'SQL' => $sqlGetListeMissions
    )
);