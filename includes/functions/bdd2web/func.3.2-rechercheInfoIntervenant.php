<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des sous-categories de point de vente
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$RechercheCompetenceExc->execute();
$RechercheQualificationExc->execute();
$RechercheExperienceExc->execute();
$RechercheGenreIntervenantExc->execute();
$RechercheSituationIntervenantExc->execute();
$RecherchePaysExc->execute();
$RechercheDepartementExc->execute();
$sqlRechercheNiveauExec->execute();
$dureeTotal = 0; $nbHeureCumule = 0;

$infoIntervenant = null;
if(filter_has_var(INPUT_POST, 'idIntervenant')){
    $RechercheInfoIntervenantTotalExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $RechercheInfoIntervenantTotalExc->execute();
    $infoIntervenant = $RechercheInfoIntervenantTotalExc->fetch(PDO::FETCH_OBJ);

    $lstHeure = array();
    $RechercheHistoriqueIntervention4MoisExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'));
    $RechercheHistoriqueIntervention4MoisExc->execute();
    while($InfoDuree = $RechercheHistoriqueIntervention4MoisExc->fetch(PDO::FETCH_OBJ)){
        $DT = new DateTime($InfoDuree->dateDebut);
        @$lstHeure[$DT->format('Ymd')] += $InfoDuree->nbSeconde;
    }

    $nbInter = 1;
    foreach($lstHeure as $jour => $nbSeconde){
        $Total = ($nbSeconde > 25200) ? 25200 : $nbSeconde;

        if($nbInter != sizeof($lstHeure)){
            if(isset($lstHeure[$jour+1])){
                $nbHeureCumule += $Total;
            }else $nbHeureCumule = $Total;

        }else {
            if(isset($lstHeure[$jour-1])){
                $nbHeureCumule += $Total;
            }else $nbHeureCumule = $Total;
        }
        $nbInter++;
        $dureeTotal += $Total;
    }

    $dureeTotal = formatSecToMin($dureeTotal);
    $nbHeureCumule = formatSecToMin($nbHeureCumule);
}

print json_encode(array(
    'competences' => $RechercheCompetenceExc->fetchAll(PDO::FETCH_OBJ),
    'qualifications' => $RechercheQualificationExc->fetchAll(PDO::FETCH_OBJ),
    'experiences' => $RechercheExperienceExc->fetchAll(PDO::FETCH_OBJ),
    'genres' => $RechercheGenreIntervenantExc->fetchAll(PDO::FETCH_OBJ),
    'situations' => $RechercheSituationIntervenantExc->fetchAll(PDO::FETCH_OBJ),
    'pays' => $RecherchePaysExc->fetchAll(PDO::FETCH_OBJ),
    'departements' => $RechercheDepartementExc->fetchAll(PDO::FETCH_OBJ),
    'niveaux' => $sqlRechercheNiveauExec->fetchAll(PDO::FETCH_OBJ),
    'intervenant' => $infoIntervenant,
    'dureeTotal' => $dureeTotal,
    'nbHeureCumule' => $nbHeureCumule
));