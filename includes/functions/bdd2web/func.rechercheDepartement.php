<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des departements
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$RechercheDepartementExc->execute();

print json_encode(array(
    'departements' => $RechercheDepartementExc->fetchAll(PDO::FETCH_OBJ)
));