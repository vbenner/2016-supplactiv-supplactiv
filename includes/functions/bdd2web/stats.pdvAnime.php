<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des points de vente les plus / moins animés
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

$from = filter_input(INPUT_POST, 'from');
$from = DateTime::createFromFormat('d/m/Y', $from);
$to = filter_input(INPUT_POST, 'to');
$to = DateTime::createFromFormat('d/m/Y', $to);
$limit = filter_input(INPUT_POST, 'limit');

/** -----------------------------------------------------------------------------------------------
 * Init
 */

/** -----------------------------------------------------------------------------------------------
 * SQL : récupère la liste des interventions avec les limites demandées. Première sélection avec
 * tri décroissant
 */
$pdvP = array();
$pdvM = array();

$sqlPdvAnime= '
SELECT DISTINCT P.libellePdv, COUNT(I.idIntervention) AS leCount, 
  CONCAT(P.codePostalPdv, " ", P.villePdv) AS cpville
FROM su_intervention I
  INNER JOIN su_pdv P ON P.idPdv = I.FK_idPdv
WHERE 
  I.dateDebut >= :du 
AND
  I.dateFin <= :au
AND NOT ISNULL(FK_idContrat)
GROUP BY P.idPdv
ORDER BY LeCount DESC
'. ($limit != 0 ? ' LIMIT 0, '.$limit : '') .'
';

$pdvAnimeExec = DbConnexion::getInstance()->prepare($sqlPdvAnime);
$pdvAnimeExec->bindValue(':du', $from->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$pdvAnimeExec->bindValue(':au', $to->format('Y-m-d').' 23:59:59', PDO::PARAM_STR);
$pdvAnimeExec->execute();
while ($pdvAnime = $pdvAnimeExec->fetch(PDO::FETCH_OBJ)) {

    $pdvP[] = array(
        'NOM' => $pdvAnime->libellePdv,
        'ADR' => $pdvAnime->cpville,
        'QTE' => $pdvAnime->leCount
    );
}

$sqlPdvAnime= '
SELECT DISTINCT P.libellePdv, COUNT(I.idIntervention) AS leCount, 
  CONCAT(P.codePostalPdv, " ", P.villePdv) AS cpville
FROM su_intervention I
  INNER JOIN su_pdv P ON P.idPdv = I.FK_idPdv
WHERE 
  I.dateDebut >= :du 
AND
  I.dateFin <= :au
AND NOT ISNULL(FK_idContrat)
GROUP BY libellePdv
ORDER BY LeCount ASC
'. ($limit != 0 ? ' LIMIT 0, '.$limit : '') .'
';

$pdvAnimeExec = DbConnexion::getInstance()->prepare($sqlPdvAnime);
$pdvAnimeExec->bindValue(':du', $from->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$pdvAnimeExec->bindValue(':au', $to->format('Y-m-d').' 23:59:59', PDO::PARAM_STR);
$pdvAnimeExec->execute();
while ($pdvAnime = $pdvAnimeExec->fetch(PDO::FETCH_OBJ)) {
    $pdvM[] = array(
        'NOM' => $pdvAnime->libellePdv,
        'ADR' => $pdvAnime->cpville,
        'QTE' => $pdvAnime->leCount
    );
}

print json_encode(
    array(
        'RES' => 1,
        'PLUS' => $pdvP,
        'MOINS' => $pdvM,
    )
);