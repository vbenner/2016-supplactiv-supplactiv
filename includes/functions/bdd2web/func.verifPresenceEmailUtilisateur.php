<?php
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Demande d'un nouvel identifiant
// @detail :
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Db Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

// ------------------------------------------------------------------------------------
// Classe PHP Mailer
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../../librairies/phpmailer/class.phpmailer.php';

class SUMailer extends PHPMailer {

    public $CharSet = "utf-8";
    public $From = "dlaurent@supplactiv.fr";
    public $FromName = "BO Suppl'Activ";
    public $Mailer = "smtp";
    public $Host = "in.mailjet.com";
    public $Port = 587;
    public $SMTPAuth = true;
    public $SMTPSecure = "tls";
    public $Username = "8127b7f50d6e81673e3d353d46b1a335";
    public $Password = "5dacb7ae369589c78793ccdf8b24cf1d";
    public $ContentType = "text/html";
    public $SMTPDebug = false;


    /** *
     * Adds a string or binary attachment (non-filesystem) to the list.
     * This method can be used to attach ascii or binary data,
     * such as a BLOB record from a database.
     * @param string $string String attachment data.
     * @param string $cid Content ID of the attachment.  Use this to identify
     *        the Id for accessing the image in an HTML form.
     * @param string $filename Name of the attachment.
     * @param string $encoding File encoding (see $Encoding).
     * @param string $type File extension (MIME) type.
     * @return void
     *
     * @see http://www.jason-palmer.com/2009/09/phpmailer-inline-string-attachment/
     */
    public function AddInlineStringAttachment($string, $cid, $filename, $encoding = 'base64', $type = 'application/octet-stream') {
        // ----------------------------------------------------------------------------------------
        // Append to $attachment array
        // ----------------------------------------------------------------------------------------
        $this->attachment[] = array(
            0 => $string,
            1 => $filename,
            2 => $filename,
            3 => $encoding,
            4 => $type,
            5 => true, // isStringAttachment
            6 => 'inline',
            7 => $cid
        );
    }
}


// ------------------------------------------------------------------------------------
// PREPA REQUETE - Recherche de l'utilisateur
// ------------------------------------------------------------------------------------
$sqlRechercheUtilisateur = '
SELECT *
FROM du_utilisateur 
	INNER JOIN du_utilisateur_type ON du_utilisateur_type.idTypeUtilisateur = du_utilisateur.FK_idTypeUtilisateur
WHERE mailUtilisateur = :mail';
$RechercheUtilisateurExc = DbConnexion::getInstance ()->prepare ( $sqlRechercheUtilisateur );

// ------------------------------------------------------------------------------------
// PREPA REQUETE - Maj de la cle de dde de mot de passe
// ------------------------------------------------------------------------------------
$sqlModificationPassUtilisateur = '
UPDATE du_utilisateur SET
    tokenReinitMdp = :token,
    boolDdeReinit = 1,
    passUtilisateurReinit = :pass
WHERE idUtilisateur = :idUtilisateur
';
$ModificationPassUtilisateurExc = DbConnexion::getInstance()->prepare($sqlModificationPassUtilisateur);

// ------------------------------------------------------------------------------------
// On verifie les POST
// ------------------------------------------------------------------------------------
if (filter_has_var ( INPUT_POST, 'ztEmailUtilisateur' )) {

    // ------------------------------------------------------------------------------------
    // Recherche de l'utilisateur dans la BDD
    // ------------------------------------------------------------------------------------
    $RechercheUtilisateurExc->bindValue ( ':mail', strtolower ( filter_input(INPUT_POST, 'ztEmailUtilisateur') ), PDO::PARAM_STR );
    $RechercheUtilisateurExc->execute ();

    // ------------------------------------------------------------------------------------
    // Si l'utilisateur a ete trouve
    // ------------------------------------------------------------------------------------
    if ($RechercheUtilisateurExc->rowCount () > 0) {

        // ------------------------------------------------------------------------------------
        // On liste les infos de l'utilisateur et enregistre en session
        // ------------------------------------------------------------------------------------
        $InfoUtilisateur = $RechercheUtilisateurExc->fetch ( PDO::FETCH_OBJ );

        // ------------------------------------------------------------------------------------
        // Token de reinitialisation d'un mdp
        // ------------------------------------------------------------------------------------
        $tokenReinit = hash('sha512', md5(uniqid(mt_rand(), true)));


        // ------------------------------------------------------------------------------------
        // Generation des mots de passes
        // ------------------------------------------------------------------------------------
        $passUtilisateur = generePass(8);

        // ------------------------------------------------------------------------------------
        // Ajout de la cle pour demande de nouveau mot de passe
        // ------------------------------------------------------------------------------------
        $ModificationPassUtilisateurExc->bindValue(':token', $tokenReinit, PDO::PARAM_STR);
        $ModificationPassUtilisateurExc->bindValue(':idUtilisateur', $InfoUtilisateur->idUtilisateur, PDO::PARAM_INT);
        $ModificationPassUtilisateurExc->bindValue(':pass', hash('sha512', $passUtilisateur), PDO::PARAM_STR);
        if($ModificationPassUtilisateurExc->execute()){

            // ------------------------------------------------------------------------------------
            // Donnees necessaire pour l'envoi du MAIL
            // ------------------------------------------------------------------------------------
            $mail = new SUMailer();
            $mail->FromName = "Enregistrement Suppl'ACTIV";
            $mail->AddAddress($InfoUtilisateur->mailUtilisateur);
            #$mail->AddAddress('vbenner@pageup.fr');
            $mail->Subject='Suppl\'ACTIV - Votre demande d\'identifiant';

            // ------------------------------------------------------------------------------------
            // Initialisation du message
            // ------------------------------------------------------------------------------------
            $ContentMess = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
            <html>
            <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
            <title>BHV OSMOSE</title>
            </head>
            <body style="background-color: #FAFAFA; color: #333333; line-height: 1.2; font-size: 14px; font-family: arial; margin: 0px; padding: 0px;" bgcolor="#FAFAFA">
                    <div id="divGeneral" style="background-color: White; width: 620px !important; margin: 50px auto 30px; border: 1px solid #e2e2e2;">
                        <div style="text-align:center;padding:10px">
                            <img src="'.ADRESSE_BO.'/assets/global/img/logo-sidebar.png">
                        </div>
                        <h1 style="font-size: 16px; color: rgb(0, 0, 0); margin: 0px 0px 10px; padding: 20px;">Bonjour '.utf8_decode($InfoUtilisateur->prenomUtilisateur.' '.$InfoUtilisateur->nomUtilisateur).', vous avez demand&eacute; le '.date('d/m/Y - H:i').' de nouveaux identifiants pour la plateforme Suppl\'ACTIV.</h1>
                        <div id="divSep" style="border-bottom-width: 1px; border-bottom-color: #e6e6e6; border-bottom-style: solid;"></div>
                        <div id="divIdentifiant" style="border-radius: 1px; height: 125px; background-color: #e6e6e6; margin: 25px; padding: 10px 15px; border: 1px solid #cccccc;">
                            <p>Identifant :<font style="color: #a5c229 !important; font-weight: bold; float: right; font-size: 22px;">'.$InfoUtilisateur->mailUtilisateur.'</font></p>
                            <p>Mot de passe :<font style="color: #a5c229; font-weight: bold; float: right; font-size: 22px;">'.$passUtilisateur.'</font></p>
                            <p>Si vous n\'avez pas fait de demande de mot de passe merci de ne pas tenir compte de cet e-mail</p>
                        </div>
                        <p id="pLink" style="text-align: center;" align="center"><a href="'.ADRESSE_BO.'/login.php?token='.$tokenReinit.'" style="color: #1a9fdd; text-align: center;">CLIQUEZ-ICI POUR REINITIALISER MES IDENTIFIANTS</a></p>

                    </div>
                    <div id="divFooter" style="text-align: center; color: #5e5e5e; font-size: 11px; padding: 7px 0;" align="center">
                        Envoy&eacute; par Page Up. Ne pas r&eacute;pondre &agrave; cet email.
                    </div>
                </body>
            </html>';

            $mail->Body = $ContentMess;

            $mail->Send();
            echo json_encode ( array (
                'STATUS' => 1
            ) );
        }else {
            echo json_encode ( array (
                'STATUS' => 0
            ) );
        }
    }
    // ------------------------------------------------------------------------------------
    // L'utilisateur n'a pas ete trouve
    // ------------------------------------------------------------------------------------
    else {
        echo json_encode ( array (
            'STATUS' => 0
        ) );
    }
}

// ------------------------------------------------------------------------------------
// Variable POST non presente
// ------------------------------------------------------------------------------------
else {
    echo json_encode ( array (
        'STATUS' => 0
    ) );
}