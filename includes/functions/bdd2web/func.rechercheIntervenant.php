<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des intervenants
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$RechercheIntervenantExc->execute();

print json_encode(array(
    'intervenants' => $RechercheIntervenantExc->fetchAll(PDO::FETCH_OBJ)
));