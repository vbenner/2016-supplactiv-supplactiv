<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des informations sur un client
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

$sqlRechercheInfoSalarie = '
SELECT *
FROM du_utilisateur
WHERE idUtilisateur = :idSalarie
';
$rechercheInfoSalarieExec = DbConnexion::getInstance()->prepare($sqlRechercheInfoSalarie);

/** On test la presence du POST */
if (filter_has_var ( INPUT_POST, 'idSalarie' )) {

    /** Recherche du point de vente */
    $rechercheInfoSalarieExec->bindValue(':idSalarie', filter_input(INPUT_POST, 'idSalarie'), PDO::PARAM_INT);
    $rechercheInfoSalarieExec->execute();

    $sqlRechercheType = 'SELECT * FROM du_utilisateur_type WHERE idTypeUtilisateur != 1 ORDER BY libelleTypeUtilisateur';
    $rechercheTypeExec = DbConnexion::getInstance()->prepare($sqlRechercheType);
    $rechercheTypeExec->execute();

    if($rechercheInfoSalarieExec->rowCount() > 0){
        print json_encode(array(
            'result' => 1,
            'infoSalarie' => $rechercheInfoSalarieExec->fetch(PDO::FETCH_OBJ),
            'types' => $rechercheTypeExec->fetchAll(PDO::FETCH_OBJ),
        ));
    }else {
        print json_encode(array(
            'result' => 0,
            'types' => $rechercheTypeExec->fetchAll(PDO::FETCH_OBJ),
        ));
    }
}