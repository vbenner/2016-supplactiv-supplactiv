<?php
/** -----------------------------------------------------------------------------------------------
 * @author Vincent BENNER
 * @detail Recherche la liste des INTERVENANT
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * On rajoute 2 paramètres
 * idCampagne: idCampagne,
 * idMission: idMission
 */
$sqlGetListOfTrucs = '
SELECT DISTINCT SU.*
FROM su_intervenant SU
';
$clauseCampagne = '';
$clauseMission = '';
if (isset($_POST['idCampagne'])) {
    if ($_POST['idCampagne'] != 0 && $_POST['idCampagne'] != '') {
        $sqlGetListOfTrucs .= '
        INNER JOIN su_intervention SI ON SI.FK_idIntervenant = SU.idIntervenant
        INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
        INNER JOIN su_campagne SC ON SC.idCampagne = SM.FK_idCampagne
        ';
        $clauseCampagne = ' AND SC.idCampagne = :idc ';
    }
}

if (isset($_POST['idMission'])) {
    if (!isset($_POST['idCampagne'])) {
        $sqlGetListOfTrucs .= '
        INNER JOIN su_intervention SI ON SI.FK_idIntervenant = SU.idIntervenant
        INNER JOIN su_mission SM ON SM.idMission = SI.FK_idMission
        ';
    }
    if ($_POST['idMission'] != 0 && $_POST['idMission'] != '') {
        $clauseMission = ' AND SM.idMission = :idm ';
    }
}

$sqlGetListOfTrucs .= '
WHERE idIntervenant != 1
'.$clauseCampagne.$clauseMission;

/** -----------------------------------------------------------------------------------------------
 * On enlève les AUTO
 */
if (isset($_POST['bAUTO'])) {
    if ($_POST['bAUTO'] == 0) {
        $sqlGetListOfTrucs .= ' AND boolAutoEntrepreneuse = \'NON\'';
    }
    else {
        $sqlGetListOfTrucs .= ' AND boolAutoEntrepreneuse = \'OUI\'';
    }
}
$sqlGetListOfTrucs .= '
ORDER BY SU.nomIntervenant
';
$getListOfTrucsExec = DbConnexion::getInstance()->prepare($sqlGetListOfTrucs);
if ($clauseCampagne != '') {
    $getListOfTrucsExec->bindValue(':idc', $_POST['idCampagne'], PDO::PARAM_INT);
}
if ($clauseMission != '') {
    $getListOfTrucsExec->bindValue(':idm', $_POST['idMission'], PDO::PARAM_INT);
}
$getListOfTrucsExec->execute();

print json_encode(array(
    'items' => $getListOfTrucsExec->fetchAll(PDO::FETCH_OBJ),
    'sql' => $sqlGetListOfTrucs
));