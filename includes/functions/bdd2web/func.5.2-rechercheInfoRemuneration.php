<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des differentes remunerations pour cette intervenant
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

#echo '<pre>';
#print_r($_POST);
#echo '</pre>';

/** ON test la presence des POSTS */
if(filter_has_var(INPUT_POST, 'idIntervenant')){
    if (filter_input(INPUT_POST, 'idIntervenant') != 'ALL') {

        DbConnexion::getInstance()->errorInfo();

        $RechercheRemunerationIntervenantSansNumInterventionExc->bindValue(':idIntervenant', filter_input(INPUT_POST, 'idIntervenant'), PDO::PARAM_INT);
        $RechercheRemunerationIntervenantSansNumInterventionExc->execute();

        print json_encode(array(
            'remunerations' => $RechercheRemunerationIntervenantSansNumInterventionExc->fetchAll(PDO::FETCH_OBJ)
        ));
        return;
    }
}

if(filter_has_var(INPUT_POST, 'idIntervention')){

    $RechercheRemunerationIntervenantExc->bindValue(':idIntervention', filter_input(INPUT_POST, 'idIntervention'), PDO::PARAM_INT);
    $RechercheRemunerationIntervenantExc->execute();

    print json_encode(array(
        'remunerations' => $RechercheRemunerationIntervenantExc->fetchAll(PDO::FETCH_OBJ)
    ));
    return;
}