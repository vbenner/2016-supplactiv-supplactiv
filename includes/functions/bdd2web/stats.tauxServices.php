<?php
/** -----------------------------------------------------------------------------------------------
 * Liste des rôles utilsateurs
 *
 * @author Vincent BENNER / Page Up
 * @copyright Page Up 2018 ©
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion à la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** -----------------------------------------------------------------------------------------------
 * Init variables
 */
$arOutput = array();

$from = filter_input(INPUT_POST, 'from');
$from = DateTime::createFromFormat('d/m/Y', $from);
$to = filter_input(INPUT_POST, 'to');
$to = DateTime::createFromFormat('d/m/Y', $to);
$year = $to->format('Y');

/** ----------------------------------------------------------------------------------------------
 * SI LA CAMPAGNE est séléctionnée, on va vérifier la compatibilité des DATES
 */
$idc = filter_input(INPUT_POST, 'campagne');
$idm = filter_input(INPUT_POST, 'mission');
if ($idc != '') {
    /** -----------------------------------------------------
     *
     */
    $sqlGetInfoCampagne = '
    SELECT *
    FROM su_campagne
    WHERE idCampagne = :idc
    ';
    $getInfoCampagneExec = DbConnexion::getInstance()->prepare($sqlGetInfoCampagne);
    $getInfoCampagne = $getInfoCampagneExec->bindValue(':idc', $idc, PDO::PARAM_INT);
    $getInfoCampagneExec->execute();
    $infoCampagne = $getInfoCampagneExec->fetch(PDO::FETCH_OBJ);
    if ($infoCampagne->dateDebut < $from->format('Y-m-d') ||
        $infoCampagne->dateDebut > $to->format('Y-m-d')
    ) {
        print json_encode(
            array(
                'RES' => 0,
                'INFO' => 'Dates incompatibles'
            )
        );
        return;
    }
    $year = substr($infoCampagne->dateDebut, 0, 4);
}
/** -----------------------------------------------------------------------------------------------
 * On récupère la liste des MISSIONS qui sont entre les 2 dates
 * 3 = Animation
 * 2 = Formation
 */
$sqlGetListeMission = '
SELECT DISTINCT(M.idMission), nbInterPrevisionnel, FK_idTypeMission
FROM su_mission M
  INNER JOIN su_intervention S ON S.FK_idMission = M.idMission
  INNER JOIN su_contrat C ON C.idContrat = S.FK_idContrat
WHERE 
  S.dateDebut >= :du 
AND
  S.dateFin <= :au
';

/** -----------------------------------------------------------------------------------------------
 * Si on parle de CAMPAGNE
 */
if ($idc != '') {
    $sqlGetListeMission .= '
    AND M.FK_idCampagne = :idc
    ';
}
if ($idm != '') {
    $sqlGetListeMission .= '
    AND M.idMission = :idm
    ';
}

$getListeMissionExec = DbConnexion::getInstance()->prepare($sqlGetListeMission);
$getListeMissionExec->bindValue(':du', $from->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
$getListeMissionExec->bindValue(':au', $to->format('Y-m-d') . ' 23:59:59', PDO::PARAM_STR);

if ($idc != '') {
    $getListeMissionExec->bindValue(':idc', $idc, PDO::PARAM_INT);
}
if ($idm != '') {
    $getListeMissionExec->bindValue(':idm', $idm, PDO::PARAM_INT);
}
$getListeMissionExec->execute();
#$mission = filter_input(INPUT_POST, 'mission');

/** -----------------------------------------------------------------------------------------------
 * Init
 */
$month = $tx = array();
$tx = array();
for ($i = 1; $i <= 12; $i++) {
    $month[ $i ] = $tx[ $i ] = 0;
}

#echo '<pre>';
#print_r($getListeMissionExec);
#echo '</pre>';

/** -----------------------------------------------------------------------------------------------
 * Cette fois-ci, on fait ligne / ligne
 */
$cumul = 0;
while ($mission = $getListeMissionExec->fetch(PDO::FETCH_OBJ)) {

    $cumul += $mission->nbInterPrevisionnel;

    /** -----------------------------------------------------------------------------------------------
     * SQL : récupère la liste des mission avec pour chaque mois, la différence entre le nombre
     * d'interventions réalisées VS le
     */
    $sqlTauxService = '
    SELECT COUNT(S.idIntervention) AS LeCount,
        DATE_FORMAT(S.dateDebut, \'%m\') AS LeMois
    FROM su_mission M
      INNER JOIN su_intervention S ON S.FK_idMission = M.idMission
      INNER JOIN su_contrat C ON C.idContrat = S.FK_idContrat
    WHERE 
      S.dateDebut >= :du 
    AND
      S.dateFin <= :au
    AND M.idMission = :idm
    GROUP BY LeMois
    ORDER BY LeMois
    ';

    $tauxServiceExec = DbConnexion::getInstance()->prepare($sqlTauxService);
    $tauxServiceExec->bindValue(':du', $from->format('Y-m-d') . ' 00:00:00', PDO::PARAM_STR);
    $tauxServiceExec->bindValue(':au', $to->format('Y-m-d') . ' 23:59:59', PDO::PARAM_STR);
    $tauxServiceExec->bindValue(':idm', $mission->idMission, PDO::PARAM_INT);
    $tauxServiceExec->execute();
    while ($tauxService = $tauxServiceExec->fetch(PDO::FETCH_OBJ)) {
        $month[ (int)$tauxService->LeMois ] = $tauxService->LeCount;
    }

    /** -----------------------------------------------------------------------------------------------
     * Récupération du nombre d'interventions prévisionnel
     */
    $sqlTauxService = '
    SELECT nbInterPrevisionnel
    FROM su_mission M
    WHERE M.idMission = :idm
    ';
    $tauxServiceExec = DbConnexion::getInstance()->prepare($sqlTauxService);
    $tauxServiceExec->bindValue(':idm', $mission->idMission, PDO::PARAM_INT);
    $tauxServiceExec->execute();
    $tauxService = $tauxServiceExec->fetch(PDO::FETCH_OBJ);

    /** -----------------------------------------------------------------------------------------------
     * Si le TX de service est renseigné, on calcule le %
     */
    if ($tauxService->nbInterPrevisionnel != 0) {
        for ($i = 1; $i <= 12; $i++) {
            // tx = (cumul / objectif)
            $sum = 0;
            #echo 'i = '.$i;
            for ($j = 1; $j <= $i; $j++) {
                $sum = $sum += $month[$j];
                #echo 'j = '.$j.'<br/>';
            }
            #echo 'sum = '.$sum.' / '.$tauxService->nbInterPrevisionnel.'<br/>';
            $tx[ $i ] = round(100 * $sum / $tauxService->nbInterPrevisionnel, 2);
            $tx[ $i ] = str_replace('.', ',', $tx[ $i ]) . '%';
        }
    }
}


print json_encode(
    array(
        'RES' => 1,
        'YEAR' => $year,
        'DATA' => $month,
        'PERCENT' => $tx,
        'REF' => $cumul,
    )
);