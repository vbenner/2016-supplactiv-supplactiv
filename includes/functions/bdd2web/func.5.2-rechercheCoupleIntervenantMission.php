<?php
/**
 * @author Kevin MAURICE - Page UP
 * @detail Recherche des intervenants / missions en fonction des dates de recherche
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

/** Initialisation des dates de recherche */
$dateFin = new DateTime('last day of this month');
$_SESSION['dateRechercheD_5_2'] = (isset($_SESSION['dateRechercheD_5_2'])) ? $_SESSION['dateRechercheD_5_2'] : date('01/m/Y');
$_SESSION['dateRechercheF_5_2'] = (isset($_SESSION['dateRechercheF_5_2'])) ? $_SESSION['dateRechercheF_5_2'] : $dateFin->format('d/m/Y');

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_5_2']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_5_2']);

$listeFinal = array(
    'intervenants' => array(),
    'missions' => array()
);

$sqlRechercheCoupleSansContrat = '
SELECT idIntervenant, idMission, nomIntervenant, prenomIntervenant, libelleMission, boolAutoEntrepreneuse
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE FK_idContrat IS NULL AND FK_idIntervenant <> 1 AND dateDebut BETWEEN :dateDebut AND :dateFin';

if(filter_input(INPUT_POST, 'typeRecherche') == 'intervenant' && $_SESSION['idMission_5_2'] != 'ALL'){
    $sqlRechercheCoupleSansContrat .= ' AND idMission = '.$_SESSION['idMission_5_2'].' ';
}

if(filter_input(INPUT_POST, 'typeRecherche') == 'mission' && $_SESSION['idIntervenant_5_2'] != 'ALL'){
    $sqlRechercheCoupleSansContrat .= ' AND idIntervenant = '.$_SESSION['idIntervenant_5_2'].' ';
}

$sqlRechercheCoupleSansContrat .= '
GROUP BY '.((filter_input(INPUT_POST, 'typeRecherche') == 'intervenant') ? 'idIntervenant' : 'idMission').'
ORDER BY '.((filter_input(INPUT_POST, 'typeRecherche') == 'intervenant') ? 'nomIntervenant' : 'libelleMission');
$RechercheCoupleSansContratExc = DbConnexion::getInstance()->prepare($sqlRechercheCoupleSansContrat);


/** Recherche des CONTRATS */
$RechercheCoupleSansContratExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheCoupleSansContratExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheCoupleSansContratExc->execute();
while($InfoCouple = $RechercheCoupleSansContratExc->fetch(PDO::FETCH_OBJ)){

    if(filter_input(INPUT_POST, 'typeRecherche') == 'intervenant') {

        array_push($listeFinal['intervenants'], array(
            'id' => $InfoCouple->idIntervenant,
            'auto' => $InfoCouple->boolAutoEntrepreneuse,
            'libelle' => addCaracToString($InfoCouple->idIntervenant, 5, '0') . ' - ' . $InfoCouple->nomIntervenant . ' ' . $InfoCouple->prenomIntervenant
        ));

    }else {
        array_push($listeFinal['missions'], array(
            'id' => $InfoCouple->idMission,
            'libelle' => strtoupper($InfoCouple->libelleMission)
        ));

    }
}


print json_encode($listeFinal);