<?php

/** Definition du timezone */
date_default_timezone_set('Europe/Paris');

/** Initialisation du JSON de sortie */
$ListeSemaine = array();

/** Initialisation des variables */
$annee = date('Y'); $currentWeek = 0;

/** On parcours les semaines de l'anne */
for($idWeek = 1; $idWeek <= 53; $idWeek++){

    $timestamp_lundi_sem01=mktime(0,0,0,01,01,$annee) - ((date("w",mktime(0,0,0,01,01,$annee)) -2)*24*3600);
    $timestamp = $timestamp_lundi_sem01 + (($idWeek-1)*7*24*3600);

    /** Initialisation des Jours */
    $jour1 = $jour6 = null;

    /** ATTENTION - Supprimer cette condition si vous souhaitez avoir la premiere et derniere
    semaine complete (par defaut je prends uniquement les jours de l'annee demandée) */
    if(!in_array($idWeek, array(1, 53))){
       # $jour1 = new DateTime('@'.$timestamp);
        #$jour6 = new DateTime('@'.($timestamp+ (6*24*3600)));
    }else {
        #$jour1 = ($idWeek == 1) ? new DateTime($annee.'-01-01 00:00:00') : new DateTime('@'.$timestamp);
       # $jour6 = ($idWeek == 53) ? new DateTime($annee.'-12-31 23:59:59') : new DateTime('@'.($timestamp+ (6*24*3600)));
    }

    $jour1 = new DateTime('@'.$timestamp);
    $jour6 = new DateTime('@'.($timestamp+ (6*24*3600)));

    #if($jour1->format('W') > $currentWeek){

        /** On initialise le tableau de la semaine */
        $semaine = array(
            'debut' => $jour1->format('Y-m-d'),
            'fin' => $jour6->format('Y-m-d'),
            'debutF' => $jour1->format('d/m/Y'),
            'finF' => $jour6->format('d/m/Y'),
            'indiceF' => $jour6->format('W - Y'),
            'indice' => $jour6->format('W')
        );

        /** On stock les infos de la semaine dans le tableau final */
        array_push($ListeSemaine, $semaine);
        $currentWeek = $jour1->format('W');
    #}
}

print json_encode($ListeSemaine);