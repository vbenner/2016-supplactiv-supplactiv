<?php
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Genenal Function's
// @detail : 
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Check session validity
// ------------------------------------------------------------------------------------
function checkValiditySession() {
	@session_start ();
	if (isset ( $_SESSION ["session_informations" . PROJECT_NAME] )) {

        /*$token = hash ( 'sha512', (KEY_ID . URL_TYPE . $_SERVER ["SERVER_NAME"] . $_SERVER ['HTTP_USER_AGENT']) . $_SESSION ["session_informations" . PROJECT_NAME] );
		if (strcmp ( $_SESSION ["session_token" . PROJECT_NAME], $token ) == 0) {
			$InfoSession = preg_split ( '/-/', $_SESSION ["session_informations" . PROJECT_NAME] );
			return ($InfoSession [0] + TIME_VALIDITY >= time () and $InfoSession [0] <= time ()) ? true : false;
		} else {
			return false;
		} */

        return true;
	} else {
		return false;
	}
}

// ------------------------------------------------------------------------------------
// Delete special carac
// ------------------------------------------------------------------------------------
function deleteSpecialCarac($str, $charset = 'utf-8') {
	$str = htmlentities ( $str, ENT_NOQUOTES, $charset );
	$str = preg_replace ( '#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
	$str = preg_replace ( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
	$str = preg_replace ( '#&[^;]+;#', '', $str );
	$str = str_replace ( '\'', ' ', $str );
	return trim(strtoupper ( $str ));
}

function dateFrToSql($Dte, $Time){
	return substr($Dte, -4).'-'.substr($Dte, 3, 2).'-'.substr($Dte, 0, 2).' '.$Time;
}

function dateSqlToFr($Dte, $short = false, $hasTime = false, $withTime = false) {
    //return '@'.$Dte;
    if (!$hasTime) {
        return substr($Dte,-2).'/'.substr($Dte, 5, 2).'/'.($short == false ? substr($Dte,0,4) : substr($Dte, 2,2));
    } else {
        return substr($Dte, 8, 2).'/'.substr($Dte, 5, 2).'/'.($short == false ? substr($Dte,0,4) : substr($Dte, 2,2)) . ($withTime ? ' ' . substr($Dte, -8) : '');
    }
}

// ------------------------------------------------------------------------------------
// Fonction permettant de convertir une date au format datetime
// Parametre d'entree  : YYYYMMDDHHIISS
// Parametre de sortie : YYYY-MM-DD HH:II:SS
// ------------------------------------------------------------------------------------
function datePdaToSQL($Dte){
	return substr($Dte,0,4).'-'.substr($Dte,4,2).'-'.substr($Dte,6,2).' '.substr($Dte,8,2).':'.substr($Dte,10,2).':'.substr($Dte,12,2);
}

function logInfo($Fichier, $Date, $Info){
	
	$sqlAjoutLog = 'INSERT INTO du_log(libelleService, dateLog, contenuPost) VALUES(:libelle, :date, :contenu)';
	$AjoutLogExc = DbConnexion::getInstance()->prepare($sqlAjoutLog);
	
	$AjoutLogExc->bindValue(':libelle', $Fichier, PDO::PARAM_STR);
	$AjoutLogExc->bindValue(':date', $Date, PDO::PARAM_STR);
	$AjoutLogExc->bindValue(':contenu', $Info, PDO::PARAM_STR);
	$AjoutLogExc->execute();
}

// ------------------------------------------------------------------------------------
// Fonction permettant de generer un nouveau mot de passe
// ------------------------------------------------------------------------------------
function generationMDP($nb_carMax) {
    $chaine = 'ABCDEFGHIJKLMNOPKRSTUVWWXYZazertyuiopqsdfghjklmwxcvbn123456789';
    $nb_lettres = strlen($chaine) - 1;
    $generation = '';
    for ($i = 0; $i < $nb_carMax; $i++){
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $generation .= $car;
    }
    return $generation;
}

// ------------------------------------------------------------------------------------
// Fonction permettant de convertir une date au format datetime
// Parametre d'entree  : DD/MM/YYYY HH:II
// Parametre de sortie : YYYY-MM-DD HH:II:SS
// ------------------------------------------------------------------------------------
function datePickerToSQL($Dte){
    return substr($Dte, 6, 4).'-'.substr($Dte, 0, 2).'-'.substr($Dte, 3, 2).' '.substr($Dte, -5).':00';
}

function addDataToLog($linkService, $typeService, $contentService, $infoPost, $infoGet){
    $AddDataToLogExc = DbConnexion::getInstance()->prepare('INSERT INTO du_log(lienService, typeService, dateService, contenuService, tailleDonnees, contenuPost, contenuGet) VALUES(:lien, :type, :date, :contenu, :taille, :post, :get)');

    $AddDataToLogExc->bindValue(':lien', $linkService);
    $AddDataToLogExc->bindValue(':type', $typeService);
    $AddDataToLogExc->bindValue(':date', date('Y-m-d H:i:s'));
    $AddDataToLogExc->bindValue(':contenu', $contentService);
    $AddDataToLogExc->bindValue(':taille', strlen($contentService));
    $AddDataToLogExc->bindValue(':post', json_encode($infoPost));
    $AddDataToLogExc->bindValue(':get',  json_encode($infoGet));
    $AddDataToLogExc->execute();
}

function addCaracToString($str, $size, $carac){
    $strFinal = $str;
    while(strlen($strFinal) < $size) $strFinal = $carac.$strFinal;

    return $strFinal;
}

function formatTel($tel){
    $telFinal = str_replace(' ', '', trim($tel));
    $telFinal = str_replace('+33', '0', $telFinal);
    $telFinal = str_replace('.', '', $telFinal);
    $telFinal = str_replace('-', '', $telFinal);
    while(strlen($telFinal) < 10) $telFinal = '0'.$telFinal;

    return ($telFinal != '0000000000') ? substr($telFinal, 0, 2).'-'.substr($telFinal, 2, 2).'-'.substr($telFinal, 4, 2).'-'.substr($telFinal, 6, 2).'-'.substr($telFinal, 8, 2) : '';
}

function formatMinuteTohhHmm($nbMinute){
    $infoMinute = array();
    $infoMinute['H'] = (($nbMinute-($nbMinute%60))/60);
    $infoMinute['M'] = $nbMinute%60;
    while(strlen($infoMinute['H']) < 5) $infoMinute['H'] = '&nbsp;'.$infoMinute['H'];
    while(strlen($infoMinute['M']) < 2) $infoMinute['M'] = '0'.$infoMinute['M'];
    return (object)$infoMinute;
}

class DateTimeFrench extends DateTime
{
    public function format($format)
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Décember');
        $french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, parent::format($format)));
    }
}

function formatZToEmpty($str){
    return (round($str) > 0) ? $str : '';
}

# ------------------------------------------------------------------------------
# Retourne un MDP BO
# ------------------------------------------------------------------------------
function generePass($nb_carMax){
    $chaine = 'ABCDEFGHIJKLMNOPKRSTUVWWXYZazertyuiopqsdfghjklmwxcvbn123456789';
    $nb_lettres = strlen($chaine) - 1;
    $generation = '';
    for($i=0; $i < $nb_carMax; $i++){
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $generation .= $car;
    }
    return $generation;
}

function formatSecToMin($nbSec){
    $nbHeure = ($nbSec-($nbSec%3600))/3600;
    $nbHeure = str_pad($nbHeure, 2, '0', STR_PAD_LEFT);

    $nbMin = ($nbSec%3600)/60;
    $nbMin = str_pad($nbMin, 2, '0', STR_PAD_LEFT);

    return $nbHeure.'h'.$nbMin;
}

function afficheHeureEssai($nbMin){
    $Minutes = intval(($nbMin)/5);
    $Reste = $Minutes%60;
    $HeureEssai = ($Minutes - $Reste)/60;
    if($Reste < 15) {
        $Reste = '';
    }else if($Reste < 24) {
        $Reste = ' 15 Minutes.';
    }else if($Reste < 37) {
        $Reste = ' 30 Minutes.';
    }else if($Reste < 52) {
        $Reste = ' 45 Minutes.';
    }else{
        $HeureEssai++;
        $Reste = '';
    }

    $HeureEssai = ($HeureEssai*60)+$Reste;

    return ceil($HeureEssai/420);
}


function afficheHeureEssaiContrat($Minutes){
    $Minutes = intval(($Minutes)/5);
    $Reste = $Minutes%60;
    $HeureEssai = ($Minutes - $Reste)/60;
    if($Reste < 15)
    {
        $Reste = '';
    }else if($Reste < 24)
    {
        $Reste = ' 15 Minutes.';
    }else if($Reste < 37)
    {
        $Reste = ' 30 Minutes.';
    }else if($Reste < 52)
    {
        $Reste = ' 45 Minutes.';
    }else{
        $HeureEssai++;
        $Reste = '';
    }
    $HeureEssai .= ' Heure(s)'.$Reste;

    return $HeureEssai;
}

function forcerTelechargement($nom, $situation, $poids){
    header('Content-Type: application/octet-stream');
    //header('Content-Length: '. $poids);
    header('Content-disposition: attachment; filename='.$nom);
    header('Pragma: no-cache');
    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    header('Expires: 0');
    readfile($situation);
    exit();
}