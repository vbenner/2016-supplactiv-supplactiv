<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Ajout d'un administrateur
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$sqlRechercheAdministrateurAgence = '
SELECT du_utilisateur.*, du_utilisateur_type.libelleTypeUtilisateur
FROM du_utilisateur
  INNER JOIN du_utilisateur_type ON du_utilisateur_type.idTypeUtilisateur = du_utilisateur.FK_idTypeUtilisateur
WHERE boolActif = 1
';
$RechercheAdministrateurAgenceExc = DbConnexion::getInstance()->prepare($sqlRechercheAdministrateurAgence);

$arOutput = array('aaData' => array());

$RechercheAdministrateurAgenceExc->execute();
while($InfoAdministrateur = $RechercheAdministrateurAgenceExc->fetch(PDO::FETCH_OBJ)){
    if($_SESSION ['idUtilisateur'.PROJECT_NAME] != $InfoAdministrateur->idUtilisateur)
    $arOutput['aaData'][] = array(
        $InfoAdministrateur->nomUtilisateur,
        $InfoAdministrateur->prenomUtilisateur,
        $InfoAdministrateur->mailUtilisateur,
        $InfoAdministrateur->libelleTypeUtilisateur,
        '<button class="btn btn-xs bg bg-red btn-block btn-delete" data-id="'.$InfoAdministrateur->idUtilisateur.'"><i class="fa fa-trash-o"></i> Supprimer</button>'
    );
}

print json_encode($arOutput);