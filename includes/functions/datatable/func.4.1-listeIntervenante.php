<?php
ini_set('display_errors','on');

/**
 * Liste des intervenants
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requete sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';



/** Initialisation de la variable de sortie */
$arOutput = array();

$ListContrat = array();
$RechercheNbContratIntervenantExc->execute();
while($InfoContrat = $RechercheNbContratIntervenantExc->fetch(PDO::FETCH_OBJ)){
    $ListContrat[$InfoContrat->idIntervenant] = $InfoContrat;
}

$LstCompetence = array();
$RechercheCompetenceIntervenantExc->execute();
while($InfoCompetence = $RechercheCompetenceIntervenantExc->fetch(PDO::FETCH_OBJ)){
    $LstCompetence[$InfoCompetence->FK_idIntervenant][$InfoCompetence->FK_idCompetence] = true;
}

$LstQualification = array();
$RechercheQualificationIntervenantExc->execute();
while($InfoQualification = $RechercheQualificationIntervenantExc->fetch(PDO::FETCH_OBJ)) {
    $LstQualification[$InfoQualification->FK_idIntervenant][$InfoQualification->FK_idQualification] = true;
}

$LstExperience = array();
$RechercheExperienceIntervenantExc->execute();
while($InfoExperience = $RechercheExperienceIntervenantExc->fetch(PDO::FETCH_OBJ)){
    $LstExperience[$InfoExperience->FK_idIntervenant][$InfoExperience->FK_idExperience] = true;
}

/** Recherche des intervenants */
$arOutput = array('aaData' => array () );
$RechercheListeIntervenantExc->execute();
while($InfoIntervenant = $RechercheListeIntervenantExc->fetch(PDO::FETCH_OBJ)){

    $boolAffiche = true;

    if(isset($_SESSION['lstCompetence_3_1']) && is_array($_SESSION['lstCompetence_3_1'])){
        foreach($_SESSION['lstCompetence_3_1'] as $id=>$idCompetence){
           $boolAffiche = (!isset($LstCompetence[$InfoIntervenant->idIntervenant][$idCompetence])) ? false : $boolAffiche;
        }
    }

    if(isset($_SESSION['lstExperience_3_1']) &&  is_array($_SESSION['lstExperience_3_1'])){
        foreach($_SESSION['lstExperience_3_1'] as $id=>$idExperience){
            $boolAffiche = (!isset($LstExperience[$InfoIntervenant->idIntervenant][$idExperience])) ? false : $boolAffiche;
        }
    }

    if(isset($_SESSION['lstQualification_3_1']) &&  is_array($_SESSION['lstQualification_3_1'])){
        foreach($_SESSION['lstQualification_3_1'] as $id=>$idQualification){
            $boolAffiche = (!isset($LstQualification[$InfoIntervenant->idIntervenant][$idQualification])) ? false : $boolAffiche;
        }
    }

    /** -------------------------------------------------------------------------------------------
     * Si il y a des NIVEAUX dans la session, on va filtrer...
     */
    if(isset($_SESSION['lstNiveau_3_1']) &&  is_array($_SESSION['lstNiveau_3_1'])){
        if ( !in_array($InfoIntervenant->FK_idNiveau, $_SESSION['lstNiveau_3_1'])) {
            $boolAffiche = false;
        } else {

        }
    }

    if($boolAffiche) {
        /** @var $ListeDept Liste des departements d'intervention */
        $ListeDept = '';
        $RechercheDepartementIntervenantExc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
        $RechercheDepartementIntervenantExc->execute();
        while ($InfoDept = $RechercheDepartementIntervenantExc->fetch(PDO::FETCH_OBJ)) {
            $ListeDept .= '<span class="badge badge-info badge-roundless">D' . $InfoDept->idDepartement . '</span>&nbsp;&nbsp;';
        }

        /** @var $nbMinute Nombre de minute d'intervention des 4 derniers mois */
        $nbMinute = 0;
        $RechercheMinuteInterventionExc->bindValue(':idIntervenant', $InfoIntervenant->idIntervenant, PDO::PARAM_INT);
        $RechercheMinuteInterventionExc->execute();
        if ($RechercheMinuteInterventionExc->rowCount() > 0) {
            while ($InfoMinute = $RechercheMinuteInterventionExc->fetch(PDO::FETCH_OBJ)) {
                $nbMinute += ($InfoMinute->nbMinute > 420) ? 420 : $InfoMinute->nbMinute;
            }
        }

        /** Recherche de la presence de contrat */
        $boolContrat = (isset($ListContrat[$InfoIntervenant->idIntervenant])) ? true : false;


        $infoMinute = formatMinuteTohhHmm($nbMinute);

        $tdClass = '';
        if ($boolContrat) {
            $InfoContrat = $ListContrat[$InfoIntervenant->idIntervenant];
            if ($InfoContrat->nbCDD == 0 && $InfoContrat->nbCIDD == 0) {
                $tdClass = 'text-primary';
            }

            if ($InfoIntervenant->boolAutoEntrepreneuse == 'OUI') {
                $tdClass = 'text-danger';
            }
        }

        if ($InfoIntervenant->b64_photo != '') {
            $fic = '../../../download/id.'.$InfoIntervenant->idIntervenant;
            $list = glob($fic.'.*');
            $photo = '<a class="fancy" class="iframe" href="'.substr($list[0], 9).'"><i class="fa fa-user"></i></a>';
        } else {
            $photo = '';
        }

        if ($InfoIntervenant->b64_cv != '') {
            $fic = '../../../download/CV/cv.'.$InfoIntervenant->idIntervenant;
            $list = glob($fic.'.*');

            #echo '----------------'."</br>";
            #print_r ($list);
            #echo '----------------'."</br>";

            $cv = '<a class="fancy" class="iframe" href="'.substr($list[0], 9).'"><i class="fa fa-file-word-o"></i></a>';
        } else {
            $cv = '';
        }

        /** ---------------------------------------------------------------------------------------
         *
         */
        $ajoute = false;
        if (($_SESSION['lstAuto_3_1'] == 1 && $InfoIntervenant->boolAutoEntrepreneuse != 'NON') ||
            ($_SESSION['lstAuto_3_1'] == 0 && $InfoIntervenant->boolAutoEntrepreneuse == 'NON') ||
            ($_SESSION['lstAuto_3_1'] == 'ALL')
        ) {

            if ($_SESSION['lstMap_3_1'] == 'ALL') {
                $ajoute = true;
            }
            else {
                if ($_SESSION['lstMap_3_1'] == 0 && ($InfoIntervenant->latitude == null || $InfoIntervenant->latitude == 0)) {
                    $ajoute = true;
                } elseif ($_SESSION['lstMap_3_1'] != 0 && ($InfoIntervenant->latitude != null && $InfoIntervenant->latitude != 0)) {
                    $ajoute = true;
                }
            }
            if ($ajoute) {
                $map = ($InfoIntervenant->latitude == null || $InfoIntervenant->latitude == 0.0) ? '' : '<i class="icon-map" style="color: #953b39"></i>';
                $arOutput['aaData'][] = array(
                    'DT_RowClass' => $tdClass,
                    '<button class="btn bg '.
                        (
                            $InfoIntervenant->boolActif ? 'bg-green' :
                                (
                                    $InfoIntervenant->boolCLOS ? 'bg-red-sunglo' : 'bg-yellow'
                                )
                        )
                    .' btn-xs no-margin" data-id="' . $InfoIntervenant->idIntervenant . '"><i class="icon-book-open"></i> ' . addCaracToString($InfoIntervenant->idIntervenant, 7, '0') . '</button>',
                    $InfoIntervenant->nomIntervenant.'  '.$InfoIntervenant->prenomIntervenant.($photo != '' ? '&nbsp;'.$photo : '').($cv != '' ? '&nbsp;'.$cv : ''),
                    $InfoIntervenant->codePostalIntervenant.'&nbsp;'.$map,
                    $InfoIntervenant->villeIntervenant,
                    '<a href="mailto:'.($InfoIntervenant->mailIntervenant).'">'.$InfoIntervenant->mailIntervenant.'</a>',
                    formatTel($InfoIntervenant->mobileIntervenant),
                    $ListeDept,
                    (($nbMinute > 0) ? $infoMinute->H . '.' . $infoMinute->M : ''),
                    $InfoIntervenant->boolAutoEntrepreneuse,
                    $InfoIntervenant->boolActif,
                    $InfoIntervenant->boolCLOS,
                    (($InfoContrat->nbCDD == 0 && $InfoContrat->nbCIDD == 0 && $InfoIntervenant->idIntervenant <> 1 && $nbMinute == 0)) ? '<button class="btn bg-red-thunderbird btn-xs no-margin" data-id="'.$InfoIntervenant->idIntervenant.'" data-name="'.$InfoIntervenant->prenomIntervenant.' '.$InfoIntervenant->nomIntervenant.'"><i class="icon-trash"></i> Supprimer</button>' : ''
                );
            }
        }
    }

}


print json_encode($arOutput);
