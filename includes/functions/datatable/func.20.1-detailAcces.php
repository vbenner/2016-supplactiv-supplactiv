<?php

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array(), 'nom' => '', 'prenom' => '');

/** -----------------------------------------------------------------------------------------------
 * Il y a forcément des données de SESSION
 */
$idUser = $_POST['id'];
$du = date_create_from_format('d/m/Y', $_SESSION['dateRechercheD_20_1']);
$au = date_create_from_format('d/m/Y', $_SESSION['dateRechercheF_20_1']);

/** -----------------------------------------------------------------------------------------------
 * On crée une requête pour savoir qui s'est connecté entre ce laps de dates
 * (hors root)
 */
$sqlGetListeConnexions = '
SELECT DUC.*, DU.nomUtilisateur, DU.prenomUtilisateur
FROM du_utilisateur_connexion DUC
	INNER JOIN du_utilisateur DU ON DU.idUtilisateur = DUC.FK_idUtilisateur 
WHERE 
(
  lastConnexion >= :datedu
  AND lastConnexion <= :dateau
)
AND DUC.FK_idUtilisateur = :idu
ORDER BY DUC.lastConnexion DESC
';

$getListeConnexionsExec = DbConnexion::getInstance()->prepare($sqlGetListeConnexions);
$getListeConnexionsExec->bindValue(':datedu', $du->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$getListeConnexionsExec->bindValue(':dateau', $au->format('Y-m-d').' 23:59:59', PDO::PARAM_STR);
$getListeConnexionsExec->bindValue(':idu', $idUser, PDO::PARAM_INT);
$getListeConnexionsExec->execute();

while($getListeConnexions = $getListeConnexionsExec->fetch(PDO::FETCH_OBJ)) {

    /** -------------------------------------------------------------------------------------------
     * On va vérifier que, dans cette plage de date, il n'y a pas d'accès un jour de WEEK END ?
     */

    $arOutput['nom'] = $getListeConnexions->nomUtilisateur;
    $arOutput['prenom'] = $getListeConnexions->prenomUtilisateur;
    $date = date_create_from_format('Y-m-d H:i:s', $getListeConnexions->lastConnexion);

    $arOutput['aaData'][] = array(
        $date->format('d/m/Y'),
        $date->format('H:i:s'),
        $getListeConnexions->nomUtilisateur.' '.$getListeConnexions->prenomUtilisateur,
        $getListeConnexions->minTime,
        $getListeConnexions->maxTime,
        $getListeConnexions->weekend,
        (
        $getListeConnexions->weekend > 0 ||
        $getListeConnexions->minTime <= '08:00:00' ||
        $getListeConnexions->maxTime >= '18:30:00' ? 'OUI' : ''
        ),
        '<a class="btn btn-xs btn-primary no-margin btn-block" data-id="'.$getListeConnexions->idu.'">
        <i class="icon-folder-alt"></i> DÉTAIL</a>'
    );
}

/** -----------------------------------------------------------------------------------------------
 * On va maintenant récupérer la liste des
 */
print json_encode($arOutput);
