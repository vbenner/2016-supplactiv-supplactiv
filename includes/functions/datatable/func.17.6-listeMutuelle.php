<?php
/**
 * Recherche du registre du personnel
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

set_time_limit(0);
// -------------------------------------------------------------------------------
// Connexion a la base de donnees
// -------------------------------------------------------------------------------
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

// -------------------------------------------------------------------------------
// Requete SQL
// -------------------------------------------------------------------------------
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';
require_once dirname(__FILE__) . '/../../classes/class.carbon.php';

// -------------------------------------------------------------------------------
// Librairie PHP Excel
// -------------------------------------------------------------------------------
require_once dirname(__FILE__) . '/../../librairies/phpexcel/PHPExcel.php';

// -------------------------------------------------------------------------------
// Initialisation de la variable de sortie 
// -------------------------------------------------------------------------------
$arOutput = array('aaData' => array());

// -------------------------------------------------------------------------------
// Initialisation des dates de recherche
// -------------------------------------------------------------------------------
$DT = new DateTimeFrench(date('Y-m-d H:i:s'));
if ($DT->format('j') <= 9) {
    $DT->sub(new DateInterval('P1M'));
}


$fisrtDay = new DateTime($DT->format('Y-m-01') . ' 00:00:00');
$lastDay = new DateTime($fisrtDay->format('Y-m-d') . ' 00:00:00');
$lastDay->modify('last day of this month');

// -------------------------------------------------------------------------------
// On peut passer des dates de recherche en GET (DDMMYYYY)
// -------------------------------------------------------------------------------
if (filter_has_var(INPUT_GET, 'begin') && filter_has_var(INPUT_GET, 'end')) {
    $fisrtDay = \Carbon\Carbon::createFromFormat('dmY', filter_input(INPUT_GET, 'begin'));
    $lastDay = \Carbon\Carbon::createFromFormat('dmY', filter_input(INPUT_GET, 'end'));
}

// -------------------------------------------------------------------------------
// PREPA REQUETE - Recherche des interventions
// -------------------------------------------------------------------------------
$sqlRechercheInterventionMutuelleAvecContrat = '
SELECT *
FROM su_intervention
WHERE FK_idContrat IS NOT NULL AND dateDebut >= :begin AND dateFin <= :end
AND FK_idIntervenant = :idinter
ORDER BY FK_idIntervenant, dateDebut';
$RechercheInterventionMutuelleAvecContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionMutuelleAvecContrat);

// -------------------------------------------------------------------------------
// PREPA REQUETE - Recherche des intervenants
// -------------------------------------------------------------------------------
$sqlRechercheInfoIntervenant = '
SELECT *
FROM su_intervenant
WHERE boolAutoEntrepreneuse = "NON"
';
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);

// -------------------------------------------------------------------------------
// Initialisation du tableau contenant les intervenants
// -------------------------------------------------------------------------------
$intervenants = array();

$content_excel = array();

// -------------------------------------------------------------------------------
// Recherche des intervenants
// -------------------------------------------------------------------------------
$RechercheIntervenantExc->execute();
while ($infoIntervenant = $RechercheIntervenantExc->fetch(PDO::FETCH_OBJ)) {
    $intervenants[$infoIntervenant->idIntervenant] = $infoIntervenant;
}

// -------------------------------------------------------------------------------
// Recherche des interventions
// -------------------------------------------------------------------------------
foreach ($intervenants as $key => $val) {
    $RechercheInterventionMutuelleAvecContratExc->bindValue(':begin', $fisrtDay->format('Y-m-01') . ' 00:00:00');
    $RechercheInterventionMutuelleAvecContratExc->bindValue(':end', $lastDay->format('Y-m-d') . ' 23:59:59');
    $RechercheInterventionMutuelleAvecContratExc->bindValue(':idinter', $key);

    $RechercheInterventionMutuelleAvecContratExc->execute();

    while ($infoIntervention = $RechercheInterventionMutuelleAvecContratExc->fetch(PDO::FETCH_OBJ)) {

        $infoIntervenant = $intervenants[$infoIntervention->FK_idIntervenant];

        if (!isset($content_excel[$infoIntervenant->idIntervenant])) {
            $content_excel[$infoIntervenant->idIntervenant] = array(
                'id' => $infoIntervenant->idIntervenant,
                'nom' => $infoIntervenant->nomIntervenant . ' ' . $infoIntervenant->prenomIntervenant .
                    ($infoIntervenant->boolAutoEntreneuse == 'OUI' ? '<span class="label label-danger"> O </span>' : ''),
                'periodes' => array(),
                'interventions' => array()
            );
        }

        if (!isset($content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')])) {
            $content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')] = array(
                'debut' => $infoIntervention->dateDebut,
                'fin' => $infoIntervention->dateFin
            );


        } else {

            $datelast = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')]['fin']);
            $datemax = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateFin) > $datelast ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateFin) : $datelast;
            $content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')]['fin'] = $datemax->format('Y-m-d H:i:s');

        }

        if (!isset($content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')]['nbheure'])) {
            $content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')]['nbheure'] = 0;
        }

        $content_excel[$infoIntervenant->idIntervenant]['interventions'][\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut)->format('Ymd')]['nbheure'] += \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateFin)->diffInMinutes(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $infoIntervention->dateDebut));

    }
}
// -------------------------------------------------------------------------------
// On parcourt les dates d'intervention pour determiner les periodes
// -------------------------------------------------------------------------------
foreach ($content_excel as $intervenant_id => $intervenant) {
    foreach ($intervenant['interventions'] as $intervention_dte => $intervention) {
        $begindate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $intervention['debut']);
        $enddate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $intervention['fin']);

        $boolPeriode = false;
        foreach ($content_excel[$intervenant_id]['periodes'] as $periode) {
            if (\Carbon\Carbon::createFromFormat('Ymd', $periode['enddate'])->addDay()->format('Ymd') == $enddate->format('Ymd')) {
                $content_excel[$intervenant_id]['periodes'][$periode['begindate']]['enddate'] = $begindate->format('Ymd');
                $content_excel[$intervenant_id]['interventions'][$intervention_dte]['periode'] = $content_excel[$intervenant_id]['periodes'][$periode['begindate']]['begindate'];
                $boolPeriode = true;
            }
        }

        if (!$boolPeriode) {
            $content_excel[$intervenant_id]['periodes'][$begindate->format('Ymd')] = array(
                'begindate' => $begindate->format('Ymd'),
                'enddate' => $enddate->format('Ymd')
            );

            $content_excel[$intervenant_id]['interventions'][$intervention_dte]['periode'] = $begindate->format('Ymd');
        }
    }
}

// -------------------------------------------------------------------------------
// On cree le contenu du fichier EXCEL FINAL
// -------------------------------------------------------------------------------
$i=1;
$content_excel_final = array();
foreach ($content_excel as $intervenant_id => $intervenant) {

    $infoIntervenant = $intervenants[$intervenant_id];
    foreach ($intervenant['interventions'] as $intervention) {

        /*
        if ($i == 208) {
            echo '----'.$i."\n";
            echo '>'.$intervenant_id."\r";
            $infoIntervenant = $intervenants[$intervenant_id] ;

            print_r($infoIntervenant);
            echo '++++++++'."\r";
            print_r($intervenant);
            echo '--------------------'."\r";
            $line_excel = new \StdClass();
            $line_excel->siren = "528 147 150";
            $line_excel->numeroSS = $infoIntervenant->numeroSS;
            $line_excel->codeSS = substr($infoIntervenant->numeroSS, -2);
            $line_excel->typeContrat = 'CIDD';
            $line_excel->nom = $infoIntervenant->nomIntervenant;
            $line_excel->prenom = $infoIntervenant->prenomIntervenant;
            $line_excel->dateNaissance = \Carbon\Carbon::createFromFormat('Y-m-d', $infoIntervenant->dateNaissance)->format('d/m/Y');
            $line_excel->statut = 'Non Cadre';
            $line_excel->debutCIDD = \Carbon\Carbon::createFromFormat('Ymd', $intervenant['periodes'][$intervention['periode']]['begindate'])->format('d/m/Y');
            $line_excel->finCIDD = \Carbon\Carbon::createFromFormat('Ymd', $intervenant['periodes'][$intervention['periode']]['enddate'])->format('d/m/Y');
            echo '#######'."\r";
            die();
        } else {
            $i++;
        }

        #echo '$i = '.$i."\r";
        #$i++;
        */
        $line_excel = new \StdClass();
        $line_excel->siren = "528 147 150";
        $line_excel->numeroSS = $infoIntervenant->numeroSS;
        $line_excel->codeSS = substr($infoIntervenant->numeroSS, -2);
        $line_excel->typeContrat = 'CIDD';
        $line_excel->nom = $infoIntervenant->nomIntervenant;
        $line_excel->prenom = $infoIntervenant->prenomIntervenant;
        $line_excel->dateNaissance = \Carbon\Carbon::createFromFormat('Y-m-d', $infoIntervenant->dateNaissance)->format('d/m/Y');

        $line_excel->statut = 'Non Cadre';
        $line_excel->debutCIDD = \Carbon\Carbon::createFromFormat('Ymd', $intervenant['periodes'][$intervention['periode']]['begindate'])->format('d/m/Y');
        $line_excel->finCIDD = \Carbon\Carbon::createFromFormat('Ymd', $intervenant['periodes'][$intervention['periode']]['enddate'])->format('d/m/Y');

        $line_excel->adresse = $infoIntervenant->adresseIntervenant_A;
        $line_excel->adresseComplement = $infoIntervenant->adresseIntervenant_B;
        $line_excel->commune = $infoIntervenant->villeIntervenant;
        $line_excel->codepostal = $infoIntervenant->codePostalIntervenant;
        $line_excel->bureau = 'Dijon';
        $line_excel->codeISO = substr($infoIntervenant->numeroIBAN, 0, 2);
        $line_excel->cleIBAN = substr($infoIntervenant->numeroIBAN, 2, 2);
        $line_excel->IBAN = substr($infoIntervenant->numeroIBAN, 4);
        $line_excel->BIC = $infoIntervenant->codeBIC;
        $line_excel->banque = $infoIntervenant->libelleBanque;
        $line_excel->dateTravail = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $intervention['debut'])->format('d/m/Y');
        $line_excel->nbHeure = $intervention['nbheure'];
        $line_excel->debut = $intervention['debut'];
        $line_excel->fin = $intervention['fin'];
        $content_excel_final[] = $line_excel;

    }
}

/** @var $objPHPExcel Objet PHPExcel */
$objPHPExcel = new PHPExcel();

/** Initialisation du document */
$objPHPExcel->getProperties()->setCreator("Suppl'ACTIV")
    ->setLastModifiedBy("Suppl'ACTIV")
    ->setTitle("MUTUELLE")
    ->setSubject("MUTUELLE")
    ->setKeywords("MUTUELLE")
    ->setCategory("MUTUELLE");

/** Creation des colonnes */
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Siren de l\'entreprise')
    ->setCellValue('B1', 'N° Sécurité Sociale salarié')
    ->setCellValue('C1', 'Clé N° Sécurité Sociale salarié')
    ->setCellValue('D1', 'Type de contrat')
    ->setCellValue('E1', 'Nom bénéficiaire')
    ->setCellValue('F1', 'Prénom bénéficiaire')
    ->setCellValue('G1', 'date naissance')
    ->setCellValue('H1', 'Statut')
    ->setCellValue('I1', 'Date début CIDD')
    ->setCellValue('J1', 'Date de fin de CIDD')
    ->setCellValue('K1', 'Rue')
    ->setCellValue('L1', 'Complément Rue')
    ->setCellValue('M1', 'Commune')
    ->setCellValue('N1', 'Code Postal')
    ->setCellValue('O1', 'Bureau distributeur')
    ->setCellValue('P1', 'Code pays ISO')
    ->setCellValue('Q1', 'Clé IBAN')
    ->setCellValue('R1', 'IBAN')
    ->setCellValue('S1', 'BIC')
    ->setCellValue('T1', 'Domiciliation bancaire')
    ->setCellValue('U1', 'Date du jour travaillé')
    ->setCellValue('V1', 'NB heure travaillées dans la journée concernée');


$numeroLigne = 2;

foreach ($content_excel_final as $content) {

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $numeroLigne, $content->siren)
        ->setCellValue('B' . $numeroLigne, $content->numeroSS)
        ->setCellValue('C' . $numeroLigne, $content->codeSS)
        ->setCellValue('D' . $numeroLigne, $content->typeContrat)
        ->setCellValue('E' . $numeroLigne, $content->nom)
        ->setCellValue('F' . $numeroLigne, $content->prenom)
        ->setCellValue('G' . $numeroLigne, $content->dateNaissance)
        ->setCellValue('H' . $numeroLigne, $content->statut)
        ->setCellValue('I' . $numeroLigne, $content->debutCIDD)
        ->setCellValue('J' . $numeroLigne, $content->finCIDD)
        ->setCellValue('K' . $numeroLigne, $content->adresse)
        ->setCellValue('L' . $numeroLigne, $content->adresseComplement)
        ->setCellValue('M' . $numeroLigne, $content->commune)
        ->setCellValue('N' . $numeroLigne, ' ' . $content->codepostal)
        ->setCellValue('O' . $numeroLigne, $content->bureau)
        ->setCellValue('P' . $numeroLigne, ' ' . $content->codeISO)
        ->setCellValue('Q' . $numeroLigne, ' ' . $content->cleIBAN)
        ->setCellValue('R' . $numeroLigne, ' ' . $content->IBAN)
        ->setCellValue('S' . $numeroLigne, ' ' . $content->BIC)
        ->setCellValue('T' . $numeroLigne, $content->banque)
        ->setCellValue('U' . $numeroLigne, $content->dateTravail)
        ->setCellValue('V' . $numeroLigne, ($content->nbHeure >= 480) ? round((($content->nbHeure - 60) / 60), 2) : round($content->nbHeure / 60, 2));


    $numeroLigne++;

    $arOutput['aaData'][] = array(
        $content->numeroSS,
        $content->nom . ' ' . $content->prenom,
        $content->dateNaissance,
        $content->dateTravail,
        ($content->nbHeure >= 480) ? round((($content->nbHeure - 60) / 60), 2) : round($content->nbHeure / 60, 2),
        $content->IBAN,
        $content->BIC,
        $content->banque
    );
}

// -------------------------------------------------------------------------------
// Fichier DATATABLE
// -------------------------------------------------------------------------------
if (!filter_has_var(INPUT_GET, 'datatable') && !filter_has_var(INPUT_GET, 'detail')) {
    $objPHPExcel->getActiveSheet()->setAutoFilter('A1:V' . $numeroLigne);
    /** On renomme la feuille */
    $objPHPExcel->getActiveSheet()->setTitle('MUTUELLE');

    /** On active la feuille par defaut */
    $objPHPExcel->setActiveSheetIndex(0);

    /** On force le telechargement du fichier */
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="MUTUELLE-Intervenant.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}

// -------------------------------------------------------------------------------
// DEBUG
// -------------------------------------------------------------------------------
else if (filter_has_var(INPUT_GET, 'detail')) {
    print json_encode($content_excel);
}

// -------------------------------------------------------------------------------
// DATATABLE
// -------------------------------------------------------------------------------
else {
    print json_encode($arOutput);
}