<?php
/**
 * Liste des contrats
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/**
 * Recherche des contrats (suivant filtre)
 *
 * @author Kevin MAURICE - Page UP
 * @var     $RechercheListeContratExc
 */
$sqlRechercheListeContrat = '
SELECT *
FROM v_liste_contrat
	INNER JOIN v_liste_contrat_date ON v_liste_contrat_date.FK_idContrat = v_liste_contrat.idContrat
	LEFT JOIN su_contrat_retour ON su_contrat_retour.FK_idContrat = v_liste_contrat.idContrat
WHERE dateDebut BETWEEN :dateDebut AND :dateFin AND boolContratArchive = :boolArchive ';

if ($_SESSION['boolAuto_5_1'] != 'ALL') {
$sqlRechercheListeContrat.= '    
    AND boolAutoEntrepreneur = :boolAuto
';
}

#echo $sqlRechercheListeContrat;

/** Filtre sur l'id du contrat */
if(isset($_SESSION['idContrat_5_1']) && $_SESSION['idContrat_5_1'] != 'ALL'){
    $sqlRechercheListeContrat .= ' AND idContrat = '.$_SESSION['idContrat_5_1'];
}

/** Filtre sur l'id de l'intervenant */
if(isset($_SESSION['idIntervenant_5_1']) && $_SESSION['idIntervenant_5_1'] != 'ALL'){
    $sqlRechercheListeContrat .= ' AND idIntervenant = '.$_SESSION['idIntervenant_5_1'];
}

$sqlRechercheListeContrat .= ' GROUP BY idContrat';
$RechercheListeContratExc = DbConnexion::getInstance()->prepare($sqlRechercheListeContrat);

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_5_1']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_5_1']);

$sqlArchiveContratM6 = '
UPDATE su_contrat SET
	boolContratArchive = 1
WHERE dateCreationContrat < (DATE_SUB(CURDATE(),INTERVAL 180 DAY)) ';
$ArchiveContratM6Exc = DbConnexion::getInstance()->prepare($sqlArchiveContratM6);

$ArchiveContratM6Exc->execute();


/** Recherche des contrats suivant les filtres de l'utilisateur */
$RechercheListeContratExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheListeContratExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheListeContratExc->bindValue(':boolArchive', $_SESSION['boolArchive_5_1'], PDO::PARAM_INT);

if ($_SESSION['boolAuto_5_1'] != 'ALL') {
    $RechercheListeContratExc->bindValue(':boolAuto', ($_SESSION[ 'boolAuto_5_1' ] == 1 ? 'OUI' : 'NON'), PDO::PARAM_STR);
}
$RechercheListeContratExc->execute();
while($InfoContrat = $RechercheListeContratExc->fetch(PDO::FETCH_OBJ)){

    #echo '<pre>';
    #print_r($InfoContrat);
    #echo '</pre>';
    #die();
    $nbMin = 0; $listeDuree = array();
    $RechercheNbMinContratExc->bindValue(':idContrat', $InfoContrat->idContrat);
    $RechercheNbMinContratExc->execute();
    while($infoDuree = $RechercheNbMinContratExc->fetch(PDO::FETCH_OBJ)){
        $dt = new DateTime($infoDuree->dateDebut);
        @$listeDuree[$dt->format('Y-m-d')] += $infoDuree->nbMin*60;
    }

    $dureeTotalFinal = 0;
    foreach ($listeDuree as $date => $nbSeconde) {
        $dureeTotalFinal += ($nbSeconde >= 28800) ? $nbSeconde-3600 : $nbSeconde;
    }

    $arrayLstIntervention = preg_split('/_/', $InfoContrat->listeIntervention);
    $listeIntervention = '';
    foreach($arrayLstIntervention as $dateIntervention){
        $dateIntervention = preg_split('/-/', $dateIntervention);
        $listeIntervention .= (!empty($listeIntervention)) ? ', '.$dateIntervention[2]  : $dateIntervention[0].' - '.$dateIntervention[1].' ('.$dateIntervention[2];
    }

    $dateContrat = new DateTime($InfoContrat->dateCreationContrat.' 00:00:00');
    $listeIntervention .= ')';

    $infoDureeContrat = formatSecToMin($dureeTotalFinal);
    $arOutput['aaData'][] = array(

        '<a class="btn btn-xs '.($InfoContrat->boolAutoEntrepreneur == 'OUI' ? 'btn-danger' : 'btn-primary').' no-margin" data-id="'.$InfoContrat->idContrat.'" href="index.php?ap=5.0-Contrat&ss_m=5.4-FicheContrat&s='.$InfoContrat->idContrat.'"><i class="icon-book-open"></i> N° '.addCaracToString($InfoContrat->idContrat, 6, '0').'</a>',
        '<span class="fs0">'.$dateContrat->format('Ymd').'</span>'. $dateContrat->format('d-m-Y'),
        $InfoContrat->nomIntervenant.' '.$InfoContrat->prenomIntervenant,
        $InfoContrat->libelleCampagne,
        $InfoContrat->libelleMission,
        $listeIntervention,
        $infoDureeContrat,
        '<input type="checkbox" id="cnt_'.$InfoContrat->idContrat.'" '.((!is_null($InfoContrat->dateRetour)) ? 'checked' : ''.'').' onchange="ClassContrat.retourContratChk('.$InfoContrat->idContrat.')" />',
        '<button class="btn bg bg-red btn-xs btn-block" onclick="ClassContrat.suppressionContrat(\''.$InfoContrat->nomIntervenant.' '.$InfoContrat->prenomIntervenant.'\', '.$InfoContrat->idContrat.')"><i class="fa fa-trash-o"></i> Supprimer</button>',
        /**
         * ($InfoContrat->boolContratArchive == 1) ? '<button class="btn bg bg-blue btn-xs btn-block" onclick="ClassContrat.archiveContrat('.$InfoContrat->idContrat.', 0)">Désarchiver</button>' : '<button class="btn bg bg-blue btn-xs btn-block" onclick="ClassContrat.archiveContrat('.$InfoContrat->idContrat.', 1)">Archiver</button>'
         */

    );
}

print json_encode($arOutput);
