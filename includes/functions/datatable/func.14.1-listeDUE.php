<?php
/**
 * Recherche des DUE
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_14_1']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_14_1']);

/** Recherche des couples PDV / intervenant */
$RechercheDUEExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheDUEExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheDUEExc->execute();
while($InfoDUE = $RechercheDUEExc->fetch(PDO::FETCH_OBJ)){

    $NumeroSS = str_replace(' ','',$InfoDUE->numeroSS);
    $F_NumeroSS = substr($NumeroSS, 0, 1).' '.substr($NumeroSS, 1, 2).' '.substr($NumeroSS, 3, 2).' '.substr($NumeroSS, 5, 2).' '.substr($NumeroSS, 7, 3).' '.substr($NumeroSS, 10, 3).' '.substr($NumeroSS, -2);

    $boolSel = (strlen($NumeroSS) == 15 && !empty($InfoDUE->depNaissanceDUE) && !empty($InfoDUE->villeNaissance) && !empty($InfoDUE->nomIntervenant)  && !empty($InfoDUE->prenomIntervenant) && !empty($InfoDUE->dateNaissance)) ? true : false;
    $boolCheck = (empty($InfoDUE->dateGeneration)) ? true : false;
    $dateNaissance = new DateTime($InfoDUE->dateNaissance.' 00:00:00');

    $arOutput['aaData'][] = array(
        /* 00 - ID du contrat       */ $InfoDUE->idContrat,
        /* 01 - Nom + Prenom        */ $InfoDUE->nomIntervenant.' '.$InfoDUE->prenomIntervenant,
        /* 02 - Numero SS   	    */ (strlen($NumeroSS) == 15 ) ? $F_NumeroSS : '<span class="label label-danger">'.$InfoDUE->numeroSS.'</font>',
        /* 03 - Date Naissance		*/ '<font style="font-size: 0">'.$dateNaissance->format('Ymd').'</font>'. $dateNaissance->format('d/m/Y'),
        /* 04 - Ville Naissance		*/ $InfoDUE->villeNaissance,
        /* 05 - Dept Naissance    	*/ (!empty($InfoDUE->libelleDepartement)) ? $InfoDUE->depNaissanceDUE.' - '.$InfoDUE->libelleDepartement : '',
        /* 06 - Date Generation DUE */ $InfoDUE->dateGeneration,
        /* 07 - Checkbox		*/ ($boolSel) ? '<input type="checkbox" id="'.$InfoDUE->idContrat.'" class="chk" '.(($boolCheck) ? 'checked' : '').' />' : ''
    );
}

print json_encode($arOutput);
