<?php
/**
 * Liste des interventions
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requete sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** On test la presence de l'id PDV */
if(filter_has_var(INPUT_GET, 'idPdv')){

    /** Recherche des intervention sur un point de vente */
    $RechercheListeInterventionPdvExc->bindValue(':idPdv', filter_input(INPUT_GET, 'idPdv'), PDO::PARAM_INT);
    $RechercheListeInterventionPdvExc->execute();
    while($InfoInterventionPdv = $RechercheListeInterventionPdvExc->fetch(PDO::FETCH_OBJ)){

        $dateIntervention = new DateTime($InfoInterventionPdv->dateDebutIntervention);
        $arOutput['aaData'][] = array(
            addCaracToString($InfoInterventionPdv->idIntervention, 7, '0'),
            $InfoInterventionPdv->libelleCampagne,
            $InfoInterventionPdv->libelleMission,
            $InfoInterventionPdv->nomIntervenant.' '.$InfoInterventionPdv->prenomIntervenant,
            '<span class="fs0">'.$InfoInterventionPdv->dateDebutIntervention.'</span>'.$dateIntervention->format('d/m/Y'),
            (!empty($InfoInterventionPdv->FK_idContrat)) ? '<a class="btn btn-xs btn-primary no-margin btn-block" href="index.php?ap=5.0-Contrat&ss_m=5.4-FicheContrat&s='.$InfoInterventionPdv->FK_idContrat.'"><i class="icon-folder-alt"></i> '.addCaracToString($InfoInterventionPdv->FK_idContrat, 7, '0').'</a>' : ''
        );
    }
}

print json_encode($arOutput);
