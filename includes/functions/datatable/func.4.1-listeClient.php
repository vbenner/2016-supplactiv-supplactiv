<?php
/**
 * Liste des clients
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/**
 * Recherche des clients
 */
$RechercheListeClientExc->execute();
while($InfoClient = $RechercheListeClientExc->fetch(PDO::FETCH_OBJ)){

    $arOutput['aaData'][] = array(
        '<button class="btn btn-xs btn-primary no-margin" data-id="'.$InfoClient->idClient.'"><i class="icon-book-open"></i> Fiche Client</button>',
        $InfoClient->idClient,
        $InfoClient->libelleClient,
        $InfoClient->adresseClient,
        $InfoClient->codePostalClient,
        $InfoClient->villeClient,
        $InfoClient->telClient,
        $InfoClient->emailClient,
        ($InfoClient->nbCampagne == 0) ? '<button class="btn btn-xs btn-danger no-margin" data-id="'.$InfoClient->idClient.'" onclick="ClassClient.suppressionClient('.$InfoClient->idClient.')"><i class="icon-trash"></i> Supprimer</button>' : ''
    );
}

print json_encode($arOutput);
