<?php
/**
 * Liste des heures supplementaires par intervenante / semaine
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** Liste des heures d'interventions */
$listeHeure = array();

/** Initialisation des dates de recherche */
$dateDRecherche = $dateFRecherche = null;
if(isset($_SESSION['semaine_10_0']) && $_SESSION['semaine_10_0'] != 'ALL'){
    $dateRecherche = preg_split('/_/', $_SESSION['semaine_10_0']);
    $dateDRecherche = $dateRecherche[0].' 00:00:00';
    $dateFRecherche = $dateRecherche[1].' 23:59:59';
}else {
    $dateDRecherche = date('Y').'-01-01 00:00:00';
    $dateFRecherche = date('Y').'-12-31 23:59:59';
}

/** Recherche des heures d'interventions */
$RechercheHeureInterventionAnneeCouranteExc->bindValue(':dateDebut', $dateDRecherche, PDO::PARAM_STR);
$RechercheHeureInterventionAnneeCouranteExc->bindValue(':dateFin', $dateFRecherche ,PDO::PARAM_STR);
$RechercheHeureInterventionAnneeCouranteExc->execute();
while($InfoHeure = $RechercheHeureInterventionAnneeCouranteExc->fetch(PDO::FETCH_OBJ)){

    /** On test si l'utilisateur est celui recherché ou non */
    $boolAffiche = false;
    if(isset($_SESSION['idIntervenant_10_0']) && $_SESSION['idIntervenant_10_0'] != 'ALL') {
        $boolAffiche = ($_SESSION['idIntervenant_10_0'] == $InfoHeure->FK_idIntervenant) ? true : false;
    }else $boolAffiche = true;

    if($boolAffiche){
        /** @var $dateDebut Creation d'un datetime pour determiner la semaine */
        $dateDebut = new DateTime($InfoHeure->dateDebut);

        /** On stock les heures */
        $listeHeure[$InfoHeure->FK_idIntervenant . '_' . $InfoHeure->nomIntervenant . ' ' . $InfoHeure->prenomIntervenant]['SEMAINE ' . $dateDebut->format('W [Y]')] += ($InfoHeure->nbSecond >= 28800) ?  $InfoHeure->nbSecond-3600 : $InfoHeure->nbSecond;
    }
}

/** On parcours les listes d'heures */
foreach($listeHeure as $infoIntervenant => $infoSemaine){

    /** On parcours les semaines d'interventions pour l'internvenant en cours */
    foreach($infoSemaine as $idSemaine => $nbSecond){

        /** @var $infoI Informations sur l'intervenant */
        $infoI = preg_split('/_/', $infoIntervenant);

        /** @var $couleurTR Couleur de la ligne en fonction du nombre d'heure */
        $couleurTR = ($nbSecond/60 > 2100) ? (($nbSecond/60 > 2340) ? 'danger' : 'warning'): '';

        $arOutput['aaData'][] = array(
            'DT_RowClass' => $couleurTR,
            0 => $infoI[0],
            1 => $infoI[1],
            2 => $idSemaine,
            3 => formatSecToMin($nbSecond)
        );
    }
}

print json_encode($arOutput);