<?php
/**
 * Liste des points de ventes
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/** -----------------------------------------------------------------------------------------------
 * Recherche des points de vente
 */
$RechercheListePdvExc->execute();
while($InfoPdv = $RechercheListePdvExc->fetch(PDO::FETCH_OBJ)){

    $allowSuppr = $InfoPdv->nbIntervention > 0 ? 0 : 1;
    $arOutput['aaData'][] = array(
        $InfoPdv->codeMerval,
        $InfoPdv->libellePdv,
        $InfoPdv->ciblage,
        $InfoPdv->categorie,
        $InfoPdv->sousCategorie,
        $InfoPdv->adressePdv_A,
        $InfoPdv->codePostalPdv,
        $InfoPdv->villePdv,
        $InfoPdv->telephoneMagasin,
        $InfoPdv->emailPdv,
        $InfoPdv->nomTitulairePdv_A,
        $InfoPdv->nbIntervention,
        $InfoPdv->boolActif,
        '<button class="btn fiche btn-xs '.($InfoPdv->boolActif ? 'bg-green' : 'bg-yellow').' 
            no-margin" data-id="'.$InfoPdv->idPdv.'"><i class="icon-book-open"></i> Fiche PDV</button>'.
        ($allowSuppr ? '<button data-name="'.$InfoPdv->libellePdv.'" style="margin-top:3px;" class="btn delete btn-xs bg-red-thunderbird no-margin" data-id="'.$InfoPdv->idPdv.'">
        <i class="icon-trash"></i> Supprimer</button>' : '')

    );


}

print json_encode($arOutput);
