<?php
/**
 * Liste des PDV pour convention de forfait
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Initialisation du tableau */
$arOutput = array('aaData' => array());

$sqlRechercheInfoContrat = '
SELECT *
FROM su_contrat_cidd
WHERE idContrat = :IdContrat';
$RechercheInfoContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoContrat);

$MissionConvention = null; $ListePdvConvention = null;
$RechercheInfoContratExc->bindValue(':IdContrat', $_SESSION['IDContrat_Recherche_6_5'], PDO::PARAM_INT); 
$RechercheInfoContratExc->execute();
while($InfoContrat = $RechercheInfoContratExc->fetch(PDO::FETCH_OBJ)):
	$MissionConvention 		= $InfoContrat->FK_idMission;
	$ListePdvConventionTmp 	= preg_split('/_/',$InfoContrat->listePDV);
	foreach($ListePdvConventionTmp as $Pdv):
		@$ListePdvConvention[$Pdv] = true;
	endforeach;
endwhile;

$sqlRechercheLstPdv = '
SELECT libellePdv, codePostalPdv, idPdv, villePdv
FROM su_pdv
	INNER JOIN su_intervention ON su_intervention.FK_idPdv = su_pdv.IdPdv
WHERE FK_idMission = :IdMission
GROUP BY idPdv';
$RechercheLstPdvExc = DbConnexion::getInstance()->prepare($sqlRechercheLstPdv);

$RechercheLstPdvExc->bindValue(':IdMission', $MissionConvention, PDO::PARAM_INT); 
$RechercheLstPdvExc->execute();
while($InfoPdv = $RechercheLstPdvExc->fetch(PDO::FETCH_OBJ)) {

    $Sel = (isset($ListePdvConvention[$InfoPdv->idPdv])) ? 'checked="checked"' : '';
    # ------------------------------------------------------------------------------------
    # Cr�ation de la ligne de mission
    # ------------------------------------------------------------------------------------
    $arOutput['aaData'][] = array(
        '<input type="checkbox" id="PdvConvention_' . $InfoPdv->idPdv . '" ' . $Sel . '/>',
        $InfoPdv->libellePdv,
        $InfoPdv->codePostalPdv . ' - ' . $InfoPdv->villePdv
    );
}

echo json_encode($arOutput);
