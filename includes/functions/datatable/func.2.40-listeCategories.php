<?php
/**
 * Liste des CATEGORIES
 *
 * @author Vincent BENNER - Page Up
 * @copyright Page Up
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/* ------------------------------------------------------------------------------------------------
 * Liste des catégories
 */
$sqlListeCategories = '
SELECT *
FROM su_pdv_categorie
';
$listeCategoriesExec = DbConnexion::getInstance()->prepare($sqlListeCategories);
$listeCategoriesExec->execute();
while($categorie = $listeCategoriesExec->fetch(PDO::FETCH_OBJ)){

    /** -------------------------------------------------------------------------------------------
     * Liste des SOUS-CAT
     */
    $sqlCount = '
    SELECT COUNT(idSousCategorie) AS LeCount
    FROM su_pdv_sous_categorie
    WHERE FK_idCategorie = :id
    ';
    $countExec = DbConnexion::getInstance()->prepare($sqlCount);
    $countExec->bindValue(':id', $categorie->idCategoriePdv, PDO::PARAM_INT);
    $countExec->execute();
    $count = $countExec->fetch(PDO::FETCH_OBJ);
    $arOutput['aaData'][] = array(
        $categorie->idCategoriePdv,
        $categorie->libelleCategoriePdv,
        $count->LeCount . ' ' .
        '<a class="btn btn-xs btn-primary" href="index.php?ap=2.0-PointDeVente&ss_m=2.42-FicheSousCategorie&s='.$categorie->idCategoriePdv.'"><i class="icon-folder-alt"></i> Gérer </a>',
        '<a class="btn btn-xs btn-primary" href="index.php?ap=2.0-PointDeVente&ss_m=2.41-FicheCategorie&s='.$categorie->idCategoriePdv.'"><i class="icon-folder-alt"></i> Modif </a>'
    );
}

print json_encode($arOutput);
