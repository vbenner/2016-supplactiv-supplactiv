<?php
/**
 * Recherche du registre du personnel
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
#ini_set('display_errors', 'on');
set_time_limit(0);
#ini_set('memory_limit', -1);
error_reporting( E_ALL );

require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

$tabIntervenant = array();
$Nb_Intervention_Mois = array(); $Liste_Intervention = array();
$ListeIntervenantTOTAL = array(); $ListeInterventionFinal = array();

/** Recherche des intervenants */
$RechercheIntervenantNonAutoExec->execute();
while($DataIntervenant = $RechercheIntervenantNonAutoExec->fetch(PDO::FETCH_OBJ)) {

    $tabIntervenant[] = $DataIntervenant->idIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['NOM'] = $DataIntervenant->nomIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['PRENOM'] = $DataIntervenant->prenomIntervenant;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['NAISSANCE'] = $DataIntervenant->dateNaissance;
    @$ListeIntervenantTOTAL[$DataIntervenant->idIntervenant]['DEP'] = $DataIntervenant->departementNaissance;
}
/** Recherche des interventions avec contrat */
foreach ($tabIntervenant as $idInter) {
    $RechercheInterventionAvecContratNoAutoExec->bindValue(':idinter', $idInter);
    $RechercheInterventionAvecContratNoAutoExec->execute();
    while($DataContrat = $RechercheInterventionAvecContratNoAutoExec->fetch(PDO::FETCH_OBJ)) {
        @$Liste_Intervention[$DataContrat->FK_idIntervenant][$DataContrat->Dte] = true;
    }
}

#print_r($Liste_Intervention);
#die();

$fp = fopen('RegistrePersonnel.csv', 'w+');
fwrite($fp, "INTERVENANT;SEXE;NATIONALITE;DATE NAISSANCE;DATE ENTREE;DATE SORTIE;TYPE DE TRAVAIL;"."\n");

/** Parcours des interventions */
foreach($Liste_Intervention as $IdIntervenant=>$Valeur) {

    /** Initialisation des variables de debut et de fin */
    $DateDebut = ''; $DateFin = '';
    foreach ($Valeur as $DateIntervention => $Total) {
        if ($DateIntervention != '') {

            $DateDebut = (empty($DateDebut)) ? $DateIntervention : $DateDebut;
            $DateFin = (empty($DateFin)) ? $DateIntervention : $DateFin;

            if (isset($Liste_Intervention[$IdIntervenant][$DateIntervention + 1])) {
                $DateFin = $DateIntervention + 1;
            } else {

                # ------------------------------------------------------------------------------------
                # Création de la ligne de mission
                # ------------------------------------------------------------------------------------
                $arOutput['aaData'][] = array(
                    @$ListeIntervenantTOTAL[$IdIntervenant]['NOM'] . ' ' . @$ListeIntervenantTOTAL[$IdIntervenant]['PRENOM'],
                    "F",
                    $ListeIntervenantTOTAL[$IdIntervenant]['DEP'],
                    $ListeIntervenantTOTAL[$IdIntervenant]['NAISSANCE'],
                    substr($DateDebut, 0, 4) . ' - ' . substr($DateDebut, 4, 2) . ' - ' . substr($DateDebut, -2),
                    substr($DateFin, 0, 4) . ' - ' . substr($DateFin, 4, 2) . ' - ' . substr($DateFin, -2),
                    'Animateur / Formateur'
                );

                fwrite($fp, @$ListeIntervenantTOTAL[$IdIntervenant]['NOM'] . ' ' . @$ListeIntervenantTOTAL[$IdIntervenant]['PRENOM'].";".
                    "F".";".
                    $ListeIntervenantTOTAL[$IdIntervenant]['DEP'].";".
                    $ListeIntervenantTOTAL[$IdIntervenant]['NAISSANCE'].";".
                    substr($DateDebut, 0, 4) . ' - ' . substr($DateDebut, 4, 2) . ' - ' . substr($DateDebut, -2).";".
                    substr($DateFin, 0, 4) . ' - ' . substr($DateFin, 4, 2) . ' - ' . substr($DateFin, -2).";".
                    'Animateur / Formateur'.";\n"
                );

                $DateDebut = ''; $DateFin = '';
            }
        }
    }
}

$RechercheCIDDNoAutoExec->execute();
while($InfoCidd = $RechercheCIDDNoAutoExec->fetch(PDO::FETCH_OBJ)) {
    $Dte = preg_split('/-/', $InfoCidd->dateContrat);
    $DernierJourMois = date(date("t", mktime(0, 0, 0, $Dte[1], 1, $Dte[0])), mktime(0, 0, 0, $Dte[1], 1, $Dte[0]));

    $arOutput['aaData'][] = array(
        '<font style="color:Blue">' . $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NOM'] . ' ' . @$ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['PRENOM'] . '</font>',
        '<font style="color:Blue">' . "F" . '</font>',
        '<font style="color:Blue">' . $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['DEP'] . '</font>',
        '<font style="color:Blue">' . $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NAISSANCE'] . '</font>',
        '<font style="color:Blue">' . $Dte[0] . ' - ' . $Dte[1] . ' - 01' . '</font>',
        '<font style="color:Blue">' . $Dte[0] . ' - ' . $Dte[1] . ' - ' . $DernierJourMois . '</font>',
        '<font style="color:Blue"><i>Convention de forfait</i></font>'
    );

    fwrite($fp, $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NOM'] . ' ' . @$ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['PRENOM'].";".
        "F".";".
        $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['DEP'].";".
        $ListeIntervenantTOTAL[$InfoCidd->idIntervenant]['NAISSANCE'].";".
        $Dte[0] . ' - ' . $Dte[1] . ' - 01'.";".
        $Dte[0] . ' - ' . $Dte[1] . ' - ' . $DernierJourMois.";".
        "Convention de forfait".";\n"
    );


}

fclose($fp);

print json_encode($arOutput);