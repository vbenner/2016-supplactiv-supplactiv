<?php
/**
 * Liste des interventions sans contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation des dates de recherche */
$dateFin = new DateTime('last day of this month');
$_SESSION['dateRechercheD_5_2'] = (isset($_SESSION['dateRechercheD_5_2'])) ? $_SESSION['dateRechercheD_5_2'] : date('01/m/Y');
$_SESSION['dateRechercheF_5_2'] = (isset($_SESSION['dateRechercheF_5_2'])) ? $_SESSION['dateRechercheF_5_2'] : $dateFin->format('d/m/Y');

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_5_2']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_5_2']);

$sqlRechercheInterventionSansContrat = '
SELECT su_intervention.dateDebut dteInter, idIntervention, idIntervenant, idMission, nomIntervenant, 
  prenomIntervenant, libelleMission, libellePdv, libellePdv, libelleCampagne, boolAutoEntrepreneuse,
  su_intervenant.dateFinSejour
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
    INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE FK_idContrat IS NULL AND FK_idIntervenant <> 1 AND su_intervention.dateDebut BETWEEN :dateDebut AND :dateFin';

$sqlRechercheInterventionSansContrat .= ( $_SESSION['idMission_5_2'] != 'ALL') ? ' AND idMission = '.$_SESSION['idMission_5_2'].' ' : '';
$sqlRechercheInterventionSansContrat .= ( $_SESSION['idIntervenant_5_2'] != 'ALL') ? ' AND idIntervenant = '.$_SESSION['idIntervenant_5_2'].' ' : '';

$RechercheInterventionSansContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionSansContrat);
/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/** Recherche des CONTRATS */
$RechercheInterventionSansContratExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheInterventionSansContratExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheInterventionSansContratExc->execute();
while($InfoCouple = $RechercheInterventionSansContratExc->fetch(PDO::FETCH_OBJ)){

    /** -------------------------------------------------------------------------------------------
     * Pour les titres de fin de séjour, on bloque maintenant si il y a une expiration
     * dans le mois prochain
     */
    $dayNow = date("Y-m-d");
    $monthEnd = date("Y-m-d", strtotime($dayNow.' + 1 month'));
    if (
        $InfoCouple->dateFinSejour >= $dayNow &&
        $InfoCouple->dateFinSejour <= $monthEnd
    ) {
        $titre = '<span class="label label-danger"><small><i class="icon-calendar"></i> Titre Séjour</small></span>';
    }
    else {
        $titre = '<input id="'.$InfoCouple->idIntervention.'" type="checkbox" data-intervenant="'.$InfoCouple->idIntervenant.'" data-mission="'.$InfoCouple->idMission.'" value="'.$InfoCouple->idIntervention.'" '.(($_SESSION['idIntervention_5_2'] == $InfoCouple->idIntervention) ? 'checked' : '').' class="chk"/>';
    }
    $dteInter = new DateTime($InfoCouple->dteInter);
    $arOutput['aaData'][] = array(
        'DT_RowClass' => ($InfoCouple->boolAutoEntrepreneuse == 'OUI') ? 'text-danger' : '',
        $InfoCouple->idIntervention,
        $InfoCouple->libelleCampagne,
        $InfoCouple->libelleMission,
        addCaracToString($InfoCouple->idIntervenant, 5, '0').' - '.$InfoCouple->nomIntervenant.' '.$InfoCouple->prenomIntervenant,
        $InfoCouple->libellePdv,
        '<span class="fs0">'.$dteInter->format('Ymd').'</span>'. $dteInter->format('d-m-Y'),
        $titre
    );
}

print json_encode($arOutput);