<?php
/**
 * Liste des campagnes pour un client
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

$RechercheCampagneClientExc->bindValue(':idClient', filter_input(INPUT_GET, 'idClient'));
$RechercheCampagneClientExc->execute();
while($InfoCampagne = $RechercheCampagneClientExc->fetch(PDO::FETCH_OBJ)){
    $arOutput['aaData'][] = array(
        $InfoCampagne->libelleCampagne,
        $InfoCampagne->dateDebut,
        $InfoCampagne->dateFin,
        $InfoCampagne->nbIntervention,
        $InfoCampagne->nomInterlocuteurClient,
        '<button class="btn btn-xs btn-primary no-margin" data-id="'.$InfoCampagne->idCampagne.'">Fiche campagne</button>'
    );
}

print json_encode($arOutput);