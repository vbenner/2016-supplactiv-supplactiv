<?php
/**
 * Liste des interventions pour une mission
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());
$boolCheck = false;
$InfoStatut = null;

$RechercheStatutInterventionMissionExc->bindValue(':idMission', filter_input(INPUT_GET, 'idMission'));
$RechercheStatutInterventionMissionExc->execute();
while($InfoIntervention = $RechercheStatutInterventionMissionExc->fetch(PDO::FETCH_OBJ)){

    $couleur = '';
    if(!is_null($InfoIntervention->FK_idContrat)){
        $couleur = 'c-success';
    }else {
        if($InfoIntervention->idIntervenant == 1){
            if(is_null($InfoStatut->dateDebut)){
                $couleur = 'c-danger';
            }else $couleur = 'c-warning';
        }else {
            if(is_null($InfoIntervention->dateDebut)){
                $couleur = 'c-warning';
            }else $couleur = 'c-active';
        }
    }
    if(!empty($InfoIntervention->dateDebut))
    $date = new DateTime($InfoIntervention->dateDebut);
    $arOutput['aaData'][] = array(
        'DT_RowClass' => $couleur,
        '<button class="btn btn-xs btn-primary" data-id="'.$InfoIntervention->idIntervention.'"><i class="fa fa-book"></i> '.addCaracToString($InfoIntervention->idIntervention, 6, '0').'</button>',
        $InfoIntervention->libellePdv,
        $InfoIntervention->codePostalPdv,
        $InfoIntervention->villePdv,
        $InfoIntervention->nomIntervenant.' '.$InfoIntervention->prenomIntervenant,
        (!empty($InfoIntervention->dateDebut)) ? '<span class="fs0">'.$date->format('Ymd').'</span>'. $date->format('d-m-Y')
 : '',
        (!is_null($InfoIntervention->FK_idContrat)) ? addCaracToString($InfoIntervention->FK_idContrat, 6, '0') : '',
        ( '<input type="checkbox" id="'.$InfoIntervention->idIntervention.'" class="chk" '.(($boolCheck) ? 'checked' : '').' />' )
    )
    ;
}

print json_encode($arOutput);