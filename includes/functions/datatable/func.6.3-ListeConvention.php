<?php
/**
 * Liste des convention
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';


# ------------------------------------------------------------------------------------
# Ajoute des caracteres a une chaine
# ------------------------------------------------------------------------------------
function ajoutCrc($Ch,$Taille,$Carac){
	$C = '';
	for($i = strlen($Ch); $i < $Taille; $i++){
		$C .= $Carac;
	}	
	return $C.$Ch;
}

$sqlRechercheListeContrat = '
SELECT *
FROM su_mission
	INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idMission = su_mission.idMission
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_contrat_cidd.FK_idIntervenant';
$RechercheLstContratExc = DbConnexion::getInstance()->prepare($sqlRechercheListeContrat);


# ------------------------------------------------------------------------------------
# Initialisation de mes donn�es de sortie
# ------------------------------------------------------------------------------------
$arOutput = array(); $arOutput['aaData'] = array();


$RechercheLstContratExc->execute();
while($InfoContrat = $RechercheLstContratExc->fetch(PDO::FETCH_OBJ)):
	
	# ------------------------------------------------------------------------------------
	# Cr�ation de la ligne de mission
	# ------------------------------------------------------------------------------------
    $row = array(
	    /* 01 - Id Contrat          */ ajoutCrc($InfoContrat->idContrat,6,'0'),
	    /* 02 - BTN Contrat   	    */ $InfoContrat->nomIntervenant.' '.$InfoContrat->prenomIntervenant,
	    /* 03 - Intervenante   		*/ $InfoContrat->libelleMission,
	    /* 04 - Libelle Campagne    */ $InfoContrat->dureeMensuelle.' heure(s)',
	    /* 05 - Date       			*/ $InfoContrat->indemniteForfaitaire.' &euro;',
	    /* 06 - Btn Action         	*/ $InfoContrat->remunerationBrute.' &euro;',
	    $InfoContrat->dateContrat,
	    /* 07 - Supprimer           */ '<button class="btn btn-success btn-xs" onclick="telechargementConvention('.$InfoContrat->idContrat.')">T&eacute;l&eacute;charger</button>',
	    							   '<button class="btn btn-success btn-xs"  onclick="afficheInfoConvention('.$InfoContrat->idContrat.')">Modifier</button>',
	    /* 08 - Archiver            */ '<button class="btn btn-danger btn-xs" onclick="suppressionContrat('.$InfoContrat->idContrat.')">Supprimer</button>'
    );
    
	# ------------------------------------------------------------------------------------
	# On ajoute au r�sultat la ligne de r�ception
	# ------------------------------------------------------------------------------------
    $arOutput['aaData'][] = $row;
endwhile;

# ------------------------------------------------------------------------------------
# Encodage en Json 
# ------------------------------------------------------------------------------------
echo json_encode($arOutput);
?>