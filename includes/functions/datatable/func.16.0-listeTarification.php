<?php
/**
 * Recherche des differentes tarifications
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

$RechercheTarificationCampagneExc->execute();
while($InfoCampagne = $RechercheTarificationCampagneExc->fetch(PDO::FETCH_OBJ)){

    $boolAffiche = true;
    $boolAffiche = (isset($_SESSION['idIntervenant_16_0']) && in_array($_SESSION['idIntervenant_16_0'], array($InfoCampagne->idIntervenant, 'ALL'))) ? $boolAffiche : false;
    $boolAffiche = (isset($_SESSION['idCampagne_16_0']) && in_array($_SESSION['idCampagne_16_0'], array($InfoCampagne->idCampagne, 'ALL'))) ? $boolAffiche : false;

    if($boolAffiche) {
        $arOutput['aaData'][] = array(
            addCaracToString($InfoCampagne->idIntervenant, 5, '0') . ' - ' . $InfoCampagne->nomIntervenant . ' ' . $InfoCampagne->prenomIntervenant,
            $InfoCampagne->adresseIntervenant_A,
            $InfoCampagne->codePostalIntervenant,
            $InfoCampagne->villeIntervenant,
            $InfoCampagne->mobileIntervenant,
            $InfoCampagne->telephoneIntervenant_A,
            $InfoCampagne->mailIntervenant,
            $InfoCampagne->libelleCampagne,
            $InfoCampagne->salaireBrut,
            $InfoCampagne->fraisRepas,
            $InfoCampagne->fraisTelephone,
            $InfoCampagne->tarifKM,
            $InfoCampagne->nbIntervention
        );
    }
}

print json_encode($arOutput);