<?php
/**
 * Liste des SOUS-CATEGORIES
 *
 * @author Vincent BENNER - Page Up
 * @copyright Page Up
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';
session_start();

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/* ------------------------------------------------------------------------------------------------
 * In use ?
 */
$sqlInUse = '
SELECT COUNT(idPdv) AS LeCount
FROM su_pdv
WHERE FK_idCategorie = :FK_idCategorie
AND FK_idSousCategorie = :FK_idSousCategorie
';
$inUseExec = DbConnexion::getInstance()->prepare($sqlInUse);

/* ------------------------------------------------------------------------------------------------
 * Liste des sous-catégories
 */
$sqlListeSousCategories = '
SELECT *
FROM su_pdv_sous_categorie
WHERE FK_idCategorie = :id
';
$listeSousCategoriesExec = DbConnexion::getInstance()->prepare($sqlListeSousCategories);
$listeSousCategoriesExec->bindValue(':id', filter_input(INPUT_GET, 'idCategorie'));
$listeSousCategoriesExec->execute();
while($sousCategorie = $listeSousCategoriesExec->fetch(PDO::FETCH_OBJ)){

    /** -------------------------------------------------------------------------------------------
     * Un bouton SUPPR apparaît si on peut supprimer
     */
    $inUseExec->bindValue(':FK_idCategorie', filter_input(INPUT_GET, 'idCategorie'), PDO::PARAM_INT);
    $inUseExec->bindValue(':FK_idSousCategorie', $sousCategorie->idSousCategorie, PDO::PARAM_INT);
    $inUseExec->execute();
    $inUse = $inUseExec->fetchObject();

    $arOutput['aaData'][] = array(
        $sousCategorie->idSousCategorie,
        $sousCategorie->libelleSousCategorie,
        '<a class="btn btn-xs btn-primary" href="index.php?ap=2.0-PointDeVente&ss_m=2.43-FicheSousCategorie&s='.$sousCategorie->idSousCategorie.'_'.$sousCategorie->FK_idCategorie.'"><i class="icon-folder-alt"></i> Modif </a>'.
        ($inUse->LeCount == 0 ? '<a class="btn btn-xs btn-danger delete" data-name="'.$sousCategorie->libelleSousCategorie.'" data-id="'.$sousCategorie->idSousCategorie.'"><i class="icon-trash"></i> Suppr </a>' : '')
    );
}

print json_encode($arOutput);
