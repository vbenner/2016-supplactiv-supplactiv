<?php
/**
 * Recherche des prises d'accords
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

//date('Y-m-d 00:00:00')
$RecherchePriseAccordExc->bindValue(':dateDebut', date('Y-m-d').' 00:00:00');
$RecherchePriseAccordExc->execute();
while($InfoPA = $RecherchePriseAccordExc->fetch(PDO::FETCH_OBJ)) {

    # ------------------------------------------------------------------------------------
    # Création de la ligne de mission
    # ------------------------------------------------------------------------------------
    $arOutput['aaData'][] = array(
        $InfoPA->libelleCampagne,
        $InfoPA->libelleMission,
        $InfoPA->codeMerval . ' - ' . $InfoPA->libellePdv,
        '<button class="btn btn-xs bg bg-green" onclick="ClassPriseAccord.telechargementPriseAccord('.$InfoPA->idMission.', '.$InfoPA->idPdv.')">T&eacute;l&eacute;charger</button>'
    );

}

print json_encode($arOutput);
