<?php
/**
 * Recherche des differents kM pour une couple intervenant / PDV
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 * Recherche des couple point de vente / intervenant avec leurs KM
 *
 * @param idIntervenant
 * @param idPdv
 *
 * @author Kevin MAURICE - Page UP
 * @var $RecherchePdvVisiteKmExc
 */
$sqlRecherchePdvVisiteKm = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, codePostalIntervenant, villeIntervenant, idPdv, codePostalPdv, villePdv, libellePdv, codeMerval, totalKm
FROM su_intervention
	INNER JOIN su_intervenant  ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv  ON su_pdv.idPdv = su_intervention.FK_idPdv
	LEFT JOIN su_pdv_kilometrage ON su_pdv_kilometrage.FK_idPdv = su_intervention.FK_idPdv AND su_pdv_kilometrage.FK_idIntervenant = su_intervenant.idIntervenant
WHERE su_intervention.FK_idIntervenant <> 1';

/** Filtre sur le point de vente */
if(isset($_SESSION['idPointdeVente_13_0']) && $_SESSION['idPointdeVente_13_0'] != 'ALL'){
    $sqlRecherchePdvVisiteKm .= ' AND su_intervention.FK_idPdv = :idPdv';
}

/** Filtre sur l'intervenante */
if(isset($_SESSION['idIntervenant_13_0']) && $_SESSION['idIntervenant_13_0'] != 'ALL'){
    $sqlRecherchePdvVisiteKm .= ' AND su_intervention.FK_idIntervenant = :idIntervenant';
}

/** Filtre sur la date de debut et de fin */
$sqlRecherchePdvVisiteKm .= (isset($_SESSION['boolPlanification_13_0']) && $_SESSION['boolPlanification_13_0'] == 1) ? ' AND ((dateDebut BETWEEN :dateDebut AND :dateFin) OR dateDebut IS NULL )' : ' AND dateDebut BETWEEN :dateDebut AND :dateFin';
$sqlRecherchePdvVisiteKm .= ' GROUP BY idIntervenant, idPdv';

$RecherchePdvVisiteKmExc = DbConnexion::getInstance()->prepare($sqlRecherchePdvVisiteKm);

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array(), 'sql' => '');

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_13_0']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_13_0']);

/** Recherche des couples PDV / intervenant */
$RecherchePdvVisiteKmExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RecherchePdvVisiteKmExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
if(isset($_SESSION['idPointdeVente_13_0']) && $_SESSION['idPointdeVente_13_0'] != 'ALL'){
    $RecherchePdvVisiteKmExc->bindValue(':idPdv', $_SESSION['idPointdeVente_13_0'], PDO::PARAM_INT);
}
if(isset($_SESSION['idIntervenant_13_0']) && $_SESSION['idIntervenant_13_0'] != 'ALL'){
    $RecherchePdvVisiteKmExc->bindValue(':idIntervenant', $_SESSION['idIntervenant_13_0'], PDO::PARAM_INT);
}
$RecherchePdvVisiteKmExc->execute();
while($InfoKm = $RecherchePdvVisiteKmExc->fetch(PDO::FETCH_OBJ)){
    $arOutput['aaData'][] = array(
        /* 01 - Code MERVAL         */ $InfoKm->codeMerval,
        /* 02 - Libelle PDV   	    */ $InfoKm->libellePdv,
        /* 03 - Code Postal PDV 	*/ $InfoKm->codePostalPdv,
        /* 04 - Ville PDV   		*/ $InfoKm->villePdv,
        /* 04 - Libelle Campagne    */ $InfoKm->nomIntervenant.' '.$InfoKm->prenomIntervenant,
        /* 05 - Date       			*/ $InfoKm->adresseIntervenant_A,
        /* 06 - Btn Action         	*/ $InfoKm->codePostalIntervenant,
        /* 07 - Retour Contrat		*/ $InfoKm->villeIntervenant,
        /* 07 - Supprimer           */ '<input id="km_'.$InfoKm->idPdv.'_'.$InfoKm->idIntervenant.'" class="form-control" value="'.$InfoKm->totalKm.'" />',
        /* 08 - Archiver            */ '<button class="btn bg bg-green btn-xs" onclick="ClassKM.validModifKm(\''.$InfoKm->idPdv.'_'.$InfoKm->idIntervenant.'\')"><i class="fa fa-pencil"></i> Modifier</button>'
    );
}

$arOutput['sql'] = $sqlRecherchePdvVisiteKm;
print json_encode($arOutput);
