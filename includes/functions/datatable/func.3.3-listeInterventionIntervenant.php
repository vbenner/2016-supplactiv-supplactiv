<?php
/**
 * Recherche du detail des interventions
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/**
 * Recherche des interventions
 */
$RechercheNbInterventionIntervenantExc->bindValue(':idIntervenant', filter_input(INPUT_GET, 'idIntervenant'));
$RechercheNbInterventionIntervenantExc->execute();
while($Info = $RechercheNbInterventionIntervenantExc->fetch(PDO::FETCH_OBJ)){
    $arOutput['aaData'][] = array(
        $Info->libelleClient,
        $Info->libelleMission,
        $Info->nbIntervention.' (de '.$Info->dateMin.' à '.$Info->dateMax.')'
    );
}

print json_encode($arOutput);