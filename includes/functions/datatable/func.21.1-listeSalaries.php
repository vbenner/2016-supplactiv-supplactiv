<?php
/**
 * Liste des salariés
 *
 * @author Vincent BENNER
 * @copyright Page Up
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Requete sql
 */
$sqlListeSalaries = '
SELECT *
FROM du_utilisateur
';
$listeSalariesExec = DbConnexion::getInstance()->prepare($sqlListeSalaries);

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/**
 * Recherche des clients
 */
$listeSalariesExec->execute();
while($salarie = $listeSalariesExec->fetch(PDO::FETCH_OBJ)){

    $arOutput['aaData'][] = array(
        $salarie->nomUtilisateur,
        $salarie->prenomUtilisateur,
        $salarie->immatUtilisateur,
        $salarie->typeIntervenantExpensia,
        $salarie->contratIntervenant,
        $salarie->boolActif == 1 ? 'OUI' : 'NON',
        '<button class="btn btn-xs btn-primary no-margin" data-id="'.$salarie->idUtilisateur.'"><i class="icon-book-open"></i> Fiche</button>'
    );
}

print json_encode($arOutput);
