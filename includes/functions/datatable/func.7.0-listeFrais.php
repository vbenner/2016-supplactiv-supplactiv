<?php
/**
 * Liste des frais d'intervention
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

$ListeTauxKM = array();
$RechercheTauxKmExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
$RechercheTauxKmExc->execute();
while ($InfoTaux = $RechercheTauxKmExc->fetch(PDO::FETCH_OBJ)) {
    $ListeTauxKM[$InfoTaux->tarifKilometre] = 0;
}

$RechercheIntervenantMoisExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
$RechercheIntervenantMoisExc->execute();
while ($InfoFrais = $RechercheIntervenantMoisExc->fetch(PDO::FETCH_OBJ)) {

    foreach ($ListeTauxKM as $tarif => $km) {
        $ListeTauxKM[$tarif] = 0;
    }

    /** Recherche des differents KM possible */
    $RechercheKmInterventionExc->bindValue(':idIntervenant', $InfoFrais->idIntervenant, PDO::PARAM_INT);
    $RechercheKmInterventionExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateFrais'), PDO::PARAM_STR);
    $RechercheKmInterventionExc->execute();

    $listeKM = '';
    while ($InfoKm = $RechercheKmInterventionExc->fetch(PDO::FETCH_OBJ)) {
        $listeKM .= (!empty($listeKM)) ? ' | ' . $InfoKm->tarifKilometre . ' &euro; : ' . $InfoKm->totalKilometre : $InfoKm->tarifKilometre . ' &euro; : ' . $InfoKm->totalKilometre;
        $ListeTauxKM[$InfoKm->tarifKilometre] = $InfoKm->totalKilometre;
    }

    $infoIntervenant = array(
        $InfoFrais->nomIntervenant . ' ' . $InfoFrais->prenomIntervenant.' ('.addCaracToString($InfoFrais->idIntervenant, 7, '0') . ')',
        formatZToEmpty($InfoFrais->montantPrimeObjectif),
        formatZToEmpty($InfoFrais->montantIndemniteAnnulation),
        formatZToEmpty($InfoFrais->montantAutrePrime),
        formatZToEmpty($InfoFrais->montantPrimeRDV),
        formatZToEmpty($InfoFrais->montantIndemniteFormation),
        formatZToEmpty($InfoFrais->montantFraisJustificatif),
        formatZToEmpty($InfoFrais->montantIndemniteTelephone),
        formatZToEmpty($InfoFrais->montantAutreFrais),
        formatZToEmpty($InfoFrais->montantRepriseAccompte)
    );

    $hasKM = false;
    foreach ($ListeTauxKM as $tarif => $km) {
        array_push($infoIntervenant, formatZToEmpty($km));
        if ($km != 0 ) {
            $hasKM = true;
        }
    }

    if (
        $InfoFrais->montantPrimeObjectif == 0 &&
        $InfoFrais->montantIndemniteAnnulation == 0 &&
        $InfoFrais->montantAutrePrime == 0 &&
        $InfoFrais->montantPrimeRDV == 0 &&
        $InfoFrais->montantIndemniteFormation == 0 &&
        $InfoFrais->montantFraisJustificatif == 0 &&
        $InfoFrais->montantIndemniteTelephone == 0 &&
        $InfoFrais->montantAutreFrais == 0 &&
        $InfoFrais->montantRepriseAccompte == 0 &&
        !$hasKM
    ) {

    } else {
        $arOutput['aaData'][] = $infoIntervenant;

    }
}

print json_encode($arOutput);
