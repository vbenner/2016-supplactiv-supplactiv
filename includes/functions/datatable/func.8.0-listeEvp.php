<?php
/** -----------------------------------------------------------------------------------------------
 * Mise à jour du modèle du fichier des EVP
 * 2020-11-27
 */


/** -----------------------------------------------------------------------------------------------
 * @param $str
 * @return string
 */
function doubleFormat($str){
    return number_format($str, 2, '.', '');
}

function doubleFormatVirgule($str){
    return number_format($str, 2, ',', '');
}

function doubleFormatPoint($str){
    return number_format($str, 2, '.', '');
}

/** -----------------------------------------------------------------------------------------------
 * Liste des EVP pour le mois courant
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** -----------------------------------------------------------------------------------------------
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** -----------------------------------------------------------------------------------------------
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';
require_once dirname(__FILE__) . '/../../queries/queries.web2bdd.php';

#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);


/** -----------------------------------------------------------------------------------------------
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array(), 'jour' => array(), 'fichier' => '');

/** -----------------------------------------------------------------------------------------------
 * On horodate le fichier
 */
$fName = filter_input(INPUT_GET, 'dateEvp').'-'.date('H-m-i').'-EVP.csv';
$fp = fopen('../../../download/EVP/'.$fName, 'w+');

/** -----------------------------------------------------------------------------------------------
 * On test la presence de la date
 */
if (filter_has_var(INPUT_GET, 'dateEvp')) {

    $dernierJour = new DateTime(filter_input(INPUT_GET, 'dateEvp') . ' 00:00:00');
    $dernierJour->modify('last day of this month');

    $premierJourMoisDernier = new DateTime(filter_input(INPUT_GET, 'dateEvp') . ' 00:00:00');
    $premierJourMoisDernier->modify('first day of previous month');

    $ListeIntervenantContratCDD = array();

    /** -------------------------------------------------------------------------------------------
     * Écriture de l'entête
     * MATRICULE	NB_INTERVENTION#		Heures mensuelles	Salaire mensuel	REPAS_FRAIS_REELS	IND_REPAS#		KMS	IND_KMS#		ACOMPTE	PRIME_OBJECTIF		IND_TEL#	IND_FORM#	FRAIS_PRO	IND_RDV_TEL#	PRIME_DIV#		IND_ANN#		HS 25 %	HS 50 %
     */
    $chaine = "MATRICULE;NB_INTERVENTION#;;Heures mensuelles;Salaire mensuel;REPAS_FRAIS_REELS;IND_REPAS#;;";
    $chaine .= "KMS;IND_KMS#;;ACOMPTE;PRIME_OBJECTIF;;IND_TEL#;IND_FORM#;FRAIS_PRO;";
    $chaine .= "IND_RDV_TEL#;PRIME_DIV#;;IND_ANN#;;HEUSU25EXO;HEUSU50EXO;";
    $chaine .= "\r\n";
    fwrite($fp, $chaine);

    /** -------------------------------------------------------------------------------------------
     * Recherche des intervenantes
     */
    $RechercheIntervenantEvpExc->bindValue(':dateDebut', filter_input(INPUT_GET, 'dateEvp') . ' 00:00:00');
    $RechercheIntervenantEvpExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
    $RechercheIntervenantEvpExc->execute();
    while ($InfoEvp = $RechercheIntervenantEvpExc->fetch(PDO::FETCH_OBJ)) {

        /** ---------------------------------------------------------------------------------------
         * Initialisation des variables
         */
        $boolAutorisationHS = false;
        $nbSecondeSup = 0;
        $listeHeureIntervention = array();
        $listeFraisRepas = array();
        $listeFraisTel = array();
        $nbSecondeTotal = 0;
        $nbSecondeHS_25 = 0;
        $nbSecondeHS_50 = 0;
        $arOutput['jour'][$InfoEvp->idIntervenant] = array();

        /** ---------------------------------------------------------------------------------------
         * On parcours les jours du mois
         */
        for ($idJour = 1; $idJour <= $dernierJour->format('d'); $idJour++) {


            $jourRecherche = ($idJour < 10) ? '0'.$idJour : $idJour;
            /** @var $jourSel Datetime du jour en cours */
            $jourSel = new DateTime($dernierJour->format('Y-m-') . $jourRecherche . ' 23:59:59');

            $boolAffichageHS = false;

            /** -----------------------------------------------------------------------------------
             * Le premier jour du mois n'est pas a Lundi
             * @todo On doit rechercher les heures pour la semaine complete
             */
            if ($idJour == 1 && $jourSel->format('w') > 1) {

                /** -------------------------------------------------------------------------------
                 * On reinitialise les HS
                 */
                $nbSecondeSup = 0;

                /** -------------------------------------------------------------------------------
                 * Recherche des HS
                 */
                $RechercheHSIntervenantExc->bindValue(':dateHS', $premierJourMoisDernier->format('Y-m'). '-01');
                $RechercheHSIntervenantExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
                $RechercheHSIntervenantExc->execute();
                while ($InfoHS = $RechercheHSIntervenantExc->fetch(PDO::FETCH_OBJ)) {
                    $nbSecondeSup += $InfoHS->nbSeconde;
                    $boolAutorisationHS = true;
                }
            }

            /** -----------------------------------------------------------------------------------
             * Autorisation des heures sup pour la semaine
             */
            if ($jourSel->format('w') == 1) {
                $boolAutorisationHS = true;
            }

            /** -----------------------------------------------------------------------------------
             * Il s'agit d'un dimanche on arrete de compter les HS
             */
            if ($jourSel->format('w') == 0 && $boolAutorisationHS) {
                $boolAutorisationHS = false;
                $boolAffichageHS = true;
            }

            /** -----------------------------------------------------------------------------------
             * Fin de semaine incomplete
             * @todo Stock en BDD des heures des X derniers jour de la derniere semaine
             */
            if ($idJour == $dernierJour->format('d') && $jourSel->format('w') != 0) {
                $InsertionHSIntervenantExc->bindValue(':dateHS', filter_input(INPUT_GET, 'dateEvp'));
                $InsertionHSIntervenantExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
                $InsertionHSIntervenantExc->bindValue(':nbSeconde', $nbSecondeSup);
                if(!$InsertionHSIntervenantExc->execute()){
                }
            }

            /** -----------------------------------------------------------------------------------
             * On recherche les infos sur les interventions
             */
            $nbSecondeJour = 0;
            $RechercheJourInterventionEvpExc->bindValue(':dateDebut', $jourSel->format('Y-m-d') . ' 00:00:00');
            $RechercheJourInterventionEvpExc->bindValue(':dateFin', $jourSel->format('Y-m-d') . ' 23:59:59');
            $RechercheJourInterventionEvpExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
            $RechercheJourInterventionEvpExc->execute();
            while ($InfoIntervention = $RechercheJourInterventionEvpExc->fetch(PDO::FETCH_OBJ)) {

                array_push($arOutput['jour'][$InfoEvp->idIntervenant],array('jour' => $dernierJour->format('Y-m-') . $jourRecherche . ' 23:59:59', 'frais' => $InfoIntervention->fraisRepas));

                /** -------------------------------------------------------------------------------
                 * On stock le nombre de seconde de l'intervention
                 */
                $nbSecondeJour += $InfoIntervention->nbSeconde;
                $listeHeureIntervention[$jourSel->format('Y-m-d')][$InfoIntervention->salaireBrut] += $InfoIntervention->nbSeconde;
                if (!is_null($InfoIntervention->fraisRepas)) {
                    $listeFraisRepas[$jourSel->format('Y-m-d')] = $InfoIntervention->fraisRepas;
                }
                if (!is_null($InfoIntervention->fraisTelephone)) {
                    $listeFraisTel[$jourSel->format('Y-m-d')] = $InfoIntervention->fraisTelephone;
                }
            }

            /** -----------------------------------------------------------------------------------
             * On limite le nombre de seconde a 25200
             */
            if ($nbSecondeJour >= 28800) {
                $nbSecondeTotal += $nbSecondeJour-3600;
                $nbSecondeJour =  $nbSecondeJour-3600;
            } else $nbSecondeTotal += $nbSecondeJour;

            $nbSecondeSup += ($boolAutorisationHS) ? $nbSecondeJour : 0;

            if ($boolAffichageHS && $nbSecondeSup > 126000) {
                $TotalSupplementaire = $nbSecondeSup - 126000;
                if ($TotalSupplementaire > 28800) {
                    $nbSecondeHS_25 += 28800;
                    $nbSecondeHS_50 += ($TotalSupplementaire - 28800);
                } else $nbSecondeHS_25 += $TotalSupplementaire;
            }

            $nbSecondeSup = ($jourSel->format('w') == 0) ? 0 : $nbSecondeSup;
        }

        /** ---------------------------------------------------------------------------------------
         * Montant repas + Commentaire
         */
        $montantRepas = 0;
        $commentaireRepas = 0;
        $listeTarifRepas = array(); $nbRepasFinal = 0;
        foreach ($listeFraisRepas as $dateRepas => $montant) {
            if ($montant > 0) {
                $montantRepas += $montant;
                $listeTarifRepas[$montant]++;
                $nbRepasFinal++;
            }
        }
        foreach ($listeTarifRepas as $montant => $qte) {
            $commentaireRepas .= (!empty($commentaireRepas)) ? ", ".round($qte)." repas a $montant Euro" : round($qte)." repas a $montant Euro";
        }

        /** ---------------------------------------------------------------------------------------
         * Montant telephone
         */
        $montantTel = 0;
        foreach($listeFraisTel as $dateTel => $montant){
            $montantTel += $montant;
        }

        /** ---------------------------------------------------------------------------------------
         * On recherche les differents tarif KM pour cet intervenant
         */
        $RechercheTauxKmIntervenantMoisExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
        $RechercheTauxKmIntervenantMoisExc->bindValue(':dateDebut', filter_input(INPUT_GET, 'dateEvp') . ' 00:00:00');
        $RechercheTauxKmIntervenantMoisExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
        $RechercheTauxKmIntervenantMoisExc->execute();
        while($InfoTaux = $RechercheTauxKmIntervenantMoisExc->fetch(PDO::FETCH_OBJ)){

            /** -----------------------------------------------------------------------------------
             * Initialisation du taux si non present dans la table frais pour saisi manuelle
             */
            $InsertionTauxKmExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
            $InsertionTauxKmExc->bindValue(':dateKm', filter_input(INPUT_GET, 'dateEvp'), PDO::PARAM_STR);
            $InsertionTauxKmExc->bindValue(':tarifKm', $InfoTaux->tarifKM, PDO::PARAM_STR);
            $InsertionTauxKmExc->bindValue(':totalKm',0, PDO::PARAM_INT);
            $InsertionTauxKmExc->execute();
        }

        $infoFraisIntervention = '';

        /** ---------------------------------------------------------------------------------------
         * Recherche des frais d'intervention
         */
        $RechercheFraisInterventionExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
        $RechercheFraisInterventionExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateEvp'));
        $RechercheFraisInterventionExc->execute();
        if($RechercheFraisInterventionExc->rowCount() > 0){
            $infoFraisIntervention = $RechercheFraisInterventionExc->fetch(PDO::FETCH_OBJ);
        }
        else {

            /** -----------------------------------------------------------------------------------
             * Initialisation des frais si non existant
             */
            $InsertionFraisInterventionExc->bindValue(':idIntervenant',  $InfoEvp->idIntervenant);
            $InsertionFraisInterventionExc->bindValue(':dateFrais', filter_input(INPUT_GET, 'dateEvp'));
            $InsertionFraisInterventionExc->execute();

            $RechercheFraisInterventionExc->execute();
            $infoFraisIntervention = $RechercheFraisInterventionExc->fetch(PDO::FETCH_OBJ);
        }

        /** ---------------------------------------------------------------------------------------
         * Recherche des frais KM
         */
        $listeKmIntervenant = array(); $nbKmTotal = 0; $montantKm = 0; $commentaireKm = '';
        $RechercheKmIntervenantExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant);
        $RechercheKmIntervenantExc->bindValue(':dateKilometre', filter_input(INPUT_GET, 'dateEvp'));
        $RechercheKmIntervenantExc->execute();
        while($InfoKm = $RechercheKmIntervenantExc->fetch(PDO::FETCH_OBJ)){
            $listeKmIntervenant[$InfoKm->tarifKilometre] += round($InfoKm->totalKilometre, 2);
            $nbKmTotal += round($InfoKm->totalKilometre, 2);
            $montantKm += round($InfoKm->tarifKilometre*$InfoKm->totalKilometre, 2);

            if($InfoKm->totalKilometre > 0)
                $commentaireKm .= (empty($commentaireKm))?
                    doubleFormatPoint($InfoKm->totalKilometre)." km(s) a $InfoKm->tarifKilometre Euro":
                    ", ".doubleFormatPoint($InfoKm->totalKilometre)." km(s) a $InfoKm->tarifKilometre Euro";

        }


        /** ---------------------------------------------------------------------------------------
         * Recherche des interventions
         */
        $nbIntervention = 0; $commentaireIntervention = ''; $typeIntervention = array();
        $RechercheListeInterventionMoisExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
        $RechercheListeInterventionMoisExc->bindValue(':dateDebut', filter_input(INPUT_GET, 'dateEvp') . ' 00:00:00');
        $RechercheListeInterventionMoisExc->bindValue(':dateFin', $dernierJour->format('Y-m-d') . ' 23:59:59');
        $RechercheListeInterventionMoisExc->execute();
        while($InfoIntervention = $RechercheListeInterventionMoisExc->fetch(PDO::FETCH_OBJ)){
            $nbIntervention += $InfoIntervention->nbIntervention;
            $typeIntervention[$InfoIntervention->libelleMission] = $InfoIntervention->nbIntervention;
        }

        $Remun_CIDD      = 0;
        $Indem_F         = 0;
        $Nb_Heure_CIDD   = 0;
        $Nb_Inter_CIDD   = 0;

        /** ---------------------------------------------------------------------------------------
         * Recherche des contrats CIDD
         */
        $RechercheInterventionCiddExc->bindValue(':idIntervenant', $InfoEvp->idIntervenant, PDO::PARAM_INT);
        $RechercheInterventionCiddExc->bindValue(':dateContrat', filter_input(INPUT_GET, 'dateEvp'), PDO::PARAM_STR);
        $RechercheInterventionCiddExc->execute();
        while ($InfoContrat = $RechercheInterventionCiddExc->fetch(PDO::FETCH_OBJ)) {

            /** @var $Liste_PDV Liste des points de vente a visiter */
            $listePDV = preg_split('/_/', $InfoContrat->listePDV);
            foreach ($listePDV as $idPdv) {
                if (!empty($idPdv)) {
                    $Nb_Inter_CIDD++;
                    @$typeIntervention[$InfoContrat->libelleMission]++;
                }
            }

            $Remun_CIDD += $InfoContrat->remunerationBrute * $InfoContrat->dureeMensuelle;
            $Nb_Heure_CIDD += $InfoContrat->dureeMensuelle;
            $Indem_F += $InfoContrat->indemniteForfaitaire * ($InfoContrat->dureeMensuelle);
        }

        $nbIntervention += $Nb_Heure_CIDD;


        foreach($typeIntervention as $libelleMission => $nb){
            $commentaireIntervention .= (empty($commentaireIntervention)) ? $libelleMission.' : '.$nb.' intervention(s)' : ', '.$libelleMission.' : '.$nb.' intervention(s)';
        }



        /** On calcul la remuneration de l'intervenant */
        $nbHeureTotal   = 0; $montantTotal = 0; $nbHeureTotal2 = 0;
        foreach($listeHeureIntervention as $jour => $duree){
            foreach($duree as $tarif=>$nbInter){
                if($nbInter >= 28800){
                    $nbHeureTotal += $nbInter-3600;
                    $montantTotal += (($nbInter-3600)*$tarif);
                }else {
                    $nbHeureTotal += $nbInter;
                    $montantTotal += ($nbInter*$tarif);
                }
            }
        }

        /** Formatage du nombre d'heure et du montant */
        $nbHeureTotal = round(($nbHeureTotal/3600),2)+$Nb_Heure_CIDD;
        $montantTotal  = ($montantTotal/3600)+$Remun_CIDD;

        /** Conversion des secondes en heures */
        $nbHS_25 = round(($nbSecondeHS_25/3600),2);
        $nbHS_50 = round(($nbSecondeHS_50/3600),2);

        $nbHeureNormal = $nbHeureTotal-$nbHS_25-$nbHS_50+$Nb_Heure_CIDD;
        $montantTotalNormal = round((($nbHeureNormal/$nbHeureTotal)*round($montantTotal,2)),2)+$Remun_CIDD;


        if($nbIntervention > 0) {
            $arOutput['aaData'][] = array(
                $InfoEvp->idIntervenant . ' - ' . $InfoEvp->nomIntervenant . ' ' . $InfoEvp->prenomIntervenant,
                $nbIntervention,
                $nbHeureTotal,
                doubleFormatPoint($montantTotal),
                $nbRepasFinal,
                $montantRepas,
                doubleFormatPoint($nbKmTotal),
                doubleFormatPoint($montantKm),
                $infoFraisIntervention->montantRepriseAccompte,
                $infoFraisIntervention->montantPrimeObjectif,
                $infoFraisIntervention->montantPrimeRDV,
                $infoFraisIntervention->montantIndemniteFormation,
                $infoFraisIntervention->montantFraisJustificatif,
                $infoFraisIntervention->montantIndemniteTelephone
            );

            $ListeIntervenantContratCDD[$InfoEvp->idIntervenant] = true;

            $MatriculeIntervenant = '';
            for ($i = strlen($InfoEvp->idIntervenant); $i < 5; $i++)
                $MatriculeIntervenant .= '0';
            $MatriculeIntervenant .= $InfoEvp->idIntervenant;

            /** -----------------------------------------------------------------------------------
             * Création du fichier pour les EVP
             * ICI, de A (1) à Z (26)
             * A-1; B-2; C-3; D-4; E-5; F-6; G-7; G-8; I-9; J-10; K-11; L-12; M-13; N-14;
             * O-15; P-16; Q-17; R-18; S-19; T-20; U-21; V-22; W-23; X-24; Y-25; Z-26
             */
            $col=1;
            $tab[$col++] = $MatriculeIntervenant;
            #$tab[$col++] = '00001';
            #$tab[$col++] = $dernierJour->format('d/m/Y');
            $tab[$col++] = $nbIntervention;
            $tab[$col++] = preg_replace('(\r\n|\n|\r)', '', $commentaireIntervention);
            $tab[$col++] = doubleFormatPoint($nbHeureNormal) ;
            $tab[$col++] = doubleFormatPoint($montantTotalNormal);
            $tab[$col++] = doubleFormatPoint($nbRepasFinal);
            $tab[$col++] = doubleFormatPoint($montantRepas) ;
            $tab[$col++] = preg_replace('(\r\n|\n|\r)', '', $commentaireRepas);
            $tab[$col++] = (($nbKmTotal > 0) ? doubleFormatPoint($nbKmTotal) : '') ;
            $tab[$col++] = (($montantKm > 0) ? doubleFormatPoint($montantKm) : '');
            $tab[$col++] = $commentaireKm ;
            $tab[$col++] = (($infoFraisIntervention->montantRepriseAccompte > 0) ? doubleFormatPoint($infoFraisIntervention->montantRepriseAccompte) : '');
            #$tab[$col++] = '';
            $tab[$col++] = (($infoFraisIntervention->montantPrimeObjectif > 0) ? doubleFormatPoint($infoFraisIntervention->montantPrimeObjectif) : '');
            $tab[$col++] = preg_replace('(\r\n|\n|\r)', '', $infoFraisIntervention->commentairePrimeObjectif);
            $tab[$col++] = (($montantTel > 0) ? doubleFormatPoint($montantTel) : '');
            #$tab[$col++] = '';
            #$tab[$col++] = (($infoFraisIntervention->montantPriseRDV > 0) ? doubleFormat($infoFraisIntervention->montantPriseRDV) : '');
            $tab[$col++] = (($infoFraisIntervention->montantIndemniteFormation > 0) ? doubleFormatPoint($infoFraisIntervention->montantIndemniteFormation) : '');
            #$tab[$col++] = '';
            $tab[$col++] = (($infoFraisIntervention->montantFraisJustificatif > 0) ? doubleFormatPoint($infoFraisIntervention->montantFraisJustificatif) : '');
            #$tab[$col++] = '';

            /** -----------------------------------------------------------------------------------
             * Y-25; Z-26
             * 25 -> montantAutreFrais correspond maintenant à "indemnités RDV téléphone"
             * 26 -> montantAutrePrime
             */
            $tab[$col++] = (($infoFraisIntervention->montantAutreFrais > 0) ? doubleFormatPoint($infoFraisIntervention->montantAutreFrais) : '') ;
            $tab[$col++] = (($infoFraisIntervention->montantAutrePrime > 0) ? doubleFormatPoint($infoFraisIntervention->montantAutrePrime) : '') ;
            #$tab[25] = doubleFormat($infoFraisIntervention->montantIndemniteTelephone);

            /** -----------------------------------------------------------------------------------
             * Création du fichier pour les EVP
             * ICI, de AA (27) à AG (34)
             * AA-27; AB-28; AC-29; AD-30; AE-31; AF-32; AG-33; AH-34
             * O-15; P-16; Q-17; R-18; S-19; T-20; U-21; V-22; W-23; X-24; Y-25; Z-26
             */
            $tab[$col++] = preg_replace('(\r\n|\n|\r)', '', $infoFraisIntervention->commentaireAutrePrime) ;

            $tab[$col++] = (($infoFraisIntervention->montantIndemniteAnnulation > 0) ? doubleFormatPoint($infoFraisIntervention->montantIndemniteAnnulation) : '') ;
            $tab[$col++] = preg_replace('(\r\n|\n|\r)', '', $infoFraisIntervention->commentaireIndemniteAnnulation);

            #$tab[$col++] = (($Indem_F > 0) ? doubleFormat($Indem_F) : '') ;
            #$tab[$col++] = '';
            $tab[$col++] = (($nbHS_25 > 0) ? doubleFormatPoint($nbHS_25) : '');
            $tab[$col++] = (($nbHS_50 > 0) ? doubleFormatPoint($nbHS_50) : '');

            #$tab[''] = (($infoFraisIntervention->montantAutreFrais > 0) ? doubleFormat($infoFraisIntervention->montantAutreFrais) : '') ;

            $chaine = implode(';', $tab). "\r\n";
            fwrite($fp, $chaine);
        }
    }

    /** Recherche des contrat CIDD pour le mois selectionne */
    $RechercheContratCiddExc->bindValue(':DteContrat', filter_input(INPUT_GET, 'dateEvp'), PDO::PARAM_STR);
    $RechercheContratCiddExc->execute();
    while($InfoCidd = $RechercheContratCiddExc->fetch(PDO::FETCH_OBJ)) {
        $chaine = "";
        $TotalIntervention = 0;
        $Type_Intervention = array();
        $Remun_CIDD = 0;
        $Nb_Heure_CIDD = 0;
        $Indem_F = 0;

        /** On verifi que l'intervenant ne possede pas de contrat "normal" */
        if (!isset($InfoEvp->idIntervenant[$Cidd->FK_Id_Intervenant])) {

            /** Recherche des contrats */
            $RechercheInterventionCiddExc->bindValue(':idIntervenant', $InfoCidd->FK_idIntervenant, PDO::PARAM_INT);
            $RechercheInterventionCiddExc->bindValue(':dateContrat', filter_input(INPUT_GET, 'dateEvp'), PDO::PARAM_STR);
            $RechercheInterventionCiddExc->execute();
            while ($InfoContrat = $RechercheInterventionCiddExc->fetch(PDO::FETCH_OBJ)):

                /** @var $Liste_PDV Liste des points de vente a visiter */
                $listePDV = preg_split('/_/', $InfoContrat->listePDV);
                foreach ($listePDV as $idPdv) {
                    if(!empty($idPdv)) {
                        $TotalIntervention++;
                        @$Type_Intervention[$InfoContrat->libelleMission]++;
                    }
                }

                $Remun_CIDD += $InfoContrat->remunerationBrute * $InfoContrat->dureeMensuelle;
                $Nb_Heure_CIDD += $InfoContrat->dureeMensuelle;
                $Indem_F += $InfoContrat->indemniteForfaitaire * ($InfoContrat->dureeMensuelle);
            endwhile;

            /** Nombre d'intervention et commentaire  */
            $CommentaireIntervention = "";
            foreach ($Type_Intervention as $Type => $Nb) {
                $CommentaireIntervention .= ($CommentaireIntervention == '') ? "$Type : $Nb intervention(s)" : ", $Type : $Nb intervention(s)";
            }

            $MatriculeIntervenant = '';
            for ($i = strlen($InfoCidd->FK_idIntervenant); $i < 5; $i++)
                $MatriculeIntervenant .= '0';
            $MatriculeIntervenant .= $InfoCidd->FK_idIntervenant;


            $Indem_F = (intval($Indem_F) != 0) ? $Indem_F : '';
            $chaine = "AAAA"."$MatriculeIntervenant;00001;".$dernierJour->format('d/m/Y').";$TotalIntervention;" .
                preg_replace("(\r\n|\n|\r)", '', $CommentaireIntervention) .
                ";$Nb_Heure_CIDD;".doubleFormatPoint($Remun_CIDD).";;;;;;;;;;;;;;;;;;;;;;;$Indem_F;;;;;;;;;;\r\n";
            fwrite($fp, $chaine);
        }
    }
}



fclose($fp);
$arOutput['fichier'] = $fName;


#$records = array (
#    $arOutput,
#    "File" => $fName
#);

print json_encode($arOutput);
#print json_encode($arOutput);