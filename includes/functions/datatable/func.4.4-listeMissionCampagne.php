<?php
/**
 * Liste des missions pour une campagne
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

$RechercheMissionCampagneExc->bindValue(':idCampagne', filter_input(INPUT_GET, 'idCampagne'));
$RechercheMissionCampagneExc->execute();
while($InfoMission = $RechercheMissionCampagneExc->fetch(PDO::FETCH_OBJ)){
    $arOutput['aaData'][] = array(
        $InfoMission->libelleMission,
        $InfoMission->libelleTypeMission,
        $InfoMission->libelleGamme,
        $InfoMission->nbIntervention.' intervention(s) / '.$InfoMission->nbInterPrevisionnel.' prévisionnelle(s)',
        @round(((($InfoMission->nbIntervention-$InfoMission->nbInterventionSansContrat)*100)/$InfoMission->nbIntervention),2).' %',
        '<button class="btn btn-xs btn-primary no-margin" data-id="'.$InfoMission->idMission.'">Fiche mission</button>'
    );
}

print json_encode($arOutput);