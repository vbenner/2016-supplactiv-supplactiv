<?php
ini_set('display_errors','on');
/**
 * Liste des missions
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/**
 * Requete sql
 */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/**
 *  Initialisation de la variable de sortie
 */
$arOutput = array('aaData' => array());

/**
 * Date du jour
 */
$dateJour = new DateTime(date('Y-m-d').' 00:00:00');

/**
 * Recherche des missions
 */
$RechercheListeMissionExc->execute();
while($InfoMission = $RechercheListeMissionExc->fetch(PDO::FETCH_OBJ)){

    /** Initialisation des dates */
    $dateDebut = new DateTime($InfoMission->dateDebut.' 00:00:00');
    $dateFin = new DateTime($InfoMission->dateFin.' 23:59:59');

    $arOutput['aaData'][] = array(
        'DT_RowClass' => ($dateFin < $dateJour) ? 'danger' : '',
        $InfoMission->libelleClient,
        $InfoMission->libelleCampagne,
        $InfoMission->libelleMission,
        '<span class="fs0">'.$InfoMission->dateDebut.'</span>'.$dateDebut->format('d-m-Y'),
        '<span class="fs0">'.$InfoMission->dateFin.'</span>'.$dateFin->format('d-m-Y'),
        '<button class="btn btn-xs '.($InfoMission->typeMission == 3 ? 'btn-primary' : 'btn-danger'). ' no-margin" data-id="'.$InfoMission->idMission.'" data-campagne="'.$InfoMission->idCampagne.'" data-client="'.$InfoMission->idClient.'"><i class="icon-book-open"></i> Fiche Mission</button>'
    );
}

print json_encode($arOutput);
