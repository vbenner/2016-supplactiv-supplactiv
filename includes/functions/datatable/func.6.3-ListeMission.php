<?php
/**
 * Liste des missions
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';
# ------------------------------------------------------------------------------------
# Connexion a la BDD
# ------------------------------------------------------------------------------------
include_once '../../../_config/PDO.Connexion.php';
$Connexion = DbConnexion::getInstance();


$sqlRechercheListeMission = '
SELECT *
FROM su_mission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
	INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient';
$RechercheLstMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheListeMission);

$sqlRechercheTypeContrat = '
SELECT *
FROM CONTRAT_CIDD_TYPE
WHERE FK_Id_Mission = :IdMission';
$RechercheTypeContratExc = $Connexion->prepare($sqlRechercheTypeContrat);

/** @var $arOutput Initialisation du tableau de sortie */
$arOutput = array('aaData' => array());

/** Recherche des missions */
$RechercheLstMissionExc->execute();
while($InfoContrat = $RechercheLstMissionExc->fetch(PDO::FETCH_OBJ)) {

    $ContratType = false;
    $Dte_Contrat_Type = '';

    $RechercheTypeContratExc->bindValue(':IdMission', $InfoContrat->idMission, PDO::PARAM_INT);
    $RechercheTypeContratExc->execute();
    while ($InfoMission = $RechercheTypeContratExc->fetch(PDO::FETCH_OBJ)) {
        $ContratType = true;
        $Dte_Contrat_Type = $InfoMission->Date_Contrat;
    }

    $arOutput['aaData'][] = array(
        $InfoContrat->libelleClient,
        $InfoContrat->libelleCampagne,
        $InfoContrat->libelleMission,
        ($ContratType) ? '<button class="btn btn-success btn-xs" onclick="modificationContratMission(' . $InfoContrat->idMission . ')">Contrat type cr&eacute;e le : ' . $Dte_Contrat_Type . ' - Modifier</button>' : '<button class="btn btn-success btn-xs"  onclick="creationContratMission(' . $InfoContrat->idMission . ')">Cr&eacute;er un contrat type</button>', ($ContratType) ? '<button class="btn btn-success btn-xs" onclick="creationContrat(' . $InfoContrat->idMission . ')">Cr&eacute;er un contrat</button>' : ''
    );
}

print json_encode($arOutput);
