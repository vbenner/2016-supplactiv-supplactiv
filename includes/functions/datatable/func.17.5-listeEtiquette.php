<?php
/**
 * Recherche des etiquettes du personnel en fonction d'une periode contrat
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Requête sql */
require_once dirname(__FILE__) . '/../../queries/queries.bdd2web.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** Formatage des dates de recherche */
$dateDebut = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheD_17_5']);
$dateFin = DateTime::createFromFormat('d/m/Y', $_SESSION['dateRechercheF_17_5']);

/** Recherche des interventions */
$RechercheInterventionPeriodeExc->bindValue(':dateDebut', $dateDebut->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$RechercheInterventionPeriodeExc->bindValue(':dateFin', $dateFin->format('Y-m-d').' 23:59:00', PDO::PARAM_STR);
$RechercheInterventionPeriodeExc->execute();
while($InfoIntervention = $RechercheInterventionPeriodeExc->fetch(PDO::FETCH_OBJ)){
    $dep = preg_split('/,/', $InfoIntervention->depIntervention); $depIntervention = '';
    foreach($dep as $idDepartement){
        if(!empty($idDepartement))
        $depIntervention .= (empty($depIntervention)) ? '<span class="badge badge-info badge-roundless">D'.$idDepartement.'</span>' : ' <span class="badge badge-info badge-roundless">D'.$idDepartement.'</span>';
    }
    $arOutput['aaData'][] = array(
        $InfoIntervention->idIntervenant,
        $InfoIntervention->nomIntervenant,
        $InfoIntervention->prenomIntervenant,
        $InfoIntervention->codePostalIntervenant,
        $InfoIntervention->villeIntervenant,
        $InfoIntervention->telephoneIntervenant_A,
        $InfoIntervention->mobileIntervenant,
        $depIntervention,
        '<input type="checkbox" data-id="'.$InfoIntervention->idIntervenant.'" class="chk" checked/>'
    );
}

print json_encode($arOutput);

