<?php

/**
 * @author Kevin MAURICE - Page UP
 * @detail Liste des interlocuterus agence
 */

/** Connexion a la base de donnees */
require_once dirname ( __FILE__ ) . '/../../../_config/config.sql.php';

/** Fichier SQL */
require_once dirname ( __FILE__ ) . '/../../queries/queries.bdd2web.php';

$sqlRechercheAdministrateurAgence = '
SELECT *
FROM du_utilisateur';
$RechercheAdministrateurAgenceExc = DbConnexion::getInstance()->prepare($sqlRechercheAdministrateurAgence);

$arOutput = array('aaData' => array());

$RechercheInterlocuteurAgenceExc->execute();
while($Info = $RechercheInterlocuteurAgenceExc->fetch(PDO::FETCH_OBJ)){
    if($_SESSION ['idUtilisateur'.PROJECT_NAME] != $InfoAdministrateur->idUtilisateur)
        $arOutput['aaData'][] = array(
            '<input type="text" id="ztNom_'.$Info->idInterlocuteurAgence.'" class="form-control" value="'.$Info->nomInterlocuteurAgence.'" />',
            '<input type="text" id="ztPrenom_'.$Info->idInterlocuteurAgence.'" class="form-control" value="'.$Info->prenomInterlocuteurAgence.'" />',
            '<input type="text" id="ztTel_'.$Info->idInterlocuteurAgence.'" class="form-control" value="'.$Info->telInterlocuteurAgence.'" />',
            '<input type="text" id="ztMail_'.$Info->idInterlocuteurAgence.'" class="form-control" value="'.$Info->mailInterlocuteurAgence.'" />',
            '<button class="btn btn-xs bg bg-green btn-block btn-modif" data-id="'.$Info->idInterlocuteurAgence.'">Modifier</button>'
        );
}

print json_encode($arOutput);