<?php
/**
 * Liste des PDV pour convention de forfait
 *
 * @author Kevin MAURICE - Page UP
 * @copyright Page UP
 */

/**
 * Connexion a la base de donnees
 */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

# ------------------------------------------------------------------------------------
# Initialisation de mes donn�es de sortie
# ------------------------------------------------------------------------------------
$arOutput = array(); $arOutput['aaData'] = array();

$sqlRechercheLstPdv = '
SELECT libellePdv, codePostalPdv, idPdv, villePdv
FROM su_pdv
	INNER JOIN su_intervention ON su_intervention.FK_idPdv = su_pdv.idPdv
WHERE FK_idMission = :IdMission
GROUP BY idPdv';
$RechercheLstPdvExc = DbConnexion::getInstance()->prepare($sqlRechercheLstPdv);

$RechercheLstPdvExc->bindValue(':IdMission', $_SESSION['ID_Mission_CreationContrat_6_7'], PDO::PARAM_INT); 
$RechercheLstPdvExc->execute();
while($InfoPdv = $RechercheLstPdvExc->fetch(PDO::FETCH_OBJ)){
	
	# ------------------------------------------------------------------------------------
	# Ajout du PDV dans la tableau de sortie
	# ------------------------------------------------------------------------------------
   $arOutput['aaData'][] = array(
	    /* 01 - Code CIP           */ '<input type="checkbox" id="PdvConvention_'.$InfoPdv->idPdv.'"/>',
	    /* 02 - Libelle PDV        */ $InfoPdv->libellePdv,
	    /* 03 - Code Postal        */ $InfoPdv->codePostalPdv.' - '.$InfoPdv->villePdv
    );
}

# ------------------------------------------------------------------------------------
# Encodage en Json 
# ------------------------------------------------------------------------------------
echo json_encode($arOutput);
?>