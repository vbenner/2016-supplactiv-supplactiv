<?php

/** -----------------------------------------------------------------------------------------------
 * Liste des accès = liste des connexions à la base de données
 *
 * @author Vincenbt BENNER / Page Up
 * @copyright ©Page Up
 */

/** Connexion a la base de donnees */
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

/** Initialisation de la variable de sortie */
$arOutput = array('aaData' => array());

/** -----------------------------------------------------------------------------------------------
 * Il y a forcément des données de SESSION
 */
$idUser = $_SESSION['idSalarie_20_1'];
$du = date_create_from_format('d/m/Y', $_SESSION['dateRechercheD_20_1']);
$au = date_create_from_format('d/m/Y', $_SESSION['dateRechercheF_20_1']);

/** -----------------------------------------------------------------------------------------------
 * On crée une requête pour savoir qui s'est connecté entre ce laps de dates
 * (hors root)
 */
$sqlGetListeConnexions = '
SELECT DATE_FORMAT(MAX(DUC.lastConnexion), \'%d/%m/%Y\') AS maxDate,
  MIN(DATE_FORMAT(DUC.lastConnexion, \'%H:%i:%s\')) AS minTime,
  MAX(DATE_FORMAT(DUC.lastConnexion, \'%H:%i:%s\')) AS maxTime,
  SUM(if(DAYOFWEEK(DUC.lastConnexion) = 1 OR DAYOFWEEK(DUC.lastConnexion) = 7, 1, 0)) AS weekend,
  DU.idUtilisateur as idu, DU.nomUtilisateur, DU.prenomUtilisateur
FROM du_utilisateur_connexion DUC
	INNER JOIN du_utilisateur DU ON DU.idUtilisateur = DUC.FK_idUtilisateur 
WHERE 
(
  lastConnexion >= :datedu
  AND lastConnexion <= :dateau
)
AND DUC.FK_idUtilisateur != 1
'.($idUser != 0 ? ' AND DU.idUtilisateur = :idu' : '').'
GROUP BY DU.idUtilisateur
ORDER BY MaxDate DESC
';

/** -----------------------------------------------------------------------------------------------
 * SELECT DATE WEEK END ?
 */

$getListeConnexionsExec = DbConnexion::getInstance()->prepare($sqlGetListeConnexions);
$getListeConnexionsExec->bindValue(':datedu', $du->format('Y-m-d').' 00:00:00', PDO::PARAM_STR);
$getListeConnexionsExec->bindValue(':dateau', $au->format('Y-m-d').' 23:59:59', PDO::PARAM_STR);
if ($idUser != 0) {
    $getListeConnexionsExec->bindValue(':idu', $idUser, PDO::PARAM_INT);
}
$getListeConnexionsExec->execute();
while($getListeConnexions = $getListeConnexionsExec->fetch(PDO::FETCH_OBJ)) {

    /** -------------------------------------------------------------------------------------------
     * On va vérifier que, dans cette plage de date, il n'y a pas d'accès un jour de WEEK END ?
     */

    $arOutput['aaData'][] = array(
        $getListeConnexions->maxDate,
        $getListeConnexions->nomUtilisateur.' '.$getListeConnexions->prenomUtilisateur,
        $getListeConnexions->minTime,
        $getListeConnexions->maxTime,
        $getListeConnexions->weekend,
        (
            $getListeConnexions->weekend > 0 ||
            $getListeConnexions->minTime <= '08:00:00' ||
            $getListeConnexions->maxTime >= '18:30:00' ? 'OUI' : ''
        ),
        '<a class="btn btn-xs btn-primary no-margin btn-block" data-id="'.$getListeConnexions->idu.'">
        <i class="icon-folder-alt"></i> DÉTAIL</a>'
    );
}

/** -----------------------------------------------------------------------------------------------
 * On va maintenant récupérer la liste des
 */
print json_encode($arOutput);
