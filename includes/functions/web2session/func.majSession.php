<?php
# ------------------------------------------------------------------------------------
# @author  : Kevin MAURICE / PAGE UP 2013
# @date    : 12/08/2013
# @version : 1
# @info    : MAJ de la session
# @detail  :
# ------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------------
# Connexion a la base de données
# ------------------------------------------------------------------------------------
require_once dirname(__FILE__) . '/../../../_config/config.sql.php';

# ------------------------------------------------------------------------------
# On liste les variables post pour les passer en Session
# ------------------------------------------------------------------------------
#if(getValiditySession()){
	foreach($_POST as $Nom=>$Valeur){
		if($Nom != '' && $Nom != 'EnSession')
			$_SESSION[$Nom] = $Valeur; 
	}
	echo json_encode(array('STATUT' => 1, 'INFO_COMP' => ''));

#}else echo json_encode(array('STATUT' => 0, 'INFO_COMP' => 'Session invalide'));

