<?php

/**
 * Recherche des missions dans la BDD
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheListeMissionExc
 */
$sqlRechercheListeMission = '
SELECT *
FROM v_liste_mission
';
$RechercheListeMissionExc  = DbConnexion::getInstance()->prepare($sqlRechercheListeMission);

/**
 * Recherche des points de vente dans la BDD
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheListePdvExc
 *
 * Ancienne vue :
 *
 * select `PDV`.`idPdv` AS `idPdv`,`PDV`.`codeMerval` AS `codeMerval`,`PDV`.`libellePdv` AS `libellePdv`,`PDV`.`codePostalPdv` AS `codePostalPdv`,`PDV`.`villePdv` AS `villePdv`,`PDV`.`telephoneMagasin` AS `telephoneMagasin`,`PDV`.`faxPdv` AS `faxPdv`,`PDV`.`adressePdv_A` AS `adressePdv_A`,(select count(0) from `su_intervention` where (`su_intervention`.`FK_idPdv` = `PDV`.`idPdv`)) AS `nbIntervention` from `su_pdv` `PDV`
 */
$sqlRechercheListePdv = '
SELECT *
FROM v_liste_pdv';
$RechercheListePdvExc  = DbConnexion::getInstance()->prepare($sqlRechercheListePdv);

/**
 * Recherche des informations sur un point de vente
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheInfoPdvExc
 * @param idPdv Identifiant du point de vente
 */
$sqlRechercheInfoPdv = '
SELECT *
FROM su_pdv
	LEFT JOIN su_pdv_interlocuteur ON su_pdv_interlocuteur.FK_idPdv = su_pdv.idPdv
	LEFT JOIN su_pdv_interlocuteur_fonction ON su_pdv_interlocuteur.FK_idFonctionInterlocuteurPDV = su_pdv_interlocuteur_fonction.idFonctionInterlocuteurPDV
WHERE idPdv = :idPdv';
$RechercheInfoPdvExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoPdv);

/**
 * Recherche des categories de point de vente
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheCategoriePdvExc
 */
$sqlRechercheCategoriePdv = '
SELECT *
FROM su_pdv_categorie
ORDER BY libelleCategoriePdv';
$RechercheCategoriePdvExc = DbConnexion::getInstance()->prepare($sqlRechercheCategoriePdv);

/**
 * Recherche des fonctions de point de vente
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheCategoriePdvExc
 */
$sqlRechercheInterlocuteurPdvFonction = '
SELECT *
FROM su_pdv_interlocuteur_fonction
ORDER BY libelleFonctionInterlocuteurPDV';
$RechercheInterlocuteurPdvFonctionExc = DbConnexion::getInstance()->prepare($sqlRechercheInterlocuteurPdvFonction);

/**
 * Recherche des sous-categories de point de vente
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheSousCategoriePdvExc
 * @param idCategorie Identifiant de la categorie
 */
$sqlRechercheSousCategoriePdv = '
SELECT *
FROM su_pdv_sous_categorie
WHERE FK_idCategorie = :idCategorie
ORDER BY libelleSousCategorie';
$RechercheSousCategoriePdvExc = DbConnexion::getInstance()->prepare($sqlRechercheSousCategoriePdv);

/**
 * Recherche des interventions sur un point de vente
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheListePdvExc
 * @param idPdv Identifiant du point de vente
 */
$sqlRechercheListeInterventionPdv = '
SELECT *
FROM v_liste_intervention_pdv
WHERE FK_idPdv = :idPdv
AND (NOT (
    ISNULL(dateDebutIntervention) AND
    ISNULL(dateFinIntervention) AND
    ISNULL(FK_idContrat))
)
';
$RechercheListeInterventionPdvExc  = DbConnexion::getInstance()->prepare($sqlRechercheListeInterventionPdv);

/**
 * Recherche des intervenants
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheListeIntervenantExc
 */
$sqlRechercheListeIntervenant = '
SELECT *
FROM su_intervenant
ORDER BY nomIntervenant';
$RechercheListeIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheListeIntervenant);

$sqlRechercheListeIntervenantNoAuto = '
SELECT *
FROM su_intervenant
WHERE boolAutoEntrepreneuse="NON"
ORDER BY nomIntervenant
';
$RechercheListeIntervenantNoAutoExec = DbConnexion::getInstance()->prepare($sqlRechercheListeIntervenantNoAuto);

/**
 * Recherche des departements d'intervenant
 *
 * @author Kevin MAURICE - Page UP
 * @var  $RechercheListeIntervenantExc
 * @param idIntervenant Identifiant de l'intervenant
 */
$sqlRechercheDepartementIntervenant = '
SELECT *
FROM v_liste_departement_intervenant
WHERE idIntervenant = :idIntervenant';
$RechercheDepartementIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheDepartementIntervenant);

/**
 * Recherche des minutes d'intervention pour un intervenant (4 derniers mois)
 *
 * @author Kevin MAUICE - Page UP
 * @var $RechercheMinuteInterventionExc
 * @param idIntervenant Identifiant de l'intervenant
 */
$sqlRechercheMinuteIntervention = '
SELECT *
FROM v_liste_minute_intervention
WHERE FK_idIntervenant = :idIntervenant';
$RechercheMinuteInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheMinuteIntervention);

/**
 * Recherche du nombre de contrat pour un intervenant
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheNbContratIntervenantExc
 */
$sqlRechercheNbContratIntervenant = '
SELECT idIntervenant, COUNT(idIntervention) AS nbCDD,
(
	SELECT COUNT(*)
	FROM su_contrat_cidd
	WHERE FK_idIntervenant = INTERVENANT.idIntervenant
) AS nbCIDD
FROM su_intervenant INTERVENANT
	LEFT JOIN su_intervention INTERVENTION ON INTERVENTION.FK_idIntervenant = INTERVENANT.idIntervenant AND FK_idContrat IS NOT NULL
GROUP BY idIntervenant';
$RechercheNbContratIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheNbContratIntervenant);

/**
 * Recherche des informations sur une mission
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInfoMissionExc
 */
$sqlRechercheInfoMission = '
SELECT *
FROM su_mission
  INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
  INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
  INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient
  LEFT JOIN du_utilisateur ON su_mission.decideur_id = du_utilisateur.idUtilisateur
WHERE idMission = :idMission
';
$RechercheInfoMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoMission);

/**
 * Recherche des interventions
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInterventionExc
 */
$sqlRechercheIntervention = '
SELECT idPdv, idIntervenant, nomIntervenant, prenomIntervenant, codeMerval, libellePdv, adressePdv_A, codePostalPdv, villePdv, telephoneMagasin
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE FK_idMission = :idMission
GROUP BY idIntervenant, idPdv';
$RechercheInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervention);

/**
 * Recherche des clients
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheListeClientExc
 */
$sqlRechercheListeClient = '
SELECT idClient, libelleClient, adresseClient, codePostalClient, villeClient, telClient, emailClient,
  (
    SELECT COUNT(*)
    FROM su_campagne
      INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
    WHERE FK_idClient = client.idClient
  ) AS nbCampagne
FROM su_client client';
$RechercheListeClientExc = DbConnexion::getInstance()->prepare($sqlRechercheListeClient);

/**
 * Recherche des contrats
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheIdContratExc
 */
$sqlRechercheIdContrat = '
SELECT DISTINCT FK_idContrat AS idContrat, FK_idContrat, LPAD(FK_idContrat , 6, "0") AS libelleContrat
FROM su_intervention
  INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE dateDebut BETWEEN :dateDebut AND :dateFin AND boolContratArchive = :boolArchive';
$RechercheIdContratExc = DbConnexion::getInstance()->prepare($sqlRechercheIdContrat);


/**
 * Recherche des intervenantes
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheIntervenanteContratExc
 */
$sqlRechercheIntervenanteContrat = '
SELECT DISTINCT idIntervenant, CONCAT(LPAD(idIntervenant , 8, "0"), " - ", nomIntervenant, " ", prenomIntervenant) AS libelleIntervenant
FROM su_intervention
  INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
  INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE dateDebut BETWEEN :dateDebut AND :dateFin AND boolContratArchive = :boolArchive
ORDER BY nomIntervenant';
$RechercheIntervenanteContratExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenanteContrat);


/**
 * Recherche de la duree d'un contrat
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheDureeInterventionContratExc
 */
$sqlRechercheDureeInterventionContrat = '
SELECT TIMESTAMPDIFF(SECOND, dateDebut, dateFin) AS nbSeconde
FROM su_intervention
WHERE FK_idContrat = :idContrat';
$RechercheDureeInterventionContratExc = DbConnexion::getInstance()->prepare($sqlRechercheDureeInterventionContrat);

/**
 * Recherche des frais d'intervention
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheFraisInterventionExc
 */
$sqlRechercheFraisIntervention = '
SELECT *
FROM su_intervenant_frais
WHERE FK_idIntervenant = :idIntervenant AND dateFrais = :dateFrais';
$RechercheFraisInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheFraisIntervention);


/**
 * Recherche des differents tarifs KM pour une intervenante
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheKmInterventionExc
 */
$sqlRechercheKmIntervention = '
SELECT *
FROM su_intervenant_km
WHERE FK_idIntervenant = :idIntervenant AND dateKilometre = :dateFrais';
$RechercheKmInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheKmIntervention);

/**
 * Recherche des intervenantes travaillant pour un mois donnee
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheIntervenantMoisExc
 */
$sqlRechercheIntervenantMois = '
SELECT *
FROM su_intervenant
	INNER JOIN su_intervenant_frais ON su_intervenant_frais.FK_idIntervenant = su_intervenant.idIntervenant
WHERE dateFrais = :dateFrais
GROUP BY FK_idIntervenant
ORDER BY nomIntervenant
';
$RechercheIntervenantMoisExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenantMois);

/**
 * Recherche des differents taux KM
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheTauxKm
 */
$sqlRechercheTauxKm = '
SELECT DISTINCT tarifKilometre
FROM su_intervenant_km
WHERE dateKilometre = :dateFrais AND tarifKilometre > 0
ORDER BY tarifKilometre';
$RechercheTauxKmExc = DbConnexion::getInstance()->prepare($sqlRechercheTauxKm);

/**
 * Recherche des intervenants pour un mois données
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheIntervenantEvpExc
 */
$sqlRechercheIntervenantEvp = '
SELECT *
FROM (
    SELECT DISTINCT idIntervenant, nomIntervenant, prenomIntervenant
    FROM su_intervenant
        INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
    WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
        AND su_intervenant.boolAutoEntrepreneuse = "NON"
    
    UNION
    
    SELECT DISTINCT idIntervenant, nomIntervenant, prenomIntervenant
    FROM su_intervenant 
        INNER JOIN su_intervenant_frais ON su_intervenant.idIntervenant = su_intervenant_frais.FK_idIntervenant
    WHERE idIntervenant NOT IN (
        SELECT DISTINCT idIntervenant
        FROM su_intervenant
            INNER JOIN su_intervention ON su_intervention.FK_idIntervenant = su_intervenant.idIntervenant
        WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
    )
    AND su_intervenant_frais.dateFrais BETWEEN :dateDebut AND :dateFin
    AND su_intervenant.boolAutoEntrepreneuse = "NON"
    AND (
        montantPrimeRDV <> 0
        OR montantIndemniteFormation <> 0
        OR montantIndemniteTelephone <> 0
        OR montantAutreFrais <> 0
        OR montantFraisJustificatif <> 0
        OR montantPrimeObjectif <> 0
        OR montantIndemniteAnnulation <> 0
        OR montantAutrePrime <> 0
        OR montantRepriseAccompte <> 0
    )
) T
ORDER BY nomIntervenant
';
$RechercheIntervenantEvpExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenantEvp);

/**
 * Recherche des HS pour le mois dernier
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheHSIntervenantExc
 */
$sqlRechercheHSIntervenant = '
SELECT nbSeconde
FROM su_intervenant_hs
WHERE FK_idIntervenant = :idIntervenant AND dateHS = :dateHS';
$RechercheHSIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheHSIntervenant);

/**
 * Insertion des Heures Sup
 */
$sqlInsertionHSIntervenant = '
INSERT INTO su_intervenant_hs(FK_idIntervenant, dateHS, nbSeconde)
VALUES(:idIntervenant, :dateHS, :nbSeconde)';
$InsertionHSIntervenantExc = DbConnexion::getInstance()->prepare($sqlInsertionHSIntervenant);

/**
 * Recherche des Jour d'intervention pour le fichier des EVP
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheJourInterventionEvpExc
 */
$sqlRechercheJourInterventionEvp = '
SELECT idIntervention, SUM(TIME_TO_SEC(TIMEDIFF(dateFin,dateDebut))) AS nbSeconde,FK_idMission, idContrat, dateFin, dateDebut, salaireBrut, tarifKM, fraisRepas, fraisTelephone
FROM su_intervention
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	LEFT JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE FK_idIntervenant = :idIntervenant AND dateDebut BETWEEN :dateDebut AND :dateFin';
$RechercheJourInterventionEvpExc = DbConnexion::getInstance()->prepare($sqlRechercheJourInterventionEvp);

/**
 * Recherche des differents taux pour un intervenant et pour un mois
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheTauxKmIntervenantMoisExc
 */
$sqlRechercheTauxKmIntervenantMois = '
SELECT DISTINCT tarifKM
FROM su_intervention
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE FK_idIntervenant = :idIntervenant AND dateDebut BETWEEN :dateDebut AND :dateFin';
$RechercheTauxKmIntervenantMoisExc = DbConnexion::getInstance()->prepare($sqlRechercheTauxKmIntervenantMois);

/**
 * Recherche du nombre total de KM pour un mois donnee
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheKmIntervenantExc
 */
$sqlRechercheKmIntervenant = '
SELECT SUM(totalKilometre) AS totalKilometre, tarifKilometre
FROM su_intervenant_km
WHERE dateKilometre = :dateKilometre AND FK_idIntervenant = :idIntervenant
GROUP BY tarifKilometre';
$RechercheKmIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheKmIntervenant);

$sqlRechercheListeInterventionMois = '
SELECT COUNT(idIntervention) AS nbIntervention, libelleMission
FROM su_contrat
	INNER JOIN su_intervention ON su_intervention.FK_idContrat = su_contrat.idContrat
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE dateDebut BETWEEN :dateDebut AND :dateFin AND FK_idIntervenant = :idIntervenant
GROUP BY libelleMission';
$RechercheListeInterventionMoisExc = DbConnexion::getInstance()->prepare($sqlRechercheListeInterventionMois);

$sqlRechercheContratCidd = "
SELECT DISTINCT FK_idIntervenant
FROM su_contrat_cidd
	INNER JOIN su_mission ON su_mission.idMission = su_contrat_cidd.FK_idMission
WHERE dateContrat = :dateContrat";
$RechercheContratCiddExc = DbConnexion::getInstance()->prepare($sqlRechercheContratCidd);

$sqlRechercheInterventionCidd = "
SELECT listePDV, libelleMission, libelleGamme, remunerationBrute, dureeMensuelle, indemniteForfaitaire
FROM su_contrat_cidd
	INNER JOIN su_mission ON su_mission.idMission = su_contrat_cidd.FK_idMission
WHERE FK_idIntervenant = :idIntervenant AND dateContrat = :dateContrat";
$RechercheInterventionCiddExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionCidd);

/**
 * Recherche des heures d'interventions pour l'annee en cours
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheHeureInterventionAnneeCouranteExc
 */
$sqlRechercheHeureInterventionAnneeCourante = '
SELECT FK_idIntervenant, nomIntervenant, prenomIntervenant, dateDebut, dateFin, TIME_TO_SEC((TIMEDIFF(dateFin, dateDebut))) AS nbSecond
FROM su_intervention
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin';
$RechercheHeureInterventionAnneeCouranteExc = DbConnexion::getInstance()->prepare($sqlRechercheHeureInterventionAnneeCourante);

/**
 * Recherche des points de vente visite
 *
 * @param idIntervenant
 * @author Kevin MAURICE - Page UP
 * @var $RecherchePdvVisiteExc
 */
$sqlRecherchePdvVisite = '
SELECT *
FROM su_pdv
	INNER JOIN su_intervention ON su_intervention.FK_idPdv = su_pdv.idPdv
WHERE FK_idIntervenant = :idIntervenant
GROUP BY idPdv
ORDER BY libellePdv';
$RecherchePdvVisiteExc = DbConnexion::getInstance()->prepare($sqlRecherchePdvVisite);

/**
 * Recherche des DUE
 *
 * @param dateDebut
 * @param dateFin
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheDUEExc
 */
$sqlRechercheDUE = '    
SELECT idContrat, idIntervenant, nomIntervenant, prenomIntervenant, dateNaissance, villeNaissance, depNaissanceDUE, numeroSS, dateGeneration, libelleDepartement
FROM su_contrat
	INNER JOIN su_intervention ON su_intervention.FK_idContrat = su_contrat.idContrat
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	LEFT JOIN su_due ON su_due.FK_idContrat = su_contrat.idContrat
	LEFT JOIN su_departement ON su_departement.idDepartement = su_intervenant.depNaissanceDUE
WHERE dateDebut BETWEEN :dateDebut AND :dateFin
AND boolAutoEntrepreneuse = "NON"
GROUP BY idContrat, idIntervenant';
$RechercheDUEExc = DbConnexion::getInstance()->prepare($sqlRechercheDUE);

/**
 * Recherche de la duree d'un contrat pour l'export DUE
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheNbMinContratExc
 */
$sqlRechercheNbMinContrat = '
SELECT dateDebut, SUM(ROUND((TIME_TO_SEC((TIMEDIFF(dateFin, dateDebut))))/60)) AS nbMin
FROM su_intervention
WHERE FK_idContrat = :idContrat
GROUP BY dateDebut ORDER BY dateDebut';
$RechercheNbMinContratExc = DbConnexion::getInstance()->prepare($sqlRechercheNbMinContrat);

/**
 * Recherche des infos sur l'intervenant pour DUE
 *
 * @author Kevin MAURICE
 * @var $RechercheInfoIntervenantExc
 */
$sqlRechercheInfoIntervenant = '
SELECT idContrat, idIntervenant, nomIntervenant, prenomIntervenant, genre, DATE_FORMAT(dateNaissance, "%Y-%m-%d") AS dateNaissance, villeNaissance, depNaissanceDUE, numeroSS
FROM su_contrat CNT
	INNER JOIN su_intervention INTER ON INTER.FK_IdContrat = CNT.IdContrat
	INNER JOIN su_intervenant INTE ON INTE.IdIntervenant = INTER.FK_IdIntervenant
WHERE idContrat = :idContrat
GROUP BY idContrat, idIntervenant';
$RechercheInfoIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenant);

/**
 * Recherche des informations sur un client
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInfoClientExc
 */
$sqlRechercheInfoClient = '
SELECT *
FROM su_client
WHERE idClient = :idClient';
$RechercheInfoClientExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoClient);

/**
 * Recherche des filieres client
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheFiliereClientExc
 */
$sqlRechercheFiliereClient = '
SELECT *
FROM su_client_filiere
ORDER BY libelleFiliere';
$RechercheFiliereClientExc = DbConnexion::getInstance()->prepare($sqlRechercheFiliereClient);

/**
 * Recherche des activites client
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheActiviteClientExc
 */
$sqlRechercheActiviteClient = '
SELECT *
FROM su_client_activite
ORDER BY libelleTypeActivite';
$RechercheActiviteClientExc = DbConnexion::getInstance()->prepare($sqlRechercheActiviteClient);

/**
 * Recherche des interlocuteurs client
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInterlocuteurClientExc
 */
$sqlRechercheInterlocuteurClient = '
SELECT *,
(
	SELECT COUNT(*)
	FROM su_campagne WHERE FK_idInterlocuteurClient = INTER.idInterlocuteurClient
) AS nbCampagne
FROM su_client_interlocuteur INTER
WHERE FK_idClient = :idClient
ORDER BY nomInterlocuteurClient';
$RechercheInterlocuteurClientExc = DbConnexion::getInstance()->prepare($sqlRechercheInterlocuteurClient);

/**
 * Recherche des titres interlocuteurs client
 */
$sqlRechercheTitreInterlocuteurClient = '
SELECT *
FROM su_client_interlocuteur_titre
ORDER BY libelleTitreInterlocuteurClient';
$RechercheTitreInterlocuteurClientExc = DbConnexion::getInstance()->prepare($sqlRechercheTitreInterlocuteurClient);


/**
 * Recherche des campagnes du client
 */
$sqlRechercheCampagneClient = '
SELECT *,
(
  SELECT COUNT(*)
  FROM su_intervention
    INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
  WHERE FK_idCampagne = CAMPAGNE.idCampagne
) AS nbIntervention
FROM su_campagne CAMPAGNE
  INNER JOIN su_client_interlocuteur INTERLOCUTEUR ON INTERLOCUTEUR.idInterlocuteurClient = CAMPAGNE.FK_idInterlocuteurClient
WHERE FK_idClient = :idClient
';
$RechercheCampagneClientExc = DbConnexion::getInstance()->prepare($sqlRechercheCampagneClient);

/**
 * Recherche des tarifs campagne
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheTarifCampagneExc
 */
$sqlRechercheTarifCampagne = '
SELECT COUNT(*) AS nbIntervention, FK_idTypeMission, libelleTypeMission, tarifAnimation, tarifFormation, tarifMarchandising
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_mission_type ON su_mission_type.idTypeMission = su_mission.FK_idTypeMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE FK_idCampagne = :idCampagne
AND NOT ISNULL (FK_idContrat)
GROUP BY FK_idTypeMission
';
$RechercheTarifCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheTarifCampagne);

$sqlRechercheTarifCampagne2 = '
SELECT DATE_FORMAT(su_intervention.dateDebut, "%Y%m%d") AS dateD, FK_idTypeMission,  libelleTypeMission, tarifAnimation, tarifFormation, tarifMarchandising
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_mission_type ON su_mission_type.idTypeMission = su_mission.FK_idTypeMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE FK_idCampagne = :idCampagne
AND NOT ISNULL (FK_idContrat)
';
$RechercheTarifCampagne2Exc = DbConnexion::getInstance()->prepare($sqlRechercheTarifCampagne2);

/**
 * Recherche des frais pour une campagne
 *
 * @author Kevin MAURICE
 * @var $RechercheFraisCampagneExc
 */
$sqlRechercheFraisCampagne = '
SELECT idIntervention, idContrat, DATE_FORMAT(su_intervention.dateDebut,"%Y%m%d") AS dateD, salaireBrut, tarifKM, boolVehiculePersonnel, boolVehiculeFonction, autorisationFraisTel, TIME_TO_SEC(TIMEDIFF(su_intervention.dateFin,su_intervention.dateDebut)) AS DUREE, fraisAutre, fraisHT, fraisRepas, fraisTelephone, montantPrime, totalKm
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	LEFT JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE idCampagne = :idCampagne
AND NOT ISNULL (FK_idContrat)
';
$RechercheFraisCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheFraisCampagne);


/**
 * Recherche des informations sur la campagne
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInfoCampagneExc
 */
$sqlRechercheInfoCampagne = '
SELECT *
FROM su_campagne
  INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
WHERE idCampagne = :idCampagne';
$RechercheInfoCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoCampagne);

/**
 * Recherche des interlocuteurs Agence
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInterlocuteurAgenceExc
 */
$sqlRechercheInterlocuteurAgence = '
SELECT *
FROM su_agence_interlocuteur
ORDER BY nomInterlocuteurAgence';
$RechercheInterlocuteurAgenceExc = DbConnexion::getInstance()->prepare($sqlRechercheInterlocuteurAgence);

/**
 * Recherche des missions d'une campagne
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheMissionCampagneExc
 */
$sqlRechercheMissionCampagne = '
SELECT idMission, libelleGamme, libelleMission, libelleTypeMission, nbInterPrevisionnel, 
(
	SELECT COUNT(idIntervention)
	FROM su_intervention
	WHERE FK_idMission = MISSION.idMission
) AS nbIntervention,
(
	SELECT COUNT(idIntervention)
	FROM su_intervention
	WHERE FK_idContrat IS NULL AND FK_idMission = MISSION.idMission
) AS nbInterventionSansContrat
FROM su_mission MISSION
	INNER JOIN su_mission_type TYPE ON TYPE.idTypeMission = MISSION.FK_idTypeMission
WHERE FK_idCampagne = :idCampagne';
$RechercheMissionCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheMissionCampagne);

/**
 * Recherche des types de mission
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheTypeMissionExc
 */
$sqlRechercheTypeMission = '
SELECT *
FROM su_mission_type
ORDER BY libelleTypeMission';
$RechercheTypeMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheTypeMission);

/**
 * Recherche des charges de mission
 *
 * @author Vincent BENNER - Page Up
 * @var $RechercheChargeMission
 */
$sqlRechercheChargeMission = '
SELECT *
FROM du_utilisateur
WHERE idUtilisateur != 1
AND idUtilisateur != 1170
AND boolActif = 1
ORDER BY nomUtilisateur';
$RechercheChargeMissionExec = DbConnexion::getInstance()->prepare($sqlRechercheChargeMission);

/**
 * Recherche de l'etat des interventions d'une mission
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheStatutInterventionMissionExc
 */
$sqlRechercheStatutInterventionMission = '
SELECT idIntervention, libellePdv, codePostalPdv, villePdv, FK_idContrat, FK_idIntervenant, dateDebut, idIntervenant, nomIntervenant, prenomIntervenant
FROM su_intervention
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE FK_idMission =  :idMission
';
$RechercheStatutInterventionMissionExc = DbConnexion::getInstance()->prepare($sqlRechercheStatutInterventionMission);

/**
 * Recherche des departements
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheDepartementExc
 */
$sqlRechercheDepartement = '
SELECT *
FROM su_departement
ORDER BY libelleDepartement';
$RechercheDepartementExc = DbConnexion::getInstance()->prepare($sqlRechercheDepartement);

$sqlRechercheNiveau = '
SELECT *
FROM su_niveau
ORDER BY ordreNiveau';
$sqlRechercheNiveauExec = DbConnexion::getInstance()->prepare($sqlRechercheNiveau);

$sqlRechercheIntervenant = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, dateNaissance, departementNaissance
FROM su_intervenant
WHERE (boolActif = 1 AND boolCLOS = 0)
ORDER BY nomIntervenant
';
$RechercheIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheIntervenant);

$sqlRechercheIntervenantNonAuto = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, dateNaissance, departementNaissance
FROM su_intervenant
WHERE boolAutoEntrepreneuse <> "OUI"
AND idIntervenant <> 1
ORDER BY nomIntervenant
';
$RechercheIntervenantNonAutoExec = DbConnexion::getInstance()->prepare($sqlRechercheIntervenantNonAuto);

$sqlRecherchePdvDepartement = '
SELECT *
FROM su_pdv
WHERE codePostalPdv LIKE :codeDepartement
AND boolActif = 1
';
$RecherchePdvDepartementExc = DbConnexion::getInstance()->prepare($sqlRecherchePdvDepartement);

/**
 * Recherche des informations sur une intervention
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheInfoInterventionExc
 */
$sqlRechercheInfoIntervention = '
SELECT nomIntervenant, DATE_FORMAT(su_intervention.dateDebut, "%Y-%m-%d") AS dIntervention, 
    idPdv, libelleCampagne, libelleMission, libellePdv, codePostalPdv, villePdv, FK_idContrat, 
    FK_idIntervenant, DATE_FORMAT(su_intervention.dateDebut, "%d/%m/%Y") AS dateIntervention, 
    DATE_FORMAT(su_intervention.dateDebut, "%H:%i") AS heureDebut, 
    DATE_FORMAT(su_intervention.dateFin, "%H:%i") AS heureFin
FROM  su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
WHERE idIntervention = :idIntervention';
$RechercheInfoInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervention);

$sqlRechercheAutreIntervention = '
SELECT *, DATE_FORMAT(su_intervention.dateDebut, "%d/%m/%Y") AS dteDebut, DATE_FORMAT(su_intervention.dateDebut, "%H h %i") AS hDebut, DATE_FORMAT(su_intervention.dateFin, "%H h %i") AS hFin
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
WHERE su_intervention.dateDebut BETWEEN :dateDebut AND :dateFin AND idIntervention <> :idIntervention';
$RechercheAutreInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheAutreIntervention);

/**
 * Recherche des differents tarifs pour un couple Intervenant / Campagne
 *
 * @author Kevin MAURICE - Page UP
 * @var $RechercheTarificationCampagneExc
 */
$sqlRechercheTarificationCampagne = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, codePostalIntervenant, villeIntervenant, mobileIntervenant, telephoneIntervenant_A, telephoneIntervenant_B, mailIntervenant, commentaireIntervenant, idCampagne, libelleCampagne, salaireBrut, fraisRepas, fraisTelephone, tarifKM, COUNT(idIntervention) AS nbIntervention
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
GROUP BY idIntervenant, idCampagne, salaireBrut, fraisRepas, fraisTelephone, tarifKM
ORDER BY su_campagne.dateFin DESC
';
$RechercheTarificationCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheTarificationCampagne);

$sqlRechercheAffiliationTarificationCampagne = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, codePostalIntervenant, villeIntervenant, telephoneIntervenant_A, telephoneIntervenant_B, mailIntervenant, commentaireIntervenant, idCampagne, libelleCampagne, salaireBrut, fraisRepas, fraisTelephone, tarifKM, COUNT(idIntervention) AS nbIntervention
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
GROUP BY idIntervenant, idCampagne
ORDER BY nomIntervenant, libelleCampagne
';
$RechercheAffiliationTarificationCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheAffiliationTarificationCampagne);


$sqlRechercheAffiliationTarificationCampagneOnly = '
SELECT idCampagne, libelleCampagne
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
GROUP BY idCampagne
ORDER BY idCampagne DESC
';
$RechercheAffiliationTarificationCampagneOnlyExc = DbConnexion::getInstance()->prepare($sqlRechercheAffiliationTarificationCampagneOnly);


$sqlRechercheRemunerationIntervenant = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, codePostalIntervenant, villeIntervenant,
    telephoneIntervenant_A, telephoneIntervenant_B, mailIntervenant, commentaireIntervenant, idCampagne, libelleCampagne,
    salaireBrut, fraisRepas, fraisTelephone, tarifKM, COUNT(idIntervention) AS nbIntervention, boolVehiculeFonction, boolVehiculePersonnel, autorisationFraisTel, forfaitJournalier, forfaitJournalierNet
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE idIntervenant = (
  SELECT FK_idIntervenant 
  FROM su_intervention 
  WHERE idIntervention = :idIntervention
)
AND boolContratArchive = 0
GROUP BY idIntervenant, salaireBrut, fraisRepas, fraisTelephone, tarifKM, boolVehiculeFonction, boolVehiculePersonnel, autorisationFraisTel, forfaitJournalier
ORDER BY nbIntervention DESC
';
$RechercheRemunerationIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheRemunerationIntervenant);

$sqlRechercheRemunerationIntervenantSansNumIntervention = '
    SELECT idIntervenant, nomIntervenant, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, codePostalIntervenant, villeIntervenant,
        telephoneIntervenant_A, telephoneIntervenant_B, mailIntervenant, commentaireIntervenant, idCampagne, libelleCampagne,
        salaireBrut, fraisRepas, fraisTelephone, tarifKM, COUNT(idIntervention) AS nbIntervention, boolVehiculeFonction, boolVehiculePersonnel, autorisationFraisTel, forfaitJournalier, forfaitJournalierNet
    FROM su_intervention
        INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
        INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
        INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
        INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
        INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
    WHERE idIntervenant = :idIntervenant
    AND boolContratArchive = 0
    GROUP BY idIntervenant, salaireBrut, fraisRepas, fraisTelephone, tarifKM, boolVehiculeFonction, boolVehiculePersonnel, autorisationFraisTel, forfaitJournalier
    ORDER BY nbIntervention DESC
';
$RechercheRemunerationIntervenantSansNumInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheRemunerationIntervenantSansNumIntervention);

$sqlRechercheDureeIntervention = '
SELECT dateDebut, dateFin, SUM(TIME_TO_SEC(TIMEDIFF(dateFin, dateDebut))) AS dureeSeconde, libellePdv, codePostalPdv, villePdv, DATE_FORMAT(dateDebut, "%d/%m/%Y") AS dteIntervention, DATE_FORMAT(dateDebut, "%H:%i") AS dteHDIntervention, DATE_FORMAT(dateFin, "%H:%i") AS dteHFIntervention
FROM su_intervention
  INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE idIntervention = :idIntervention';
$RechercheDureeInterventionExc = DbConnexion::getInstance()->prepare($sqlRechercheDureeIntervention);

$sqlRechercheInfoMinIntervenant = '
SELECT *
FROM su_intervenant
WHERE idIntervenant = :idIntervenant';
$RechercheInfoMinIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoMinIntervenant);

$sqlRechercheInfoContrat = '
SELECT *, su_contrat.boolAutoEntrepreneur AS AutoE
FROM su_contrat
  INNER JOIN su_intervention ON su_intervention.FK_idContrat = su_contrat.idContrat
  INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
  INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
  INNER JOIN su_mission_type ON su_mission_type.idTypeMission = su_mission.FK_idTypeMission
  INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
  INNER JOIN su_intervention_frais ON su_intervention_frais.FK_idIntervention = su_intervention.idIntervention
WHERE idContrat = :idContrat
GROUP BY idContrat
';
$RechercheInfoContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoContrat);

$sqlRechercheInterventionContrat = '
SELECT DATE_FORMAT(dateDebut, "%d/%m/%Y") AS dteInter, DATE_FORMAT(dateDebut, "%H.%i") AS heureD, DATE_FORMAT(dateFin, "%H.%i") AS heureF, idIntervention, libellePdv, codePostalPdv, villePdv, dateDebut, dateFin, commentairePdv_EM, idCategoriePdv, libelleCategoriePdv,  SUM(TIME_TO_SEC(TIMEDIFF(dateFin, dateDebut))) AS dureeSeconde
FROM su_intervention
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_pdv_categorie ON su_pdv_categorie.idCategoriePdv = su_pdv.FK_idCategorie
WHERE FK_idContrat = :idContrat
GROUP BY idIntervention ORDER BY dateDebut, libellePdv';
$RechercheInterventionContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionContrat);


$sqlRechercheInterventionDisponible = '
SELECT idIntervention, libelleCampagne, libelleMission, nomIntervenant, prenomIntervenant, libellePdv, DATE_FORMAT(su_intervention.dateDebut, "%d/%m/%Y") AS dteInter
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
WHERE FK_idContrat IS NULL 
AND FK_idIntervenant = :idIntervenant 
AND idMission = :idMission 
AND (su_intervention.dateDebut >= :dateDebut AND su_intervention.dateFin <= :dateFin)
';
$RechercheInterventionDisponibleExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionDisponible);

$sqlRechercheCampagne = '
SELECT idCampagne, libelleCampagne, DATE_FORMAT(dateDebut, "%Y") AS anneeCampagne
FROM su_campagne
ORDER BY libelleCampagne';
$RechercheCampagneExc = DbConnexion::getInstance()->prepare($sqlRechercheCampagne);

$sqlRecherchePriseAccord = "
SELECT idPdv, codeMerval, libellePdv, idCampagne, libelleCampagne, idMission, libelleMission, FK_idContrat
FROM su_intervenant
	INNER JOIN su_intervention ON su_intervenant.idIntervenant = su_intervention.FK_idIntervenant
	INNER JOIN su_pdv ON su_pdv.idPdv = su_intervention.FK_idPdv
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
WHERE FK_idContrat IS NOT NULL AND su_intervention.dateDebut > :dateDebut
GROUP BY idPdv, idCampagne, idMission";
$RecherchePriseAccordExc = DbConnexion::getInstance()->prepare($sqlRecherchePriseAccord);

//AND dateDebut > "'.date('Y').'-01-01 00:00:00"

$sqlRechercheInterventionAvecContrat = '
SELECT idIntervention, FK_idIntervenant, DATE_FORMAT(dateDebut,"%Y%m%d") AS Dte
FROM su_intervention
WHERE FK_idContrat IS NOT NULL
ORDER BY dateDebut';
$RechercheInterventionAvecContratExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionAvecContrat);

$sqlRechercheInterventionAvecContratNoAuto = '
SELECT idIntervention, FK_idIntervenant, DATE_FORMAT(dateDebut,"%Y%m%d") AS Dte
FROM su_intervention
WHERE FK_idContrat IS NOT NULL
AND FK_idIntervenant = :idinter 
ORDER BY dateDebut
';
$RechercheInterventionAvecContratNoAutoExec = DbConnexion::getInstance()->prepare($sqlRechercheInterventionAvecContratNoAuto);

//WHERE dateContrat > "'.date('Y').'-01-01"
$sqlRechercheCIDD = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, departementNaissance, dateNaissance, dateContrat
FROM su_mission
    INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idMission = su_mission.idMission
    INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_contrat_cidd.FK_idIntervenant
GROUP BY idIntervenant, dateContrat';
$RechercheCIDDExc = DbConnexion::getInstance()->prepare($sqlRechercheCIDD);

$sqlRechercheCIDDNoAuto = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, departementNaissance, dateNaissance, dateContrat
FROM su_mission
    INNER JOIN su_contrat_cidd ON su_contrat_cidd.FK_idMission = su_mission.idMission
    INNER JOIN su_intervenant ON su_intervenant.idIntervenant = su_contrat_cidd.FK_idIntervenant
WHERE su_intervenant.boolAutoEntrepreneuse = "NON"
GROUP BY idIntervenant, dateContrat';
$RechercheCIDDNoAutoExec = DbConnexion::getInstance()->prepare($sqlRechercheCIDDNoAuto);

$sqlRechercheInterventionPeriode = '
SELECT idIntervenant, nomIntervenant, prenomIntervenant, codePostalIntervenant, villeIntervenant, telephoneIntervenant_A, mobileIntervenant,
(
	SELECT GROUP_CONCAT(FK_idDepartement)
	FROM su_intervenant_departement
	WHERE FK_idIntervenant = INTERVENANT.idIntervenant
	ORDER BY FK_idDepartement
) AS depIntervention
FROM su_intervenant INTERVENANT
	INNER JOIN su_intervention INTERVENTION ON INTERVENTION.FK_idIntervenant = INTERVENANT.idIntervenant
WHERE FK_idContrat IS NOT NULL AND dateDebut BETWEEN :dateDebut AND :dateFin
GROUP BY idIntervenant';
$RechercheInterventionPeriodeExc = DbConnexion::getInstance()->prepare($sqlRechercheInterventionPeriode);

$sqlRecherchePresenceEmail = '
SELECT *
FROM du_utilisateur
WHERE UPPER(mailUtilisateur) = UPPER(:mailUtilisateur)';
$RecherchePresenceMailExc = DbConnexion::getInstance()->prepare($sqlRecherchePresenceEmail);


$sqlRechercheCompetence = '
SELECT *
FROM su_competence
ORDER BY libelleCompetence';
$RechercheCompetenceExc = DbConnexion::getInstance()->prepare($sqlRechercheCompetence);

$sqlRechercheQualification = '
SELECT *
FROM su_qualification
ORDER BY libelleQualification';
$RechercheQualificationExc = DbConnexion::getInstance()->prepare($sqlRechercheQualification);

$sqlRechercheExperience = '
SELECT *
FROM su_experience
ORDER BY libelleExperience';
$RechercheExperienceExc = DbConnexion::getInstance()->prepare($sqlRechercheExperience);

$sqlRechercheGenreIntervenant = '
SELECT *
FROM su_intervenant_genre
ORDER BY idGenre';
$RechercheGenreIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheGenreIntervenant);

$sqlRechercheSituationIntervenant = '
SELECT *
FROM su_intervenant_situation
ORDER BY idSituation';
$RechercheSituationIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheSituationIntervenant);

$sqlRecherchePays = '
SELECT *
FROM su_pays
ORDER BY libellePays';
$RecherchePaysExc = DbConnexion::getInstance()->prepare($sqlRecherchePays);

$sqlRechercheInfoIntervenantTotal = '
SELECT INTER.*, DATE_FORMAT(dateNaissance, "%d/%m/%Y") AS dteNaissance, 
  DATE_FORMAT(dateFinSejour, "%d/%m/%Y") AS dteFinSejour,
  DATE_FORMAT(dateContratCadre, "%d/%m/%Y") AS dteContratCadre,
  DATE_FORMAT(dateCreation, "%d/%m/%Y") AS dteCreation,
  DATE_FORMAT(dateModification, "%d/%m/%Y") AS dteModification,
  DATE_FORMAT(dateUrssaf, "%d/%m/%Y") AS dteUrssaf,
(
	SELECT GROUP_CONCAT(FK_idCompetence)
	FROM su_intervenant_competence
	WHERE FK_idIntervenant = INTER.idIntervenant
) AS lstCompetence,
(
	SELECT GROUP_CONCAT(FK_idQualification)
	FROM su_intervenant_qualification
	WHERE FK_idIntervenant = INTER.idIntervenant
) AS lstQualification,
(
	SELECT GROUP_CONCAT(FK_idExperience)
	FROM su_intervenant_experience
	WHERE FK_idIntervenant = INTER.idIntervenant
) AS lstExperience,
(
	SELECT GROUP_CONCAT(FK_idDepartement)
	FROM su_intervenant_departement
	WHERE FK_idIntervenant = INTER.idIntervenant
) AS lstDepartement,
CONCAT(U.nomUtilisateur, " ", U.prenomUtilisateur) AS userModification
FROM su_intervenant INTER
  LEFT JOIN du_utilisateur U ON U.idUtilisateur = INTER.FK_idUtilisateur
WHERE idIntervenant = :idIntervenant
';
$RechercheInfoIntervenantTotalExc = DbConnexion::getInstance()->prepare($sqlRechercheInfoIntervenantTotal);

$sqlRechercheNbInterventionIntervenant = '
SELECT COUNT(*) AS nbIntervention, libelleMission, libelleClient, DATE_FORMAT(MIN(su_intervention.dateDebut), "%m/%Y") AS dateMin, 
      DATE_FORMAT(MAX(su_intervention.dateDebut), "%m/%Y") AS dateMax
FROM su_intervention
	INNER JOIN su_mission ON su_mission.idMission = su_intervention.FK_idMission
	INNER JOIN su_campagne ON su_campagne.idCampagne = su_mission.FK_idCampagne
	INNER JOIN su_client_interlocuteur ON su_client_interlocuteur.idInterlocuteurClient = su_campagne.FK_idInterlocuteurClient
	INNER JOIN su_client ON su_client.idClient = su_client_interlocuteur.FK_idClient
WHERE FK_idIntervenant = :idIntervenant
GROUP BY idClient, idMission';
$RechercheNbInterventionIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheNbInterventionIntervenant);

$sqlRechercheHistoriqueIntervention4Mois = '
SELECT SUM(TIME_TO_SEC(TIMEDIFF(dateFin,dateDebut))) AS nbSeconde, dateDebut, DATE_FORMAT(dateDebut,"%Y%m%d") AS dteD
FROM su_intervention
WHERE FK_idIntervenant = :idIntervenant AND DATE_SUB(CURDATE(),INTERVAL 120 DAY) <= dateDebut
GROUP BY dateDebut
ORDER BY dateDebut';
$RechercheHistoriqueIntervention4MoisExc = DbConnexion::getInstance()->prepare($sqlRechercheHistoriqueIntervention4Mois);

$sqlRechercheInterlocuteurPdv = '
SELECT idInterlocuteurPdv
FROM su_pdv_interlocuteur
WHERE FK_idPdv = :idPdv
LIMIT 1';
$RechercheInterlocuteurPdvExc = DbConnexion::getInstance()->prepare($sqlRechercheInterlocuteurPdv);

$sqlRechercheCompetenceIntervenant = '
SELECT *
FROM su_intervenant_competence';
$RechercheCompetenceIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheCompetenceIntervenant);

$sqlRechercheQualificationIntervenant = '
SELECT *
FROM su_intervenant_qualification';
$RechercheQualificationIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheQualificationIntervenant);

$sqlRechercheNiveauIntervenant = '
SELECT *
FROM su_intervenant_niveau';
$RechercheNiveauIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheNiveauIntervenant);

$sqlRechercheExperienceIntervenant = '
SELECT *
FROM su_intervenant_experience';
$RechercheExperienceIntervenantExc = DbConnexion::getInstance()->prepare($sqlRechercheExperienceIntervenant);

$sqlRechercheFraisContrat = '
SELECT fraisRepas, fraisTelephone
FROM su_intervention_frais
	INNER JOIN su_intervention ON su_intervention_frais.FK_idIntervention  = su_intervention.idIntervention
	INNER JOIN su_contrat ON su_contrat.idContrat = su_intervention.FK_idContrat
WHERE idContrat = :idContrat
GROUP BY idContrat';
$RechercheFraisContratExc = DbConnexion::getInstance()->prepare($sqlRechercheFraisContrat);