<?php


$sqlInsertionTauxKm = '
INSERT INTO su_intervenant_km(FK_idIntervenant, tarifKilometre, dateKilometre, totalKilometre)
VALUES(:idIntervenant, :tarifKm, :dateKm, :totalKm)';
$InsertionTauxKmExc = DbConnexion::getInstance()->prepare($sqlInsertionTauxKm);

$sqlInsertionFraisIntervention = '
INSERT INTO su_intervenant_frais(FK_idIntervenant, dateFrais)
VALUES(:idIntervenant, :dateFrais)';
$InsertionFraisInterventionExc = DbConnexion::getInstance()->prepare($sqlInsertionFraisIntervention);


$sqlInsertionKmPdvIntervenant = '
INSERT INTO su_pdv_kilometrage(FK_idPdv, FK_idIntervenant, totalKm)
VALUES(:idPdv, :idIntervenant, :nbKm) ON DUPLICATE KEY UPDATE totalKm = :nbKm';
$InsertionKmPdvIntervenantExc = DbConnexion::getInstance()->prepare($sqlInsertionKmPdvIntervenant);

$sqlInsertionDateDue = "
INSERT INTO su_due(FK_idContrat, dateGeneration)
VALUES(:idContrat, :date)
ON DUPLICATE KEY UPDATE dateGeneration = :date";
$InsertionDateDueExc = DbConnexion::getInstance()->prepare($sqlInsertionDateDue);

$sqlModificationInterlocuteurClient = '
UPDATE su_client_interlocuteur SET
  nomInterlocuteurClient = :nom,
  prenomInterlocuteurClient = :prenom,
  sexeInterlocuteurClient = :sexe,
  telInterlocuteurClient_A = :tel,
  telInterlocuteurClient_B = :mobile,
  mailInterlocuteurClient = :email,
  FK_idTitreInterlocuteurClient = :statut
WHERE idInterlocuteurClient = :idInterlocuteur';
$ModificationInterlocuteurClientExc = DbConnexion::getInstance()->prepare($sqlModificationInterlocuteurClient);

$sqlAjoutInterlocuteurClient = '
INSERT INTO su_client_interlocuteur(FK_idClient, FK_idTitreInterlocuteurClient, nomInterlocuteurClient, prenomInterlocuteurClient, sexeInterlocuteurClient, telInterlocuteurClient_A, telInterlocuteurClient_B, mailInterlocuteurClient)
VALUES(:idClient, :statut, :nom, :prenom, :sexe, :tel, :mobile, :email)';
$AjoutInterlocuteurClientExc = DbConnexion::getInstance()->prepare($sqlAjoutInterlocuteurClient);

$sqlSuppressionInterlocuteurClient = '
DELETE FROM su_client_interlocuteur
WHERE idInterlocuteurClient = :idInterlocuteur';
$SuppressionInterlocuteurClientExc = DbConnexion::getInstance()->prepare($sqlSuppressionInterlocuteurClient);

$sqlAjoutMission = '
INSERT INTO su_mission(libelleMission, libelleGamme, FK_idCampagne, FK_idTypeMission, 
  descriptionMission, fraisGestion, nbInterPrevisionnel, decideur_id)
VALUES(:libelleMission, :gammeProduit, :idCampagne, :typeMission, :description, :fraisGestion, 
  :nbInterPrevisionnel, :chargeMission)';
$AjoutMissionExc = DbConnexion::getInstance()->prepare($sqlAjoutMission);

$sqlSuppressionIntervention = '
DELETE FROM su_intervention WHERE idIntervention = :idIntervention';
$SuppressionInterventionExc = DbConnexion::getInstance()->prepare($sqlSuppressionIntervention);

$sqlSuppressionDateIntervention = '
UPDATE su_intervention SET
  dateDebut = NULL,
  dateFin = NULL
WHERE idIntervention = :idIntervention';
$SuppressionDateInterventionExc = DbConnexion::getInstance()->prepare($sqlSuppressionDateIntervention);

$sqlSuppressionContratIntervention = '
UPDATE su_intervention SET
  FK_idContrat = NULL
WHERE idIntervention = :idIntervention';
$SuppressionContratInterventionExc = DbConnexion::getInstance()->prepare($sqlSuppressionContratIntervention);

$sqlAjoutIntervention = '
INSERT INTO su_intervention(FK_idIntervenant, FK_idMission, FK_idPdv)
VALUES(:idIntervenant, :idMission, :idPdv)';
$AjoutInterventionExc = DbConnexion::getInstance()->prepare($sqlAjoutIntervention);

$sqlModificationInformationMission = '
UPDATE su_mission SET
  libelleMission = :libelleMission,
  libelleGamme = :gammeProduit,
  FK_idTypeMission = :typeMission,
  decideur_id = :chargeMission,
  descriptionMission = :commentaireMission,
  fraisGestion = :fraisGestion,
  nbInterPrevisionnel = :nbInterPrevisionnel,
  FK_idMissionPrec = :missionPrec
WHERE idMission = :idMission
';
$ModificationInformationsMissionExc = DbConnexion::getInstance()->prepare($sqlModificationInformationMission);

$sqlAjoutFraisIntervention = '
INSERT INTO su_intervention_frais(FK_idIntervention, fraisRepas, fraisTelephone)
VALUES(:idIntervention, :fraisRepas, :fraisTel)
ON DUPLICATE KEY UPDATE fraisRepas = :fraisRepas, fraisTelephone = :fraisTel';
$AjoutFraisInterventionExc = DbConnexion::getInstance()->prepare($sqlAjoutFraisIntervention);

$sqlAjoutContrat = '
INSERT INTO su_contrat(
  salaireBrut, tarifKM, autorisationFraisTel, boolVehiculePersonnel, 
  boolVehiculeFonction, forfaitJournalier, forfaitJournalierNet, 
  societeA, societeB, dateCreationContrat, boolAutoEntrepreneur, boolFraisInclus)
VALUES(
  :salaireBrut, :tarifKM, :fraisTel, :veP, 
  :veF, :forfaitJ, :forfaitN, 
  :societe, :societe, :date, :autoentre, :boolFraisInclus
  )';
$AjoutContratExc = DbConnexion::getInstance()->prepare($sqlAjoutContrat);

$sqlAffiliationInterventionContrat = '
UPDATE su_intervention SET
  FK_idContrat = :idContrat
WHERE idIntervention = :idIntervention';
$AffiliationInterventionContratExc = DbConnexion::getInstance()->prepare($sqlAffiliationInterventionContrat);

$sqlAjoutRetourContrat = '
INSERT INTO su_contrat_retour(FK_idContrat, dateRetour)
VALUES(:idContrat, :dateRetour)';
$AjoutRetourContratExc = DbConnexion::getInstance()->prepare($sqlAjoutRetourContrat);

$sqlSuppressionRetourContrat = '
DELETE FROM su_contrat_retour WHERE FK_idContrat = :idContrat';
$SuppressionRetourContratExc = DbConnexion::getInstance()->prepare($sqlSuppressionRetourContrat);

$sqlMajInformationContrat = '
UPDATE su_contrat SET
  societeA = :societeA,
  societeB = :societeB,
  detailContrat = :detail,
  salaireBrut = :tauxh,
  forfaitJournalier = :forfaitj,
  forfaitJournalierNet = :forfaitjn,
  tarifKM = :coutkm,
  boolFraisInclus = :boolFraisInclus
WHERE idContrat = :idContrat
';
$MajInformationContratExc = DbConnexion::getInstance()->prepare($sqlMajInformationContrat);

$sqlAjoutInterventionContrat = '
UPDATE su_intervention SET
  FK_idContrat = :idContrat
WHERE idIntervention = :idIntervention';
$AjoutInterventionContratExc = DbConnexion::getInstance()->prepare($sqlAjoutInterventionContrat);

$sqlSuppressionInterventionContrat = '
UPDATE su_intervention SET
  FK_idContrat = NULL
WHERE FK_idContrat = :idContrat';
$SuppressionInterventionContratExc = DbConnexion::getInstance()->prepare($sqlSuppressionInterventionContrat);

$sqlSuppressionContrat = '
DELETE FROM su_contrat WHERE idContrat = :idContrat';
$SuppressionContratExc = DbConnexion::getInstance()->prepare($sqlSuppressionContrat);

$sqlModificationFraisIntervention = '
UPDATE su_intervenant_frais SET
    montantPrimeRdv = :montantPrimeRdv,
    montantIndemniteFormation = :montantIndemniteFormation,
    montantIndemniteTelephone = :montantIndemniteTelephone,
    montantAutreFrais = :montantAutreFrais,
    montantFraisJustificatif = :montantFraisJustificatif,
    montantFraisJustificatifTVA = :montantFraisJustificatifTVA,
    montantPrimeObjectif = :montantPrimeObjectif,
    montantIndemniteAnnulation = :montantIndemniteAnnulation,
    montantAutrePrime = :montantAutrePrime,
    montantRepriseAccompte = :montantRepriseAccompte,
    commentairePrimeObjectif = :commentairePrimeObjectif,
    commentaireIndemniteAnnulation = :commentaireIndemniteAnnulation,
    commentaireAutrePrime = :commentaireAutrePrime
WHERE FK_idIntervenant = :idIntervenant AND dateFrais = :dateFrais';
$ModificationFraisInterventionExc = DbConnexion::getInstance()->prepare($sqlModificationFraisIntervention);

$sqlModificationKmIntervenant = '
UPDATE su_intervenant_km SET
  totalKilometre = :nbKM
WHERE FK_idIntervenant = :idIntervenant AND dateKilometre = :dateFrais AND tarifKilometre = :tarif';
$ModificationKmIntervenantExc = DbConnexion::getInstance()->prepare($sqlModificationKmIntervenant);

$sqlSuppressionClient = '
DELETE FROM su_client WHERE idClient = :idClient';
$SuppressionClientExc = DbConnexion::getInstance()->prepare($sqlSuppressionClient);


$sqlModificationClient = '
UPDATE su_client SET
  idSiret = :idSiret,
  libelleClient = :libelleClient,
  telClient = :telClient,
  faxClient = :faxClient,
  emailClient = :emailClient,
  commentaireClient = :commentaireClient,
  adresseClient = :adresseClient,
  codePostalClient = :codePostalClient,
  villeClient = :villeClient,
  destinataireFacturationClient = :destinataireFacturationClient,
  adresseFacturationClient = :adresseFacturationClient,
  codePostalFacturationClient = :codePostalFacturationClient,
  villeFacturationClient = :villeFacturationClient,
  FK_idTypeActivite = :FK_idTypeActivite,
  FK_idFiliere = :FK_idFiliere,
  dateCreation = :dateCreation
WHERE idClient = :idClient';
$ModificationClientExc = DbConnexion::getInstance()->prepare($sqlModificationClient);

$sqlAjoutClient = '
INSERT INTO su_client(idSiret, libelleClient, telClient, faxClient, emailClient, 
  commentaireClient, adresseClient, codePostalClient, villeClient, destinataireFacturationClient, 
  adresseFacturationClient, codePostalFacturationClient, villeFacturationClient, FK_idTypeActivite, 
  FK_idFiliere, dateCreation)
VALUES(:idSiret, :libelleClient, :telClient, :faxClient, :emailClient, 
  :commentaireClient, :adresseClient, :codePostalClient, :villeClient, :destinataireFacturationClient, 
  :adresseFacturationClient, :codePostalFacturationClient, :villeFacturationClient, :FK_idTypeActivite, 
  :FK_idFiliere, :dateCreation)';
$AjoutClientExc = DbConnexion::getInstance()->prepare($sqlAjoutClient);

$sqlAjoutAdministrateurAgence = '
INSERT INTO du_utilisateur(
  nomUtilisateur, prenomUtilisateur, passUtilisateur, mailUtilisateur, boolActif, 
  FK_idTypeUtilisateur, villeIntervenant, codePostalIntervenant,
  adresseIntervenant_A, adresseIntervenant_B)
VALUES(:nomUtilisateur, :prenomUtilisateur, :passUtilisateur, :emailUtilisateur, 1, :roleUtilisateur,
  :ville, :cp, :adresseA, :adresseB
)';
$AjoutAdministrateurAgenceExc = DbConnexion::getInstance()->prepare($sqlAjoutAdministrateurAgence);

$sqlSuppressionAdministrateur = '
DELETE FROM du_utilisateur WHERE idUtilisateur = :idUtilisateur';
$SuppressionAdministrateurAgenceExc = DbConnexion::getInstance()->prepare($sqlSuppressionAdministrateur);

$sqlAjoutIntervenant = '
INSERT INTO su_intervenant (
    nomIntervenant, nomIntervenant_JF, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, 
    adresseIntervenant_C, telephoneIntervenant_A, telephoneIntervenant_B, faxIntervenant, mobileIntervenant, 
    mailIntervenant,  numeroSS, dateNaissance, villeNaissance, departementNaissance, 
    paysNaissance, nationalite, commentaireIntervenant, codePostalIntervenant, villeIntervenant, 
    genre, situationFamiliale, numeroCarteSejour, dateFinSejour, nombreEnfant, 
    boolAutoEntrepreneuse, depNaissanceDUE, dateContratCadre, numeroIBAN, codeBIC, 
    libelleBanque, codeBanque, codeGuichet, numeroCompte, cleSecurite,
    FK_idNiveau, numSIRET, dateCreation, dateModification, FK_idUtilisateur, dateUrssaf)
VALUES(
  :nomIntervenant, :nomIntervenant_JF, :prenomIntervenant, :adresseIntervenant_A, :adresseIntervenant_B, 
  :adresseIntervenant_C, :telephoneIntervenant_A, :telephoneIntervenant_B, :faxIntervenant, :mobileIntervenant, 
  :mailIntervenant, :numeroSS, :dateNaissance, :villeNaissance, :departementNaissance, 
  :paysNaissance, :nationalite, :commentaireIntervenant, :codePostalIntervenant, :villeIntervenant, 
  :genre, :situationFamiliale, :numeroCarteSejour, :dateFinSejour, :nombreEnfant, 
  :boolAutoEntrepreneuse, :depNaissanceDUE, :dateContrat, :numeroIBAN, :codeBIC, 
  :libelleBanque, :codeBanque, :codeGuichet, :numCompte, :cleSecurite,
  :niveau, :siret, :dateCreation, :dateModification, :idUtilisateur, :dateUrssaf)';
$AjoutIntervenantExc = DbConnexion::getInstance()->prepare($sqlAjoutIntervenant);

$sqlModificationIntervenant = '
REPLACE INTO su_intervenant (idIntervenant, nomIntervenant, nomIntervenant_JF, prenomIntervenant, adresseIntervenant_A, adresseIntervenant_B, 
    adresseIntervenant_C, telephoneIntervenant_A, telephoneIntervenant_B, faxIntervenant, mobileIntervenant, mailIntervenant, numeroSS, dateNaissance, 
    villeNaissance, departementNaissance, paysNaissance, nationalite, commentaireIntervenant, codePostalIntervenant, villeIntervenant, genre, 
    situationFamiliale, numeroCarteSejour, dateFinSejour, nombreEnfant, boolAutoEntrepreneuse, depNaissanceDUE, numeroIBAN, codeBIC,
    FK_idNiveau, numSIRET)
VALUES(:idIntervenant, :nomIntervenant, :nomIntervenant_JF, :prenomIntervenant, :adresseIntervenant_A, :adresseIntervenant_B, 
    :adresseIntervenant_C, :telephoneIntervenant_A, :telephoneIntervenant_B, :faxIntervenant, :mobileIntervenant, :mailIntervenant, :numeroSS, 
    :dateNaissance, :villeNaissance, :departementNaissance, :paysNaissance, :nationalite, :commentaireIntervenant, :codePostalIntervenant, 
    :villeIntervenant, :genre, :situationFamiliale, :numeroCarteSejour, :dateFinSejour, :nombreEnfant, :boolAutoEntrepreneuse, 
    :depNaissanceDUE, :numeroIBAN, :codeBIC, :niveau, :siret)';
$ModificationIntervenantExc = DbConnexion::getInstance()->prepare($sqlModificationIntervenant);

$sqlModificationIntervenant2 = '
UPDATE su_intervenant SET
  nomIntervenant = :nomIntervenant,
  nomIntervenant_JF = :nomIntervenant_JF,
  prenomIntervenant = :prenomIntervenant,
  adresseIntervenant_A = :adresseIntervenant_A,
  adresseIntervenant_B = :adresseIntervenant_B,
  adresseIntervenant_C = :adresseIntervenant_C,
  latitude = :latitude,
  longitude = :longitude,
  telephoneIntervenant_A = :telephoneIntervenant_A,
  telephoneIntervenant_B = :telephoneIntervenant_B,
  faxIntervenant = :faxIntervenant,
  mobileIntervenant = :mobileIntervenant,
  mailIntervenant = :mailIntervenant,
  numeroSS = :numeroSS,
  FK_idNiveau = :niveau,
  dateNaissance = :dateNaissance,
  villeNaissance = :villeNaissance,
  departementNaissance = :departementNaissance,
  paysNaissance = :paysNaissance,
  nationalite = :nationalite,
  commentaireIntervenant = :commentaireIntervenant,
  codePostalIntervenant = :codePostalIntervenant,
  villeIntervenant = :villeIntervenant,
  genre = :genre,
  situationFamiliale = :situationFamiliale,
  numeroCarteSejour = :numeroCarteSejour,
  dateFinSejour = :dateFinSejour,
  nombreEnfant = :nombreEnfant,
  boolAutoEntrepreneuse = :boolAutoEntrepreneuse,
  depNaissanceDUE = :depNaissanceDUE,
  numeroIBAN = :numeroIBAN,
  codeBIC = :codeBIC,
  numSIRET = :siret,
  
  libelleBanque =  :libelleBanque,
  codeBanque = :codeBanque, 
  codeGuichet = :codeGuichet, 
  numeroCompte = :numCompte, 
  cleSecurite = :cleSecurite,

  dateContratCadre = :dateContrat,
  dateUrssaf = :dateUrssaf,
  dateCreation = :dateCreation,
  dateModification = :dateModification,
  FK_idUtilisateur = :idUtilisateur
  
WHERE idIntervenant = :idIntervenant';
$ModificationIntervenant2Exc = DbConnexion::getInstance()->prepare($sqlModificationIntervenant2);

$sqlAjoutCompetence = '
INSERT INTO su_intervenant_competence(FK_idIntervenant, FK_idCompetence)
VALUES(:idIntervenant, :id)';
$AjoutCompetenceExc = DbConnexion::getInstance()->prepare($sqlAjoutCompetence);

$sqlAjoutNiveau= '
INSERT INTO su_intervenant_niveau(FK_idIntervenant, FK_idNiveau)
VALUES(:idIntervenant, :id)';
$AjoutNiveauExec = DbConnexion::getInstance()->prepare($sqlAjoutNiveau);

$sqlAjoutQualification = '
INSERT INTO su_intervenant_qualification(FK_idIntervenant, FK_idQualification)
VALUES(:idIntervenant, :id)';
$AjoutQualificationExc = DbConnexion::getInstance()->prepare($sqlAjoutQualification);

$sqlAjoutExperience = '
INSERT INTO su_intervenant_experience(FK_idIntervenant, FK_idExperience)
VALUES(:idIntervenant, :id)';
$AjoutExperienceExc = DbConnexion::getInstance()->prepare($sqlAjoutExperience);

$sqlAjoutDepartement = '
INSERT INTO su_intervenant_departement(FK_idIntervenant, FK_idDepartement)
VALUES(:idIntervenant, :id)';
$AjoutDepartementExc = DbConnexion::getInstance()->prepare($sqlAjoutDepartement);

$sqlSuppressionCompetence = '
DELETE FROM su_intervenant_competence WHERE FK_idIntervenant = :idIntervenant';
$SuppressionCompetenceExc = DbConnexion::getInstance()->prepare($sqlSuppressionCompetence);

$sqlSuppressionQualification = '
DELETE FROM su_intervenant_qualification WHERE FK_idIntervenant = :idIntervenant';
$SuppressionQualificationExc = DbConnexion::getInstance()->prepare($sqlSuppressionQualification);

$sqlSuppressionExperience = '
DELETE FROM su_intervenant_experience WHERE FK_idIntervenant = :idIntervenant';
$SuppressionExperienceExc = DbConnexion::getInstance()->prepare($sqlSuppressionExperience);

$sqlSuppressionDepartement = '
DELETE FROM su_intervenant_departement WHERE FK_idIntervenant = :idIntervenant';
$SuppressionDepartementExc = DbConnexion::getInstance()->prepare($sqlSuppressionDepartement);

$sqlAjoutInterventionAvecDate = '
INSERT INTO su_intervention(FK_idIntervenant, FK_idPdv, FK_idMission, dateDebut, dateFin)
VALUES(:idIntervenant, :idPdv, :idMission, :dateDebut, :dateFin)';
$AjoutInterventionAvecDateExc = DbConnexion::getInstance()->prepare($sqlAjoutInterventionAvecDate);

$sqlModificationInterlocuteur = '
UPDATE su_agence_interlocuteur SET
  nomInterlocuteurAgence = :nom,
  prenomInterlocuteurAgence = :prenom,
  telInterlocuteurAgence = :tel,
  mailInterlocuteurAgence = :mail
WHERE idInterlocuteurAgence = :id';
$ModificationInterlocuteurExc = DbConnexion::getInstance()->prepare($sqlModificationInterlocuteur);

$sqlAjoutInterlocuteurAgence = '
INSERT INTO su_agence_interlocuteur(nomInterlocuteurAgence, prenomInterlocuteurAgence, telInterlocuteurAgence, mailInterlocuteurAgence)
VALUES(:nom, :prenom, :tel, :mail)';
$AjoutInterlocuteurAgenceExc = DbConnexion::getInstance()->prepare($sqlAjoutInterlocuteurAgence);

$sqlModificationAffiliationIntervention = '
UPDATE su_intervention SET
  FK_idIntervenant = :idIntervenant
WHERE idIntervention = :idIntervention';
$ModificationAffiliationInterventionExc = DbConnexion::getInstance()->prepare($sqlModificationAffiliationIntervention);

$sqlModificationDateIntervention = '
UPDATE su_intervention SET
dateDebut = :dateDebut,
dateFin = :dateFin
WHERE idIntervention = :idIntervention';
$ModificationDateInterventionExc = DbConnexion::getInstance()->prepare($sqlModificationDateIntervention);

$sqlMajInformationsPdv = '
UPDATE su_pdv SET
    libellePdv = :libellePdv,
    adressePdv_A = :adressePdv_A,
    adressePdv_B = :adressePdv_B,
    adressePdv_C = :adressePdv_C,
    codePostalPdv = :codePostalPdv,
    codeMerval = :codeMerval,
    codeMerval_old = :codeMerval_old,
    villePdv = :villePdv,
    latitude = :latitude,
    longitude = :longitude,
    commentairePdv = :commentairePdv,
    commentairePdv_EM = :commentairePdv_EM,
    FK_idCategorie = :categorie,
    FK_idSousCategorie = :souscategorie,
    ciblage = :ciblage
WHERE idPdv = :idPdv
';
$MajInformationsPdvExc = DbConnexion::getInstance()->prepare($sqlMajInformationsPdv);

$sqlMajInformationsContactPdv = '
    UPDATE su_pdv SET
      nomTitulairePdv_A = :nomTitulairePdv_A,
      prenomTitulairePdv_A = :prenomTitulairePdv_A,
      nomTitulairePdv_B = :nomTitulairePdv_B,
      prenomTitulairePdv_B = :prenomTitulairePdv_B,
      telephoneMagasin = :telephoneMagasin,
      faxPdv = :faxPdv,
      emailPdv = :emailPdv
    WHERE idPdv = :idPdv';
$MajInformationsContactPdvExc = DbConnexion::getInstance()->prepare($sqlMajInformationsContactPdv);

$sqlMajInterlocuteurPdv = '
UPDATE su_pdv_interlocuteur SET
  nomInterlocuteurPdv = :nomInterlocuteurPdv,
  prenomInterlocuteurPdv = :prenomInterlocuteurPdv,
  telephoneInterlocuteurPdv = :telephoneInterlocuteurPdv,
  faxInterlocuteurPdv = :faxInterlocuteurPdv,
  mailInterlocuteurPdv = :mailInterlocuteurPdv,
  FK_idFonctionInterlocuteurPDV = :fonctionInterlocuteurPdv
WHERE idInterlocuteurPdv = :idInterlocuteur';
$MajInterlocuteurPdvExc = DbConnexion::getInstance()->prepare($sqlMajInterlocuteurPdv);

$sqlAjoutInterlocuteurPdv = '
INSERT INTO su_pdv_interlocuteur(FK_idPdv, nomInterlocuteurPdv, prenomInterlocuteurPdv, 
    telephoneInterlocuteurPdv, faxInterlocuteurPdv, mailInterlocuteurPdv, 
    FK_idTypeInterlocuteurPdv, FK_idFonctionInterlocuteurPDV)
VALUES(:idPdv, :nomInterlocuteurPdv, :prenomInterlocuteurPdv, :telephoneInterlocuteurPdv, 
    :faxInterlocuteurPdv, :mailInterlocuteurPdv, 1, 
    :fonctionInterlocuteurPdv)';
$AjoutInterlocuteurPdvExc = DbConnexion::getInstance()->prepare($sqlAjoutInterlocuteurPdv);


$sqlSuppressionIntervenant = 'DELETE FROM su_intervenant WHERE idIntervenant = :idIntervenant';
$SuppressionIntervenantExc = DbConnexion::getInstance()->prepare($sqlSuppressionIntervenant);

$sqlReinitAffiliationIntervention = '
UPDATE su_intervention SET
  FK_idIntervenant = 1
WHERE FK_idIntervenant = :idIntervenant';
$ReinitAffiliationInterventionExc = DbConnexion::getInstance()->prepare($sqlReinitAffiliationIntervention);

$sqlInsertionPdv = '
INSERT INTO su_pdv(libellePdv, codeMerval, adressePdv_A, 
   adressePdv_B, adressePdv_C, codePostalPdv, villePdv, 
   telephoneMagasin, emailPdv, FK_idCategorie, FK_idSousCategorie, 
   nomTitulairePdv_A, prenomTitulairePdv_A, nomTitulairePdv_B, 
   prenomTitulairePdv_B)
VALUES(:libellePdv, :codeMerval, :adressePdv_A, :adressePdv_B, :adressePdv_C, 
       :codePostalPdv, :villePdv, :telephoneMagasin, 
       :emailPdv, :FK_idCategorie, :FK_idSousCategorie, :nomTitulairePdv_A, 
       :prenomTitulairePdv_A, :nomTitulairePdv_B, :prenomTitulairePdv_B
)';
$InsertionPdvExc = DbConnexion::getInstance()->prepare($sqlInsertionPdv);

$sqlUpdatePdv = '
UPDATE su_pdv
SET libellePdv = :libellePdv, adressePdv_A = :adressePdv_A, 
   adressePdv_B = :adressePdv_B, adressePdv_C = :adressePdv_C, 
   codePostalPdv = :codePostalPdv, villePdv = :villePdv, 
   telephoneMagasin = :telephoneMagasin, emailPdv = :emailPdv, 
   FK_idCategorie = :FK_idCategorie, FK_idSousCategorie = :FK_idSousCategorie, 
   nomTitulairePdv_A = :nomTitulairePdv_A, prenomTitulairePdv_A = :prenomTitulairePdv_A,
   nomTitulairePdv_B = :nomTitulairePdv_B, prenomTitulairePdv_B = :prenomTitulairePdv_B
WHERE codeMerval = :codeMerval 
';
$UpdatePdvExc = DbConnexion::getInstance()->prepare($sqlUpdatePdv);

$sqlInsertionIntPdv = '
INSERT INTO su_pdv_interlocuteur(FK_idPdv, nomInterlocuteurPdv, prenomInterlocuteurPdv, telInterlocuteurPdv, faxInterlocuteurPdv, mailInterlocuteurPdv, FK_idTypeInterlocuteurPdv)
VALUES (:FK_idPdv, :nomInterlocuteurPdv, :prenomInterlocuteurPdv, :telInterlocuteurPdv, :faxInterlocuteurPdv, :mailInterlocuteurPdv, :FK_idTypeInterlocuteurPdv)';
$InsertionIntPdvExc = DbConnexion::getInstance()->prepare($sqlInsertionIntPdv);

$sqlArchiveContrat = 'UPDATE su_contrat SET boolContratArchive = :boolContratArchive WHERE idContrat = :idContrat';
$ArchiveContratExc = DbConnexion::getInstance()->prepare($sqlArchiveContrat);

$sqlSuppressionPDV = 'DELETE FROM su_pdv WHERE  idPdv = :idPDV';
$SuppressionPDVExc = DbConnexion::getInstance()->prepare($sqlSuppressionPDV);

$sqlSuppressionInterlocuteurPDV = 'DELETE FROM su_pdv_interlocuteur WHERE  FK_idPdv = :idPDV';
$SuppressionInterlocuteurPDVExec = DbConnexion::getInstance()->prepare($sqlSuppressionInterlocuteurPDV);
