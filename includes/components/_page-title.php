<?php
// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Create title page for module and sub-module 
// @detail :
// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------
// Require Database Connexion
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../../_config/config.sql.php';

// ------------------------------------------------------------------------------------
// Require module class
// ------------------------------------------------------------------------------------
require_once dirname ( __FILE__ ) . '/../classes/class.module.php';

// ------------------------------------------------------------------------------------
// Check input POST
// ap -> module
// ss_m -> sub-module
// ------------------------------------------------------------------------------------
if(filter_has_var(INPUT_POST, 'ap') && filter_has_var(INPUT_POST, 'ss_m')){
	
	$Title = '';
	
	// ------------------------------------------------------------------------------------
	// Search sub-module name
	// ------------------------------------------------------------------------------------
	if(filter_input(INPUT_POST, 'ss_m') !== ''){
		$InfoSModule = Sous_Module::loadByLink(DbConnexion::getInstance(), filter_input(INPUT_POST, 'ss_m'));
		if($InfoSModule){
			$Title = $InfoSModule->getModule()->getLibelleModule().' <small> ' . $InfoSModule->getLibellesousmodule() . '</small>';
		}
	}
	
	// ------------------------------------------------------------------------------------
	// Search module name
	// ------------------------------------------------------------------------------------
	else{
		$InfoModule = Module::loadByLink(DbConnexion::getInstance(), filter_input(INPUT_POST, 'ap'));
		if($InfoModule){
			$Title = $InfoModule->getLibelleModule();
		}
	}
	
	// ------------------------------------------------------------------------------------
	// Return module or sub-module title
	// ------------------------------------------------------------------------------------
	echo json_encode(array(
		'Title' => $Title,
		'Html' => '<div class="row"><div class="col-md-12"><h3 class="page-title">'.$Title.'</h3><ul class="page-breadcrumb breadcrumb"><li><i class="fa fa-home"></i><a href="index.php">Dashboard</a><i class="fa fa-angle-right"></i></li></ul></div></div>'
	));
}