
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <span class="badge bg bg-blue" style="border-radius: 0 !important;">2015 &copy; Page UP </span> <span class="badge bg bg-green" style="border-radius: 0 !important;">v3.05-22032016-PROD</span>
    </div>
    <div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>

<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>

<!-- DATATABLES -->
<!--
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/date.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
-->

<script src="assets/global/plugins/datatables2/datatables.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/datatables2/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<link href="assets/global/plugins/datatables2/datatables.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-validation/js/localization/messages_fr.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js"></script>

<script type="text/javascript" src="assets/global/plugins/fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-lazyload/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/dropzone/dropzone.min.js"></script>

<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/amcharts.js"></script>
<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/lang/fr.js"></script>
<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/serial.js"></script>
<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/plugins/dataloader/dataloader.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="assets/global/plugins/amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script type="text/javascript" src="assets/global/plugins/amcharts/amcharts/themes/light.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYinfTYbQZNLJYFIYqgikVtjj5RITpm5k&callback=initMap()"
        type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<?php print $linkJsModule ?>
<!-- END PAGE LEVEL SCRIPTS -->
<script>

    $.fn.bdatepicker.dates['fr'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        weekStart: 1
    };
    
    jQuery(document).ready(function() {
        Dropzone.autoDiscover = false;
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout

    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>