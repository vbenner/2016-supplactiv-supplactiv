<?php

// ------------------------------------------------------------------------------------
// @author : Kevin MAURICE - PAGE UP
//
// @info : Define Sidebar for connect user
// @detail : 
// ------------------------------------------------------------------------------------

/** -------------------------------------------------------------------------------------------
 * Maintenant, on va limiter l'accès aux modules en fonction du NIVEAU
 * --> Utilisation de
 */

// ------------------------------------------------------------------------------------
// Require module class
// ------------------------------------------------------------------------------------
require_once dirname(__FILE__) . '/../classes/class.module.php';

// ------------------------------------------------------------------------------------
// Init content sidebar
// ------------------------------------------------------------------------------------
$ContentSidebar = NULL;

// ------------------------------------------------------------------------------------
// PREPA QUERY - Search Bloc
// ------------------------------------------------------------------------------------
$BlocStmt = Bloc::selectAll(DbConnexion::getInstance(), Bloc::FIELDNAME_ORDREBLOC);
while ($InfoBloc = Bloc::fetch(DbConnexion::getInstance(), $BlocStmt)) {

    // ------------------------------------------------------------------------------------
    // If name bloc is not empty we create a li separator with the name
    // ------------------------------------------------------------------------------------
    #if($InfoBloc->getLibelleBloc() != ''){
    //$ContentSidebar .= '<li class="li_separator_section">' . $InfoBloc->getLibelleBloc() . '<ul class="sub-menu">';

    $ContentSidebar .= '
    <li>
        <a  href="javascript:;"><span class="title li_separator_section title_bloc">' . $InfoBloc->getLibelleBloc() . '</span></a></li>
    ';
    #}

    #echo '<pre>';
    #print_r($_SESSION);
    #echo '</pre>';

    // ------------------------------------------------------------------------------------
    // PREPA QUERY - Search module for the bloc
    // ------------------------------------------------------------------------------------
    $ModuleStmt = Module::selectByBloc(DbConnexion::getInstance(), $InfoBloc, Module::FIELDNAME_ORDREMODULE);
    while ($InfoModule = Module::fetch(DbConnexion::getInstance(), $ModuleStmt)) {

        #if ($InfoModule->getLevelMax() >= $_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME]){
        $typeActif = (filter_has_var(INPUT_GET, 'ap') && filter_input(INPUT_GET, 'ap') == $InfoModule->getCheminModule()) ? true : false;

        /** ---------------------------------------------------------------------------------------
         * Seuls les modules actifs autorisés
         */
        $authorized = (($InfoModule->getLevelMax() >= $_SESSION['FK_idTypeUtilisateur' . PROJECT_NAME]) ? 1 : 0);

        /** ---------------------------------------------
         *
         */
        #echo 'Module #'.$InfoModule->getIdModule().' '.$InfoModule->getLevelMax().' / '.$_SESSION['FK_idTypeUtilisateur'.PROJECT_NAME].' -> '.$authorized.'<br/>';
        if ($InfoModule->getBoolActif() && $authorized == 1) {
            // ------------------------------------------------------------------------------------
            // If Module has sub-module we search all sub-modules
            // ------------------------------------------------------------------------------------
            if ($InfoModule->getBoolSousModule()) {

                // ------------------------------------------------------------------------------------
                // Init sub-module variable
                // ------------------------------------------------------------------------------------
                $HeaderAcc = $FooterAcc = $ContentSubModule = '';

                // ------------------------------------------------------------------------------------
                // Check if module type is "accordion"
                // ------------------------------------------------------------------------------------
                if ($InfoModule->getBoolAccordeon()) {
                    $HeaderAcc = '
                    <li class="' . (($typeActif) ? 'active open' : '') . '">
                        <a href="javascript:;">
                            <i class="' . $InfoModule->getLogoModule() . '"></i>
                            <span class="title"> ' . $InfoModule->getLibelleModule() . ' </span>'
                        . ($typeActif ? '<span class="selected"></span>' : '') .
                        '<span class="arrow ' . (($typeActif) ? 'open' : '') . '"></span>
                        </a>
                        <ul class="sub-menu">';

                    $FooterAcc = '
                    </ul></li>';
                }

                // ------------------------------------------------------------------------------------
                // Search sub-module for module
                // ------------------------------------------------------------------------------------
                $SubModuleStmt = Sous_Module::selectByModule(DbConnexion::getInstance(), $InfoModule, Sous_Module::FIELDNAME_ORDRESOUSMODULE);
                while ($InfoSubModule = Sous_Module::fetch(DbConnexion::getInstance(), $SubModuleStmt)) {
                    if ($InfoSubModule->getBoolActif()) {
                        $typeSActif = (filter_has_var(INPUT_GET, 'ss_m') && filter_input(INPUT_GET, 'ss_m') == $InfoSubModule->getCheminsousmodule()) ? true : false;

                        if ($InfoModule->getBoolAccordeon()) {

                            // ------------------------------------------------------------------------------------
                            // Check if sub-module have a notification function's
                            // ------------------------------------------------------------------------------------
                            $Badge = ($InfoSubModule->getFonctionnotification() != '') ? '<span class="' . $InfoSubModule->getTypeNotification() . '">0</span>' : '';

                            // ------------------------------------------------------------------------------------
                            // Add link
                            // ------------------------------------------------------------------------------------
                            $ContentSubModule .= '
                            <li class="' . (($typeSActif) ? 'active' : '') . '">
                                <a href="index.php?ap=' . $InfoModule->getCheminModule() . '&ss_m=' . $InfoSubModule->getCheminsousmodule() . '">
                                    ' . $Badge . '
                                    ' . $InfoSubModule->getLibellesousmodule() . '
                                </a>
                            </li>';
                        } else {

                            // ------------------------------------------------------------------------------------
                            // Add link
                            // ------------------------------------------------------------------------------------
                            $ContentSubModule .= '
                            <li class="' . (($typeSActif) ? 'active' : '') . '">
                                <a href="index.php?ap=' . $InfoModule->getCheminModule() . '&ss_m=' . $InfoSubModule->getCheminsousmodule() . '">
                                    <i class="fa fa-file-text"></i>
                                    <span class="title">' . $InfoSubModule->getLibellesousmodule() . '</span>
                                </a>
                            </li>';
                        }

                        if (!is_dir('modules/' . $InfoModule->getCheminModule())) {
                            mkdir('modules/' . $InfoModule->getCheminModule() . '/', 0777);
                        }
                        if (!is_dir('modules/' . $InfoModule->getCheminModule() . '/' . $InfoSubModule->getCheminsousmodule())) {
                            mkdir('modules/' . $InfoModule->getCheminModule() . '/' . $InfoSubModule->getCheminsousmodule() . '/', 0777);
                            $fp = fopen('modules/' . $InfoModule->getCheminModule() . '/' . $InfoSubModule->getCheminsousmodule() . '/m_' . $InfoSubModule->getCheminsousmodule() . '.php', 'w+');
                            fclose($fp);
                            $fp = fopen('modules/' . $InfoModule->getCheminModule() . '/' . $InfoSubModule->getCheminsousmodule() . '/s_' . $InfoSubModule->getCheminsousmodule() . '.js', 'w+');
                            fclose($fp);
                        }

                    }
                }

                // ------------------------------------------------------------------------------------
                // Add sub-module's to the content
                // ------------------------------------------------------------------------------------
                $ContentSidebar .= $HeaderAcc . $ContentSubModule . $FooterAcc;
            }

            // ------------------------------------------------------------------------------------
            // Else we put module link
            // ------------------------------------------------------------------------------------
            else {
                $ContentSidebar .= '
                <li class="' . (($typeActif) ? 'active selected' : '') . '">
                    <a href="index.php?ap=' . $InfoModule->getCheminModule() . '">
                        <i class="fa ' . $InfoModule->getLogoModule() . '"></i>' .
                    ($typeActif ? '<span class="selected"></span>' : '')
                    . '<span class="title">' . $InfoModule->getLibelleModule() . '</span>
                        </a>
                </li>';

                if (!is_dir('modules/' . $InfoModule->getCheminModule())) {
                    mkdir('modules/' . $InfoModule->getCheminModule() . '/', 0777);
                    $fp = fopen('modules/' . $InfoModule->getCheminModule() . '/m_' . $InfoModule->getCheminModule() . '.php', 'w+');
                    fclose($fp);
                    $fp = fopen('modules/' . $InfoModule->getCheminModule() . '/s_' . $InfoModule->getCheminModule() . '.js', 'w+');
                    fclose($fp);
                }
            }
        }
        #}
        #else {
        #}
    }
    $ContentSidebar .= '</li>';

}

?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <?php
            // ------------------------------------------------------------------------------------
            // Echo content sidebar for user
            // ------------------------------------------------------------------------------------
            echo $ContentSidebar;
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->